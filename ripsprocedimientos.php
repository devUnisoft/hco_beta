﻿<?php
error_reporting(0);
//conexion
$pdo = new PDO('mysql:host=localhost;dbname=hco', 'root', 'usc2017*');
//
//recibo fechas
$fechainicial =  $_REQUEST["f1"];
$fechafinal = $_REQUEST["f2"];
//convierto fecha
$fechainicial = date("d-m-Y", strtotime($fechainicial));
//agrego la hora
$fechainicial= $fechainicial." 01:00:00";
//convierto fecha
$fechafinal = date("d-m-Y", strtotime($fechafinal));
//agrego la hora
$fechafinal= $fechafinal." 24:00:00";
//query
$query = "SELECT * FROM pagos INNER JOIN clinicas ON pagos.clinica=clinicas.razonsocial  INNER JOIN presupuestos ON pagos.procedimiento=presupuestos.nombreprocedimiento INNER JOIN evolutions ON pagos.procedimiento=evolutions.procedimiento  where  evolutions._date <= '$fechafinal' and _date  >= '$fechainicial'";

$result = $pdo->query($query);
//año actual
$fecha_actual=date("Y");
//fecha y hora
$fecha_hora=date("Y-m-d h:i:s");


$data = '';

while ($row = $result->fetch(PDO::FETCH_OBJ)) {

		//abrevio tipo documento
        if($row->tipodocumento == "Cedula de Ciudadania"){
            $tipo = "CC";
        }
        if($row->tipodocumento == "Cedula de Extranjeria"){
            $tipo = "CE";
        }
        if($row->tipodocumento == "Registro Civil"){
            $tipo = "RC";
        }
        if($row->tipodocumento == "Tarjeta de Identidad"){
            $tipo = "TI";
        }
        if($row->tipodocumento == "NIT"){
            $tipo = "NT";
        }
        if($row->tipodocumento == "Adulto sin identificacion"){
            $tipo = "AS";
        }
        if($row->tipodocumento == "Menor sin identificacion"){
            $tipo = "MS";
        }if($row->tipodocumento == "Pasaporte"){
            $tipo = "PA";
        }
        
        //tipo de finalidad procedimiento
        if($row->type == "Consulta de primera vez por odontologia especializada" OR $row->type == "Consulta de primera vez por odontologia general"){
            $finalidad = "1";
        }else{
            $finalidad = "2";
        }

        

        $cups = explode(",", $row->diagnosticoprincipal);
        $cupgeneral = $cups[0];

        $querycups = "SELECT * FROM cups where subcategory='$cupgeneral' ";
        $resultc = $pdo->query($querycups);
        while ($rowc = $resultc->fetch(PDO::FETCH_OBJ)) {
        $cupgeneral = $rowc->code;	
        }

        
        //validacion de diagnostico
        $diagnostico = explode(",", $row->nuevodiagnostico);
        $count = count($diagnostico);
        if($count > 0){
        //partes de diagnostico	
        $diagnostico1 = $diagnostico[0];
        $diagnostico2 = $diagnostico[1];
        $diagnostico3 = $diagnostico[2];
        $diagnostico4 = $diagnostico[3];

        $query2 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico1' ";
        $result2 = $pdo->query($query2);
        while ($row2 = $result2->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad1 = $row2->codigo;	
        }

        $query3 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico2' ";
        $result3 = $pdo->query($query3);
        while ($row3 = $result3->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad2 = $row3->codigo;	
        }
        $query4 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico3' ";
        $result4 = $pdo->query($query4);
        while ($row4 = $result4->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad3 = $row4->codigo;	
        }
        $query5 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico4' ";
        $result5 = $pdo->query($query5);
        while ($row5 = $result5->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad4 = $row5->codigo;	
        }

        }else{

        $query6 = "SELECT * FROM enfermedades_evolucion where nombre='$row->nuevodiagnostico' ";
        $result6 = $pdo->query($query6);
        while ($row6 = $result6->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad1 = $row6->codigo;	
        }
        $codigoenfermedad2 ="";
        $codigoenfermedad3 ="";
        $codigoenfermedad4 ="";
        }
        //remplazo las , del valor
        $salida = str_replace(',', '', $row->valor);
        $valorformateado = $salida;

        

        $data .= "{$row->consecutivo},{$row->codigoprestador},{$tipo},{$row->documento},{$row->fecha},,$cupgeneral,1,$finalidad,,$codigoenfermedad1,$codigoenfermedad2,,,$valorformateado\r\n";
}
//saco el semestre para enviarlo en el nombre del txt
 $mes = date("m",strtotime($fecha_hora));
$mes = is_null($mes) ? date('m') : $mes;
$trim=floor(($mes-1) / 6)+1;
//tipos de header a enviar
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-disposition: attachment; filename="AP'.$fecha_actual.'-'.$trim.'.txt"');
echo $data;

?>