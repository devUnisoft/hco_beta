<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Registro Usuario Nuevo</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
	</script>
	<style type="text/css">
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}

		header {
    width: 100%;
    height: 150px;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
	</style>
	<script type="text/javascript">
		function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
         window.onload = init();
	</script>
	<script>
		function mostrar(enla) {
		  obj = document.getElementById('oculto');
		  obj.style.visibility = (obj.style.visibility == 'hidden') ? 'visible' : 'hidden';
		  enla.innerHTML = (enla.innerHTML == '-') ? '+' : '-';
		}
	</script>		
</head>
<body>

<header>
    <div class="container clearfix">
        </br>
        <center><img src="images/logo_blanco.png"  height="15%" width="15%"></center>
    </div>
</header>

<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
	<div class="row">
		</br>
		<center><h2>FORMULARIO DE REGISTRO</h2></center>
		<div class="col-md-2">
		</div>
		<div class="col-md-8">

			<form role="form" name="fcontacto" method="post" action="MensajeInsercionUsuarioAdm.php">
				<div class="form-group">
					 
					<label for="tipodocumento">
						Tipo de Documento
					</label>
					<select id="tipodedocumento" name="tipodedocumento" class="form-control" required>
					  <option value="Cedula de Ciudadania">Cedula de Ciudadania</option>
					  <option value="Registro Civil">Registro Civil</option>
					  <option value="Tarjeta de Identidad">Tarjeta de Identidad</option>
					  <option value="Cedula de Extranjeria">Cedula de Extranjeria</option>
					  <option value="NIT">NIT</option>
					</select>
				</div>
				<div class="form-group">
					<label for="numerodocumento">
						Numero de Documento
					</label>
					<input type="text" class="form-control" id="numerodocumento" name="numerodocumento" onclick="mostrar(this);" required />
				</div>
				<div id="oculto" style="visibility:hidden" class="form-group">
				<div class="col-md-6">
				<label for="nombre">
						Nombre
					</label>
					<input type="text" class="form-control" id="nombre" name="nombre" required />
					<label for="correo">
						E-mail
					</label>
					<input type="email" class="form-control" id="correo" name="correo" required />
					<label for="fecha">
						Fecha de Nacimiento
					</label>
					<input type="date" class="form-control" id="fecha" name="fecha" required />
					<label for="lugarr">
						Lugar de Residencia
					</label>
					<input type="text" class="form-control" id="lugarr" name="lugarr" required />
					<label for="pass1">
						Repita su Contraseña
					</label>
					<input type="password" class="form-control" id="pass1" name="pass1" required />
					
					</div>

				<div class="col-md-6">
					<label for="apellido">
						Apellido
					</label>
					<input type="text" class="form-control" id="apellido" name="apellido" required />

					<label>Sexo</label>
					</br> 
					<label>Masculino</label>           
                    <input type = "radio" name = "radSize" id = "radSize" value = "M" required  /> 
                    <label>Femenino</label>
                    <input type = "radio" name = "radSize" id = "radSize" value = "F" required  />
                    </br>
                    <label for="lugarn">
						Lugar de Nacimiento
					</label>
					<input type="text" class="form-control" id="lugarn" name="lugarn" required />
					</br>
					<label for="pass">
						Contraseña
					</label>
					<input type="password" class="form-control" id="pass" name="pass" required />
				    </br>
                    </br>
                    </br>
				</div>
				</br>
				</br>
				<div class="btn-group">
				<button type="submit" class="btn btn-primary pull-right">
					Crear Usuario
				</button>
				</div>
				<div class="btn-group">
				<button type='button' value='Atras' onClick='history.go(-1);' class="btn btn-primary pull-right">Regresar</button>
				</div>
				</br>
				</br>
				</div>
			</form>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>


	
</body>
</html>