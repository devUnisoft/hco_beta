<?php
	require_once('fpdf/fpdf.php');	
	require_once 'db_config.php';
	session_start();

 	$con = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysql_error());
    mysqli_set_charset($con,"utf8");

	class PDF extends FPDF {	
	    function HeaderTabla($header) {
	    	$this->SetTextColor(6, 131, 201); //Letra color blanco    	
	    	$this->Cell(110,5,"DESCRIPCION","R",0,'L');	
		    foreach($header as $col){
				$this->SetTextColor(6, 131, 201); //Letra color blanco
		    	$this->Cell(25,5,$col,"R",0,'C');			    
		   	}
		    
	   	}

	   	function Tabla($header, $descripcion) {
	    	$this->SetTextColor(0, 0, 0); //Letra color blanco    	
	    	$this->Cell(110,5,$descripcion,"R",0,'L');	
		    foreach($header as $col){
				$this->SetTextColor(0, 0, 0); //Letra color blanco
		    	$this->Cell(25,5,$col,"R",0,'C');			    
		   	}
		    $this->Ln(6);
	   	}

	   	function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '') {
	        $k = $this->k;
	        $hp = $this->h;
	        if($style=='F')
	            $op='f';
	        elseif($style=='FD' || $style=='DF')
	            $op='B';
	        else
	            $op='S';
	        $MyArc = 4/3 * (sqrt(2) - 1);
	        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

	        $xc = $x+$w-$r;
	        $yc = $y+$r;
	        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
	        if (strpos($corners, '2')===false)
	            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
	        else
	            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

	        $xc = $x+$w-$r;
	        $yc = $y+$h-$r;
	        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
	        if (strpos($corners, '3')===false)
	            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
	        else
	            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

	        $xc = $x+$r;
	        $yc = $y+$h-$r;
	        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
	        if (strpos($corners, '4')===false)
	            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
	        else
	            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

	        $xc = $x+$r ;
	        $yc = $y+$r;
	        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
	        if (strpos($corners, '1')===false)
	        {
	            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
	            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
	        }
	        else
	            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
	        $this->_out($op);
	    }

	    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
	        $h = $this->h;
	        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
	            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	    }

	    function numtoletras($xcifra) {
		    $xarray = array(0 => "Cero",
		        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		    );
			//
		    $xcifra = trim($xcifra);
		    $xlength = strlen($xcifra);
		    $xpos_punto = strpos($xcifra, ".");
		    $xaux_int = $xcifra;
		    $xdecimales = "00";
		    if (!($xpos_punto === false)) {
		        if ($xpos_punto == 0) {
		            $xcifra = "0" . $xcifra;
		            $xpos_punto = strpos($xcifra, ".");
		        }
		        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		    }
		 
		    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		    $xcadena = "";
		    for ($xz = 0; $xz < 3; $xz++) {
		        $xaux = substr($XAUX, $xz * 6, 6);
		        $xi = 0;
		        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		        $xexit = true; // bandera para controlar el ciclo del While
		        while ($xexit) {
		            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
		                break; // termina el ciclo
		            }
		 
		            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
		            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
		            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
		                switch ($xy) {
		                    case 1: // checa las centenas
		                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
		                             
		                        } else {
		                            $key = (int) substr($xaux, 0, 3);
		                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
		                                $xseek = $xarray[$key];
		                                $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
		                                if (substr($xaux, 0, 3) == 100)
		                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
		                            }
		                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
		                                $key = (int) substr($xaux, 0, 1) * 100;
		                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
		                                $xcadena = " " . $xcadena . " " . $xseek;
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 0, 3) < 100)
		                        break;
		                    case 2: // checa las decenas (con la misma lógica que las centenas)
		                        if (substr($xaux, 1, 2) < 10) {
		                             
		                        } else {
		                            $key = (int) substr($xaux, 1, 2);
		                            if (TRUE === array_key_exists($key, $xarray)) {
		                                $xseek = $xarray[$key];
		                                $xsub = $this->subfijo($xaux);
		                                if (substr($xaux, 1, 2) == 20)
		                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3;
		                            }
		                            else {
		                                $key = (int) substr($xaux, 1, 1) * 10;
		                                $xseek = $xarray[$key];
		                                if (20 == substr($xaux, 1, 1) * 10)
		                                    $xcadena = " " . $xcadena . " " . $xseek;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 1, 2) < 10)
		                        break;
		                    case 3: // checa las unidades
		                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
		                             
		                        } else {
		                            $key = (int) substr($xaux, 2, 1);
		                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
		                            $xsub = $this->subfijo($xaux);
		                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                        } // ENDIF (substr($xaux, 2, 1) < 1)
		                        break;
		                } // END SWITCH
		            } // END FOR
		            $xi = $xi + 3;
		        } // ENDDO
		 
		        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
		            $xcadena.= " DE";
		 
		        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
		            $xcadena.= " DE";
		 
		        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		        if (trim($xaux) != "") {
		            switch ($xz) {
		                case 0:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN BILLON ";
		                    else
		                        $xcadena.= " BILLONES ";
		                    break;
		                case 1:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN MILLON ";
		                    else
		                        $xcadena.= " MILLONES ";
		                    break;
		                case 2:
		                    if ($xcifra < 1) {
		                        $xcadena = "CERO PESOS $xdecimales/100 M.N.";
		                    }
		                    if ($xcifra >= 1 && $xcifra < 2) {
		                        $xcadena = "UN PESO $xdecimales/100 M.N. ";
		                    }
		                    if ($xcifra >= 2) {
		                        $xcadena.= " PESOS $xdecimales/100 M.N. "; //
		                    }
		                    break;
		            } // endswitch ($xz)
		        } // ENDIF (trim($xaux) != "")
		        // ------------------      en este caso, para México se usa esta leyenda     ----------------
		        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		    } // ENDFOR ($xz)
		    return trim($xcadena);
		}
		 
		// END FUNCTION
		 
		function subfijo($xx)
		{ // esta función regresa un subfijo para la cifra
		    $xx = trim($xx);
		    $xstrlen = strlen($xx);
		    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		        $xsub = "";
		    //
		    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		        $xsub = "MIL";
		    //
		    return $xsub;
		}

   	}
   	$document = $_GET['identificacion']; 
   	$idFactura = $_GET['idFactura']; 
   	$centro_radiologico = $_SESSION['id_centro_radiologico']; 
   	$result = mysqli_query($con, "SELECT * FROM users where document__number='$document'"); 
   	$rows = mysqli_fetch_array($result, MYSQLI_ASSOC);  
   	$total = 0;

	$pdf = new PDF();
	//Primera página
	$pdf->AddPage();
	
	$resultLogo = mysqli_query($con, "SELECT * FROM centro_radiologico where id='$centro_radiologico'"); 
   	$rowLogo = mysqli_fetch_array($resultLogo, MYSQLI_ASSOC);

	$pdf->Image($rowLogo["logo"], 5, 10, 60, 'PNG', '');
	// Texto centrado en una celda con cuadro 20*10 mm y salto de línea
	$pdf->SetFont('Arial','I',18);
	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(131);
	$pdf->Cell(60,0,'Recibo de pago No. ' . $idFactura,0,1,'C');
	$pdf->Ln(10);	

	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetFont('Arial','B',8);
	$pdf->SetX(40);
	$pdf->Cell(80,5,'No. IDENTIFICACION',0,0,'R');
	$pdf->SetTextColor(155, 159, 161);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->SetX(120);
	$pdf->Cell(80,5,$document,0,1,'C');
	$pdf->RoundedRect(120, 20, 80, 5, 1, '1234', '');
	$pdf->Ln(1);

	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(40);
	$pdf->Cell(80,5,'NOMBRE',0,0,'R');
	$pdf->SetTextColor(155, 159, 161);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->SetX(120);
	$pdf->Cell(80,5,$rows["first_name"] . " " . $rows["last_name"],0,1,'C');
	$pdf->RoundedRect(120, 26, 80, 5, 1, '1234', '');
	$pdf->Ln(1);

	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(40);
	$pdf->Cell(80,5,'DIRECCION',0,0,'R');
	$pdf->SetTextColor(155, 159, 161);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->SetX(120);
	$pdf->Cell(80,5,$rows["lugar_residencia"],0,1,'C');
	$pdf->RoundedRect(120, 32, 80, 5, 1, '1234', '');
	$pdf->Ln(1);

	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(40);
	$pdf->Cell(80,5,'TELEFONO',0,0,'R');
	$pdf->SetTextColor(155, 159, 161);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->SetX(120);
	$pdf->Cell(80,5,$rows["telefono"] . " - " . $rows["celular"],0,1,'C');
	$pdf->RoundedRect(120, 38, 80, 5, 1, '1234', '');
	$pdf->Ln(1);

	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(40);
	$pdf->Cell(80,5,'EMAIL',0,0,'R');
	$pdf->SetTextColor(155, 159, 161);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->SetX(120);
	$pdf->Cell(80,5,$rows["email"],0,1,'C');
	$pdf->RoundedRect(120, 44, 80, 5, 1, '1234', '');

	$pdf->Ln();
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->Line(10,54,200,54);

	$pdf->Ln(2);
	$header = array('CANTIDAD', 'VR.UNITARIO', 'SUBTOTAL');
	$pdf->HeaderTabla($header);	
	$pdf->Ln(6);

	$resultDetalle = mysqli_query($con, "SELECT * FROM `detallefacturaradiologia` WHERE `id_factura_radiologia`='$idFactura'");

   	while ($rowDetalle = mysqli_fetch_array($resultDetalle, MYSQLI_ASSOC)) {
   		$datos = array($rowDetalle["cantidad"], $rowDetalle["precio"], $rowDetalle["total"]);
		$pdf->Tabla($datos, $rowDetalle["descripcion"]);
		$total += $rowDetalle["total"];
   	}
	for ($i=0; $i < 5; $i++) { 
		
	}
	$total = number_format($total,0, ',', '.');

	$pdf->Ln(20);
	$pdf->SetTextColor(6, 131, 201); //Letra color blanco
	$pdf->SetX(115);
	$pdf->Cell(25,5,"VALOR TOTAL",0,0,'R');		
	$pdf->Cell(60,5,$total,1,0,'C');		

	$pdf->SetTextColor(204, 204, 204); //Letra color blanco
 	$pdf->SetDrawColor(204, 204, 204);
	$pdf->SetX(10);
	$pdf->SetMargins(0, 0, 0);
	$pdf->Cell(80,15,"Son: " . $pdf->numtoletras($total) . " pesos M-L",1,0,'T');	

	$pdf->Ln(30);
	$pdf->SetX(15);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->Cell(70,8,"Firma de Funcionario","T",0,'T');

	$pdf->SetX(120);
 	$pdf->SetDrawColor(6, 131, 201);
	$pdf->Cell(70,8,"Firma de Cliente","T",0,'T');	
	$pdf->Output();
	
?>