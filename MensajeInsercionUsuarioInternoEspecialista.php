<?php //error_reporting(E_ALL);?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Mensaje Insersion</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
	</script>
	<style type="text/css">
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}

		header {
    width: 100%;
    height: 150px;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
	</style>
	<script type="text/javascript">
		function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
         window.onload = init();
	</script>	
</head>
<body>
<header>
    <div class="container clearfix">
        </br>
        <center><img src="images/logo_blanco.png"  height="15%" width="15%"></center>
    </div>
</header>
<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6">
         <?php 
         //error_reporting(0);
         //inicio de session
         session_start();
         //valores por post de RegistroUsuario.php
            
             $tipodedocumento =  $_REQUEST["tipodedocumento"];
             $numerodocumento = $_REQUEST["numerodocumento"];
             $nombre = $_REQUEST["nombre"];
             $correo = $_REQUEST["correo"];
             $fecha = $_REQUEST["fecha"];
             $departr = $_REQUEST["tipo"];
             $lugarr = $_REQUEST["lugarr"];
             $apellido = $_REQUEST["apellido"];
             $radSize = $_REQUEST["radSize"];
             $departn = $_REQUEST["tipo2"];
             $lugarn = $_REQUEST["lugarn"];
             $zona = $_REQUEST["Zona"];
             
             $clinica = $_REQUEST["clinica"];
             $emailespecialista = $_REQUEST["emailesp"];
             $direccion = $_REQUEST["direccion"];
             $telefono = $_REQUEST["telefono"];
             $tipousuario = $_REQUEST["tipodeusuario"];
             $acudientenombre = $_REQUEST["acudientenombre"];
             $acudientetelefono = $_REQUEST["acudientetelefono"];
             $acudienteparentesco = $_REQUEST["acudienteparentesco"];

            $otro_lugar_nac = $_REQUEST["txt_otro_lugar_nacimiento"];
            $otro_lugar_resi = $_REQUEST["txt_otro_lugar_residencia"];

            if($departr == "Otro"){
                $lugarr = $otro_lugar_nac;
            }

            if($departn == "Otro"){
                $lugarn = $otro_lugar_resi;
            }

            //print_r($_POST);
            //conexion
            require_once 'db_connect.php';
            // connecting to db
            $db = new DB_CONNECT();

            //selecciono datos en busqueda
            $resultuserrepeat = mysql_query("SELECT * FROM users where (document__number='$numerodocumento' or  email='$correo') and  clinica='$clinica'  ");
            $rows = mysql_num_rows($resultuserrepeat);
            
            function redireccionar($direccion,$mensaje=""){
                   
                ?>
                   <script language="JavaScript">
                    setTimeout(function(){                        
                        window.location="<?php echo $direccion?>";
                    },300);
                    </script> 
                <?php
                }

            if ($rows > 0) {
                // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-warning\" role=\"alert\">";
                            echo "Error el usuario ya se encuentra registado";
                            echo "</div>";
                            echo "</div>";
                            //header("refresh:3;url=listapacientesespecialista.php");
                            redireccionar("listapacientesespecialista.php");
            }else{  


        if (empty($_FILES['foto']['name'])) {         

        
            // mysql inserting a new row
            $result = mysql_query("INSERT INTO users(__t,first_name,last_name,email,birthdate,gender,document__document_type__oid,document__number,active,lugar_residencia,lugar_nacimiento,clinica,especialista,telefono,direccion,zona,tipousuario,acudientenombre,acudienteparentesco,acudientetelefono) 
            VALUES('person', '".$nombre."', '".$apellido."' , '".$correo."', '".$fecha."', '".$radSize."', '".$tipodedocumento."', '".$numerodocumento."', 'True', '".$lugarr."', '".$lugarn."', '".$clinica."', '".$emailespecialista."', '".$telefono."', '".$direccion."', '".$zona."', '".$tipousuario."', '".$acudientenombre."', '".$acudienteparentesco."', '".$acudientetelefono."')");
            // check if row inserted or not
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Usuario";
                            echo "</div>";
                            echo "</div>";
                            //header("refresh:3;url=consultapacientes.php?usuario=$correo");
                            redireccionar("consultapacientes.php?usuario={$correo}");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Usuario";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            //header( "refresh:3;url=listapacientesespecialista.php" );
                            redireccionar("listapacientesespecialista.php");
            }  
        





         }else{

             //directorio de subida de imagenes al servidor
         $dir_destino = './uploads/';
         $imagen_subida = $dir_destino .$numerodocumento.basename($_FILES['foto']['name']);
         //validaciones
         if(!is_writable($dir_destino)){
            echo "no tiene permisos";
        }else{

        if(is_uploaded_file($_FILES['foto']['tmp_name'])){

            if (move_uploaded_file($_FILES['foto']['tmp_name'], $imagen_subida)) {

            
            // mysql inserting a new row
            $result = mysql_query("INSERT INTO users(__t,first_name,last_name,email,birthdate,gender,document__document_type__oid,document__number,active,lugar_residencia,lugar_nacimiento,clinica,especialista,image_url,telefono,direccion,zona,tipousuario,acudientenombre,acudienteparentesco,acudientetelefono) VALUES('person', '".$nombre."', '".$apellido."' , '".$correo."', '".$fecha."', '".$radSize."', '".$tipodedocumento."', '".$numerodocumento."', 'True', '".$lugarr."', '".$lugarn."', '".$clinica."', '".$emailespecialista."', '".$imagen_subida."', '".$telefono."', '".$direccion."', '".$zona."', '".$tipousuario."', '".$acudientenombre."', '".$acudienteparentesco."', '".$acudientetelefono."')");
            // check if row inserted or not
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Usuario";
                            echo "</div>";
                            echo "</div>";
                            //header("refresh:3;url=consultapacientes.php?usuario=$correo");
                             redireccionar("consultapacientes.php?usuario={$correo}");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Usuario";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            //header( "refresh:3;url=listapacientesespecialista.php" );
                             redireccionar("listapacientesespecialista.php");
            }
               


        }else{


            
            // mysql inserting a new row
            $result = mysql_query("INSERT INTO users(__t,first_name,last_name,email,birthdate,gender,document__document_type__oid,document__number,active,lugar_residencia,lugar_nacimiento,clinica,especialista,telefono,direccion,zona,tipousuario,acudientenombre,acudienteparentesco,acudientetelefono) VALUES('person', '".$nombre."', '".$apellido."' , '".$correo."', '".$fecha."', '".$radSize."', '".$tipodedocumento."', '".$numerodocumento."', 'True', '".$lugarr."', '".$lugarn."', '".$clinica."', '".$emailespecialista."', '".$telefono."', '".$direccion."', '".$zona."', '".$tipousuario."', '".$acudientenombre."', '".$acudienteparentesco."', '".$acudientetelefono."')");
            // check if row inserted or not
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Usuario";
                            echo "</div>";
                            echo "</div>";
                            //header("refresh:3;url=consultapacientes.php?usuario=$correo");
                             redireccionar("consultapacientes.php?usuario={$correo}");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Usuario";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            //header( "refresh:3;url=listapacientesespecialista.php" );
                             redireccionar("listapacientesespecialista.php");
            }   





        

        }}}

         } 
         }  
            ?>
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
</body>
</html>