﻿<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Login Hco</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style type="text/css">

body {
    background-image: url("images/Test-Group.jpg") ;
    background-position: center;
    background-color:#f6f6f6;
    background-repeat: no-repeat;
}

html, body, .hco-content {
    height: 100%;
    position: relative;
}

.blank {
    height: 100%;
}
@media (min-width: 992px){
.container {
    width: 970px;
}

html, body, .hco-content {
    height: 100%;
    position: relative;
}
}

@media (max-width: 768px){
.container {
    width: auto;
}

html, body, .hco-content {
    height: 80%;
    position: relative;
}


}

}
.container {
    margin-right: auto;
    margin-left: auto;
    padding-left: 15px;
    padding-right: 15px;
}

.login {
    position: relative;
    top: 10%;
    height: 30%;
    width: 100%;
    margin-top: -15%;
    font-weight: 300;
}

.hidden-xs {
    display: block !important;
}

@media (min-width: 992px){}
.col-md-1 {
    width: 8.333333333333332%;
}}

@media (min-width: 992px){}
.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
}}

@media (min-width: 992px){
.col-md-3 {
    width: 25%;
}
}

@media (min-width: 992px){
.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
}
}


@media (min-width: 992px){
.col-md-2 {
    width: 16.666666666666664%;
}
}

@media (min-width: 992px){
.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
}
}

.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
}

@media (min-width: 992px){
.col-md-6 {
    width: 50%;
}
}

@media (min-width: 992px){
.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
    float: left;
}
.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
}}

.login table {
    border-spacing: 5px;
    border-collapse: separate;
}
table, td {
    padding: 0;
    margin: 0;
    vertical-align: top;
}
table {
    max-width: 100%;
    background-color: transparent;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}

.login button[type="submit"] {
    background-image: url(images/btn-login.png);
    background-color: transparent;
    border: none;
    background-repeat: no-repeat;
    background-position: center top;
    height: 73px;
    width: 62px;
    text-align: center;
    padding-top: 44px;
}

.login .logo {
    max-width: 266px;
    text-align: center;
    margin: 0 auto;
    margin-top: 10%;
}

.img-responsive {
    display: block;
    max-width: 80%;
    height: auto;
    margin-top: 20px;
}
img {
    vertical-align: middle;
}
img {
    border: 0;
}

h1, h2, h3 {
    color: #3ca3c1;
}
h1, .h1 {
    font-size: 36px;
}
h1, h2, h3 {
    margin-top: 20px;
    margin-bottom: 10px;
}
h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
    font-family: "Lato", sans-serif;
    font-weight: 500;
    line-height: 1.1;
    color: #3ca3c1;
}
h1 {
    font-size: 2em;
    margin: 0.67em 0;
}

.form-control {
    border: 2px solid #676767;
    background: transparent;
    height: 34px;
    border-radius: 10px;
}


@media (min-width: 768px) and (max-width: 992px) { 
.container {
    width: 100%;
    padding-left: 150px;
}
}

.modal-header {
    padding: 15px;
    border-bottom: transparent;
}
.modal-footer {
    padding: 0px; 
    text-align: right;
    border-top: 1px solid transparent;
    margin-left: 30px;
    margin-right: 30px;
}
.modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 3px solid #385d8a;
    border-radius: 16px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
}
</style>

</head>
<body>
<div class="col-md-12">
<div class="col-md-6 col-sm-6 pull-left">
</br>
</br>
   <font  style="color: #0288d1;font-family:Calibri;text-shadow: 1px 2px gray;font-size: 80px;"><b>TEST - GROUP HCO</b></font>
</div>
<div class="col-md-3 col-sm-6 pull-right">
            <img src="images/logohco.png" class="img-responsive logo">
</div>
</div>
		<!-- uiView:  --><div ui-view="" class="hco-content ng-scope"><!-- uiView:  --><div class="container blank ng-scope" ui-view=""><div class="login ng-scope">
    <div class="content">
        <div class="col-md-1 hidden-xs">
        </div>
        <div class="col-md-2 hidden-xs">
        </div>
        <div class="col-md-6">
            <form id="frmLogin" method="post" action="FormularioObservaciones.php">
                <table>
                    <tbody><tr>
                        <td colspan="2">
                            <h1>INGRESA</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input class="form-control" tabindex="1" type="email" id="txtUsuario" name="txtUsuario"  size="50" placeholder="Email" required="required" autofocus="">
                        </td>
                        <td rowspan="2" class="sep-left text-center">
                            <button type="submit" tabindex="3">Ingresar</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input class="form-control " tabindex="2" type="password" d="txtClave" name="txtClave" placeholder="Contraseña" size="50"  required="required">
                            <input type="hidden" id="text" name="text" value="<?php echo $perfil ?>"/>
                        </td>
                    </tr>
                    <tr>
                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php
							//mensaje al usuario	 
							if(isset($_GET['mensaje'])) {
			                // si no pasa mensaje por get no se evalua
							echo "<div class=\"alert alert-danger\">";
							echo "<strong>Error! </strong>"; 
							echo $_GET['mensaje'];
							echo "</div>";
							}	
							?>
                        </td>
                    </tr>
                </tbody></table>
            </form>
        </div>
        <div class="col-md-1 hidden-xs">
        </div>
    </div>
</div>
</div>

   
            </div>
        </div>
    </div>
    </div>   
</body>
</html>