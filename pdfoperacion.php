<?php

require('fpdf/fpdf.php');
require_once 'db_connect.php';
header("Content-Type: text/html; charset=iso-8859-1 ");
// connecting to db
$db = new DB_CONNECT();

//fecha
$fecha_actual=date("d/m/Y");



 $id= $_POST['usuariop'];


          

class PDF extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w)
{
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a)
{
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data)
{
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++)
	{
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		
		$this->Rect($x,$y,$w,$h);

		$this->MultiCell($w,5,$data[$i],0,$a,'true');
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h)
{
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}

function Header()
{
	$num= $_POST['usuariop'];
    $result = mysql_query("SELECT * FROM users where email =  '$num' ");
    $fila = mysql_fetch_array($result);
	
	$this->SetFont('Arial','B',11);
	$this->Text(30,15,utf8_decode('CONSENTIMIENTO ESPECIAL PARA LA OPERACIÓN U OTRO PROCEDIMIENTO'),0,'C', 0);
	$this->SetFont('Arial','B',11);
	$this->Text(70,20,utf8_decode('"ODONTOLÓGICO PARA PACIENTES"'),0,'C', 0);

	$this->SetFont('Arial','B',10);
	$this->Text(20,30,utf8_decode('Fecha '.date('d-m-Y').' '),0,'C', 0);

	$this->SetFont('Arial','B',10);
	$this->Text(20,38,utf8_decode('NOMBRE DEL PACIENTE: '.$_POST['usuarioconsultado'].' '),0,'C', 0);
	$this->SetFont('Arial','B',10);
	$this->Text(20,42,utf8_decode('NOMBRE DEL ODONTÓLOGO: '.$fila['last_name'].' '.$fila['first_name'].' '),0,'C', 0);
	
}

}

	//inicio de session
	session_start();
	$pdf=new PDF('P','mm','A4');
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(20,20,20);
	$pdf->Ln(43);

    if(isset($_POST['optradioPersona'])){
    	if($_POST['optradioPersona'] == "Acompañante"){
    		$pdf->SetFont('Arial','',9);
			$pdf->MultiCell(177,6, 'Yo '.$_POST['txtAcompanante'].' mayor de edad identificado como aparece al pie de mi firma, en pleno uso de mis facultades mentales, libre y conscientemente declaro que:
' ,0,'J');
			$_SESSION['acudiente_seleccionado'] = $_POST['txtAcompanante'];
    	}else{
    		$pdf->SetFont('Arial','',9);
			$pdf->MultiCell(177,6, 'Yo '.$_POST['optradioPersona'].' mayor de edad identificado como aparece al pie de mi firma, en pleno uso de mis facultades mentales, libre y conscientemente declaro que:
' ,0,'J');
			$_SESSION['acudiente_seleccionado'] = $_POST['optradioPersona'];
    	}
    }else{
    	$pdf->SetFont('Arial','',9);
		$pdf->MultiCell(177,6, 'Yo '.$_POST['usuarioconsultado'].' mayor de edad identificado como aparece al pie de mi firma, en pleno uso de mis facultades mentales, libre y conscientemente declaro que:
' ,0,'J');
    }	

	$num= $_POST['usuariop'];
    $result = mysql_query("SELECT * FROM users where email =  '$num' ");
    $fila = mysql_fetch_array($result);

	$pdf->MultiCell(177,5, utf8_decode('1. Por la presente autorizo a '.$fila['last_name'].' '.$fila['first_name'].' para que bajo su responsabilidad asigne a quienes practicaran la siguiente operación o procedimiento:') ,0,'J');
	$pdf->SetFont('Arial','B',10);
	$pdf->MultiCell(177,5, ''.utf8_decode($_POST['motivo']).'' ,0,'B');


    $pdf->SetFont('Arial','',9);
	$pdf->MultiCell(177,5, utf8_decode('y aquellas operaciones o procedimientos odontológicos adicionales que a juicio de estos se requieran sobre la base de la mencionada operación o procedimiento.') ,0,'J');
	$pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('2. He conversado con el Odontólogo asignado y/u otros odontólogos sobre la naturaleza y propósito de la operación o procedimiento, la posibilidad de que puedan surgir o desarrollarse complicaciones, los riesgos previsibles que puedan estar involucrados y los posibles métodos alternativos de tratamiento en los cuales se han consignados con fecha: '.date('d-m-Y').' en la historia clínica cuyo texto declaro que conozco suficientemente.') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('3. Declaro que he sido advertido en el sentido que la práctica de operación o procedimiento compromete una actividad de medio, pero no de resultado.') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('4. Autorizo e indico al Odontólogo previamente nombrado y a sus asociados y asistentes a proveer servicios adicionales que ellos consideren razonables y necesarios incluyendo, aunque no limitados a ellos, la administración y mantenimiento de la anestesia, la administración de sangre y productos sanguíneos y la preparación de servicios que incluyan la patología y la radiología, así como otras pruebas para clínicas que sean necesarias.
') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('5. Cualquier tejido o partes que sean quirúrgicamente resecadas pueden ser retenidas o descartadas, por '.$fila['last_name'].' '.$fila['first_name'].' de acuerdo con su práctica habitual.
') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('6. Se me ha informado acerca de la naturaleza, beneficios y riesgos previsibles del procedimiento u operación y se me han resuelto las dudas e interrogantes que he formulado, y habiendo dado mi consentimiento informado, acuerdo por la presente liberar a '.$fila['last_name'].' '.$fila['first_name'].', sus empleados, agentes y cuerpo odontológico y medico (anestesiólogo), de ulteriores responsabilidades con respecto al permiso para esta operación  o procedimiento odontológico y con respecto a los riesgos, reacciones o resultados desfavorables, inmediatos o tardios de imposible o difícil previsión.') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('7. Asi mismo, autorizo al médico anestesiólogo  a administrar los anestésicos que se consideren necesarios. Reconozco que siempre hay riesgos para la vida y la salud asociados con anestesia y tales riesgos me han sido explicados.') ,0,'J');
    $pdf->Ln(2);
	$pdf->MultiCell(177,5, utf8_decode('Declaro que la información aquí suministrada es auténtica y veraz. Autorizo expresamente a '.$fila['last_name'].' '.$fila['first_name'].' para verificarla a través de los medios que considere convenientes. Igualmente me obligo a actualizar los datos reportados, una vez se produzcan cambios en ellos o que '.$fila['last_name'].' '.$fila['first_name'].' lo requiera. Acepto que he leído la anterior información, así  como los deberes y derechos que como paciente poseo y me acojo a las políticas y normas que en su interior se describen. He leído cuidadosamente este formulario antes de firmarlo y he tenido oportunidad de interrogar a mi odontólogo y anestesiólogo la operación  o procedimiento odontológico, certifico que todos los espacios en blanco han sido completados antes de mi firma y que me encuentro en capacidad de manifestar mi libre consentimiento.') ,0,'J');
	
	
	
	
    
    $pdf->Ln(90);
	
	$pdf->SetFont('Arial','',9);
    $pdf->SetFillColor(255); 
    
	
	
	$pdf->SetXY(20, 250);
    $pdf->Cell(70, 5, '________________________________', 0, 0, 'C', 1);     
    
	
	$pdf->SetXY(145, 250);
    $pdf->Cell(10, 5, '_______________________________________', 0, 0, 'C', 1);

    $pdf->SetXY(145, 265);
    $pdf->Cell(10, 5, '_______________________________________', 0, 0, 'C', 1);
	
	$pdf->SetXY(20, 255);
    $pdf->Cell(70, 5, 'Paciente: '.$_POST['usuarioconsultado'].'', 0, 0, 'C', 1); 

    $pdf->SetXY(20, 260);
    $pdf->Cell(70, 5, 'Documento: '.$_POST['documento'].'', 0, 0, 'C', 1);     
	
	$pdf->SetXY(110, 255);
    $pdf->Cell(80, 5, utf8_decode('Odontólogo: ').$fila['last_name'].' '.$fila['first_name'].' ', 0, 0, 'C', 1);

    $pdf->SetXY(110, 260);
    $pdf->Cell(80, 5, 'Documento: '.$fila['document__number'].'', 0, 0, 'C', 1);

    $pdf->SetXY(110, 270);
    $pdf->Cell(80, 5, utf8_decode('Anestesiólogo:'), 0, 0, 'C', 1);
	
	 
	
	             
    $y      =   130;
    
	$pdf->Ln(40);

$pdf->Output();
?>