<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Nuevo Cliente</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="css/fullcalendar.css" rel="stylesheet">
    <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
    <link href="lib/validation/css/validation.css" rel="stylesheet">
    <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="css/base.css" rel="stylesheet">
    <link href="tools/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src = "http://www.google.com/jsapi" charset="utf-8"></script>
    <script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    </head>
    <body>
    <div class="container-fluid" style="border-style: solid;border-width: 1px;border-color: gray">
        <div class="row">
            <div class="col-md-12">
            </br>
            </br>
            <div class="col-md-4">
            <font size="5"><b>Confirmacion de Registro</b></font>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            <img id="logosuperior" alt="Bootstrap Image Preview" src="http://190.60.211.17/hco/images/logohco.png" height="80px" width="50%">
            </div>
            </div>
             <div class="col-md-12">
             <hr style="border-top: 2px solid #0288d1;">   
             </div>
             <div class="col-md-12">
             <p>Bienvenido <?php echo $nombres; ?> !</p> 
             </br>
             </br>
             <p>Los datos personales registrados tendr&aacute;n privilegios de administrador dentro del sistema de HCO, el cual le permitir&aacute; realizar la creaci&oacute;n de su grupo de trabajo como odont&oacute;logos y asistentes, adem&aacute;s de realizar la parametrizaci&oacute;n inicial de HCO para que sea ajuste de manera particular y detallada a los procesos de su cl&iacute;nica o centro odontol&oacute;gico</p> 
             <p>* Acceda desde nuestra p&aacute;gina www.hco.com.co opci&oacute;n INGRESAR A HCO, seleccionando el perfil de Administrador</p>
             <p>* Recuerda que el nombre de usuario ser&aacute; el correo registrado</p>
             </br>
             </br>
             <p> Confirma tu proceso de registro y la validaci&oacute;n de tu correo electr&oacute;nico mediante el siguente link</p>
             <center><a href="http://190.60.211.17/hco/confirmacion.php">Confirmacion de Cuenta y Registro</a></center> 
             </div>
             
        </div>
        </br>
        </br>
    </div>
    </body>
    </html>        