<?php
require_once 'Queries.php';
require_once '../templatePDF/Budget_template.php';
class Budgets_model extends Queries {

  function all($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);

    $sql = "SELECT dateSave,name,initial,sessions,discount,total, if(changeToPay = 1, 'Aprobado', 'Pendiente') as changeToPay, id FROM  tbl_budgets WHERE idClinic = $idClinic AND idUser = $idUser ORDER BY id DESC";
    return $this->requestData($sql);
  }

  function autorize($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);

    $sql = "SELECT dateSave,name,initial,sessions,discount,total, if(changeToPay = 1, 'Aprobado', 'Pendiente') as changeToPay, id FROM  tbl_budgets WHERE idClinic = $idClinic AND idUser = $idUser AND changeToPay = 1 ORDER BY id DESC";
    return $this->requestData($sql);
  }

  function filter($post){
    extract($post);
    $validate1 = "";
    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
			$validate1 = "AND DATE(dateSave) BETWEEN '$dateInit' AND '$dateFinish'";
		}
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);

    $sql = "SELECT dateSave,name,initial,sessions,discount,total, if(changeToPay = 1, 'Aprobado', 'Pendiente') as changeToPay, id FROM tbl_budgets WHERE idClinic = $idClinic AND idUser = $idUser  $validate1 ORDER BY id";
    return $this->requestData($sql);
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_budgets', $id);
    $idUser = 0;
    $res = [];
    if($pacienteEmail != 1){
        $idUser = $this->getIDUser($pacienteEmail);
    }
    $total = $this->removeFormmat($total);
    $initial = $this->removeFormmat($initial);
    if(!$discount>0){
      $discount = 0;
    }
    $sql = "UPDATE tbl_budgets SET idUser = '$idUser', idProfesional = '$idProfesional', sessions = '$sessions', name = '$name', initial = '$initial', idClinic = $idClinic, total = $total, discount = '$discount', descripcion = '$descripcion', active = 1 WHERE id = '$id'";
    $this->requestCount2($sql);

    $this->addDetails($id, $items, $idClinic);
    $res['id'] = $id;
    return $res;
  }

  function addDetails($id, $items, $idClinic){
    $sql = "DELETE FROM tbl_budgets_details WHERE idBudget = $id";
    $this->requestCount2($sql);

    $json = json_decode($items);
    if(is_array($json)){
      foreach ($json as $item) {
        if($item != null){
          $idSpecialty = $item->idSpecialty;
          $idProcess = $item->idProcess;
          $price = $item->price;
          $idSpecialtyProcess = 0;
          $sql = "SELECT idProcess FROM tbl_specialty_process WHERE idSpecialty = $idSpecialty AND idProcess = $idProcess AND idClinic = $idClinic";
          if($this->requestCount($sql)>0){
              $idSpecialtyProcess = $this->requestData($sql)[0]['id'];
          }
          else{
            $sql = "SELECT id FROM tbl_process WHERE idSpecialty = $idSpecialty AND id = $idProcess AND idClinic = $idClinic";
            $idSpecialtyProcess = $this->requestData($sql)[0]['id'];
          }
          $sql = "INSERT INTO tbl_budgets_details(idSpecialtyProcess, idBudget, price, active) VALUE('$idSpecialtyProcess','$id','$price','1')";
          $this->requestCount2($sql);
        }
      }
    }
  }

  function delete($post){
    extract($post);
    $sql = "DELETE FROM tbl_budgets_details WHERE idBudget = $id";
    $this->requestCount2($sql);
    $sql = "DELETE FROM tbl_budgets WHERE id = $id";
    return $this->requestCount2($sql);
  }

  function item($post){
    extract($post);
    $sql = "SELECT * FROM  tbl_budgets WHERE id = $id";
    $res['item'] = $this->requestData($sql);

    $sql = "SELECT
            	P.idSpecialty,
            	P.id as idProcess,
            	S.name as specialty,
            	P.name as process,
            	BD.price
            FROM
            	tbl_budgets_details as BD
            LEFT JOIN tbl_process as P ON P.id = BD.idSpecialtyProcess
            LEFT JOIN tbl_specialty_process as SP ON SP.idProcess = P.id
            LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
            WHERE
            	idBudget = $id
            GROUP BY P.id
            ORDER BY
            	P.name ASC";
    $res['details'] = $this->requestData($sql);
    return $res;
  }

  function changeToPay($post){
    extract($post);
    $sql = "UPDATE tbl_budgets SET changeToPay = '$changeToPay' WHERE id = '$id'";
    $this->requestCount2($sql);
    return $sql;
  }

  function templatePDF($post){
    extract($post);
    $sql = "SELECT
            	C.imagen as clinicLogo,
            	C.razonsocial as clinicName,
              C.email as clinicEmail,
            	C.nit as clinicNit,
            	C.telefono as clinicPhone,
            	C.direccion as clinicAddress,
            	B.dateSave as budgetDate,
            	B.name as budgetCode,
            	CONCAT(P.first_name, ' ',P.last_name) as patientName,
            	P.document__number as patientCode,
              P.email as email,
              B.sessions,
              B.discount,
              B.descripcion,
            	FORMAT(B.initial, 2) as initial,
              FORMAT(B.total, 2) as budgetTotal
            FROM
            	tbl_budgets as B
            LEFT JOIN clinicas as C ON C.id = B.idClinic
            LEFT JOIN users as P ON P.id = B.idUser
            WHERE
            	B.id = '$idBudget'";
    $res['budget'] = $this->requestData($sql)[0];


    $sql = "SELECT
            	P.name as process,
            	FORMAT(BD.price, 2) as price
            FROM
            	tbl_budgets_details as BD
            INNER JOIN tbl_process as P ON P.id = BD.idSpecialtyProcess
            WHERE
            	BD.idBudget = '$idBudget'";
    $res['budgetDetails'] = $this->requestData($sql);

    $res['budget']['validateDes'] = "";

    $budget = new Budget_template();
    $template = $budget->budgetData($res);
    $this->sendEmail($template, $res['budget']['email'], $res['budget']['clinicEmail'], $res['budget']['clinicPhone']);

    $res['budget']['validateDes'] = '<hr style="display:block;width100%;"><p align="left" style="width100%;display:block;">Observaciones: <span class="ligther">'.$res['budget']['descripcion'].'</span></p>';
    $budget = new Budget_template();
    $template = $budget->budgetData($res);

    return $template;
  }

  function sendEmail($template, $email, $emailClinica, $phoneClinica){
    include_once 'PHPMailer/Mailer.php';
    $mail = new Mailer();
    $subject = "Presupuesto Odontológico";
    return $mail->sendEmail($subject, $template, $email, $emailClinica, $phoneClinica, $subject);
  }
}
