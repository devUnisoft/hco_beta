<?php
require_once 'Queries.php';
class Products_model extends Queries {

  function all($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	P.name AS name,
            	C.name as category,
            	SC.name as subCategory,
            	OT.name AS type,
            	IFNULL(PR.name, 'Sin establecer') AS provider,
            	IFNULL(PC.stock, 'Sin establecer') AS stock,
            	IFNULL(PC.und, 'Sin establecer') AS und,
            	IFNULL(PC.price, 'Sin establecer') AS price,
            	IFNULL(OT.name, 'Sin establecer') AS type,
            	P.id AS idProduct,
            	PC.idProduct AS id
            FROM
            	tbl_products AS P
            LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = P.id AND PC.idClinic = '$idClinic'
            LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
            LEFT JOIN tbl_products_subcategory AS SC ON SC.id = P.idSubCategory
            LEFT JOIN tbl_order_type_request OT ON OT.id = PC.idTypeOrderRequest
            LEFT JOIN tbl_providers as PR ON PR.id = PC.idProvider
            ORDER BY
            	P.name ASC";
    return $this->requestData($sql);
  }

  function select($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	P.name AS name,
            	C.name as category,
            	SC.name as subCategory,
            	OT.name AS type,
            	IFNULL(PR.name, 'Sin establecer') AS provider,
            	IFNULL(PC.stock, 'Sin establecer') AS stock,
            	IFNULL(PC.und, 'Sin establecer') AS und,
            	IFNULL(PC.price, 'Sin establecer') AS price,
            	IFNULL(OT.name, 'Sin establecer') AS type,
            	P.id AS idProduct,
            	PC.idProduct AS id
            FROM
            	tbl_products AS P
            LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = P.id
            LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
            LEFT JOIN tbl_products_subcategory AS SC ON SC.id = P.idSubCategory
            LEFT JOIN tbl_order_type_request OT ON OT.id = PC.idTypeOrderRequest
            LEFT JOIN tbl_providers as PR ON PR.id = PC.idProvider
            WHERE
              PC.idClinic = '$idClinic'
            ORDER BY
            	P.name ASC";
    return $this->requestData($sql);
  }



  function productForCategory($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              P.name AS name,
              C.name as category,
              SC.name as subCategory,
              OT.name AS type,
              IFNULL(PR.name, 'Sin establecer') AS provider,
              IFNULL(PC.stock, 'Sin establecer') AS stock,
              IFNULL(PC.und, 'Sin establecer') AS und,
              IFNULL(FORMAT(PC.price, 2), 'Sin establecer') AS price,
              IFNULL(OT.name, 'Sin establecer') AS type,
              P.id AS idProduct,
              PC.idProduct AS id
              FROM
              	tbl_products AS P
              LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = P.id AND PC.idClinic = '$idClinic'
              LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
              LEFT JOIN tbl_products_subcategory AS SC ON SC.id = P.idSubCategory
              LEFT JOIN tbl_order_type_request OT ON OT.id = PC.idTypeOrderRequest
              LEFT JOIN tbl_providers as PR ON PR.id = PC.idProvider
              ORDER BY
              	P.name ASC";
    return $this->requestData($sql);
  }

  function item($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              P.name AS name,
              P.idCategory,
              P.idSubCategory,
              PC.sanitaryRegistration,
              PC.auditorRecord,
              PC.stock,
              PC.und,
              P.descripcion,
              PC.price,
              PC.idTypeOrderRequest,
              PC.alertStock,
              PC.countProvider,
              PC.idProvider,
              P.id AS idProduct,
              PC.idProduct AS id
              FROM
              	tbl_products AS P
              LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = P.id AND PC.idClinic = '$idClinic'
              LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
              LEFT JOIN tbl_products_subcategory AS SC ON SC.id = P.idSubCategory
              LEFT JOIN tbl_order_type_request OT ON OT.id = PC.idTypeOrderRequest
              LEFT JOIN tbl_providers as PR ON PR.id = PC.idProvider
              WHERE P.id = $id
              ORDER BY
              	P.name ASC";
    return $this->requestData($sql);
  }

  function removeFormmat($number){
    $sig[]='.';
    $sig[]=',';
    $sig[]='$';
    return str_replace($sig,'',$number);
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    if($id == 0){
      $insert = "INSERT INTO tbl_products (idCategory, idSubCategory, name, descripcion, active) VALUES ('$idCategory', '$idSubCategory', '$name', '$descripcion', '1')";
      $this->requestCount($insert);

      $sql = "SELECT id FROM tbl_products ORDER BY id DESC LIMIT 1";
      $id = $this->requestData($sql)[0]['id'];
    }

    $sql = "SELECT idProduct FROM tbl_products_clinic WHERE idProduct = '$id'";
    if(!$this->requestCount($sql)>0){
      $insert = "INSERT INTO tbl_products_clinic (idProduct, idClinic) VALUES ('$id', '$idClinic')";
      $this->requestCount($insert);
    }

    $id = $this->requestData($sql)[0]['idProduct'];

    $price = $this->removeFormmat($price);

    $sql = "SELECT stock FROM tbl_products_clinic WHERE idProduct = '$id' AND idClinic = $idClinic";
    $data = $this->requestData($sql);
    $stock2 = $data[0]['stock'];

    if($stock2>$stock){
      $countTotal =$stock2-$stock;
      $this->changelog($id, $countTotal, 'Restado', $idClinic);
    }
    else if($stock>$stock2){
      $countTotal =$stock-$stock2;
      $this->changelog($id, $countTotal, 'Agregado', $idClinic);
    }
    $validateRequest = 0;
    if(!isset($idTypeOrderRequest)){
      $validateRequest = 1;
    }
    else{
      $validateRequest = $idTypeOrderRequest;
    }


    $validateProvider = 0;
    if(!isset($idProvider)){
      $validateProvider = 1;
    }
    else{
      $validateProvider = $idProvider;
    }
    $sql = "UPDATE
              tbl_products_clinic
            SET
              price                = '$price',
              stock                = '$stock',
              und                  = '$und',
              sanitaryRegistration = '$sanitaryRegistration',
              auditorRecord        = '$auditorRecord',
              idTypeOrderRequest   = '$validateRequest',
              idProvider           = '$validateProvider',
              alertStock           = '$alertStock',
              countProvider        = '$countProvider',
              idClinic             = '$idClinic',
              automatic             = '$idTypeOrderRequest'
            WHERE
              idProduct = '$id' AND idClinic = $idClinic";
    $this->requestCount2($sql);
    return $id;
  }

  function delete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_products_clinic WHERE idProduct = $id AND idClinic = $idClinic";
    $this->requestCount2($sql);
    return true;
  }

  function categories($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT * FROM tbl_products_category ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function updateCategory($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_products_category', $id);
    $sql = "UPDATE tbl_products_category SET name  = '$name' WHERE id = $id";
    $this->requestCount2($sql);
    return $id;
  }

  function subcategories($post){
    extract($post);
      $idClinic = $this->getIDClinic($clinicaName);
    if($idCategory>0){
      $sql = "SELECT * FROM tbl_products_subcategory WHERE idCategory = $idCategory ORDER BY name ASC";
      return $this->requestData($sql);
    }
    else{
      $sql = "SELECT * FROM tbl_products_subcategory ORDER BY name ASC";
      return $this->requestData($sql);
    }
  }

  function updateSubCategory($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_products_subcategory', $id);

    $sql = "UPDATE
              tbl_products_subcategory
            SET
              name  = '$name',
              idCategory = '$idCategory'
            WHERE
              id = '$id'";
    $this->requestCount2($sql);
    return $id;
  }

  function updateproductAdd($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT stock FROM tbl_products_clinic WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
    $data = $this->requestData($sql);
    $stock = $data[0]['stock'];
    $stock = $stock+$count;
    $sql = "UPDATE tbl_products_clinic SET stock  = '$stock'  WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
    $this->changelog($idProduct, $count, 'Agregado', $idClinic);
    return $this->requestCount2($sql);
  }

  function updateproductMinus($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT stock FROM tbl_products_clinic WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
    $data = $this->requestData($sql);
    $stock = $data[0]['stock'];
    $stock = $stock-$count;
    $sql = "UPDATE tbl_products_clinic SET stock  = '$stock'  WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
    $this->changelog($idProduct, $count, 'Restado', $idClinic);
    return $this->requestCount2($sql);
  }

  function validateName($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT name FROM tbl_products WHERE name like '%$name%'";
    return $this->requestCount($sql);
  }

  function changelog($idProduct, $count, $type, $idClinic){
    $date = date('Y-m-d');
    
    $sql="INSERT INTO tbl_products_log(idProduct, count, type, active, date, idClinic) VALUE('$idProduct','$count','$type', '1', '$date', '$idClinic' );";
    return $this->requestCount2($sql);
  }
}
