<?php
require_once 'Queries.php';
require_once 'Budgets_model.php';
class Cups_model extends Queries {

  function process($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ETC.id,
              IFNULL(ETCC.name, ETC.name) AS name,
              IFNULL(ETCC.code, ETC.code) AS code,
              IFNULL(ETCC.price, 0) AS price
            FROM
              tbl_evolutions_type_process AS ETC
            LEFT JOIN tbl_evolutions_type_process_clinic as ETCC ON ETC.id = ETCC.idEvolutionTypeProcess AND ETCC.idClinic = $idClinic
            WHERE ETC.idClinicCreated = 0 OR ETC.idClinicCreated = $idClinic
            ORDER BY ETC.name ASC";
    return $this->requestData($sql);
  }

  function processItem($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ETC.id,
              IFNULL(ETCC.name, ETC.name) AS name,
              IFNULL(ETCC.code, ETC.code) AS code,
              IFNULL(ETCC.price, 0) AS price
            FROM
              tbl_evolutions_type_process AS ETC
            LEFT JOIN tbl_evolutions_type_process_clinic as ETCC ON ETC.id = ETCC.idEvolutionTypeProcess AND ETCC.idClinic = $idClinic
            WHERE ETC.id = $id";
    $res['item'] = $this->requestData($sql);
    return $res;
  }

  function processUpdate($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    if($id == 0){
      $id = $this->getID('tbl_evolutions_type_process', $id);
      $sql = "UPDATE tbl_evolutions_type_process SET name = '$name', code = '$code', idClinicCreated = '$idClinic' WHERE id = $id";
      $this->requestCount2($sql);
    }

    $sql = "SELECT * FROM tbl_evolutions_type_process_clinic WHERE idEvolutionTypeProcess = $id AND idClinic = '$idClinic'";
    if(!$this->requestCount($sql)>0){
      $idc = $this->getID('tbl_evolutions_type_process_clinic', 0);
      $sql = "UPDATE tbl_evolutions_type_process_clinic SET name = '$name', code = '$code', idEvolutionTypeProcess = '$id', price = '$price', idClinic = '$idClinic' WHERE id = $idc";
      $this->requestCount2($sql);
    }
    else{
      $sql = "UPDATE tbl_evolutions_type_process_clinic SET name = '$name', code = '$code', price = '$price' WHERE idEvolutionTypeProcess = $id AND idClinic = '$idClinic'";
      $this->requestCount2($sql);
    }
  }

  function processDelete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_evolutions_type_process WHERE idClinicCreated = $idClinic AND id = '$id'";
    $this->requestCount2($sql);
    $sql = "DELETE FROM tbl_evolutions_type_process_clinic WHERE idClinic = $idClinic AND idEvolutionTypeProcess = '$id'";
    return $this->requestCount2($sql);
  }

  function diagnostics($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ED.id,
              IFNULL(EDC.name, ED.name) AS name
            FROM tbl_evolutions_diagnosis AS ED
            LEFT JOIN tbl_evolutions_diagnosis_clinic as EDC ON ED.id = EDC.idEvolutionDiagnosis AND EDC.idClinic = $idClinic
            WHERE ED.idClinicCreated = 0 OR ED.idClinicCreated = $idClinic
            ORDER BY ED.name ASC";
    return $this->requestData($sql);
  }

  function diagnosticsItem($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ED.id,
              IFNULL(EDC.name, ED.name) AS name
            FROM tbl_evolutions_diagnosis AS ED
            LEFT JOIN tbl_evolutions_diagnosis_clinic as EDC ON ED.id = EDC.idEvolutionDiagnosis AND EDC.idClinic = $idClinic
            WHERE ED.id = $id";
    $res['item'] = $this->requestData($sql);
    return $res;
  }

  function diagnosticsUpdate($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    if($id == 0){
      $id = $this->getID('tbl_evolutions_diagnosis', $id);
      $sql = "UPDATE tbl_evolutions_diagnosis SET name = '$name', idClinicCreated = '$idClinic' WHERE id = $id";
      $this->requestCount2($sql);
    }

    $sql = "SELECT * FROM tbl_evolutions_diagnosis_clinic WHERE idEvolutionDiagnosis = $id AND idClinic = $idClinic";
    if(!$this->requestCount($sql)>0){
      $ids = $this->getID('tbl_evolutions_diagnosis_clinic', 0);
      $sql = "UPDATE tbl_evolutions_diagnosis_clinic SET name = '$name', idEvolutionDiagnosis = $id, idClinic = $idClinic WHERE id = $ids";
      return $this->requestCount2($sql);
    }
    else{
      $sql = "UPDATE tbl_evolutions_diagnosis_clinic SET name = '$name' WHERE idEvolutionDiagnosis = $id AND idClinic = $idClinic";
      return $this->requestCount2($sql);
    }
  }

  function diagnosticsDelete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_evolutions_diagnosis WHERE idClinicCreated = $idClinic AND id = '$id'";
    $this->requestCount2($sql);
    $sql = "DELETE FROM tbl_evolutions_diagnosis_clinic WHERE idClinic = $idClinic AND idEvolutionDiagnosis = '$id'";
    return $this->requestCount2($sql);
  }

  function originDisease($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ED.id,
              IFNULL(EDC.name, ED.name) AS name
            FROM tbl_evolutions_disease AS ED
            LEFT JOIN tbl_evolutions_disease_clinic as EDC ON ED.id = EDC.idEvolutionDisease AND EDC.idClinic = $idClinic
            WHERE ED.idClinicCreated = 0 OR ED.idClinicCreated = $idClinic
            ORDER BY ED.name ASC";
    return $this->requestData($sql);
  }

  function originDiseaseItem($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ED.id,
              IFNULL(EDC.name, ED.name) AS name
            FROM tbl_evolutions_disease AS ED
            LEFT JOIN tbl_evolutions_disease_clinic as EDC ON ED.id = EDC.idEvolutionDisease AND EDC.idClinic = $idClinic
            WHERE ED.id = $id";
    $res['item'] = $this->requestData($sql);
    return $res;
  }

  function originDiseaseUpdate($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    if($id == 0){
      $id = $this->getID('tbl_evolutions_disease', $id);
      $sql = "UPDATE tbl_evolutions_disease SET name = '$name', idClinicCreated = '$idClinic' WHERE id = $id";
      $this->requestCount2($sql);
    }

    $sql = "SELECT * FROM tbl_evolutions_disease_clinic WHERE idEvolutionDisease = $id AND idClinic = $idClinic";
    if(!$this->requestCount($sql)>0){
      $ids = $this->getID('tbl_evolutions_disease_clinic', 0);
      $sql = "UPDATE tbl_evolutions_disease_clinic SET name = '$name', idEvolutionDisease = $id, idClinic = $idClinic WHERE id = $ids";
      return $this->requestCount2($sql);
    }
    else{
      $sql = "UPDATE tbl_evolutions_disease_clinic SET name = '$name' WHERE idEvolutionDisease = $id AND idClinic = $idClinic";
      return $this->requestCount2($sql);
    }
  }

  function originDiseaseDelete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_evolutions_disease WHERE idClinicCreated = $idClinic AND id = '$id'";
    $this->requestCount2($sql);
    $sql = "DELETE FROM tbl_evolutions_disease_clinic WHERE idClinic = $idClinic AND idEvolutionDisease = '$id'";
    return $this->requestCount2($sql);
  }

  function typeConsult($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ETC.id,
              IFNULL(ETCC.name, ETC.name) AS name
            FROM tbl_evolutions_type_consult AS ETC
            LEFT JOIN tbl_evolutions_type_consult_clinic as ETCC ON ETC.id = ETCC.idEvolutionTypeConsult AND ETCC.idClinic = $idClinic
            WHERE ETC.idClinicCreated = 0 OR ETC.idClinicCreated = $idClinic
            ORDER BY ETC.name ASC";
    return $this->requestData($sql);
  }

  function typeConsultItem($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              ETC.id,
              IFNULL(ETCC.name, ETC.name) AS name
            FROM tbl_evolutions_type_consult AS ETC
            LEFT JOIN tbl_evolutions_type_consult_clinic as ETCC ON ETC.id = ETCC.idEvolutionTypeConsult AND ETCC.idClinic = $idClinic
            WHERE ETC.id = $id";
    $res['item'] = $this->requestData($sql);
    return $res;
  }

  function typeConsultUpdate($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    if($id == 0){
      $id = $this->getID('tbl_evolutions_type_consult', $id);
      $sql = "UPDATE tbl_evolutions_type_consult SET name = '$name', idClinicCreated = '$idClinic' WHERE id = $id";
      $this->requestCount2($sql);
    }
    $sql = "SELECT * FROM tbl_evolutions_type_consult_clinic WHERE idEvolutionTypeConsult = $id AND idClinic = $idClinic";
    if(!$this->requestCount($sql)>0){
      $ids = $this->getID('tbl_evolutions_type_consult_clinic', 0);
      $sql = "UPDATE tbl_evolutions_type_consult_clinic SET name = '$name', idEvolutionTypeConsult = $id, idClinic = $idClinic WHERE id = $ids";
      return $this->requestCount2($sql);
    }
    else{
      $sql = "UPDATE tbl_evolutions_type_consult_clinic SET name = '$name' WHERE idEvolutionTypeConsult = $id AND idClinic = $idClinic";
      return $this->requestCount2($sql);
    }
  }

  function typeConsultDelete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_evolutions_type_consult WHERE idClinicCreated = $idClinic AND id = '$id'";
    $this->requestCount2($sql);
    $sql = "DELETE FROM tbl_evolutions_type_consult_clinic WHERE idClinic = $idClinic AND idEvolutionTypeConsult = '$id'";
    return $this->requestCount2($sql);
  }
}
