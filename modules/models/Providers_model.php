<?php
require_once 'Queries.php';
class Providers_model extends Queries {

  function all($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	P.name AS name,
            	P.code AS code,
            	P.email AS email,
            	P.phone AS phone,
            	P.personContact AS personContact,
            	P.id AS id
            FROM
            	tbl_providers AS P
            WHERE idClinic = '$idClinic'
            ORDER BY name ASC";
    return $this->requestData($sql);
  }
  function providerSearch($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              P.name AS name,
              P.code AS code,
              P.email AS email,
              P.phone AS phone,
              P.personContact AS personContact,
              P.id AS id
            FROM
              tbl_providers AS P
            WHERE
              name LIKE '%$search%'
              AND idClinic = $idClinic
            ORDER BY name ASC";
    if($this->requestCount($sql)>0){
        return $this->requestData($sql);
    }
    else{
      if($search == ""){
        return $this->all($post);
      }
      else{
        return null;
      }

    }
  }
  function forProduct($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	PR.name AS name,
            	PP.idProduct AS idProduct,
            	PP.idProvider AS idProvider,
            	PR.id AS id
            FROM
            	tbl_providers_products AS PP
            INNER JOIN tbl_providers AS PR ON PR.id = PP.idProvider
            WHERE PP.idProduct = $idProduct AND PR.idClinic = $idClinic
            GROUP BY PR.id
            ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function item($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT * FROM tbl_providers WHERE id = $id ORDER BY name ASC";
    $res['item'] = $this->requestData($sql);

    $sql = "SELECT P.name, P.id FROM tbl_providers_products as PP INNER JOIN tbl_products as P ON P.id = PP.idProduct WHERE PP.idprovider = $id AND PP.idClinic = $idClinic ORDER BY P.name ASC";
    $res['products'] = $this->requestData($sql);
    return $res;
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_providers', $id);

    $sql = "DELETE FROM tbl_providers_products WHERE idprovider = $id AND idClinic = $idClinic";
    $this->requestCount2($sql);

    $json = json_decode($providerProducts);
    foreach ($json as $item) {
      $idProduct = $item->id;
      $sql="INSERT INTO tbl_providers_products(idprovider, idProduct, active, idClinic) VALUE('$id','$idProduct','1','$idClinic');";
      $this->requestCount2($sql);
    }

    $sql = "UPDATE
              tbl_providers
            SET
              name          = '$name',
              code          = '$code',
              email         = '$email',
              phone         = '$phone',
              personContact = '$personContact',
              idClinic = '$idClinic'
            WHERE
              id = '$id'";
    $this->requestCount2($sql);
    return $id;
  }

  function delete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "DELETE FROM tbl_providers WHERE id = $id";
    return $this->requestCount2($sql);
  }
}
