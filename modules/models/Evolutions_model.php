<?php
require_once 'Queries.php';
class Evolutions_model extends Queries {

  function all($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              E.id,
            	E.dateSave,
              IFNULL(ETS.name, ' ') as type,
              ET.name as idTypeEvolution,
              E.dateSterilization,
              ETP.name as idEvolutionTypeProcess,
              ED.name AS idEvolutionDiagnostic,
              E.price,
              EDE.name as idOriginDisease,
              CONCAT(P.first_name, ' ', P.last_name) as idProfesional,
              E.observations
            FROM
            	tbl_evolutions AS E
            LEFT JOIN tbl_evolutions_type AS ET ON ET.id = E.idTypeEvolution
            LEFT JOIN tbl_evolutions_type_state AS ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_details_process AS EDP ON EDP.idEvolution = E.id
            LEFT JOIN tbl_evolutions_type_process AS ETP ON ETP.id = EDP.idEvolutionTypeProcess
            LEFT JOIN tbl_evolutions_details_diagnosis AS EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis AS ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN tbl_evolutions_disease AS EDE ON EDE.id = E.idOriginDisease
            LEFT JOIN users AS P ON P.id = E.idProfesional
            WHERE E.idClinic = $idClinic AND E.idUser = $idUser
            GROUP BY E.id
            ORDER BY E.id DESC ";
    return $this->requestData($sql);
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_evolutions', $id);
    $idUser = 0;
    if($pacienteEmail != 1){
        $idUser = $this->getIDUser($pacienteEmail);
    }
    $price = $this->removeFormmat($price);
    if(!$discount>0){
      $discount = 0;
    }
    $date = date('Y-m-d');
    $test = 0;

    if($idTypeEvolution == 1){
      if(isset($aditionalCost)){
        if($aditionalCost > 0){
          $test = 1;
        }
        else{
          $price = 0;
        }
      }
      else{
        $price = 0;
      }
    }
    else{
      if($price > 0){
        $test = 1;
      }
    }

    $sql = "UPDATE tbl_evolutions SET type = '$type', idUser = '$idUser', idClinic = $idClinic, idProfesional = $idProfesional, idTypeEvolution = '$idTypeEvolution', idBudget = '$idBudget', aditionalCost = '$test', idTypeConsult = '$idTypeConsult', dateSterilization = '$dateSterilization', idOriginDisease = $idOriginDisease, observations = '$observations', date = '$date', discount = '$discount', price = '$price' WHERE id = '$id'";
    $this->requestCount2($sql);

    $this->addDetails('tbl_evolutions_details_process', 'idEvolutionTypeProcess', $id, $process, $idClinic);

    $this->addDetails('tbl_evolutions_details_diagnosis', 'idEvolutionDiagnostic', $id, $diagnosis, $idClinic);

    return $sql;
  }

  function addDetails($table, $field, $id, $items, $idClinic){
    $sql = "DELETE FROM $table WHERE idEvolution = $id";
    $this->requestCount2($sql);

    if(is_array($items)){
      foreach ($items as $item) {
        if($item != null){
          $idItem = $item;
          $sql = "INSERT INTO $table(idEvolution, $field) VALUE('$id','$idItem')";
          $this->requestCount2($sql);
        }
      }
    }
  }

  function item($post){
    extract($post);
    $sql = "SELECT * FROM  tbl_evolutions WHERE id = $id";
    $res['item'] = $this->requestData($sql);

    $sql = "SELECT
            	ETP.id
            FROM
            	tbl_evolutions_details_process as EDP
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = EDP.idEvolutionTypeProcess
            WHERE
            	EDP.idEvolution = $id
            ORDER BY
            	ETP.name ASC";
    $data = $this->requestData($sql);
    $datas = Array();
    foreach ($data as $item) {
      $datas[] = $item['id'];
    }
    $res['process'] = $datas;

    $sql = "SELECT
              ETP.id
            FROM
              tbl_evolutions_details_diagnosis as EDP
            LEFT JOIN tbl_evolutions_diagnosis as ETP ON ETP.id = EDP.idEvolutionDiagnostic
            WHERE
              EDP.idEvolution = $id
            ORDER BY
              ETP.name ASC";

    $data = $this->requestData($sql);
    $datas2 = Array();
    foreach ($data as $item) {
      $datas2[] = $item['id'];
    }
    $res['diagnosis'] = $datas2;
    return $res;
  }

  function delete($post){
    extract($post);
    $sql = "DELETE FROM tbl_evolutions WHERE id = $id";
    return $this->requestData($sql);
  }

  function typeConsult($post){
    extract($post);
    $sql = "SELECT * FROM tbl_evolutions_type_consult ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function process($post){
    extract($post);
    $sql = "SELECT * FROM tbl_evolutions_type_process ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function diagnostics($post){
    extract($post);
    $sql = "SELECT * FROM tbl_evolutions_diagnosis ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function originDisease($post){
    extract($post);
    $sql = "SELECT * FROM tbl_evolutions_disease ORDER BY name ASC";
    return $this->requestData($sql);
  }

  function priceCups($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $total = 0;
    $priceCups = explode(",", $priceCups);
    foreach ($priceCups as $item) {
      $idItem = $item;
      $sql = "SELECT
                IFNULL(ETCC.price, 0) AS price
              FROM
                tbl_evolutions_type_process AS ETC
              LEFT JOIN tbl_evolutions_type_process_clinic as ETCC ON ETC.id = ETCC.idEvolutionTypeProcess AND ETCC.idClinic = $idClinic
              WHERE ETC.id = $idItem";
      $price = $this->requestData($sql)[0]['price'];
      if($price>0){
        $total = $price+$total;
      }
    }
    return $total;//var_dump($priceCups);
  }
}
