<?php
require_once '../db_connect.php';
class Queries extends DB_CONNECT {

  function requestData($sql){
    $query = mysql_query($sql);
    if(!$query) {
        die('Consulta no válida: ' . mysql_error());
    }
    else{
      if($this->requestCount($sql)>0){
        $data = [];
        while ($fila = mysql_fetch_assoc($query)) {
          $data[] = $fila;
        }
        return $data;
      }
      else{
        return null;
      }
    }
  }

  function requestCount($sql){
    $query = mysql_query($sql);
    if(!$query) {
        die('Consulta no válida: ' . mysql_error());
    }
    else{
      if(mysql_affected_rows()>0){
        return mysql_affected_rows();
      }
      else{
        return mysql_num_rows($query);
      }
    }
  }

  function requestCount2($sql){
    $query = mysql_query($sql);
  }

  function getID($table, $id){
    if($id == 0){
      $sql="INSERT INTO $table(active) VALUE('1');";
      $query = $this->requestCount($sql);
      $sql = "SELECT max(id) as id FROM $table";
      $data = $this->requestData($sql);
      $id = $data[0]['id'];
    }
    return $id;
  }

  function getIDClinic($clinicaName){
    $sql = "SELECT id FROM clinicas WHERE razonsocial LIKE '%$clinicaName%'";
    if($this->requestCount($sql)>0){
      $data = $this->requestData($sql);
      $id = $data[0]['id'];
      return $id;
    }
  }

  function getIDUser($pacienteEmail){
    $sql = "SELECT id FROM users WHERE email = '$pacienteEmail'";
    $data = $this->requestData($sql);
    $id = $data[0]['id'];
    return $id;
  }

  function removeFormmat($number){
    $sig[]='.';
    $sig[]=',';
    $sig[]='$';
    return str_replace($sig,'',$number);
  }

  function validateNameTable($table, $name){
      $sql = "SELECT name FROM $table WHERE name like '%$name%'";
      return $this->requestCount($sql);
  }

}
