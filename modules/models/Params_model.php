<?php
require_once 'Queries.php';
class Params_model extends Queries {
  function all($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              P.id AS orden,
              IF(
            			(SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            			S.name,
            			(SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
            	) as specialty,
              IF(
            			(SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            			P.name,
            			(SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
            	) as process,
            	IF(
            			(SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            			P.priceDefault,
            			(SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
            	) as price,
              IF(
            			(SELECT SU.id FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            			P.id,
            			(SELECT SU.id FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
            	) as id,
              IF(
            			(SELECT SU.id FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            			-1,
            			(SELECT SU.id FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
            	) as validate,
              IF(
            			(SELECT SU.id FROM tbl_process AS SU WHERE SU.id = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            		  'por defecto',
            			'Si'
            	) as validateCreate,
              IF(
            			(SELECT SU.id FROM tbl_process AS SU WHERE SU.id = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
            		  'Sin Editar',
            			'Editado'
            	) as validateCreate2
            FROM
            	tbl_process AS P
            LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
            LEFT JOIN tbl_specialty_process as SP ON SP.idClinic = $idClinic
            GROUP BY P.id
            ORDER BY
            P.id ASC";
    return $this->requestData($sql);
  }

  function item($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    if($validate == -1){
      $sql = "SELECT
                IF(
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    S.name,
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as specialty,
                IF(
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    P.name,
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as process,
              	IF(
              			(SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
              			P.priceDefault,
              			(SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
              	) as price,
                IF(
              			(SELECT SU.idSpecialty FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
              			P.idSpecialty,
              			(SELECT SU.idSpecialty FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
              	) as idSpecialty,
                P.id as idProcess,
              	SP.id
              FROM
              	tbl_process AS P
              LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
              LEFT JOIN tbl_specialty_process as SP ON SP.idClinic = $idClinic
              WHERE P.id = $id";
              return $this->requestData($sql);
    }
    else{
      $sql = "SELECT
                IF(
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    S.name,
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_specialty as SS ON SS.id = SU.idSpecialty WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as specialty,
                IF(
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    P.name,
                    (SELECT SS.name FROM tbl_specialty_process AS SU LEFT JOIN tbl_process as SS ON SS.id = SU.idProcess WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as process,
                IF(
                    (SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    P.priceDefault,
                    (SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as price,
                IF(
                    (SELECT SU.idSpecialty FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1) is null,
                    P.idSpecialty,
                    (SELECT SU.idSpecialty FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic LIMIT 1)
                ) as idSpecialty,
                P.id as idProcess,
                SP.id
              FROM
                tbl_specialty_process as SP
              INNER JOIN tbl_process AS P ON SP.idProcess = P.id
              INNER JOIN tbl_specialty as S ON S.id = SP.idSpecialty
              WHERE SP.id = $id";
              return $this->requestData($sql);
    }

  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $price = $this->removeFormmat($price);

    if($id == 0 && $validate == -1){
      $id = $this->getID('tbl_specialty_process', 0);
      $sql = "UPDATE tbl_specialty_process SET idSpecialty = '$idSpecialty', idProcess = '$id', price = '$price', idClinic = '$idClinic' WHERE id = $id";
      $this->requestCount2($sql);
    }

    if($validate == -1){
      $idc = $this->getID('tbl_specialty_process', 0);
      $sql = "UPDATE tbl_specialty_process SET idSpecialty = '$idSpecialty', idProcess = '$id', price = '$price', idClinic = '$idClinic' WHERE id = $idc";
      $this->requestCount2($sql);
    }
    else{
      $sql = "UPDATE tbl_specialty_process SET idSpecialty = '$idSpecialty', price = '$price', idProcess = '$idProcess' WHERE id = $id AND idClinic = '$idClinic'";
      $this->requestCount2($sql);
    }
    return $sql;
  }

  function delete($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    if($validate == -1){
      $sql = "DELETE FROM tbl_process WHERE id = $id AND idClinic = '$idClinic'";
      $this->requestCount2($sql);
    }
    else{
      $sql = "DELETE FROM tbl_specialty_process WHERE id = $id AND idClinic = '$idClinic'";
      $this->requestCount2($sql);
    }
    return true;
  }

  function specialties($post){
    extract($post);
    $sql = "";
    if(isset($clinic)){
        $idClinic = $this->getIDClinic($clinicaName);
        $sql = "SELECT
                	S.name,
                	S.id
                FROM
                	tbl_process AS P
                LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
                LEFT JOIN tbl_specialty_process as SP ON SP.idClinic = $idClinic
                GROUP BY S.id
                ORDER BY
                	S.name ASC";
    }
    else{
        $sql = "SELECT id, name FROM tbl_specialty GROUP BY name";
    }
    return $this->requestData($sql);
  }

  function updateSpecialty($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_specialty', $id);
    $sql = "UPDATE tbl_specialty SET name = '$name', idClinic = '$idClinic' WHERE id = $id";
    $this->requestCount2($sql);
    return $id;
  }

  function process($post){
    extract($post);
    $sql = "";
    if($idSpecialty>0){
      if(isset($clinic)){
          $idClinic = $this->getIDClinic($clinicaName);
          $sql = "SELECT
                  	P.id,
                    P.name
                  FROM
                  	tbl_process AS P
                  LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
                  LEFT JOIN tbl_specialty_process as SP ON SP.idClinic = $idClinic
                  WHERE P.idSpecialty = $idSpecialty
                  GROUP BY P.id
                  ORDER BY
                  	S.name ASC";
      }
      else{
          $sql = "SELECT * FROM tbl_process WHERE idSpecialty = '$idSpecialty' ORDER BY name ASC";
      }
    }
    else{
      $sql = "SELECT * FROM tbl_process ORDER BY name ASC";
    }
    return $this->requestData($sql);
  }

  function itemProcess($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              IF(
                  (SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic) is null,
                  P.priceDefault,
                  (SELECT SU.price FROM tbl_specialty_process AS SU WHERE SU.idProcess = P.id AND SU.idClinic = $idClinic)
              ) as price,
              P.id
            FROM
              tbl_process AS P
            LEFT JOIN tbl_specialty as S ON S.id = P.idSpecialty
            LEFT JOIN tbl_specialty_process as SP ON SP.idClinic = $idClinic
            WHERE P.id = $idProcess
            GROUP BY P.id
            ORDER BY
              P.name ASC";
    return $this->requestData($sql);
  }

  function updateProcess($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_process', $id);
    $sql = "UPDATE tbl_process SET idSpecialty = '$idSpecialty', name = '$name', idClinic = '$idClinic' WHERE id = '$id'";
    $this->requestCount2($sql);
    return $id;
  }

  function validateName($post){
    extract($post);
    return $this->validateNameTable($type, $name);
  }
}
