<?php
require_once 'Queries.php';
require_once 'Products_model.php';
class Orders_model extends Queries {

  public $Products_model;
  function __construct() {
    $this->Products_model = new Products_model();
  }

  function all($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	O.uniquecode AS uniquecode,
            	O.date AS date,
            	sum(OD.count) AS count,
            	if(O.recived = 1,'Si','No') AS recived,
            	O.id AS id
            FROM
            tbl_orders AS O
            INNER JOIN tbl_orders_details AS OD ON OD.idOrder = O.id
            WHERE
            	OD.count > 0
            AND O.idClinic = $idClinic
            GROUP BY
            	O.id
            ORDER BY
            	O.id DESC";

    if($this->requestCount($sql)>0){
      $data = $this->requestData($sql);

      $res = [];
      foreach ($data as $row) {
          $idOrder = $row['id'];
          $uniquecode = $row['uniquecode'];
          $date = $row['date'];
          $count = $row['count'];
          $recived = $row['recived'];

          $subSQL = "SELECT * FROM tbl_orders_details WHERE recived = 1 AND idOrder = $idOrder";
          if($this->requestCount($subSQL)>0){
            if($recived == 'No'){
              $recived = 'Parcial';
            }
          }
          $item['uniquecode'] = $uniquecode;
          $item['date'] = $date;
          $item['count'] = $count;
          $item['recived'] = $recived;
          $item['id'] = $idOrder;

            $res[] = $item;
        }
        return $res;
      }
      else{
          return null;
      }
  }

  function filter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";
    if($dateInit != null){
			$dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
			$dateInit = date ( 'Y-m-j' , $dateInit );
			$dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
			$dateFinish = date ( 'Y-m-j' , $dateFinish );
			$validate1 = "AND date BETWEEN '$dateInit' AND '$dateFinish'";
		}

    $sql = "SELECT
            	O.uniquecode AS uniquecode,
            	O.date AS date,
            	sum(OD.count) AS count,
            	if(O.recived = 1,'Si','No') AS recived,
            	O.id AS id
            FROM
            tbl_orders AS O
            INNER JOIN tbl_orders_details AS OD ON OD.idOrder = O.id
            WHERE
            	OD.count > 0
            AND O.idClinic = $idClinic $validate1
            GROUP BY
            	O.id
            ORDER BY
            	O.id DESC";

        if($this->requestCount($sql)>0){

            $data = $this->requestData($sql);
            $res = [];
            foreach ($data as $row) {
                $idOrder = $row['id'];
                $uniquecode = $row['uniquecode'];
                $date = $row['date'];
                $count = $row['count'];
                $recived = $row['recived'];

                $subSQL = "SELECT * FROM tbl_orders_details WHERE recived = 1 AND id = $idOrder";
                if($this->requestCount($subSQL)>0){
                  if($recived == 'No'){
                    $recived = 'Parcial';
                  }
                }
                $item['uniquecode'] = $uniquecode;
                $item['date'] = $date;
                $item['count'] = $count;
                $item['recived'] = $recived;
                $item['id'] = $idOrder;

                $res[] = $item;
            }

            return $res;
      }
      else{
        return null;
      }
  }

  function item($post){
    extract($post);
    $sql = "SELECT * FROM tbl_orders WHERE id = $id";
    return $this->requestData($sql);
  }

  function delete($post){
    extract($post);
    $sql = "DELETE FROM tbl_orders WHERE id = $id";
    return $this->requestCount2($sql);
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('tbl_orders', $id);

    $uniquecode = $this->validateCode($uniquecode);

    $sql = "UPDATE tbl_orders SET uniquecode  = '$uniquecode', date  = '$date', idClinic = $idClinic WHERE id = '$id'";
    $this->requestCount2($sql);

    $this->addDetails($id, $items, $idClinic);

    $this->getProviderOrder($id, $uniquecode, $clinicaName);

    return $id;
  }

  function validateCode($uniquecode){
    if($uniquecode == "Sin generar"){
      $sql = "SELECT max(uniquecode) as id FROM tbl_orders";
      $data = $this->requestData($sql);
      $uniquecode = $data[0]['id'];
      $uniquecode = $uniquecode+1;
    }
    return $uniquecode;
  }

  function addDetails($id, $items, $idClinic){

    $sql = "DELETE FROM tbl_orders_details WHERE idOrder = $id";
    $this->requestCount2($sql);

    $json = json_decode($items);
		if(is_array($json)){
      foreach ($json as $item) {
        if($item != null){
          $idProvider = $item->idProvider;
          $idProduct = $item->idProduct;
          $count = $item->count;
          $sql = "INSERT INTO tbl_orders_details(idOrder, idProvider, idProduct, count, active, idClinic) VALUE('$id','$idProvider','$idProduct','$count','1', '$idClinic')";
          $this->requestCount2($sql);
        }
      }
    }
  }

  function getProduct($idProduct, $idClinic){
    $res = [];
    $sql = "SELECT P.name, PC.und FROM tbl_products as P INNER JOIN tbl_products_clinic as PC ON PC.idProduct = P.id WHERE PC.idProduct = $idProduct AND PC.idClinic = '$idClinic'";
    $data = $this->requestData($sql);
    $res['productName'] = $data[0]['name'];
    $res['und'] = $data[0]['und'];
    return $res;
  }

  function getProvider($idprovider){
    $res = [];
    $sql = "SELECT name, email FROM tbl_providers WHERE id  = $idprovider";
    $data = $this->requestData($sql);
    $res['providerName'] = $data[0]['name'];
    $res['providerEmail'] = $data[0]['email'];
    return $res;
  }

  function templateProvider($idOrder, $idprovider, $uniquecode, $clinicaName){
    $idClinic = $this->getIDClinic($clinicaName);

    $template = '<br><b style="font-weight: bold;font-size: 16px;color:#444;">Solicitante: '.$clinicaName.'</b>';
    $template .= '<br><b style="font-weight: bold;font-size: 16px;color:#444;">Proveedor: '.$this->getProvider($idprovider)['providerName'].'</b>';
    $template .= '<br><br><br><b style="font-weight: bold;font-size: 16px;color:#444;">Solicitud:</b>';

    $sql = "SELECT
                	D.*,
                	P.name as product,
                	PC.und
                FROM
                	tbl_orders_details as D
                INNER JOIN tbl_products as P ON P.id = D.idProduct
                INNER JOIN tbl_products_clinic as PC ON PC.idProduct = P.id
                WHERE
                	D.idOrder = $idOrder
                AND D.idProvider = '$idprovider'";
    $details = $this->requestData($sql);

    $template .= "<br><table>";
    $template .= "<tr><td style='width: 300px;color: #337ab7;font-weight: bold;font-size: 16px;'>Producto</td><td style='color: #337ab7;font-weight: bold;font-size: 16px;width: 150px;'>Unid. De Medida</td><td style='color: #337ab7;font-weight: bold;font-size: 16px;width: 100px;'>Cantidad</td></tr>";

    foreach ($details as $detail) {
      $idProduct    = $detail['idProduct'];
      $count        = $detail['count'];
      $product      = $this->getProduct($idProduct, $idClinic);
      $productName  = $detail['product'];
      $productUnd   =  $detail['und'];

      $template .= "<tr><td style='width: 300px;'>$productName</td><td>$productUnd</td><td style='text-align: center;'>$count</td></tr>";
    }

    $template .= "</table>";


    $email = $this->getProvider($idprovider)['providerEmail'];
    $subject = 'ORDEN DE PEDIDO No.'. $uniquecode;

    $sql = "SELECT telefono, email FROM clinicas WHERE razonsocial LIKE '%$clinicaName%'";
    $phoneClinica = $this->requestData($sql)[0]['telefono'];
    $emailClinica = $this->requestData($sql)[0]['email'];

    return $this->sendEmail($subject, $template, $email, $emailClinica, $phoneClinica);
  }

  function getProviderOrder($idOrder, $uniquecode, $clinicaName){
    $idprovider = 0;
    $providers = [];
    $sql = "SELECT * FROM tbl_orders_details WHERE idOrder = $idOrder";
    foreach ($this->requestData($sql) as $value) {
      $this->templateProvider($idOrder, $value['idProvider'], $uniquecode, $clinicaName);
    }
  }

  function sendEmail($subject, $text, $email, $emailClinica, $phoneClinica){
    include_once 'PHPMailer/Mailer.php';
    $mail = new Mailer();
    return $mail->sendEmail($subject, $text, $email, $emailClinica, $phoneClinica, "Solicitud de productos HCO");
  }

  function recived($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "UPDATE tbl_orders SET recived  = '1' WHERE id = '$id'";
    $this->requestCount2($sql);
    $sql = "SELECT * FROM  tbl_orders_details WHERE idOrder = $id AND recived != 1";
    $data = $this->requestData($sql);
    foreach ($data as $item) {
      $idDetail = $item['id'];
      $idProduct = $item['idProduct'];
      $count = $item['count'];
      $stock = 0;
      $sql = "SELECT stock FROM tbl_products_clinic WHERE idProduct = $idProduct AND idClinic = $idClinic";
      $data = $this->requestData($sql);
      foreach ($data as $value) { $stock = $value['stock'];}
      $stock = $stock+$count;
      $this->Products_model->changelog($idProduct, $count, 'Agregado', $idClinic);
      if($stock<0){
        $stock = 0;
      }

      $sql = "SELECT automatic FROM tbl_products_clinic WHERE idProduct = $idProduct AND automatic = 1 AND idClinic = $idClinic";
      if($this->requestCount2($sql)>0){
        $sql = "UPDATE tbl_products_clinic SET automatic = null WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
        $this->requestCount2($sql);
      }

      $sql = "UPDATE tbl_products_clinic SET stock = '$stock' WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
      $this->requestCount2($sql);

      $sql = "UPDATE tbl_orders_details SET recived  = '1' WHERE id = '$idDetail'";
      $this->requestCount2($sql);

    }
    $res['Msg'] = "Se ha actualizado correctamente.";
    return $res;
  }

  function RecivedParcial($post){
    extract($post);

    $sql = "SELECT P.name as product, PC.und, OD.count, if(OD.recived = 1,'Si','No') AS recived, OD.id
            FROM tbl_orders_details as OD
            INNER JOIN tbl_products as P ON P.id = OD.idProduct
            INNER JOIN tbl_products_clinic as PC ON P.id = PC.idProduct
            WHERE OD.idOrder = $id";

    return $this->requestData($sql);
  }

  function addRecived($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "UPDATE tbl_orders_details SET recived  = '1' WHERE id = '$id'";
    $this->requestCount2($sql);
    $sql = "SELECT * FROM  tbl_orders_details WHERE id = '$id'";
    $data = $this->requestData($sql);
    $idOrder = 0;
    foreach ($data as $item) {
      $idProduct = $item['idProduct'];
      $count = $item['count'];
      $idOrder = $item['idOrder'];
      $stock = 0;
      $sql = "SELECT stock FROM tbl_products_clinic WHERE idProduct = $idProduct AND idClinic = $idClinic";
      $data = $this->requestData($sql);
      foreach ($data as $value) { $stock = $value['stock'];}
      $stock = $stock+$count;
      $this->Products_model->changelog($idProduct, $count, 'Agregado', $idClinic);
      if($stock<0){
        $stock = 0;
      }

      $sql = "SELECT automatic FROM tbl_products_clinic WHERE idProduct = $idProduct AND automatic = 1 AND idClinic = $idClinic";
      if($this->requestCount2($sql)>0){
        $sql = "UPDATE tbl_products_clinic SET automatic = null WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
        $this->requestCount2($sql);
      }

      $sql = "UPDATE tbl_products_clinic SET stock = '$stock' WHERE idProduct = '$idProduct' AND idClinic = $idClinic";
      $this->requestCount2($sql);
    }

    $sql = "SELECT * FROM  tbl_orders_details WHERE idOrder = '$idOrder'";
    $datas = $this->requestData($sql);
    $details = $this->requestCount($sql);
    $detailsA = 0;
    foreach ($datas as $item) {
      $id = $item['id'];
      $sql = "SELECT * FROM  tbl_orders_details WHERE id = '$id'";
      $data = $this->requestData($sql);
      foreach ($data as $item2) {
        $recived = $item2['recived'];
        if($recived == 1){
          $detailsA++;
        }
      }
    }

    if($details == $detailsA){
      $sql = "UPDATE tbl_orders SET recived  = '1' WHERE id = '$idOrder'";
      $this->requestCount2($sql);
    }

    $res['Msg'] = "Se ha actualizado correctamente.";
    return $res;
  }
}
