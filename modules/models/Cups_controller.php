<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Cups_model.php';
class Cups_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Cups_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($process)){
      $this->process();
    }
    else if(isset($processItem)){
      $this->processItem();
    }
    else if(isset($processUpdate)){
      $this->processUpdate();
    }
    else if(isset($processDelete)){
      $this->processDelete();
    }
    else if(isset($diagnostics)){
      $this->diagnostics();
    }
    else if(isset($diagnosticsItem)){
      $this->diagnosticsItem();
    }
    else if(isset($diagnosticsUpdate)){
      $this->diagnosticsUpdate();
    }
    else if(isset($diagnosticsDelete)){
      $this->diagnosticsDelete();
    }
    else if(isset($originDisease)){
      $this->originDisease();
    }
    else if(isset($originDiseaseItem)){
      $this->originDiseaseItem();
    }
    else if(isset($originDiseaseUpdate)){
      $this->originDiseaseUpdate();
    }
    else if(isset($originDiseaseDelete)){
      $this->originDiseaseDelete();
    }
    else if(isset($typeConsult)){
      $this->typeConsult();
    }
    else if(isset($typeConsultItem)){
      $this->typeConsultItem();
    }
    else if(isset($typeConsultUpdate)){
      $this->typeConsultUpdate();
    }
    else if(isset($typeConsultDelete)){
      $this->typeConsultDelete();
    }
  }

  function process(){
    $res = $this->model->process($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function processItem(){
    $res = $this->model->processItem($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function processUpdate(){
    $res = $this->model->processUpdate($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function diagnostics(){
    $res = $this->model->diagnostics($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function diagnosticsItem(){
    $res = $this->model->diagnosticsItem($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function diagnosticsUpdate(){
    $res = $this->model->diagnosticsUpdate($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function originDisease(){
    $data = $this->helper->gather($_POST);
    extract($data);
    $res = $this->model->originDisease($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function originDiseaseItem(){
    $res = $this->model->originDiseaseItem($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function originDiseaseUpdate(){
    $res = $this->model->originDiseaseUpdate($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function typeConsult(){
    $res = $this->model->typeConsult($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function typeConsultItem(){
    $res = $this->model->typeConsultItem($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function typeConsultUpdate(){
    $res = $this->model->typeConsultUpdate($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Cups_controller();
?>
