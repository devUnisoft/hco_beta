<?php
session_start();
require_once 'Queries.php';
class Pays_model extends Queries {

  function cards($post){
    extract($post);
    $sql = "SELECT * FROM  tbl_methods_cards WHERE active  = 1";
    return $this->requestData($sql);
  }

  function methodsPays($post){
    extract($post);
    $sql = "SELECT * FROM  tbl_methods_pay WHERE active  = 1";
    return $this->requestData($sql);
  }

  function filter($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";
    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
      $validate1 = "AND DATE(B.dateSave) BETWEEN '$dateInit' AND '$dateFinish'";
    }
    $sql = "SELECT
              B.dateSave,
              B.name,
              B.total,
              B.discount,
              B.sessions,
              B.initial,
              ((B.total-B.initial)/B.sessions) as valorCuota,
              CONCAT(P.first_name, ' ',P.last_name) as profesional,
              IFNULL(SUM(BP.rode), 0) as totalAbonos,
              IFNULL((B.total - ((if(SUM(BP.rode) > 0, SUM(BP.rode), 0)) + (if(B.payInitial = 1, initial, 0)))), 0) as saldo,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.date >= CURDATE() AND PU.date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY) AND PU.idBudget = B.id), 0) as saldomora,
              B.id
            FROM
              tbl_budgets as B
            LEFT JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            WHERE
              B.idClinic = $idClinic AND B.idUser = $idUser AND B.changeToPay = 1 $validate1
            GROUP BY B.id
            ORDER BY B.id DESC";
    $data = $this->requestData($sql);
    return $this-> requestBudgets($data);
  }

  function all($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              B.dateSave,
              B.name,
              B.total,
              B.discount,
              B.sessions,
              B.initial,
              B.payInitial,
              ((B.total-B.initial)/B.sessions) as valorCuota,
              CONCAT(P.first_name, ' ',P.last_name) as profesional,
              IFNULL(SUM(BP.rode), 0) as totalAbonos,
              IFNULL((B.total - ((if(SUM(BP.rode) > 0, SUM(BP.rode), 0)) + (if(B.payInitial = 1, initial, 0)))), 0) as saldo,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.date >= CURDATE() AND PU.date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY) AND PU.idBudget = B.id), 0) as saldomora,
              B.id
            FROM
            	tbl_budgets as B
            LEFT JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            WHERE
            	B.idClinic = $idClinic AND B.idUser = $idUser AND B.changeToPay = 1
            GROUP BY B.id
            ORDER BY B.id DESC";
    $data = $this->requestData($sql);
    return $this-> requestBudgets($data);
  }

  function requestBudgets($data){
    $res;
    $totalSaldoC = 0;
    if(count($data)>0){
      foreach ($data as $item) {
        $id = $item['id'];
        $date = $item['dateSave'];
        $name = $item['name'];
        $total = $item['total'];
        $discount = $item['discount'];
        $sessions = $item['sessions'];
        $initial = $item['initial'];
        $payInitial = $item['payInitial'];

        $valorCuota = intval($item['valorCuota']);
        $profesional = $item['profesional'];
        $totalAbonos = $item['totalAbonos'];
        $saldo = $item['saldo'];
        $saldomora = $this->validateMora($id, $valorCuota);

        $item['id'] = $id;
        $item['date'] = $date;
        $item['name'] = $name;
        $item['total'] = $total;
        $item['discount'] = $discount;
        $item['sessions'] = $sessions;
        $item['initial'] = $initial;
        $item['payInitial'] = $payInitial;
        $item['valorCuota'] = $valorCuota;
        $item['profesional'] = $profesional;
        $item['totalAbonos'] = $totalAbonos;
        $item['saldo'] = $saldo;
        $item['saldomora'] = $saldomora;

        $res['all'][] = $item;
        $totalSaldoC = $saldo+$totalSaldoC;
      }
    }
    $res['totalSaldo'] = $totalSaldoC;
    return $res;
  }

  function evolutions($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
              E.id,
            	E.dateSave as date,
              ET.name as idTypeEvolution,
              E.dateSterilization,
              ETP.name as idEvolutionTypeProcess,
              ED.name AS idEvolutionDiagnostic,
              EDE.name as idOriginDisease,
              CONCAT(P.first_name, ' ', P.last_name) as idProfesional,
              E.price,
              E.discount,
              (SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id) as Abonos,
            	IFNULL((E.price - (if((SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id) > 0, (SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id), 0))), 0) as saldo,
            	E.observations
            FROM
            	tbl_evolutions AS E
            LEFT JOIN tbl_evolutions_type AS ET ON ET.id = E.idTypeEvolution
            LEFT JOIN tbl_evolutions_details_process AS EDP ON EDP.idEvolution = E.id
            LEFT JOIN tbl_evolutions_type_process AS ETP ON ETP.id = EDP.idEvolutionTypeProcess
            LEFT JOIN tbl_evolutions_details_diagnosis AS EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis AS ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN tbl_evolutions_disease AS EDE ON EDE.id = E.idOriginDisease
            LEFT JOIN tbl_evolutions_pays AS EDDP ON EDDP.idEvolution = E.id
            LEFT JOIN users AS P ON P.id = E.idProfesional
            WHERE E.idClinic = $idClinic AND E.idUser = $idUser AND E.price > 0
            GROUP BY E.id
            ORDER BY E.id DESC";
    $data = $this->requestData($sql);
    return $this-> requestEvolutions($data);
  }

  function filterEvolutions($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";
    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
      $validate1 = "AND E.date BETWEEN '$dateInit' AND '$dateFinish'";
    }
    $sql = "SELECT
              E.id,
            	E.dateSave as date,,
              ET.name as idTypeEvolution,
              E.dateSterilization,
              ETP.name as idEvolutionTypeProcess,
              ED.name AS idEvolutionDiagnostic,
              EDE.name as idOriginDisease,
              CONCAT(P.first_name, ' ', P.last_name) as idProfesional,
              E.price,
              E.discount,
              (SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id) as Abonos,
            	IFNULL((E.price - (if((SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id) > 0, (SELECT IFNULL(SUM(EDDP.rode), 0) FROM tbl_evolutions_pays AS EDDP WHERE EDDP.idEvolution = E.id), 0))), 0) as saldo,
            	E.observations
            FROM
            	tbl_evolutions AS E
            LEFT JOIN tbl_evolutions_type AS ET ON ET.id = E.idTypeEvolution
            LEFT JOIN tbl_evolutions_details_process AS EDP ON EDP.idEvolution = E.id
            LEFT JOIN tbl_evolutions_type_process AS ETP ON ETP.id = EDP.idEvolutionTypeProcess
            LEFT JOIN tbl_evolutions_details_diagnosis AS EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis AS ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN tbl_evolutions_disease AS EDE ON EDE.id = E.idOriginDisease
            LEFT JOIN tbl_evolutions_pays AS EDDP ON EDDP.idEvolution = E.id
            LEFT JOIN users AS P ON P.id = E.idProfesional
            WHERE E.idClinic = $idClinic AND E.idUser = $idUser AND E.price > 0 $validate1
            GROUP BY E.id
            ORDER BY E.id DESC";
    $data = $this->requestData($sql);
    return $this-> requestEvolutions($data);
  }

  function requestEvolutions($data){
    $res;
    $totalSaldoC = 0;
    if(count($data)>0){
      foreach ($data as $item) {
        $id = $item['id'];
        $date = $item['date'];
        $idTypeEvolution = $item['idTypeEvolution'];
        $idEvolutionTypeProcess = $item['idEvolutionTypeProcess'];
        $idEvolutionDiagnostic = $item['idEvolutionDiagnostic'];
        $idOriginDisease = $item['idOriginDisease'];
        $idProfesional = $item['idProfesional'];
        $price = $item['price'];
        $discount = $item['discount'];
        $Abonos = $item['Abonos'];
        $saldo = $item['saldo'];
        $observations = $item['observations'];

        $item['id'] = $id;
        $item['date'] = $date;
        $item['idTypeEvolution'] = $idTypeEvolution;
        $item['idEvolutionTypeProcess'] = $idEvolutionTypeProcess;
        $item['idEvolutionDiagnostic'] = $idEvolutionDiagnostic;
        $item['idOriginDisease'] = $idOriginDisease;
        $item['idProfesional'] = $idProfesional;
        $item['price'] = $price;
        $item['discount'] = $discount;
        $item['Abonos'] = $Abonos;
        $item['saldo'] = $saldo;
        $item['observations'] = $observations;
        $totalSaldoC = $saldo+$totalSaldoC;
        $res['all'][] = $item;
      }
    }

    $res['totalSaldo'] = $totalSaldoC;
    return $res;
  }

  function validateMora($id, $cuota){
    $date = "";
    $res;
    $sql = "SELECT date FROM tbl_budgets_pays WHERE idBudget = $id  AND date is not null ORDER BY id DESC LIMIT 1";
    if($this->requestCount($sql)>0){
      $date = $this->requestData($sql)[0]['date'];
      $res['date1'] = $date;
    }
    else{
      $sql = "SELECT dateSave FROM tbl_budgets WHERE id = $id ORDER BY id DESC LIMIT 1";
      $date = $this->requestData($sql)[0]['dateSave'];
      $res['date2'] = $date;
    }

    $dateInit = strtotime ( '+0 day' , strtotime ( $date ) ) ;
    $dateInit = date ( 'Y-m-d' , $dateInit );

    $res['$dateInit'] = $dateInit;

    $date = date('Y-m-d');
    $dateFinish = strtotime ( '+0 day' , strtotime ( $date ) ) ;
    $dateFinish = date ( 'Y-m-d' , $dateFinish );

    $res['$dateFinish'] = $dateFinish;

    $dias = $this->dias_transcurridos($dateInit, $dateFinish)+1;

    $res['$dias'] = $dias;
    if($dias > 31){
      return $cuota;
    }
    else{
      return "Sin mora";
    }
  }

  public function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias = abs($dias); $dias = floor($dias);
		return $dias;
	}

  // public function dias_transcurridos($date1,$date2)
	// {
  //   $diff = abs(strtotime($date2) - strtotime($date1));
  //   $years = floor($diff / (365*60*60*24));
  //   $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
  //   $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	// 	return $days;
	// }

  function update($post){
    extract($post);
    if($type == 1){
      $this->updatePayBudgets($post);
    }
    else{
      $this->updatePayEvolutions($post);
    }
    return $this->sendReceipt($post);
  }

  function updatePayBudgets($post){
    extract($post);
    $id = $this->getID('tbl_budgets_pays', 0);
    $rode = $this->removeFormmat($rode);
    $idStatePay = 1;
    $date = date('Y-m-d');
    $sql = "UPDATE tbl_budgets_pays SET idReceptor = '$idReceptor', cheque = '$cheque', idCard = '$idCard', formPay = '$formPay', document = '$document',  code = '$code', dateBank = '$dateBank', codeBank = '$codeBank',  accountBank = '$accountBank', idBudget = '$idItem', idMethodPay = '$idMethodPay', rode = '$rode', idStatePay = '$idStatePay', observations = '$observations', active = 1, date = '$date' WHERE id = '$id'";
    $this->requestCount2($sql);

    if(isset($payInitial)){
      if($payInitial == 1){
        $sql = "UPDATE tbl_budgets_pays SET payInitial2 = '$payInitial' WHERE id = '$id'";
        $this->requestCount2($sql);

        $sql = "UPDATE tbl_budgets SET payInitial = '$payInitial', initial = '$rode'  WHERE id = '$idItem'";
        $this->requestCount2($sql);
      }
    }
  }

  function updatePayEvolutions($post){
    extract($post);
    $id = $this->getID('tbl_evolutions_pays', 0);
    $rode = $this->removeFormmat($rode);
    $idStatePay = 1;
    $date = date('Y-m-d');
    $sql = "UPDATE tbl_evolutions_pays SET idReceptor = '$idReceptor', cheque = '$cheque', idCard = '$idCard', formPay = '$formPay', document = '$document',  code = '$code', dateBank = '$dateBank', codeBank = '$codeBank',  accountBank = '$accountBank',idEvolution = '$idItem', idMethodPay = '$idMethodPay', rode = '$rode', idStatePay = '$idStatePay', observations = '$observations', active = 1, date = '$date' WHERE id = '$id'";
    $this->requestCount2($sql);
  }

  function delete($post){
    extract($post);
    if($type == 1){
      $sql = "DELETE FROM tbl_budgets_pays WHERE id = $id";
      return $this->requestCount2($sql);
    }
    else{
      $sql = "DELETE FROM tbl_evolutions_pays WHERE id = $id";
      return $this->requestCount2($sql);
    }
  }

  function item($post){
    extract($post);
    if($type == 1){
      $sql = "SELECT
              	BP.id,
                BP.date,
                if(BP.payInitial2 is null, 'Si', 'No') as payInitial,
                MP.name as methodName,
                BP.rode,
                if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                if(BP.accountBank is null, ' ', BP.accountBank) as accountBank,
                if(BP.dateBank is null, ' ', BP.dateBank) as dateBank,
                if(BP.cheque is null, ' ', BP.cheque) as cheque,
                if(BP.idCard is null, ' ', if(MPC.name is null, ' ', MPC.name)) as idCard,
                if(BP.formPay is null, ' ', BP.formPay) as formPay,
                if(BP.document is null, ' ', BP.document) as document,
                if(BP.code is null, ' ', BP.code) as code,
                if(BP.observations is null, ' ', BP.observations) as observations
              FROM
              	tbl_budgets_pays as BP
              LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
              LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
              WHERE
              	BP.idBudget = $idItem";
      return $this->requestData($sql);
    }
    else{
      $sql = "SELECT
              	BP.id,
                BP.date,
                'No' as payInitial,
                MP.name as methodName,
                BP.rode,
                if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                if(BP.accountBank is null, ' ', BP.accountBank) as accountBank,
                if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                if(BP.dateBank is null, ' ', BP.dateBank) as dateBank,
                if(BP.cheque is null, ' ', BP.cheque) as cheque,
                if(BP.idCard is null, ' ', if(MPC.name is null, ' ', MPC.name)) as idCard,
                if(BP.formPay is null, ' ', BP.formPay) as formPay,
                if(BP.document is null, ' ', BP.document) as document,
                if(BP.code is null, ' ', BP.code) as code,
                if(BP.observations is null, ' ', BP.observations) as observations
              FROM
              	tbl_evolutions_pays as BP
              LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
              LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
              WHERE
              	BP.idEvolution = $idItem";
      return $this->requestData($sql);
    }
  }

  function getInfo($idUser){
    $sql = "SELECT
            	C.imagen as clinicLogo,
            	C.razonsocial as clinicName,
              C.email as clinicEmail,
            	C.nit as clinicNit,
            	C.telefono as clinicPhone,
            	C.direccion as clinicAddress,
            	CONCAT(P.first_name, ' ',P.last_name) as patientName,
            	P.document__number as patientCode,
              P.email as email
            FROM
            	users as P
            LEFT JOIN clinicas as C ON C.razonsocial = P.clinica
            WHERE
              P.id = $idUser";
    return $this->requestData($sql)[0];
  }

  function getBudgets($data){
    if(count($data['all'])>0){
      $budgetTotal = 0;
      $budgetAbono = 0;
      $budgetSaldo = 0;
      $sql;
      foreach ($data['all'] as $row) {
        $idBudget = $row['id'];
        $total    = $row['total'];
        $budgetTotal = $total+$budgetTotal;
        $sql = "SELECT
                	BP.id,
                  BP.date,
                  if(BP.payInitial2 is null, 'Si', 'No') as payInitial,
                  MP.name as methodName,
                  BP.rode,
                  if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                  if(BP.accountBank is null, ' ', BP.accountBank) as accountBank,
                  if(BP.dateBank is null, ' ', BP.dateBank) as dateBank,
                  if(BP.cheque is null, ' ', BP.cheque) as cheque,
                  if(BP.idCard is null, ' ', MPC.name) as idCard,
                  if(BP.formPay is null, ' ', BP.formPay) as formPay,
                  if(BP.document is null, ' ', BP.document) as document,
                  if(BP.code is null, ' ', BP.code) as code,
                  if(BP.observations is null, ' ', BP.observations) as observations
                FROM
                	tbl_budgets_pays as BP
                LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
                LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
                WHERE
                  BP.idBudget = $idBudget";
            if(count($this->requestData($sql))>0){

              foreach($this->requestData($sql) as $rows) {
                  $abono = $rows['rode'];
                  $budgetAbono = $abono+$budgetAbono;
              }
              $res[$idBudget]['pays'] = $this->requestData($sql);
            }
      }
      //item
      $res['budgets'] = $data['all'];

      $res['total'] = $budgetTotal;
      $res['abono'] = $budgetAbono;
      $res['saldo'] = $budgetTotal - $budgetAbono;
      return $res;
    }
    else{
      return 0;
    }
  }

  function getEvolutions($data){
    if(count($data['all'])>0){
      $budgetTotal = 0;
      $budgetAbono = 0;
      $budgetSaldo = 0;
      foreach ($data['all'] as $row) {
        $idBudget = $row['id'];
        $total    = $row['price'];
        $budgetTotal = $total+$budgetTotal;
        $sql = "SELECT
                	BP.id,
                  BP.date,
                  'No' as payInitial,
                  MP.name as methodName,
                  BP.rode,
                  if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                  if(BP.accountBank is null, ' ', BP.accountBank) as accountBank,
                  if(BP.codeBank is null, ' ', BP.codeBank) as codeBank,
                  if(BP.dateBank is null, ' ', BP.dateBank) as dateBank,
                  if(BP.cheque is null, ' ', BP.cheque) as cheque,
                  if(BP.idCard is null, ' ', MPC.name) as idCard,
                  if(BP.formPay is null, ' ', BP.formPay) as formPay,
                  if(BP.document is null, ' ', BP.document) as document,
                  if(BP.code is null, ' ', BP.code) as code,
                  if(BP.observations is null, ' ', BP.observations) as observations
                FROM
                	tbl_evolutions_pays as BP
                LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
                LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
                WHERE
                  BP.idEvolution = $idBudget";
          if(count($this->requestData($sql))>0){
            foreach($this->requestData($sql) as $rows) {
                $abono = $rows['rode'];
                $budgetAbono = $abono+$budgetAbono;
            }
            $res[$idBudget]['pays'] = $this->requestData($sql);
          }
      }
      $res['evolutions'] = $data['all'];
      $res['total'] = $budgetTotal;
      $res['abono'] = $budgetAbono;
      $res['saldo'] = $budgetTotal - $budgetAbono;
      return $res;
    }
    else{
      return 0;
    }
  }

  function getBudget($idItem){
    $sql = "SELECT
              B.dateSave,
              B.name,
              B.total,
              B.discount,
              B.sessions,
              B.initial,
              ((B.total-B.initial)/B.sessions) as valorCuota,
              CONCAT(P.first_name, ' ',P.last_name) as profesional,
              IFNULL(SUM(BP.rode), 0) as totalAbonos,
              IFNULL((B.total - ((if(SUM(BP.rode) > 0, SUM(BP.rode), 0)) + (if(B.payInitial = 1, initial, 0)))), 0) as saldo,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.date >= CURDATE() AND PU.date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY) AND PU.idBudget = B.id), 0) as saldomora,
              B.id
            FROM
            	tbl_budgets as B
            LEFT JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            WHERE
            	B.id = $idItem
            GROUP BY B.id
            ORDER BY BP.id DESC";
    $data['all'] = $this->requestData($sql);
    return $this->getBudgets($data);
  }

  function getEvolution($idItem){
    $sql = "SELECT
              E.id,
              E.date,
              ET.name as idTypeEvolution,
              E.dateSterilization,
              ETP.name as idEvolutionTypeProcess,
              ED.name AS idEvolutionDiagnostic,
              EDE.name as idOriginDisease,
              CONCAT(P.first_name, ' ', P.last_name) as idProfesional,
              E.price,
              E.discount,
              IFNULL(SUM(EDDP.rode), 0) as Abonos,
              IFNULL((E.price - (if(SUM(EDDP.rode) > 0, SUM(EDDP.rode), 0))), 0) as saldo,
              E.observations
            FROM
              tbl_evolutions AS E
            LEFT JOIN tbl_evolutions_type AS ET ON ET.id = E.idTypeEvolution
            LEFT JOIN tbl_evolutions_details_process AS EDP ON EDP.idEvolution = E.id
            LEFT JOIN tbl_evolutions_type_process AS ETP ON ETP.id = EDP.idEvolutionTypeProcess
            LEFT JOIN tbl_evolutions_details_diagnosis AS EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis AS ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN tbl_evolutions_disease AS EDE ON EDE.id = E.idOriginDisease
            LEFT JOIN tbl_evolutions_pays AS EDDP ON EDDP.idEvolution = E.id
            LEFT JOIN users AS P ON P.id = E.idProfesional
            WHERE E.id = $idItem
            GROUP BY E.id
            ORDER BY E.id DESC";
    $data['all'] = $this->requestData($sql);
    return $this->getEvolutions($data);
  }

  function sendReceipt($post){
    extract($post);
    $data;
    $idUser = $this->getIDUser($pacienteEmail);

    $info = $this->getInfo($idUser);

    if($type == 1){
      $data = $this->getBudget($idItem);
    }
    else{
      $data = $this->getEvolution($idItem);
    }

    $sql = "SELECT name FROM  tbl_methods_pay WHERE id = $idMethodPay";

    $data['pays']['methodName'] = $this->requestData($sql)[0]['name'];

    $data['pays']['rode'] = $rode;

    require_once '../templatePDF/Pay_receipt_template.php';
    $pay = new Pay_receipt_template();
    $template = $pay->payData($info, $data, $type);
    return $template;
  }

  // $dateInit = strtotime ( '+0 day' , strtotime ( $date ) ) ;
  // $dateInit = date ( 'Y-m-d' , $dateInit );
  //
  // $res['$dateInit'] = $dateInit;
  //
  // $date = date('Y-m-d');
  // $dateFinish = strtotime ( '+0 day' , strtotime ( $date ) ) ;
  // $dateFinish = date ( 'Y-m-d' , $dateFinish );
  //
  // $res['$dateFinish'] = $dateFinish;
  //
  // $dias = $this->dias_transcurridos($dateInit, $dateFinish)+1;

  function budgetsReport($idUser){
    $sql = "SELECT
            	B.id as id,
            	(SELECT COUNT(DISTINCT ZS.id) FROM tbl_budgets_pays as ZS WHERE ZS.idBudget = B.id) as pagos,
            	if(
            		(B.sessions - (SELECT COUNT(DISTINCT ZS.id) FROM tbl_budgets_pays as ZS WHERE ZS.idBudget = B.id))< 0,
            		'Pendiente de paz y salvo',
            		(B.sessions - (SELECT COUNT(DISTINCT ZS.id) FROM tbl_budgets_pays as ZS WHERE ZS.idBudget = B.id))
            	) as faltantes,
              DATE_ADD((SELECT BPZ.date FROM tbl_budgets_pays as BPZ WHERE BPZ.idBudget = B.id ORDER BY BPZ.date DESC LIMIT 1), INTERVAL 30 DAY) as proximo,
            	B.name as procedimiento,
            	MP.name as metodopago,
            	B.sessions,
              B.initial,
              B.total as tas,
            	FORMAT(((B.total-B.initial)/B.sessions), 2) as valorCuota,
            	BP.date as fecha,
            	FORMAT(BP.rode, 2) as valorpagado,
            	FORMAT(IF((((B.total-B.initial)/B.sessions)-BP.rode)>0,((B.total-B.initial)/B.sessions)-BP.rode,0), 2) as saldocuota,
            	FORMAT(B.total,2) as total,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0)),2) as saldo,
            	(SELECT SUM(Z.total) FROM tbl_budgets as Z WHERE Z.idUser = '$idUser') as acomulado
            FROM
            	tbl_budgets_pays as BP
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_budgets as B ON B.id = BP.idBudget
            WHERE B.idUser = '$idUser'
            GROUP BY BP.id
            ORDER BY B.id ASC";
    $data = $this->requestData($sql);

    $idActual = 0;
    $template = '';
    $templateDetail = '';
    $saldoTotal = 0;
    $count = 0;
    $count2 = 0;
    $final = count($data);
    $rest = 0;
    if(count($data)>0){
    foreach ($data as $row) {
      $id = $row['id'];
      $saldoTotal = $row['acomulado'];
      $sessions = $row['sessions'];
      $pagos = $row['pagos'];
      $count2++;

      $templateSaldo = '';

      if($row['saldo']>0){
        if($row['faltantes']>0){

          $cuotaActual = ($row['tas']-$row['initial'])/$row['faltantes'];

          $cuotaActual = number_format((float)$cuotaActual, 2, '.', ',');

          $templateSaldo = '<tr>
                              <td><b>Fecha de proximo pago:</b></td>
                              <td>'.$row['proximo'].'</td>
                            <tr>
                            <tr>
                              <td><b>Pago minimo:</b></td>
                              <td>'.$cuotaActual.'</td>
                            <tr>';
        }
        else{
          $templateSaldo = '<tr>
                              <td><b>Fecha de proximo pago:</b></td>
                              <td>'.$row['proximo'].'</td>
                            <tr>
                            <tr>
                              <td><b>Pago minimo:</b></td>
                              <td>'.$row['saldo'].'</td>
                            <tr>';
        }
      }


      $templateDetail .= '<tr>
                            <td>'.$row['procedimiento'].'</td>
                            <td> Cuota '.$count2.'</td>
                            <td>'.$row['metodopago'].'</td>
                            <td>'.$row['valorCuota'].'</td>
                            <td>'.$row['fecha'].'</td>
                            <td>'.$row['valorpagado'].'</td>
                            <td>'.$row['saldocuota'].'</td>
                          </tr>';

      if($final == $count+1){
        $template .= '<table>
                        <thead>
                          <tr>
                            <th align="left">Procedimiento</th>
                            <th align="left">Descripción</th>
                            <th align="left">Forma de pago</th>
                            <th align="left">Valor cuota</th>
                            <th align="left">Fecha</th>
                            <th align="left">Valor pagado</th>
                            <th align="left">Saldo cuota</th>
                          </tr>
                        </thead>
                        <tbody>'.$templateDetail.'</tbody>
                      </table>
                      <table align="left" style="width:300px;">
                        <tr>
                          <td><b>Valor procedimiento:</b></td>
                          <td>'.$row['total'].'</td>
                        <tr>
                        <tr>
                          <td><b>Valor abono:</b></td>
                          <td>'.$row['abonos'].'</td>
                        <tr>
                        <tr>
                          <td><b>Saldo:</b></td>
                          <td>'.$row['saldo'].'</td>
                        <tr>
                        <tr>
                          <td><b>Cuotas restantes:</b></td>
                          <td>'.$row['faltantes'].'</td>
                        <tr>
                        '.$templateSaldo.'
                      </table><hr>';
        $templateDetail = "";
        $count2 = 0;
      }
      else{
        if($data[$count+1]['id'] > $id){
          $template .= '<table>
                          <thead>
                            <tr>
                              <th align="left">Procedimiento</th>
                              <th align="left">Descripción</th>
                              <th align="left">Forma de pago</th>
                              <th align="left">Valor cuota</th>
                              <th align="left">Fecha</th>
                              <th align="left">Valor pagado</th>
                              <th align="left">Saldo cuota</th>
                            </tr>
                          </thead>
                          <tbody>'.$templateDetail.'</tbody>
                        </table>
                        <table align="left" style="width:300px;">
                          <tr>
                            <td><b>Valor procedimiento:</b></td>
                            <td>'.$row['total'].'</td>
                          <tr>
                          <tr>
                            <td><b>Valor abono:</b></td>
                            <td>'.$row['abonos'].'</td>
                          <tr>
                          <tr>
                            <td><b>Saldo:</b></td>
                            <td>'.$row['saldo'].'</td>
                          <tr>
                          <tr>
                            <td><b>Cuotas restantes:</b></td>
                            <td>'.$row['faltantes'].'</td>
                          <tr>
                          '.$templateSaldo.'
                        </table><hr>';
          $templateDetail = "";
          $count2 = 0;
        }
      }
      $count++;
    }
    $res['template'] = $template;
    $res['total'] = $saldoTotal;
    return $res;
  }
  else{
    $res['total'] = 0;
    $res['template'] = "";
    return $res;
  }
  }

  function evolutionReport($idUser){
    $sql = "SELECT
            	B.id as id,
            	(SELECT COUNT(DISTINCT ZS.id) FROM tbl_evolutions_pays as ZS WHERE ZS.idEvolution = B.id) as pagos,
            	'Pendiente de paz y salvo' as faltantes,
            	DATE_ADD((SELECT BPZ.date FROM tbl_evolutions_pays as BPZ WHERE BPZ.idEvolution = B.id ORDER BY BPZ.date DESC LIMIT 1), INTERVAL 30 DAY) as proximo,
            	CONCAT('Consulta - ',B.observations) as procedimiento,
            	MP.name as metodopago,
            	1 as sessions,
            	0 as initial,
            	B.price as tas,
            	FORMAT(B.price, 2) as valorCuota,
            	BP.date as fecha,
            	FORMAT(BP.rode, 2) as valorpagado,
            	FORMAT(IF((B.price-BP.rode)>0,B.price-BP.rode,0), 2) as saldocuota,
            	FORMAT(B.price,2) as total,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0), 2) as abonos,
            	FORMAT(B.price-(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0)),2) as saldo,
            	(SELECT SUM(Z.price) FROM tbl_evolutions as Z WHERE Z.idUser = '$idUser') as acomulado
            FROM
            	tbl_evolutions_pays as BP
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_evolutions as B ON B.id = BP.idEvolution
            WHERE B.idUser = '$idUser'
            GROUP BY BP.id
            ORDER BY B.id ASC";
    $data = $this->requestData($sql);

    $idActual = 0;
    $template = '';
    $templateDetail = '';
    $saldoTotal = 0;
    $count = 0;
    $count2 = 0;
    $final = count($data);
    $rest = 0;

    if(count($data)>0){
    foreach ($data as $row) {
      $id = $row['id'];
      $saldoTotal = $row['acomulado'];
      $sessions = $row['sessions'];
      $pagos = $row['pagos'];
      $count2++;

      $templateSaldo = '';

      if($row['saldo']>0){
        if($row['faltantes']>0){

          $cuotaActual = ($row['tas']-$row['initial'])/$row['faltantes'];

          $cuotaActual = number_format((float)$cuotaActual, 2, '.', ',');

          $templateSaldo = '<tr>
                              <td><b>Fecha de proximo pago:</b></td>
                              <td>'.$row['proximo'].'</td>
                            <tr>
                            <tr>
                              <td><b>Pago minimo:</b></td>
                              <td>'.$cuotaActual.'</td>
                            <tr>';
        }
        else{
          $templateSaldo = '<tr>
                              <td><b>Fecha de proximo pago:</b></td>
                              <td>'.$row['proximo'].'</td>
                            <tr>
                            <tr>
                              <td><b>Pago minimo:</b></td>
                              <td>'.$row['saldo'].'</td>
                            <tr>';
        }
      }


      $templateDetail .= '<tr>
                            <td>'.$row['procedimiento'].'</td>
                            <td> Cuota '.$count2.'</td>
                            <td>'.$row['metodopago'].'</td>
                            <td>'.$row['valorCuota'].'</td>
                            <td>'.$row['fecha'].'</td>
                            <td>'.$row['valorpagado'].'</td>
                            <td>'.$row['saldocuota'].'</td>
                          </tr>';

      if($final == $count+1){
        $template .= '<table>
                        <thead>
                          <tr>
                            <th align="left">Procedimiento</th>
                            <th align="left">Descripción</th>
                            <th align="left">Forma de pago</th>
                            <th align="left">Valor cuota</th>
                            <th align="left">Fecha</th>
                            <th align="left">Valor pagado</th>
                            <th align="left">Saldo cuota</th>
                          </tr>
                        </thead>
                        <tbody>'.$templateDetail.'</tbody>
                      </table>
                      <table align="left" style="width:300px;">
                        <tr>
                          <td><b>Valor procedimiento:</b></td>
                          <td>'.$row['total'].'</td>
                        <tr>
                        <tr>
                          <td><b>Valor abono:</b></td>
                          <td>'.$row['abonos'].'</td>
                        <tr>
                        <tr>
                          <td><b>Saldo:</b></td>
                          <td>'.$row['saldo'].'</td>
                        <tr>
                        <tr>
                          <td><b>Cuotas restantes:</b></td>
                          <td>'.$row['faltantes'].'</td>
                        <tr>
                        '.$templateSaldo.'
                      </table><hr>';
        $templateDetail = "";
        $count2 = 0;
      }
      else{
        if($data[$count+1]['id'] > $id){
          $template .= '<table>
                          <thead>
                            <tr>
                              <th align="left">Procedimiento</th>
                              <th align="left">Descripción</th>
                              <th align="left">Forma de pago</th>
                              <th align="left">Valor cuota</th>
                              <th align="left">Fecha</th>
                              <th align="left">Valor pagado</th>
                              <th align="left">Saldo cuota</th>
                            </tr>
                          </thead>
                          <tbody>'.$templateDetail.'</tbody>
                        </table>
                        <table align="left" style="width:300px;">
                          <tr>
                            <td><b>Valor procedimiento:</b></td>
                            <td>'.$row['total'].'</td>
                          <tr>
                          <tr>
                            <td><b>Valor abono:</b></td>
                            <td>'.$row['abonos'].'</td>
                          <tr>
                          <tr>
                            <td><b>Saldo:</b></td>
                            <td>'.$row['saldo'].'</td>
                          <tr>
                          <tr>
                            <td><b>Cuotas restantes:</b></td>
                            <td>'.$row['faltantes'].'</td>
                          <tr>
                          '.$templateSaldo.'
                        </table><hr>';
          $templateDetail = "";
          $count2 = 0;
        }
      }
      $count++;
    }
    $res['template'] = $template;
    $res['total'] = $saldoTotal;
    return $res;
  }
  else{
    $res['template'] = "";
    $res['total'] = 0;
    return $res;
  }


  }

  function generatePDF($post){
    extract($post);
    $idUser = $this->getIDUser($pacienteEmail);
    $info = $this->getInfo($idUser);
    $budgets = $this->budgetsReport($idUser);//$this->getBudgets($this->all($post));
    $evolutions = $this->evolutionReport($idUser);//$this->getEvolutions($this->evolutions($post));

    require_once '../templatePDF/Pay_template.php';
    $budget = new Pay_template();
    $template = $budget->payData($info, $budgets, $evolutions);
    return $template;

    // $res['$info'] = $info;
    // $res['$budgets'] = $budgets;
    // $res['$evolutions'] = $evolutions;
    // return $res;
  }


}
