<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

class Mailer
{
	public function sendPHPMailer($subject, $text, $email){

    $subject="[F] Frecuency -".$subject;


    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->isSMTP();
        $mail->Host = '190.60.211.17';
        $mail->Port = 25;

        //Recipients
        $mail->setFrom('contacto@hco.com.co', 'Solicitud Pedido de Productos HCO');
        $mail->addAddress($email);

        //Content
        $mail->isHTML(true);
        $mail->Subject = "[HCO] Solicitud de productos -".$subject;
        $mail->Body    = $this->getTemplate($subject, $text, $emailClinica, $phoneClinica);

        $mail->send();
        return 'Message has been sent';
    } catch (Exception $e) {
        return 'Message could not be sent.';
        return 'Mailer Error: ' . $mail->ErrorInfo;
    }
	}

  public function sendEmail($subject, $text, $email, $emailClinica, $phoneClinica, $title){

		$template = $this->getTemplate($subject, $text, $emailClinica, $phoneClinica, $title);
		$subject="[HCO] ".$subject;

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		//$headers .= 'To: Usuario <'.$email.'>' . "\r\n";
		$headers .= 'From: HCO <contacto@hco.com.co>' . "\r\n";
		$headers .= "Date: ".date("r (T)")." \r\n";
		//$headers .= 'Reply-To: programador1@solucionesstar.com' . "\r\n" .
		$headers .= 'X-Mailer: PHP/' . phpversion();
		//$headers .= "X-Sender: <programador1@solucionesstar.com>\n";
		$headers .= "X-Priority: 1\n"; // Urgent message!
		//$headers .= "Return-Path:programador1@solucionesstar.com\n"; // Return path for errors
		//$headers .= "Bcc: $email\r\n";  //direcciones que recibirán copia oculta
		$send = mail($email, $subject, $template, $headers);


		// //$email = "juliocortesweb@gmail.com";
		// //$email = "servicios@coopafa.com";
		// $template = $this->getTemplate($subject, $text);
    // $subject="[F] Frecuency -".$subject;
		//
		// $headers  = 'MIME-Version: 1.0' . "\r\n";
		// $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		// $headers .= 'To: Usuario <'.$email.'>' . "\r\n";
		// $headers .= 'From: Frecuency <programador1@solucionesstar.com>' . "\r\n";
		// $headers .= "Date: ".date("r (T)")." \r\n";
		// $headers .= 'Reply-To: programador1@solucionesstar.com' . "\r\n" .
		// $headers .= 'X-Mailer: PHP/' . phpversion();
		// //$headers .= "X-Sender: <programador1@solucionesstar.com>\n";
		// $headers .= "X-Priority: 1\n"; // Urgent message!
		// $headers .= "Return-Path:programador1@solucionesstar.com\n"; // Return path for errors
		// $headers .= "Bcc: programador1@solucionesstar.com\r\n";  //direcciones que recibirán copia oculta
		// $send = mail($email, $subject, $template, $headers);
		if($send){
			return "Se ha enviado correctamente.";
		}
		else{
			return "Error al enviar mensaje.";
		}
  }

  public function getTemplate($subject, $text, $emailClinica, $phoneClinica, $title){
    $template = '<html>
							    <head>
							      <meta charset="utf-8">
							      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
							      <meta name="apple-mobile-web-app-capable" content="yes">
							    </head>
							    <body>
							        <table class="email" width="100%" border= 0; cellspacing=0; cellpadding="20" style="border-bottom-width: 10px; border-bottom-style: solid; border-bottom-color: #444">
							        <tr>
							          <td class="header"  style="border-bottom: solid #0683c9 5px;">
							            <table border="0" style="color: #444;width: 100%;margin: 0 auto;font-family: Arial, Helvetica, sans-serif; width:100%; max-width:720px;"cellspacing="0">
							              <tr>
							                <td>
							                  <h1 style="text-align: center;">'.$title.'</h1>
							                </td>
							                <td>

							                </td>
							              </tr>
							            </table>
							          </td>
							        </tr>
							        <tr>
							           <td class="content" style="background-color: #fff;">
							             <table border="0" style="max-width:640px;width: 100%;background-color:#fff; color: #444; margin: 0 auto;border-bottom-width: 1px;border-bottom-style: solid;border-bottom-color: #ddd;font-family: Arial, Helvetica, sans-serif;" cellspacing="0">
							               <tr style="padding:15px;">
							                 <td>
							                   <h2 style="text-align: left; color: #444;font-size:18px;"> '.$subject.'</h2>
							                   <p style="text-align: left; color: #444;font-size:14px;">'.$text.'</p>
							                 </td>
							                </tr>
							            </table>
							          </td>
							        </tr>
							       <tr>
							     <td class="footer" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;">
							        <table border="0" style="margin: 0 auto;font-family: Arial, Helvetica, sans-serif;margin-bottom: 20px;font-size: small;color: #999;"cellspacing="0">
							          <tr>
							            <td width="60%">
							              <p style="text-align: center; color: #444;font-size:14px;">Si deseas registrar un comentario o una observación</p>
							              <p style="text-align: center; color: #444;font-size:14px;">Puedes escribirnos a '.$emailClinica.' o a través de las líneas telefónicas '.$phoneClinica.'</p>
							              <p style="text-align: center;">HCO - 2018</p>
							            </td>
							          </tr>
							        </table>
							      </td>
							    </tr>
							  </table>
							</body>
							</html>';
        return $template;
  }
}
