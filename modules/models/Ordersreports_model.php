<?php
require_once 'Queries.php';
class Ordersreports_model extends Queries {

  function all($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	L.id AS id,
            	L.date AS date,
            	P.name AS product,
            	C.name AS category,
            	S.name AS subcategory,
            	L.count AS count,
            	L.type AS type,
            	PC.und AS und
            FROM
              tbl_products_log AS L
              LEFT JOIN tbl_products AS P ON P.id = L.idProduct
              LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = L.idProduct
              LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
              LEFT JOIN tbl_products_subcategory AS S ON S.id = P.idSubCategory
            WHERE
            	L.idClinic = '$idClinic'
            ORDER BY
            	L.id DESC";
    return $this->requestData($sql);
  }

  function filter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";
    if($dateInit != null){
			$dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
			$dateInit = date ( 'Y-m-j' , $dateInit );
			$dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
			$dateFinish = date ( 'Y-m-j' , $dateFinish );
			$validate1 = "AND L.date BETWEEN '$dateInit' AND '$dateFinish'";
		}

    $sql = "SELECT
            	L.id AS id,
            	L.date AS date,
            	P.name AS product,
            	C.name AS category,
            	S.name AS subcategory,
            	L.count AS count,
            	L.type AS type,
            	PC.und AS und
            FROM
              tbl_products_log AS L
              LEFT JOIN tbl_products AS P ON P.id = L.idProduct
              LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = L.idProduct
              LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
              LEFT JOIN tbl_products_subcategory AS S ON S.id = P.idSubCategory
            WHERE
            	L.idClinic = '$idClinic' $validate1
            ORDER BY
            	L.id DESC";
    return $this->requestData($sql);
  }
}
