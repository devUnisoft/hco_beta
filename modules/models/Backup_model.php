<?php
require_once 'Queries.php';
class Backup_model extends Queries {
  /**
 * @function    backupDatabaseTables
 * @author      CodexWorld
 * @link        http://www.codexworld.com
 * @usage       Backup database tables and save in SQL file
 */
function backupDatabaseTables($dbHost,$dbUsername,$dbPassword,$dbName,$tables = '*'){
    //connect & select the database
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

    //get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = $db->query("SHOW TABLES");
        while($row = $result->fetch_row()){
            $tables[] = $row[0];
        }
    }else{
        $tables = is_array($tables)?$tables:explode(',',$tables);
    }

    $return = "";
    //loop through the tables
    foreach($tables as $table){
        $result = $db->query("SELECT * FROM $table");
        $numColumns = $result->field_count;

        $return .= "DROP TABLE $table;";

        $result2 = $db->query("SHOW CREATE TABLE $table");
        $row2 = $result2->fetch_row();

        $return .= "\n\n".$row2[1].";\n\n";

        for($i = 0; $i < $numColumns; $i++){
            while($row = $result->fetch_row()){
                $return .= "INSERT INTO $table VALUES(";
                for($j=0; $j < $numColumns; $j++){
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
                    if ($j < ($numColumns-1)) { $return.= ','; }
                }
                $return .= ");\n";
            }
        }

        $return .= "\n\n\n";
    }

    //save file
    $nameFile = 'db-backup-'.time().'.sql';
    $handle = fopen($nameFile,'w+');
    fwrite($handle,$return);
    fclose($handle);
    return $nameFile;
}


  function clinic($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	razonsocial,
            	nit,
            	direccion,
            	telefono,
            	email,
            	personacontacto,
            	documento,
            	codigoentidad,
            	codigoprestador,
            	codigoadministradora,
            	nombreadministradora,
            	imagen
            FROM
            	clinicas
            WHERE
            	id = $idClinic";
    $data =  $this->requestData($sql)[0];

    extract($data);

    $sql = "INSERT INTO clinicas (razonsocial,
                                  nit,
                                  direccion,
                                  telefono,
                                  email,
                                  personacontacto,
                                  documento,
                                  codigoentidad,
                                  codigoprestador,
                                  codigoadministradora,
                                  nombreadministradora,
                                  imagen
                                )
                          VALUES (
                            '$razonsocial',
                            '$nit',
                            '$direccion',
                            '$telefono',
                            '$email',
                            '$personacontacto',
                            '$documento',
                            '$codigoentidad',
                            '$codigoprestador',
                            '$codigoadministradora',
                            '$nombreadministradora',
                            '$imagen'
                          )";
    return $sql;
  }

  function pacientes($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $sql = "SELECT
            	razonsocial,
            	nit,
            	direccion,
            	telefono,
            	email,
            	personacontacto,
            	documento,
            	codigoentidad,
            	codigoprestador,
            	codigoadministradora,
            	nombreadministradora,
            	imagen
            FROM
            	users
            WHERE
            	id = $idClinic";
    $data =  $this->requestData($sql)[0];

    extract($data);

    $sql = "INSERT INTO clinicas (razonsocial,
                                  nit,
                                  direccion,
                                  telefono,
                                  email,
                                  personacontacto,
                                  documento,
                                  codigoentidad,
                                  codigoprestador,
                                  codigoadministradora,
                                  nombreadministradora,
                                  imagen
                                )
                          VALUES (
                            '$razonsocial',
                            '$nit',
                            '$direccion',
                            '$telefono',
                            '$email',
                            '$personacontacto',
                            '$documento',
                            '$codigoentidad',
                            '$codigoprestador',
                            '$codigoadministradora',
                            '$nombreadministradora',
                            '$imagen'
                          )";
    return $sql;
  }

  function backup($post){
    // $res['backup'] = $this->general();
    // $res['info'] = $this->clinic($post);
    // $res['pacientes'] = $this->pacientes($post);

    return shell_exec('mysqldump --user=root --password=usc2017* --host=localhost hco > file.sql');
  }
}
