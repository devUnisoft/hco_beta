<?php
require_once 'Queries.php';
class Rips_model extends Queries {

  function getTypeDocument($type){
    //abrevio tipo documento
    if($type == "Cedula de Ciudadania"){
        return "CC";
    }
    else if($type == "Cedula de Extranjeria"){
        return "CE";
    }
    else if($type == "Registro Civil"){
        return "RC";
    }
    else if($type == "Tarjeta de Identidad"){
        return "TI";
    }
    else if($type == "NIT"){
        return "NT";
    }
    else if($type == "Adulto sin identificacion"){
        return "AS";
    }
    else if($type == "Menor sin identificacion"){
        return "MS";
    }
    else if($type == "Pasaporte"){
        return "PA";
    }
    else{
        return "CC";
    }
  }

  function getName($name){
    $names = explode(" ", $name);
    $count = count($names);
    if($count > 0){
      return $names;
    }else{
      $names[0] = $name;
      $names[1] = "";
      return $names;
    }
  }

  function getAge($age){
    $date = time() - strtotime($age);
    return floor($date / 31556926);
  }

  function getValidateFilter($post){
    extract($post);
    $validate1 = "";
    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
      $validate1 .= " AND E.date BETWEEN '$dateInit' AND '$dateFinish' ";
    }

    if($idProfesional != null){
      $validate1 .= " AND E.idProfesional = '$idProfesional' ";
    }
    return $validate1;
  }

  function usuarios($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, 0) AS code,
              IFNULL(C.codigoentidad, 0) AS codeAdmin,
              IFNULL(U.tipodeusuario, 0) AS userType,
              IFNULL(U.first_name, ' ') AS name,
              IFNULL(U.last_name, ' ') AS lastName,
              IFNULL(U.birthdate, ' ') AS age,
              IFNULL(U.gender, ' ') AS gener,
              IFNULL(G.codigodepartamento, 0) AS codeDepartament,
              IFNULL(G.codigomunicipio, 0) AS codeCity,
              IFNULL(U.zona, ' ') AS zone
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
        foreach ($this->requestData($sql) as $row) {
          $typeDocument = $row['typeDocument'];
          $code = $row['code'];
          $codeAdmin = $row['codeAdmin'];
          $userType = $row['userType'];
          $name = $row['name'];
          $lastName = $row['lastName'];
          $age = $row['age'];
          $gener = $row['gener'];
          $codeDepartament = $row['codeDepartament'];
          $codeCity = $row['codeCity'];
          $zone = $row['zone'];

          $type = $this->getTypeDocument($typeDocument);
          $names = $this->getName($name);
          $lastnames = $this->getName($lastName);
          $age = $this->getAge($age);
          $ageUnd = 3;

          $name1 = $names[0];
          $name2 = $names[1];

          $lastname1 = $names[0];
          $lastname2 = $names[1];

          $data .= "{$type},{$code},{$codeAdmin},{$userType},{$lastname1},{$lastname2},{$name1},{$name2},{$age},{$ageUnd},{$gener},{$codeDepartament},{$codeCity},{$zone}\r\n";
        }
      }
      return $data;
  }

  function transaccion($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(C.razonsocial, ' ') AS razonSocial,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.id, ' ') AS bill,
              IFNULL(E.date, ' ') AS dateBill,
              IFNULL(C.codigoentidad, ' ') AS codeAdmin,
              IFNULL(C.nombreadministradora, ' ') AS nameAdmin,
              IFNULL(FORMAT(((E.price*U.porcentaje)/100), 2), 0) AS comision,
              IFNULL(FORMAT(((E.price*E.discount)/100), 2), 0) AS discount,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $prestadorCode = $row['prestadorCode'];
        $razonSocial = $row['razonSocial'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $bill = $row['bill'];
        $dateBill = $row['dateBill'];
        $dateInit;
        $dateFinish;
        $codeAdmin = $row['codeAdmin'];
        $nameAdmin = $row['nameAdmin'];
        $numberContract = 0;
        $plainBenefit = 30;
        $numberPolize = 10;
        $copago = 0;
        $discount = $row['discount'];
        $comision = $row['comision'];
        $price = $row['price'];
        $data .= "{$prestadorCode},{$razonSocial},{$typeDocument},{$code},{$bill},{$dateBill},{$dateInit},{$dateFinish},{$codeAdmin},{$nameAdmin},{$numberContract},{$plainBenefit},{$numberPolize},{$copago},{$discount},{$comision},{$price}\r\n";
      }
      }
      return $data;
  }

  function consulta($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS dateBill,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = $row['bill'];
        $prestadorCode = $row['prestadorCode'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $dateBill = $row['dateBill'];
        $codeAutorizacion = 0;
        $codeCups = $row['codeconsult'];
        $finishCups = 10;
        $causeExtern = 13;
        $diagnosticName = $row['diagnosticName'];
        $diagnosticName1 = '';
        $diagnosticName2 = '';
        $diagnosticName3 = '';
        $typeDiagnostic = $row['typeDiagnostic'];
        $price = $row['price'];
        $data .= "{$bill},{$prestadorCode},{$typeDocument},{$code},{$dateBill},{$codeAutorizacion},{$codeCups},{$finishCups},{$causeExtern},{$diagnosticName},{$diagnosticName1},{$diagnosticName2},{$diagnosticName3},{$typeDiagnostic},{$price},{$price},{$price}\r\n";
      }
      }
      return $data;
  }


  function procedimientos($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS dateBill,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = $row['bill'];
        $prestadorCode = $row['prestadorCode'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $dateBill = $row['dateBill'];
        $codeAutorizacion = 0;
        $codeCups = $row['codeconsult'];
        $ambito = 1;
        $finalidad = 1;
        $person = "";
        $diagnosticName = $row['diagnosticName'];
        $diagnosticName1 = '';
        $diagnosticNameC = "";
        $forma = "";
        $price = $row['price'];
        $data .= "{$bill},{$prestadorCode},{$typeDocument},{$code},{$dateBill},{$codeAutorizacion},{$codeCups},{$ambito},{$finalidad},{$person},{$diagnosticName},{$diagnosticName1},{$diagnosticNameC},{$forma},{$price}\r\n";
      }
      }
      return $data;
  }
  function control($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(E.date, ' ') AS dateBill
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $prestadorCode = $row['prestadorCode'];
        $date = $row['dateBill'];
        $code = "";
        $records = 0;
        $data .= "{$prestadorCode},{$date},{$code},{$records}\r\n";
      }
      }
      return $data;
  }
  function urgencias($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS date,
              IFNULL(DATE_FORMAT(E.dateSave, '%H:%i:00'), ' ') AS hour,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = $row['bill'];
        $prestadorCode = $row['prestadorCode'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $date = $row['date'];
        $hour = $row['hour'];
        $codeAutorizacion = 0;
        $causeExtern = 13;
        $diagnosticName = $row['diagnosticName'];
        $diagnosticName1 = '';
        $diagnosticName2 = '';
        $diagnosticName3 = '';
        $destino = 0;
        $state = 1;
        $causeBasic = "";
        $date2 = "";
        $hour2 = "";
        $data .= "{$bill},{$prestadorCode},{$typeDocument},{$code},{$date},{$hour},{$codeAutorizacion},{$causeExtern},{$diagnosticName},{$diagnosticName1},{$diagnosticName2},{$diagnosticName3},{$destino},{$state},{$causeBasic},{$date2},{$hour2}\r\n";
      }
      }
      return $data;
  }

  function otherServices($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS date,
              IFNULL(DATE_FORMAT(E.dateSave, '%H:%i:00'), ' ') AS hour,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = $row['bill'];
        $prestadorCode = $row['prestadorCode'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $codeAutorizacion = 0;
        $typeService = 0;
        $codeService = 0;
        $service = 0;
        $serviceCount = 0;
        $servicePrice = 0;
        $price = 0;
        $data .= "{$bill},{$prestadorCode},{$typeDocument},{$code},{$codeAutorizacion},{$typeService},{$codeService},{$service},{$serviceCount},{$servicePrice},{$price}\r\n";
      }
      }
      return $data;
  }

  function medicamentos($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS date,
              IFNULL(DATE_FORMAT(E.dateSave, '%H:%i:00'), ' ') AS hour,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = $row['bill'];
        $prestadorCode = $row['prestadorCode'];
        $typeDocument = $row['typeDocument'];
        $typeDocument = $this->getTypeDocument($typeDocument);
        $code = $row['code'];
        $codeAutorizacion = 0;
        $codeMedic = 0;
        $typeMedic = 1;
        $medicName = "";
        $medicforma = "";
        $medicConcent = "";
        $medicUnd = "";
        $und = "";
        $MedicPrice = 0;
        $price = 0;
        $data .= "{$bill},{$prestadorCode},{$typeDocument},{$code},{$codeAutorizacion},{$codeMedic},{$typeMedic},{$medicName},{$medicforma},{$medicConcent},{$medicUnd},{$und},{$MedicPrice},{$price}\r\n";
      }
      }
      return $data;
  }

  function recienNacido($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS date,
              IFNULL(DATE_FORMAT(E.dateSave, '%H:%i:00'), ' ') AS hour,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = "";
        $prestadorCode = "";
        $typeDocument = "";
        $code = "";
        $date = "";
        $hour = "";
        $age = "";
        $control = "";
        $gener = "";
        $peso = "";
        $codeDiagnostic = "";
        $causeExtern = 0;
        $date2 = "";
        $hour2 = "";
        $data = "{$bill},{$prestadorCode},{$typeDocument},{$code},{$date},{$hour},{$age},{$control},{$gener},{$peso},{$codeDiagnostic},{$causeExtern},{$date2},{$hour2}\r\n";
      }
      }
      return $data;
  }

  function hospitalizacion($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = $this->getValidateFilter($post);

    $sql = "SELECT
              IFNULL(E.id, ' ') AS bill,
              IFNULL(C.codigoprestador, ' ') AS prestadorCode,
              IFNULL(U.document__document_type__oid, ' ') AS typeDocument,
              IFNULL(U.document__number, ' ') AS code,
              IFNULL(E.date, ' ') AS date,
              IFNULL(DATE_FORMAT(E.dateSave, '%H:%i:00'), ' ') AS hour,
              IFNULL(ETP.code, ' ') AS codeconsult,
              IFNULL(ED.code, ' ') AS diagnosticName,
              IFNULL(ETS.name, ' ') AS typeDiagnostic,
              IFNULL(FORMAT(E.price, 2), 0) AS price
            FROM
              users AS U
            LEFT JOIN tbl_evolutions as E ON E.idUser = U.id
            LEFT JOIN tbl_evolutions_type_state as ETS ON ETS.id = E.type
            LEFT JOIN tbl_evolutions_type_process as ETP ON ETP.id = E.idTypeConsult
            LEFT JOIN tbl_evolutions_details_diagnosis as EDD ON EDD.idEvolution = E.id
            LEFT JOIN tbl_evolutions_diagnosis as ED ON ED.id = EDD.idEvolutionDiagnostic
            LEFT JOIN georeferences as G ON U.lugar_residencia = G.name
            LEFT JOIN clinicas as C ON C.id = $idClinic
            WHERE E.idClinic = $idClinic $validate1
            ORDER BY E.id ASC";

      $data = "";
      if($this->requestCount($sql)>0){
      foreach ($this->requestData($sql) as $row) {
        $bill = "";
        $prestadorCode = "";
        $typeDocument = "";
        $code = "";
        $date = "";
        $hour = "";
        $age = "";
        $control = "";
        $gener = "";
        $peso = "";
        $codeDiagnostic = "";
        $causeExtern = 0;
        $date2 = "";
        $hour2 = "";
        $data = "{$bill},{$prestadorCode},{$typeDocument},{$code},{$date},{$hour},{$age},{$control},{$gener},{$peso},{$codeDiagnostic},{$causeExtern},{$date2},{$hour2}\r\n";
      }
      }
      return $data;
  }
}
