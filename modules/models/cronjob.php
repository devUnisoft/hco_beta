<?php
require_once 'Queries.php';
class cronjob_model extends Queries {

  function orderautomatic($data){
    extract($data);
    $idClinic = $this->getIDClinic($clinicaName);
    $products = $this->products($idClinic);
    $res = [];
    $res[] = "Cantidad de productos a procesar: ".count($products);
    if(count($products)>0){
      $res[] = "Se van a procesar a continuacion.";
      foreach ($products as $item) {
        //product
        $idProduct = $item['id'];
        $product = $item['name'];
        $res[] = "Producto: ".$product;
        //provider
        $idProvider = $item['idProvider'];
        $res[] = "idProvider: ".$idProvider;
        $countProvider = $item['countProvider'];
        $provider = $this->getProvider($idProvider);
        $res[] = "proveedor: ".$provider[0]['name'];
        //order
        $uniquecode = $this->getUniqueCode();
        $res[] = "uniquecode: ".$uniquecode;
        $idOrder = $this->setOrder($uniquecode, $idClinic);
        $res[] = "idOrder: ".$idOrder;
        $details = $this->setOrderDetails($idOrder, $idProvider, $idProduct, $countProvider, $idClinic);
        $res[] = "details: ".$details;
        //email
        $template = $this->getTemplate($idOrder);
        $res[] = "template: ".$template;
        $result = $this->sendEmail($uniquecode, $template, $provider, $clinicaName);
        $res[] = "Email: ".$result;
        $sql="UPDATE tbl_products_clinic SET automatic = 1 WHERE idProduct = $idProduct";
        $this->requestCount2($sql);
      }
    }
    return $res;
  }

  function products($idClinic){
    $sql = "SELECT
              PC.idProduct AS id,
              P.name AS name,
              PC.stock,
              PC.stock,
            	PC.idProvider,
            	PC.alertStock,
            	PC.countProvider,
            	PC.und
            FROM
            	tbl_products AS P
            LEFT JOIN tbl_products_clinic AS PC ON P.id = PC.idProduct AND PC.idClinic = '$idClinic'
            LEFT JOIN tbl_products_category AS C ON C.id = P.idCategory
            LEFT JOIN tbl_products_subcategory AS SC ON SC.id = P.idSubCategory
            LEFT JOIN tbl_order_type_request OT ON OT.id = PC.idTypeOrderRequest
            LEFT JOIN tbl_providers as PR ON PR.id = PC.idProvider
            WHERE
              alertStock > stock
              AND idProvider > 0 AND automatic is null
            ORDER BY
            	P.name ASC";
    return $this->requestData($sql);
  }

  function getProvider($idProvider){
    $sql = "SELECT name, email FROM tbl_providers WHERE id  = $idProvider";
    return $this->requestData($sql);
  }

  function getUniqueCode(){
    $sql = "SELECT max(uniquecode) as id FROM tbl_orders";
    $data = $this->requestData($sql);
    $uniquecode = $data[0]['id'];
    $uniquecode = $uniquecode+1;
    return $uniquecode;
  }

  function setOrder($uniquecode, $idClinic){
    $date = date('Y-m-d');
    $id = 0;
    $sql="INSERT INTO tbl_orders(uniquecode, date, active, idClinic) VALUE('$uniquecode','$date','1', '$idClinic');";
    $this->requestCount2($sql);

    $sql = "SELECT max(id) as id FROM tbl_orders";
    $data = $this->requestData($sql);
    $id = $data[0]['id'];
    return $id;
  }

  function setOrderDetails($idOrder, $idProvider, $idProduct, $count, $idClinic){
    $sql="INSERT INTO tbl_orders_details(idOrder, idProvider, idProduct, count, active, idClinic) VALUE('$idOrder','$idProvider','$idProduct','$count','1', '$idClinic');";
    $this->requestCount2($sql);
    return $sql;
  }

  function getTemplate($idOrder){
    $sql = "SELECT * FROM tbl_orders_details WHERE idOrder  = $idOrder";
    $data = $this->requestData($sql);

    $template = "<table>";
    $template .= "<tr><td>Producto</td><td>Unidad de medida</td><td>Cantidad</td></tr>";

    foreach ($data as $value) {
      $idProvider = $value['idProvider'];
      $idProduct = $value['idProduct'];
      $count = $value['count'];
      $provider;
      $emailProvider;
      $und;
      $sql = "SELECT P.name, PC.und FROM tbl_products as P LEFT JOIN tbl_products_clinic AS PC ON PC.idProduct = P.id WHERE P.id  = $idProduct";
      $data = $this->requestData($sql);
      $idProduct = $data[0]['name'];
      $und = $data[0]['und'];

      $sql = "SELECT name, email FROM tbl_providers WHERE id  = $idProvider";
      $data = $this->requestData($sql);
      $provider = $data[0]['name'];
      $emailProvider = $data[0]['email'];

      $template .= "<tr><td>$idProduct</td><td>$und</td><td>$count</td></tr>";
    }
    return $template .= "</table>";
  }



  function sendEmail($uniquecode, $template, $provider, $clinica){

    $providerName  = $provider[0]['name'];
    $providerEmail = $provider[0]['email'];

    $sql = "SELECT telefono, email FROM clinicas WHERE razonsocial LIKE '%$clinica%'";
    $phoneClinica = $this->requestData($sql)[0]['telefono'];
    $emailClinica = $this->requestData($sql)[0]['email'];

    require_once 'PHPMailer/Mailer.php';
    $text = '<br>Solicitante: <b>'.$clinica.'</b>';
    $text .= '<br>Proveedor: <b>'.$providerName.'</b>';
    $text .= '<br>Solicitud:';
    $text .= $template;

    $subject = 'ORDEN DE PEDIDO No.'. $uniquecode;
    $mail = new Mailer();
    return $mail->sendEmail($subject, $text, $providerEmail, $emailClinica, $phoneClinica, "Solicitud Pedido de Productos HCO");
  }
}

?>
