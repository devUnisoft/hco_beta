<?php
require_once 'Queries.php';
class Reports_model extends Queries {

  //Reporte #1
  function tratamientosFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }

    if($state == 1){
      $validate1 .= " AND B.changeToPay IS NULL";
    }
    else if($state == 2){
      $validate1 .= " AND B.changeToPay = 1";
    }

    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;

    return $this->tratamientos($res);
  }

  function tratamientos($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }
    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
            	CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
            	if(B.changeToPay = 1, 'Autorizado', 'Sin Autorizar') as state,
            	FORMAT(B.total,2) as total,
            	FORMAT(IFNULL(B.initial, 0), 2) as initial,
            	FORMAT(((B.total-B.initial)/B.sessions), 2) as valorCuota,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0)),2) as saldo,
            	IFNULL(BP.dateSave, ' ') as dateSave
            FROM
            	tbl_budgets as B
            LEFT JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            WHERE B.idClinic = $idClinic $validate
            GROUP BY B.id
            ORDER BY B.id DESC";
    return $this->requestData($sql);
  }

  //Reporte #2
  function consultadiaFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";
    if(isset($validate2)){
      $validate1 = $validate2;
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }

    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;

    return $this->consultadia($res);
  }

  function consultadia($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            U.document__number as patientCode,
            CONCAT(U.first_name,' ', U.last_name) as patientName,
            CONCAT(P.first_name,' ', P.last_name) as profesional,
            B.dateSave as date,
            CONCAT('Consulta - ', B.observations) as procedimiento,
            FORMAT(B.price,2) as total,
            FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0), 2) as abonos,
            FORMAT(B.price-(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0)),2) as saldo,
            IFNULL(BP.dateSave, ' ') as dateSave
            FROM
            	tbl_evolutions  as B
            LEFT JOIN tbl_evolutions_pays as BP ON BP.idEvolution = B.id
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            WHERE B.idClinic = $idClinic $validate
            GROUP BY B.id
            ORDER BY B.id DESC";
    return $this->requestData($sql);
  }

  //Reporte #2
  function pagoProfesionalesFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";

    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
      $validate1 .= "AND DATE(B.dateSave) BETWEEN '$dateInit' AND '$dateFinish'";
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }
    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;
    return $this->pagoProfesionales($res);
  }

  function pagoProfesionales($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
              CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
              FORMAT(B.total,2) as total,
              P.porcentaje as porcentaje,
              FORMAT(((B.total*P.porcentaje)/100), 2) as comision,
              BP.dateSave as fecha,
            	MP.name as metodopago,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0)),2) as saldo,
              IFNULL(CONCAT(R.first_name,' ', R.last_name), ' ') as receptor,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 6 AND B.idClinic = $idClinic $validate ), 0) as cheque,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 1 AND B.idClinic = $idClinic $validate ), 0) as efectivo,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 4 AND B.idClinic = $idClinic $validate ), 0) as credito,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 3 AND B.idClinic = $idClinic $validate ), 0) as debito,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 2 AND B.idClinic = $idClinic $validate ), 0) as transferencia,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 5 AND B.idClinic = $idClinic $validate ), 0) as otros,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as totalT,
              IFNULL((SELECT (SUM(PU.rode)*P.porcentaje)/100 FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as comisionTotal
            FROM
            	tbl_budgets as B
            INNER JOIN tbl_budgets_pays as BP  ON  BP.idBudget = B.id
            INNER JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            INNER JOIN users as U ON U.id = B.idUser
            INNER JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN users as R ON R.id = BP.idReceptor
            WHERE B.idClinic = $idClinic AND BP.dateSave IS NOT NULL $validate
            GROUP BY B.id
            ORDER BY B.id DESC";

            $data1 =  $this->requestData($sql);

            $sql = "SELECT
                    	U.document__number as patientCode,
                    	CONCAT(U.first_name,' ', U.last_name) as patientName,
                      CONCAT(P.first_name,' ', P.last_name) as profesional,
                    	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
                    	  CONCAT('Consulta - ', B.observations) as procedimiento,
                      FORMAT(B.price,2) as total,
                      P.porcentaje as porcentaje,
                      FORMAT(((B.price*P.porcentaje)/100), 2) as comision,
                      BP.dateSave as fecha,
                    	MP.name as metodopago,
                    	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution AND BP.date  >= CURDATE()), 0), 2) as abonos,
                    	FORMAT(B.price-(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution AND BP.date  >= CURDATE()), 0)),2) as saldo,
                      IFNULL(CONCAT(R.first_name,' ', R.last_name), ' ') as receptor,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 6 AND B.idClinic = $idClinic $validate ), 0)as cheque,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 1 AND B.idClinic = $idClinic $validate ), 0) as efectivo,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 4 AND B.idClinic = $idClinic $validate ), 0) as credito,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 3 AND B.idClinic = $idClinic $validate ), 0) as debito,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 2 AND B.idClinic = $idClinic $validate ), 0) as transferencia,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 5 AND B.idClinic = $idClinic $validate ), 0) as otros,
                      IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as totalT,
                      IFNULL((SELECT (SUM(PU.rode)*P.porcentaje)/100 FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as comisionTotal
                    FROM
                    	tbl_evolutions as B
                    INNER JOIN tbl_evolutions_pays as BP  ON  BP.idEvolution = B.id
                    INNER JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
                    LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
                    INNER JOIN users as U ON U.id = B.idUser
                    INNER JOIN users as P ON P.id = B.idProfesional
                    LEFT JOIN users as R ON R.id = BP.idReceptor
                    WHERE B.idClinic = $idClinic AND BP.dateSave IS NOT NULL $validate
                    GROUP BY B.id
                    ORDER BY B.id DESC";
            $data2 =  $this->requestData($sql);

    if(count($data1)>0 && count($data2)>0 ){

      $data['pays']['cheque'] = $data1[0]['cheque'] + $data2[0]['cheque'];
      $data['pays']['efectivo'] = $data1[0]['efectivo'] + $data2[0]['efectivo'];
      $data['pays']['credito'] = $data1[0]['credito'] + $data2[0]['credito'];
      $data['pays']['debito'] = $data1[0]['debito'] + $data2[0]['debito'];
      $data['pays']['transferencia'] = $data1[0]['transferencia'] + $data2[0]['transferencia'];
      $data['pays']['otros'] = $data1[0]['otros'] + $data2[0]['otros'];
      $data['pays']['totalT'] = $data1[0]['totalT'] + $data2[0]['totalT'];
      $data['pays']['comisionTotal'] = $data1[0]['comisionTotal'] + $data2[0]['comisionTotal'];

      $data['data'] = array_merge($data1, $data2);

      return $data;
    }
    else if(count($data1)>0){
      $data['pays']['cheque'] = $data1[0]['cheque'];
      $data['pays']['efectivo'] = $data1[0]['efectivo'];
      $data['pays']['credito'] = $data1[0]['credito'];
      $data['pays']['debito'] = $data1[0]['debito'];
      $data['pays']['transferencia'] = $data1[0]['transferencia'];
      $data['pays']['otros'] = $data1[0]['otros'];
      $data['pays']['totalT'] = $data1[0]['totalT'];
      $data['pays']['comisionTotal'] = $data1[0]['comisionTotal'];
      $data['data'] = $data1;
      return $data;
    }
    else if(count($data2)>0){
      $data['pays']['cheque'] = $data2[0]['cheque'];
      $data['pays']['efectivo'] = $data2[0]['efectivo'];
      $data['pays']['credito'] = $data2[0]['credito'];
      $data['pays']['debito'] = $data2[0]['debito'];
      $data['pays']['transferencia'] = $data2[0]['transferencia'];
      $data['pays']['otros'] = $data2[0]['otros'];
      $data['pays']['totalT'] = $data2[0]['totalT'];
      $data['pays']['comisionTotal'] = $data2[0]['comisionTotal'];
      $data['data'] = $data2;
      return $data;
    }
    else{
      return null;
    }

    return $this->requestData($sql);
  }


  //Reporte #2
  function cajaFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";

    if($dateInit != null){
      $validate1 .= "AND DATE(B.dateSave) = '$dateInit'";
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }


    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;
    return $this->caja($res);
  }

  function caja($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
              CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
              FORMAT(B.total,2) as total,
              BP.dateSave as fecha,
            	MP.name as metodopago,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0)),2) as saldo,
              IFNULL(CONCAT(R.first_name,' ', R.last_name), ' ') as receptor,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 6 AND B.idClinic = $idClinic $validate ), 0) as cheque,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 1 AND B.idClinic = $idClinic $validate ), 0) as efectivo,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 4 AND B.idClinic = $idClinic $validate ), 0) as credito,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 3 AND B.idClinic = $idClinic $validate ), 0) as debito,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 2 AND B.idClinic = $idClinic $validate ), 0) as transferencia,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE PU.idMethodPay = 5 AND B.idClinic = $idClinic $validate ), 0) as otros,
              IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as totalT,
              IFNULL((SELECT (SUM(PU.rode)*P.porcentaje)/100 FROM tbl_budgets_pays AS PU INNER JOIN tbl_budgets as B ON  B.id = PU.idBudget WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as comisionTotal
            FROM
            	tbl_budgets as B
            INNER JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            INNER JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            INNER JOIN users as U ON U.id = B.idUser
            INNER JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN users as R ON R.id = BP.idReceptor
            WHERE B.idClinic = $idClinic AND BP.dateSave IS NOT NULL $validate
            GROUP BY B.id
            ORDER BY B.id DESC";
    $data1 =  $this->requestData($sql);

    $sql = "SELECT
            U.document__number as patientCode,
            CONCAT(U.first_name,' ', U.last_name) as patientName,
            CONCAT(P.first_name,' ', P.last_name) as profesional,
            B.dateSave as date,
            CONCAT('Consulta - ', B.observations) as procedimiento,
            FORMAT(B.price,2) as total,
            BP.date as fecha,
            MP.name as metodopago,
            FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution AND BP.date  >= CURDATE()), 0), 2) as abonos,
            FORMAT(B.price-(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution AND BP.date  >= CURDATE()), 0)),2) as saldo,
            IFNULL(CONCAT(R.first_name,' ', R.last_name), ' ') as receptor,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 6 $validate ), 0) as cheque,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 1 $validate ), 0) as efectivo,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 4 $validate ), 0) as credito,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 3 $validate ), 0) as debito,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 2 $validate ), 0) as transferencia,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE PU.idMethodPay = 5 $validate ), 0) as otros,
            IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE B.id > 0  $validate ), 0) as totalT,
            IFNULL((SELECT (SUM(PU.rode)*P.porcentaje)/100 FROM tbl_evolutions_pays AS PU INNER JOIN tbl_evolutions as B ON  B.id = PU.idEvolution WHERE B.id > 0 AND B.idClinic = $idClinic $validate ), 0) as comisionTotal
            FROM
            	tbl_evolutions  as B
            INNER JOIN tbl_evolutions_pays as BP ON BP.idEvolution = B.id
            INNER JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            INNER JOIN users as U ON U.id = B.idUser
            INNER JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN users as R ON R.id = BP.idReceptor
            WHERE B.idClinic = $idClinic AND BP.dateSave IS NOT NULL $validate
            GROUP BY B.id
            ORDER BY B.id DESC";
    $data2 =  $this->requestData($sql);

    if(count($data1)>0 && count($data2)>0 ){

      $data['pays']['cheque'] = $data1[0]['cheque'] + $data2[0]['cheque'];
      $data['pays']['efectivo'] = $data1[0]['efectivo'] + $data2[0]['efectivo'];
      $data['pays']['credito'] = $data1[0]['credito'] + $data2[0]['credito'];
      $data['pays']['debito'] = $data1[0]['debito'] + $data2[0]['debito'];
      $data['pays']['transferencia'] = $data1[0]['transferencia'] + $data2[0]['transferencia'];
      $data['pays']['otros'] = $data1[0]['otros'] + $data2[0]['otros'];
      $data['pays']['totalT'] = $data1[0]['totalT'] + $data2[0]['totalT'];
      $data['pays']['comisionTotal'] = $data1[0]['comisionTotal'] + $data2[0]['comisionTotal'];

      $data['data'] = array_merge($data1, $data2);

      return $data;
    }
    else if(count($data1)>0){
      $data['pays']['cheque'] = $data1[0]['cheque'];
      $data['pays']['efectivo'] = $data1[0]['efectivo'];
      $data['pays']['credito'] = $data1[0]['credito'];
      $data['pays']['debito'] = $data1[0]['debito'];
      $data['pays']['transferencia'] = $data1[0]['transferencia'];
      $data['pays']['otros'] = $data1[0]['otros'];
      $data['pays']['totalT'] = $data1[0]['totalT'];
      $data['pays']['comisionTotal'] = $data1[0]['comisionTotal'];
      $data['data'] = $data1;
      return $data;
    }
    else if(count($data2)>0){
      $data['pays']['cheque'] = $data2[0]['cheque'];
      $data['pays']['efectivo'] = $data2[0]['efectivo'];
      $data['pays']['credito'] = $data2[0]['credito'];
      $data['pays']['debito'] = $data2[0]['debito'];
      $data['pays']['transferencia'] = $data2[0]['transferencia'];
      $data['pays']['otros'] = $data2[0]['otros'];
      $data['pays']['totalT'] = $data2[0]['totalT'];
      $data['pays']['comisionTotal'] = $data2[0]['comisionTotal'];
      $data['data'] = $data2;
      return $data;
    }
    else{
      return null;
    }
  }
}
