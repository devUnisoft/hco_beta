<?php
require_once 'Queries.php';
class Reports_model extends Queries {

  //Reporte #1
  function tratamientosFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate1 = "";

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }

    if($state != null){
      if($state == 1){
        $validate1 .= " AND B.changeToPay = 0";
      }
      if($state == 2){
        $validate1 .= " AND B.changeToPay = 1 AND BP.date IS NULL";
      }
    }

    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;

    return $this->tratamientos($res);
  }

  function tratamientos($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }
    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
            	CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
            	if(B.changeToPay = 1, 'Autorizado', 'Sin Autorizar') as state,
            	FORMAT(B.total,2) as total,
            	FORMAT(IFNULL(B.initial, 0), 2) as initial,
            	FORMAT(((B.total-B.initial)/B.sessions), 2) as valorCuota,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget), 0)),2) as saldo,
            	BP.date as fecha
            FROM
            	tbl_budgets as B
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_budgets_pays as BP ON BP.idBudget = B.id
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            WHERE B.idClinic = $idClinic $validate
            GROUP BY B.id
            ORDER BY BP.dateSave DESC";
    return $this->requestData($sql);
  }

  //Reporte #2
  function consultadiaFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";
    if(isset($validate2)){
      $validate1 = $validate2;
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }

    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;

    return $this->consultadia($res);
  }

  function consultadia($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            U.document__number as patientCode,
            CONCAT(U.first_name,' ', U.last_name) as patientName,
            CONCAT(P.first_name,' ', P.last_name) as profesional,
            DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            B.name as procedimiento,
            FORMAT(B.total,2) as total,
            BP.date as fecha,
            FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0), 2) as abonos,
            FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_evolutions_pays AS PU WHERE PU.idEvolution = BP.idEvolution), 0)),2) as saldo,
            CONCAT(R.first_name,' ', R.last_name) as receptor
            FROM
            	tbl_evolutions  as B
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_evolutions_pays as BP ON BP.idEvolution = B.id
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            WHERE B.idClinic = $idClinic $validate
            GROUP BY BP.id
            ORDER BY BP.dateSave DESC";
    return $this->requestData($sql);
  }

  //Reporte #2
  function pagoProfesionalesFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";

    if($dateInit != null){
      $dateInit = strtotime ( '+0 day' , strtotime ( $dateInit ) ) ;
      $dateInit = date ( 'Y-m-j' , $dateInit );
      $dateFinish = strtotime ( '+0 day' , strtotime ( $dateFinish ) ) ;
      $dateFinish = date ( 'Y-m-j' , $dateFinish );
      $validate1 .= "AND DATE(dateSave) BETWEEN '$dateInit' AND '$dateFinish'";
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }
    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;
    return $this->pagoProfesionales($res);
  }

  function pagoProfesionales($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
              CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
              FORMAT(B.total,2) as total,
              P.porcentaje as porcentaje,
              FORMAT(((B.total*P.porcentaje)/100), 2) as comision,
              BP.date as fecha,
            	MP.name as metodopago,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0)),2) as saldo,
              CONCAT(R.first_name,' ', R.last_name) as receptor
            FROM
            	tbl_budgets as B
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_budgets_pays as BP  ON  BP.idBudget = B.id
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN users as R ON R.id = B.idReceptor
            WHERE B.idClinic = $idClinic $validate
            GROUP BY BP.id
            ORDER BY BP.dateSave DESC";
    return $this->requestData($sql);
  }


  //Reporte #2
  function cajaFilter($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);

    $validate1 = "";

    if($dateInit != null){
      $validate1 .= "AND DATE(dateSave) = '$dateInit'";
    }

    if($idProfesional != null){
      $validate1 .= " AND B.idProfesional = $idProfesional";
    }

    if($idMethod != null){
      $validate1 .= " AND BP.idMethodPay = $idMethod";
    }

    if($patientDocument != null){
      $validate1 .= " AND U.document__number = $patientDocument";
    }


    $res['clinicaName'] = $clinicaName;
    $res['validate1'] = $validate1;
    return $this->caja($res);
  }

  function caja($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $validate = "";
    if(isset($validate1)){
      $validate = $validate1;
    }

    $sql = "SELECT
            	U.document__number as patientCode,
            	CONCAT(U.first_name,' ', U.last_name) as patientName,
              CONCAT(P.first_name,' ', P.last_name) as profesional,
            	DATE_FORMAT(B.dateSave, '%Y-%m-%d') as date,
            	B.name as procedimiento,
              FORMAT(B.total,2) as total,
              BP.date as fecha,
            	MP.name as metodopago,
            	FORMAT(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0), 2) as abonos,
            	FORMAT(B.total-(IFNULL((SELECT SUM(PU.rode) FROM tbl_budgets_pays AS PU WHERE PU.idBudget = BP.idBudget AND BP.date  >= CURDATE()), 0)),2) as saldo,
              CONCAT(R.first_name,' ', R.last_name) as receptor
            FROM
            	tbl_budgets_pays as BP
            LEFT JOIN tbl_methods_pay as MP ON MP.id = BP.idMethodPay
            LEFT JOIN tbl_methods_cards as MPC ON MPC.id = BP.idCard
            LEFT JOIN tbl_budgets as B ON B.id = BP.idBudget
            LEFT JOIN users as U ON U.id = B.idUser
            LEFT JOIN users as P ON P.id = B.idProfesional
            LEFT JOIN users as R ON R.id = B.idReceptor
            WHERE B.idClinic = $idClinic $validate
            GROUP BY BP.id
            ORDER BY BP.dateSave DESC";
    return $this->requestData($sql);
  }
}
