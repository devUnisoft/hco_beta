<?php
require_once 'Queries.php';
require_once 'Budgets_model.php';
class Patient_model extends Queries {

  function all($post){
      extract($post);
    $sql = "SELECT id, CONCAT(document__number, ' - ', first_name, ' ', last_name) AS name FROM users  WHERE clinica = '$clinicaName' AND __t = 'Person' ORDER BY first_name ASC";
    return $this->requestData($sql);
  }

  function update($post){
    extract($post);
    $idClinic = $this->getIDClinic($clinicaName);
    $id = $this->getID('users', $id);

    $this->updateBudget($idBudget, $id);

    $sql = "UPDATE users
              SET __t = 'Person',
               first_name = '$name',
               last_name = '$lastname',
               email = '$email',
               gender = '$gender',
               active = 'True',
               document__document_type__oid = '$idTypeDocument',
               document__number = '$code',
               telefono = '$phone',
               clinica = '$clinicaName',
               direccion = '$address',
               porcentaje = 0,
               zona = 'U',
               tipousuario = '1',
               birthdate = '$dateBirth',
               lugar_nacimiento = '$placeBirth',
               lugar_residencia = '$residencePlace',
               tipodeusuario = '$userType',
               tipodesangre = '$bloodType',
               eps = '$eps',
               estadocivil = '$civilStatus',
               ocupacion = '$occupation',
               acudientenombre = '$attendantName',
               acudienteparentesco = '$attendantParent',
               acudientetelefono = '$attendantPhone'
              WHERE
              	id = '$id'";
    $this->requestCount2($sql);
    return $email;
  }

  function updateBudget($idBudget, $idUser){
    if($idBudget>0){
      $sql = "UPDATE tbl_budgets SET idUser = '$idUser' WHERE id = '$idBudget'";
      $this->requestCount2($sql);

      $sql = "SELECT email FROM users  WHERE id = '$idUser'";
      return $this->requestData($sql)[0]['email'];
    }
  }
}
