<?php
class Budget_template {

  function budgetData($data){
    extract($data);
    $templateDetail = $this->budgetTemplateDetails($budgetDetails);
    $template = $this->budgetTemplate($budget, $templateDetail);
    return $template;
  }

  function budgetTemplateDetails($data){
    $template = "";
    foreach ($data as $detail) {
      $template .= '<tr><td>'.$detail['process'].'</td><td>'.$detail['price'].'</td></tr>';
    }
    return $template;
  }

  function budgetTemplate($budget, $templateBudgetDetails){
    extract($budget);
    $clinicLogo = substr($clinicLogo, 2, strlen($clinicLogo));
    $template = '<!DOCTYPE html>
                  <html lang="es">
                  <head>
                    <meta charset="UTF-8">
                    <style type="text/css">
                      * {
                        text-transform: capitalize;
                      }
                      body {
                        position: relative;
                        width: 21cm;
                        height: 29.7cm;
                        padding: 0;
                        margin: 0 auto;
                        color: #555;
                        background: #FFFFFF;
                        font-family: Arial, sans-serif;
                        font-size: 12px;
                        max-width: 785px !important;
                        max-height: 612px !important;
                      }
                      table {
                        width: 100%;
                      }
                      img {
                        width: 150px;
                        height: 150px;
                      }
                      p {
                        font-weight: bold;
                        text-transform: uppercase;
                      }
                      .title-hco {
                        font-size: 21px;
                        color: #33c4ee;
                        letter-spacing: -1px;
                        line-height: 1;
                        vertical-align: top;
                        text-align: right;
                        font-weight: lighter;
                      }
                      .border-bottom {
                        border-bottom: solid 2px #555;
                      }
                      .ligther{
                        font-weight: lighter !important;
                      }
                    </style>
                  </head>
                  <body>
                    <table>
                      <tr>
                        <td align="left"><img src="http://190.60.211.17/hco/'.$clinicLogo.'" alt="logo" /></td>
                        <td align="right">
                          <p class="title-hco">HCO</p>
                        </td>
                      </tr>
                    </table>
                    <table>
                      <tr>
                        <td align="left">
                          <p>'.$clinicName.'</p>
                          <p>'.$clinicNit.'</p>
                          <p>'.$clinicPhone.'</p>
                          <p>'.$clinicAddress.'</p>
                        </td>
                        <td align="right">
                          <p>'.$budgetDate.'</p>
                          <p>Presupuesto: <span class="ligther">'.$budgetCode.'</span></p>
                          <p>Paciente: <span class="ligther">'.$patientName.'</span></p>
                          <p>Documento: <span class="ligther">'.$patientCode.'</span></p>
                          <p></p>
                        </td>
                      </tr>
                    </table>
                    <hr>
                    <table>
                      <tr class="border-bottom">
                        <td align="left">
                          <p>Procedimiento</p>
                        </td>
                        <td>
                          <p>Precio</p>
                        </td>
                      </tr>
                      <tbody>
                        '.$templateBudgetDetails.'
                      </tbody>
                    </table>
                    <hr>
                    <div style="display:block;width100%;">
                      <table align="right" style="width: 250px;display:block;">
                        <tr align="right">
                          <td align="right"><p>Cuota inicial:</p></td>
                          <td align="right">'.$initial.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>N° de sessiones:</p></td>
                          <td align="right">'.$sessions.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Descuento:</p></td>
                          <td align="right">'.$discount.'%</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Total:</p></td>
                          <td align="right">'.$budgetTotal.'</td>
                        </tr>
                      </table>
                    </div>
                    '.$validateDes.'
                  </body>
                </html>';
    return $template;
  }
}
?>
