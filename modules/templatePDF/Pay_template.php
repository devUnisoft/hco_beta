<?php
class Pay_template{

  function payData($info, $budgets, $evolutions){
    $total = $evolutions['total'] + $budgets['total'];
    $total = number_format($total, 2, ',', '.');
    $templateInfo = $this->templateInfo($info, $total);
    $template = $this->template($templateInfo, $budgets['template'], $evolutions['template']);
    return $template;
  }

  function templateInfo($info, $total){
    extract($info);
    $date = date('d-m-Y');
    $template = '<table>
      <tr>
        <td align="left"><img src="http://190.60.211.17/hco/'.$clinicLogo.'" alt="logo" /></td>
        <td align="right">
          <p class="title-hco">HCO</p>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td align="left">
          <p>'.$clinicName.'</p>
          <p>'.$clinicNit.'</p>
          <p>'.$clinicPhone.'</p>
          <p>'.$clinicAddress.'</p>
        </td>
        <td align="right">
          <p>'.$date.'</p>
          <p>Paciente: <span class="ligther">'.$patientName.'</span></p>
          <p>Documento: <span class="ligther">'.$patientCode.'</span></p>
          <p>Saldo total: <span class="ligther">'.$total.'</span></p>
        </td>
      </tr>
    </table>
    <hr>';
    return $template;
  }

  function template($templateInfo, $templateBudgets, $templateEvolutions){
    $template = '<!DOCTYPE html>
                  <html lang="es">
                  <head>
                    <meta charset="UTF-8">
                    <style type="text/css">
                      * {
                        text-transform: capitalize;
                      }
                      body {
                        position: relative;
                        width: 21cm;
                        height: 29.7cm;
                        padding: 0;
                        margin: 0 auto;
                        color: #555;
                        background: #FFFFFF;
                        font-family: Arial, sans-serif;
                        font-size: 12px;
                        max-width: 785px !important;
                        max-height: 612px !important;
                      }
                      table {
                        width: 100%;
                      }
                      img {
                        width: 150px;
                        height: 150px;
                      }
                      p {
                        font-weight: bold;
                        text-transform: uppercase;
                      }
                      .title-hco {
                        font-size: 21px;
                        color: #33c4ee;
                        letter-spacing: -1px;
                        line-height: 1;
                        vertical-align: top;
                        text-align: right;
                        font-weight: lighter;
                      }
                      .border-bottom {
                        border-bottom: solid 2px #555;
                      }
                      .ligther{
                        font-weight: lighter !important;
                      }
                    </style>
                  </head>
                  <body>
                    '.$templateInfo.'
                    <div><p align="left">Tratamientos<p></div>
                    '.$templateBudgets.'
                    <div><p align="left">Consultas<p></div>
                    '.$templateEvolutions.'
                  </body>
                </html>';
    return $template;
  }
}
?>
