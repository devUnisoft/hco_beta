<?php
require '../libs/php/mpdf/mpdf.php';
class PDF {
  function __construct() {
    $this->validate($_POST['html']);
  }

  function validate($html){
    $mpdf = new mPDF('R','A4', 11,'Arial');
    $mpdf -> SetTitle('Presupuesto');
    $mpdf -> WriteHTML($html);
    $mpdf -> Output('NombreDeTuArchivo.pdf', 'I');
    exit;
  }
}

$PDF = new PDF();
?>
