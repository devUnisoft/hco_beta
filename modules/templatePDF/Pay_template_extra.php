<?php
class Pay_template {

  function payData($data){
    extract($data);
    $templateBudgets = $this->payTemplateBudgets($data);
    $template = $this->payTemplate($templateBudgets,$budgets[0]);
    return $templateBudgets;
  }

  function payTemplateDetails($budgetDetails, $id){
    $template = "";
    foreach ($budgetDetails as $row) {
      if($id == $row[0]['idBudget']){
          foreach ($row as $rows) {
            $template .= '<tr><td>'.$rows['process'].'</td><td>'.$rows['price'].'</td></tr>';
          }
      }
    }
    return $template;
  }

  function payTemplateBudgets($data){
    extract($data);
    $template = "";
    $date = date('d-m-Y');
    $count = 0;
    foreach ($budgets as $row) {
      $budgetDate = $row['budgetDate'];
      $budgetCode = $row['budgetCode'];
      $sessions = $row['sessions'];
      $discount = $row['discount'];
      $descripcion = $row['descripcion'];
      $id = $row['id'];
      $initial = $row['initial'];
      $budgetTotal = $row['budgetTotal'];

      if($count == 0){

        $clinicLogo = $row['clinicLogo'];
        $clinicName = $row['clinicName'];
        $clinicNit  = $row['clinicNit'];
        $clinicPhone = $row['clinicPhone'];
        $clinicAddress = $row['clinicAddress'];
        $patientName = $row['patientName'];
        $patientCode = $row['patientCode'];
        $clinicLogo = substr($clinicLogo, 2, strlen($clinicLogo));
        $count = 1;
        $template .= '<!DOCTYPE html>
                      <html lang="es">
                      <head>
                        <meta charset="UTF-8">
                        <style type="text/css">
                          * {
                            text-transform: capitalize;
                          }
                          body {
                            position: relative;
                            width: 21cm;
                            height: 29.7cm;
                            padding: 0;
                            margin: 0 auto;
                            color: #555;
                            background: #FFFFFF;
                            font-family: Arial, sans-serif;
                            font-size: 12px;
                            max-width: 785px !important;
                            max-height: 612px !important;
                          }
                          table {
                            width: 100%;
                          }
                          img {
                            width: 150px;
                            height: 150px;
                          }
                          p {
                            font-weight: bold;
                            text-transform: uppercase;
                          }
                          .title-hco {
                            font-size: 21px;
                            color: #33c4ee;
                            letter-spacing: -1px;
                            line-height: 1;
                            vertical-align: top;
                            text-align: right;
                            font-weight: lighter;
                          }
                          .border-bottom {
                            border-bottom: solid 2px #555;
                          }
                          .ligther{
                            font-weight: lighter !important;
                          }
                        </style>
                      </head>
                      <body>
                        <table>
                          <tr>
                            <td align="left"><img src="http://190.60.211.17/hco/'.$clinicLogo.'" alt="logo" /></td>
                            <td align="right">
                              <p class="title-hco">HCO</p>
                            </td>
                          </tr>
                        </table>
                        <table>
                          <tr>
                            <td align="left">
                              <p>'.$clinicName.'</p>
                              <p>'.$clinicNit.'</p>
                              <p>'.$clinicPhone.'</p>
                              <p>'.$clinicAddress.'</p>
                            </td>
                            <td align="right">
                              <p>'.$date.'</p>
                              <p>Paciente: <span class="ligther">'.$patientName.'</span></p>
                              <p>Documento: <span class="ligther">'.$patientCode.'</span></p>
                            </td>
                          </tr>
                        </table>
                        <hr>';
      }

      $template .= '<table>
                      <tr>
                        <td align="left">
                          <p> Fecha:'.$budgetDate.'</p>
                        </td>
                        <td align="right">
                          <p> Nombre:'.$budgetCode.'</p>
                        </td>
                      </tr>
                    </table>
                    <hr>
                    <table>
                      <tr class="border-bottom">
                        <td align="left">
                          <p>Procedimiento</p>
                        </td>
                        <td>
                          <p>Precio</p>
                        </td>
                      </tr>
                      <tbody>
                        '.$this->payTemplateDetails($budgetDetails, $id).'
                      </tbody>
                    </table>
                    <hr>
                    <div style="display:block;width100%;">
                      <table align="right" style="width: 250px;display:block;">
                        <tr align="right">
                          <td align="right"><p>Cuota inicial:</p></td>
                          <td align="right">'.$initial.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>N° de sessiones:</p></td>
                          <td align="right">'.$sessions.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Descuento:</p></td>
                          <td align="right">'.$discount.'%</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Total:</p></td>
                          <td align="right">'.$budgetTotal.'</td>
                        </tr>
                      </table>
                    </div>
                    <div style="display:block;width100%;">
                      <p>'.$descripcion.'</p>
                    </div>
                    <hr><hr>';
    }

    $template .= '</body></html>';
    return $template;

  }

  function payTemplateDetails($budgetDetails, $id){
    $template = "";
    foreach ($budgetDetails as $row) {
      if($id == $row[0]['idBudget']){
          foreach ($row as $rows) {
            $template .= '<tr><td>'.$rows['process'].'</td><td>'.$rows['price'].'</td></tr>';
          }
      }
    }
    return $template;
  }

  function payTemplate($budget){
    $template = '<table id="pays-history-table" class="table table-bordered table-hover" style="width: 100%;">
      <thead>
        <tr>
          <th align="center">Fecha</th>
          <th align="center">Cuota inicial</th>
          <th align="center">Metodo de pago</th>
          <th align="center">Valor</th>
          <th align="center">codigo del banco</th>
          <th align="center">numero de la cuenta</th>
          <th align="center">fecha de consignacion</th>
          <th align="center">Observaciones</th>
          <th align="center">Acción</th>
        </tr>
      </thead>
      <tbody id="pays-history-data">
      '..'
      </tbody>
    </table>';
  }
  function payTemplate($templateB, $budget){
    extract($budget);
    $clinicLogo = substr($clinicLogo, 2, strlen($clinicLogo));
    $date = date('d-m-Y');
    $template = '<!DOCTYPE html>
                  <html lang="es">
                  <head>
                    <meta charset="UTF-8">
                    <style type="text/css">
                      * {
                        text-transform: capitalize;
                      }
                      body {
                        position: relative;
                        width: 21cm;
                        height: 29.7cm;
                        padding: 0;
                        margin: 0 auto;
                        color: #555;
                        background: #FFFFFF;
                        font-family: Arial, sans-serif;
                        font-size: 12px;
                        max-width: 785px !important;
                        max-height: 612px !important;
                      }
                      table {
                        width: 100%;
                      }
                      img {
                        width: 150px;
                        height: 150px;
                      }
                      p {
                        font-weight: bold;
                        text-transform: uppercase;
                      }
                      .title-hco {
                        font-size: 21px;
                        color: #33c4ee;
                        letter-spacing: -1px;
                        line-height: 1;
                        vertical-align: top;
                        text-align: right;
                        font-weight: lighter;
                      }
                      .border-bottom {
                        border-bottom: solid 2px #555;
                      }
                      .ligther{
                        font-weight: lighter !important;
                      }
                    </style>
                  </head>
                  <body>
                    <table>
                      <tr>
                        <td align="left"><img src="http://190.60.211.17/hco/'.$clinicLogo.'" alt="logo" /></td>
                        <td align="right">
                          <p class="title-hco">HCO</p>
                        </td>
                      </tr>
                    </table>
                    <table>
                      <tr>
                        <td align="left">
                          <p>'.$clinicName.'</p>
                          <p>'.$clinicNit.'</p>
                          <p>'.$clinicPhone.'</p>
                          <p>'.$clinicAddress.'</p>
                        </td>
                        <td align="right">
                          <p>'.$date.'</p>
                          <p>Paciente: <span class="ligther">'.$patientName.'</span></p>
                          <p>Documento: <span class="ligther">'.$patientCode.'</span></p>
                        </td>
                      </tr>
                    </table>
                    <hr>
                    '.$templateB.'
                  </body>
                </html>';
    return $template;
  }
}
?>
