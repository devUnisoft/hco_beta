<?php
class Pay_receipt_template {

  function payData($info, $data, $type){
    extract($data);
    $templateDetail;
    if($type == 1){
        $templateDetail = $this->TemplateDetailBudget($budgets);
    }
    else{
        $templateDetail = $this->TemplateDetailEvolution($evolutions);
    }

    // $total
    // $abono
    // $saldo
    $template = $this->Template($info, $templateDetail, $pays);
    return $template;
  }

  function TemplateDetailBudget($data){
    $template = "";
    foreach ($data as $detail) {
      $template .= '<tr>
                      <td>'.$detail['dateSave'].'</td>
                      <td>'.$detail['name'].'</td>
                      <td>'.number_format(intval($detail['total']),2,",",".") .'</td>
                      <td>'.$detail['discount'].'</td>
                      <td>'.$detail['sessions'].'</td>
                      <td>'.number_format(intval($detail['initial']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['valorCuota']),2,",",".").'</td>
                      <td>'.$detail['profesional'].'</td>
                      <td>'.number_format(intval($detail['totalAbonos']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['saldo']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['saldomora']),2,",",".").'</td>
                    </tr>';
    }

    $template2 = '<table>
                    <thead>
                      <tr>
                        <th align="center">Fecha</th>
                        <th align="center">Tratamiento</th>
                        <th align="center">Valor total</th>
                        <th align="center">Descuento</th>
                        <th align="center">Cuotas</th>
                        <th align="center">Cuota Inicial</th>
                        <th align="center">Valor cuota fija</th>
                        <th align="center">Profesional</th>
                        <th align="center">Total abonos</th>
                        <th align="center">Saldo</th>
                        <th align="center">Saldo en mora</th>
                      </tr>
                    </thead>
                    <tbody>'.$template.'</tbody>
                  </table>';

    return $template2;
  }

  function TemplateDetailEvolution($data){
    $template = "";
    foreach ($data as $detail) {

      $template .= '<tr>
                      <td>'.$detail['dateSave'].'</td>
                      <td>'.$detail['name'].'</td>
                      <td>'.number_format(intval($detail['total']),2,",",".").'</td>
                      <td>'.$detail['discount'].'</td>
                      <td>'.$detail['sessions'].'</td>
                      <td>'.number_format(intval($detail['initial']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['valorCuota']),2,",",".").'</td>
                      <td>'.$detail['profesional'].'</td>
                      <td>'.number_format(intval($detail['totalAbonos']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['saldo']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['saldomora']),2,",",".").'</td>
                    </tr>';

      $template .= '<tr>
                      <td>'.$detail['date'].'</td>
                      <td>'.$detail['idTypeEvolution'].'</td>
                      <td>'.$detail['idEvolutionTypeProcess'].'</td>
                      <td>'.$detail['idEvolutionDiagnostic'].'</td>
                      <td>'.$detail['idOriginDisease'].'</td>
                      <td>'.$detail['idProfesional'].'</td>
                      <td>'.number_format(intval($detail['price']),2,",",".").'</td>
                      <td>'.$detail['discount'].'</td>
                      <td>'.number_format(intval($detail['Abonos']),2,",",".").'</td>
                      <td>'.number_format(intval($detail['saldo']),2,",",".").'</td>
                      <td>'.$detail['observations'].'</td>
                    </tr>';
    }

    $template2 = '<table>
                    <thead>
                      <tr>
                        <th align="center">Fecha</th>
                        <th align="center">Tipo de consulta</th>
                        <th align="center">Fecha de Esterilización</th>
                        <th align="center">Tipo de procedimiento</th>
                        <th align="center">Diagnóstico principal</th>
                        <th align="center">Origen de la enfermedad actual</th>
                        <th align="center">Profesional</th>
                        <th align="center">Valor total</th>
                        <th align="center">Descuento</th>
                        <th align="center">Abonos</th>
                        <th align="center">Saldo</th>
                      </tr>
                    </thead>
                    <tbody>'.$template.'</tbody>
                  </table>';

    return $template;
  }

  function TemplateDetailPay($data){
    $template;
    $count = 0;
    foreach ($data as $detail) {
      if(count($data)-1 == $count){
        $template['methodName'] = $detail['methodName'];
        $template['rode'] = $detail['rode'];
        $template['id'] = $detail['id'];
      }
      $count++;
    }
    return $template;
  }


  function Template($info, $templateDetail, $templatePays){
    extract($info);
    $clinicLogo = substr($clinicLogo, 2, strlen($clinicLogo));
    $template = '<!DOCTYPE html>
                  <html lang="es">
                  <head>
                    <meta charset="UTF-8">
                    <style type="text/css">
                      * {
                        text-transform: capitalize;
                      }
                      body {
                        position: relative;
                        width: 21cm;
                        height: 29.7cm;
                        padding: 0;
                        margin: 0 auto;
                        color: #555;
                        background: #FFFFFF;
                        font-family: Arial, sans-serif;
                        font-size: 12px;
                        max-width: 785px !important;
                        max-height: 612px !important;
                      }
                      table {
                        width: 100%;
                      }
                      img {
                        width: 150px;
                        height: 150px;
                      }
                      p {
                        font-weight: bold;
                        text-transform: uppercase;
                      }
                      .title-hco {
                        font-size: 21px;
                        color: #33c4ee;
                        letter-spacing: -1px;
                        line-height: 1;
                        vertical-align: top;
                        text-align: right;
                        font-weight: lighter;
                      }
                      .border-bottom {
                        border-bottom: solid 2px #555;
                      }
                      .ligther{
                        font-weight: lighter !important;
                      }
                    </style>
                  </head>
                  <body>
                    <table>
                      <tr>
                        <td align="left"><img src="http://190.60.211.17/hco/'.$clinicLogo.'" alt="logo" /></td>
                        <td align="right">
                          <p class="title-hco">HCO</p>
                        </td>
                      </tr>
                    </table>
                    <table>
                      <tr>
                        <td align="left">
                          <p>'.$clinicName.'</p>
                          <p>'.$clinicNit.'</p>
                          <p>'.$clinicPhone.'</p>
                          <p>'.$clinicAddress.'</p>
                        </td>
                        <td align="right">
                          <p>Paciente: <span class="ligther">'.$patientName.'</span></p>
                          <p>Documento: <span class="ligther">'.$patientCode.'</span></p>
                          <p></p>
                        </td>
                      </tr>
                    </table>
                    <hr>
                    '.$templateDetail.'
                    <hr>
                    <table>
                      <tr>
                        <td align="left">
                          <p>Forma de pago: '.$templatePays['methodName'].'</p>
                          <p></p>
                          <br>
                          <br>
                          <p>_______________________________________</p>
                          <p>Elaboro</p>
                        </td>
                        <td align="right">
                          <p>Valor abonado: '.$templatePays['rode'].'</p>
                          <p></p>
                          <br>
                          <br>
                          <p>_______________________________________</p>
                          <p>Firma de cliente</p>
                        </td>
                      </tr>
                    </table>
                  </body>
                </html>';
    return $template;
  }
}
?>
