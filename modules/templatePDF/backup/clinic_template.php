<?php
class clinic_template {

  function clinicTemplate($clinic){
    extract($clinic);
    $clinicLogo = substr($clinicLogo, 2, strlen($clinicLogo));
    $template = '<!DOCTYPE html>
                  <html lang="es">
                  <head>
                    <meta charset="UTF-8">
                    <style type="text/css">
                      * {
                        text-transform: capitalize;
                      }
                      body {
                        position: relative;
                        width: 21cm;
                        height: 29.7cm;
                        padding: 0;
                        margin: 0 auto;
                        color: #555;
                        background: #FFFFFF;
                        font-family: Arial, sans-serif;
                        font-size: 12px;
                        max-width: 785px !important;
                        max-height: 612px !important;
                      }
                      table {
                        width: 100%;
                      }
                      img {
                        width: 150px;
                        height: 150px;
                      }
                      p {
                        font-weight: bold;
                        text-transform: uppercase;
                      }
                      .title-hco {
                        font-size: 21px;
                        color: #33c4ee;
                        letter-spacing: -1px;
                        line-height: 1;
                        vertical-align: top;
                        text-align: right;
                        font-weight: lighter;
                      }
                      .border-bottom {
                        border-bottom: solid 2px #555;
                      }
                      .ligther{
                        font-weight: lighter !important;
                      }
                    </style>
                  </head>
                  <body>
                    <table>
                      <tr>
                        <td align="left"><img src="http://190.60.211.17/hco/'.$imagen.'" alt="logo" /></td>
                        <td align="right">
                          <p class="title-hco">HCO</p>
                        </td>
                      </tr>
                    </table>
                    <table>
                      <tr>
                        <td align="left">
                          <p>Razón social: '.$razonsocial.'</p>
                          <p>Nit: '.$nit.'</p>
                          <p>Dirección: '.$direccion.'</p>
                          <p>Teléfono: '.$telefono.'</p>
                          <p>Correo electrónico: '.$email.'</p>

                        </td>
                        <td align="right">
                          <p>Persona de contacto: '.$clinicAddress.'</p>
                          <p>Identificación: <span class="ligther">'.$budgetCode.'</span></p>
                          <p>Código entidad: <span class="ligther">'.$patientName.'</span></p>
                          <p>Código prestador: <span class="ligther">'.$patientCode.'</span></p>
                          <p>Código administrador:</p>
                          <p>Nombre Administrador:</p>
                        </td>
                      </tr>
                    </table>
                    <hr>
                    <table>
                      <tr class="border-bottom">
                        <td align="left">
                          <p>Procedimiento</p>
                        </td>
                        <td>
                          <p>Precio</p>
                        </td>
                      </tr>
                      <tbody>
                        '.$templateBudgetDetails.'
                      </tbody>
                    </table>
                    <hr>
                    <div style="display:block;width100%;">
                      <table align="right" style="width: 250px;display:block;">
                        <tr align="right">
                          <td align="right"><p>Cuota inicial:</p></td>
                          <td align="right">'.$initial.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>N° de sessiones:</p></td>
                          <td align="right">'.$sessions.'</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Descuento:</p></td>
                          <td align="right">'.$discount.'%</td>
                        </tr>
                        <tr align="right">
                          <td align="right"><p>Total:</p></td>
                          <td align="right">'.$budgetTotal.'</td>
                        </tr>
                      </table>
                    </div>
                    '.$validateDes.'
                  </body>
                </html>';
    return $template;
  }
}
?>
