<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Providers_model.php';
class Providers_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Providers_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($providerSearch)){
      $this->providerSearch();
    }
    else if(isset($forProduct)){
      $this->forProduct();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($update)){
      $this->update();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function providerSearch(){
    $res = $this->model->providerSearch($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function forProduct(){
    $res = $this->model->forProduct($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }


}

$controller = new Providers_controller();

?>
