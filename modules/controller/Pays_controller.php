<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Pays_model.php';
class Pays_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Pays_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($filter)){
      $this->filter();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($update)){
      $this->update();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($generatePDF)){
      $this->generatePDF();
    }
    else if(isset($cards)){
      $this->cards();
    }
    else if(isset($evolutions)){
      $this->evolutions();
    }
    else if(isset($filterEvolutions)){
      $this->filterEvolutions();
    }
    else if(isset($methodsPays)){
      $this->methodsPays();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function filter(){
    $res = $this->model->filter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function cards(){
    $res = $this->model->cards($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function evolutions(){
    $res = $this->model->evolutions($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function filterEvolutions(){
    $res = $this->model->filterEvolutions($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function methodsPays(){
    $res = $this->model->methodsPays($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function generatePDF(){
    $res['theme'] = $this->model->generatePDF($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Pays_controller();
?>
