<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Rips_model.php';
class Rips_controller {

  public $helper;
  public $model;
  public $response;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Rips_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($filter)){
      $this->usuarios();
      $this->transaccion();
      $this->consulta();
      $this->procedimientos();
      $this->control();
      $this->urgencias();
      $this->otherServices();
      $this->medicamentos();
      $this->recienNacido();
      $this->hospitalizacion();
      $this->helper->printJSON($this->response);
    }
  }

  function usuarios(){
    $res = $this->model->usuarios($this->helper->gather($_POST));
    $this->generateDocument($res, 'US');

  }

  function transaccion(){
    $res = $this->model->transaccion($this->helper->gather($_POST));
    $this->generateDocument($res, 'AF');
  }

  function consulta(){
    $res = $this->model->consulta($this->helper->gather($_POST));
    $this->generateDocument($res, 'AC');
  }

  function procedimientos(){
    $res = $this->model->procedimientos($this->helper->gather($_POST));
    $this->generateDocument($res, 'AP');
  }

  function control(){
    $res = $this->model->control($this->helper->gather($_POST));
    $this->generateDocument($res, 'CO');
  }

  function urgencias(){
    $res = $this->model->urgencias($this->helper->gather($_POST));
    $this->generateDocument($res, 'UR');
  }

  function otherServices(){
    $res = $this->model->otherServices($this->helper->gather($_POST));
    $this->generateDocument($res, 'OS');
  }

  function medicamentos(){
    $res = $this->model->medicamentos($this->helper->gather($_POST));
    $this->generateDocument($res, 'ME');
  }

  function recienNacido(){
    $res = $this->model->recienNacido($this->helper->gather($_POST));
    $this->generateDocument($res, 'RN');
  }

  function hospitalizacion(){
    $res = $this->model->hospitalizacion($this->helper->gather($_POST));
    $this->generateDocument($res, 'HO');
  }

  function generateDocument($res, $type){
    //fecha actual
    $fecha_actual=date("Y");
    $fecha_hora=date("Y-m-d h:i:s");
    //saco el semestre para enviarlo en el nombre del txt
    $mes = date("m",strtotime($fecha_hora));
    $mes = is_null($mes) ? date('m') : $mes;
    $trim = floor(($mes-1) / 6)+1;
    $title = $type.''.$fecha_actual.'-'.$trim.'.txt';

    //tipos de header a enviar
    // header("Pragma: public");
    // header("Expires: 0");
    // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    // header("Content-Type: application/force-download");
    // header("Content-Type: application/octet-stream");
    // header("Content-Type: application/download");
    // header("Content-Transfer-Encoding: binary ");
    // header('Content-disposition: attachment; filename="'.$title.'"');
    // echo $res;
    $data['title'] = $title;
    $data['data'] = $res;
    $this->response[] = $data;
  }
}

$controller = new Rips_controller();
?>
