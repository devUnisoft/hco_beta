<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Calendar_model.php';
class Calendar_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Calendar_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    
    $this->helper->printJSON($res);
  }
}

$controller = new Calendar_controller();
?>
