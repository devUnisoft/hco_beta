<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Reports_model.php';
class Reports_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Reports_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($tratamientos)){
      $this->tratamientos();
    }
    else if(isset($tratamientosFilter)){
      $this->tratamientosFilter();
    }
    else if(isset($pagoProfesionales)){
      $this->pagoProfesionales();
    }
    else if(isset($pagoProfesionalesFilter)){
      $this->pagoProfesionalesFilter();
    }
    else if(isset($consultadiaFilter)){
      $this->consultadiaFilter();
    }
    else if(isset($consultadia)){
      $this->consultadia();
    }
    else if(isset($cajaFilter)){
      $this->cajaFilter();
    }
    else if(isset($caja)){
      $this->caja();
    }else if(isset($reporteCitas)){
       $this->reporteCitas();
    }

  }

  function tratamientos(){
    $res = $this->model->tratamientos($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function tratamientosFilter(){
    $res = $this->model->tratamientosFilter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function pagoProfesionales(){
    $res = $this->model->pagoProfesionales($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function pagoProfesionalesFilter(){
    $res = $this->model->pagoProfesionalesFilter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function consultadiaFilter(){
    $res = $this->model->consultadiaFilter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function consultadia(){
    $res = $this->model->consultadia($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function cajaFilter(){
    $res = $this->model->cajaFilter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function caja(){
    $res = $this->model->caja($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function reporteCitas(){
    $res = $this->model->reporteCitas($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Reports_controller();

?>
