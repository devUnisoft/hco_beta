<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Products_model.php';
class Products_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Products_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($select)){
      $this->select();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($productForCategory)){
      $this->productForCategory();
    }
    else if(isset($categories)){
      $this->categories();
    }
    else if(isset($updateCategory)){
      $this->updateCategory();
    }
    else if(isset($subCategories)){
      $this->subcategories();
    }
    else if(isset($updateSubCategory)){
      $this->updateSubCategory();
    }
    else if(isset($update)){
      $this->update();
    }
    else if(isset($updateproductAdd)){
      $this->updateproductAdd();
    }
    else if(isset($updateproductMinus)){
      $this->updateproductMinus();
    }
    else if(isset($validateName)){
      $this->validateName();
    }

  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
  function select(){
    $res = $this->model->select($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function productForCategory(){
    $res = $this->model->productForCategory($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function categories(){
    $res = $this->model->categories($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateCategory(){
    $res = $this->model->updateCategory($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function subcategories(){
    $res = $this->model->subcategories($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateSubCategory(){
    $res = $this->model->updateSubCategory($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateproductAdd(){
    $res = $this->model->updateproductAdd($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateproductMinus(){
    $res = $this->model->updateproductMinus($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function validateName(){
    $res = $this->model->validateName($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

}

$controller = new Products_controller();

?>
