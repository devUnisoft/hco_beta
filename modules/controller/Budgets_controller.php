<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Budgets_model.php';
class Budgets_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Budgets_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($autorize)){
      $this->autorize();
    }
    else if(isset($filter)){
      $this->filter();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($update)){
      $this->update();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($changeToPay)){
      $this->changeToPay();
    }
    else if(isset($templatePDF)){
      $this->templatePDF();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function autorize(){
    $res = $this->model->autorize($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function filter(){
    $res = $this->model->filter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function changeToPay(){
    $res = $this->model->changeToPay($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function templatePDF(){
    $data = $this->model->templatePDF($this->helper->gather($_POST));
    $this->helper->printJSON($data);
  }

}

$controller = new Budgets_controller();
?>
