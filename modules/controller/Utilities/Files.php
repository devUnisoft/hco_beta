<?php
class Files
{
	public function createDir($directorio)
	{
		try {
			if (!file_exists($directorio)){
				if(!mkdir($directorio, 0777, true)){
	                    return false; //fallo al crear la carpeta
	                }
	            else{return $directorio; /*se creo carpeta correctamente*/}
	        }
	    else{return $directorio; /*ya existe a carpeta*/}
	}
	catch(PDOException $e){print "Error!: " . $e->getMessage();}
}

public function getExtension($str) {
     $i = strrpos($str,".");
     if (!$i) { return ""; }
     $l = strlen($str) - $i;
     $ext = substr($str,$i+1,$l);
     return $ext;
}

public function getExtension2($str) {
         if($str == 'image/jpg'){
         	$str = 'jpg';
         }
         if($str == 'image/jpeg'){
         	$str = 'jpg';
         }
         if($str == 'image/gif'){
         	$str = 'gif';
         }
         if($str == 'image/png'){
         	$str = 'png';
         }
         return $str;
}

public function validaImage($archivo, $directorio){
	try {
		$resultados = [];
			# MIME types permitidos
		$mime = array('image/jpg', 'image/pjpeg', 'image/jpeg', 'image/gif', 'image/png');
			# Buscamos si el archivo que subimos tiene el MIME type que permitimos en nuestra subida

		if($_FILES[$archivo]['size'] < 6000000)
		{
			if(in_array($_FILES[$archivo]['type'], $mime ))
			{

		    $extension = $this->getExtension2($_FILES[$archivo]['type']);
				//$extension = ".jpg";//pathinfo($_FILES[$archivo]['name'], PATHINFO_EXTENSION);
				$newImage = date('YmdHis').".".$extension;
				//se establece directorio
				$directorio = $this->createDir($directorio);
				$directorio = $directorio.$newImage;
				//movemos el archivo
				if (move_uploaded_file($_FILES[$archivo]['tmp_name'],$directorio))
				{
					list($width,$height)=getimagesize($directorio);
					$R = new ResizeImage($directorio);
					$R->resizeTo($width*0.5, $height*0.5, 'default');
					$R->saveImage($directorio);
					$resultados['success'] = true;
					$resultados['imagen'] = $newImage;
				}
				else{$resultados['error'] = false; $resultados['Msg'] = 'Ups!, ha ocrrido un error. intentalo de nuevo.';}
			}else{$resultados['error'] = false; $resultados['Msg'] = 'Formato de imagen valido';}
		}else{$resultados['error'] = false; $resultados['Msg'] = 'La imagen es muy pesada porfavor sube imagenes menores a 6 MB.';}

		return $resultados;
	}catch(PDOException $e){print "Error!: " . $e->getMessage();}
}

public function validaFiles($archivo, $directorio){
	try {
		$resultados = [];
			# MIME types permitidos
		$mime = array('application/msword', 'application/pdf');
			# Buscamos si el archivo que subimos tiene el MIME type que permitimos en nuestra subida

		if($_FILES[$archivo]['size'] < 6000000)
		{
			if(in_array($_FILES[$archivo]['type'], $mime ))
			{
				$extension = pathinfo($_FILES[$archivo]['name'], PATHINFO_EXTENSION);
				$newImage = date('YmdHis').".".$extension;
					//se establece directorio
				$directorio = $this->createDir($directorio);
				$directorio = $directorio.$newImage;
					// return $directorio;
					// return var_dump($_FILES[$archivo]);
					//movemos el archivo
				if (move_uploaded_file($_FILES[$archivo]['tmp_name'],$directorio))
				{
					$resultados['success'] = true;
					$resultados['documento'] = $newImage;
				}
				else{$resultados['error'] = false; $resultados['Msg'] = 'Ups!, ha ocrrido un error. intentalo de nuevo.';}
			}else{$resultados['error'] = false; $resultados['Msg'] = 'Formato de imagen valido';}
		}else{$resultados['error'] = false; $resultados['Msg'] = 'La imagen es muy pesada porfavor sube imagenes menores a 6 MB.';}

		return $resultados;
	}catch(PDOException $e){print "Error!: " . $e->getMessage();}
}

public function validaExcel($archivo, $directorio){
	try {
		$resultados = [];
			# MIME types permitidos
		$mime = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			# Buscamos si el archivo que subimos tiene el MIME type que permitimos en nuestra subida

		if($_FILES[$archivo]['size'] < 6000000)
		{
			if(in_array($_FILES[$archivo]['type'], $mime ))
			{
				$excel = date('YmdHis').".xlsx";
				//se establece directorio
				$directorio = $this->createDir($directorio);
				$directorio = $directorio.$excel;
				//movemos el archivo
				if (move_uploaded_file($_FILES[$archivo]['tmp_name'],$directorio))
				{
					$resultados['success'] = true;
					$resultados['excel'] = $excel;
				}
				else{$resultados['error'] = false; $resultados['Msg'] = 'Ups!, ha ocrrido un error. intentalo de nuevo.';}
			}else{$resultados['error'] = false; $resultados['Msg'] = 'Formato no valido';}
		}else{$resultados['error'] = false; $resultados['Msg'] = 'El documento es muy pesada porfavor sube un documento menor a 6 MB.';}

		return $resultados;
	}catch(PDOException $e){print "Error!: " . $e->getMessage();}
}
}
?>
