<?php
class Utilities {
  public function gather($POST){
    try {
      $data;
      foreach($POST as $name => $value){
        $data[$name] = $value;
      }
      return $data;
    }
    catch(PDOException $e){print "Error!: " . $e->getMessage();}
  }

  public function printJSON($array)
  {
      //header('Content-type: application/json');
      echo json_encode($array);
  }
}
