<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Ordersreports_model.php';
class Ordersreports_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Ordersreports_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($filter)){
      $this->filter();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function filter(){
    $res = $this->model->filter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Ordersreports_controller();

?>
