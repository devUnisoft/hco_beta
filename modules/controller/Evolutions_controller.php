<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Evolutions_model.php';
class Evolutions_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Evolutions_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($update)){
      $this->update();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($typeConsult)){
      $this->typeConsult();
    }
    else if(isset($process)){
      $this->process();
    }
    else if(isset($diagnostics)){
      $this->diagnostics();
    }
    else if(isset($originDisease)){
      $this->originDisease();
    }
    else if(isset($priceCups)){
      $this->priceCups();
    }

  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function typeConsult(){
    $res = $this->model->typeConsult($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function process(){
    $res = $this->model->process($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function diagnostics(){
    $res = $this->model->diagnostics($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function originDisease(){
    $res = $this->model->originDisease($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function priceCups(){
    $res = $this->model->priceCups($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

}

$controller = new Evolutions_controller();
?>
