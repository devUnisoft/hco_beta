<?php
require_once 'Utilities/Utilities.php';
require_once '../models/cronjob.php';
class cronjob {

   public $helper;
   public $model;

   function __construct() {
     $this->helper = new Utilities();
     $this->model  = new cronjob_model();
     $data = $this->helper->gather($_POST);
     $this->orderautomatic($data);
   }

   function orderautomatic($data){
     $res = $this->model->orderautomatic($data);
     $this->helper->printJSON($res);
   }
}

$controller = new cronjob();

?>
