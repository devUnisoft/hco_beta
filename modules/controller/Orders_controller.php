<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Orders_model.php';
class Orders_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Orders_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($filter)){
      $this->filter();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($recived)){
      $this->recived();
    }
    else if(isset($RecivedParcial)){
      $this->RecivedParcial();
    }
    else if(isset($addRecived)){
      $this->addRecived();
    }
    else if(isset($update)){
      $this->update();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function filter(){
    $res = $this->model->filter($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function recived(){
    $res = $this->model->recived($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function RecivedParcial(){
    $res = $this->model->RecivedParcial($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function addRecived(){
    $res = $this->model->addRecived($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }


}

$controller = new Orders_controller();

?>
