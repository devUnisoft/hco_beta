<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Params_model.php';
class Params_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Params_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($all)){
      $this->all();
    }
    else if(isset($item)){
      $this->item();
    }
    else if(isset($update)){
      $this->update();
    }
    else if(isset($delete)){
      $this->delete();
    }
    else if(isset($specialties)){
      $this->specialties();
    }
    else if(isset($updateSpecialty)){
      $this->updateSpecialty();
    }
    else if(isset($process)){
      $this->process();
    }
    else if(isset($updateProcess)){
      $this->updateProcess();
    }
    else if(isset($itemProcess)){
      $this->itemProcess();
    }
    else if(isset($type)){
      $this->validateName();
    }

  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function item(){
    $res = $this->model->item($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function delete(){
    $res = $this->model->delete($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function specialties(){
    $res = $this->model->specialties($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateSpecialty(){
    $res = $this->model->updateSpecialty($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function process(){
    $res = $this->model->process($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateProcess(){
    $res = $this->model->updateProcess($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function itemProcess(){
    $res = $this->model->itemProcess($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function validateName(){
    $res = $this->model->validateName($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Params_controller();

?>
