<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Patient_model.php';
class Patient_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Patient_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($update)){
      $this->update();
    }
    if(isset($all)){
      $this->all();
    }
    if(isset($updateBudget)){
      $this->updateBudget();
    }
  }

  function all(){
    $res = $this->model->all($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function update(){
    $res = $this->model->update($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }

  function updateBudget(){
    $data = $this->helper->gather($_POST);
    extract($data);
    $res = $this->model->updateBudget($idBudget, $idUser);
    $this->helper->printJSON($res);
  }
}

$controller = new Patient_controller();
?>
