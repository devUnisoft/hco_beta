<?php
require_once 'Utilities/Utilities.php';
require_once 'Utilities/Files.php';
require_once '../models/Backup_model.php';
class Backup_controller {

  public $helper;
  public $model;

  function __construct() {
    $this->helper = new Utilities();
    $this->model  = new Backup_model();
    $this->validate();
  }

  function validate(){
    $data = $this->helper->gather($_POST);
    extract($data);
    if(isset($backup)){
      $this->backup();
    }
  }

  function backup(){

    $res = $this->model->backupDatabaseTables('localhost','root','usc2017*','hco');
    //$res = $this->model->backup($this->helper->gather($_POST));
    $this->helper->printJSON($res);
  }
}

$controller = new Backup_controller();
?>
