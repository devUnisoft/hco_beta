/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */

  $( document ).ready(function() {

  });

  $(window).on("load", function (e) {
    routerPages()
  })

  function activeCronJobOrderautomatic(){
    let data = new FormData()
    let url = 'cronjob.php'
    postX(url, data).then(response => {
      console.log("Success!", response);

    }, error => {
      console.error("Failed!", error);
    })
  }

  function routerPages(){
    if(getNameURLWeb() == 'inventario.php'){
      productsRouter()
      activeCronJobOrderautomatic();
    }
    else if(getNameURLWeb() == 'proveedores.php'){
      providersRouter()
    }
    else if(getNameURLWeb() == 'pedidos.php'){
      orderRouter()
    }
    else if(getNameURLWeb() == 'pedidos_reportes.php'){
      reportsRouter()
    }
    else if(getNameURLWeb() == 'parametros.php'){
      paramsRouter()
    }
    else if(getNameURLWeb() == 'presupuestos.php'){
      budgetsRouter()
    }


  }
