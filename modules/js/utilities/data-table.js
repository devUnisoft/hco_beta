/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */


function dataTable(table, order){
  $(table).DataTable({
      responsive: true,
      "language": {
        "url": "../../../libs/datatables/Spanish.json"
      },
      "bDestroy": true,
      dom: 'Bfrtip',
      buttons: [
          'excel', 'pdf', 'print'
      ],
      "order": [[ order, "desc" ]]
    });
}
