/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */
function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
}

function postX(url, data){
   // Return a new promise.
   return new Promise(function(resolve, reject) {

     if(self.fetch) {
       // ejecutar petición fetch
       var headersOptions = {
           'Content-Type': 'application/json'
       }
       var headers = new Headers(headersOptions);

       var params = {
                  method: 'POST',
                  mode: 'cors',
                  cache: 'default'
       };

       //params.headers = headers;

       data.append('clinicaName', getNameClinica())
       data.append('idReceptor', getReceptor())
       params.body = data;

       var request = new Request(getMainPath()+url, params);
       fetch(request)
       .then(status)
       .then(res => res.json())
       .then(data => {
         //console.log('Request succeeded with JSON response', data);
         // Resolve the promise with the response text
         resolve(data);

       }).catch(error => {
         //console.log('Request failed', error);
         reject(Error('Request failed', error));
       });
     }
     else {
         // ¿hacer algo con XMLHttpRequest?
     }
  });
}


function sinConexion(err){
  console.log(err)
  message("Detectamos que posees una conexion inestable de internet.")
}


function openWindowWithPostRequest(resp, winURL) {
  var winName='MyWindow';
  var windowoption='resizable=yes,fullscreen=yes,location=0,menubar=0,scrollbars=1';
  var params = { 'html' : resp};
  var form = document.createElement("form");
  form.setAttribute("method", "post");
  form.setAttribute("action", winURL);
  form.setAttribute("target",winName);
  for (var i in params) {
    if (params.hasOwnProperty(i)) {
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = i;
      input.value = params[i];
      form.appendChild(input);
    }
  }
  document.body.appendChild(form);
  window.open('', winName,windowoption);
  form.target = winName;
  form.submit();
  document.body.removeChild(form);
  if($("#pacienteEmail1").val() == 1){
    setTimeout(function(){ redirect('../../../../menu.php'); }, 1000);
  }
  else{
    reload();
  }
}
