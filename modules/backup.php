<?php
header_remove('Access-Control-Allow-Origin');
header('Access-Control-Allow-Origin: *');
ini_set('max_execution_time', 30000000);
setlocale(LC_ALL, 'es_CO');
setlocale(LC_TIME, 'es_CO');
setlocale(LC_MONETARY, 'es_CO');
date_default_timezone_set('America/Bogota');
ini_set('memory_limit', '512M');
ini_set('display_errors', true);
error_reporting(E_ALL);

/**
 * @function    backupDatabaseTables
 * @usage       Backup database tables and save in SQL file
 */

function backupDatabaseTables($dbHost,$dbUsername,$dbPassword,$dbName,$tables = '*'){
    //connect & select the database
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

    //get all of the tables
    if($tables == '*'){
        $tables = array();
        $result = $db->query("SHOW TABLES");
        while($row = $result->fetch_row()){
            $tables[] = $row[0];
        }
    }else{
        $tables = is_array($tables)?$tables:explode(',',$tables);
    }

    $return = "";

    //loop through the tables
    foreach($tables as $table){
        $result = $db->query("SELECT * FROM $table");
        $numColumns = mysqli_num_fields($result);

        //$return .= "DROP TABLE $table;";

        $result2 = $db->query("SHOW CREATE TABLE $table");
        $row2;
        /* obtener el array asociativo */
         while ($row2 = mysqli_fetch_row($result2)) {

         }

        $row2;

        $return .= "\n\n".$row2[1].";\n\n";
        //$row[$j] = preg_replace("\n","\\n",$row[$j]);
        for($i = 0; $i < $numColumns; $i++){
            while($row = $result->fetch_row()){
                $return .= "INSERT INTO $table VALUES(";
                for($j=0; $j < $numColumns; $j++){
                    $row[$j] = addslashes($row[$j]);
                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
                    if ($j < ($numColumns-1)) { $return.= ','; }
                }
                $return .= ");\n";
            }
        }

        $return .= "\n\n\n";
    }

    //save file
    $nameFile = 'backups/db-backup-'.date('Y-m-d_H:i:s').'.sql';
    $handle = fopen($nameFile,'w+');
    fwrite($handle,$return);
    fclose($handle);

    header('Location: http://190.60.211.17/hcobeta/modules/'.$nameFile);
    //return $return; http://190.60.211.17/hcobeta/modules/backup.php
}

// $tables[] = 'analisisatm';
// $tables[] = 'analisisfacial';
// $tables[] = 'analisisintraoral';
// $tables[] = 'anamnesis';
// $tables[] = 'antecedentesmedicos';
// $tables[] = 'antecedentesodontologicos';
// $tables[] = 'appointments';
// $tables[] = 'calendar';
// $tables[] = 'clinicas';
// $tables[] = 'controlplaca';
// $tables[] = 'convenios';
// $tables[] = 'detalle_orden_medica';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';
// $tables[] = 'analisisatm';

backupDatabaseTables('localhost','root','usc2017*','hco');
