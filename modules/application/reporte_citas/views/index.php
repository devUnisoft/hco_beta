<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Administración de presupuestos</title>
	<?php include '../../layouts/head.php';?>
	<?php include '../../layouts/css.php';?>
</head>
<body>
	<?php include '../../layouts/header.php';?>
	<?php include '../layouts/menuheader.php';?>
	<div class="container" style="margin-top: 40px;">
		<div class="row tt" style="margin-top: 62px;">
			<h2 align="center" class="color-default text-uppercase">Reporte Citas General</h2>
			<div class="col-md-2">
				<?php include '../../layouts/paciente.php';?>
			</div>
			<div class="col-md-12">
				<div class="col-md-4 col-md-offset-1 pull-left">
					<label>Fecha Inicial</label>
					<input type="date" class="form-control" id="fechainicial" name="fechainicial"   required />
				</div>
				<div class="col-md-4 col-md-offset-1 pull-left">
					<label>Fecha Final</label>
					<input type="date" class="form-control" id="fechafinal" name="fechafinal"   required />
				</div>

				<div class="col-md-4 col-md-offset-1 pull-left" id="comboDentist">	
				</div> 
			</div>
			<div class="col-md-6 col-md-offset-3" style="margin-top: 20px;">
				<a class="btn btn-primary btn-block" id="btnBuscar"> Buscar</a>
			</div>
			<div class="col-md-12" style="margin-top: 20px; display:none" id="tabla">
				<form action="ficheroExcelCitasGeneral.php" method="post" target="_blank" id="FormularioExportacion">
					<button id="buttonsend"  class="btn btn-success" disabled>Exportar a Excel</button>
					<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
				</form> 
				<div id="resultado">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Realizado</th>
							<th>Tipo Consulta</th>
							<th>Descripcion</th>
							<th>Fecha Inicial</th>
							<th>Fecha Final</th>
							<th>Ubicacion Sala</th>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>E-mail</th>
							<th>Especialista</th>
							<th>Genero</th>
						</tr>
					</thead>
					<tbody id="bodyTable">
						<tr><td></td></tr>
					</tbody>
				</table>
				</div>
			</div>
		</div>

	</div>
	<!-- MODAL CREAR PACIENTE clinica de ortodoncia-->

	<!-- END MODAL CREAR PRODUCTO -->

	<?php include '../../modals/budgets.php';?>
	<?php include '../../modals/evolutions.php';?>
	<?php include '../layouts/footer.php';?>
	<?php include '../../layouts/libs.php';?>
	<?php include '../../layouts/js.php';?>
	<?php include '../layouts/js.php';?>
	<script type="text/javascript" src="../../evoluciones/js/evolution.js"></script>
	<script type="text/javascript" src="../../evoluciones/js/evolution.validate.js"></script>
	<script>
		var emailOdontologo;
		$(window).on("load", function(e) {
			
			if('<?= $_SESSION['perfil']?>' != 'Asistente'){				
				emailOdontologo = '<?= $_SESSION['userid']?>'
			}
			
			setNameClinica('<?= $_SESSION['clinica']?>');
			getComboDentist('<?= $_SESSION['perfil']?>')
			getAllDentist('<?= $_SESSION['clinica']?>')
			$('#buttonsend').removeAttr('disabled');
		})

		$("#btnBuscar").click(function(){
			var fechaini = $("#fechainicial").val();
			var fechafinal = $("#fechafinal").val();			
			var especialista = $("#especialista").val();

			if (fechaini =="") {
				alert("Fecha inicial no encontrada")
				return
			}
			if (fechafinal =="") {
				alert("Fecha Final no encontrada")
				return
			}

			getAllDates(fechaini,fechafinal,especialista,emailOdontologo)

		})

		$("#buttonsend").click(function(){
			$("#datos_a_enviar").val( $("<div>").append( $("#resultado").eq(0).clone()).html());
          $("#FormularioExportacion").submit();
		})
	</script>

</body>
</html>
