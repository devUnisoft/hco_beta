<form id="form-consultadia-filter" action="javascript:reportsFilter('consultadia')" method="post">
  <div class="col-md-12 content_notification">
    <div class="col-md-2">
      <label class="color-default">Profesional:</label>
      <select class="form-control dentists select2" name="idProfesional"></select>
    </div>
    <div class="col-md-2">
      <label class="color-default">Forma de pago:</label>
      <select class="form-control methodsPays select2" name="idMethod"></select>
    </div>
    <div class="col-md-2">
      <label class="color-default">Documento paciente:</label>
      <input type="text" name="patientDocument" class="form-control">
    </div>
    <div class="col-md-2" align="center">
      <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Filtrar</button>
    </div>
    <div class="col-md-2" align="center">
      <a href="javascript:reports('consultadia')" class="btn btn-primary" style="margin-top: 25px;">Mostrar todos</a>
    </div>
  </div>
</form>
<table id="consultadia-table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th align="center">Documento paciente</th>
      <th align="center">Paciente</th>
      <th align="center">Profesional</th>
      <th align="center">Fecha consulta</th>
      <th align="center">Procedimiento</th>
      <th align="center">Valor consulta</th>
      <th align="center">Abonos</th>
      <th align="center">Saldo</th>
      <th align="center">Última Fecha de Abono</th>
    </tr>
  </thead>
  <tbody id="consultadia-data"></tbody>
</table>
