  <form id="form-tratamientos-filter" action="javascript:reportsFilter('tratamientos')" method="post">
    <div class="col-md-12 content_notification">
      <div class="col-md-2">
        <label class="color-default">Profesional:</label>
        <select class="form-control dentists select2" name="idProfesional"></select>
      </div>
      <div class="col-md-2">
        <label class="color-default">Forma de pago:</label>
        <select class="form-control methodsPays select2" name="idMethod"></select>
      </div>
      <div class="col-md-2">
        <label class="color-default">Documento paciente:</label>
        <input type="text" name="patientDocument" class="form-control">
      </div>
      <div class="col-md-2">
        <label class="color-default">Estado:</label>
        <select class="form-control" name="state">
          <option value="">Seleccecione una opción</option>
          <option value="1">no autorizados </option>
          <option value="2">autorizados</option>
        </select>
      </div>
      <div class="col-md-2" align="center">
        <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Filtrar</button>
      </div>
      <div class="col-md-2" align="center">
        <a href="javascript:reports('tratamientos')" class="btn btn-primary" style="margin-top: 25px;">Mostrar todos</a>
      </div>
    </div>
  </form>
  <table id="tratamientos-table" class="table table-bordered table-hover">
    <thead>
      <tr>
        <th align="center">Documento paciente</th>
        <th align="center">Paciente</th>
        <th align="center">Profesional </th>
        <th align="center">Fecha de tratamiento </th>
        <th align="center">Tratamiento</th>
        <th align="center">Estado</th>
        <th align="center">Valor tratamiento</th>
        <th align="center">Cuota inicial</th>
        <th align="center">Valor cuota</th>
        <th align="center">Abonos</th>
        <th align="center">Saldo</th>
        <th align="center">Última Fecha de Abono</th>
      </tr>
    </thead>
    <tbody id="tratamientos-data"></tbody>
  </table>
