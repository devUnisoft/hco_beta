<form id="form-progreso-filter" action="javascript:reportsFilter('progreso')" method="post">
  <div class="col-md-12 content_notification">
    <div class="col-md-2">
      <label class="color-default">Fecha incial:</label>
      <input type="date" class="form-control" name="dateInit">
    </div>
    <div class="col-md-2">
      <label class="color-default">Fecha final:</label>
      <input type="date" class="form-control" name="dateFinish">
    </div>
    <div class="col-md-2">
      <label class="color-default">Profesional:</label>
      <select class="form-control dentists select2" name="idProfesional"></select>
    </div>
    <div class="col-md-2">
      <label class="color-default">Documento paciente:</label>
      <input type="date" class="form-control" name="dateInit">
    </div>
    <div class="col-md-2" align="center">
      <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Filtrar</button>
    </div>
    <div class="col-md-2" align="center">
      <a href="javascript:reports('progreso')" class="btn btn-primary" style="margin-top: 25px;">Mostrar todos</a>
    </div>
  </div>
</form>
<table id="progreso-table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th align="center">Documento paciente</th>
      <th align="center">Paciente</th>
      <th align="center">Fecha</th>
      <th align="center">Procedimiento</th>
      <th align="center">Forma de pago</th>
      <th align="center">Valor cuota</th>
      <th align="center">Fecha de pago</th>
      <th align="center">Valor pagado</th>
      <th align="center">Saldo cuota</th>
      <th align="center">Total</th>
      <th align="center">Abonos</th>
      <th align="center">Saldo</th>
    </tr>
  </thead>
  <tbody id="progreso-data"></tbody>
</table>
