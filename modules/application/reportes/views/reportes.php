<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de reportes</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de reportes</h2>
      <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="margin-bottom: 130px;">
          <ul class="nav nav-tabs" role="tablist" style="margin-top: -40px;display: block;width: 100%;left: 33%;">
            <li role="presentation" class="active"><a href="#tratamientos" onclick="reports('tratamientos')" aria-controls="tratamientos" role="tab" data-toggle="tab">Tratamientos</a></li>
            <li role="presentation"><a href="#consultadia" onclick="reports('consultadia')" aria-controls="consultadia" role="tab" data-toggle="tab">Consulta día</a></li>
            <li role="presentation"><a href="#pagoProfesionales" onclick="reports('pagoProfesionales')" aria-controls="pagoProfesionales" role="tab" data-toggle="tab">Pago profesionales</a></li>
            <li role="presentation"><a href="#caja" onclick="reports('caja')" aria-controls="caja" role="tab" data-toggle="tab">Cierre de caja</a></li>
          </ul>
          <div class="tab-content" style="margin-top: 75px;">
            <!--tratamientos-->
            <div role="tabpanel" class="tab-pane active" id="tratamientos">
              <?php include 'tratamientos.php';?>
            </div>
            <!--consultadia-->
            <div role="tabpanel" class="tab-pane" id="consultadia">
              <?php include 'consultadia.php';?>
            </div>
            <!--pagoProfesionales-->
            <div role="tabpanel" class="tab-pane" id="pagoProfesionales">
              <?php include 'pagoProfesionales.php';?>
            </div>
            <!--caja-->
            <div role="tabpanel" class="tab-pane" id="caja">
              <?php include 'caja.php';?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>

</html>
