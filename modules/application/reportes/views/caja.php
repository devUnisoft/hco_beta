<form id="form-caja-filter" action="javascript:reportsFilter('caja')" method="post">
  <div class="col-md-12 content_notification">
    <div class="col-md-2">
      <label class="color-default">Fecha:</label>
      <input type="date" class="form-control" name="dateInit">
    </div>
    <div class="col-md-2">
      <label class="color-default">Profesional:</label>
      <select class="form-control dentists select2" name="idProfesional"></select>
    </div>
    <div class="col-md-2">
      <label class="color-default">Forma de pago:</label>
      <select class="form-control methodsPays select2" name="idMethod"></select>
    </div>
    <div class="col-md-2">
      <label class="color-default">Documento paciente:</label>
      <input type="text" name="patientDocument" class="form-control">
    </div>
    <div class="col-md-1" align="center">
      <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Filtrar</button>
    </div>
    <div class="col-md-1" align="center">
      <a href="javascript:reports('caja')" class="btn btn-primary" style="margin-top: 25px;">Mostrar todos</a>
    </div>
  </div>
</form>
<table id="caja-table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th align="center">Documento paciente</th>
      <th align="center">Paciente</th>
      <th align="center">Profesional</th>
      <th align="center">Fecha procedimiento</th>
      <th align="center">Procedimiento</th>
      <th align="center">Valor procedimiento</th>
      <th align="center">Fecha de Abono</th>
      <th align="center">Forma de pago</th>
      <th align="center">Abono de la fecha</th>
      <th align="center">Saldo a la fecha</th>
      <th align="center">Receptor del  pago</th>
    </tr>
  </thead>
  <tbody id="caja-data"></tbody>
</table>

<div class="row">
  <form id="form-pays-caja" method="post">
    <div class="col-xs-3">
      <label class="color-default">Recaudo cheque:</label>
      <input type="text" class="form-control" name="cheque">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Recaudo Efectivo:</label>
      <input type="text" class="form-control" name="efectivo">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Tarjeta de Crédito:</label>
      <input type="text" class="form-control" name="credito">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Recaudo Tarjeta débito:</label>
      <input type="text" class="form-control" name="debito">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Recaudo Transferencia:</label>
      <input type="text" class="form-control" name="transferencia">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Recaudo Otros pagos:</label>
      <input type="text" class="form-control" name="otros">
    </div>
    <div class="col-xs-3">
      <label class="color-default">Total Recaudo:</label>
      <input type="text" class="form-control" name="total">
    </div>
  </form>
</div>
