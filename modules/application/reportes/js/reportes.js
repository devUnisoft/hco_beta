/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */
 $(window).on("load", function (e) {
   reportesRouter()
 })
 /*========================================================================*/
 function reportesRouter(){

   methodsPaySelect().then(response => {
     $(".select2").select2()
   }, error => {})

   dentistsSelect().then(response => {
     $(".select2").select2()
   }, error => {})

   reports('tratamientos')
 }
/*========================================================================*/
function reports(type) {
  let url = 'Reports_controller.php'
  let data = new FormData()
  data.append(type, 1)
  all(url, data).then(response => {
    Rreports(type, response)
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function reportsFilter(type) {
  const form = document.getElementById('form-'+type+'-filter')
  let data = new FormData(form)
  let url = 'Reports_controller.php'
  data.append(type+'Filter', 1)
  return all(url, data).then(response => {
    Rreports(type, response)
  }, error => {})
}
 /*========================================================================*/
function Rreports(div,resp) {
  if(div == "pagoProfesionales"){
    RreportsPago(div,resp)
  }
  else if(div == "consultadia"){
    RreportsConsultaDia(div,resp)
  }
  else if(div == "tratamientos"){
    RreportsTratamientos(div,resp)
  }
  else{
    RreportsCaja(div,resp)
  }
}

/*========================================================================*/
function RreportsTratamientos(div,resp) {
 $('#'+div+'-table').dataTable().fnDestroy()
 document.querySelector('#'+div+'-data').innerHTML= ""
 if(resp != null){
   resp.map(row => {
     let template ='<tr>'+
                     '<td class="text-left">'+row.patientCode+'</td>'+
                     '<td class="text-left">'+row.patientName+'</td>'+
                     '<td class="text-left">'+row.profesional+'</td>'+
                     '<td class="text-left">'+row.date+'</td>'+
                     '<td class="text-left">'+row.procedimiento+'</td>'+
                     '<td class="text-left">'+row.state+'</td>'+
                     '<td class="text-left">'+row.total+'</td>'+
                     '<td class="text-left">'+row.initial+'</td>'+
                     '<td class="text-left">'+row.valorCuota+'</td>'+
                     '<td class="text-left">'+row.abonos+'</td>'+
                     '<td class="text-left">'+row.saldo+'</td>'+
                     '<td class="text-left">'+row.dateSave+'</td>'+
                   '</tr>';
       $('#'+div+'-data').append(template);
   })
 }
 dataTable('#'+div+'-table',4)
}
/*========================================================================*/
function RreportsConsultaDia(div,resp) {
 $('#'+div+'-table').dataTable().fnDestroy()
 document.querySelector('#'+div+'-data').innerHTML= ""
 if(resp != null){
   resp.map(row => {
     let template ='<tr>'+
                     '<td class="text-left">'+row.patientCode+'</td>'+
                     '<td class="text-left">'+row.patientName+'</td>'+
                     '<td class="text-left">'+row.profesional+'</td>'+
                     '<td class="text-left">'+row.date+'</td>'+
                     '<td class="text-left">'+row.procedimiento+'</td>'+
                     '<td class="text-left">'+row.total+'</td>'+
                     '<td class="text-left">'+row.abonos+'</td>'+
                     '<td class="text-left">'+row.saldo+'</td>'+
                     '<td class="text-left">'+row.dateSave+'</td>'+
                   '</tr>';
       $('#'+div+'-data').append(template);
   })
 }
 dataTable('#'+div+'-table',4)
}
/*========================================================================*/
function RreportsPago(div,resp) {
 $('#'+div+'-table').dataTable().fnDestroy()
 document.querySelector('#'+div+'-data').innerHTML= ""

 const form  =  document.getElementById("form-pays-profesional")

 form.cheque.value = 0
 form.efectivo.value = 0
 form.credito.value = 0
 form.debito.value = 0
 form.transferencia.value = 0
 form.otros.value = 0
 form.total.value = 0
 form.comision.value = 0


 if(resp != null){
   resp.data.map(row => {
     let template ='<tr>'+
                     '<td class="text-left">'+row.patientCode+'</td>'+
                     '<td class="text-left">'+row.patientName+'</td>'+
                     '<td class="text-left">'+row.profesional+'</td>'+
                     '<td class="text-left">'+row.date+'</td>'+
                     '<td class="text-left">'+row.procedimiento+'</td>'+
                     '<td class="text-left">'+row.total+'</td>'+
                     '<td class="text-left">'+row.porcentaje+'</td>'+
                     '<td class="text-left">'+row.comision+'</td>'+
                     '<td class="text-left">'+row.fecha+'</td>'+
                     '<td class="text-left">'+row.metodopago+'</td>'+
                     '<td class="text-left">'+row.abonos+'</td>'+
                     '<td class="text-left">'+row.saldo+'</td>'+
                     '<td class="text-left">'+row.receptor+'</td>'+
                   '</tr>';
       $('#'+div+'-data').append(template);
   })

    form.cheque.value = formatNumber.new( resp['pays'].cheque)
    form.efectivo.value = formatNumber.new( resp['pays'].efectivo)
    form.credito.value = formatNumber.new( resp['pays'].credito)
    form.debito.value = formatNumber.new( resp['pays'].debito)
    form.transferencia.value = formatNumber.new( resp['pays'].transferencia)
    form.otros.value = formatNumber.new( resp['pays'].otros)
    form.total.value = formatNumber.new( resp['pays'].totalT)
    form.comision.value = formatNumber.new( resp['pays'].comisionTotal)
 }
 dataTable('#'+div+'-table',4)
}

/*========================================================================*/
function RreportsCaja(div,resp) {
 $('#'+div+'-table').dataTable().fnDestroy()
 document.querySelector('#'+div+'-data').innerHTML= ""

 const form  =  document.getElementById("form-pays-caja")

 form.cheque.value = 0
 form.efectivo.value = 0
 form.credito.value = 0
 form.debito.value = 0
 form.transferencia.value = 0
 form.otros.value = 0
 form.total.value = 0

 if(resp != null){
   resp.data.map(row => {
     let template ='<tr>'+
                     '<td class="text-left">'+row.patientCode+'</td>'+
                     '<td class="text-left">'+row.patientName+'</td>'+
                     '<td class="text-left">'+row.profesional+'</td>'+
                     '<td class="text-left">'+row.date+'</td>'+
                     '<td class="text-left">'+row.procedimiento+'</td>'+
                     '<td class="text-left">'+row.total+'</td>'+
                     '<td class="text-left">'+row.fecha+'</td>'+
                     '<td class="text-left">'+row.metodopago+'</td>'+
                     '<td class="text-left">'+row.abonos+'</td>'+
                     '<td class="text-left">'+row.saldo+'</td>'+
                     '<td class="text-left">'+row.receptor+'</td>'+
                   '</tr>';
       $('#'+div+'-data').append(template);
   })

   form.cheque.value = formatNumber.new( resp['pays'].cheque)
   form.efectivo.value = formatNumber.new( resp['pays'].efectivo)
   form.credito.value = formatNumber.new( resp['pays'].credito)
   form.debito.value = formatNumber.new( resp['pays'].debito)
   form.transferencia.value = formatNumber.new( resp['pays'].transferencia)
   form.otros.value = formatNumber.new( resp['pays'].otros)
   form.total.value = formatNumber.new( resp['pays'].totalT)
 }
 dataTable('#'+div+'-table',4)
}
