/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function loadPreOrder(){
  fechaActual()
  productsSelect()
  return providersSelect()
}
/*========================================================================*/
function loadProduct(idProduct){
  let data = new FormData()
  let url = 'Products_controller.php'
  data.append('item', 1)
  data.append('id', idProduct)
  postX(url, data).then(response => {
    providersForProductSelect(idProduct)
    set(".set-product-und").value = response[0]['und']
    set(".set-product-count").value = 1
    //$(".set-product-count").attr('max', response[0]['stock'])
    set(".set-product-price").value = formatNumber.new(response[0]['price'])
    set(".set-product-total").value = formatNumber.new(response[0]['price'])
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function updateOrder2(){
  if(ordersItems2.length>0){
    const form = document.getElementById("form-order")
      set("#items").value = JSON.stringify(ordersItems2)
      form.submit()
  }
  else{
    message("Ingrese almenos un producto.")
  }
}
/*========================================================================*/

// function orderUpdate() {
//   console.log("Success!");
//   let url    = 'Orders_controller.php'
//   let form   = 'form-order'
//   let div    = 'file-order'
//   let id = getIdorder()
//   let folder = 'orders'
//   update(url, form, id, div, folder).then(response => {
//     console.log("Success!", response);
//     //setIdorder(response.id)
//     message(response.Msg)
//     alertify.alert(response.Msg, function(){
//       reload()
//     });
//
//   }, error => {
//     console.error("Failed!", error);
//   })
// }

/*========================================================================*/
function priceForCount(count){
  let price = set(".set-product-price").value
  let subto = 0;
  price = removeNumber(price);
  let total = price * count
  set(".set-total").value = formatNumber.new(total)
}
/*========================================================================*/
var ordersItems = []
var ordersItems2 = []
var index = 0
var totalOrder = 0
var subtotalOrder = 0
var ivaOrder = 0
function orderAddItem(x) {
  let gtotal = 0
  let iva = 0
  console.log(x)
  const form = document.getElementById("form-addItem")
  let idProduct = form.idProduct.value
  let und = form.und.value
  let idProvider = form.idProvider.value
  let price = form.price.value
  let count = form.count.value
  let total = form.total.value
  var validserv = []
  let product = form.idProduct.options[form.idProduct.options.selectedIndex].text
  let provider = form.idProvider.options[form.idProvider.options.selectedIndex].text
  var a = -1;
  if(ordersItems2.length>0){
    //var resultObject = search(idProduct, ordersItems2);
    for(var i = 0; i < 200; i++)
    {
      if(ordersItems2[i]){
        if(ordersItems2[i].idProduct){
          if(ordersItems2[i].idProduct == idProduct)
          {
            validserv.push(ordersItems2[i].idProduct);
            console.log(ordersItems2[i].idProduct )
          }
        }
      }
    }
    a = validserv.indexOf(idProduct);
  }

  if(a < 0) {
    // element doesn't exist in array
    //table
    let inputCount = generateInput('text', 'count-'+index, count, "orderChangeCountItem", "orderChangeCountItem", "")
    let inputTotal = generateInput('text', 'total-'+index, total, "", "", "readonly")
    let inputPrice = generateInput('text', 'price-'+index, price, "", "", "readonly")
    console.log(inputCount)
    let item = {product:product, und:und, provider:provider, price:inputPrice, count:inputCount, total:inputTotal, id:index};
    ordersItems[x] = item
    //json
    price = removeNumber(price)
    total = removeNumber(total)
    let item2 = {idProduct:idProduct, und:und,  idProvider:idProvider, price:price, count:count, total:total,id:index};
    ordersItems2[x] = item2
    console.log(ordersItems)
    index++
    document.getElementById("form-addItem").reset();
    $('#emptyProduct').val('').trigger('change');
    $('#emptyProvider').val('').trigger('change');
    form.setAttribute("action", "javascript:orderAddItem("+index+")")
    updateviewitems2()
  }
  else{
    message("El producto ya se encuentra ingresado.")
  }
}

function updateFormItem(){
  let gtotal = 0
  let iva = 0
  for(i=0;i<=index;i++){
    gtotal = gtotal +  parseInt(removeNumber(document.getElementById("total-"+i).value))
    console.log(gtotal)
    $('input[name="subtotal"]').val(formatNumber.new(gtotal))

    //iva
    iva = iva + ( parseInt(removeNumber(document.getElementById("total-"+i).value)) * ( 19 / 100 ) )
    set(".iva").value = formatNumber.new(iva)
    $('input[name="total"]').val(formatNumber.new( gtotal + iva))

   }
}

function updateTotal(subtotal, iva, total){

  totalOrder    = total+totalOrder
  subtotalOrder = total+subtotalOrder
  ivaOrder      = iva
  updateFormItem(subtotal, iva, total)
}

function discountOrder(val){
  if(val > 0){
  let subt = removeNumber($('input[name="subtotal"]').val())
  let iva = removeNumber(set(".iva").value);
  console.log(subt + " " + iva)
  let desc = subt - ( subt * ( val / 100) )
  $('input[name="subtotal"]').val(formatNumber.new(desc))

  iva =  desc * ( 19 / 100 )
     set(".iva").value = formatNumber.new(iva)
     $('input[name="total"]').val(formatNumber.new( desc + iva))

  }else{
    updateFormItem()
  }
  //updateFormItem(subtotalOrder, ivaOrder, totalOrders)
}

/* ======================================================================== */
function orderChangeCountItem(input){
  let str = input.id
  var id = str.substring(str.length,6)
  console.log(id)
  let count = input.value
  let price = document.querySelector("#price-"+id).value
  price = removeNumber(price);
  let total = count*price

  document.querySelector("#total-"+id).value = formatNumber.new(total)
  let inputCount = generateInput('text', 'count-'+id, count, "orderChangeCountItem", "orderChangeCountItem", "")
  let inputTotal = generateInput('text', 'total-'+id, formatNumber.new(total), "", "", "readonly")
  ordersItems[id].count = inputCount
  ordersItems[id].total = inputTotal
  ordersItems2[id].count = count
  ordersItems2[id].total = total
  updateviewitems2()
}
/* ======================================================================== */
function orderDeleteItem(idArray){
  alertify.confirm("¿Confirmas que deseas remover el servicio?", function () {
    console.log(ordersItems)
    posi = search(idArray, ordersItems)
    posi2 = search(idArray, ordersItems2)
    ordersItems.splice(posi, 1);
    ordersItems2.splice(posi2, 1);
    updateviewitems2();
  }, function() {}
  )
}
/* ======================================================================== */
function orderUpdateItemCount(coun){
  const form = document.getElementById("form-addItem")
  let price = form.price.value
  let count = form.count.value
  price = removeNumber(price);
  let total = parseInt(price*count)
  form.total.value = total
}

function orderViewItems(){
  //orderUpdate(true).then(response => {order()}, error => {})
  $("#orders-items-data").empty()
  let btnDelete = ['times', 'danger', 'orderDeleteItem']
  let btns = new Array()
  btns.push(btnDelete)
  console.log(ordersItems)
  table(ordersItems, "orders-items", btns, 0)
}


function updateviewitems2(){
  $('#totalOrder').val(0)
  $('#cantidadtotal').val(0)
  if(ordersItems.length>0){
    let counttotal = 0
    let gtotal = 0
    for(i=0;i<ordersItems.length;i++){
     if(ordersItems2[i]){
       gtotal = gtotal +  parseInt(ordersItems2[i].total)
       console.log(gtotal)
       $('#totalOrder').val(formatNumber.new(gtotal))
       counttotal = counttotal +  parseInt(ordersItems2[i].count)
       $('#cantidadtotal').val(counttotal)
     }
    }
  }
  orderViewItems()
}
