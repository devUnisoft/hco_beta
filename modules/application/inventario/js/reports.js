/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */
 /*========================================================================*/
 function reportsRouter(){
   reports()
 }
/*========================================================================*/
function reports() {
  let url = 'Ordersreports_controller.php'
  let data = new FormData()
  data.append('all', 1)
  all(url, data).then(response => {
    Rreports(response)
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function reportsFilter() {
  const form = document.getElementById('form-report-filter')
  let data = new FormData(form)
  let url = 'Ordersreports_controller.php'
  data.append('filter', 1)
  return all(url, data).then(response => {
    Rreports(response)
  }, error => {})
}
 /*========================================================================*/
function Rreports(resp) {
  $("#reports-table").dataTable().fnDestroy()
  document.querySelector("#reports-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {
      let template ='<tr>'+
                      '<td class="text-left">'+row.id+'</td>'+
                      '<td class="text-left">'+row.date+'</td>'+
                      '<td class="text-left">'+row.product+'</td>'+
                      '<td class="text-left">'+row.category+'</td>'+
                      '<td class="text-left">'+row.subcategory+'</td>'+
                      '<td class="text-center">'+row.count+'</td>'+
                      '<td class="text-left">'+row.type+'</td>'+
                      '<td class="text-left">'+row.und+'</td>'+
                    '</tr>';
        $("#reports-data").append(template);
    })
  }

  dataTable("#reports-table",2)
}
