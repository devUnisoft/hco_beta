/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */

 function setIdprovider(id) {localStorage.setItem("idprovider", id)}
 function getIdprovider() {return localStorage.getItem("idprovider")}
 /*========================================================================*/
 function providersRouter(){
   providers()
 }
/*========================================================================*/
function providers() {
  let url = 'Providers_controller.php'
  let data = new FormData()
  data.append('all', 1)
  all(url, data).then(response => {
    Rproviders(response)
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function providerSearch() {
  const form = document.getElementById('form-providerSearch')
  let data = new FormData(form)
  let url = 'Providers_controller.php'
  data.append('providerSearch', 1)
  all(url, data).then(response => {
    Rproviders(response)
  }, error => {})
}

 /*========================================================================*/
function Rproviders(resp) {
  $("#providers-data").empty()
  if(resp == null){
    message("No se encontraron resultados.")
  }
  else{
    resp.map(row => {
      template = '<div class="col-xs-4">'+
          '<div class="media">'+
            '<a class="pull-left" href="javascript:providerGo('+row.id+')">'+
              '<img class="media-object dp img-circle" src="../../../../images/paciente.png" style="width: 100px;height:100px;"></a>'+
              '<div class="media-body">'+
                '<h4 class="media-heading">'+row.name+'</h4>'+
                '<hr style="margin:8px auto">'+
              '</div>'+
            '</div>'+
        '</div>';
        $("#providers-data").append(template)
    })
  }
}
/*========================================================================*/
function providerGo(id){
 setIdprovider(id)
 productsSelect()
 provider()
 cleanForm("form-provider")
 $('#providerProducts-data').empty();
 $('#mdl-provider').modal('show');
}
/*========================================================================*/
function provider() {
 if(getIdprovider()>0){
   let url = 'Providers_controller.php'
   let data = new FormData()
   data.append('item', 1)
   data.append('id', getIdprovider())
   let form = 'form-provider'
   let folder = 'providers'
   item(url, data, form, folder).then(provider => {
     const form = document.getElementById('form-provider')
     console.log(form)
     form.name.value = provider.item[0]['name']
     form.code.value = provider.item[0]['code']
     form.email.value = provider.item[0]['email']
     form.phone.value = provider.item[0]['phone']
     form.personContact.value = provider.item[0]['personContact']
     if(provider.products != null){
       providerProducts = provider.products
       providerProducts2()
     }
   }, error => {
     //console.error("Failed!", error);
   })
 }
 else{

 }
}
/*========================================================================*/
function providerUpdate() {
  if(providerProducts.length>0){
    $('#mdl-provider').modal('hide');
    let url = 'Providers_controller.php'
    const form = document.getElementById('form-provider')
    let data = new FormData(form)
    data.append('update', 1)
    data.append('id', getIdprovider())
    postX(url, data).then(response => {
      //console.log("Success!", response);
      message("Se ha guardado correctamente.")
      providers()
      setIdprovider(response.id)
    }, error => {
      //console.error("Failed!", error);
    })
  }
  else{
    message("Seleccione almenos un producto.")
  }
}
/*========================================================================*/
function providerRemove(id){
 let url = 'Providers_controller.php'
 let data = new FormData()
 data.append('delete', 1)
 data.append('id', id)
 let msg = "¿Confirmas que deseas remover el proveedor?"
 deleteItem(url, data, msg).then(response => {
   //console.log("Success!", response);
   $('#mdl-provider').modal('hide');
   message("Se ha removido correctamente.")
   providers()
 }, error => {
   //console.error("Failed!", error);
 })
}
/*========================================================================*/
function providersSelect(){
 let url = 'Providers_controller.php'
 let data = new FormData()
 data.append('all', 1)
 let div = '.providers'
 let text = 'Seleccione una opción'
 return selectOptions(url, data, div, text)
}

/*========================================================================*/
function providersForProductSelect(idProduct){
  let url = 'Providers_controller.php'
  let data = new FormData()
  data.append('forProduct', 1)
 data.append('idProduct', idProduct)
 let div = '.providers'
 return selectOptions(url, data, div)
}
/*========================================================================*/
var providerProducts = []
function addProviderProduct(){
  const form = document.getElementById("form-provider")
  let id = form.idProduct.value
  let name = form.idProduct.options[form.idProduct.options.selectedIndex].text
  var index = search(id, providerProducts);
  console.log(typeof index)
  if(typeof index == "boolean"){
    //table
    let item = {name:name,id:id};
    providerProducts.push(item);
    providerProducts2()
  }
  else if(index >= 0) {
      message("El producto ya se encuentra ingresado.")
  }
  else{
    console.log("Nose")
  }
}
/* ======================================================================== */
function providerProducts2(){
  $("#providerProducts-data").empty()
  if(providerProducts != null){
    set("#providerProducts").value = JSON.stringify(providerProducts)
    //orderUpdate(true).then(response => {order()}, error => {})
    let btnDelete = ['times', 'danger', 'providerProductsDeleteItem']
    let btns = new Array()
    btns.push(btnDelete)
    table(providerProducts, "providerProducts", btns, 1)
  }

}
/* ======================================================================== */
function providerProductsDeleteItem(id){
  alertify.confirm("¿Confirmas que deseas remover el producto?", function () {
    var index = search(id, providerProducts);
    console.log(index)
    if(typeof index == "boolean"){
      console.log("No se encontro")
    }
    else if(index >= 0) {
      providerProducts.splice(index, 1);
      providerProducts2()
    }
  }, function() {}
  )
}
