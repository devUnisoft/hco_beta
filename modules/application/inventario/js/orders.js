/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function setIdorder(id) {localStorage.setItem("idorder", id)}
function getIdorder() {return localStorage.getItem("idorder")}

/*========================================================================*/
function orderRouter(){
  orders()
}
/*========================================================================*/
function orders() {
  let url = 'Orders_controller.php'
  let data = new FormData()
  data.append('all', 1)
  all(url, data).then(response => {
    console.log("Success!", response);
    Rorders(response)
  }, error => {
    console.error("Failed!", error);
  })
 }
/*========================================================================*/
function orderFilter() {
  const form = document.getElementById('form-order-filter')
  let data = new FormData(form)
  let url = 'Orders_controller.php'
  data.append('filter', 1)
  return all(url, data).then(response => {
    Rorders(response)
  }, error => {})
}

function Rorders(resp) {
  $("#orders-table").dataTable().fnDestroy()
  document.querySelector("#orders-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {

      let templateBtn = '';

      if(row.recived == "No"){
        templateBtn += '<a href="javascript:orderRecived('+row.id+')" class="space"><i class="fa fa-check"></i></a>';

      }

      templateBtn += '<a href="javascript:orderRecivedParcial('+row.id+')" class="space"><i class="fa fa-eye"></i></a>';
      // else{
      //   templateBtn = '<p>Sin Acción</p>';
      // }
      let template ='<tr>'+
                      '<td class="text-left">'+row.uniquecode+'</td>'+
                      '<td class="text-left">'+row.date+'</td>'+
                      '<td class="text-center">'+row.count+'</td>'+
                      '<td class="text-center">'+row.recived+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100">'+templateBtn+'</div>'+
                      '</td>'+
                    '</tr>';
        $("#orders-data").append(template);
    })
  }
  dataTable("#orders-table",2)
}
 /*========================================================================*/
function orderRecivedParcial(id){
 setIdorder(id)
 let data = new FormData()
 let url = 'Orders_controller.php'
 data.append('RecivedParcial', 1)
 data.append('id', getIdorder())
 postX(url, data).then(response => {
   console.log("Success!", response);
   RorderRecivedParcial(response)
 }, error => {
   console.error("Failed!", error);
 })
}
// ============================================
function RorderRecivedParcial(resp){
  $("#orders-recived-table").dataTable().fnDestroy()
  document.querySelector("#orders-recived-data").innerHTML= ""
  $("#orders-recived-data").empty()
  if(resp.length>0){
    $("#mdl_orderParcial").modal('show')
    resp.map(row => {
      let templateBtn = '';
      if(row.recived == "No"){
        templateBtn = '<a href="javascript:addRecived('+row.id+')" class="space"><i class="fa fa-check"></i></a>';
      }
      else{
        templateBtn = '<p>Sin Acción</p>';
      }
      let template ='<tr>'+
                      '<td class="text-left">'+row.product+'</td>'+
                      '<td class="text-left">'+row.und+'</td>'+
                      '<td class="text-center">'+row.count+'</td>'+
                      '<td class="text-center">'+row.recived+'</td>'+

                      '<td>'+
                        '<div class="btn-group space100">'+templateBtn+'</div>'+
                      '</td>'+
                    '</tr>';
        $("#orders-recived-data").append(template);
    })
    dataTable("#orders-recived-table",1)
  }
  else{

  }
}
/*========================================================================*/
function addRecived(id){
  let data = new FormData()
  let url = 'Orders_controller.php'
  data.append('addRecived', 1)
  data.append('id', id)
  let msg = "¿Confirmas que deseas recibir el item?"
  deleteItem(url, data, msg).then(response => {
    console.log("Success!", response);
    orderRecivedParcial(getIdorder())
  }, error => {
    console.error("Failed!", error);
  })
}
/*========================================================================*/
function orderRecived(id){
  let data = new FormData()
  let url = 'Orders_controller.php'
  data.append('recived', 1)
  data.append('id', id)
  let msg = "¿Confirmas que deseas recibir el pedido?"
  deleteItem(url, data, msg).then(response => {
    console.log("Success!", response);
    reload()
  }, error => {
    console.error("Failed!", error);
  })
}
 /*========================================================================*/
function orderGo(id){
  setIdorder(id)
  cleanForm("form-order")
  cleanForm("form-addItem")
  $("#orders-items-data").empty()
  ordersItems = []
  ordersItems2 = []
  index = 0
  totalOrder = 0
  subtotalOrder = 0
  ivaOrder = 0
  loadPreOrder().then(response => {
     console.log("Success!", response);
     $("#mdl_order").modal('show')
     $('.select2').select2({
      placeholder: 'Seleccione un producto'
    });
     order()
   }, error => {
     console.error("Failed!", error);
   })
}

function order() {
  if(getIdorder()>0){
    let data = new FormData()
    let url = 'Orders_controller.php'
    data.append('item', 1)
    data.append('id', getIdorder())
    let form = 'form-order-update'
    let folder = 'orders'
    let folderCategory = 'documents'
    item(url, data, form, folder, folderCategory).then(response => {
     console.log("Success!", response.details)
     var data = response.details;
     for(var item in data){
     console.log(data[item].site)
     $('#orders-items-table > tbody:last-child').append('<tr><td>'+data[item].idActivities+'</td><td>'+data[item].idServices+'</td><td>sss'+data[item].site+'</td><td>'+data[item].count+'</td><td>'+data[item].price+'</td><td>'+data[item].total+'</td><td>qqq'+$("#apellido").val()+'</td></tr>');
     //validateLoadOrder(response)
     }
    }, error => {
      reject(Error('Request failed', error))
    })
  }
}
 /*========================================================================*/
function orderUpdate() {
  $("#mdl_order").modal('hide')
  let url = 'Orders_controller.php'
  const form = document.getElementById('form-order')
  let data = new FormData(form)
  data.append('update', 1)
  data.append('id', getIdorder())
  postX(url, data).then(response => {
    console.log("Success!", response);
    reload()
    //orders()
  }, error => {
    console.error("Failed!", error);
  })
}

 /*========================================================================*/
 function orderSelect(){
   let data = new FormData()
   let url = 'Orders_controller.php'
   data.append('all', 1)
   let div = '.orders'
   return selectOptions(url, data, div)
 }
