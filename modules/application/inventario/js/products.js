/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */

 function setIdproduct(id) {localStorage.setItem("idproduct", id)}
 function getIdproduct() {return localStorage.getItem("idproduct")}
 /*========================================================================*/
 function productsRouter(){
   products()
 }
 /*========================================================================*/
 function validatecheck2() {
   if ($('#validatecheck').is(':checked')) {
     $("#showDetail").css("display", "block")
     $("#alertStock1").attr("required", "required")
     $("#idProvider1").attr("required", "required")
     $("#countProvider1").attr("required", "required")
   } else {
     $("#showDetail").css("display", "none")
     $("#alertStock1").removeAttr("required")
     $("#idProvider1").removeAttr("required")
     $("#countProvider1").removeAttr("required")
   }
 }
/*========================================================================*/
function products() {
  let url = 'Products_controller.php'
  let data = new FormData()
  data.append('all', 1)
  all(url, data).then(response => {
    Rproducts(response)
  }, error => {
    //console.error("Failed!", error);
  })
}
 /*========================================================================*/
function Rproducts(resp) {
  $("#products-table").dataTable().fnDestroy()
  document.querySelector("#products-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {
      let template ='<tr>'+
                      '<td class="text-left">'+row.name+'</td>'+
                      '<td class="text-left">'+row.category+'</td>'+
                      '<td class="text-left">'+row.subCategory+'</td>'+
                      '<td class="text-left">'+row.type+'</td>'+
                      '<td class="text-left">'+row.provider+'</td>'+
                      '<td class="text-center">'+row.stock+'</td>'+
                      '<td class="text-left">'+row.und+'</td>'+
                      '<td style="text-align: right;">'+row.price+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100">'+
                          '<a href="javascript:productAdd('+row.idProduct+','+row.id+')" class="space"><i class="fa fa-plus"></i></a>'+
                          '<a href="javascript:productMinus('+row.idProduct+','+row.id+')" class="space"><i class="fa fa-minus"></i></a>'+
                          '<a href="javascript:productGo('+row.idProduct+','+row.id+')" class="space"><i class="fa fa-pencil"></i></a>'+
                          '<a href="javascript:productRemove('+row.idProduct+','+row.id+')" class="space"><i class="fa fa-times"></i></a>'+
                        '</div>'+
                      '</td>'+
                    '</tr>';
        $("#products-data").append(template);
    })
  }
  dataTable("#products-table",1)
}
/*========================================================================*/
function productAdd(idProduct, id) {
  if(id == null){
    message("Para agregar una cantidad, es importante primero editar el producto.")
  }
  else{
    let msg = "Digite la cantidad que ingresará al inventario"
    alertify.okBtn("Aceptar").cancelBtn("Cancelar").prompt(msg,
       function (val, ev) {
         ev.preventDefault();
         updateproductAdd(idProduct,val)
       }, function(ev) {ev.preventDefault();}
     );
     $(".alertify .dialog div input").attr("type", "number")
  }
}

/*========================================================================*/
function updateproductAdd(idProduct,count){
 let url = 'Products_controller.php'
 let data = new FormData()
 data.append('updateproductAdd', 1)
 data.append('count', count)
 data.append('idProduct', idProduct)
 postX(url, data).then(response2 => {
   message("Se ha actualizado correctamente.")
   products()
 }, error => {console.error("Failed!", error);})
}
/*========================================================================*/
function productMinus(idProduct, id) {
  if(id == null){
    message("Para restar una cantidad, es importante primero editar el producto.")
  }
  else{
  let msg = "Digite la cantidad que restará al inventario"
  alertify.okBtn("Aceptar").cancelBtn("Cancelar").prompt(msg,
     function (val, ev) {
       ev.preventDefault();
       updateproductMinus(idProduct,val)
     }, function(ev) {ev.preventDefault();}
   );
   $(".alertify .dialog div input").attr("type", "number")
 }
}
/*========================================================================*/
function updateproductMinus(idProduct,count){
 let url = 'Products_controller.php'
 let data = new FormData()
 data.append('updateproductMinus', 1)
 data.append('count', count)
 data.append('idProduct', idProduct)
 postX(url, data).then(response2 => {
   message("Se ha actualizado correctamente.")
   products()
 }, error => {console.error("Failed!", error);})
}
/*========================================================================*/
function productGo(idProduct, id){
 setIdproduct(idProduct)
 $('#mdl-product').modal('show');
 cleanForm("form-product")
 if(idProduct == 0){
   categories()
   providersSelect()
 }
 else{
   product()
 }
}
/*========================================================================*/
function product() {
    let url = 'Products_controller.php'
    let data = new FormData()
    data.append('item', 1)
    data.append('id', getIdproduct())
    let form = 'form-product'
    let folder = 'products'
    item(url, data, form, folder).then(item => {
      //console.log("Success!", response);
      categories().then(response => {
        subCategories(item[0]['idCategory']).then(response => {
          providersSelect().then(response => {
            if(getIdproduct()>0){
              $('.categories').val(item[0]['idCategory'])
              $('.subCategories').val(item[0]['idSubCategory'])
              $('.providers').val(item[0]['idProvider'])
               const form = document.getElementById('form-product')
               console.log(form)
               form.name.value = item[0]['name']
               form.price.value = item[0]['price']
               form.sanitaryRegistration.value = item[0]['sanitaryRegistration']
               form.auditorRecord.value = item[0]['auditorRecord']
               form.stock.value = item[0]['stock']
               form.und.value = item[0]['und']
               form.descripcion.value = item[0]['descripcion']
               var valradio = item[0]['idTypeOrderRequest']
               $("input[name=idTypeOrderRequest][value='"+valradio+"']").prop("checked",true);
               if(item[0]['idTypeOrderRequest'] == 2){
                 $("#showDetail").css("display", "block")
               }
               form.alertStock.value = item[0]['alertStock']
               form.countProvider.value = item[0]['countProvider']
            }
          }, error => {console.error("Failed!", error);})
        }, error => {console.error("Failed!", error);})
      }, error => {console.error("Failed!", error);})
    }, error => {
      //console.error("Failed!", error);
    })
}
/*========================================================================*/
function productUpdate() {
 $("#mdl-product").modal('hide')
 let url = 'Products_controller.php'
 const form = document.getElementById('form-product')
 let data = new FormData(form)
 data.append('update', 1)
 data.append('id', getIdproduct())
 postX(url, data).then(response => {
   products()
   message("Se ha guardado correctamente.")
 }, error => {
   //console.error("Failed!", error);
 })
}
 /*========================================================================*/
function productRemove(idProduct, id){
  if(id == null){
    message("Para remover un producto, es importante primero editar el producto.")
  }
  else{
    let url = 'Products_controller.php'
    let data = new FormData()
    data.append('delete', 1)
    data.append('id', id)
    let msg = "¿Confirmas que deseas remover el producto?"
    deleteItem(url, data, msg).then(response => {
      //console.log("Success!", response);
      message("Se ha removido correctamente.")
      products()
    }, error => {
      //console.error("Failed!", error);
    })
  }
}
/*========================================================================*/
function validateName(name){
 let url = 'Products_controller.php'
 let data = new FormData()
 data.append('validateName', 1)
 data.append('name', name)
 data.append('id', getIdproduct())
 postX(url, data).then(response2 => {
   if(response2 >0){
     message("ya se encuentra registrado el producto.")
   }
 }, error => {console.error("Failed!", error);})
}
 /*========================================================================*/
function categories(){
  let url = 'Products_controller.php'
  let data = new FormData()
  data.append('categories', 1)
  let div = '.categories'
  return selectOptions(url, data, div)
}
/*========================================================================*/
function subCategories(idCategory){
  let url = 'Products_controller.php'
  let data = new FormData()
  data.append('subCategories', 1)
  data.append('idCategory', idCategory)
  let div = '.subCategories'
  return selectOptions(url, data, div)
}
/*========================================================================*/
function productForCategory(idSubCategory){
  let url = 'Products_controller.php'
  let data = new FormData()
  data.append('productForCategory', 1)
  data.append('idSubCategory', idSubCategory)
  let div = '.products'
  return selectOptions(url, data, div)
}
/*========================================================================*/
function productsSelect(){
  let url = 'Products_controller.php'
  let data = new FormData()
  data.append('select', 1)
  let div = '.products'
  let text = 'Seleccione una opción'
  return selectOptions(url, data, div, text)
}


/*========================================================================*/
function addCategory() {
  alertify.prompt("Nombre de la categoria",
     function (val, ev) {
       ev.preventDefault();
       updateCategory(val)
     }, function(ev) {ev.preventDefault();}
   );
}
/*========================================================================*/
function updateCategory(name){
 let url = 'Products_controller.php'
 let data = new FormData()
 data.append('updateCategory', 1)
 data.append('name', name)
 data.append('id', 0)
 postX(url, data).then(response2 => {
   categories().then(response => {
     $('.categories').val(response2)
     $('.subCategories').empty()
     $('.subCategories').append('<option value="">Seleccione una opción</option>')
   }, error => {console.error("Failed!", error);})
 }, error => {console.error("Failed!", error);})
}
/*========================================================================*/
function addSubCategory() {
 var idCategory = $('#idCategory').val()
 if(idCategory>0){
   alertify.prompt("Nombre de la categoria",
      function (val, ev) {
        ev.preventDefault();
        updateSubCategory(val)
      }, function(ev) {
        ev.preventDefault();
      }
    );
 }
 else{
   message("Seleccione una categoria, antes de continuar.")
 }
}
/*========================================================================*/
function updateSubCategory(name){
var idCategory = $('#idCategory').val()
let url = 'Products_controller.php'
let data = new FormData()
data.append('updateSubCategory', 1)
data.append('name', name)
data.append('idCategory', idCategory)
data.append('id', 0)
postX(url, data).then(response2 => {
  subCategories().then(response => {
    $('.subCategories').val(response2)
  }, error => {console.error("Failed!", error);})
}, error => {console.error("Failed!", error);})
}
