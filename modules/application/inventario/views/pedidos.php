<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de Inventarios</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de Inventarios</h2>
      <h1 align="center" class="color-default">Pedidos</h1>
      <div class="col-md-12">
        <div class="col-md-1">
          <div class="pull-left">
            <a href="javascript:orderGo(0);" class="pull-left"><img src="../../../../images/pedidos1.png" style="width:90px;"/></a>
          </div>
        </div>
        <div class="col-md-10" style="margin-bottom: 130px;">
            <div class="col-md-12">
              <form id="form-order-filter" action="javascript:orderFilter()" method="post">
                <div class="col-md-12 content_notification">
                    <div class="col-md-5">
                      <div class="col-md-3">
                        <label class="color-default">Fecha inicio</label>
                      </div>
                      <div class="col-md-8">
                        <input type="date" name="dateInit" class="form-control" required />
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="col-md-3">
                        <label class="color-default">Fecha final</label>
                      </div>
                      <div class="col-md-8">
                        <input type="date" name="dateFinish" class="form-control" required />
                      </div>
                    </div>
                    <div class="col-md-2" align="center">
                      <button type="submit" class="btn btn-primary">Filtrar</button>
                    </div>
                    <div class="col-md-2" align="center">
                      <a href="javascript:reload()" class="btn btn-primary">Mostrar todos</a>
                    </div>
                </div>
              </form>
            </div>
            <table id="orders-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">Pedido N°</th>
                  <th align="center">Fecha</th>
                  <th align="center">Cantidad solicitada</th>
                  <th align="center">Recibido</th>
                  <th align="center">Acción</th>
                </tr>
              </thead>
              <tbody id="orders-data"></tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl_order" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">FORMULARIO DE REGISTRO PARA PEDIDOS</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <p style="color: #666;font-size: 1.5em;margin-left: 25px;">Resultados de búsqueda</p>
          <table id="orders-items-table" class="table table-bordered table-striped table-responsive" style="margin-left: 17px;">
            <thead>
              <tr>
                <th class="color-blue" style="width: 250px;">Producto</th>
                <th class="color-blue">Unidad de medida</th>
                <th class="color-blue" style="width: 250px;">Proveedor</th>
                <th class="color-blue">Valor</th>
                <th class="color-blue">Cantidad</th>
                <th class="color-blue">Total</th>
                <th class="color-blue">Acción</th>
              </tr>
            </thead>
            <form action="javascript:orderAddItem(0);" id="form-addItem">
              <tr>
                <td style="width: 250px;">
                  <div class="input-group" style="width: 250px;">
                    <select class="form-control products select2" id="emptyProduct" name="idProduct" style="width: 250px;" onchange="loadProduct(this.value)" required></select>
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <input type="text" class="form-control set-product-und" name="und" readonly required/>
                  </div>
                </td>
                <td style="width: 250px;">
                  <div class="input-group" style="width: 250px;">
                    <select class="form-control providers select2"  id="emptyProvider" name="idProvider" style="width: 250px;" required></select>
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <input type="number" class="form-control set-product-price" name="price" readonly required/>
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <input type="number" class="form-control set-product-count" name="count" min="1" onkeyup="priceForCount(this.value)" required/>
                  </div>
                </td>
                <td>
                  <div class="input-group">
                    <input type="text" class="form-control set-product-total set-total" name="total" readonly required/>
                  </div>
                </td>
                <td>
                  <div class="center block">
                    <button type="submit" class="brn btn-primary" style="border: 0px;"><i class="fa fa-check" style="font-size: 2em;" aria-hidden="true"></i></button>
                  </div>
                </td>
              </tr>
            </form>
          </table>
          <p style="color: #666;font-size: 1.5em;margin-left: 25px;margin-top: 100px;">Productos Seleccionados</p>
          <table id="orders-items-table" class="table table-bordered table-striped table-responsive" style="margin-left: 17px;">
            <thead>
                <tr>
                  <th class="color-blue" style="width: 250px;">Producto</th>
                  <th class="color-blue">Unidad de medida</th>
                  <th class="color-blue" style="width: 250px;">Proveedor</th>
                  <th class="color-blue">Valor</th>
                  <th class="color-blue">Cantidad</th>
                  <th class="color-blue">Total</th>
                  <th class="color-blue">Acción</th>
                </tr>
            </thead>
            <tbody id="orders-items-data">

            </tbody>
          </table>
          <form id="form-order" action="javascript:orderUpdate()" method="post">
            <input type="hidden" name="total"/>
            <input type="hidden" name="items" id="items">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4" style="display:none;">
                 <div class="form-group">
                   <label class="col-md-5 control-label label-form color-default">Fecha de creación:</label>
                   <div class="input-group col-md-6">
                     <input type="date" class="form-control today input-readonly no-border" name="date" readonly/>
                   </div>
                 </div>
                </div>
                <div class="col-md-4" style="display:none;">
                  <div class="form-group">
                    <label class="col-md-5 control-label label-form color-default">Número de pedido:</label>
                    <div class="input-group col-md-7">
                      <input type="text" class="form-control input-readonly get-uniquecode no-border" name="uniquecode" value="Sin generar" readonly/>
                    </div>
                  </div>
                </div>
                <!-- <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-5 control-label label-form color-default">Fecha de creación:</label>
                    <div class="input-group col-md-6">
                      <input type="date" class="form-control today input-readonly no-border" name="date" readonly/>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-5 control-label label-form color-default">Número de pedido:</label>
                    <div class="input-group col-md-7">
                      <input type="text" class="form-control input-readonly get-uniquecode no-border" name="uniquecode" value="Sin generar" readonly/>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label class="col-md-5 control-label label-form color-default">Total:</label>
                    <div class="input-group col-md-7">
                      <input type="text" class="form-control bg-white no-border" id="totalOrder" name="total"  readonly/>
                    </div>
                  </div>
                </div> -->
                <div class="col-md-8">

                </div>
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-md-8 control-label label-form color-default">Cantidad:</label>
                        <div class="input-group col-md-4">
                          <input type="text" class="form-control bg-white no-border center" id="cantidadtotal" name="cantidadtotal"  readonly/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-md-5 control-label label-form color-default">Total:</label>
                        <div class="input-group col-md-7">
                            <input type="text" class="form-control bg-white no-border" style="text-align: right;" id="totalOrder" name="total"  readonly/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="center block text-center">
                <button type="button" onclick="updateOrder2();" class="btn btn-lg btn-primary color-blue" style="margin-top: 30px;margin-bottom: 15px;">Generar pedido</button>
              </div>
            </div>
            <div class="col-xs-12">
              <div class="center block text-center">
                <button type="button" class="btn btn-default" style="margin-bottom: 15px;" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->

  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl_orderParcial" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">Detalle del pedido</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <!-- <p style="color: #666;font-size: 1.5em;margin-left: 25px;margin-top: 100px;">Productos Seleccionados</p> -->
          <table id="orders-recived-table" class="table table-bordered table-striped table-responsive" style="margin-left: 17px;width: 100%;">
            <thead>
                <tr>
                  <th class="color-blue" style="width: 250px;">Producto</th>
                  <th class="color-blue">Unidad de medida</th>
                  <th class="color-blue">Cantidad</th>
                  <th class="color-blue">Recibido</th>
                  <th class="color-blue">Acción</th>
                </tr>
            </thead>
            <tbody id="orders-recived-data"></tbody>
          </table>
          <div class="col-xs-12">
            <div class="center block text-center">
              <button type="button" class="btn btn-default" style="margin-bottom: 15px;" onclick="reload();">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->

  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
