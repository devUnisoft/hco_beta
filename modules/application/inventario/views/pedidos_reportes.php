<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de Inventarios</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de Inventarios</h2>
      <h1 align="center" class="color-default">Reporte de movimientos</h1>
      <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="margin-bottom: 130px;">
            <div class="col-md-12">
              <form id="form-report-filter" action="javascript:reportsFilter()" method="post">
                <div class="col-md-12 content_notification">
                    <div class="col-md-4">
                      <div class="col-md-3">
                        <label class="color-default">Fecha inicio</label>
                      </div>
                      <div class="col-md-8">
                        <input type="date" name="dateInit" class="form-control" required />
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="col-md-3">
                        <label class="color-default">Fecha final</label>
                      </div>
                      <div class="col-md-8">
                        <input type="date" name="dateFinish" class="form-control" required />
                      </div>
                    </div>
                    <div class="col-md-2" align="center">
                      <button type="submit" class="btn btn-primary">Filtrar</button>
                    </div>
                    <div class="col-md-2" align="center">
                      <a href="javascript:reload()" class="btn btn-primary">Mostrar todos</a>
                    </div>
                </div>
              </form>
            </div>
            <div class="col-md-12">
            <br>
            </div>
            <table id="reports-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">#</th>
                  <th align="center">Fecha</th>
                  <th align="center">Producto</th>
                  <th align="center">Categoría</th>
                  <th align="center">Sub-categoría</th>
                  <th align="center">Cantidad</th>
                  <th align="center">Tipo de movimiento</th>
                  <th align="center">Unid. medida</th>
                </tr>
              </thead>
              <tbody id="reports-data"></tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
