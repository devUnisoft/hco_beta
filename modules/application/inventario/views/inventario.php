<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de Inventarios</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de Inventarios</h2>
      <h1 align="center" class="color-default">Productos</h1>
      <div class="col-md-12">
        <div class="col-md-1">
          <div class="pull-left">
            <a href="javascript:productGo(0);" class="pull-left"><img src="../../../../images/productos1.png" style="width:90px;"/></a>
          </div>
        </div>
        <div class="col-md-10" style="margin-bottom: 130px;">
            <div class="col-md-12">
              &nbsp;
            </div>
            <table id="products-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">Producto</th>
                  <th align="center">Categoría</th>
                  <th align="center">Sub-categoría</th>
                  <th align="center">Tipo de pedido</th>
                  <th align="center">Proveedor primario</th>
                  <th align="center">Stock actual</th>
                  <th align="center">Unid. medida</th>
                  <th align="center">Precio</th>
                  <th align="center">Acción</th>
                </tr>
              </thead>
              <tbody id="products-data"></tbody>
            </table>
        </div>
      </div>
    </div>

    <!-- MODAL CREAR PACIENTE -->
    <div id="mdl-product" class="modal fade" data-backdrop="static" role="dialog">
      <div class="modal-dialog" style="width: 75%;">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body tt">
            <h2 align="center" style="margin-top: 90px;margin-bottom: 40px;" class="color-default">FORMULARIO DE REGISTRO PARA PRODUCTOS</h2>
            <form id="form-product" action="javascript:productUpdate()" method="post">
              <header align="center">
                <div class="col-md-12" align="center">
                  <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
                </div>
              </header>
              <div class="row content_notification">
                <div class="col-md-6">
                  <div class="col-md-3">
                    <label class="m10 color-default">Nombre del producto:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" name="name" class="form-control color-default m10" onchange="validateName(this.value)" required />
                  </div>
                </div>
                <div class="col-md-6 row">
                  <div class="col-md-3">
                    <label class="m10 color-default">Precio:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" name="price" onkeyup="numberFormat(this)" class="form-control color-default" required />
                  </div>
                </div>
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <div class="col-md-3 ">
                      <label class="m10 color-default">Categoría:</label>
                    </div>
                    <div class="col-md-6">
                      <select name="idCategory" id="idCategory" class="form-control categories m10" onchange="subCategories(this.value)" required></select>
                    </div>
                    <div class="col-md-3">
                      <a href="javascript:addCategory()" class="btn btn-primary center-icon color-blue circle m10"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-3">
                      <label class="m10 color-default">Sub-Categoría:</label>
                    </div>
                    <div class="col-md-6">
                      <select name="idSubCategory" class="form-control subCategories m10" required></select>
                    </div>
                    <div class="col-md-3">
                      <a href="javascript:addSubCategory()" class="btn btn-primary center-icon color-blue circle m10"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <div class="col-md-3">
                      <label class="m10 color-default">Registro sanitario:</label>
                    </div>
                    <div class="col-md-9">
                      <input type="text" name="sanitaryRegistration" class="form-control m10" required />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-3">
                      <label class="m10 color-default">Registro Auditor:</label>
                    </div>
                    <div class="col-md-9">
                      <input type="text" name="auditorRecord" class="form-control m10" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <div class="col-md-3">
                      <label class="m10 color-default">Cantidad:</label>
                    </div>
                    <div class="col-md-9">
                      <input type="number" name="stock" class="form-control m10" required />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-3">
                      <label class="m10 color-default">Unidad de medida:</label>
                    </div>
                    <div class="col-md-9">
                      <input type="text" name="und" class="form-control m10" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-12 row" style="padding-left: 30px;padding-right: 30px;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Descripción del Producto:</label>
                  </div>
                  <div class="col-md-9">
                    <textarea name="descripcion" rows="4" cols="40" class="form-control m10"></textarea>
                    <div class="row" style="width: 700px;">
                      <div class="col-md-4">
                        <label class="m10 color-default" style="text-align: right;float: right;">Solicitud de Pedido:</label>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox center" style="text-align: left;">
                          <label class="m10 color-default">
                            <input type="radio" name="idTypeOrderRequest" value="1" onchange="validatecheck2()" required> Pedido manual
                          </label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="checkbox center">
                          <label class="m10 color-default">
                            <input type="radio" id="validatecheck" name="idTypeOrderRequest" value="2" onchange="validatecheck2()"> Pedido automático
                          </label>
                        </div>
                        <div id="showDetail" style="display:none;border: solid #666 2px;border-radius: 7px;padding: 20px;width: 370px;">
                          <div class="row">
                            <div class="col-md-8">
                              <label class="m10 color-default">Stock minimo que activa pedido automático:</label>
                            </div>
                            <div class="col-md-4">
                              <input type="number" class="form-control m10" name="alertStock" id="alertStock1" />
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">
                              <label class="m10 color-default">Proveedor:</label>
                            </div>
                            <div class="col-md-8">
                              <select class="providers form-control m10" name="idProvider" id="idProvider1"></select>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">
                              <label class="m10 color-default">Cantidad a solicitar</label>
                            </div>
                            <div class="col-md-6">
                              <input type="number" class="form-control m10" name="countProvider" id="countProvider1" />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12" style="margin-top: 70px;">
                <div class="center">
                  <button type="submit" class="btn btn-primary" style="width: 200px;">Guardar</button>
                  <button type="button" class="btn btn-default" style="width: 200px;" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END MODAL CREAR PRODUCTO -->
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
