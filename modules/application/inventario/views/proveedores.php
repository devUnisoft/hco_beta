<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de Inventarios</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>

<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row">
      <h2 align="center" class="color-default">Administración de Inventarios</h2>
      <h1 align="center" class="color-default">Proveedores</h1>
      <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="margin-bottom: 130px;">
            <div class="pull-left">
              <a href="javascript:providerGo(0);" class="pull-left"><img src="../../../../images/proveedor1.png" style="width:90px;"/></a>
            </div>
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-4 pull-right">
                <form action="javascript:providerSearch();" id="form-providerSearch">
                  <h4 class="color-default">Busqueda por nombre</h4>
                  <div id="custom-search-input">
                    <div class="input-group col-md-12">
                      <input type="text" class="form-control input-sm" id="search" name="search" placeholder="Buscar..">
                      <span class="input-group-btn">
                            <button class="btn btn-info btn-sm" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  </div>
                </form>
              </div>
              <!-- <div class="col-md-1 pull-right">
                <a href="javascript:providers();"><img src="../../images/Proveedores.png" style="width: 60px;margin-top:10px;"/></a>
              </div> -->
            </div>
            <div class="row" id="providers-data">

            </div>
            <!-- <table id="providers-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">Nombre</th>
                  <th align="center">Identificación</th>
                  <th align="center">Email</th>
                  <th align="center">Teléfono</th>
                  <th align="center">Persona de contacto</th>
                  <th align="center">Acción</th>
                </tr>
              </thead>
              <tbody id="providers-data"></tbody>
            </table> -->
        </div>
      </div>
    </div>
  </div>

  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl-provider" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 75%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom: 40px;" class="color-default">FORMULARIO DE REGISTRO PARA PROVEEDORES</h2>
          <form id="form-provider" action="javascript:providerUpdate()" method="post">
            <header align="center">
              <div class="col-md-12" align="center">
                <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
              </div>
            </header>
            <div class="row content_notification">
              <div class="col-md-12">
                <div class="col-md-12">
                  <label class="m10 color-default">Nombre:</label>
                  <input type="text" class="form-control m10" name="name" required />
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="m10 color-default">Identificación:</label>
                  <input type="text" class="form-control m10" name="code" required />
                </div>
                <div class="col-md-6">
                  <label class="m10 color-default">Email:</label>
                  <input type="email" class="form-control m10" name="email" required />
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="m10 color-default">Teléfono:</label>
                  <input type="text" class="form-control m10" name="phone" required />
                </div>
                <div class="col-md-6">
                  <label class="m10 color-default">Persona de contacto:</label>
                  <input type="text" class="form-control m10" name="personContact" required />
                </div>
              </div>
              <div class="col-md-12">
                <div class="row center">
                  <div class="col-md-3">
                    <label class="m10 color-default">Productos:</label>
                  </div>
                  <div class="col-md-6">
                    <select name="idProduct" id="idProduct" class="form-control products m10"></select>
                  </div>
                  <div class="col-md-3">
                    <a href="javascript:addProviderProduct()" class="btn btn-primary center-icon color-blue circle m10"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
              <input type="hidden" name="providerProducts" id="providerProducts">
              <div class="col-md-12">
                <div class="row center">
                  <div class="col-md-3">

                  </div>
                  <style>
                    .fixed-table{
                      height: 250px;
                      overflow-y: scroll;
                    }
                    /* .fixed-table table thead{
                      position: fixed;
                      background: #fff;
                      width: 480px;
                    }
                    .fixed-table table tbody{
                      margin-top: 30px;
                      position: absolute;
                    } */

                  </style>
                  <div class="col-md-6 fixed-table">
                    <table id="providerProducts-table" class="table table-bordered table-hover" >
                      <thead>
                        <tr>
                          <th align="center">Nombre</th>
                          <th align="center">Acción</th>
                        </tr>
                      </thead>
                      <tbody id="providerProducts-data"></tbody>
                    </table>
                  </div>
                  <div class="col-md-3">

                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" style="margin-top: 70px;">
              <div class="center">
                <button type="submit" class="btn btn-primary" style="width: 200px;">Guardar</button>
                <button type="button" class="btn btn-default" style="width: 200px;" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->

  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>

</html>
