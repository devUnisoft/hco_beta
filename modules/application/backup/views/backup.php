<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de backup</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de backup</h2>
      <div class="col-md-12">
        <a href="javascript:backup()" class="btn btn-primary btn-lg"> Generar backup</a>
      </div>
    </div>
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
