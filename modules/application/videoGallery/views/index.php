<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Administración de presupuestos</title>
	<?php include '../../layouts/head.php';?>
	<?php include '../../layouts/css.php';?>
</head>
<body>
	<?php include '../../layouts/header.php';?>
	<?php include '../layouts/menuheader.php';?>
	<div class="container" style="margin-top: 40px;">
		<div class="row tt" style="margin-top: 62px; margin-bottom: 500px;" >
			<h2 align="center" class="color-default text-uppercase">Videos instructivos</h2>
			
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video1"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a><div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video1" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video2"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a><div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video2" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video3"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a>
			      <div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video3" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video4"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a>
			      <div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video4" href="#" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video5"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a>
			      <div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video5" href="#" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>
			  <div class="col-sm-6 col-md-4">
			    <div class="thumbnail">
			      <a href="#myModal" data-toggle="modal" id="video6"><img src="https://4vector.com/i/free-vector-rubik-s-cube-random-clip-art_106251_Rubiks_Cube_Random_clip_art_medium.png" alt=""></a>
			      <div class="caption">
			        <h3 class="text-center text-uppercase">Tutorial 1</h3>
			        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus et esse earum, dolor eos ut ad deleniti eaque deserunt quo. Quae molestiae, molestias quod sed nulla dignissimos similique tenetur repudiandae.</p>
			        <p><a href="#myModal" data-toggle="modal" id="video6" href="#" class="btn btn-primary btn-block" role="button">Ver Video</a>
			      </div>
			    </div>
			  </div>

		</div>

		<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">YouTube Video</h4>
                </div>
                <div class="modal-body">
                    <iframe id="cartoonVideo" width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

	</div>
	<!-- MODAL CREAR PACIENTE clinica de ortodoncia-->

	<!-- END MODAL CREAR PRODUCTO -->

	<?php include '../../modals/budgets.php';?>
	<?php include '../../modals/evolutions.php';?>
	<?php include '../layouts/footer.php';?>
	<?php include '../../layouts/libs.php';?>
	<?php include '../../layouts/js.php';?>
	<?php include '../layouts/js.php';?>
	<script type="text/javascript" src="../../evoluciones/js/evolution.js"></script>
	<script type="text/javascript" src="../../evoluciones/js/evolution.validate.js"></script>
	<script>

		$("#video3").click(function(){
			$("#cartoonVideo").attr('src', '../videos/HCOAdministracion.mp4');
		});
		$("#video2").click(function(){
			$("#cartoonVideo").attr('src', '../videos/HCOAnamnesis.mp4');
		});
		$('.modal').on('hidden.bs.modal', function () {
		 	$("#cartoonVideo").attr('src', '');
		})
	</script>

</body>
</html>
