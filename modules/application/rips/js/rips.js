/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */
 $(window).on("load", function (e) {
   ripsRouter()
 })
 /*========================================================================*/
 function ripsRouter(){
   dentistsSelect().then(response => {
     $(".select2").select2()
   }, error => {})
 }
/*========================================================================*/
function ripsFilter() {
  const form = document.getElementById('form-rips-filter')
  let data = new FormData(form)
  let url = 'Rips_controller.php'
  data.append('filter', 1)
  return postX(url, data).then(response => {
    Rrips(response)
  }, error => {})
}
 /*========================================================================*/
function Rrips(resp) {
  for (var i = 0; i < resp.length; i++) {
    generateDocument(resp[i].data, resp[i].title)
  }
}


function generateDocument(data, title){
  console.log(data);
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
  element.setAttribute('download', title);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}
