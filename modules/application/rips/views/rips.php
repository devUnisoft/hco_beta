<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de Rips</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de Rips</h2>
      <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="margin-bottom: 130px;">
          <form id="form-rips-filter" action="javascript:ripsFilter()" method="post">
            <div class="col-md-12 content_notification">
              <div class="col-md-2">
                <label class="color-default">Fecha incial:</label>
                <input type="date" class="form-control" name="dateInit">
              </div>
              <div class="col-md-2">
                <label class="color-default">Fecha final:</label>
                <input type="date" class="form-control" name="dateFinish">
              </div>
              <div class="col-md-2">
                <label class="color-default">Profesional:</label>
                <select class="form-control dentists select2" name="idProfesional"></select>
              </div>
              <div class="col-md-2" align="center">
                <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Filtrar</button>
              </div>
              <div class="col-md-2" align="center">
                <a href="javascript:reload()" class="btn btn-primary" style="margin-top: 25px;">Mostrar todos</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>

</html>
