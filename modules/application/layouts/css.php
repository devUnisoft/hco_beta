<style type="text/css">
  .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../../../../images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
  }


header h1#logo {
  display: inline-block;
  height: 150px;
  line-height: 150px;
  float: left;
  font-family: "Oswald", sans-serif;
  font-size: 60px;
  color: white;
  font-weight: 400;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
header nav {
  display: inline-block;
  float: right;
}
header nav a {
  line-height: 150px;
  margin-left: 20px;
  color: #9fdbfc;
  font-weight: 700;
  font-size: 18px;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
}
header nav a:hover {
  color: white;
}
header.smaller {
  height: 75px;
}
header.smaller h1#logo {
  width: 150px;
  height: 75px;
  line-height: 75px;
  font-size: 30px;
}
header.smaller nav a {
  line-height: 75px;
}

@media all and (max-width: 660px) {
  header h1#logo {
      display: block;
      float: none;
      margin: 0 auto;
      height: 100px;
      line-height: 100px;
      text-align: center;
  }
  header nav {
      display: block;
      float: none;
      height: 50px;
      text-align: center;
      margin: 0 auto;
  }
  header nav a {
      line-height: 50px;
      margin: 0 10px;
  }
  header.smaller {
      height: 75px;
  }
  header.smaller h1#logo {
      height: 40px;
      line-height: 40px;
      font-size: 30px;
  }
  header.smaller nav {
      height: 35px;
  }
  header.smaller nav a {
      line-height: 35px;
  }
}
#footer{
  position:fixed;
  width:100%;
  height:90px;
  background-color:#FFFFFF;
  color:#000000;
  bottom:0px;
  clear:both;
  z-index: 999;
}

 .media
  {
      /*box-shadow:0px 0px 4px -2px #000;*/
      margin: 20px 0;
      padding:30px;
  }
  .dp
  {
      border:10px solid #eee;
      transition: all 0.2s ease-in-out;
  }
  .dp:hover
  {
      border:2px solid #eee;

  }
.profile
{
  min-height: 300px;
  display: inline-block;
  }
figcaption.ratings
{
  margin-top:20px;
  }
figcaption.ratings a
{
  color:#f1c40f;
  font-size:11px;
  }
figcaption.ratings a:hover
{
  color:#f39c12;
  text-decoration:none;
  }
.divider
{
  border-top:1px solid rgba(0,0,0,0.1);
  }
.emphasis
{
  border-top: 4px solid transparent;
  }
.emphasis:hover
{
  border-top: 4px solid #1abc9c;
  }
.emphasis h2
{
  margin-bottom:0;
  }
span.tags
{
  background: #1abc9c;
  border-radius: 2px;
  color: #f5f5f5;
  font-weight: bold;
  padding: 2px 4px;
  }

/* .row {
  margin-left: -130px;
} */

.col-fixed {
  /* custom width */
  width:320px;
}
.col-min {
  /* custom min width */
  min-width:320px;
}
.col-max {
  /* custom max width */
  max-width:320px;
}

#menuinicial{
  position:fixed;
  width:100%;
  height:90px;
  background-color:#FFFFFF;
  color:#000000;
  clear:both;
  z-index: 100;
}

@media (min-width: 992px)
.col-md-6 {
  width: 50%;
}

.table td {
  text-align: left;
}

@media (max-width: 767px)
{
.container-fluid {
  padding: 0;
  padding-left: 50px;
}

.well {
  margin-left: 100px;
}


#footer{
  left: 0px;
}

body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 10px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff;
}

#imagenheadersuperior{
height:50px;
width:100px;
}

.patient{
display: none;
}

.profile {
  min-height: 300px;
  display: inline-block;
  margin-top: 100px;
}

.well{
margin-left: 0px;
}

#menuinicial {
  position: fixed;
  width: 100%;
  height: 90px;
  background-color: #FFFFFF;
  color: #000000;
  clear: both;
  z-index: 100;
  left: 0px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
  padding: 0;
  padding-left: 50px;
}
#footer{
  left: 0px;
}



a{
width: 100%;
float: none;
margin-bottom: 20px;
}


body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 10px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff;
}

#imagenheadersuperior{
height:50px;
width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;
}

.modal-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 0px;
}

#logodescripcion{
height:50%;
width:50%;
}

#logoeditar{
height:50%;
width:50%;
}

.patient{
display: none;
}

.profile {
  min-height: 300px;
  display: inline-block;
  margin-top: 100px;
}

#menuinicial {
  position: fixed;
  width: 100%;
  height: 90px;
  background-color: #FFFFFF;
  color: #000000;
  clear: both;
  z-index: 100;
  left: 0px;
}

.well {
  margin-left: 0px;
}

#footer {
  position: fixed;
  width: 100%;
  height: 90px;
  background-color: #FFFFFF;
  color: #000000;
  bottom: 0px;
  clear: both;
  z-index: 999;
}

}
.space100{
          width: 160px;
}
.space{
padding: 10px;
}

.color-blue{
  color: #46b8da;
}
.color-default{
  color:#337ab7;
}
table thead tr th{
  color: #46b8da;
}
table, thead, tr, th, tbody, td{
  border: 0px !important;
}

.circle{
      border-radius: 50% !important;
}
.m10{
  margin:10px;
}
.center{
  display: block;
  margin: auto;
  text-align: center;
}
.alertify .dialog div{
  background: #045c8d;
  color: #fff !important;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
}

.alertify .dialog nav button:not(.btn):not(.pure-button):not(.md-button):not(.mdl-button){
  color: #fff !important;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
}

.alertify .dialog div input{
  color: #444 !important;
  text-align: center !important;
}

.space100 .center i{
      font-size: 1.5em;
}

.select2{
  display: block;
    width: 100% !important;
}
</style>
