<!--Jquery-->
<script type="text/javascript" src="../../../libs/jquery-2.2.4/jquery-2.2.4.min.js"></script>
<!--datatables-->
<link rel="stylesheet" href="../../../libs/datatables/datatables.css">
<script type="text/javascript" src="../../../libs/datatables/datatables.js"></script>
<!--datatables-->
<link rel="stylesheet" href="../../../libs/bootstrap-3.3.7/css/bootstrap.css">
<script type="text/javascript" src="../../../libs/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<!--alertify-->
<link rel="stylesheet" href="../../../libs/alertify/css/alertify.css">
<script type="text/javascript" src="../../../libs/alertify/js/alertify.js"></script>
<!--fontawesome-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
