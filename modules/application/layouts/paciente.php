<?php

if(isset($_GET['pacienteEmail'])){
  $pacienteEmail = $_GET['pacienteEmail'];
  $sql = "SELECT *, if(gender = 'M', 'Masculino', 'Femenino') as genders FROM `users` WHERE `email`='$pacienteEmail'";
  $rec = mysql_query($sql);
  $image_url;
  $firstname;
  $lastname ;
  $document__document_type__oid;
  $documentnumber;
  $genero;
  $birthdate;
  $lugarnacimiento;
  $lugarresidencia;
  $id;
  $acudientenombre;
  while($row = mysql_fetch_object($rec))
  {
      $image_url = $row->image_url;
      $firstname = $row->first_name;
      $lastname = $row->last_name;
      $document__document_type__oid = $row->document__document_type__oid;
      $documentnumber = $row->document__number;
      $genero = $row->genders;
      $birthdate = $row->birthdate;
      $lugarnacimiento = $row->lugar_nacimiento;
      $lugarresidencia = $row->lugar_residencia;
      $id = $row->id;
      $acudientenombre = $row->acudientenombre;

  }

  function CalculaEdad( $fecha ) {
    list($Y,$m,$d) = explode("-",$fecha);
    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
}

  $edad = CalculaEdad($birthdate);

    $nombrePaciente = $firstname." ".$lastname;
  ?>
  <style type="text/css">
      .img-circle {
          border-radius: 100%;
          background-color: #ccc;
          overflow: hidden;
          padding: 3px;
      }

  </style>
  <div class="patient">
          <div class="img">
          <?php
              if(empty($image_url)){
          ?>
          <img  class="img-circle" src="../../../../images/paciente.png" height="50%" width="50%">
          <?php
              }else{
          ?>
           <img  class="img-circle" src="../../../../<?php echo $image_url ?>" height="50%" width="50%">
          <?php
              }
          ?>
          </div>
          <div class="detail-pata"></div>
          <div class="detail">
              <div class="text-center ng-binding">
                  <b class="ng-binding"><?php echo $firstname ?> <?php echo $lastname ?></b><br>
                  <?php echo $document__document_type__oid ?>: <?php echo $documentnumber ?><br>
                  <?php echo $edad ?> años
              </div>
              <br>
              <br>
              <div class="ng-binding">
                  <?php echo $genero ?><br>
                  <?php echo $birthdate ?><br>
                  <?php echo $lugarnacimiento ?>, <?php echo $lugarresidencia ?>
              </div>
              <?php
                         //paso documento a session para uso del path imagenes en scan.php
                        $_SESSION["documentnumber"]=$documentnumber;
                        ?>
              <div class="text-center edit">
                  <a href="../../../../modificarpaciente.php?usuario=<?= $pacienteEmail ?>"><img src="../../../../images/editar.png" title="Editar Paciente"  height="20px" width="20px"></a>
              </div>
          </div>
          </br>
          <div class="text-center foto">
               <center><a href="../../../../modificarpaciente.php?usuario=<?= $pacienteEmail ?>"><img src="../../../../images/foto1.png" title="Editar Imagen"  height="50px" width="50px"></a></center>
               <center>Foto</center>
          </div>
  </div>
<?php
}
?>
