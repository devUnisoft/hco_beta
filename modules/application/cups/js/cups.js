/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */
 $(window).on("load", function (e) {
   cupsRouter()
 })

 function setIdCups(id) {localStorage.setItem("idCups", id)}
 function getIdCups() {return localStorage.getItem("idCups")}


 /*========================================================================*/
 function cupsRouter(){
   simple('process')
 }
/*========================================================================*/
function simple(type) {
  let url = 'Cups_controller.php'
  let data = new FormData()
  data.append(type, 1)
  all(url, data).then(response => {
    Rsimple(type, response)
  }, error => {
    //console.error("Failed!", error);
  })
}
 /*========================================================================*/
function Rsimple(div,resp) {
  simplePaint(div,resp)
}
/*========================================================================*/
function simplePaint(div,resp) {
 $('#'+div+'-table').dataTable().fnDestroy()
 document.querySelector('#'+div+'-data').innerHTML= ""
 type = "'"+div+"'"
 if(resp != null){
   resp.map(row => {
     templateExtra = ""
     if(div == 'process'){
       templateExtra = '<td class="text-left">'+row.code+'</td><td class="text-left">'+row.price+'</td>'
     }
     let template ='<tr>'+
                     '<td class="text-left">'+row.name+'</td>'+templateExtra+'<td class="text-left">'+
                      '<a href="javascript:goSee('+type+', '+row.id+')" class="btn btn-primary">'+
                        '<i class="fa fa-eye"></i>'+
                      '</a>'+
                      '<a href="javascript:deleteSee('+type+', '+row.id+')" class="btn btn-danger">'+
                        '<i class="fa fa-times"></i>'+
                      '</a>'+
                      '</td>'+
                   '</tr>';
       $('#'+div+'-data').append(template);
   })
 }
 dataTable('#'+div+'-table',1)
}
/*========================================================================*/
var validateForm = 0
function goSee(type, id){
  setIdCups(id)
  validateForm = 1
  let url = 'Cups_controller.php'
  let data = new FormData()
  data.append(type+'Item', 1)
  data.append('id', getIdCups())
  let form = 'form-'+type
  item(url, data, form).then(item => {

  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function deleteSee(type, id){
  let url = 'Cups_controller.php'
  let data = new FormData()
  data.append(type+'Delete', 1)
  data.append('id', id)
  postX(url, data).then(response => {
    simple(type)
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function update(type) {
  const form = document.getElementById('form-'+type)
  let data = new FormData(form)
  let url = 'Cups_controller.php'
  data.append(type+'Update', 1)
  validate = 0
  if(validateForm == 1){
    validate = getIdCups();
  }
  data.append('id', validate)
  return postX(url, data).then(response => {
    simple(type)
  }, error => {})
}
