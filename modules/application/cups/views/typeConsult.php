<form id="form-typeConsult" action="javascript:update('typeConsult')" method="post">
  <div class="row">
    <div class="col-md-6">
      <label class="color-default">Nombre:</label>
      <input type="text" class="form-control" name="name" required>
    </div>
    <div class="col-md-2" align="center">
      <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Guardar</button>
    </div>
    <div class="col-md-2" align="center">
      <button type="button" class="btn btn-primary" onclick="reload();" style="margin-top: 25px;">Limpiar</button>
    </div>
  </div>
</form>
<table id="typeConsult-table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th align="center">Nombre</th>
      <th align="center">Acción</th>
    </tr>
  </thead>
  <tbody id="typeConsult-data"></tbody>
</table>
