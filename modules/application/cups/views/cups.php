<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de datos</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">Administración de datos</h2>
      <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="margin-bottom: 130px;">
          <ul class="nav nav-tabs" role="tablist" style="margin-top: -40px;display: block;width: 100%;left: 33%;">
            <li role="presentation" class="active"><a href="#process" onclick="simple('process')" aria-controls="process" role="tab" data-toggle="tab">Cups</a></li>
            <li role="presentation"><a href="#diagnostics" onclick="simple('diagnostics')" aria-controls="diagnostics" role="tab" data-toggle="tab">Diagnósticos</a></li>
            <li role="presentation"><a href="#originDisease" onclick="simple('originDisease')" aria-controls="originDisease" role="tab" data-toggle="tab">Origen de emfermedades</a></li>
            <!-- <li role="presentation"><a href="#procedures" onclick="simple('process')" aria-controls="procedures" role="tab" data-toggle="tab">Procedimientos</a></li> -->
            <li role="presentation"><a href="#typeConsult" onclick="simple('typeConsult')" aria-controls="typeConsult" role="tab" data-toggle="tab">Tipo de evolución</a></li>
          </ul>
          <div class="tab-content" style="margin-top: 75px;">
            <!--cups-->
            <div role="tabpanel" class="tab-pane active" id="process">
              <?php include 'process.php';?>
            </div>
            <!--diagnosticos-->
            <div role="tabpanel" class="tab-pane" id="diagnostics">
              <?php include 'diagnostics.php';?>
            </div>
            <!--origin-->
            <div role="tabpanel" class="tab-pane" id="originDisease">
              <?php include 'originDisease.php';?>
            </div>
            <!--procedures-->
            <div role="tabpanel" class="tab-pane" id="procedures">
              <?php include 'procedures.php';?>
            </div>
            <!--procedures-->
            <div role="tabpanel" class="tab-pane" id="typeConsult">
              <?php include 'typeConsult.php';?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>

</html>
