<form id="form-process" action="javascript:update('process')" method="post">
  <div class="row">
    <div class="col-md-4">
      <label class="color-default">Nombre:</label>
      <input type="text" class="form-control" name="name" required>
    </div>
    <div class="col-md-2">
      <label class="color-default">Código:</label>
      <input type="text" class="form-control" name="code" required>
    </div>
    <div class="col-md-2">
      <label class="color-default">Precio:</label>
      <input type="text" class="form-control" name="price" required>
    </div>
    <div class="col-md-2" align="center">
      <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Guardar</button>
    </div>
    <div class="col-md-2" align="center">
      <button type="button" class="btn btn-primary" onclick="reload();" style="margin-top: 25px;">Limpiar</button>
    </div>
  </div>
</form>
<table id="process-table" class="table table-bordered table-hover">
  <thead>
    <tr>
      <th align="center">Nombre</th>
      <th align="center">Código</th>
      <th align="center">Precio</th>
      <th align="center">Acción</th>
    </tr>
  </thead>
  <tbody id="process-data"></tbody>
</table>
