/**
 * inventario 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: febrary 10, 2018
 * Updated on: Febrary 10, 2018
 */

 function setIdparam(id) {localStorage.setItem("idparam", id)}
 function getIdparam() {return localStorage.getItem("idparam")}

 function setIdparamValidate(id) {localStorage.setItem("Validate", id)}
 function getIdparamValidate() {return localStorage.getItem("Validate")}
 /*========================================================================*/
 function paramsRouter(){
   params()
 }
/*========================================================================*/
function params() {
  let url = 'Params_controller.php'
  let data = new FormData()
  data.append('all', 1)
  all(url, data).then(response => {
    Rparams(response)
  }, error => {
    //console.error("Failed!", error);
  })
}
 /*========================================================================*/
function Rparams(resp) {
  $("#params-table").dataTable().fnDestroy()
  document.querySelector("#params-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {
      validate2 = "";
      if(row.validate == -1){
        validate2 = "Sin editar";
      }
      else{
        validate2 = "Editado";
      }

      let template ='<tr>'+
                      '<td class="text-left">'+row.orden+'</td>'+
                      '<td class="text-left">'+row.specialty+'</td>'+
                      '<td class="text-left">'+row.process+'</td>'+
                      '<td class="text-left">'+formatNumber.new(row.price)+'</td>'+
                      '<td class="text-left">'+validate2+'</td>'+
                      '<td class="text-left">'+row.validateCreate+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100">'+
                          '<a href="javascript:paramGo('+row.id+', '+row.validate+')" class="space"><i class="fa fa-pencil"></i></a>'+
                          '<a href="javascript:paramRemove('+row.id+', '+row.validate+')" class="space"><i class="fa fa-times"></i></a>'+
                        '</div>'+
                      '</td>'+
                    '</tr>';
        $("#params-data").append(template);
    })
  }
  $("#params-table").DataTable({
      responsive: true,
      "language": {
        "url": "../../../libs/datatables/Spanish.json"
      },
      "bDestroy": true,
      dom: 'Bfrtip',
      "order": [[ 4, "asc" ]],
      buttons: [
          'excel', 'pdf', 'print'
      ]
    });

  //dataTable("#params-table", 4)
}
/*========================================================================*/
function validateName(name, type){
 let url = 'Params_controller.php'
 let data = new FormData()
 data.append('type', type)
 data.append('name', name)
 return postX(url, data).then(response => {
   if(response >0){
     message("ya se encuentra registrado.")
   }
 }, error => {console.error("Failed!", error);})
}
/*========================================================================*/
function paramGo(id, validate){
 setIdparam(id)
 setIdparamValidate(validate)
 $('#mdl-param').modal('show');
 cleanForm("form-param")
 if(getIdparam() == 0){
   specialties()
   process(0)
 }
 else{
   param()
 }
}
/*========================================================================*/
function param() {
    let url = 'Params_controller.php'
    let data = new FormData()
    data.append('item', 1)
    data.append('id', getIdparam())
    data.append('validate', getIdparamValidate())
    let form = 'form-param'
    let folder = 'params'
    item(url, data, form, folder).then(item => {
      //console.log("Success!", response);
      specialties().then(response => {
        process(0).then(response => {
          if(getIdparam()>0){
            $('.specialties').val(item[0]['idSpecialty'])
            $('.process').val(item[0]['idProcess'])
            $('.price').val(item[0]['price'])
          }
        }, error => {console.error("Failed!", error);})
      }, error => {console.error("Failed!", error);})
    }, error => {
      //console.error("Failed!", error);
    })
}
/*========================================================================*/
function paramUpdate() {
 $("#mdl-param").modal('hide')
 let url = 'Params_controller.php'
 const form = document.getElementById('form-param')
 let data = new FormData(form)
 data.append('update', 1)
 data.append('id', getIdparam())
 data.append('validate', getIdparamValidate())
 postX(url, data).then(response => {
   params()
   message("Se ha guardado correctamente.")
   setIdparam(response.id)
 }, error => {
   //console.error("Failed!", error);
 })
}
 /*========================================================================*/
function paramRemove(id, validate){
  setIdparamValidate(validate)
  let url = 'Params_controller.php'
  let data = new FormData()
  data.append('delete', 1)
  data.append('id', id)
  data.append('validate', getIdparamValidate())
  let msg = "¿Confirmas que deseas remover el registro?"
  deleteItem(url, data, msg).then(response => {
    //console.log("Success!", response);
    message("Se ha removido correctamente.")
    params()
  }, error => {
    //console.error("Failed!", error);
  })
}
 /*========================================================================*/
function specialties(){
  let url = 'Params_controller.php'
  let data = new FormData()
  data.append('specialties', 1)
  if(getNameURLWeb() == 'presupuestos.php'){
    data.append('clinic', 1)
  }
  let div = '.specialties'
  return selectOptions(url, data, div)
}
/*========================================================================*/
function addSpecialty() {
  alertify.prompt("Nombre de la especialidad",
     function (val, ev) {
       ev.preventDefault();
       validateName(val, 'tbl_specialty').then(response => {
           if(response >0){
             message("ya se encuentra registrado.")
           }
           else{
             updateSpecialty(val)
           }
         },
         error => {console.error("Failed!", error);}
        )

     }, function(ev) {ev.preventDefault();}
   );
}
/*========================================================================*/
function updateSpecialty(name){
 let url = 'Params_controller.php'
 let data = new FormData()
 data.append('updateSpecialty', 1)
 data.append('name', name)
 data.append('id', 0)
 postX(url, data).then(response2 => {
   specialties().then(response => {
     $('.specialties').val(response2)
     $('.process').empty()
     $('.process').append('<option value="">Seleccione una opción</option>')
   }, error => {console.error("Failed!", error);})
 }, error => {console.error("Failed!", error);})
}
/*========================================================================*/
function process(idSpecialty){
  let url = 'Params_controller.php'
  let data = new FormData()
  data.append('process', 1)
  data.append('idSpecialty', idSpecialty)
  if(getNameURLWeb() == 'presupuestos.php'){
    data.append('clinic', 1)
  }
  let div = '.process'
  return selectOptions(url, data, div)
}

/*========================================================================*/
function addProcess() {
 var idSpecialty = $('#idSpecialty').val()
 if(idSpecialty != ""){
   alertify.prompt("Nombre del procedimiento",
      function (val, ev) {
        ev.preventDefault();
        validateName(val, 'tbl_process').then(response => {
            if(response >0){
              message("ya se encuentra registrado.")
            }
            else{
              updateProcess(val)
            }
          },
          error => {console.error("Failed!", error);}
         )
      }, function(ev) {
        ev.preventDefault();
      }
    );
 }
 else{
   message("Seleccione un tratamiento, antes de continuar.")
 }
}
/*========================================================================*/
function updateProcess(name){
var idSpecialty = $('#idSpecialty').val()
let url = 'Params_controller.php'
let data = new FormData()
data.append('updateProcess', 1)
data.append('idSpecialty', idSpecialty)
data.append('name', name)
data.append('id', 0)
postX(url, data).then(response2 => {
  process().then(response => {
    $('.process').val(response2)
  }, error => {console.error("Failed!", error);})
}, error => {console.error("Failed!", error);})
}

/*========================================================================*/
function paramExcelGo(){
  $('#mdl-excel').modal('show')
}
