<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de parametros generales</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <div class="container-fluid">
    <div class="row tt">
      <h2 align="center" class="color-default">PARÁMETROS GENERALES</h2>
      <div class="col-md-12">
        <div class="col-md-1">
          <div class="pull-left">
            <a href="javascript:paramGo(0, -1);" class="pull-left"><img src="../../../../images/globo_plus.png" style="width:50px;"/></a>
          </div>
        </div>
        <div class="col-md-10" style="margin-bottom: 130px;">
            <div class="col-md-12">
              &nbsp;
            </div>
            <table id="params-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">#</th>
                  <th align="center">Especialidad</th>
                  <th align="center">Procedimiento</th>
                  <th align="center">Valor</th>
                  <th align="center">Estado</th>
                  <th align="center">Creado</th>
                  <th align="center">Acción</th>
                </tr>
              </thead>
              <tbody id="params-data"></tbody>
            </table>
        </div>
      </div>
    </div>

    <div id="mdl-excel" class="modal fade" data-backdrop="static" role="dialog">
      <div class="modal-dialog" style="width: 75%;">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body tt">
            <h2 align="center" style="margin-top: 90px;margin-bottom: 40px;" class="color-default">FORMULARIO DE REGISTROS PARAMETROS GENERALES</h2>
              <header align="center">
                <div class="col-md-12" align="center">
                  <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
                </div>
              </header>
              <div class="row content_notification">
                  <div class="col-md-6">
                    <a href="#" class="btn btn-default">Subir Plantilla</a>
                  </div>
                  <div class="col-md-6">
                    <a href="#" class="btn btn-default">Descargar Plantilla</a>
                  </div>
              </div>
              <div class="col-md-12" style="margin-top: 70px;">
                <div class="center">
                  <button type="button" class="btn btn-default" style="width: 200px;" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL CREAR PACIENTE -->
    <div id="mdl-param" class="modal fade" data-backdrop="static" role="dialog">
      <div class="modal-dialog" style="width: 75%;">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body tt">
            <h2 align="center" style="margin-top: 90px;margin-bottom: 40px;" class="color-default">FORMULARIO DE REGISTROS PARAMETROS GENERALES</h2>
            <form id="form-param" action="javascript:paramUpdate()" method="post">
              <header align="center">
                <div class="col-md-12" align="center">
                  <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
                </div>
              </header>
              <div class="row content_notification">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-3">
                        <label class="m10 color-default">Especialidad:</label>
                      </div>
                      <div class="col-md-8">
                        <select name="idSpecialty" id="idSpecialty" class="form-control specialties m10" onchange="process(this.value);" required></select>
                      </div>
                      <div class="col-md-1">
                        <a href="javascript:addSpecialty()" class="btn btn-primary center-icon color-blue circle m10"><i class="fa fa-plus"></i></a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <label class="m10 color-default">Procedimiento:</label>
                      </div>
                      <div class="col-md-8">
                        <select name="idProcess" class="form-control process m10" required></select>
                      </div>
                      <div class="col-md-1">
                        <a href="javascript:addProcess()" class="btn btn-primary center-icon color-blue circle m10"><i class="fa fa-plus"></i></a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <label class="m10 color-default">Valor:</label>
                      </div>
                      <div class="col-md-8">
                        <input type="text" name="price" onkeyup="numberFormat(this)" class="form-control price color-default"  style="margin-left: 9px;" required />
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-12" style="margin-top: 70px;">
                <div class="center">
                  <button type="submit" class="btn btn-primary" style="width: 200px;">Guardar</button>
                  <button type="button" class="btn btn-default" style="width: 200px;" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- END MODAL CREAR PRODUCTO -->
  </div>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
