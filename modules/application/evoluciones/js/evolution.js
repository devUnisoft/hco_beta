/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function setIdEvolution(id) {localStorage.setItem("idEvolution", id)}
function getIdEvolution() {return localStorage.getItem("idEvolution")}

/*========================================================================*/
function EvolutionsRouter(){

}
/*========================================================================*/
function Evolutions(pacienteEmail) {
  let url = 'Evolutions_controller.php'
  let data = new FormData()
  data.append('all', 1)
  data.append('pacienteEmail', pacienteEmail)
  all(url, data).then(response => {
    console.log("Success!", response);
    REvolutions(response, pacienteEmail)
  }, error => {
    console.error("Failed!", error);
  })
 }
/*========================================================================*/
function EvolutionFilter(pacienteEmail) {
  const form = document.getElementById('form-Evolution-filter')
  let data = new FormData(form)
  let url = 'Evolutions_controller.php'
  data.append('filter', 1)
  data.append('pacienteEmail', pacienteEmail)
  return all(url, data).then(response => {
    REvolutions(response, pacienteEmail)
  }, error => {})
}

function REvolutions(resp, pacienteEmail) {
  $("#Evolutions-table").dataTable().fnDestroy()
  $("#Evolutions-data").empty();
  if(resp != null){
    resp.map(row => {

      var date = "'"+row.dateSave+"'"

      var pacienteEmails = "'"+pacienteEmail+"'"

      let template ='<tr>'+
                      '<td class="text-left">'+row.dateSave+'</td>'+
                      '<td class="text-left">'+row.type+'</td>'+
                      '<td class="text-left">'+row.idTypeEvolution+'</td>'+
                      '<td class="text-center">'+row.dateSterilization+'</td>'+
                      '<td class="text-center">'+row.idEvolutionTypeProcess+'</td>'+
                      '<td class="text-center">'+row.idEvolutionDiagnostic+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.price)+'</td>'+
                      '<td class="text-center">'+row.idOriginDisease+'</td>'+
                      '<td class="text-center">'+row.idProfesional+'</td>'+
                      '<td class="text-center">'+row.observations+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100"><a href="javascript:EvolutionGo('+row.id+', 1)" class="space"><i class="fa fa-eye"></i></a>'+
                        '<a href="javascript:EvolutionShare('+pacienteEmails+', '+date+')" class="space"><i class="fa fa-share"></i></a></div>'+
                      '</td>'+
                    '</tr>';
        $("#Evolutions-data").append(template);
    })
  }
  dataTable("#Evolutions-table", 1)
}
/*========================================================================*/
function EvolutionShare(pacienteEmail, date){
  window.open('../../../../compartirevolucionhistorial.php?usuario='+pacienteEmail+'&fecha='+date)
}
  /*========================================================================*/
  function EvolutionDelete(id){
    let url = 'Evolutions_controller.php'
    let data = new FormData()
    data.append('delete', 1)
    data.append('id', id)
    let msg = "¿Confirmas que deseas remover el presupuesto?"
    deleteItem(url, data, msg).then(response => {
      //console.log("Success!", response);
      message("Se ha actualizado correctamente.")
      Evolutions()
    }, error => {
      //console.error("Failed!", error);
    })
  }
/*========================================================================*/
function EvolutionGo(id, type){
    setIdEvolution(id)
    $("#mdl_Evolution").modal('show')
    cleanForm("form-Evolution")
    if(type == 0){
      $("#hide-btn2").css('display', 'none')
    }
    else{
        $("#hide-btn2").css('display', 'block')
    }
    typeConsult().then(response => {
      process().then(response => {
        diagnostics().then(response => {
          originDisease().then(response => {
            dentistsSelect().then(response => {
              $('.select2').select2();
              Evolution()
            }, error => {reject(Error('Request failed', error))})
          }, error => {reject(Error('Request failed', error))})
        }, error => {reject(Error('Request failed', error))})
      }, error => {reject(Error('Request failed', error))})
    }, error => {reject(Error('Request failed', error))})
}
 /*========================================================================*/
function Evolution() {
  if(getIdEvolution()>0){
    let data = new FormData()
    let url = 'Evolutions_controller.php'
    data.append('item', 1)
    data.append('id', getIdEvolution())
    let form = 'form-Evolution'
    let folder = 'Evolutions'
    let folderCategory = 'documents'
    item(url, data, form, folder, folderCategory).then(response => {
     $('.originDisease').val(response.item[0].idOriginDisease).trigger('change');
     $('.dentists').val(response.item[0].idProfesional).trigger('change');
     $('.process').val(response.process).trigger('change');
     $('.diagnostics').val(response.diagnosis).trigger('change');
     $('.typeConsult').val(response.item[0].idTypeConsult).trigger('change');
     if(response.item[0].idBudget>0){
       document.getElementById('selectBudget').style.display = 'block';
       //document.getElementById('selectConsulting').style.display = 'none';
       $('.budgets').val(response.item[0].idBudget).trigger('change');
     }
     else{

       document.getElementById('selectBudget').style.display = 'none';
       document.getElementById('selectConsulting').style.display = 'block';
     }


    }, error => {
      reject(Error('Request failed', error))
    })
  }
}
 /*========================================================================*/
function updateEvolution() {
  $("#mdl_Evolution").modal('hide')
  let url = 'Evolutions_controller.php'
  const form = document.getElementById('form-Evolution')
  let data = new FormData(form)
  data.append('update', 1)
  data.append('id', getIdEvolution())
  postX(url, data).then(response => {
    console.log("Success!", response);
    reload()
  }, error => {
    console.error("Failed!", error);
  })
}
