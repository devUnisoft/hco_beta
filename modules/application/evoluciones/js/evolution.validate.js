/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

/*========================================================================*/
function generateConsentimiento(){

}


function validateEvolution(val){
  if(val == 1){
    document.getElementById('selectBudget').style.display = 'block';
    document.getElementById('selectConsulting').style.display = 'none';
    validateCheckPrice()
  }
  else{
    document.getElementById('selectBudget').style.display = 'none';
    document.getElementById('selectConsulting').style.display = 'block';
  }
}

function validateCheckPrice(){
   var confirm = document.getElementById("aditionalCost");
   if(confirm.checked==false)
   {
      $("#hidePay").css("display", "none")
      $("#aditionalCost").val(0)
   }
   else{
     $("#hidePay").css("display", "block")
     $("#aditionalCost").val(1)
   }
}

function validateCheckPrice2(val){
  if(val==1){
    validateCheckPrice()
  }
  else if(val==2)
  {
     $("#hidePay").css("display", "block")
     $("#aditionalCost").val(1)
  }
  else{
    $("#hidePay").css("display", "none")
    $("#aditionalCost").val(0)
  }
}

function EvolutionApplyDiscount(){
  let discount = $("#discountEvolution").val()
  let total = $("#TotalEvolution").val()
  if(discount > 0){
    let subt = removeNumber(total)
    let desc = subt - ( subt * ( discount / 100) )
    $('#TotalEvolution').val(formatNumber.new(desc))
  }
}

function addpriceEvolution(){
  var process = $("#addpriceEvolution1").val();
  let url = 'Evolutions_controller.php'
  let data = new FormData()
  data.append('priceCups', process)
  all(url, data).then(response => {
    console.log("Success!", response);
    $('#TotalEvolution').val(formatNumber.new(response))
    EvolutionApplyDiscount()
  }, error => {
    console.error("Failed!", error);
  })
}


/*========================================================================*/
function typeConsult(){
 let url = 'Evolutions_controller.php'
 let data = new FormData()
 data.append('typeConsult', 1)
 let div = '.typeConsult'
 return selectOptions(url, data, div, 1)
}
/*========================================================================*/
function process(){
 let url = 'Evolutions_controller.php'
 let data = new FormData()
 data.append('process', 1)
 let div = '.process'
 return selectOptions(url, data, div, 1)
}
/*========================================================================*/
function diagnostics(){
 let url = 'Evolutions_controller.php'
 let data = new FormData()
 data.append('diagnostics', 1)
 let div = '.diagnostics'
 return selectOptions(url, data, div, 1)
}
/*========================================================================*/
function originDisease(){
 let url = 'Evolutions_controller.php'
 let data = new FormData()
 data.append('originDisease', 1)
 let div = '.originDisease'
 return selectOptions(url, data, div, 1)
}
