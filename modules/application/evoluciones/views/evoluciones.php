<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de presupuestos</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <?php include '../../pagos/layouts/menuheader.php';?>
  <div class="container-fluid" style="margin-top: 120px;">
    <h2 align="center" class="color-default">Evolución</h2>
    <div class="row">
      <div class="col-md-2">
        <?php include '../../layouts/paciente.php';?>
      </div>
      <div class="col-md-1">
        <div class="pull-left">
          <a href="javascript:EvolutionGo(0, 2);" class="pull-left"><img src="../../../../images/globo_plus.png" style="width:50px;"/></a>
        </div>
      </div>
      <div class="col-md-8" style="margin-bottom: 130px;">
        <table id="Evolutions-table" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th align="center">Fecha</th>
              <th align="center">Tipo</th>
              <th align="center">Tipo de consulta</th>
              <th align="center">Fecha de Esterilización</th>
              <th align="center">Tipo de procedimiento</th>
              <th align="center">Diagnóstico principal</th>
              <th align="center">Valor procedimiento</th>
              <th align="center">Origen de la enfermedad actual</th>
              <th align="center">Profesional</th>
              <th align="center">Evolución</th>
              <th align="center">Acción</th>
            </tr>
          </thead>
          <tbody id="Evolutions-data"></tbody>
        </table>
      </div>
      <div class="col-md-1">
        <a href="../../../../compartirevolucion.php?usuario=<?php echo $_GET['pacienteEmail']; ?>">
          <img title="Compartir Evolucion"
               src="../../../../images/compartirlogo.png"
               onmouseover="this.src='../../../../images/compartir_azul.png'"
               onmouseout="this.src='../../../../images/compartirlogo.png';"
               style="width: 50px;height: 50px;margin-top: 26px;" />
        </a>
      </div>
    </div>
  </div>
  <?php include '../../modals/evolutions.php';?>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>

  <script type="text/javascript">
    $(window).on("load", function (e) {
      Evolutions('<?= $_GET['pacienteEmail'];?>')
      budgetsSelect('<?= $_GET['pacienteEmail'];?>')
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>

</html>
