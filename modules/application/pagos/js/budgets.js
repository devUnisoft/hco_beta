/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function setIdBudget(id) {localStorage.setItem("idBudget", id)}
function getIdBudget() {return localStorage.getItem("idBudget")}

/*========================================================================*/
function budgetsRouter(){
  //budgets()
}
/*========================================================================*/
function budgetsSelect(pacienteEmail){
 let url = 'Budgets_controller.php'
 let data = new FormData()
 data.append('autorize', 1)
 data.append('pacienteEmail', pacienteEmail)
 let div = '.budgets'
 return selectOptions(url, data, div)
}
/*========================================================================*/
function total(pacienteEmail){
  var totaly = 0
  let url = 'Pays_controller.php'
  let data = new FormData()
  data.append('all', 1)
  data.append('pacienteEmail', pacienteEmail)
  all(url, data).then(response => {
    console.log("Success!", response);
    totaly = response.totalSaldo
    console.log(totaly)
    let url = 'Pays_controller.php'
    let data = new FormData()
    data.append('evolutions', 1)
    data.append('pacienteEmail', pacienteEmail)
    all(url, data).then(response => {
      console.log("Success!", response);
      totaly = response.totalSaldo + totaly
      console.log(totaly)
      $("#totalAbonos").html(formatNumber.new(totaly))
    }, error => {
      console.error("Failed!", error);
    })
  }, error => {
    console.error("Failed!", error);
  })
}
/*========================================================================*/
function budgets(pacienteEmail) {
  total(pacienteEmail)
  if(getIdPatient()>0){
    noPatient()
  }
  else {
    $("#content-hide").css('display','block')
    setIdPatient(0)
    let url = 'Budgets_controller.php'
    let data = new FormData()
    data.append('all', 1)
    data.append('pacienteEmail', pacienteEmail)
    all(url, data).then(response => {
      console.log("Success!", response);
      Rbudgets(response)
    }, error => {
      console.error("Failed!", error);
    })
  }
 }
/*========================================================================*/
function budgetFilter(pacienteEmail) {
  total(pacienteEmail)
  const form = document.getElementById('form-budget-filter')
  let data = new FormData(form)
  let url = 'Budgets_controller.php'
  data.append('filter', 1)
  data.append('pacienteEmail', pacienteEmail)
  return all(url, data).then(response => {
    Rbudgets(response)
  }, error => {})
}

function Rbudgets(resp) {
  $("#budgets-table").dataTable().fnDestroy()
  document.querySelector("#budgets-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {
      templateBtn = ""
      if(row.changeToPay == "Pendiente"){
        templateBtn = '<a href="javascript:changeToPay('+row.id+')" class="space"><i class="fa fa-check"></i></a>'
      }

      var changeToPay = "'"+row.changeToPay+"'"

      let template ='<tr>'+
                      '<td class="text-left">'+row.dateSave+'</td>'+
                      '<td class="text-left">'+row.name+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.initial)+'</td>'+
                      '<td class="text-center">'+row.sessions+'</td>'+
                      '<td class="text-center">'+row.discount+'%</td>'+
                      '<td class="text-center">'+formatNumber.new(row.total)+'</td>'+
                        '<td class="text-center">'+row.changeToPay+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100">'+templateBtn+'<a href="javascript:BudgetGo('+row.id+', '+changeToPay+')" class="space"><i class="fa fa-eye"></i></a></div>'+
                      '</td>'+
                    '</tr>';
        $("#budgets-data").append(template);
    })
  }
  dataTable("#budgets-table",1)
}

  /*========================================================================*/
  function bugetDelete(id){
    let url = 'Budgets_controller.php'
    let data = new FormData()
    data.append('delete', 1)
    data.append('id', id)
    let msg = "¿Confirmas que deseas remover el presupuesto?"
    deleteItem(url, data, msg).then(response => {
      //console.log("Success!", response);
      message("Se ha actualizado correctamente.")
      budgets()
    }, error => {
      //console.error("Failed!", error);
    })
  }
/*========================================================================*/
function BudgetGo(id, type){
  if(type == "Pendiente"){
    $("#changeBTNS").css("display", "block")
  }
  else{
    $("#changeBTNS").css("display", "none")
  }
  setIdBudget(id)
  cleanForm("form-budget")
  cleanForm("form-addItem")
  $("#budgets-items-data").empty()
  budgetsItems = 0
  budgetsItems2 = 0
  index = 0
  totalBudget = 0
  subtotalBudget = 0
  ivaBudget = 0
  loadPreBudget().then(response => {
     console.log("Success!", response);
     $("#mdl_budget").modal('show')
     $('.select2').select2({
      placeholder: 'Seleccione una especialidad'
    });
     Budget()
   }, error => {
     console.error("Failed!", error);
   })
}
 /*========================================================================*/
function Budget() {
  $("#changeBTNS").html("Generar presupuesto")
  if(getIdBudget()>0){
    $("#changeBTNS").html("Modificar presupuesto")
    let data = new FormData()
    let url = 'Budgets_controller.php'
    data.append('item', 1)
    data.append('id', getIdBudget())
    let form = 'form-budget'
    let folder = 'budgets'
    let folderCategory = 'documents'
    item(url, data, form, folder, folderCategory).then(response => {
     console.log("Success!", response.details)
     $('#idDentist').val(response.item[0].idProfesional).trigger('change');
     var data = response.details;
     var last = 0
     for (var i = 0; i < data.length; i++) {
       var idSpecialty = data[i].idSpecialty
       var idProcess = data[i].idProcess
       var price = data[i].price
       var specialty = data[i].specialty
       var process = data[i].process
       let inputprice = generateInput('text', 'price-'+i, formatNumber.new(price), "changePriceItem", "", "")
       let item = {specialty:specialty, process:process, price:inputprice, id:i};
       BudgetsItems[i] = item
       //json
       let item2 = {idSpecialty:idSpecialty, idProcess:idProcess, price:price,id:i};
       BudgetsItems2[i] = item2
       last = i
     }
     last = last+1;
     index = last;
     $("#form-addItem").attr('action', 'javascript:BudgetAddItem('+index+');')
     discountBudget(0)
    }, error => {
      reject(Error('Request failed', error))
    })
  }
}
 /*========================================================================*/
function BudgetUpdate() {
  $("#mdl_budget").modal('hide')
  let url = 'Budgets_controller.php'
  const form = document.getElementById('form-budget')
  let data = new FormData(form)
  data.append('update', 1)
  data.append('id', getIdBudget())
  postX(url, data).then(response => {
    console.log("Success!", response);
      setIdBudget(response.id)
      if($("#pacienteEmail1").val() == 1){
        $("#mdl_patients").modal('show')
        patients().then(response => {
           console.log("Success!", response);
           $('.select2').select2({
            placeholder: 'Seleccione un paciente'
          });
         }, error => {
           console.error("Failed!", error);
         })
      }
      else{
        sendBudget(getIdBudget())
      }
    budgets()
  }, error => {
    console.error("Failed!", error);
  })
}

function sendBudget(idBudget){
  let url = 'Budgets_controller.php'
  let data = new FormData()
  data.append('idBudget', idBudget)
  data.append('templatePDF', 1)

  postX(url, data).then(response => {
    console.log("Success!", response);
    sendPDF(response)
  }, error => {
    console.error("Failed!", error);
  })
}
