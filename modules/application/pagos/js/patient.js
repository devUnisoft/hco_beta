/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */
 function setIdPatient(id) {localStorage.setItem("idPatient", id)}
 function getIdPatient() {return localStorage.getItem("idPatient")}
 /*========================================================================*/
 function patients(){
  let url = 'Patient_controller.php'
  let data = new FormData()
  data.append('all', 1)
  let div = '.patients'
  return selectOptions(url, data, div)
 }
 /*========================================================================*/
 function validatePatient(idPatient){
   if(idPatient == 0){
     setIdPatient(0)
     $("#mdl_patient").modal('show')
   }
   else{
     let data = new FormData()
     let url = 'Patient_controller.php'
     data.append('updateBudget', 1)
     data.append('idUser', idPatient)
     data.append('idBudget', getIdBudget())
     postX(url, data).then(response => {
       console.log("Success!", response);
       $("#mdl_patients").modal('hide')
       message("Se ha guardado correctamente.")
       sendBudget(getIdBudget())
       $("#mdl_budget").modal('show')
     }, error => {
       console.error("Failed!", error);
     })
   }
 }
  /*========================================================================*/
 function closeModalPatient(){
   $("#mdl_patients").modal('hide')
   $("#mdl_patient").modal('hide')
    $("#mdl_budget").modal('show')
 }
 /*========================================================================*/
function addPatient() {
  $("#mdl_patient").modal('hide')
  let url = 'Patient_controller.php'
  const form = document.getElementById('form-patient')
  let data = new FormData(form)
  data.append('update', 1)
  data.append('id', 0)
  data.append('idBudget', getIdBudget())
  postX(url, data).then(response => {
    console.log("Success!", response);
    message("Se ha guardado correctamente.")
     $("#mdl_budget").modal('show')
     sendBudget(getIdBudget())
  }, error => {
    console.error("Failed!", error);
  })
}
 /*========================================================================*/
function noPatient(){
  setIdPatient(0)
  BudgetGo(0, "Pendiente")
  $("#content-hide").css('display','none')
}
