/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function loadPreBudget(){
  fechaActual()
  dentistsSelect()
  return specialties()
}
/*========================================================================*/
function loadProcess(idProcess){
  let data = new FormData()
  let url = 'Params_controller.php'
  data.append('itemProcess', 1)
  data.append('idProcess', idProcess)
  data.append('idSpecialty', set("#idSpecialty").value)
  postX(url, data).then(response => {
    set(".set-process-price").value = formatNumber.new(response[0]['price'])
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function updateBudget2(){
  if(BudgetsItems2.length>0){
    const form = document.getElementById("form-budget")
      set("#items").value = JSON.stringify(BudgetsItems2)
      BudgetUpdate()
  }
  else{
    message("Ingrese almenos un procedmiento.")
  }
}
/*========================================================================*/
var BudgetsItems = []
var BudgetsItems2 = []
var index = 0
var totalBudget = 0
function BudgetAddItem(x) {
  price = removeNumber($('#validatePrice').val())
  if(!price>0){
    message("Ingrese un valor mayor que 0")
  }
  else{
    let gtotal = 0
    let iva = 0
    console.log(x)
    const form = document.getElementById("form-addItem")
    let idSpecialty = form.idSpecialty.value
    let idProcess = form.idProcess.value
    let price = form.price.value
    console.log(price)
    var validserv = []
    let specialty = form.idSpecialty.options[form.idSpecialty.options.selectedIndex].text
    let process = form.idProcess.options[form.idProcess.options.selectedIndex].text
    var a = -1;
    if(BudgetsItems2.length>0){
      for(var i = 0; i < 200; i++)
      {
        if(BudgetsItems2[i]){
          if(BudgetsItems2[i].idProcess){
            if(BudgetsItems2[i].idProcess == idProcess)
            {
              validserv.push(BudgetsItems2[i].idProcess);
              console.log(BudgetsItems2[i].idProcess )
            }
          }
        }
      }
      a = validserv.indexOf(idProcess);
    }

    if(a < 0) {
      //table
      console.log(price)
      let inputprice = generateInput('text', 'price-'+x, price, "changePriceItem", "numberFormat", "")
      let item = {specialty:specialty, process:process, price:inputprice, id:index};
      BudgetsItems[x] = item
      //json
      price = removeNumber(price)
      console.log(price)
      let item2 = {idSpecialty:idSpecialty, idProcess:idProcess, price:price,id:index};
      BudgetsItems2[x] = item2
      console.log(BudgetsItems)
      index++
      document.getElementById("form-addItem").reset();
      $('#idSpecialty').val('').trigger('change');
      $('#idProcess').val('').trigger('change');
      form.setAttribute("action", "javascript:BudgetAddItem("+index+")")
      updateviewitems2()
    }
    else{
      message("El procedimiento ya se encuentra ingresado.")
    }
  }
}

let inp
/* ======================================================================== */
function changePriceItem(input){
  if(input.value>0){
    inp = input
    console.log(inp)
    var ls = inp.id.split("-")
    let inputprice = generateInput('text', 'price-'+ls[1], inp.value, "changePriceItem", "numberFormat", "")
    BudgetsItems[ls[1]].price = inputprice
    price = removeNumber(inp.value)
    BudgetsItems2[ls[1]].price = price
    discountBudget(0)
  }
  else{
    input.value = 0
  }
}
/* ======================================================================== */
function discountBudget(val){
  updateviewitems2()
  if(val > 0){
    let subt = removeNumber($('#totalBudget').val())
    let desc = subt - ( subt * ( val / 100) )
    $('input[name="total"]').val(formatNumber.new(desc))
  }
  else{
    inputDiscount = $('#inputDiscount').val()
    if(inputDiscount>0){
      let subt = removeNumber($('#totalBudget').val())
      let desc = subt - ( subt * ( inputDiscount / 100) )
      $('input[name="total"]').val(formatNumber.new(desc))
    }
  }
}
/* ======================================================================== */
function BudgetDeleteItem(idArray){
  alertify.confirm("¿Confirmas que deseas remover el procedimiento?", function () {
    console.log(BudgetsItems)
    posi = search(idArray, BudgetsItems)
    posi2 = search(idArray, BudgetsItems2)
    BudgetsItems.splice(posi, 1);
    BudgetsItems2.splice(posi2, 1);
    updateviewitems2();
  }, function() {}
  )
}
function BudgetViewItems(){
  //BudgetUpdate(true).then(response => {Budget()}, error => {})
  $("#budgets-items-data").empty()
  let btnDelete = ['times', 'danger', 'BudgetDeleteItem']
  let btns = new Array()
  btns.push(btnDelete)
  console.log(BudgetsItems)
  table(BudgetsItems, "budgets-items", btns, 0)
}


function updateviewitems2(){
  $('#totalBudget').val(0)
  if(BudgetsItems.length>0){
    let gtotal = 0
    for(i=0;i<BudgetsItems.length;i++){
     if(BudgetsItems2[i]){
       gtotal = gtotal +  parseInt(BudgetsItems2[i].price)
       console.log(gtotal)
     }
    }
    $('#totalBudget').val(formatNumber.new(gtotal))
  }

  inputDiscount = $('#inputDiscount').val()
  if(inputDiscount>0){
    let subt = removeNumber($('#totalBudget').val())
    let desc = subt - ( subt * ( inputDiscount / 100) )
    $('input[name="total"]').val(formatNumber.new(desc))
  }

  BudgetViewItems()
}
