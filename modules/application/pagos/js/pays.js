/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function setIdPay(id) {localStorage.setItem("idPay", id)}
function getIdPay() {return localStorage.getItem("idPay")}

/*========================================================================*/
function paysRouter(){

}
/*========================================================================*/
function changeToPay(id){
  let url = 'Budgets_controller.php'
  let data = new FormData()
  data.append('id', id)
  data.append('changeToPay', 1)
  let msg = "¿Confirmas que deseas aprobar el presupuesto?"
  deleteItem(url, data, msg).then(response => {
    //console.log("Success!", response);
    reload()
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function generatePDF(pacienteEmail){
  let url = 'Pays_controller.php'
  let data = new FormData()
  data.append('generatePDF', 1)
  data.append('pacienteEmail', pacienteEmail)
  postX(url, data).then(response => {
    //console.log("Success!", response);
    sendPDF(response.theme)
  }, error => {
    //console.error("Failed!", error);
  })
}
/*========================================================================*/
function pays(pacienteEmail) {
  let url = 'Pays_controller.php'
  let data = new FormData()
  data.append('all', 1)
  data.append('pacienteEmail', pacienteEmail)
  return all(url, data).then(response => {
    console.log("Success!", response);
    Rpays(response.all, pacienteEmail)
  }, error => {
    console.error("Failed!", error);
  })
 }
/*========================================================================*/
function payFilter(pacienteEmail) {
  const form = document.getElementById('form-pay-filter')
  let data = new FormData(form)
  let url = 'Pays_controller.php'
  data.append('filter', 1)
  data.append('pacienteEmail', pacienteEmail)
  return all(url, data).then(response => {
    Rpays(response.all, pacienteEmail)
  }, error => {})
}
/*========================================================================*/
function Rpays(resp, pacienteEmail) {
  $("#pays-table").dataTable().fnDestroy()
  $("#pays-data").empty();
  pacienteEmail = "'"+pacienteEmail+"'"
  if(resp != null){
    resp.map(row => {
      let template ='<tr>'+
                      '<td class="text-left">'+row.dateSave+'</td>'+
                      '<td class="text-left">'+row.name+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.total)+'</td>'+
                      '<td class="text-center">'+row.discount+'%</td>'+
                      '<td class="text-center">'+row.sessions+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.initial)+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.valorCuota)+'</td>'+
                      '<td class="text-center">'+row.profesional+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.totalAbonos)+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.saldo)+'</td>'+
                      '<td class="text-center">'+row.saldomora+'</td>'+
                      '<td class="text-center">'+
                      '<div class="btn-group space100">'+
                        '<a href="javascript:addPay('+row.id+', 1, '+pacienteEmail+', '+row.payInitial+')" class="space" title="Realizar pago"><i class="fa fa-plus"></i></a>'+
                        '<a href="javascript:PayGo('+row.id+', 1)" class="space" title="Historial de pagos"><i class="fa fa-history"></i></a>'+
                        '<a href="javascript:BudgetGo('+row.id+', 1)" class="space" title="ver presupuesto"><i class="fa fa-eye"></i></a>'+
                      '</div>'+
                      '</td>'+
                    '</tr>';
        $("#pays-data").append(template);
    })
  }
  dataTable("#pays-table",1)
}

/*========================================================================*/
function EvolutionsPays(pacienteEmail) {
  let url = 'Pays_controller.php'
  let data = new FormData()
  data.append('evolutions', 1)
  data.append('pacienteEmail', pacienteEmail)
  return all(url, data).then(response => {
    console.log("Success!", response);
    REvolutionsPays(response.all, pacienteEmail)
  }, error => {
    console.error("Failed!", error);
  })
 }
 /*========================================================================*/
 function EvolutionFilter2(pacienteEmail) {
   const form = document.getElementById('form-Evolution-filter')
   let data = new FormData(form)
   let url = 'Pays_controller.php'
   data.append('filterEvolutions', 1)
   data.append('pacienteEmail', pacienteEmail)
   return all(url, data).then(response => {
     REvolutionsPays(response.all, pacienteEmail)
   }, error => {})
 }

/*========================================================================*/
function REvolutionsPays(resp, pacienteEmail) {
  $("#Evolutions-table").dataTable().fnDestroy()
  $("#Evolutions-data").empty();
  pacienteEmail = "'"+pacienteEmail+"'"
  if(resp != null){
    resp.map(row => {
      let template ='<tr>'+
                      '<td class="text-left">'+row.date+'</td>'+
                      '<td class="text-left">'+row.idTypeEvolution+'</td>'+
                      '<td class="text-center">'+row.dateSterilization+'</td>'+
                      '<td class="text-center">'+row.idEvolutionTypeProcess+'</td>'+
                      '<td class="text-center">'+row.idEvolutionDiagnostic+'</td>'+
                      '<td class="text-center">'+row.idOriginDisease+'</td>'+
                      '<td class="text-center">'+row.idProfesional+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.price)+'</td>'+
                      '<td class="text-center">'+row.discount+'%</td>'+
                      '<td class="text-center">'+formatNumber.new(row.Abonos)+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.saldo)+'</td>'+
                      '<td>'+
                        '<div class="btn-group space100">'+
                          '<a href="javascript:addPay('+row.id+', -1, '+pacienteEmail+', 0)" class="space" title="Realizar pago"><i class="fa fa-plus"></i></a>'+
                          '<a href="javascript:PayGo('+row.id+', -1)" class="space" title="Historial de pagos"><i class="fa fa-history"></i></a>'+
                          '<a href="javascript:EvolutionGo('+row.id+', 0)" class="space"><i class="fa fa-eye"></i></a>'+
                        '</div>'+
                      '</td>'+
                    '</tr>';
        $("#Evolutions-data").append(template);
    })
  }
  dataTable("#Evolutions-table",1)
}

var initial20
function validateInitial(){
  var c1 = document.getElementById('payInitial').checked;
   if (c1==true) {
     document.getElementById('rode-mld').value = $("#rode-mld").val();
   }
}

/*========================================================================*/
function addPay(id, type, pacienteEmail, initial){
  setIdPay(id)
  cleanForm("form-pay")
  $("#mdl_pays").modal('show')
  pacienteEmail = "'"+pacienteEmail+"'"
  cards();
  if(type == 1){
    if(initial>0){
      initial20 = initial
      $("#form-pay").attr('action','javascript:PayUpdate('+type+', '+pacienteEmail+')')
      $(".hideInitial").css('display','none')
    }
    else{
      $("#form-pay").attr('action','javascript:PayUpdate('+type+', '+pacienteEmail+')')

      $(".hideInitial").css('display','block')
    }
  }
  else{
    $("#form-pay").attr('action','javascript:PayUpdate('+type+', '+pacienteEmail+')')
    $(".hideInitial").css('display','none')
  }
}

 /*========================================================================*/
function PayUpdate(type, pacienteEmail) {
  $("#mdl_pays").modal('hide')
  let url = 'Pays_controller.php'
  const form = document.getElementById('form-pay')
  let data = new FormData(form)
  data.append('update', 1)
  data.append('type', type)
  data.append('pacienteEmail', pacienteEmail)
  data.append('idItem', getIdPay())
  postX(url, data).then(response => {
    console.log("Success!", response);
    sendPDF(response);
    setTimeout(function(){ reload() }, 3000);
  }, error => {
    console.error("Failed!", error);
  })
}
/*========================================================================*/
function PayGo(id, type){
  setIdPay(id)
  $("#mdl_pays_history").modal('show')
  Pay(type)
}
 /*========================================================================*/
function Pay(type) {
  let url = 'Pays_controller.php'
  let data = new FormData()
  data.append('item', 1)
  data.append('idItem', getIdPay())
  data.append('type', type)
  all(url, data).then(response => {
    console.log("Success!", response);
    Rpay(response, type)
  }, error => {
    console.error("Failed!", error);
  })
}
/*========================================================================*/
function Rpay(resp, type) {
  $("#pays-history-table").dataTable().fnDestroy()
  document.querySelector("#pays-history-data").innerHTML= ""
  if(resp != null){
    resp.map(row => {
      let template ='<tr>'+
                      '<td class="text-left">'+row.date+'</td>'+
                      '<td class="text-left">'+row.payInitial+'</td>'+
                      '<td class="text-left">'+row.methodName+'</td>'+
                      '<td class="text-center">'+formatNumber.new(row.rode)+'</td>'+
                      '<td class="text-center">'+row.codeBank+'</td>'+
                      '<td class="text-center">'+row.accountBank+'</td>'+
                      '<td class="text-center">'+row.dateBank+'</td>'+
                      '<td class="text-center">'+row.cheque+'</td>'+
                      '<td class="text-center">'+row.idCard+'</td>'+
                      '<td class="text-center">'+row.formPay+'</td>'+
                      '<td class="text-center">'+row.document+'</td>'+
                      '<td class="text-center">'+row.code+'</td>'+
                      '<td class="text-center">'+row.observations+'</td>'+
                    '</tr>';
        $("#pays-history-data").append(template);
    })
  }
  dataTable("#pays-history-table",1)
}
/*========================================================================*/
function validateMethodPay(idMethodPay){
  $("#field1").css('display', 'none');
  $("#field2").css('display', 'none');
  $("#field3").css('display', 'none');
  $("#field4").css('display', 'none');
  $("#field5").css('display', 'none');
  $("#field6").css('display', 'none');
  $("#field7").css('display', 'none');
  $("#field8").css('display', 'none');
  $("#field9").css('display', 'none');
  if(idMethodPay == 2){
    $("#field4").css('display', 'block');
    $("#field7").css('display', 'block');
    $("#field9").css('display', 'block');
  }
  else if(idMethodPay == 3){
    $("#field4").css('display', 'block');
  }
  else if(idMethodPay == 4){
    $("#field2").css('display', 'block');
    $("#field4").css('display', 'block');
    $("#field6").css('display', 'block');
  }
  else if(idMethodPay == 5){
    $("#field1").css('display', 'block');
    $("#field2").css('display', 'block');
    $("#field3").css('display', 'block');
  }
  else if(idMethodPay == 6){
    $("#field5").css('display', 'block');
    $("#field8").css('display', 'block');
    $("#field7").css('display', 'block');

  }
}
/*========================================================================*/
function methodsPaySelect(){
 let url = 'Pays_controller.php'
 let data = new FormData()
 data.append('methodsPays', 1)
 let div = '.methodsPays'
 return selectOptions(url, data, div)
}
/*========================================================================*/
function cards(){
 let url = 'Pays_controller.php'
 let data = new FormData()
 data.append('cards', 1)
 let div = '.cards'
 return selectOptions(url, data, div)
}
