/**
 * bitacoras 1.0.0
 * software
 * Copyright 2017, Unisoft
 * https://www.unisoftsystem.com.co/
 * Auhor: Julio cesar cortes
 * Email: Juliocesar.cortes@unisoftsystem.com.co
 * Licensed Apache License
 * Released on: Octuber 25, 2017
 * Updated on: Octuber 25, 2017
 */

function setIdDentist(id) {localStorage.setItem("idDentis", id)}
function getIdDentist() {return localStorage.getItem("idDentis")}

/*========================================================================*/
function dentistsRouter(){
  dentistsSelect()
}
/*========================================================================*/
function dentistsSelect(){
 let url = 'Dentists_controller.php'
 let data = new FormData()
 data.append('all', 1)
 let div = '.dentists'
 return selectOptions(url, data, div)
}
