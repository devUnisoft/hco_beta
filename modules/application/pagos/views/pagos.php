<?php include '../../layouts/validate.php';?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Administración de presupuestos</title>
  <?php include '../../layouts/head.php';?>
  <?php include '../../layouts/css.php';?>
</head>
<body>
  <?php include '../../layouts/header.php';?>
  <?php include '../layouts/menuheader.php';?>
  <div class="container-fluid" style="margin-top: 40px;">
    <div class="row tt" style="margin-top: 62px;">
      <h2 align="center" class="color-default">Administración de pagos</h2>
      <h2 align="center" class="color-default">Saldo: <span id="totalAbonos"></span></h2>
      <div class="col-md-2">
        <?php include '../../layouts/paciente.php';?>
      </div>
      <div class="col-md-9" style="margin-bottom: 130px;">
        <ul class="nav nav-tabs" role="tablist" style="margin-top: -40px;">
          <li role="presentation" class="active"><a href="#budgets" onclick="pays('<?php echo $_GET['pacienteEmail']; ?>')" aria-controls="home" role="tab" data-toggle="tab">Tratamientos</a></li>
          <li role="presentation"><a href="#evolutions" onclick="EvolutionsPays('<?php echo $_GET['pacienteEmail']; ?>')" aria-controls="evolutions" role="tab" data-toggle="tab">Consulta</a></li>
        </ul>
        <div class="tab-content" style="margin-top: 75px;">
          <div role="tabpanel" class="tab-pane active" id="budgets">
            <form id="form-pay-filter" action="javascript:payFilter('<?php echo $_GET['pacienteEmail']; ?>')" method="post">
              <div class="col-md-12 content_notification">
                <div class="col-md-4">
                  <div class="col-md-3">
                    <label class="color-default">Fecha inicio</label>
                  </div>
                  <div class="col-md-8">
                    <input type="date" name="dateInit" class="form-control" required />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="col-md-3">
                    <label class="color-default">Fecha final</label>
                  </div>
                  <div class="col-md-8">
                    <input type="date" name="dateFinish" class="form-control" required />
                  </div>
                </div>
                <div class="col-md-2" align="center">
                  <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
                <div class="col-md-2" align="center">
                  <a href="javascript:pays('<?php echo $_GET['pacienteEmail']; ?>')" class="btn btn-primary">Mostrar todos</a>
                </div>
              </div>
            </form>
            <table id="pays-table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th align="center">Fecha</th>
                  <th align="center">Tratamiento</th>
                  <th align="center">Valor total</th>
                  <th align="center">Descuento</th>
                  <th align="center">Cuotas</th>
                  <th align="center">Cuota Inicial</th>
                  <th align="center">Valor cuota fija</th>
                  <th align="center">Profesional</th>
                  <th align="center">Total abonos</th>
                  <th align="center">Saldo</th>
                  <th align="center">Saldo en mora</th>
                  <th align="center">Acción</th>
                </tr>
              </thead>
              <tbody id="pays-data"></tbody>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane" id="evolutions">
            <form id="form-Evolution-filter" action="javascript:EvolutionFilter2('<?php echo $_GET['pacienteEmail']; ?>')" method="post">
              <div class="col-md-12 content_notification">
                <div class="col-md-4">
                  <div class="col-md-3">
                    <label class="color-default">Fecha inicio</label>
                  </div>
                  <div class="col-md-8">
                    <input type="date" name="dateInit" class="form-control" required />
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="col-md-3">
                    <label class="color-default">Fecha final</label>
                  </div>
                  <div class="col-md-8">
                    <input type="date" name="dateFinish" class="form-control" required />
                  </div>
                </div>
                <div class="col-md-2" align="center">
                  <button type="submit" class="btn btn-primary">Filtrar</button>
                </div>
                <div class="col-md-2" align="center">
                  <a href="javascript:EvolutionsPays('<?php echo $_GET['pacienteEmail']; ?>')" class="btn btn-primary">Mostrar todos</a>
                </div>
              </div>
            </form>
            <div class="">
              <table id="Evolutions-table" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th align="center">Fecha</th>
                    <th align="center">Tipo de consulta</th>
                    <th align="center">Fecha de Esterilización</th>
                    <th align="center">Tipo de procedimiento</th>
                    <th align="center">Diagnóstico principal</th>
                    <th align="center">Origen de la enfermedad actual</th>
                    <th align="center">Profesional</th>
                    <th align="center">Valor total</th>
                    <th align="center">Descuento</th>
                    <th align="center">Abonos</th>
                    <th align="center">Saldo</th>
                    <th align="center">Acción</th>
                  </tr>
                </thead>
                <tbody id="Evolutions-data"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-1">
        <div class="pull-right">
          <a href="javascript:generatePDF('<?php echo $_GET['pacienteEmail']; ?>');" class="btn btn-primary">Estado de Cuenta</a>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl_pays_history" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 95%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">HISTORIAL DE PAGOS</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <table id="pays-history-table" class="table table-bordered table-hover" style="width: 100%;">
            <thead>
              <tr>
                <th align="center">Fecha</th>
                <th align="center">Cuota inicial</th>
                <th align="center">Metodo de pago</th>
                <th align="center">Valor</th>
                <th align="center">Número de la tarjeta</th>
                <th align="center">Número de la cuenta</th>
                <th align="center">Fecha de consignación</th>
                <th align="center">Número de Cheque</th>
                <th align="center">Tipo</th>
                <th align="center">Forma de pago</th>
                <th align="center">Documento</th>
                <th align="center">Código de Autorización</th>
                <th align="center">Observaciones</th>
              </tr>
            </thead>
            <tbody id="pays-history-data"></tbody>
          </table>
          <button type="button" class="btn btn-default" style="width: 200px;margin: auto;display: block;" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->
  <div id="mdl_pays" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">FORMULARIO DE REGISTRO DE PAGOS</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <form action="javascript:PayUpdate('<?php echo $_GET['pacienteEmail']; ?>');" id="form-pay">
            <div class="row">
              <div class="col-xs-3"></div>
              <div class="col-xs-6">
                <div class="col-md-12 row">
                  <div class="col-md-3">
                    <label class="m10 color-default">Metodo de pago:</label>
                  </div>
                  <div class="col-md-9">
                    <select name="idMethodPay" onchange="validateMethodPay(this.value)" id="idMethodPay" class="form-control select2" required>
                      <option value="">Seleccione una opción</option>
                      <option value="1">Efectivo</option>
                      <option value="2">Transferencia</option>
                      <option value="3">Tarjeta débito</option>
                      <option value="4">Tarjeta crédito</option>
                      <option value="6">Cheque</option>
                      <option value="5">Otro</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 row" id="field1" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Documento:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="document-mld" name="document"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field2" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Código de Autorización:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="code-mld" name="code"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field3" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Forma de pago:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="formPay-mld" name="formPay"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field4" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Tipo:</label>
                  </div>
                  <div class="col-md-9">
                    <select class="form-control cards" id="idCard-mld" name="idCard"></select>
                  </div>
                </div>
                <div class="col-md-12 row" id="field5" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Número de Cheque:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="cheque-mld" name="cheque"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field6" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Número de la tarjeta:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="codeBank-mld" name="codeBank"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field9" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Código de banco:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="accountBank-mld" name="accountBank"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field7" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Número de cuenta:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="accountBank-mld" name="accountBank"/>
                  </div>
                </div>
                <div class="col-md-12 row" id="field8" style="display:none;">
                  <div class="col-md-3">
                    <label class="m10 color-default">Fecha de consignación:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="date" class="form-control" id="dateBank-mld" name="dateBank" />
                  </div>
                </div>
                <div class="col-md-12 row">
                  <div class="col-md-3">
                    <label class="m10 color-default">Valor:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" id="rode-mld" name="rode" onkeyup="numberFormat(this)" required/>
                  </div>
                </div>
                <div class="col-md-12 row">
                  <div class="col-md-3">
                    <label class="m10 color-default">Observaciones:</label>
                  </div>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="observations"/>
                  </div>
                </div>
                <div class="col-md-12 row hideInitial" style="display:none;">
                  <div class="col-md-4">
                    <label class="m10 color-default">Es cuota inicial:</label>
                  </div>
                  <div class="col-md-8">
                    <input type="checkbox" id="payInitial" name="payInitial" value="1" style="width: 30px;height: 30px;" onchange="validateInitial()">
                  </div>
                </div>

                <div class="col-md-12" style="margin-top: 70px;">
                  <div class="center">
                    <button type="submit" class="btn btn-primary" style="width: 200px;">Guardar</button>
                    <button type="button" class="btn btn-default" style="width: 200px;" data-dismiss="modal">Cerrar</button>
                  </div>
                </div>
              </div>
              <div class="col-xs-3"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php include '../../modals/budgets.php';?>
  <?php include '../../modals/evolutions.php';?>
  <?php include '../layouts/footer.php';?>
  <?php include '../../layouts/libs.php';?>
  <?php include '../../layouts/js.php';?>
  <?php include '../layouts/js.php';?>
  <script type="text/javascript" src="../../evoluciones/js/evolution.js"></script>
  <script type="text/javascript" src="../../evoluciones/js/evolution.validate.js"></script>
  <script type="text/javascript">
    $(window).on("load", function(e) {
      pays("<?php echo $_GET['pacienteEmail']; ?>")
      EvolutionsPays("<?php echo $_GET['pacienteEmail']; ?>")
      budgetsSelect("<?php echo $_GET['pacienteEmail']; ?>")
      total("<?php echo $_GET['pacienteEmail']; ?>")
      setReceptor("<?php echo $_SESSION['idUser']; ?>")
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      setNameClinica('<?php echo $clinica; ?>');
    });
  </script>
</body>
</html>
