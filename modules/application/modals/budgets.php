<!-- MODAL CREAR PACIENTE -->
<div id="mdl_budget" class="modal fade" data-backdrop="static" role="dialog" style="overflow: scroll;position: absolute;">
  <div class="modal-dialog" style="width: 80%;">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body tt">
        <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">FORMULARIO DE REGISTRO PARA PRESUPUESTOS ODONTOLÓGICOS</h2>
        <header align="center">
          <div class="col-md-12" align="center">
            <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
          </div>
        </header>
        <p style="color: #666;font-size: 1.5em;margin-left: 25px;">Resultados de búsqueda</p>
        <table id="budgets-items-table" class="table table-bordered table-striped table-responsive" style="margin-left: 17px;">
          <thead>
            <tr>
              <th class="color-blue">Especialidad</th>
              <th class="color-blue">Procedimiento</th>
              <th class="color-blue">Valor</th>
              <th class="color-blue">Acción</th>
            </tr>
          </thead>
          <form action="javascript:BudgetAddItem(0);" id="form-addItem">
            <tr>
              <td >
                <div class="input-group" >
                  <select name="idSpecialty" id="idSpecialty" class="form-control select2 specialties m10" onchange="process(this.value);" style="width:100%;display:block;" required></select>
                </div>
              </td>
              <td >
                <div class="input-group">
                  <select name="idProcess" id="idProcess" class="form-control process select2 m10" onchange="loadProcess(this.value);"  style="width:100%;display:block;" required></select>
                </div>
              </td>
              <td>
                <div class="input-group">
                  <input type="text" id="validatePrice" class="form-control set-process-price" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onkeyup="numberFormat(this)" name="price" required/>
                </div>
              </td>
              <td>
                <div class="center block">
                  <button type="submit" class="brn btn-primary" style="border: 0px;"><i class="fa fa-check" style="font-size: 2em;" aria-hidden="true"></i></button>
                </div>
              </td>
            </tr>
          </form>
        </table>
        <p style="color: #666;font-size: 1.5em;margin-left: 25px;margin-right: 15px;margin-top: 65px;border-top: solid #ccc 2px;padding-top: 15px;">Procedimientos Seleccionados</p>
        <table id="budgets-items-table" class="table table-bordered table-striped table-responsive" style="margin-left: 17px;">
          <thead>
              <tr>
                <th class="color-blue" >Especialidad</th>
                <th class="color-blue" >Procedimiento</th>
                <th class="color-blue">Valor</th>
                <th class="color-blue">Acción</th>
              </tr>
          </thead>
          <tbody id="budgets-items-data">

          </tbody>
        </table>
        <form id="form-budget" action="javascript:updateBudget2()" method="post" style="border-top: solid #ccc 2px;padding-top: 15px;margin: 20px;">
          <input type="hidden" id="pacienteEmail1" name="pacienteEmail" value="<?php
            if(isset($_GET['pacienteEmail'])){if($_GET['pacienteEmail'] == null){ echo 1; } else{ echo $_GET['pacienteEmail']; } } else{ echo 1; } ?>"/>
          <input type="hidden" name="items" id="items">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-md-4 control-label label-form color-default">Nombre del presupuesto:</label>
                  <div class="input-group col-md-8">
                      <input type="text" class="form-control bg-white no-border" name="name" required/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <?php if($perfilinto == "odontologo"){ ?>
                  <div class="form-group">
                    <label class="col-md-3 control-label label-form color-default">Profesional:</label>
                    <div class="input-group col-md-9">
                      <select name="idProfesional" id="idDentist" class="form-control m10"  style="margin-top: 0;width: 98%;" required>
                        <option value="<?php echo $idUser; ?>" selected><?php echo $userName; ?></option>
                      </select>
                    </div>
                  </div>
                <?php } else { ?>
                    <div class="form-group">
                      <label class="col-md-4 control-label label-form color-default">Profesional:</label>
                      <div class="input-group col-md-8">
                        <select name="idProfesional" id="idDentist" class="form-control select2 dentists m10" style="margin-top: 0;" required></select>
                      </div>
                    </div>
                  <?php } ?>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="col-md-4 control-label label-form color-default">Cuota inicial:</label>
                  <div class="input-group col-md-8">
                      <input type="text" onkeyup="numberFormat(this)" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control bg-white no-border" style="text-align: right;" id="initial" name="initial"/>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="col-md-8 control-label label-form color-default">N° de sessiones:</label>
                  <div class="input-group col-md-4">
                      <input type="number" min="1" class="form-control bg-white no-border" style="text-align: right;" id="sessions" name="sessions" required/>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label class="col-md-6 control-label label-form color-default">Descuento (%):</label>
                  <div class="input-group col-md-6">
                      <input type="number" id="inputDiscount" onchange="onkeyup(this.value)" onchange="discountBudget(this.value)" class="form-control bg-white no-border" style="text-align: right;" name="discount"/>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-md-5 control-label label-form color-default">Total:</label>
                      <div class="input-group col-md-7">
                          <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control bg-white no-border" style="text-align: right;" id="totalBudget" name="total"  required readonly/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 row" style="padding-left: 30px;padding-right: 30px;">
                <div class="col-md-3">
                  <label class="m10 color-default">Observaciones:</label>
                </div>
                <div class="col-md-9">
                  <textarea name="descripcion" rows="4" cols="40" class="form-control m10"></textarea>
                </div>
              </div>
              <div class="col-xs-12">
                <div class="center block text-center" id="hide-btn">
                  <center>
                    <button type="submit" class="btn btn-lg btn-primary color-blue" style="margin-top: 30px;margin-bottom: 15px;" id="changeBTNS">Generar presupuesto</button>
                  </center>

                </div>
              </div>
              <div class="col-xs-12">
                <div class="center block text-center">
                  <?php
                  if(isset($_GET['pacienteEmail'])){
                    if($_GET['pacienteEmail'] == null){ ?>
                      <a href="../../../../menu.php" class="btn btn-default" style="margin-bottom: 15px;">volver</a>
                    <?php }
                    else{ ?>
                       <a href="#" class="btn btn-default" style="margin-bottom: 15px;" data-dismiss="modal">Cerrar</a>
                     <?php }
                   }else{ ?>
                     <a href="../../../../menu.php" class="btn btn-default" style="margin-bottom: 15px;">volver</a>
                  <?php } ?>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL CREAR PRODUCTO -->
