
  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl_patients" class="modal fade" data-backdrop="static" role="dialog" style="overflow: scroll;position: absolute;">
    <div class="modal-dialog" style="width: 80%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">SELECCIONE UN PACIENTE</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <form id="form-patient" action="javascript:validatePatient(0)" method="post" style="border-top: solid #ccc 2px;padding-top: 15px;margin: 20px;">
              <div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Paciente:</label>
                    <div class="input-group col-md-8">
                      <select name="idPatient" id="idPatient" class="form-control select2 patients m10" onchange="validatePatient(this.value)"></select>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-primary color-blue" style="margin-bottom: 15px;">Nuevo paciente</button>
                </div>
                <div class="col-xs-12">
                  <div class="center block text-center">
                    <?php
                    if(isset($_GET['pacienteEmail'])){
                      if($_GET['pacienteEmail'] == null){ ?>
                        <a href="javascript:closeModalPatient()" class="btn btn-default" style="margin-bottom: 15px;">Cerrar</a>
                      <?php }
                      else{ ?>
                         <a href="javascript:closeModalPatient()" class="btn btn-default" style="margin-bottom: 15px;">Cerrar</a>
                       <?php }
                     }else{ ?>
                        <a href="javascript:closeModalPatient()" class="btn btn-default" style="margin-bottom: 15px;">Cerrar</a>
                    <?php } ?>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->


  <!-- MODAL CREAR PACIENTE -->
  <div id="mdl_patient" class="modal fade" data-backdrop="static" role="dialog" style="overflow: scroll;position: absolute;">
    <div class="modal-dialog" style="width: 80%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body tt">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">FORMULARIO DE REGISTRO DEL PACIENTE</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <form id="form-patient" action="javascript:addPatient()" method="post" style="border-top: solid #ccc 2px;padding-top: 15px;margin: 20px;">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Tipo de documento:</label>
                    <div class="input-group col-md-8">
                        <select class="form-control bg-white no-border" name="idTypeDocument" required>
                          <option value="">Selecione una opcion</option>
                          <option value="Cedula de ciudadania">Cedula de ciudadania</option>
                          <option value="Registro Civil">Registro civil</option>
                          <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                          <option value="Cedula extranjera">Cedula extranjera</option>
                          <option value="Nit">Nit</option>
                          <option value="Adulto de identificacion">Adulto de identificacion</option>
                          <option value="Menor de identificacion">Menor de identificacion</option>
                          <option value="Pasaporte">Pasaporte</option>
                        </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Identificación:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="code" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Nombre:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="name" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Apellido:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="lastname" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Email:</label>
                    <div class="input-group col-md-8">
                        <input type="email" id="emailPatient" class="form-control" name="email" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Dirección:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="address" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Télefono:</label>
                    <div class="input-group col-md-8">
                      <input type="tel" class="form-control" name="phone" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Genero:</label>
                    <div class="input-group col-md-8">
                        <select class="form-control bg-white no-border" name="gender" required>
                          <option value="">Selecione una opcion</option>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Fecha de nacimiento:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="dateBirth" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Lugar de nacimiento:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="placeBirth" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Lugar de residencia:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="residencePlace" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Tipo de residencia:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="residenceType" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Tipo de usuario:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="userType" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Tipo de sangre:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="bloodType" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">EPS:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="eps" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Estado civil:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="civilStatus" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Ocupación:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="occupation" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Acudiente Nombre:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="attendantName" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Acudiente Parentesco:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="attendantParent" required/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-md-4 control-label label-form color-default">Acudiente teléfono:</label>
                    <div class="input-group col-md-8">
                        <input type="text" class="form-control" name="attendantPhone" required/>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="center block text-center">
                    <button type="submit" class="btn btn-lg btn-primary color-blue" style="margin-top: 30px;margin-bottom: 15px;">Guardar</button>
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="center block text-center">
                    <?php
                    if(isset($_GET['pacienteEmail'])){
                      if($_GET['pacienteEmail'] == null){ ?>
                        <a href="javascript:closeModalPatient()" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 15px;">Cerrar</a>
                      <?php }
                      else{ ?>
                         <a href="javascript:closeModalPatient()" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 15px;">Cerrar</a>
                       <?php }
                     }else{ ?>
                        <a href="javascript:closeModalPatient()" class="btn btn-default" data-dismiss="modal" style="margin-bottom: 15px;">Cerrar</a>
                    <?php } ?>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL CREAR PRODUCTO -->
