
  <div id="mdl_Evolution" class="modal fade" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <h2 align="center" style="margin-top: 90px;margin-bottom:40px;" class="color-default">Evolución</h2>
          <header align="center">
            <div class="col-md-12" align="center">
              <img src="../../../../images/LOGO SW CENTRAL.png" width="80px" style="margin-top: 25px;">
            </div>
          </header>
          <form method="post" action="../../../../consentimiento.php" target="_blank" style="text-align: center;margin: auto;margin-bottom: 16px;">
            <input type="hidden" class="form-control" id="usuariop" name="usuariop" value="<?php echo $userName?>" />
            <input type="hidden" class="form-control" id="usuarioconsultado" name="usuarioconsultado" value="<?php echo $nombrePaciente ?>" />
            <input type="hidden" class="form-control" id="documento" name="documento" value="<?php echo $documentnumber ?>" />
            <input type="hidden" class="form-control" id="txtedad" name="txtedad" value="<?php echo $edad ?>" />
            <input type="hidden" class="form-control" id="txtacudiente" name="txtacudiente" value="<?php echo $acudientenombre ?>" />
            <button type="submit" class="btn btn-primary">Generar Consentimiento</button>
          </form>
          <form id="form-Evolution" action="javascript:updateEvolution()" method="post">
            <input type="hidden" id="pacienteEmail1" name="pacienteEmail" value="<?php
              if(isset($_GET['pacienteEmail'])){if($_GET['pacienteEmail'] == null){ echo 1; } else{ echo $_GET['pacienteEmail']; } } else{ echo 1; } ?>"/>
            <div class="row" style="display: block;margin: auto;">

              <div class="col-md-1"></div>
              <div class="col-md-5">
                <label class="radio-inline" style="display: block;text-align: center;margin-bottom: 20px;"><input type="radio" name="idTypeEvolution" value="1" onchange="validateEvolution(this.value);" required>Tratamiento en desarrollo</label>
              </div>
              <div class="col-md-4">
                <label class="radio-inline" style="display: block;text-align: center;margin-bottom: 20px;"><input type="radio" name="idTypeEvolution" value="2" onchange="validateEvolution(this.value);validateCheckPrice2(this.value)">Consulta Día</label>
              </div>
              <div class="col-md-2"></div>
              <div class="col-md-12">
                <div id="selectBudget" class="row" style="display:none;">
                  <div class="col-md-9">
                    <label class="m10 color-default">Seleccione el tratamiento:</label>
                    <select name="idBudget" id="idBudget" class="form-control select2 budgets m10"></select>
                  </div>
                  <div class="col-md-3">
                    <label class="radio-inline" style="margin-top:35px;"><input type="checkbox" name="aditionalCost" onchange="validateCheckPrice()" id="aditionalCost" value="0">Cobro Adicional</label>
                  </div>
                </div>
                <label class="m10 color-default">Tipo:</label>
                <select name="type" class="form-control m10" required>
                  <option value="">Seleccione una opción</option>
                  <option value="1">Nuevo</option>
                  <option value="2">Repetido</option>
                  <option value="3">Impresión</option>
                </select>
                <div id="selectConsulting">
                  <label class="m10 color-default">Tipo de consulta:</label>
                  <select name="idTypeConsult" id="idTypeConsult" class="form-control select2 typeConsult m10"></select>
                </div>
                <label class="m10 color-default">Fecha de Esterilización:</label>
                <input type="date" name="dateSterilization" class="form-control m10" style="margin-left: 0px;" required>
                <label class="m10 color-default">Tipo de procedimiento (Cups):</label>
                <select name="process[]" id="addpriceEvolution1" onchange="addpriceEvolution()" class="form-control process select2 m10" multiple required></select>
                <label class="m10 color-default">Diagnóstico principal:</label>
                <select name="diagnosis[]" class="form-control diagnostics select2 m10" multiple required></select>
                <label class="m10 color-default">Origen de la enfermedad actual:</label>
                <select name="idOriginDisease" class="form-control originDisease select2 m10" required></select>
                <?php if($perfilinto == "odontologo"){ ?>
                  <label class="m10 color-default">Profesional:</label>
                  <select name="idProfesional" id="idDentist" class="form-control m10" style="margin-left: 0px;" required>
                    <option value="<?php echo $idUser; ?>" selected><?php echo $userName; ?></option>
                  </select>
                <?php } else { ?>
                    <label class="m10 color-default">Profesional:</label>
                    <select name="idProfesional" id="idDentist" class="form-control select2 dentists m10" style="margin-left: 0px;" required></select>
                  <?php } ?>
                  <label class="m10 color-default">Evolución:</label>
                  <textarea name="observations" rows="8" cols="80" class="form-control m10" style="margin-left: 0px;" required></textarea>
                  <div id="hidePay" style="display:none;">
                    <div class="col-md-6">
                      <label class="m10 color-default">Descuento(%):</label>
                      <input type="number" id="discountEvolution" name="discount" onchange="EvolutionApplyDiscount()" class="form-control m10">
                    </div>
                    <div class="col-md-6">
                      <label class="m10 color-default">Total:</label>
                      <input type="text" id="TotalEvolution" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" name="price" value="0" onkeyup="numberFormat(this)" onchange="EvolutionApplyDiscount()" class="form-control m10">
                    </div>
                  </div>
              </div>
              <div class="col-md-12" id="hide-btn2">
                <button type="submit" class="btn btn-lg btn-primary" style="display: block;text-align: center;margin: 15px auto;">Guardar</button>
              </div>
            </div>
          </form>
          <button type="button" class="btn btn-default" style="width: 200px;margin: auto;display: block;" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">

  </script>
