<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica
    $_SESSION['clinica'] = $_POST["clinica"];

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login_radiologia.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $clave = base64_encode($password);
    //selecionamos el usuario

    $result = mysql_query("SELECT * FROM users_radiologia where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   

    while($row = mysql_fetch_array($result)){
      $pfil = $row["__t"];
    }

    if($pfil =="Profesional")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    

    }
    else if($pfil =="Asistente")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual   
    }
    else if($pfil =="Administrador")
    {
     //header ("Location: menuadmin.php");
    }
    else
    {
      header ("Location: index_radiologia.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>ADMINISTRACION DE PROCEDIMIENTOS</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/ValidacionNumerica.js"></script>
  <style type="text/css">
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
    }
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
td, th {
     padding: 10px; 
}
  </style>
  
</head>
<body>

<header>
  <?php include 'headerRadiologia.php';?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">ADMINISTRACION DE PROCEDIMIENTOS</h3>
  <br>
  <div class="row"> 
        
      <div class="col-md-6">
        <h4 style="color: #0683c9;">CREAR PROCEDIMIENTOS</h4> 
        <hr>
        <form id="frmCrear" method="post" action="">    
          
          <label><font color="#0683c9">Código</font></label><br>
          <input type="text" name="txtCodigo" id="txtCodigo" class="form-control">
          <span style="color: red; font-size: 12px; display: none; font-weight: bolder;" id="spanErrorCodigo" class="errorDatoCrear"></span><br>

          <label for="selectGrupo"><font color="#0683c9">Grupo</font></label><br>
          <select id="selectGrupo" class="form-control">
            <option value="Seleccione">SELECCIONE...</option>
            <?php
              $result = mysql_query("SELECT * FROM `grupo`");   

              while($row = mysql_fetch_array($result)){
                $grupo = $row["nombre"];
                $idgrupo = $row["id"];
            ?>
              <option value="<?= $idgrupo;?>"><?= $grupo;?></option>
            <?php  
              }
            ?>
          </select><br>

          <label for="selectTipo"><font color="#0683c9">Tipo</font></label><br>
          <select id="selectTipo" class="form-control"></select><br>

          <label for="selectPresentacion"><font color="#0683c9">Presentación</font></label><br>
          <select id="selectPresentacion" class="form-control"></select><br>

          <label><font color="#0683c9">Valor</font></label><br>
          <input type="text" name="txtValor" id="txtValor" onKeyUp="format(this)" class="form-control">
          <br>
          <button class="btn btn-primary pull-right" type="submit" id="btnCrear">CREAR</button>    
        </form>    
      </div> 

      <div class="col-md-6"> 
        <h4 style="color: #0683c9;">MODIFICAR PROCEDIMIENTOS</h4> 
        <hr>
        <form id="frmEditar" method="post" action="">    
          <label for="selectGrupoEditar"><font color="#0683c9">Grupo</font></label><br>
          <select id="selectGrupoEditar" class="form-control">
            <option value="Seleccione">SELECCIONE...</option>
            <?php
              $result = mysql_query("SELECT * FROM `grupo`");   

              while($row = mysql_fetch_array($result)){
                $grupo = $row["nombre"];
                $idgrupo = $row["id"];
            ?>
              <option value="<?= $idgrupo;?>"><?= $grupo;?></option>
            <?php  
              }
            ?>
          </select><br>

          <label><font color="#0683c9">Procedimiento</font></label><br>
          <select id="selectProcedimientoEditar" name="selectProcedimientoEditar" class="form-control">
            <option value="Seleccione">SELECCIONE</option>
          </select><br> 
            
          <label><font color="#0683c9">Código</font></label><br>
          <input type="text" name="txtCodigoEditar" id="txtCodigoEditar" class="form-control" disabled="disabled"><br> 

          <label><font color="#0683c9">Valor</font></label><br>
          <input type="text" name="txtValorEditar" id="txtValorEditar" onKeyUp="format(this)" class="form-control"> 
          <br>
          <button class="btn btn-primary btn-warning pull-right" type="submit" id="btnEditar" disabled="disabled">MODIFICAR</button>    
        </form>    
      </div> 
    
  </div>
  <br>
     <?php include 'footerRadiologia.php';?> 
     <br><br>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <!-- call calendar plugin -->
    <script type="text/javascript">
    $().FullCalendarExt({
      calendarSelector: '#calendar',
      quickSaveCategory: '<label>Category: </label><select name="categorie"><?php foreach($calendar->getCategories() as $categorie) { ?> <option value="<?php echo $categorie?>"><?php echo $categorie; ?></option><?php } ?></select>',
    });
  </script>
  <script src="js/jquery.validate.js"></script>
  <script type="text/javascript">
    
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrear").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmCrear").validate({
          rules: {
            txtCodigo:"required",
            txtValor:"required"
          },
          messages: {
            txtCodigo: "Por favor ingrese un código del procedimiento",
            txtValor: "Por favor ingrese un valor del procedimiento"
          },
          submitHandler: function(form) {
            var codigo = $("#txtCodigo").val();
            var valor = $("#txtValor").val();
            var grupo = $("#selectGrupo").val();
            var tipo = $("#selectTipo").val();
            var presentacion = $("#selectPresentacion").val();

            //Se guardan los datos en un JSON
            var datos = {
              codigo: codigo,
              valor: valor,
              grupo: grupo,
              tipo: tipo,
              presentacion: presentacion
            }   
            
            $.post("add_procedimiento_radiologia.php", datos)
            .done(function( data ) {console.log(data)             
               alert("¡Se registro con exito el procedimiento!")
               $("#txtCodigo").val("")
               $("#txtValor").val("")
               listarProcedimientos()
               window.location.href = 'adminProcedimientos.php'
            });
          }
        });

        $("#frmEditar").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmEditar").validate({
          rules: {
            txtNombreEditar:"required",
            txtCodigoEditar:"required",
            txtValorEditar:"required"
          },
          messages: {
            txtNombreEditar: "Por favor ingrese un nombre del procedimiento",
            txtCodigoEditar: "Por favor ingrese un código del procedimiento",
            txtValorEditar: "Por favor ingrese un valor del procedimiento"
          },
          submitHandler: function(form) {
            var nombre = $("#txtNombreEditar").val();
            var procedimiento = $("#selectProcedimientoEditar").val();
            var valor = $("#txtValorEditar").val();

            //Se guardan los datos en un JSON
            var datos = {
              nombre: nombre,
              procedimiento: procedimiento,
              valor: valor
            }   
            
            $.post("update_procedimiento_radiologia.php", datos)
            .done(function( data ) {console.log(data)             
               alert("¡Se actualizó con exito el procedimiento!")
               $("#txtNombreEditar").val("")
               $("#txtValorEditar").val("")
               $("#txtNombreEditarHidden").val("")
               $("#txtCodigoEditar").val("")
               $("#selectProcedimientoEditar").val("Seleccione")
               listarProcedimientos()
            });
          }
        });
        listarProcedimientos()
    })();

    //Validacion del campo codigo para que no se repita
    $("#txtCodigo").keyup(function(e){
      var codigo = $("#txtCodigo").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        codigo: codigo
      }   
      
      $.post("validateCodigoProcedimientoRadio.php", datos)
      .done(function( data ) {//console.log(data)             
         var json = JSON.parse(data)
         if($.trim(json.filas) != "0"){
          $("#spanErrorCodigo").css({"display":"block"})
          $("#spanErrorCodigo").html("¡El codigo de este procedimiento en el centro radiologico ya existe!")
         }else{
          $("#spanErrorCodigo").css({"display":"none"})
          $("#spanErrorCodigo").html("")
         }
         mostrarBotonCrear()
      });
      
    })

    //Validacion del campo nombre para que no se repita en la creacion
    $("#txtNombre").keyup(function(e){
      var nombre = $("#txtNombre").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        nombre: nombre
      }   
      
      $.post("validateNombreProcedimientoRadio.php", datos)
      .done(function( data ) {console.log(data)             
        var json = JSON.parse(data)
        if($.trim(json.filas) != "0"){
          $("#spanErrorNombre").css({"display":"block"})
          $("#spanErrorNombre").html("¡El nombre de este procedimiento en el centro radiologico ya existe!")
        }else{
          $("#spanErrorNombre").css({"display":"none"})
          $("#spanErrorNombre").html("")
        }
        mostrarBotonCrear()
      });
    })

    //Validacion del campo nombre para que no se repita en la modificacion
    $("#txtNombreEditar").keyup(function(e){
      var nombre = $("#txtNombreEditar").val();
      var nombreHidden = $("#txtNombreEditarHidden").val();
      
      if(nombre != nombreHidden){
        //Se guardan los datos en un JSON
        var datos = {
          nombre: nombre
        }         
        $.post("validateNombreProcedimientoRadio.php", datos)
        .done(function( data ) {
          var json = JSON.parse(data)
          if($.trim(json.filas) != "0"){
            $("#spanErrorNombreEditar").css({"display":"block"})
            $("#spanErrorNombreEditar").html("¡El nombre de este procedimiento en el centro radiologico ya existe!")          
          }else{
            $("#spanErrorNombreEditar").css({"display":"none"})
            $("#spanErrorNombreEditar").html("")
          }
          mostrarBotonEditar()
        });
      }else{
        $("#spanErrorNombreEditar").css({"display":"none"})
        $("#spanErrorNombreEditar").html("")
      }
      mostrarBotonEditar()
      
    })
    
    //Mostrar Datos de un procedimiento a modificar    
    $("#selectGrupo").change(function(e){
      if($("#selectGrupo").val() != "Seleccione"){
        var codigo = $("#selectGrupo").val()
        var datos = {
          grupo: codigo
        }   
        
        $.post("findTipoProcedimientosRadiologia.php", datos)
        .done(function( data ) {console.log(data)
          $("#selectTipo").html("")
          $("#selectPresentacion").html("")
          $("#selectTipo").append("<option value='Seleccione'>SELECCIONE...</option>")
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            for (var i = 0; i < json.length; i++) {
              $("#selectTipo").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
            }
          }else{
            $("#selectTipo").html("")
          }
        });
      }else{
        $("#selectTipo").html("")
      }
    })

    //Mostrar Datos de un procedimiento a modificar    
    $("#selectTipo").change(function(e){
      if($("#selectTipo").val() != "Seleccione"){
        var codigo = $("#selectTipo").val()
        var datos = {
          idtipo: codigo
        }   
        
        $.post("findPresentacionRadiologia.php", datos)
        .done(function( data ) {console.log(data)
          $("#selectPresentacion").html("")
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            for (var i = 0; i < json.length; i++) {
              $("#selectPresentacion").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
            }
          }else{
            $("#selectPresentacion").append("<option value=''>Sin Presentación</option>")
          }
        });
      }else{
        $("#selectPresentacion").html("")
      }
    })

    //Mostrar Datos de un procedimiento a modificar    
    $("#selectProcedimientoEditar").change(function(e){
      if($("#selectProcedimientoEditar").val() != "Seleccione"){
        var codigo = $("#selectProcedimientoEditar").val()
        var datos = {
          procedimiento: codigo
        }   
        
        $.post("findProcedimiento_Radiologia.php", datos)
        .done(function( data ) {
          if($.trim(data) != "{}"){
            var json = JSON.parse(data)
            $("#txtNombreEditar").val(json.name)
            $("#txtNombreEditarHidden").val(json.name)
            $("#txtCodigoEditar").val(json.codigo)
            $("#txtValorEditar").val(json.valor)
            $("#btnEditar").removeAttr("disabled")
            format(document.getElementById("txtValorEditar"))
          }else{
            $("#txtNombreEditar").val("")
            $("#txtNombreEditarHidden").val("")
            $("#txtCodigoEditar").val("")
            $("#txtValorEditar").val("")
            $("#btnEditar").attr("disabled", "disabled")
          }
        });
      }else{
        $("#txtNombreEditar").val("")
        $("#txtNombreEditarHidden").val("")
        $("#txtCodigoEditar").val("")
        $("#txtValorEditar").val("")
        $("#btnEditar").attr("disabled", "disabled")
      }
    })

    //funcion que valida si hay errores para habilitar el boton crear
    function mostrarBotonCrear(){
      var contador = 0;
      $( ".errorDato" ).each(function( index ) {
        if($( this ).text() != ""){
          contador++;
        }
      });
      if(contador > 0 || $("#selectPresentacion > option").length == 0){
        $("#btnCrear").attr("disabled", "disabled")
      }else{
        $("#btnCrear").removeAttr("disabled")
      }
    }

    //funcion que valida si hay errores para habilitar el boton crear
    function mostrarBotonEditar(){
      var contador = 0;
      $( ".errorDatoEditar" ).each(function( index ) {
        if($( this ).text() != ""){
          contador++;
        }
      });
      if(contador > 0){
        $("#btnEditar").attr("disabled", "disabled")
      }else{
        $("#btnEditar").removeAttr("disabled")
      }
    }

    //Funcion para listar los procedimientos en el select del modificar
    //Cuando se crea un nuevo procedimiento se actualiza el select
    function listarProcedimientos(){
      var datos = {          
      }   
      
      $.post("listProcedimientos_radio.php", datos)
      .done(function( data ) {console.log(data)
        var aux = $("#selectProcedimientoEditar").val()

        $("#selectProcedimientoEditar").html("")
        $("#selectProcedimientoEditar").append("<option value='Seleccione'>SELECCIONE</option>")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectProcedimientoEditar").append("<option value='" + json[i].id + "'>" + json[i].name + "</option>")
          }
          $("#selectProcedimientoEditar").val(aux)
        }
      });
    }

    
</script>
</body>
</html>