<?php include('includes/loader.php'); 
  //directorio tipo utf-8
  //header('Content-Type: text/html; charset=UTF-8');
  //error_reporting(0);
  ob_start();
  /* Empezamos la sesión */
  session_start();
  /* Si no hay una sesión creada, redireccionar al index. */
  if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
    header('Location: login.php');
  } // Recuerda usar corchetes

  require_once 'db_connect.php';
  // connecting to db
  $db = new DB_CONNECT();

  $idpaciente= $_GET['usuario'];
  //paso session de clinica
  $clinicaconsultada = $_SESSION['clinicaperfiluno']; 

  $result_paciente = mysql_query("SELECT * FROM users where document__number='$idpaciente' AND `clinica`='$clinicaconsultada'");   
  while($row = mysql_fetch_array($result_paciente)){
    $apellidos = $row["last_name"];
    $nombres = $row["first_name"];
    $nombre_completo = $nombres . " " . $apellidos;
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Orden Medica</title>
    <meta charset="utf-8">
      <style>
   html{
     font-family: Arial;
   }

    .container-fluid{
      width:100%;
    }
</style>   
  </head>
  <body>
    
    <div class="container-fluid">
   
    <?php
      $clinica = ($_GET['clinica']);
      $identificacion = ($_GET['paciente']);
      $id = ($_GET['orden']);
      $cont = 0;

      $info = mysql_query("SELECT DISTINCT 
                          `ordenesmedicas`.`id`,
                          `ordenesmedicas`.`documento`,
                          `ordenesmedicas`.`fecha`,
                          `ordenesmedicas`.`hora`,
                          `ordenesmedicas`.`json`,
                          `ordenesmedicas`.`usuario`,
                          `ordenesmedicas`.`clinica`,
                          `detalle_orden_medica`.`id`,
                          `detalle_orden_medica`.`id_orden_medica`,
                          `detalle_orden_medica`.`id_procedimiento`,
                          `detalle_orden_medica`.`descripcion`,
                          `detalle_orden_medica`.`seleccion`,
                          `detalle_orden_medica`.`presentacion`,
                          `detalle_orden_medica`.`diagnostico`,
                          `detalle_orden_medica`.`observacion`,
                          `detalle_orden_medica`.`estado`,
                          `presentacion`.`nombre`,
                          `users`.`first_name`,
                          `users`.`last_name`,
                          `users`.`document__document_type__oid`,
                          `users`.`document__number`,
                          `users`.`lugar_residencia`,
                          `users`.`telefono`,
                          `clinicas`.`imagen`,
                          `clinicas`.`direccion`,
                          `clinicas`.`telefono`,
                          `clinicas`.`email`,
                          `clinicas`.`razonsocial`,
                          `clinicas`.`nit`,
                          `tipo_procedimientos`.`nombre` `tipo`
                        FROM
                          `detalle_orden_medica`
                          INNER JOIN `ordenesmedicas` ON (`detalle_orden_medica`.`id_orden_medica` = `ordenesmedicas`.`id`)
                          INNER JOIN `procedimientos_radiologico` ON (`detalle_orden_medica`.`id_procedimiento` = `procedimientos_radiologico`.`id`)
                          INNER JOIN `presentacion` ON (`procedimientos_radiologico`.`id_presentacion` = `presentacion`.`id`)
                          INNER JOIN `clinicas` ON (`ordenesmedicas`.`clinica` = `clinicas`.`razonsocial`)
                          INNER JOIN `tipo_procedimientos` ON (`procedimientos_radiologico`.`id_tipo` = `tipo_procedimientos`.`id`),
                          `users`
                        WHERE
                          `ordenesmedicas`.`documento` = '{$identificacion}' AND 
                          `ordenesmedicas`.`clinica` = '{$clinica}' AND 
                          `ordenesmedicas`.`id` = '{$id}' AND 
                          `ordenesmedicas`.`documento` = `users`.`document__number`");
    
     while($row = mysql_fetch_array($info)){
      if ($cont == 0){
        $nom_emp = $row['razonsocial'];
        $email_emp = $row['email'];
        $telefono = $row['telefono'];
        $direccion = $row['direccion'];
        $nit = $row['nit'];
        ?>
      <div style="width:800px;">
        <table width='700px'>
          <tr>
            <td style="width:200px">  <img src="<?php echo $row['imagen'] ?>" width="75%" height="75%"></td>
            <td style="width:450px;padding-left;20px;">
              <p style="color: #0683c9;  font-size: 1.3em; font-weight: bold; font-family: Arial;"> ORDEN MÉDICA</p>
                <div class="row">

                  <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;width:350px; font-family: Arial; ">NOMBRE:</span>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <?php echo $row['last_name'].' '.$row['first_name']  ?> </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;">TIPO DE IDENTIFICACIÓN:</span> &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <?php echo $row['document__document_type__oid']?> </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;">NÚMERO DE IDENTIFICACIÓN:</span> &nbsp; <?php echo $row['document__number']?></div>
                  </div>
                
                <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;">DIRECCIÓN:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<?php echo $row['lugar_residencia']?></div>
                  </div>
                
                <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;">TELÉFONO:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <?php echo $row['telefono']?></div>
                  </div>
                                
                <div class="col-md-12">
                    <div class="col-xs-6" style="font-size:0.6em;"><span style="color: #0683c9;">FECHA DE SOLICITUD:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date('Y-m-d')?></div>
                  </div>

            </td>
            </tr>           

        </table>
     </div>
      
      <hr>
      
      <table width='700px' style=" margin: 0px auto;">
      <tr style="background: #0683c9;  color: #fff; text-align: center;">
        <td>Descripción</td>
        <td>Tipo</td>
        <td>Especificaciones</td>
        <td>Observaciones</td>       
      </tr>
    <?php }?>

    <tr style="font-size:0.9em">
        <td><?php echo $row['descripcion'] ?></td>
        <td><?php echo $row['tipo'] ?></td>
        <td><?php echo $row['seleccion'] ?></td>
        <td><?php echo $row['observacion'] ?></td>
        
      </tr>
    <?php $cont++;
    }?>
</table>

<br><br><br>
      <div style="width:700px;margin-top:0px;font-size:0.6em;">
      <p style="display:inline-block;text-align:center;">NOMBRE DEL DOCTOR  _______________________________ &nbsp;&nbsp;&nbsp;&nbsp; FIRMA DEL DOCTOR   ____________________________ </p>
      </div>

      <div style="width:100%;text-align:center;font-size:0.5em;">
      <hr>
        <p><?php echo $nom_emp; ?> - Telefono: <?php echo $telefono ?> -  <?php echo $direccion ?></p>
	  </div>
    
    </div>            
    
  </body>
</html>
<?php 
require_once("dompdf/dompdf_config.inc.php");
  
  $dompdf = new DOMPDF();
  $dompdf->load_html(ob_get_clean());
  $media = array(0,0,595.28,420.94);
  //$dompdf->set_paper ('letter','portrait'); 
  $dompdf->set_paper ($media); 
  
 // $dompdf->set_base_path('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
  $dompdf->render();
 
  $pdf = $dompdf->output();

  $dompdf->stream('pdfordenmedica.pdf',array('Attachment'=>0));

?>