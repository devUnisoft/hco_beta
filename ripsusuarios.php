<?php
error_reporting(0);
//conexion
$pdo = new PDO('mysql:host=localhost;dbname=hco', 'root', 'usc2017*');
//
//recibo fechas
$fechainicial =  $_REQUEST["f1"];
$fechafinal = $_REQUEST["f2"];
//convierto fecha
$fechainicial = date("d-m-Y", strtotime($fechainicial));
//agrego la hora
$fechainicial= $fechainicial." 01:00:00";
//convierto fecha
$fechafinal = date("d-m-Y", strtotime($fechafinal));
//agrego la hora
$fechafinal= $fechafinal." 24:00:00";


//query
$queryus = "SELECT DISTINCT users.birthdate, users.document__document_type__oid, users.last_name, users.first_name, users.document__number, users.tipousuario, users.gender, users.zona, clinicas.codigoentidad, georeferences.codigodepartamento, georeferences.codigomunicipio  FROM users  INNER JOIN clinicas  ON users.clinica=clinicas.razonsocial INNER JOIN georeferences ON users.lugar_residencia=georeferences.name  LEFT  JOIN evolutions  ON users.document__number=evolutions._oid  where  evolutions._date <= '$fechafinal' and _date  >= '$fechainicial' ";
//result
$result = $pdo->query($queryus);
$data = '';
while ($row = $result->fetch(PDO::FETCH_OBJ)) {
        //convierto en edad
        $fecha = time() - strtotime($row->birthdate);
        $edad = floor($fecha / 31556926);

        //abrevio tipo documento
        if($row->document__document_type__oid == "Cedula de Ciudadania"){
            $tipo = "CC";
        }
        if($row->document__document_type__oid == "Cedula de Extranjeria"){
            $tipo = "CE";
        }
        if($row->document__document_type__oid == "Registro Civil"){
            $tipo = "RC";
        }
        if($row->document__document_type__oid == "Tarjeta de Identidad"){
            $tipo = "TI";
        }
        if($row->document__document_type__oid == "NIT"){
            $tipo = "NT";
        }
        if($row->document__document_type__oid == "Adulto sin identificacion"){
            $tipo = "AS";
        }
        if($row->document__document_type__oid == "Menor sin identificacion"){
            $tipo = "MS";
        }if($row->document__document_type__oid == "Pasaporte"){
            $tipo = "PA";
        }

        //division de apellido
        $apellido = explode(" ", $row->last_name);
        $count = count($apellido);
        if($count > 0){
        $apellido1 = $apellido[0];
        $apellido2 = $apellido[1];
        }else{
        $apellido1 = $row->last_name;
        $apellido2 = "";
        }
        //division de apellido
        $nombre = explode(" ", $row->first_name);
        $count2 = count($nombre);
        if($count2 > 0){
        $nombre1 = $nombre[0];
        $nombre2 = $nombre[1];
        }else{
        $nombre1 = $row->first_name;
        $nombre2 = "";    
        }

    
        $data .= "{$tipo},{$row->document__number},{$row->codigoentidad},{$row->tipousuario},{$apellido1},{$apellido2},{$nombre1},{$nombre2},{$edad},3,{$row->gender},{$row->codigodepartamento},{$row->codigomunicipio},{$row->zona}\r\n";
}

//fecha actual
$fecha_actual=date("Y");
$fecha_hora=date("Y-m-d h:i:s");
//saco el semestre para enviarlo en el nombre del txt
$mes = date("m",strtotime($fecha_hora));
$mes = is_null($mes) ? date('m') : $mes;
$trim=floor(($mes-1) / 6)+1;
//tipos de header a enviar
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-disposition: attachment; filename="US'.$fecha_actual.'-'.$trim.'.txt"');
echo $data;
?>