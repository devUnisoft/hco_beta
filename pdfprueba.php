
<?php
ob_start();

$stringdientes =  $_POST["val2"];
$Observaciones =  $_POST["ob2"];

require('fpdf/fpdf.php');
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,10,'CARTA DENTAL',0,0,'C');
$pdf->Ln(7);

$pdf->Ln(18);

$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #11', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #12', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r11" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r12" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t11" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t12" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c11" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c12" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b11" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b12" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l11" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l12" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t11" class="cuadro click"></div><div id="l11" class="cuadro izquierdo click"></div><div id="b11" class="cuadro debajo click"></div><div id="r11" class="cuadro derecha click click"></div><div id="c11" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t12" class="cuadro click"></div><div id="l12" class="cuadro izquierdo click"></div><div id="b12" class="cuadro debajo click"></div><div id="r12" class="cuadro derecha click click"></div><div id="c12" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t11" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t12" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #13', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #14', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r13" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r14" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t13" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t14" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c13" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c14" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b13" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b14" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l13" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l14" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t13" class="cuadro click"></div><div id="l13" class="cuadro izquierdo click"></div><div id="b13" class="cuadro debajo click"></div><div id="r13" class="cuadro derecha click click"></div><div id="c13" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t14" class="cuadro click"></div><div id="l14" class="cuadro izquierdo click"></div><div id="b14" class="cuadro debajo click"></div><div id="r14" class="cuadro derecha click click"></div><div id="c14" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t13" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t14" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);



$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #15', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #16', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r15" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r16" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t15" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t16" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c15" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c16" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b15" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b16" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l15" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l16" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t15" class="cuadro click"></div><div id="l15" class="cuadro izquierdo click"></div><div id="b15" class="cuadro debajo click"></div><div id="r15" class="cuadro derecha click click"></div><div id="c15" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t16" class="cuadro click"></div><div id="l16" class="cuadro izquierdo click"></div><div id="b16" class="cuadro debajo click"></div><div id="r16" class="cuadro derecha click click"></div><div id="c16" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t15" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t16" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #17', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #18', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r17" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r18" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t17" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t18" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c17" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c18" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b17" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b18" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l17" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l18" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t17" class="cuadro click"></div><div id="l17" class="cuadro izquierdo click"></div><div id="b17" class="cuadro debajo click"></div><div id="r17" class="cuadro derecha click click"></div><div id="c17" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t18" class="cuadro click"></div><div id="l18" class="cuadro izquierdo click"></div><div id="b18" class="cuadro debajo click"></div><div id="r18" class="cuadro derecha click click"></div><div id="c18" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t17" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t18" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);



$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #21', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #22', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r21" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r22" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t21" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t22" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c21" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c22" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b21" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b22" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l21" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l22" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t21" class="cuadro click"></div><div id="l21" class="cuadro izquierdo click"></div><div id="b21" class="cuadro debajo click"></div><div id="r21" class="cuadro derecha click click"></div><div id="c21" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t22" class="cuadro click"></div><div id="l22" class="cuadro izquierdo click"></div><div id="b22" class="cuadro debajo click"></div><div id="r22" class="cuadro derecha click click"></div><div id="c22" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t21" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t22" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #23', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #24', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r23" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r24" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t23" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t24" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c23" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c24" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b23" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b24" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l23" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l24" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t23" class="cuadro click"></div><div id="l23" class="cuadro izquierdo click"></div><div id="b23" class="cuadro debajo click"></div><div id="r23" class="cuadro derecha click click"></div><div id="c23" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t24" class="cuadro click"></div><div id="l24" class="cuadro izquierdo click"></div><div id="b24" class="cuadro debajo click"></div><div id="r24" class="cuadro derecha click click"></div><div id="c24" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t23" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t24" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #25', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #26', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r25" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r26" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t25" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t26" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c25" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c26" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b25" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b26" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l25" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l26" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t25" class="cuadro click"></div><div id="l25" class="cuadro izquierdo click"></div><div id="b25" class="cuadro debajo click"></div><div id="r25" class="cuadro derecha click click"></div><div id="c25" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t26" class="cuadro click"></div><div id="l26" class="cuadro izquierdo click"></div><div id="b26" class="cuadro debajo click"></div><div id="r26" class="cuadro derecha click click"></div><div id="c26" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t25" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t26" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #27', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #28', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r27" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r28" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t27" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t28" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c27" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c28" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b27" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b28" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l27" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l28" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t27" class="cuadro click"></div><div id="l27" class="cuadro izquierdo click"></div><div id="b27" class="cuadro debajo click"></div><div id="r27" class="cuadro derecha click click"></div><div id="c27" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t28" class="cuadro click"></div><div id="l28" class="cuadro izquierdo click"></div><div id="b28" class="cuadro debajo click"></div><div id="r28" class="cuadro derecha click click"></div><div id="c28" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t27" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t28" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #31', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #32', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r31" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r32" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t31" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t32" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c31" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c32" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b31" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b32" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l31" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l32" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t31" class="cuadro click"></div><div id="l31" class="cuadro izquierdo click"></div><div id="b31" class="cuadro debajo click"></div><div id="r31" class="cuadro derecha click click"></div><div id="c31" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t32" class="cuadro click"></div><div id="l32" class="cuadro izquierdo click"></div><div id="b32" class="cuadro debajo click"></div><div id="r32" class="cuadro derecha click click"></div><div id="c32" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t31" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t32" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #33', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #34', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r33" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r34" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t33" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t34" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c33" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c34" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b33" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b34" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l33" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l34" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t33" class="cuadro click"></div><div id="l33" class="cuadro izquierdo click"></div><div id="b33" class="cuadro debajo click"></div><div id="r33" class="cuadro derecha click click"></div><div id="c33" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t34" class="cuadro click"></div><div id="l34" class="cuadro izquierdo click"></div><div id="b34" class="cuadro debajo click"></div><div id="r34" class="cuadro derecha click click"></div><div id="c34" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t33" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t34" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}




$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #35', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #36', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r35" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r36" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t35" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t36" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c35" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c36" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b35" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b36" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l35" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l36" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t35" class="cuadro click"></div><div id="l35" class="cuadro izquierdo click"></div><div id="b35" class="cuadro debajo click"></div><div id="r35" class="cuadro derecha click click"></div><div id="c35" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t36" class="cuadro click"></div><div id="l36" class="cuadro izquierdo click"></div><div id="b36" class="cuadro debajo click"></div><div id="r36" class="cuadro derecha click click"></div><div id="c36" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t35" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t36" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->addPage();


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #37', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #38', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="r37" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r38" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t37" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t38" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c37" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c38" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b37" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b38" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l37" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l38" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t37" class="cuadro click"></div><div id="l37" class="cuadro izquierdo click"></div><div id="b37" class="cuadro debajo click"></div><div id="r37" class="cuadro derecha click click"></div><div id="c37" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t38" class="cuadro click"></div><div id="l38" class="cuadro izquierdo click"></div><div id="b38" class="cuadro debajo click"></div><div id="r38" class="cuadro derecha click click"></div><div id="c38" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t37" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t38" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #41', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #42', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="41" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r42" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t41" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t42" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c41" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c42" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b41" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b42" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l41" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l42" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t41" class="cuadro click"></div><div id="l41" class="cuadro izquierdo click"></div><div id="b41" class="cuadro debajo click"></div><div id="r41" class="cuadro derecha click click"></div><div id="c41" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t42" class="cuadro click"></div><div id="l42" class="cuadro izquierdo click"></div><div id="b42" class="cuadro debajo click"></div><div id="r42" class="cuadro derecha click click"></div><div id="c42" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t41" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t42" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #43', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #44', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="43" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r44" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t43" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t44" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c43" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c44" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b43" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b44" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l43" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l44" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t43" class="cuadro click"></div><div id="l43" class="cuadro izquierdo click"></div><div id="b43" class="cuadro debajo click"></div><div id="r43" class="cuadro derecha click click"></div><div id="c43" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t44" class="cuadro click"></div><div id="l44" class="cuadro izquierdo click"></div><div id="b44" class="cuadro debajo click"></div><div id="r44" class="cuadro derecha click click"></div><div id="c44" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t43" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t44" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #45', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #46', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="45" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r46" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t45" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t46" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c45" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c46" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b45" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b46" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l45" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l46" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t45" class="cuadro click"></div><div id="l45" class="cuadro izquierdo click"></div><div id="b45" class="cuadro debajo click"></div><div id="r45" class="cuadro derecha click click"></div><div id="c45" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t46" class="cuadro click"></div><div id="l46" class="cuadro izquierdo click"></div><div id="b46" class="cuadro debajo click"></div><div id="r46" class="cuadro derecha click click"></div><div id="c46" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t45" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t46" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #47', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #48', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="47" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="r48" class="cuadro derecha click click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t47" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t48" class="cuadro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="c47" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="c48" class="centro click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="b47" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="b48" class="cuadro debajo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="l47" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="l48" class="cuadro izquierdo click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="t47" class="cuadro click"></div><div id="l47" class="cuadro izquierdo click"></div><div id="b47" class="cuadro debajo click"></div><div id="r47" class="cuadro derecha click click"></div><div id="c47" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t48" class="cuadro click"></div><div id="l48" class="cuadro izquierdo click"></div><div id="b48" class="cuadro debajo click"></div><div id="r48" class="cuadro derecha click click"></div><div id="c48" class="centro click"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t47" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="t48" class="cuadro click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);

$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #51', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #52', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche51" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche52" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche51" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche52" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche51" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche52" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche51" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche52" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche51" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche52" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche51" class="cuadro-leche top-leche click"></div><div id="lleche51" class="cuadro-leche izquierdo-leche click"></div><div id="bleche51" class="cuadro-leche debajo-leche click"></div><div id="rleche51" class="cuadro-leche derecha-leche click click"></div><div id="cleche51" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche52" class="cuadro-leche top-leche click"></div><div id="lleche52" class="cuadro-leche izquierdo-leche click"></div><div id="bleche52" class="cuadro-leche debajo-leche click"></div><div id="rleche52" class="cuadro-leche derecha-leche click click"></div><div id="cleche52" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche51" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche52" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #53', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #54', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche53" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche54" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche53" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche54" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche53" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche54" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche53" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche54" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche53" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche54" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche53" class="cuadro-leche top-leche click"></div><div id="lleche53" class="cuadro-leche izquierdo-leche click"></div><div id="bleche53" class="cuadro-leche debajo-leche click"></div><div id="rleche53" class="cuadro-leche derecha-leche click click"></div><div id="cleche53" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche54" class="cuadro-leche top-leche click"></div><div id="lleche54" class="cuadro-leche izquierdo-leche click"></div><div id="bleche54" class="cuadro-leche debajo-leche click"></div><div id="rleche54" class="cuadro-leche derecha-leche click click"></div><div id="cleche54" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche53" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche54" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #55', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #61', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche55" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche61" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche55" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche61" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche55" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche61" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche55" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche61" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche55" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche55" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche55" class="cuadro-leche top-leche click"></div><div id="lleche55" class="cuadro-leche izquierdo-leche click"></div><div id="bleche55" class="cuadro-leche debajo-leche click"></div><div id="rleche55" class="cuadro-leche derecha-leche click click"></div><div id="cleche55" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche61" class="cuadro-leche top-leche click"></div><div id="lleche61" class="cuadro-leche izquierdo-leche click"></div><div id="bleche61" class="cuadro-leche debajo-leche click"></div><div id="rleche61" class="cuadro-leche derecha-leche click click"></div><div id="cleche61" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche55" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche61" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}

$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #62', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #63', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche62" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche63" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche62" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche63" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche62" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche63" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche62" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche63" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche62" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche63" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche62" class="cuadro-leche top-leche click"></div><div id="lleche62" class="cuadro-leche izquierdo-leche click"></div><div id="bleche62" class="cuadro-leche debajo-leche click"></div><div id="rleche62" class="cuadro-leche derecha-leche click click"></div><div id="cleche62" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche63" class="cuadro-leche top-leche click"></div><div id="lleche63" class="cuadro-leche izquierdo-leche click"></div><div id="bleche63" class="cuadro-leche debajo-leche click"></div><div id="rleche63" class="cuadro-leche derecha-leche click click"></div><div id="cleche63" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche62" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche63" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #64', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #65', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche64" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche65" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche64" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche65" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche64" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche65" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche64" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche65" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche64" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche65" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche64" class="cuadro-leche top-leche click"></div><div id="lleche64" class="cuadro-leche izquierdo-leche click"></div><div id="bleche64" class="cuadro-leche debajo-leche click"></div><div id="rleche64" class="cuadro-leche derecha-leche click click"></div><div id="cleche64" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche65" class="cuadro-leche top-leche click"></div><div id="lleche65" class="cuadro-leche izquierdo-leche click"></div><div id="bleche65" class="cuadro-leche debajo-leche click"></div><div id="rleche65" class="cuadro-leche derecha-leche click click"></div><div id="cleche65" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche64" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche65" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}



$pdf->Ln(7);

$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #71', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #72', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche71" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche72" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche71" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche72" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche71" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche72" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche71" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche72" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche71" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche72" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche71" class="cuadro-leche top-leche click"></div><div id="lleche71" class="cuadro-leche izquierdo-leche click"></div><div id="bleche71" class="cuadro-leche debajo-leche click"></div><div id="rleche71" class="cuadro-leche derecha-leche click click"></div><div id="cleche71" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche72" class="cuadro-leche top-leche click"></div><div id="lleche72" class="cuadro-leche izquierdo-leche click"></div><div id="bleche72" class="cuadro-leche debajo-leche click"></div><div id="rleche72" class="cuadro-leche derecha-leche click click"></div><div id="cleche72" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche71" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche72" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #73', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #74', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche73" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche74" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche73" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche74" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche73" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche74" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche73" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche74" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche73" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche74" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche73" class="cuadro-leche top-leche click"></div><div id="lleche73" class="cuadro-leche izquierdo-leche click"></div><div id="bleche73" class="cuadro-leche debajo-leche click"></div><div id="rleche73" class="cuadro-leche derecha-leche click click"></div><div id="cleche73" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche74" class="cuadro-leche top-leche click"></div><div id="lleche74" class="cuadro-leche izquierdo-leche click"></div><div id="bleche74" class="cuadro-leche debajo-leche click"></div><div id="rleche74" class="cuadro-leche derecha-leche click click"></div><div id="cleche74" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche73" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche74" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #75', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #81', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche75" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche81" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche75" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche81" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche75" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche81" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche75" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche81" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche75" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche81" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche75" class="cuadro-leche top-leche click"></div><div id="lleche75" class="cuadro-leche izquierdo-leche click"></div><div id="bleche75" class="cuadro-leche debajo-leche click"></div><div id="rleche75" class="cuadro-leche derecha-leche click click"></div><div id="cleche75" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche81" class="cuadro-leche top-leche click"></div><div id="lleche81" class="cuadro-leche izquierdo-leche click"></div><div id="bleche81" class="cuadro-leche debajo-leche click"></div><div id="rleche81" class="cuadro-leche derecha-leche click click"></div><div id="cleche81" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche75" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche81" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);

$pdf->addPage();

$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #82', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #83', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche82" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche83" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche82" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche83" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche82" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche83" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche82" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche83" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche82" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche83" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche82" class="cuadro-leche top-leche click"></div><div id="lleche82" class="cuadro-leche izquierdo-leche click"></div><div id="bleche82" class="cuadro-leche debajo-leche click"></div><div id="rleche82" class="cuadro-leche derecha-leche click click"></div><div id="cleche82" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche83" class="cuadro-leche top-leche click"></div><div id="lleche83" class="cuadro-leche izquierdo-leche click"></div><div id="bleche83" class="cuadro-leche debajo-leche click"></div><div id="rleche83" class="cuadro-leche derecha-leche click click"></div><div id="cleche83" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche82" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche83" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);


$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100;
$pdf->MultiCell($width, 6, 'Diente  #84', 0, 'L', FALSE);
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente  #85', 0, 0, "l");


$pdf->Ln(7);
//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche84" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Mesial', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="rleche85" class="cuadro-leche derecha-leche click click click-red"></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Mesial', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche84" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Vestibular', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche85" class="cuadro-leche top-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Vestibular', 0, 0, "l");
$pdf->Ln(7);
}


//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche84" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Oclusal', 0, 'L', FALSE);
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="cleche85" class="centro-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Oclusal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche84" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Lingual', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="bleche85" class="cuadro-leche debajo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Lingual', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche84" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Distal', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="lleche85" class="cuadro-leche izquierdo-leche click click-red">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Distal', 0, 0, "l");
$pdf->Ln(7);
}

//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche84" class="cuadro-leche top-leche click"></div><div id="lleche84" class="cuadro-leche izquierdo-leche click"></div><div id="bleche84" class="cuadro-leche debajo-leche click"></div><div id="rleche84" class="cuadro-leche derecha-leche click click"></div><div id="cleche84" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Diente sano', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '><div id="tleche85" class="cuadro-leche top-leche click"></div><div id="lleche85" class="cuadro-leche izquierdo-leche click"></div><div id="bleche85" class="cuadro-leche debajo-leche click"></div><div id="rleche85" class="cuadro-leche derecha-leche click click"></div><div id="cleche85" class="centro-leche click"></div></div>');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Diente sano', 0, 0, "l");
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche84" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 100; 	
$pdf->MultiCell($width, 6, 'Ausente', 0, 'L', FALSE);
$pdf->Ln(7);
}
//validacion por diente
$pos = strpos($stringdientes, '<div id="tleche85" class="cuadro-leche top-leche click click-delete">');
 if ($pos == false) {
 //se deja vacio
 } else {
$pdf->SetXY($x + $width, $y);
$pdf->Cell(40,6, 'Ausente', 0, 0, "l");
$pdf->Ln(7);
}


$pdf->Ln(7);
$pdf->Ln(7);
$pdf->Cell(100,12,"Observaciones: ");
$pdf->Ln(9);
$y = $pdf->GetY();
$x = $pdf->GetX();
$width = 150; 	
$pdf->MultiCell($width, 6, ''.$Observaciones, 0, 'L', FALSE);
$pdf->Ln(7);

$pdf->Output();
ob_end_flush(); 
?>
