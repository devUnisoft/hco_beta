<?php
require('fpdf/WriteHTML.php');

$pdf=new PDF_HTML();

$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true, 15);

$pdf->AddPage();
$pdf->Image('http://190.60.211.17/hco/images/headerimg.png',0,0,210);
$pdf->SetFont('Arial','B',14);
$pdf->WriteHTML('<!DOCTYPE html>
        <html>
        <head>
          <title>Presupuestos</title>
        </head>
        <body>
        <header>
        <img src="http://190.60.211.17/hco/images/headerimg.png"  height="100px" width="100%">
        </header>
        <div class="container-fluid">
          <div class="row">
            </br>
            <div class="col-md-2">  
            </div>
            <div class="col-md-10">
            <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
            <div class="container-fluid">
            </br>
            <div class="row">
            <center><h4><font color="#0683c9">PRESUPUESTO PARA PROCEDIMIENTOS ODONTOLOGICOS</font></h4></center>
            <div class="col-md-12">
            <div class="col-md-1">
            </div>  
            <div class="col-md-4">
            <font color="#0683c9">Nombre de Profesional</font>
            <input class="form-control" type="text" name="nombreprofesional" id="nombreprofesional" value="'.$nombrec.''.$apellidoc.'" readonly required />    
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            <font color="#0683c9">Nombre de Paciente</font>
            <input class="form-control" type="text" name="nombrepaciente" id="nombrepaciente" value="'.$nombresp.'" readonly required />  
            </div>
            <div class="col-md-1">
            </div>
            </div>

            <div class="col-md-12">
            <div class="col-md-1">
            </div>  
            <div class="col-md-4">
            <font color="#0683c9">Fecha de Presupuesto</font>
            <input class="form-control" type="text" name="fechapresupuesto" id="fechapresupuesto"  value="'.$fecha_actual.'" readonly required />    
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            <font color="#0683c9">No.Id de Paciente</font>
            <input class="form-control" type="text" name="idpaciente" id="idpaciente"  value="'.$numerodocumento.'" readonly required />  
            </div>
            <div class="col-md-1">
            </div>
            </div>

            <div class="col-md-12">
            <div class="col-md-1">
            </div>  
            <div class="col-md-4">    
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            <font color="#0683c9">Email</font>
            <input class="form-control" type="text" name="emailpacientes" id="emailpacientes" value="'.$correop.'" readonly required />  
            </div>
            <div class="col-md-1">
            </div>
            </div>

            <div class="col-md-12">
            <div class="col-md-1">
            </div>  
            <div class="col-md-4">    
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            <font color="#0683c9">Direccion</font>
            <input class="form-control" type="text" name="direccionpaciente" id="direccionpaciente" value="'.$direccionp.'" readonly required />  
            </div>
            <div class="col-md-1">
            </div>
            </div>

            <div class="col-md-12">
            <div class="col-md-1">
            </div>  
            <div class="col-md-4">    
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
            <font color="#0683c9">Telefono</font>
            <input class="form-control" type="text" name="telefonopaciente" id="telefonopaciente" value="'.$telefonop.'" readonly required />  
            </div>
            <div class="col-md-1">
            </div>
            </div>
            <div class="col-md-12">
            </br>
            </br>
            </br>
            <div class="col-md-1">
            </div>
            <div class="col-md-10">

        <div>
                 <table class="table table-bordered table-hover">;
                 <thead>
                    <tr>
                      <th>Tratamiento</th>
                      <th>Procedimiento</th>
                      <th>Valor</th>
                    </tr>
                  </thead>
                  <tbody>
                
            </div>
            </div>


            <div class="col-md-12">
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
            <font color="#0683c9">No de Sesiones</font> 
            </div>
            <div class="col-md-1">
            <input class="form-control" type="number" name="sessiones" id="sessiones" value="'.$cuotas.'" readonly required />  
            </div>
            <div class="col-md-1">
            <font color="#0683c9">Descuento</font>
            </div>
            <div class="col-md-1">
            <input class="form-control"  type="number" min="0" max="100" name="descuento" id="descuento" value="'.$descuentop.'" readonly  required />  
            </div>
            <div class="col-md-3">
            <font color="#0683c9">VALOR TOTAL DEL TRATAMIENTO</font> 
            </div>
            <div class="col-md-3">
            <input class="form-control" type="number" name="valortotal" id="valortotal" value="'.$valorpresupuesto.'" readonly  required  readonly > 
            </div>
            </div>

            <div class="col-md-12">
            </br>
            </br>
            </br>
            </br>
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
            <font color="#0683c9">OBSERVACIONES</font> 
            </div>
            <div class="col-md-8">
            <textarea  style="width: 100%; height: 50%"  readonly>'.$observacionesp.'</textarea>
            </div>
            </div>
            </div>
            </div>
            </div>
          </div>
          </div>
          </div>
          </br>
        </br>
        </br>
        </br>
        </br>
        </div>

        <div class="col-md-12" style="width: 100%;">
        <hr style="border-top: 2px solid #0683c9;">
        </div>
        <div class="col-md-12" style="width: 100%;">
        <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="http://190.60.211.17/hco/images/tel.jpeg"  height="45px" width="45px">

        <img style="float: right!important;" src="http://190.60.211.17/hco/images/10.png"  height="45px" width="45px"> 
        <img style="float: right!important;" src="http://190.60.211.17/hco/images/11.png"  height="45px" width="45px"> 
        <img style="float: right!important;" src="http://190.60.211.17/hco/images/12.png"  height="45px" width="45px"> 
        <img style="float: right!important;" src="http://190.60.211.17/hco/images/13.png"  height="45px" width="45px"> 
        <img style="float: right!important;" src="http://190.60.211.17/hco/images/14.png"  height="45px" width="45px">
        </p>
        </div>
            <!-- javascript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="js/jquery.js"></script>
            <script src="js/bootstrap.js"></script>
            <script src="js/fullcalendar.js"></script>
            <script src="js/gcal.js"></script>
            <script src="js/jquery-ui.js"></script>
            <script src="js/jquery.calendar.js"></script>
            <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
            <script src="lib/validation/jquery.validationEngine.js"></script>
            <script src="lib/validation/jquery.validationEngine-en.js"></script>
            <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
            <script src="lib/timepicker/jquery-ui-timepicker-addon.min.js"></script>
            <script src="js/custom.js"></script>
            <script src="js/jquery-1.8.2.min.js"></script>
            <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
            <script>$(".chzn-select").chosen(); $(".chzn-select-deselect").chosen({allow_single_deselect:true});</script>  
        </body>
        </html>');
$pdf->Image('http://190.60.211.17/hco/images/footer.png', 0, null, 210);
$pdf->SetFont('Arial','B',7); 

$pdf->SetFont('Arial','B',6);
ob_end_clean();
$pdf->Output();

?>