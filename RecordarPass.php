<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Recordar Contraseña</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
	</script>
	<style type="text/css">
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}

		header {
    width: 100%;
    height: 150px;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
	</style>
	<script type="text/javascript">
		function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
         window.onload = init();
	</script>	
</head>
<body>

<header>
    <div class="container clearfix">
        </br>
        <center><img src="images/logo_blanco.png"  height="15%" width="15%"></center>
    </div>
</header>

<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
	<div class="row">
		</br>
		<center><h2>¿OLVIDO LA CONTRASEÑA?</h2></center>
		</br>
		<div class="col-md-4">
		</div>
		<div class="col-md-6">
			<form role="form" name="fcontacto" class="form-inline" method="post" action="recuperar.php">
				
				<div class="form-group">
					<label for="correo">
						Correo Electronico
					</label>
					<input type="email" class="form-control" id="correo" name="correo"  required />
					<button type='button' value='Atras' onClick='history.go(-1);' class="btn btn-primary pull-right">Regresar</button>
					<button type="submit" class="btn btn-primary pull-right">
					Reiniciar Contraseña
				    </button>
				</div>
			</form>
		</div>
		<div class="col-md-2">
		</div>
	</div>
</div>


	
</body>
</html>