<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login_radiologia.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $centro_radiologico = $_SESSION['id_centro_radiologico'];
    $logo_centro_radiologico = $_SESSION['logo_centro_radiologico']; 
    $clave = base64_encode($password);
    //selecionamos el usuario

    $result = mysql_query("SELECT * FROM users_radiologia where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   

    while($row = mysql_fetch_array($result)){
      $pfil = $row["__t"];
    }
   
    if($pfil =="Profesional")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    
      header ("Location: turneroProfesionales.php");
    }
    else if($pfil =="Asistente")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual   
      header ("Location: home.php");
    }
    else if($pfil =="Administrador")
    {
      //header ("Location: homeAdmin.php");
    }
    else
    {
      header ("Location: index_radiologia.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>REPORTE CIERRE DE CAJA</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/ValidacionNumerica.js"></script>
  <style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 5px; 
     border-right: 1px solid;
     font-weight: bold;
}
  </style>
  
</head>
<body>

<header>
  <?php include 'headerRadiologia.php';?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">REPORTE CIERRE DE CAJA</h3>
  <br>
  <div class="row"> 
    <form id="frmFind" method="post" action="">
      <div class="col-md-12">   
        <div class="col-md-1" style="padding: 0">   
          <label style="margin-top: 7px"><font color="#0683c9">FUNCIONARIO</font></label>
        </div>
        <div class="col-md-2" style="padding: 0">   
          <select id="selectFuncionarios" name="selectFuncionarios" class="form-control">
          <?php
            $result = mysql_query("SELECT * FROM `users_radiologia` WHERE `__t`='Asistente' AND `id_centro_radiologico`='$centro_radiologico' ORDER BY `first_name` ASC, `last_name` ASC");   

            while($row = mysql_fetch_array($result)){
              $name = $row["first_name"] . " " . $row["last_name"];
              $id = $row["email"];
          ?>
            <option value="<?= $id;?>"><?= $name;?></option>
          <?php  
            }
          ?>
          </select>
        </div> 
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA INICIAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaInicial" id="txtFechaInicial" style="text-align: center;" class="form-control">  
        </div>  
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA FINAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaFinal" id="txtFechaFinal" style="text-align: center;" class="form-control">  
        </div>
        <div class="col-md-1" style="padding: 0" align="right">   
          <button class="btn btn-primary" id="btnBuscar" type="submit" style="width: 90%"><b>BUSCAR</b></button>   
        </div>
      </div>
    </form>
  </div>
  <hr size="2px" style="border-color: #0683c9" />

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
    <thead>
      <tr style="background-color: #0683c9; color: white">
        <td>FECHA</td>
        <td>HORA</td>
        <td>FACTURA</td>
        <td>NOMBRE</td>
        <td>DESCRIPCION</td>
        <td>CANTIDAD</td>
        <td>VALOR</td>
        <td>ESTADO</td>
      </tr>
    </thead>
    <tbody id="tbodyDatosFactura" style="font-weight: normal;">
                                 
    </tbody>
  </table>

  

 
  <br><br>
  <div class="row">    
    <div class="col-md-8" align="right">
      <label style="margin-top: 6px"><font color="#0683c9">VALOR TOTAL</font></label>    
    </div>
    <div class="col-md-2" align="center">
      <input type="tex" id="txtValorTotal" name="txtValorTotal" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">    
    </div>
    <div class="col-md-2" align="right">
      <button class="btn btn-success" disabled="disabled" id="btnExportar">EXPORTAR INFORME</button>    
    </div>
  </div>

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 0px">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="col-md-4">
            <img width="100%" src="images/LOGO SW CENTRAL.png">
          </div>
          <div class="col-md-6">
            <h4 align="center" id="h4Mensaje"></h4>
          </div>

        </div>
        <div class="modal-body" style="border: 0px">
          
        </div>
        <div class="modal-footer" style="border: 0px">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAceptarAnulacion">ACEPTAR</button>
        </div>
      </div>

    </div>
  </div>
  <br>
  <div id="footer">
    <div align="center">
      <a href="homeAdmin.php">
        <img src="images/btn_inicio.png" title="Salir" height="50px" width="50px" style="margin-top: -20px">
        <label style="position: absolute; margin-top: 45px; margin-left: -50px;">Inicio</label>
      </a>
      <img src="images/separador.png"  height="50px" width="30px">

      <a href="reporteCierreCaja"><img id="REPORTE CIERRE CAJA" src="images/reporteCajaNegro.png" title="Ayuda" width="100px"></a>
      <img src="images/separador.png"  height="50px" width="30px">

      <a href="reporteProcesosDesarrollados.php"><img id="REPORTE PROCESO DESARROLLADO" src="images/reporteGeneralNegro.png" title="Ayuda"  width="100px"></a>
      <img src="images/separador.png"  height="50px" width="30px">       
    
    </div> 
  </div>
     <br><br>
  

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <!-- call calendar plugin -->
    <script type="text/javascript">
    $().FullCalendarExt({
      calendarSelector: '#calendar',
      quickSaveCategory: '<label>Category: </label><select name="categorie"><?php foreach($calendar->getCategories() as $categorie) { ?> <option value="<?php echo $categorie?>"><?php echo $categorie; ?></option><?php } ?></select>',
    });
  </script>
  <script src="js/jquery.validate.js"></script>
  <script type="text/javascript">
    $.validator.setDefaults({
        //Capturar evento del boton crear
        submitHandler: function() {
          var identificacion = $("#selectFuncionarios").val();
          var fechaInicial = $("#txtFechaInicial").val();
          var fechaFinal = $("#txtFechaFinal").val();
          $("#tbodyDatosFactura").html("")
          
          $("#btnAnular").attr("disabled", "disabled")  
          //Se guardan los datos en un JSON
          var datos = {
              document: identificacion,
              fechaInicial: fechaInicial,
              fechaFinal: fechaFinal
          }   
          $.post("findreportecierrecaja.php", datos)
          .done(function( data ) {console.log(data)             
            if($.trim(data) != "[]"){
              var json = JSON.parse(data)
              $("#txtNombre").val(json[0].first_name + " " + json[0].last_name)
              var total = 0;

              for (var i = 0; i < json.length; i++) {    
                var estado = "";      
                if($.trim(json[i].estadoFactura) == "ACTIVO"){
                  estado = "APROBADA";      
                }else{
                  estado = json[i].estadoFactura; 
                }       
                $("#tbodyDatosFactura").append("<tr><td>" + json[i].fecha + "</td><td>" + json[i].hora + "</td><td>" + json[i].id_factura_radiologia + "</td><td>" + json[i].first_name + " " + json[i].last_name + "</td><td>" + json[i].descripcion + "</td><td>" + json[i].cantidad + "</td><td>" + json[i].total + "</td><td>" + estado + "</td></tr>")  
                if($.trim(json[i].estadoFactura) == "ACTIVO"){
                  total += parseInt(json[i].total)
                }
                
              }     
              if($("#tbodyDatosFactura > tr").length > 0){
                $("#btnExportar").removeAttr("disabled")
              }else{
                $("#btnExportar").attr("disabled", "disabled")
              }
              $("#txtValorTotal").val(total)     
            }else{
              $("#txtNombre").val("")   
              $("#btnAnular").attr("disabled", "disabled")  
              $("#btnAceptarAnulacion").css({"display":"none"})
              $("#h4Mensaje").html("¡No hay facturas disponibles para anular!");
              $('#myModal').modal();
            }
          });         
          
        }
    });
  
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmFind").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmFind").validate({
          rules: {
            txtIdentificacion:"required",
            txtFechaInicial:"required",
            txtFechaFinal:"required"
          },
          messages: {
            txtIdentificacion: "Por favor ingrese un numero de documento",
            txtFechaInicial: "Por favor ingrese una fecha incial",
            txtFechaFinal: "Por favor ingrese una fecha final"
          }
        });
        
    })();

</script>
</body>
</html>