<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica
    $_SESSION['clinica'] = $_POST["clinica"];

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login_radiologia.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $clave = base64_encode($password);
    //selecionamos el usuario
    $centro_radiologico = $_SESSION['id_centro_radiologico'];

    $result = mysql_query("SELECT * FROM users_radiologia where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   

    while($row = mysql_fetch_array($result)){
      $pfil = $row["__t"];
    }

    if($pfil =="Profesional")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    
      header ("Location: turneroProfesionales.php");
    }
    else if($pfil =="Asistente")
    {
     header ("Location: home.php");  
    }
    else if($pfil =="Administrador")
    {
     //header ("Location: menuadmin.php");
    }
    else
    {
      header ("Location: index_radiologia.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>ADMINISTRACION DE PROFESIONALES</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/ValidacionNumerica.js"></script>
  <style type="text/css">
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
      font-weight: bolder;
    }
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
td, th {
     padding: 10px; 
}
  </style>
  
</head>
<body>

<header>
  <?php include 'headerRadiologia.php';?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">ADMINISTRACION DE PROFESIONALES</h3>
  <br>
  <div class="row"> 
        
    <div class="col-md-12">
      <form id="frmCrear" method="post" action="">    
        <label for="selectTipoDocumento"><font>Tipo de Documento</font></label><br>
        <select class="form-control" id="selectTipoDocumento" tabindex="1">
          <option>Cedula de Ciudadania</option>
          <option>Cedula de Extranjeria</option>
          <option>Pasaporte</option>
        </select><br>
        
        <label for="txtNumeroDoc"><font>Número de Documento</font></label><br>
        <input type="text" name="txtNumeroDoc" id="txtNumeroDoc" class="form-control" tabindex="2">
        <input type="hidden" name="txtNumeroDocHidden" id="txtNumeroDocHidden" class="form-control"><br>
                
        <div class="col-md-6">
          <label for="txtNombre"><font>Nombre</font></label><br>
          <input type="text" name="txtNombre" id="txtNombre" class="form-control" tabindex="3"><br>

          <label for="txtEmail"><font>E-mail</font></label><br>
          <input type="email" name="txtEmail" id="txtEmail" class="form-control" tabindex="5">
          <input type="hidden" name="txtEmailHidden" id="txtEmailHidden" class="form-control"><br>

          <label for="selectLugarNacimiento"><font>Lugar de Nacimiento</font></label><br>
          <select class="form-control" id="selectLugarNacimiento" tabindex="7">
            <?php
              $result = mysql_query("SELECT * FROM `georeferences` ORDER BY name");   

              while($row = mysql_fetch_array($result)){
                $name = $row["name"];
                $id = $row["id"];
            ?>
              <option value="<?= $name;?>"><?= $name;?></option>
            <?php  
              }
            ?>
          </select><br>

          <label for="txtClave"><font>Contraseña</font></label><br>
          <input type="password" name="txtClave" id="txtClave" class="form-control" tabindex="9"><br>

          <label for="selectConsultorio"><font>Asignación de Consultorio</font></label><br>
          <select class="form-control" id="selectConsultorio" tabindex="1">
             <?php
              $result = mysql_query("SELECT * FROM `consultorio_radiologia` WHERE `id_centro_radiologico`='$centro_radiologico'");   

              while($row = mysql_fetch_array($result)){
                $name = $row["name"];
                $id = $row["id"];
            ?>
              <option value="<?= $id;?>"><?= $name;?></option>
            <?php  
              }
            ?>
          </select><br>
        </div>

        <div class="col-md-6">
          <label for="txtApellido"><font>Apellido</font></label><br>
          <input type="text" name="txtApellido" id="txtApellido" class="form-control" tabindex="4"><br>

          <label for="txtFechaNacimiento"><font>Fecha de Nacimiento</font></label><br>
          <input type="date" name="txtFechaNacimiento" id="txtFechaNacimiento" class="form-control" tabindex="6"><br>

          <label for="selectLugarResidencia"><font>Lugar de Residencia</font></label><br>
          <select class="form-control" id="selectLugarResidencia" tabindex="8">
            <?php
              $result = mysql_query("SELECT * FROM `georeferences` ORDER BY name");   

              while($row = mysql_fetch_array($result)){
                $name = $row["name"];
                $id = $row["id"];
            ?>
              <option value="<?= $name;?>"><?= $name;?></option>
            <?php  
              }
            ?>
          </select><br>

          <label for="txtRepetirClave"><font>Repita su Contraseña</font></label><br>
          <input type="password" name="txtRepetirClave" id="txtRepetirClave" class="form-control"  tabindex="10"><br>
        </div>

        <div class="col-md-12">   
          <label><font>Genero</font></label>
          <div class="radio">
            <label><input type="radio" name="radioGenero" id="radioMasculino" value="Masculino" tabindex="11">Masculino</label>
          </div>
          <div class="radio">
            <label><input type="radio" name="radioGenero" id="radioFemenino" value="Femenino" tabindex="12">Femenino</label>
          </div>
        </div>

        <div class="col-md-12">
          <br>
          <button class="btn btn-primary pull-right" type="submit" id="btnEnviar" tabindex="13">CREAR</button>    
        </div>
      </form>    
    </div> 
    
  </div>
  <br>
     <?php include 'footerRadiologia.php';?> 
     <br><br>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <!-- call calendar plugin -->
    <script type="text/javascript">
    $().FullCalendarExt({
      calendarSelector: '#calendar',
      quickSaveCategory: '<label>Category: </label><select name="categorie"><?php foreach($calendar->getCategories() as $categorie) { ?> <option value="<?php echo $categorie?>"><?php echo $categorie; ?></option><?php } ?></select>',
    });
  </script>
  <script src="js/jquery.validate.js"></script>
  <script type="text/javascript">
    
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrear").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmCrear").validate({
          rules: {
            txtNombre:"required",
            txtNumeroDoc:"required",
            txtApellido:"required",
            txtEmail:"required",
            txtFechaNacimiento:"required",
            txtClave:"required",
            txtRepetirClave: {
              required: true,
              equalTo: "#txtClave"
            },
            radioGenero:"required"
          },
          messages: {
            txtNombre: "Por favor ingrese un nombre",
            txtNumeroDoc: "Por favor ingrese un numero de documento",
            txtApellido: "Por favor ingrese el apellido",
            txtEmail: "Por favor ingrese un e-mail válido",
            txtFechaNacimiento: "Por favor ingrese una fecha de nacimiento",
            txtClave: "Por favor ingrese una contraseña",
            txtRepetirClave: {
              required: "Por favor ingrese una contraseña",
              equalTo: "Por favor repita la contraseña ingresada en el campo Contraseña"
            },
            radioGenero: "Por favor seleccione un genero"
          },
          submitHandler: function(form) {
            var typeuser = "Profesional";
            var typedoc = $("#selectTipoDocumento").val();
            var numerodoc = $("#txtNumeroDoc").val();
            var nombre = $("#txtNombre").val();
            var apellido = $("#txtApellido").val();
            var email = $("#txtEmail").val();
            var birthdate = $("#txtFechaNacimiento").val();
            var lugarNac = $("#selectLugarNacimiento").val();
            var consultorio = $("#selectConsultorio").val();
            var lugarResi = $("#selectLugarResidencia").val();
            var clave = $("#txtClave").val();
            var genero = $('input[name=radioGenero]:checked').val();            
            
            if($.trim($("#btnEnviar").html()) == "CREAR"){
              //Se guardan los datos en un JSON
              var datos = {
                TipoUsu: typeuser,
                TipoDoc: typedoc,
                NumeroDoc: numerodoc,
                nombre: nombre,
                apellido: apellido,
                email: email,
                birthdate: birthdate,
                lugarNac: lugarNac,
                lugarResi: lugarResi,
                genero: genero,
                consultorio: consultorio,
                clave: clave
              }   
              //Enviar datos para modificarse
              $.post("addUsuarioRadiologia.php", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se registro con exito el profesional!")
                 $("input").val("")
                 $("#selectTipoDocumento").val("Cedula de Ciudadania")
                 $("#selectLugarResidencia").val("ABEJORRAL")
                 $("#selectLugarNacimiento").val("ABEJORRAL")
                 window.location.href = "adminProfesionales.php";
              });
            }else{
              var numerodocantes = $("#txtNumeroDocHidden").val();
              //Se guardan los datos en un JSON
              var datos = {
                TipoUsu: typeuser,
                TipoDoc: typedoc,
                NumeroDoc: numerodoc,
                NumeroDocAntes: numerodocantes,
                nombre: nombre,
                apellido: apellido,
                email: email,
                birthdate: birthdate,
                lugarNac: lugarNac,
                lugarResi: lugarResi,
                genero: genero,
                consultorio: consultorio,
                clave: clave
              }   

              //Enviar datos para crearse 
              $.post("updateUsuarioRadiologia.php", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se modifico con exito el profesional!")
                 $("input").val("")
                 $("#selectTipoDocumento").val("Cedula de Ciudadania")
                 $("#selectLugarResidencia").val("ABEJORRAL")
                 $("#selectLugarNacimiento").val("ABEJORRAL")
                 window.location.href = "adminProfesionales.php";
              });
            }
            
          }
        });     
        
    })();

    //Validacion del campo codigo para que no se repita
    $("#txtNumeroDoc").keyup(function(e){
      var documento = $("#txtNumeroDoc").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        NumeroDoc: documento,
        typeuser: "Profesional"
      }   
      
      $.post("findUsuarioRadiologia.php", datos)
      .done(function( data ) {console.log(data)             
        if($.trim(data) != "{}"){
          var json = JSON.parse(data)
          $("#selectTipoDocumento").val(json.document__document_type__oid);
          $("#txtNumeroDoc").val(json.document__number);
          $("#txtNumeroDocHidden").val(json.document__number);
          $("#txtNombre").val(json.first_name);
          $("#txtApellido").val(json.last_name);
          $("#txtEmail").val(json.email);
          $("#txtEmailHidden").val(json.email);
          $("#txtFechaNacimiento").val(json.birthdate);
          $("#selectLugarNacimiento").val(json.lugar_nacimiento);
          $("#selectLugarResidencia").val(json.lugar_residencia);
          $("#selectConsultorio").val(json.consultorio);
          $("#txtClave").val(json.salt);
          $("#txtRepetirClave").val(json.salt);
          if($.trim(json.gender) == "Masculino"){
            $("#radioMasculino").prop("checked", true)
          }else{
            $("#radioFemenino").prop("checked", true)
          }      

          $("#btnEnviar").html("MODIFICAR")
        }else{
          $("#btnEnviar").html("CREAR")
        }
        
         
      });
      
    })

    //Validacion del campo nombre para que no se repita en la creacion
    /*$("#txtNombre").keyup(function(e){
      var nombre = $("#txtNombre").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        nombre: nombre
      }   
      
      $.post("validateNombreProcedimientoRadio.php", datos)
      .done(function( data ) {console.log(data)             
        var json = JSON.parse(data)
        if($.trim(json.filas) != "0"){
          $("#spanErrorNombre").css({"display":"block"})
          $("#spanErrorNombre").html("¡El nombre de este procedimiento en el centro radiologico ya existe!")
        }else{
          $("#spanErrorNombre").css({"display":"none"})
          $("#spanErrorNombre").html("")
        }
        mostrarBotonCrear()
      });
    })*/  

    

    //funcion que valida si hay errores para habilitar el boton crear
    function mostrarBotonCrear(){
      var contador = 0;
      $( ".errorDato" ).each(function( index ) {
        if($( this ).text() != ""){
          contador++;
        }
      });
      if(contador > 0){
        $("#btnCrear").attr("disabled", "disabled")
      }else{
        $("#btnCrear").removeAttr("disabled")
      }
    }

    
</script>
</body>
</html>