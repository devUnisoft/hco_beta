/*
 *	jQuery FullCalendar Extendable Plugin
 *	An Ajax (PHP - Mysql - jquery) script that extends the functionalities of the fullcalendar plugin
 *  Dependencies: 
 *   - jquery
 *   - jquery Ui
 * 	 - jquery colorpicker (since 1.6.4)
 *   - jquery timepicker (since 1.6.4)
 *   - jquery Fullcalendar
 *   - Twitter Bootstrap
 *  Author: Paulo Regina
 *  Website: www.pauloreg.com
 *  Contributions: Patrik Iden, Jan-Paul Kleemans, Bob Mulder
 *	Version 1.6.4, July - 2014 
 *  Fullcalendar 1.6.4
 *	Released Under Envato Regular or Extended Licenses
 */


 
(function($, undefined) 
{
	$.fn.extend 
	({
		// FullCalendar Extendable Plugin
		FullCalendarExt: function(options) 
		{	
			// Default Configurations (General)
            var defaults = 
			{
				calendarSelector: '#calendar',
								
				ajaxJsonFetch: 'includes/cal_events.php',
				ajaxUiUpdate: 'includes/cal_update.php',
				ajaxEventSave: 'includes/cal_save.php',
				ajaxEventQuickSave: 'includes/cal_quicksave.php',
				ajaxEventDelete: 'includes/cal_delete.php',
				ajaxEventEdit: 'includes/cal_edit_update.php',
				ajaxEventExport: 'includes/cal_export.php',
				ajaxRepeatCheck: 'includes/cal_check_rep_events.php',
				ajaxRetrieveDescription: 'includes/cal_description.php',
				
				modalViewSelector: '#cal_viewModal',
				modalEditSelector: '#cal_editModal',
				modalQuickSaveSelector: '#cal_quickSaveModal',
				modalPromptSelector: '#cal_prompt',
				modalEditPromptSelector: '#cal_edit_prompt_save',
				formAddEventSelector: 'form#add_event',
				formFilterSelector: 'form#filter-category select',
				formEditEventSelector: 'form#edit_event', // php version
				formSearchSelector:"form#search",
				
				successAddEventMessage: 'Successfully Added Event',
				successDeleteEventMessage: 'Successfully Deleted Event',
				successUpdateEventMessage: 'Successfully Updated Event',
				failureAddEventMessage: 'Failed To Add Event',
				failureDeleteEventMessage: 'Failed To Delete Event',
				failureUpdateEventMessage: 'Failed To Update Event',
				generalFailureMessage: 'Failed To Execute Action',
				ajaxError: 'Failed to load content',
				
				visitUrl: 'Visit Url:',
				titleText: 'Title:',
				descriptionText: 'Description:',
				colorText: 'Color:',
				startDateText: 'Start Date:',
				startTimeText: 'Start Time:',
				endDateText: 'End Date:',
				endTimeText: 'End Time:',
				categoryText: 'Category:',
				eventText: 'Event: ',
				repetitiveEventActionText: 'Alerta evento repetido en el calendario que desea hacer?',
								
				isRTL: false,				
				monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
				monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Agos','Sep','Oct','Nov','Dic'],
				dayNames: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
				dayNamesShort: ['Dom','Lun','Mar','Mier','Juev','Vier','Sab'],
				today: 'hoy',
				month: 'mes',
				week: 'semana',
				day: 'dia',
				weekNumberTitle: 'W',
				allDayText: 'Todo-El-Dia', 
				
				defaultColor: '#587ca3',
				
				weekType: 'agendaWeek', // basicWeek
				dayType: 'agendaDay', // basicDay
				
				editable: true,
				disableDragging: false,
				disableResizing: false,
				ignoreTimezone: true,
				lazyFetching: true,
				filter: true,
				quickSave: true,
				firstDay: 0,
				
				gcal: false,
				
				version: 'modal',
				
				quickSaveCategory: '',
				
				colorpickerArgs: {format: 'hex'},
				
				defaultView: 'month', // basicWeek or basicDay or agendaWeek
				aspectRatio: 1.35, // will make day boxes bigger
				weekends: true, // show (true) the weekend or not (false)
				weekNumbers: false, // show week numbers (true) or not (false)
				weekNumberCalculation: 'iso',
				
				hiddenDays: [], // [0,1,2,3,4,5,6] to hide days as you wish
				
				theme: false,
				themePrev: 'circle-triangle-w',
				themeNext: 'circle-triangle-e',
				
				titleFormatMonth: 'MMMM yyyy',
				titleFormatWeek: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
				titleFormatDay: 'dddd, MMM d, yyyy',
				columnFormatMonth: 'ddd',
				columnFormatWeek: 'ddd M/d',
				columnFormatDay: 'dddd M/d',
				timeFormat: 'H:mm - {H:mm}',
				
				weekMode: 'fixed', // 'fixed', 'liquid', 'variable'
				
				allDaySlot: true, // ture, false
				axisFormat: 'h(:mm)tt',
				
				slotMinutes: 30,
				minTime: 0,
				maxTime: 24,
				
				slotEventOverlap: true,
								
				savedRedirect: 'index.php',
				removedRedirect: 'index.php',
				updatedRedirect: 'index.php',
				
				ajaxLoaderMarkup: '<div class="loadingDiv"></div>',
				prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
				next: "<span class='fc-text-arrow'>&rsaquo;</span>",
				prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
				nextYear: "<span class='fc-text-arrow'>&raquo;</span>",  
            }

			var options =  $.extend(defaults, options);
			
			var opt = options;
									
			if(opt.gcal == true) { opt.weekType = ''; opt.dayType = ''; }
			
			// fullCalendar
			$(opt.calendarSelector).fullCalendar
			({
				
				defaultView: opt.defaultView,
				aspectRatio: opt.aspectRatio,
				weekends: opt.weekends,
				weekNumbers: opt.weekNumbers,
				weekNumberCalculation: opt.weekNumberCalculation,
				weekNumberTitle: opt.weekNumberTitle,
				titleFormat: {
					month: opt.titleFormatMonth,
					week: opt.titleFormatWeek,
					day: opt.titleFormatDay
				},
				columnFormat: {
					month: opt.columnFormatMonth,
					week: opt.columnFormatWeek,
					day: opt.columnFormatDay
				},
				isRTL: opt.isRTL,
				hiddenDays: opt.hiddenDays,
				theme: opt.theme,
				buttonIcons: {
					prev: opt.themePrev,
					next: opt.themeNext
				},
				weekMode: opt.weekMode,
				allDaySlot: opt.allDaySlot,
				allDayText: opt.allDayText,
				axisFormat: opt.axisFormat,
				slotMinutes: opt.slotMinutes,
				minTime: opt.minTime,
				maxTime: opt.maxTime,
				slotEventOverlap: opt.slotEventOverlap,
				
				timeFormat: opt.timeFormat,
				header: 
				{
						left: 'prev,next',
						center: 'title',
						right: 'month,'+opt.weekType+','+opt.dayType	
				},
				monthNames: opt.monthNames,
				monthNamesShort: opt.monthNamesShort,
				dayNames: opt.dayNames,
				dayNamesShort: opt.dayNamesShort,
				buttonText: {
					prev: opt.prev,
					next: opt.next,
					prevYear: opt.prevYear,
					nextYear: opt.nextYear, 
					today: opt.today,
					month: opt.month,
					week: opt.week,
					day: opt.day
				},
				editable: opt.editable,
				disableDragging: opt.disableDragging,
				disableResizing: opt.disableResizing,
				ignoreTimezone: opt.ignoreTimezone,
				firstDay: opt.firstDay,
				lazyFetching: opt.lazyFetching,
				selectable: opt.quickSave,
				selectHelper: opt.quickSave,
				select: function(start, end, allDay) 
				{
					if(opt.version == 'modal')
					{
						calendar.quickModal(start, end, allDay);
						$(opt.calendarSelector).fullCalendar('unselect');
					}
				},
				eventSources: [{url: opt.ajaxJsonFetch ,  allDayDefault: false}],
				eventDrop: 
					function(event) 
					{ 
						var ed_startDate = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
						var ed_startTime = $.fullCalendar.formatDate(event.start, 'HH:mm');
						var ed_endDate = $.fullCalendar.formatDate(event.end, 'yyyy-MM-dd');
						var ed_endTime = $.fullCalendar.formatDate(event.end, 'HH:mm');
						
						event.start = ed_startDate+' '+ed_startTime;
						if(event.end === null || event.end === 'null')
						{
							event.end = ed_startDate+' '+ed_startTime;	
						} else {
							event.end = ed_endDate+' '+ed_endTime;	
						}
						
						$.post(opt.ajaxUiUpdate, event, function(response) {
							console.log(response);
							if(response == 'Exitoso') 
							{
								alert("Actualizacion Exitosa");
								$(opt.calendarSelector).fullCalendar('refetchEvents');
							} else {
								alert("Error Tiene Citas Asignadas Para Esa Fecha");
								$(opt.calendarSelector).fullCalendar('refetchEvents');	
							}
							
						});
					},
				eventResize:
					function(event) 
					{ 
						var er_startDate = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
						var er_startTime = $.fullCalendar.formatDate(event.start, 'HH:mm');
						var er_endDate = $.fullCalendar.formatDate(event.end, 'yyyy-MM-dd');
						var er_endTime = $.fullCalendar.formatDate(event.end, 'HH:mm');
						
						event.start = er_startDate+' '+er_startTime;
						if(event.end === null || event.end === 'null')
						{
							event.end = er_startDate+' '+er_startTime;	
						} else {
							event.end = er_endDate+' '+er_endTime;	
						}
						
						$.post(opt.ajaxUiUpdate, event, function(response){
							$(opt.calendarSelector).fullCalendar('refetchEvents');
						});
					},
				eventRender: 
					function(event, element) 
					{	
						var d_color = event.color;	
						var d_startDate = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
						var d_startTime = $.fullCalendar.formatDate(event.start, 'HH:mm');
						var d_endDate = $.fullCalendar.formatDate(event.end, 'yyyy-MM-dd');
						var d_endTime = $.fullCalendar.formatDate(event.end, 'HH:mm');
						
						if(opt.version == 'modal')
						{	
							// Open action  (modalView Mode)
							element.attr('data-toggle', 'modal');
							element.attr('href', 'javascript:void(0);');
							element.attr('onclick', 'calendar.openModal("' + event.title + '","' + event.url + '","' + event.original_id + '","' + event.id + '","' + event.start + '","' + event.end + '","' + d_color + '","' + d_startDate + '","' + d_startTime + '","' + d_endDate + '","' + d_endTime + '");');  
						} 
					}	
				}); //fullCalendar
				
				 // Function to Open Modal
				calendar.openModal = function(title, url, id, rep_id, eStart, eEnd, color, startDate, startTime, endDate, endTime)
				{
					 $(".modal-body").html(opt.ajaxLoaderMarkup); // clear data
					 
					 // Setup variables
					 calendar.title = title;
					 calendar.url = url;
					 calendar.id = id;
					 calendar.rep_id = rep_id;
					 
					 calendar.eventStart = eStart;
					 calendar.eventEnd = eEnd;
					  
					 calendar.color = color;	
					 calendar.startDate = startDate;
					 calendar.startTime = startTime;
					 calendar.endDate = endDate;
					 calendar.endTime = endTime;
					  					  
					  var dataString = 'id='+calendar.id;
					  
					  $.ajax({
						type: "POST",
						url: opt.ajaxRetrieveDescription,
						data: dataString,
						cache: false,
						beforeSend: function() { $('.loadingDiv').show(); $('.modal-footer').hide() },
						error: function() { $('.loadingDiv').hide(); alert(opt.ajaxError) },
						success: function(description) 
						{
							$('.loadingDiv').hide(); 
							$('.modal-footer').show();
							if(calendar.url === 'undefined' || calendar.url === undefined) 
					 		{
					  			$(".modal-body").html(opt.ajaxLoaderMarkup+description); 
					  		} else {
					  			$(".modal-body").html(opt.ajaxLoaderMarkup+description+'<br /><br />'+opt.visitUrl+' <a href="'+calendar.url+'">'+calendar.url+'</a>'); 	  
					  		}
							
							// Delete button
							$(".modal-footer").delegate('[data-option="remove"]', 'click', function(e) 
							{
								//validacion de eliminacion
								var r = confirm("Estás seguro que deseas eliminar el registro?");
								if (r == true) {
									calendar.remove(calendar.id);	
								    e.preventDefault();
								    txt = "Registro Eliminado!";
								} else {
								    
								}

							 });
							 
							 // Export button
							$(".modal-footer").delegate('[data-option="export"]', 'click', function(e) 
							{
								calendar.exportIcal(calendar.id, calendar.title, calendar.description, calendar.eventStart, calendar.eventEnd, calendar.url);	
								e.preventDefault();
							 });
						}
					  });
					  									
					  $(".modal-header").html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+'<h4>'+calendar.title+'</h4>'); 
					  
					  $(opt.modalViewSelector).modal('show');
						
					  	// Edit Button
						$(".modal-footer").delegate('[data-option="edit"]', 'click', function(e) 
						{
							$(opt.modalViewSelector).modal('hide');
							$(".modal-body").html(opt.ajaxLoaderMarkup); // clear data
							
							var dataString2 = 'id='+calendar.id+'&mode=edit';
							
							$.ajax({
								type: "POST",
								url: opt.ajaxRetrieveDescription,
								data: dataString2,
								cache: false,
								beforeSend: function() { $('.loadingDiv').show(); $('.modal-footer').hide() },
								error: function() { $('.loadingDiv').hide(); alert(opt.ajaxError) },
								success: function(description2) 
								{
									$('.loadingDiv').hide(); 
									$('.modal-footer').show()
									if(calendar.url === 'undefined') 
									{
										$(".modal-header").html(

											'<div class="col-md-12"></br><h4 class="pull-right"><font color="#3ca3c1" style="font-weight:bold">HCO-Modulo de Citas / Editar</font></h4>'+
											'</br></br></div>'
										);
										$(".modal-body").html(
											'<div class="col-md-5" id="contenedorimagen"><center><img alt="Logo" src="images/logohco.png"  height="200%" width="100%"/></center></div><div class="col-md-3"><font color="#3ca3c1" >Tipo de Consulta</font></br></br><font color="#3ca3c1" >Descripcion</font></br></br></br><font color="#3ca3c1" >Color Evento</font></br><font color="#3ca3c1" >Fecha Inicial</font></br></br><font color="#3ca3c1" >Fecha Final</font></br><font color="#3ca3c1" >Hora Inicial</font></br></br><font color="#3ca3c1" >Hora Final</font></div><div class="col-md-4">'+
											'<form id="event_title_e">' +
												'<input style="border-color:#3ca3c1" type="text" class="form-control" name="title_update" readonly value="'+calendar.title+'">' +
												'<input style="border-color:#3ca3c1" type="hidden" class="form-control" name="id_update" value="'+calendar.id+'">' +
											'</form>'+
											'<form id="event_description_e">' +
												'<textarea style="border-color:#3ca3c1" class="form-control" readonly name="description_update"></textarea>' +
												'<input style="border-color:#3ca3c1" type="text" class="form-control" id="color_update_picker" name="color_update" readonly value="'+color+'">' +
													
													'<input style="border-color:#3ca3c1" type="text" class="form-control" id="date_datepicker" name="update_start_date" required value="'+startDate+'">' +
												
										
													'<input style="border-color:#3ca3c1" type="text" class="form-control input-sm" id="date_datepicker_second" name="update_end_date" required value="'+endDate+'">' +
												

													
													'<input style="border-color:#3ca3c1" type="text" class="form-control input-sm" name="update_start_time" id="time_update_picker" required placeholder="HH:MM:SS" value="'+startTime+'">' +
												
												'<div class="clearfix"></div>' +
												
													
													
													'<input style="border-color:#3ca3c1" width="150px" type="text" class="form-control input-sm" name="update_end_time" id="time_update_picker_second" required placeholder="HH:MM:SS" value="'+endTime+'">' +
												
											'</form></div>'
										);
									} else {
											
									}
									
									 // Pickers
									 $('input#color_update_picker').colorpicker(opt.colorpickerArgs);	
									 
									 $('input#date_datepicker').datepicker({
										dateFormat: 'yy-mm-dd',
										minDate: new Date(),
										onSelect: function(dateText, obj) { $('input#date_datepicker').val(dateText); }
									 });
									 
									 $('input#date_datepicker_second').datepicker({
										dateFormat: 'yy-mm-dd',
										minDate: new Date(),
										onSelect: function(dateText, obj) { $('input#date_datepicker_second').val(dateText); }
									 });
									 
										 $(document).on('click','a.ui-datepicker-next',function() {
											 $('input#date_datepicker').datepicker('setDate', 'c+1w');
											 $('input#date_datepicker_second').datepicker('setDate', 'c+1w');
										 });
										 
										  $(document).on('click','a.ui-datepicker-prev', function(){
											 $('input#date_datepicker').datepicker('setDate', 'c-1w');
											 $('input#date_datepicker_second').datepicker('setDate', 'c-1w');
										 });
									 
									 $('input#time_update_picker').timepicker();
									 $('input#time_update_picker_second').timepicker();
								}
					  		});
							
							$(opt.modalEditSelector).modal('show'); 
							
							  // On Modal Hidden
							 $(opt.modalEditSelector).on('hidden', function() {
								 $('.modal-body').html(''); // clear data
								// $(opt.calendarSelector).fullCalendar('refetchEvents'); (by uncommenting this fixes multiply loads bug)
							 })
							 
							 // Close Button - This is due cache to prevent data being saved on another view
							 $(".modal-footer").delegate('[data-dismiss="modal"]', 'click', function(e) 
							 {
								 $('.modal-body').html(''); // clear data
								 // $(opt.calendarSelector).fullCalendar('refetchEvents'); (by uncommenting this fixes multiply loads bug)
								 e.preventDefault();
							 });
						 	 
							// After all step above save
							// Update button
							$(".modal-footer").off('click').delegate('[data-option="save"]', 'click', function(e) 
							{	
								var event_title_e = $("form#event_title_e").serializeArray(); 
								var event_description_e = $("form#event_description_e").serializeArray();
								var event_url = $("form#event_description_e").serializeArray();
																		
								calendar.update(event_title_e[1], event_title_e[0], event_description_e);
								
								e.preventDefault();
							});
							 
							e.preventDefault();
						});
						
				} // openModal

				// Function to quickModal
				calendar.quickModal = function(start, end, allDay)
				{

					calendar.start = start;
					calendar.end = end;
					//recibo los objetos en este caso recibo las fechas del calendario
					var start_factor = $.fullCalendar.formatDate(start, 'yyyy-MM-dd');
					var startTime_factor = $.fullCalendar.formatDate(start, 'HH:mm');
					var end_factor = $.fullCalendar.formatDate(end, 'yyyy-MM-dd');
					var endTime_factor = $.fullCalendar.formatDate(end, 'HH:mm');
					//obtener fecha actual formato año-mes-dia
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();
					if(dd<10){
					    dd='0'+dd
					} 
					if(mm<10){
					    mm='0'+mm
					} 
					var today = yyyy+'-'+mm+'-'+dd;
					//validacion fecha
					   if (start_factor >= today) {
						    //redireccion a creacion de cita
					        window.location.href="http://190.60.211.17/hco/add_event.php?fecha="+ start_factor +"";
					   } else{
						    alert("La fecha no puede ser menor que la actual para crear la cita");
					   }



				} // end quickModal
					
				// Function quickSave 
				calendar.quickSave = function(event_title, event_description, start, end, allDay)
				{
					var start_factor = $.fullCalendar.formatDate(start, 'yyyy-MM-dd');
					var startTime_factor = $.fullCalendar.formatDate(start, 'HH:mm');
					var end_factor = $.fullCalendar.formatDate(end, 'yyyy-MM-dd');
					var endTime_factor = $.fullCalendar.formatDate(end, 'HH:mm');

					if(opt.quickSaveCategory !== '')
					{
						var constructor = 'title='+event_title.value+'&description='+event_description.value+'&start_date='+start_factor+'&start_time='+startTime_factor+'&end_date='+end_factor+'&end_time='+endTime_factor+'&url=false&color='+opt.defaultColor+'&allDay='+allDay+'&categorie='+calendar.category;
					} else {
						var constructor = 'title='+event_title.value+'&description='+event_description.value+'&start_date='+start_factor+'&start_time='+startTime_factor+'&end_date='+end_factor+'&end_time='+endTime_factor+'&url=false&color='+opt.defaultColor+'&allDay='+allDay;
					}
					
					$.post(opt.ajaxEventQuickSave, constructor, function(response) 
					{	
						if(response == 1) 
						{
							$(opt.modalQuickSaveSelector).modal('hide');
							$(opt.calendarSelector).fullCalendar('refetchEvents');
						} else {
							alert(opt.failureAddEventMessage);	
						}
					});	
					// e.preventDefault(); prevented duplication
				} // end quickSave
					   
				// Function to Save Data to the Database 
				calendar.save = function()
				{
					$(opt.formAddEventSelector).on('submit', function(e)
					{
						
						$.ajax({
						type: "POST",
						url: opt.ajaxEventSave,
						data: $(this).serialize(),
						cache: false,
						success: function(response) {
							console.log(response);
							if(response == 'Exitoso') 
							{
							    alert("Insercion Exitosa");
								document.location.href = "citas.php";
							} 

							if(response == 'Error') {
								alert("Error Tiene Citas Asignadas Para Esa Fecha");	
							}
						},
						error: function(response) {
							alert("Error Insercion Fallida");	
						}
					});	


						
					}); 
				};
					
				// Function to Remove Event ID from the Database
				calendar.remove = function(id)
				{
					var construct = "id="+id;

					// First check if the event is a repetitive event
					$.ajax({
						type: "POST",
						url: opt.ajaxRepeatCheck,
						data: construct,
						cache: false,
						success: function(response) {
							if(response == 'REP_FOUND') 
							{
								// prompt user
								$(opt.modalViewSelector).modal('hide');
								
								if(opt.version == 'modal')
								{
									$(opt.modalPromptSelector+" .modal-header").html('<h4>'+calendar.title+'</h4>');
									$(opt.modalPromptSelector+" .modal-body").html(opt.repetitiveEventActionText);	
								} else {
									$(opt.modalPromptSelector+" .modal-header").html('<h4>'+opt.eventText+'</h4>');
									$(opt.modalPromptSelector+" .modal-body").html(opt.repetitiveEventActionText);		
								}
								
								$(opt.modalPromptSelector).modal('show');
								
								// Action - remove this
								$(".modal-footer").delegate('[data-option="remove-this"]', 'click', function(e) 
								{
									calendar.remove_this(construct);
									$(opt.modalPromptSelector).modal('hide');
									e.preventDefault();
								 });
								
								// Action - remove repetitive
								$(".modal-footer").delegate('[data-option="remove-repetitives"]', 'click', function(e) 
								{
									if(opt.version == 'modal')
									{
										var construct = "id="+id+'&rep_id='+calendar.rep_id+'&method=repetitive_event';
									} else {
										var construct = "id="+id+'&rep_id='+$("input#rep_id").val()+'&method=repetitive_event';
									}
									
									calendar.remove_this(construct);
									$(opt.modalPromptSelector).modal('hide');
									e.preventDefault();
								 });
								
							} else {
								calendar.remove_this(construct);
							}
						},
						error: function(response) {
							alert(opt.generalFailureMessage);	
						}
					});	
				};
				
				// Functo to Remove Event from the database
				calendar.remove_this = function(construct)
				{
					// just remove this	
					$.post(opt.ajaxEventDelete, construct, function(response) 
					{
						if(response == '') 
						{
							if(opt.version == 'modal')
							{
								$(opt.modalViewSelector).modal('hide');
								$(opt.calendarSelector).fullCalendar('refetchEvents');	
							} else {
								document.location.reload();		
							}
						} else {
							alert(opt.failureDeleteEventMessage);
						}
					});			
				}
					
				// Function to Update Event to the Database
				calendar.update = function(id, title, description)
				{
					calendar.update_this(id, title, description);
							
				}
				
				// Function to update single and repetitive events
				calendar.update_this = function(id, title, description)
				{

					
					
					var construct = "id="+id.value+"&title="+title.value+"&start_date="+description[8].value+"&start_time="+description[10].value+"&end_date="+description[3].value+"&end_time="+description[11].value+"&titles="+title.value;	
						
										
					$.ajax({
						type: "GET",
						url: opt.ajaxEventEdit,
						data: construct,
						cache: false,
						success: function(response) {
							console.log(response);
							if(response == 'Exitoso') 
							{
								alert("Actualizacion Exitosa");
								document.location.reload();
							} else {
								alert("Error Tiene Citas Asignadas Para Esa Fecha");	
							}
						},
						error: function(response) {
							alert("Error Actializacion Fallida");	
						}
					});	
				}
				
				// Function to Export Calendar
				calendar.exportIcal = function(expID, expTitle, expDescription, expStart, expEnd, expUrl)
				{ 
					var start_factor = $.fullCalendar.formatDate($.fullCalendar.parseDate(expStart), 'yyyy-MM-dd HH:mm:ss');
					var end_factor = $.fullCalendar.formatDate($.fullCalendar.parseDate(expEnd), 'yyyy-MM-dd HH:mm:ss');
					
					var construct = 'method=export&id='+expID+'&title='+expTitle+'&description='+expDescription+'&start_date='+start_factor+'&end_date='+end_factor+'&url='+expUrl;	

					$.post(opt.ajaxEventExport, construct, function(response) 
					{
						
						$(opt.modalViewSelector).modal('hide');
						window.location = 'includes/Event-'+expID+'.ics';
						var construct2 = 'id='+expID;
						$.post(opt.ajaxEventExport, construct2, function() {});
					});
				}
			
			// Commons - modal + phpversion
			// Fiter
			if(opt.filter == true)
			{
				$(opt.formFilterSelector).on('change', function(e) 
				{
					 selected_value = $(this).val();
					 
					 construct = 'filter='+selected_value;
					 
					 $.post('includes/loader.php', construct, function(response) 
					{
						$(opt.calendarSelector).fullCalendar('refetchEvents');
					});	
					 
					 e.preventDefault();  
				});
				
			// Search Form
			// keypress
			$(opt.formSearchSelector).keypress(function(e) 
			{
				if(e.which == 13)
				{
					search_me();
					e.preventDefault();
				}
			});
			
			// submit button
			$(opt.formSearchSelector+' button').on('click', function(e) 
			{
				search_me();
			});
			
			function search_me()
			{
				 value = $(opt.formSearchSelector+' input').val();
				 
				 construct = 'search='+value;
				 
				 $.post('includes/loader.php', construct, function(response) 
				{
					$(opt.calendarSelector).fullCalendar('refetchEvents');
				});		
			}
				
			}
					   
		} // FullCalendar Ext
		
	}); // fn
	 
})(jQuery);

// define object at end of plugin to fix ie bug
var calendar = {};


