﻿<?php
//conexion
require_once 'db_connect.php';
// connecting to db
$db = new DB_CONNECT();
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>Nuevo Cliente</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="css/fullcalendar.css" rel="stylesheet">
    <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
    <link href="lib/validation/css/validation.css" rel="stylesheet">
    <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="css/base.css" rel="stylesheet">
    <link href="tools/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src = "http://www.google.com/jsapi" charset="utf-8"></script>
    <script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    <style type="text/css">
.form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fbfbfb;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    margin-bottom: 12px;
}

.panel-info>.panel-heading {
    color: #a5a5a5;
    background-color: #0288d1;
    border-color: #a5a5a5;
}

.panel-info {
    border-color: #a5a5a5;
    background-color: #fbfbfb;
}

.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
    width: 100%;
}

span.grey {
  background: #0288d1;
  border-radius: 0.8em;
  -moz-border-radius: 0.8em;
  -webkit-border-radius: 0.8em;
  color: #fff;
  display: inline-block;
  font-weight: bold;
  line-height: 1em;
  margin-right: 15px;
  text-align: center;
  width: 1em; 
}

h1 {
    margin-top: -5px;
    margin-bottom: 10px;
}

#footer {
position:absolute;
width:100%;
height:auto;
clear:both;
}




input[type=radio], input[type=checkbox] {
  width: 17px;
  height: 17px;
}


    </style>

<script type="text/javascript">

   function valida_envia(){ 


   	if (document.fvalida.nombres.value.length==0){ 
        alert("Por favor Ingrese el nombre") 
        document.fvalida.nombres.focus() 
        return false; 
    }
    if (document.fvalida.apellidos.value.length==0){ 
        alert("Por favor Ingrese el apellido") 
        document.fvalida.apellidos.focus() 
        return false; 
    }

    if (document.fvalida.numerodocumento.value.length==0){ 
        alert("Por favor Ingrese el numero de documento") 
        document.fvalida.numerodocumento.focus() 
        return false; 
    }


    if (document.fvalida.correo.value.length==0){ 
        alert("Por favor Ingrese el correo electronico") 
        document.fvalida.correo.focus() 
        return false; 
    }
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (!emailRegex.test(document.fvalida.correo.value)) {
        alert("Error correo incorrecto") 
        document.fvalida.correo.focus()  	
      return false;
    }

    if (document.fvalida.clinica.value.length==0){ 
        alert("Por favor Ingrese la clinica") 
        document.fvalida.clinica.focus() 
        return false; 
    }
    if (document.fvalida.contrasena.value.length==0){ 
        alert("Por favor Ingrese la contraseña") 
        document.fvalida.contrasena.focus() 
        return false; 
    }
    if (document.fvalida.contrasena2.value.length==0){ 
        alert("Por favor Ingrese la contraseña") 
        document.fvalida.contrasena2.focus() 
        return false; 
    }

    
    if(document.fvalida.contrasena.value != "" && document.fvalida.contrasena.value == document.fvalida.contrasena2.value) {
      
      if(document.fvalida.contrasena.value.length < 6) {
        alert("Error: La contraseña tiene menos de 6 caracteres!");
        document.fvalida.contrasena.focus();
        return false;
      }
      if(document.fvalida.contrasena2.value.length < 6) {
        alert("Error: La contraseña tiene menos de 6 caracteres!");
        document.fvalida.contrasena2.focus();
        return false;
      }

      if(document.fvalida.contrasena.value == document.fvalida.nombres.value) {
        alert("Error: La contraseña debe ser diferente que el nombre!");
        document.fvalida.contrasena.focus();
        return false;
      }
      if(document.fvalida.contrasena.value == document.fvalida.apellidos.value) {
        alert("Error: La contraseña debe ser diferente que el apellido!");
        document.fvalida.contrasena.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(document.fvalida.contrasena.value)) {
        alert("Error: La contraseña debe contener un numero (0-9)!");
        document.fvalida.contrasena.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(document.fvalida.contrasena.value)) {
        alert("Error: La contraseña debe contener una letra minuscula (a-z)!");
        document.fvalida.contrasena.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(document.fvalida.contrasena.value)) {
        alert("Error: La contraseña debe contener una letra mayuscula (A-Z)!");
        document.fvalida.contrasena.focus();
        return false;
      }

      fvalida.submit();


    } else {
      alert("Error: Las contraseñas deben ser iguales!");
      document.fvalida.contrasena.focus();
      return false;
    }

    
  }

</script>
<script type="text/javascript">
  function mostrarsociedades(){
    $("#sociedades").css("display", "block");
    $("#universidades").css("display", "none");

  }
</script>

<script type="text/javascript">
  function mostraruniversidades(){
    $("#universidades").css("display", "block");
    $("#sociedades").css("display", "none");

  }
</script>

</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
			    <div class="col-md-1">
			    </div>
				<div class="col-md-4">
				<a href="http://www.hco.com.co"><img id="logosuperior" style="margin-top: -20px;" alt="Bootstrap Image Preview" src="images/logohco.png" height="140px" width="77%"></a>
                </br>
                </br>
                </br> 

               	<div class="panel panel-info">
                    <div class="panel-heading" style="background-color: #fbfbfb;">
                        <div class="panel-title"><font style="color: #a5a5a5;font-family:Calibri;"><span class="glyphicon glyphicon-info-sign"></span> Reg&iacute;strate, es muy sencillo</font></div>
                    </div>     

                    <div style="padding-top:30px; font-size: 1.4em;" class="panel-body">

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <div class="col-md-2">
                        <h1><span class="grey">1</span></h1>
                        </div>
                        <div class="col-md-10" style="background-color: white">
                        <font style="color: #0288d1;font-family:Calibri;"><b>DILIGENCIA EL FORMULARIO</b></font>
                        </br>
                        <font style="color: #a5a5a5;font-family:Calibri;">Registra tus datos y pulsa el bot&oacute;n "Iniciar Prueba Gratuita HCO"</font>
                        </div>
                         <div class="col-md-12">
                         <hr>
                        </div>
                       
                       	<div class="col-md-2">
                        <h1><span class="grey">2</span></h1>
                        </div>
                        <div class="col-md-10" style="background-color: white">
                        <font style="color: #0288d1;font-family:Calibri;"><b>CONFIRMA TU USUARIO</b></font>
                        </br>
                        <font style="color: #a5a5a5;font-family:Calibri;">Al correo electr&oacute;nico que registraste, te llegará un mensaje de confirmación del usuario que creaste</font>
                        </div>
                       <div class="col-md-12">
                         <hr>
                        </div>
                       	<div class="col-md-2">
                        <h1><span class="grey">3</span></h1>
                        </div>
                        <div class="col-md-10" style="background-color: white">
                        <font style="color: #0288d1;font-family:Calibri;"><b>INICIA TU PRUEBA GRATUITA</b></font>
                        </br>
                        <font style="color: #a5a5a5;font-family:Calibri;">Disfruta de nuestra aplicaci&oacute;n HCO y sus servicios de manera GRATUITA por un periodo de 60 d&iacute;as</font>
                        </div>

                        </div>                     
                    </div>	



				</div>
				<div class="col-md-1">
			    </div>
				<div class="col-md-5">

				<div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><font style="color: white;font-family:Calibri;"><b>PRU&Eacute;BALO AHORA GRATIS</b></font></div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body">

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form  name="fvalida" class="form-horizontal" role="form" action="GuardarNuevoCliente.php" method="POST">
                                    
                            <div class="col-md-12">
                            <input id="nombres" type="text" class="form-control" name="nombres" value="" placeholder="Nombre" required>    
                            </div>
                            </br>
                            <div class="col-md-12">
                            <input id="apellidos" type="text" class="form-control" name="apellidos" placeholder="Apellidos" required>
                            </div>

                            <div class="col-md-12">
                             <label style="color: #a5a5a5;font-family:Calibri;">
                                  Tipo de documento
                              </label>
                              <select id="tipodedocumento" name="tipodedocumento" class="form-control" required>
                                <option value="Cedula de ciudadanía">Cédula de ciudadanía</option>
                                <option value="Registro civil">Registro civil</option>
                                <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                                <option value="Cedula de extranjeria">Cédula de extranjería</option>
                                <option value="NIT">NIT</option>
                                <option value="Adulto sin identificación">Adulto sin identificación</option>
                                <option value="Menor sin identificación">Menor sin identificación</option>
                                <option value="Pasaporte">Pasaporte</option>
                              </select> 
                          </div>

                                    <div class="col-md-12">
                              <label style="color: #a5a5a5;font-family:Calibri;">
                                  Numero de documento
                              </label>
                              <input type="text" class="form-control" id="numerodocumento" name="numerodocumento" onclick="mostrar(this);" required />
                          </div>


                            <div class="col-md-12">
                            <input id="correo" type="email" class="form-control" name="correo" placeholder="Correo Electr&oacute;nico" required>
                            </div>
                            <div class="col-md-12">
                            <input id="clinica" type="text" class="form-control" name="clinica" placeholder="Nombre de la Cl&iacute;nica" required>
                            </div>
                            <div class="col-md-12">
                            <input id="contrasena" type="password" class="form-control" name="contrasena" placeholder="Contraseña" required>
                            </div> 
                            <div class="col-md-12">
                            <input id="contrasena2" type="password" class="form-control" name="contrasena2" placeholder="Confirma la Contraseña" required>
                            </div>

                            <div class="col-md-12">
                            <label style="color: #0288d1;font-family:Calibri;font-size="1em">La Contraseña debe tener minimo 6 caracteres y incluir al menos una letra min&uacute;scula, una may&uacute;scula y un n&uacute;mero</font></label>
                            </div>

                            <div class="col-md-12">
                            <label style="color: #a5a5a5;font-family:Calibri;"> Tipo de Usuario : </label>
                            </br>
                            <div class="col-md-3">
                            <label style="color: #a5a5a5;font-family:Calibri;">Profesional</label>
                            </div>
                            <div class="col-md-3">
                            <input id="tipoprofesional" type="radio" class="form-control" name="tipoprofesional" onclick="mostrarsociedades();" value="Profesional" >
                            </div>
                            <div class="col-md-3">
                            <label style="color: #a5a5a5;font-family:Calibri;">Estudiante</label>
                            </div>
                            <div class="col-md-3">
                            <input id="tipoprofesional" type="radio" class="form-control" name="tipoprofesional" onclick="mostraruniversidades();" value="Estudiante" >
                            </div>
                            </div>

                            <div id="sociedades" style="display:none;">
                            </br>
                            <div class="col-md-12">
                            <label style="color: #a5a5a5;font-family:Calibri;">Sociedades</label>
                            </br>
                              <select  class="form-control" id="sociedades" name="sociedades">
                              <option value="">Seleccione</option>
                            <?php
                            //consulta de tipo
                            mysql_query("SET NAMES 'UTF8'");
                            $result1 = mysql_query("SELECT * FROM sociedades order by descripcion asc  ");
                             while($row = mysql_fetch_array($result1)){
                             $descripcion = $row["descripcion"]; 
                            ?>
                              <option value="<?php echo $descripcion; ?>" ><?php echo $descripcion; ?></option>
                            <?php
                              } 
                             ?>
                            </select>
                            </div>
                            </div>

                            <div id="universidades" style="display:none;">
                            </br>
                            <div class="col-md-12">
                            <label style="color: #a5a5a5;font-family:Calibri;">Universidades</label>
                            </br>
                              <select  class="form-control" id="universidades" name="universidades">
                              <option value="">Seleccione</option>
                            <?php
                            //consulta de tipo
                            mysql_query("SET NAMES 'UTF8'");
                            $result1 = mysql_query("SELECT * FROM universidades order by descripcion asc  ");
                             while($row = mysql_fetch_array($result1)){
                             $descripcion = $row["descripcion"]; 
                            ?>
                              <option value="<?php echo $descripcion; ?>" ><?php echo $descripcion; ?></option>
                            <?php
                              } 
                             ?>
                            </select>
                            </div>
                            </div> 

                            <div class="col-md-12">
                            <span style="color: #a5a5a5;font-family:Calibri;">Especialidades</span>
                            <select class="form-control" id="especialidades" name="especialidades" required>
                              <option value="Seleccione">Seleccione</option>
    		                      <option value="Cirugia Oral y Maxilofacial">Cirugía Oral y Maxilofacial</option>
    		                      <option value="Endodoncia">Endodoncia</option>
    		                      <option value="Estomatologia">Estomatología</option>
    		                      <option value="Implantologia Oral">Implantologia Oral</option>
    		                      <option value="Odontologia">Odontología</option>
    		                      <option value="Odontopediatria">Odontopediatría</option>
    		                      <option value="Operatoria Dental Estetica">Operatoria Dental Estética</option>
    		                      <option value="Ortodoncia">Ortodoncia</option>
    		                      <option value="Ortodoncia y Ortopedia Maxilar">Ortodoncia y Ortopedia Maxilar</option>
    		                      <option value="Periodoncia">Periodoncia</option>
    		                      <option value="Prostodoncia">Prostodoncia</option>
    		                      <option value="Rehabilitacion Oral">Rehabilitación Oral</option>
    		                      <option value="Otro">Otro</option>
		                    </select> 
                            </div>
                        
                              <div class="col-md-12">
                              </br>  
                              <button type="button" onclick="valida_envia();" class="btn btn-success"><b>INICIAR PRUEBA GRATUITA HCO</b></button>
                              </div>  


                                    
                            </form>     
                        </div>                     
                    </div>	
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
				</div>
				<div class="col-md-1">
          </br>
          </br>
          </br>
			    </div>
			</div>
		</div>
	</div>
	

<div id="footer" class="row" style="background-color: #0288d1;margin-top: 15px;">
    <div class="col-md-12">
    </br>
    <center><b><font style="color: white;font-family:Calibri;">HCO® Términos y condiciones del registro de Uso y Prueba Gratuita por 60 días
Nuestra oferta de prueba gratuita por 60 días, que le ofrece a usted HCO, le otorga el derecho de acceder al Servicio básico de HCO, durante un periodo de 60 días, contados desde el momento que usted active dicho periodo de prueba mediante el envío de sus datos de registro. Al suministrar sus datos de registro, usted acepta la Oferta de Prueba Gratuita por 60 días, reconoce y acepta los Términos y Condiciones de Uso HCO. Si usted decide, una vez terminado el plazo del Periodo de Prueba Gratuito, convertirse en un usuario de pago de cualquiera de nuestros productos HCO puede solicitarlo vía e-mail a la dirección: contacto@hco.com.co o entrando en contacto telefónico con uno de nuestros asesores a través de la línea: (571)2189575 Bogotá, Colombia. Tenga en cuenta que los datos que informe en esta cuenta de demostración serán eliminados una vez finalizado el periodo de prueba de 60 días. Usted solo podrá utilizar una UNICA vez esta Oferta de Prueba Gratuita. HCO se reserva el derecho, a su absoluta discreción, de retirar o modificar esta Oferta de Prueba Gratuita y/o los Términos y Condiciones del uso de nuestra aplicación HCO Prueba Gratuita de 60 días en cualquier momento, sin previo aviso y sin responsabilidad alguna.
</font></center>
    </br>
    </div>
</div>


</div>





</body>
</html>