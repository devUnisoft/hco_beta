<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Mensaje Insersion</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
	</script>
	<style type="text/css">
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}

		header {
    width: 100%;
    height: 150px;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
	</style>
	<script type="text/javascript">
		function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
         window.onload = init();
	</script>	
</head>
<body>
<header>
    <div class="container clearfix">
        </br>
        <center><img src="images/logo_blanco.png"  height="15%" width="15%"></center>
    </div>
</header>
<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6">
         <?php 
         //error_reporting(0);
         //inicio de session
         session_start();
         //valores por post de materiales.php
             $razonsocial = $_REQUEST["razonsocial"];
             $nit = $_REQUEST["nit"];
             $direccion = $_REQUEST["direccion"];
             $telefono = $_REQUEST["telefonos"];
             $email = $_REQUEST["email"];
             $personacontacto = $_REQUEST["personacontacto"];
             $emailusuario = $_REQUEST["emailusuario"];
             $codigoentidad = $_REQUEST["codigoentidad"];
             //$codigodelprestador = $_REQUEST["codigodelprestador"];
             $codigoadministradora = $_REQUEST["codigoadministradora"];
             $nombreadministradora = $_REQUEST["nombreadministradora"];
             $numerofactura = $_REQUEST["numerofactura"];
             $numeroconsecutivo = $_REQUEST["numeroconsecutivo"];
             
             

             //conexion
             require_once 'db_connect.php';
            // connecting to db
            $db = new DB_CONNECT();

            //selecciono datos en busqueda 
            $resultselectr = mysql_query("SELECT * FROM clinicas where razonsocial='$razonsocial' ");
            $rows = mysql_num_rows($resultselectr);
            if ($rows > 0) {

            //directorio de subida de imagenes al servidor
         $dir_destino = './uploads/';
         $imagen_subida = $dir_destino .$razonsocial. basename($_FILES['foto']['name']);
         //validaciones
         if(!is_writable($dir_destino)){
            echo "no tiene permisos";
        }else{

            if(is_uploaded_file($_FILES['foto']['tmp_name'])){

            if (move_uploaded_file($_FILES['foto']['tmp_name'], $imagen_subida)) {

            // mysql inserting a new row
            $result = mysql_query("UPDATE clinicas SET direccion='$direccion',telefono='$telefono',personacontacto='$personacontacto',documento='$emailusuario',codigoentidad='$codigoentidad', codigoadministradora='$codigoadministradora', nombreadministradora='$nombreadministradora', numerofactura='$numerofactura', numeroconsecutivoevolucion='$numeroconsecutivo',imagen='$imagen_subida'  WHERE documento='".$emailusuario."' ");

            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Actualizacion Exitosa de Clinica";
                            echo "</div>";
                            echo "</div>";
                            header("refresh:3;url=menuadmin.php");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Actualizacion Fallida de Clinica";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            header( "refresh:3;url=menuadmin.php" );
             }
            //cerrando imagen
             }
            }
            }

            }else{    

            //selecciono datos en busqueda 
            $resultselect = mysql_query("SELECT * FROM clinicas where documento='$emailusuario' ");
            $rows = mysql_num_rows($resultselect);
            if ($rows > 0) {


            //directorio de subida de imagenes al servidor
         $dir_destino = './uploads/';
         $imagen_subida = $dir_destino .$razonsocial. basename($_FILES['foto']['name']);
         //validaciones
         if(!is_writable($dir_destino)){
            echo "no tiene permisos";
        }else{

            if(is_uploaded_file($_FILES['foto']['tmp_name'])){

            if (move_uploaded_file($_FILES['foto']['tmp_name'], $imagen_subida)) {

            // mysql inserting a new row
            $result = mysql_query("UPDATE clinicas SET razonsocial='$razonsocial',nit='$nit',direccion='$direccion',telefono='$telefono',email='$email',personacontacto='$personacontacto',documento='$emailusuario',codigoentidad='$codigoentidad', codigoadministradora='$codigoadministradora', nombreadministradora='$nombreadministradora', numerofactura='$numerofactura', numeroconsecutivoevolucion='$numeroconsecutivo',imagen='$imagen_subida'  WHERE documento='".$emailusuario."' ");

            $result1 = mysql_query("SELECT * FROM clinicas where documento='$emailusuario' ");
             while($row = mysql_fetch_array($result1)){
            $razonsocial = $row["razonsocial"];
            } 

            $result2 = mysql_query("UPDATE users  SET clinica='$razonsocial'  WHERE clinica='".$razonsocial."' ");

            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Clinica";
                            echo "</div>";
                            echo "</div>";
                            header("refresh:3;url=menuadmin.php");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Clinica";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            header( "refresh:3;url=menuadmin.php" );
            }
            //cerrando imagen
            }}}

                
            }else{

            
        //directorio de subida de imagenes al servidor
         $dir_destino = './uploads/';
         $imagen_subida = $dir_destino .$razonsocial. basename($_FILES['foto']['name']);
         //validaciones
         if(!is_writable($dir_destino)){
            echo "no tiene permisos";
        }else{


            if(is_uploaded_file($_FILES['foto']['tmp_name'])){

            if (move_uploaded_file($_FILES['foto']['tmp_name'], $imagen_subida)) {


            // mysql inserting a new row
            $result = mysql_query("INSERT INTO clinicas(razonsocial,nit,direccion,telefono,email,personacontacto,documento,codigoentidad,codigoadministradora,nombreadministradora,numerofactura,numeroconsecutivoevolucion,imagen) VALUES('".$razonsocial."', '".$nit."', '".$direccion."', '".$telefono."', '".$email."', '".$personacontacto."', '".$emailusuario."', '".$codigoentidad."',  '".$codigoadministradora."', '".$nombreadministradora."', '".$numerofactura."', '".$numeroconsecutivo."', '".$imagen_subida."')");
            // check if row inserted or not    
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Clinica";
                            echo "</div>";
                            echo "</div>";
                            header("refresh:3;url=menuadmin.php");
            }else{
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Clinica";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                            header( "refresh:3;url=menuadmin.php" );
            }


        }
       }
      }



        }
    }
            ?>
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
</body>
</html>