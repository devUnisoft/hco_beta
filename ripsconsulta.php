﻿<?php
error_reporting(0);
//conexion
$pdo = new PDO('mysql:host=localhost;dbname=hco', 'root', 'usc2017*');
//

//recibo fechas
$fechainicial =  $_REQUEST["f1"];
$fechafinal = $_REQUEST["f2"];
//convierto fecha
$fechainicial = date("d-m-Y", strtotime($fechainicial));
//agrego la hora
$fechainicial= $fechainicial." 01:00:00";
//convierto fecha
$fechafinal = date("d-m-Y", strtotime($fechafinal));
//agrego la hora
$fechafinal= $fechafinal." 24:00:00";

//query
$query = "SELECT * FROM pagos INNER JOIN clinicas ON pagos.clinica=clinicas.razonsocial  INNER JOIN presupuestos ON pagos.procedimiento=presupuestos.nombreprocedimiento INNER JOIN evolutions ON pagos.procedimiento=evolutions.procedimiento where  evolutions._date <= '$fechafinal' and _date  >= '$fechainicial'";

$result = $pdo->query($query);
//año actual
$fecha_actual=date("Y");
//fecha y hora
$fecha_hora=date("Y-m-d h:i:s");


$data = '';

while ($row = $result->fetch(PDO::FETCH_OBJ)) {

		//abrevio tipo documento
        if($row->tipodocumento == "Cedula de Ciudadania"){
            $tipo = "CC";
        }
        if($row->tipodocumento == "Cedula de Extranjeria"){
            $tipo = "CE";
        }
        if($row->tipodocumento == "Registro Civil"){
            $tipo = "RC";
        }
        if($row->tipodocumento == "Tarjeta de Identidad"){
            $tipo = "TI";
        }
        if($row->tipodocumento == "NIT"){
            $tipo = "NT";
        }
        if($row->tipodocumento == "Adulto sin identificacion"){
            $tipo = "AS";
        }
        if($row->tipodocumento == "Menor sin identificacion"){
            $tipo = "MS";
        }if($row->tipodocumento == "Pasaporte"){
            $tipo = "PA";
        }
        //validacion de  causas
        if($row->cause == "Accidente de trabajo"){
            $causa = "1";
        }
        if($row->cause == "Accidente de tránsito"){
            $causa = "2";
        }
        if($row->cause == "Accidente rábico"){
            $causa = "3";
        }
        if($row->cause == "Accidente ofídico"){
            $causa = "4";
        }
        if($row->cause == "Otro tipo de accidente"){
            $causa = "5";
        }
        if($row->cause == "Evento catastrófico"){
            $causa = "6";
        }
        if($row->cause == "Lesión por agresión"){
            $causa = "7";
        }
        if($row->cause == "Lesión auto infligida"){
            $causa = "8";
        }
        if($row->cause == "Sospecha de maltrato fisico"){
            $causa = "9";
        }
        if($row->cause == "Sospecha de abuso sexual"){
            $causa = "10";
        }
        if($row->cause == "Sospecha de violencia sexual"){
            $causa = "11";
        }
        if($row->cause == "Sospecha de maltrato emocional"){
            $causa = "12";
        }
        if($row->cause == "Enfermedad general"){
            $causa = "13";
        }
        if($row->cause == "Enfermedad profesional"){
            $causa = "14";
        }
        if($row->cause == "Otra"){
            $causa = "15";
        }
        // validacion tipo Nuevo 
        if($row->cups == "Nuevo"){
            $opcion = "2";
        }
        if($row->cups == "Repetido"){
            $opcion = "3";
        }
        if($row->cups == "Impresion"){
            $opcion = "1";
        }

        //modificacion formato fecha
        $fecha = date("d/m/Y", strtotime($row->_date));

        $cups = explode(",", $row->diagnosticoprincipal);
        $cupgeneral = $cups[0];

        $querycups = "SELECT * FROM cups where subcategory='$cupgeneral' ";
        $resultc = $pdo->query($querycups);
        while ($rowc = $resultc->fetch(PDO::FETCH_OBJ)) {
        $cupgeneral = $rowc->code;	
        }

        
        //validacion de diagnostico
        $diagnostico = explode(",", $row->nuevodiagnostico);
        $count = count($diagnostico);
        if($count > 0){
        //partes de diagnostico	
        $diagnostico1 = $diagnostico[0];
        $diagnostico2 = $diagnostico[1];
        $diagnostico3 = $diagnostico[2];
        $diagnostico4 = $diagnostico[3];

        $query2 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico1' ";
        $result2 = $pdo->query($query2);
        while ($row2 = $result2->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad1 = $row2->codigo;	
        }

        $query3 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico2' ";
        $result3 = $pdo->query($query3);
        while ($row3 = $result3->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad2 = $row3->codigo;	
        }
        $query4 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico3' ";
        $result4 = $pdo->query($query4);
        while ($row4 = $result4->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad3 = $row4->codigo;	
        }
        $query5 = "SELECT * FROM enfermedades_evolucion where nombre='$diagnostico4' ";
        $result5 = $pdo->query($query5);
        while ($row5 = $result5->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad4 = $row5->codigo;	
        }

        }else{

        $query6 = "SELECT * FROM enfermedades_evolucion where nombre='$row->nuevodiagnostico' ";
        $result6 = $pdo->query($query6);
        while ($row6 = $result6->fetch(PDO::FETCH_OBJ)) {
        $codigoenfermedad1 = $row6->codigo;	
        }
        $codigoenfermedad2 ="";
        $codigoenfermedad3 ="";
        $codigoenfermedad4 ="";
        }

        $valorformateado = $row->valorpago;

        
        $data .= "{$row->consecutivo},{$row->codigoprestador},{$tipo},{$row->_oid},{$fecha},,$cupgeneral,10,$causa,$codigoenfermedad1,$codigoenfermedad2,$codigoenfermedad3,$codigoenfermedad4,$opcion,$valorformateado,0,$valorformateado\r\n";
}
//saco el semestre para enviarlo en el nombre del txt
 $mes = date("m",strtotime($fecha_hora));
$mes = is_null($mes) ? date('m') : $mes;
$trim=floor(($mes-1) / 6)+1;
//tipos de header a enviar
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-disposition: attachment; filename="AC'.$fecha_actual.'-'.$trim.'.txt"');
echo $data;

?>