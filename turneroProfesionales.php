<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login_radiologia.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $clave = base64_encode($password);
    //selecionamos el usuario

    $result = mysql_query("SELECT * FROM users_radiologia where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   

    while($row = mysql_fetch_array($result)){
      $pfil = $row["__t"];
    }
    
    if($pfil =="Profesional")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    
      
    }
    else if($pfil =="Asistente")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual   
    }
    else if($pfil =="Administrador")
    {
     //header ("Location: menuadmin.php");
    }
    else
    {
      header ("Location: index_radiologia.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>TURNERO - CONSULTORIO</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/ValidacionNumerica.js"></script>
  <style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 2px; 
     border-right: 1px solid;
     font-weight: bold;
}
  </style>
  
</head>
<body>

<header>
  <?php include 'headerRadiologia.php';?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">TURNERO - CONSULTORIO</h3>

  
  <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
    ORDENES REGISTRADAS POR RECEPCION
  </div>
  <br><br>

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="0">
    <thead>
      <tr style="color: #0683c9; font-weight: bolder;">
        <td>HORA</td>
        <td>DESCRIPCION</td>
        <td>CANTIDAD</td>
        <td>NOMBRE</td>
        <td>OBSERV. INICIALES DE SOLICITUD</td>
        <td>OBSERV. DE RECEPCION</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="7" height="1px" style="padding: 0; border: 0">&nbsp;</td>
      </tr>
    </thead>
    <tbody id="tbodyOrdenMedica">
      <?php
        $centro_radiologico = $_SESSION['id_centro_radiologico']; 
        $result = mysql_query("SELECT detallefacturaradiologia.*, users.first_name, users.last_name, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, factura_radiologia.atendido FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE `id_centro_radiologico`='$centro_radiologico' AND detallefacturaradiologia.estado = 'ACTIVO'");   
        $contador = 0;

        while($row = mysql_fetch_array($result)){
          $fecha = $row["hora"];
          $descripcion = $row["descripcion"];
          $cantidad = $row["cantidad"];
          $name = $row["first_name"] . " " . $row["last_name"];
          $observacion_inicial = $row["observacion_inicial"];
          $observacion_recepcion = $row["observacion_recepcion"];
          $id_factura_radiologia = $row["id_factura_radiologia"];
          $documento = $row["documento"];
          $atendido = $row["atendido"];
      ?>
        <tr>
          <td><?= $fecha;?></td>
          <td><?= $descripcion;?></td>
          <td><?= $cantidad;?></td>
          <td><?= $name;?></td>
          <td><?= $observacion_inicial;?></td>
          <td><?= $observacion_recepcion;?></td>
          <?php
            if(trim($atendido) == "true"){
          ?>
          <td align="center"><button class="btn btn-warning boton" disabled="disabled" ind="<?= $contador;?>" id="btnLlamar<?= $contador;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" documento="<?= $documento;?>"><b>LLAMADO</b></button></td>
          <?php
            }else{
              ?>
          <td align="center"><button class="btn btn-success boton" ind="<?= $contador;?>" id="btnLlamar<?= $contador;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" documento="<?= $documento;?>"><b>LLAMADO</b></button></td>
          <?php
            }
          ?>
        </tr>
        <tr>
          <td colspan="7" height="1px" style="padding: 0; border: 0">&nbsp;</td>
        </tr>
      <?php  
        $contador++;
        }
      ?>                     
    </tbody>
  </table>

  <br>
  
 
  <?php include 'footerRadiologia.php';?> 
  
  

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <script src="js/jquery.validate.js"></script>
    <script type="text/javascript">
      $(".boton").click(function(e){
        var idFactura = $(this).attr("id_factura_radiologia")
        var documento = $(this).attr("documento")
        
        //Se guardan los datos en un JSON
        var datos = {
          user_radiologia: "<?php echo $_SESSION['userid'];?>",
          document: documento,
          idFactura: idFactura
        }   
        //Enviar datos para modificarse
        $.post("updateFactura.php", datos)
        .done(function( data ) {console.log(data)      
          window.location.href = "capturaInformacion.php";
        });
      })
      setInterval(function(){ 
        window.location.href = "turneroProfesionales.php";
      },1500);
    </script>
</body>
</html>