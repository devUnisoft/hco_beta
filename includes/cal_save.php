<?php
	session_start();
    // Loader - class and connection
	$dbhost							= "localhost";
	$dbuser							= "root";
	$dbpass							= "usc2017*";
	$dbname							= "hco";

	$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ("Error connecting to database". mysql_error());
	mysql_select_db($dbname);
  	mysql_set_charset("UTF8", $conn);

  	$fechaactual = date('Y-m-d'); 

  	// Catch start, end and id from javascript
	$titles = $_POST['title'];
	$descriptions = $_POST['description'];
	$start_date = $_POST['start_date'];
	$start_time = $_POST['start_time'];
	$end_date = $_POST['end_date'];
	$end_time = $_POST['end_time'];
	$color = $_POST['color'];
	$allDay = "true";
	$url = "false";
	$nidentificacion = $_POST['nidentificacion'];
	$apellidos = $_POST['apellidos'];
	$nombres = $_POST['nombres'];
	$fechan = $_POST['fechan'];
	$genero = $_POST['genero'];
	$email = $_POST['email'];
	$especialista = $_POST['especialista'];
	$id_especialista = $_POST['txt_id_especialista'];
	$cat = $_POST['categorie'];
	$clinica = $_POST['clinica'];
	$espacio = "  ";
	$title = $espacio.$titles;
	$description = $espacio.$descriptions;

	$nombre = $_SESSION['userid']; 
	$password = $_SESSION['pass'];
	$perfilinto = $_SESSION['perfilinto']; 
	$salt = '12000:';
	$clave = md5($salt . $password);

	$resultus = mysql_query("SELECT * FROM users where email='$nombre' AND `salt`='$clave' AND `active`='True' AND __t='$perfilinto'");   
	while($row = mysql_fetch_array($resultus)){
		$selclinica = $row["clinica"];
	} 
	
	/*
		Tipo: Variable
		Descripcion: Array que se da como resultado, el cual contiene el tipo de respuesta
		Contenido: TIPO => OK/ERROR, MENSAJE => Mensaje de confirmacion de error dependiendo de cual fue el error
	*/
	$array_response = array();

	//Conversion de horas a formato bd: yyyy-mm-dd hh:mm:ss
	$start = $start_date.' '.$start_time.':00';
	$end = $end_date.' '.$end_time.':00';
	
	$result_turno = mysql_query("SELECT * FROM users WHERE id='$id_especialista' and clinica='$selclinica'");

	while($row = mysql_fetch_array($result_turno)) {
		$turno = $row["turno"];
		$document__number = $row["document__number"];
	}

	$query_existe_turno= "SELECT * FROM `turnoslaborales` WHERE `nombreturno` = '$turno' AND clinica = '$selclinica'";
	$result_existe_turno = mysql_query($query_existe_turno) or die(mysql_error());

	if(strtotime($start) > strtotime(date('Y-m-d H:i:s')) && strtotime($end) > strtotime(date('Y-m-d H:i:s'))){
	 	/*
			Tipo: Validacion
			Descripcion: Permite verificar si un especialista tiene turno asignado. Si no tiene se manda un mensaje de error
		*/
	 	if(mysql_num_rows($result_existe_turno) > 0){

	 		/*
				Tipo: Validacion
				Descripcion: Permite verificar si si la fecha y el rango de horas de la nueva cita esta dentro del turno laboral del especialista
			*/
	 		switch (validate_turno_laboral($turno, $start_time, $end_time, $selclinica)) {
	 			case true:

	 				/*
						Tipo: Validacion
						Descripcion: Permite verificar si un especialista tiene o no bloqueo de turno laboral en una fecha y en un rango de horas de la nueva cita
					*/
			 		switch (tiene_bloqueo_turno($document__number, $start_date, $start_time, $end_time)) {
			 			case true:

			 				$array_response = array (
								"TIPO" => "ERROR",
								"MENSAJE" => "Error: El especialista " . $especialista . " no esta disponible el dia " . $start_date . " en el rango de " . $start_time . " a " . $end_time . " por bloqueo de turno."
							);
			 				break;
			 			
			 			case false:
			 				/*
								Tipo: Validacion
								Descripcion: Permite verificar si el rango de horas de la nueva cita se cruza o no con el descanso 1 del turno laboral
							*/						
							switch (validate_descansos_turno_laboral($turno, "DESCANSO_1", $start_time, $end_time, $selclinica)) {
								case false:
									/*
										Tipo: Validacion
										Descripcion: Permite verificar si el rango de horas de la nueva cita se cruza o no con el descanso 2 del turno laboral
									*/						
									switch (validate_descansos_turno_laboral($turno, "DESCANSO_2", $start_time, $end_time, $selclinica)) {
										case false:
											/*
												Tipo: Validacion
												Descripcion: Permite verificar si el rango de horas de la nueva cita se cruza o no con el descanso 3 del turno laboral
											*/						
											switch (validate_descansos_turno_laboral($turno, "DESCANSO_3", $start_time, $end_time, $selclinica)) {
												case false:												
									 				/*
														Tipo: Validacion
														Descripcion: Permite verificar si un especialista esta disponible en una fecha y hora para una cita. Si no esta disponible se manda un mensaje de error.
													*/
													
													switch (esta_disponible_calendar($id_especialista, $start, $end)) {
														case true:
															$rs_id = mysql_query("SELECT MAX(id) AS id FROM calendar");
															$id_registro = 1;
															if(mysql_num_rows($rs_id) > 0){
																while($row_id = mysql_fetch_array($result_turno)) {
																	$id_registro = trim($row_id["id"]) + 1;
																}
															}else{
																$id_registro = 1;
															}
															// insert in calendar
															$sql_insert = "INSERT INTO calendar(title, description, start,end,allDay,color,url,category,cedula,nombres,apellidos,genero,fecha_nacimiento,email,especialista,clinica,repeat_id,id_especialista) VALUES('".$title."', '".$description."', '".$start."', '".$end."', '".$allDay."', '".$color."', '".$url."', '".$cat."', '".$nidentificacion."', '".$nombres."', '".$apellidos."', '".$genero."', '".$fechan."', '".$email."', '".$especialista."', '".$clinica."', '".$id_registro."', '".$id_especialista."')";
															
														    $result_insert = mysql_query($sql_insert);

														    if($result_insert){
														    	$array_response = array (
																	"TIPO" => "OK",
																	"MENSAJE" => "¡Cita registrada con exito!"
																);
														    }else{
														    	$array_response = array (
																	"TIPO" => "ERROR",
																	"MENSAJE" => "Hubo un error interno al tratar de crear la cita"
																);
														    }
															break;	

														case false:
															$array_response = array (
																"TIPO" => "ERROR",
																"MENSAJE" => "Error: El especialista " . $especialista . " no esta disponible el dia " . $start_date . " en el rango de " . $start_time . " a " . $end_time . ". El rango de horas se cruza con otra cita asignada"
															);
															break;		
														
													}
													
													break;
												case true:
													$array_response = array (
														"TIPO" => "ERROR",
														"MENSAJE" => "Error: El rango de horas seleccionado " . $start_time . " a " . $end_time . " se cruza con las horas del descanso 3"
													);
													break;
											}
											
											break;
										case true:
											$array_response = array (
												"TIPO" => "ERROR",
												"MENSAJE" => "Error: El rango de horas seleccionado " . $start_time . " a " . $end_time . " se cruza con las horas del descanso 2"
											);
											break;
									}
									
									break;
								case true:
									$array_response = array (
										"TIPO" => "ERROR",
										"MENSAJE" => "Error: El rango de horas seleccionado " . $start_time . " a " . $end_time . " se cruza con las horas del descanso 1"
									);
									break;
							}		 				
			 				break;
			 		}

	 						

	 				break;
	 			
	 			case false:
	 				$array_response = array (
						"TIPO" => "ERROR",
						"MENSAJE" => "Error: El rango de horas seleccionado " . $start_time . " a " . $end_time . " no esta dentro del turno laboral"
					);
	 				break;
	 		}	

	 	}else{
	 		$array_response = array (
				"TIPO" => "ERROR",
				"MENSAJE" => "Error: El especialista " . $especialista . " no tiene turno asigando."
			);
	 	}
 	}else{
 		$array_response = array (
			"TIPO" => "ERROR",
			"MENSAJE" => "Error: La hora inicial y/o final debe ser mayor que la hora actual"
		);
 	}
 	echo json_encode($array_response);
	
	
	/*
		Tipo: Funcion
		Descripcion: Permite verificar si un especialista esta disponible en una fecha y hora para una cita.
	*/
	function esta_disponible_calendar($id_especialista, $start, $end){
  		$estado = true;

  		//Consulta para saber si un existe una cita con los mismos datos del rango de horas
  		$query_select_existe = "SELECT * FROM `calendar` WHERE `start` = '$start' AND `end` = '$end' AND `id_especialista` = '$id_especialista'";

  		//Consulta para saber si un especialista esta disponible en una fecha y hora que esta dentro de un rango
  		$query_select_entre = "SELECT * FROM `calendar` WHERE ('$start' > `start` AND '$start' < `end`) OR ('$end' > `start` AND '$end' < `end`) AND `id_especialista` = '$id_especialista'";

  		//Consulta para saber si un especialista esta disponible en una fecha y hora que se cruza con alguna cita
  		$query_select_antes_despues = "SELECT * FROM `calendar` WHERE (`start` > '$start' AND `start` < '$end') OR (`end` > '$start' AND `end` <  '$end') AND `id_especialista` = '$id_especialista'";

	  	$result_select_existe = mysql_query($query_select_existe) or die(mysql_error());
	  	$result_select_entre = mysql_query($query_select_entre) or die(mysql_error());
	  	$result_select_antes_despues = mysql_query($query_select_antes_despues) or die(mysql_error());

	  	if((mysql_num_rows($result_select_entre) > 0) || (mysql_num_rows($result_select_antes_despues) > 0) || (mysql_num_rows($result_select_existe) > 0)){
	  		$estado = false;
	  	}else{
	  		$estado = true;
	  	}

	  	return $estado;
	}

	/*
		Tipo: Funcion
		Descripcion: Permite verificar si un especialista tiene o no bloqueo de turno laboral en una fecha y en un rango de horas de la nueva cita
	*/
	function tiene_bloqueo_turno($documento, $fecha, $hora_inicio, $hora_final){
		$estado = false;
		$hora_inicio = $hora_inicio . ":00";
		$hora_final = $hora_final . ":00";

		//Consulta para saber si un existe un bloqueo con los mismos datos del rango de horas
  		$query_select_existe = "SELECT * FROM `bloqueoturnos` WHERE `horainicial` = '$hora_inicio' AND `horafinal` = '$hora_final' AND `documento` = '$documento'";

		//Consulta para saber si un especialista tiene o no bloqueo de turno laboral en una fecha y hora que esta dentro de un rango
  		$query_select_entre = "SELECT * FROM `bloqueoturnos` WHERE (('$hora_inicio' BETWEEN `horainicial` AND `horafinal`) OR ('$hora_final' BETWEEN `horainicial` AND `horafinal`)) AND (fechainicial <= '$fecha' AND fechafinal >= '$fecha') AND `documento` = '$documento'";

  		//Consulta para saber si un especialista esta disponible en una fecha y hora que se cruza con algun bloqueo de turno
  		$query_select_antes_despues = "SELECT * FROM `bloqueoturnos` WHERE ((`horainicial` BETWEEN '$hora_inicio' AND '$hora_final') OR (`horafinal` BETWEEN '$hora_inicio' AND '$hora_final')) AND (fechainicial <= '$fecha' AND fechafinal >= '$fecha') AND `documento` = '$documento'";

  		$result_select_existe = mysql_query($query_select_existe) or die(mysql_error());
  		$result_select_entre = mysql_query($query_select_entre) or die(mysql_error());
	  	$result_select_antes_despues = mysql_query($query_select_antes_despues) or die(mysql_error());

	  	if((mysql_num_rows($result_select_entre) > 0) || (mysql_num_rows($result_select_antes_despues) > 0) || (mysql_num_rows($result_select_existe) > 0)){
	  		$estado = true;
	  	}else{
	  		$estado = false;
	  	}

		return $estado;
	}

	/*
		Tipo: Funcion
		Descripcion: Permite verificar si la fecha y el rango de horas de la nueva cita esta dentro del turno laboral del especialista
	*/
	function validate_turno_laboral($turno, $hora_inicio, $hora_final, $selclinica){
		$estado = false;
		
		//Consulta para saber el rango de horas de la nueva cita esta dentro del rango de horas del turno laboral del especialista
		$query_select_entre = "SELECT * FROM `turnoslaborales` WHERE ('$hora_inicio' BETWEEN horainicialturno AND horafinalturno) AND ('$hora_final' BETWEEN horainicialturno AND horafinalturno) AND `nombreturno` = '$turno' AND clinica = '$selclinica'";

		$result_select_entre = mysql_query($query_select_entre) or die(mysql_error());

		if(mysql_num_rows($result_select_entre) > 0){
	  		$estado = true;
	  	}else{
	  		$estado = false;
	  	}
	
		return $estado;
	}

	/*
		Tipo: Funcion
		Descripcion: Permite verificar si la fecha y el rango de horas de la nueva cita se cruza con las horas de descanso
	*/
	function validate_descansos_turno_laboral($turno, $tipo_descanso, $hora_inicio, $hora_final, $selclinica){
		
		$estado = false;
		$columna_hora_inicio = "";
		$columna_hora_fin = "";

		switch ($tipo_descanso) {
			case 'DESCANSO_1':
				$columna_hora_inicio = "horainicialdescanso1";
				$columna_hora_fin = "horafinaldescanso1";
				break;
			case 'DESCANSO_2':
				$columna_hora_inicio = "horainicialdescanso2";
				$columna_hora_fin = "horafinaldescanso2";
				break;
			case 'DESCANSO_3':
				$columna_hora_inicio = "horainicialdescanso3";
				$columna_hora_fin = "horafinaldescanso3";
				break;
		}

		$query_turno = "SELECT * FROM `turnoslaborales` WHERE `nombreturno` = '$turno'";
		$result_select_turno = mysql_query($query_turno) or die(mysql_error());

		while($row = mysql_fetch_array($result_select_turno)) {
	       $hora_incio_descanso = $row[$columna_hora_inicio];
	       $hora_final_descanso = $row[$columna_hora_fin];

	       	if($hora_incio_descanso != "" && $hora_final_descanso != ""){

	       		//Consulta para saber si un existe un descanso con los mismos datos del rango de horas
  				$query_select_existe = "SELECT * FROM `turnoslaborales` WHERE `$columna_hora_inicio` = '$hora_inicio' AND `$columna_hora_fin` = '$hora_final' AND `nombreturno` = '$turno' AND clinica = '$selclinica'";

	    		//Consulta para saber si un rango de horas se cruza con un descanso
		  		$query_select_entre = "SELECT * FROM `turnoslaborales` WHERE (('$hora_inicio' > `$columna_hora_inicio` AND '$hora_inicio' < `$columna_hora_fin`) OR ('$hora_final' > `$columna_hora_inicio` AND '$hora_final' < `$columna_hora_fin`)) AND `nombreturno` = '$turno' AND clinica = '$selclinica'";

		  		//Consulta para saber si un rango de horas se cruza con un descanso
		  		$query_select_antes_despues = "SELECT * FROM `turnoslaborales` WHERE ((`$columna_hora_inicio` > '$hora_inicio' AND`$columna_hora_inicio` < '$hora_final') OR (`$columna_hora_fin` > '$hora_inicio' AND`$columna_hora_fin` < '$hora_final')) AND `nombreturno` = '$turno' AND clinica = '$selclinica'";

		  		$result_select_existe = mysql_query($query_select_existe) or die(mysql_error());
		  		$result_select_entre = mysql_query($query_select_entre) or die(mysql_error());
			  	$result_select_antes_despues = mysql_query($query_select_antes_despues) or die(mysql_error());

			  	if((mysql_num_rows($result_select_entre) > 0) || (mysql_num_rows($result_select_antes_despues) > 0) || (mysql_num_rows($result_select_existe) > 0)){
			  		$estado = true;
			  	}else{
			  		$estado = false;
			  	} 

	       	}else{
	       		$estado = false;
	       	}
	 	}

		

		return $estado;
	}
?>