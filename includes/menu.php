﻿<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica
    $_SESSION['clinica'] = $_POST["clinica"];

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $salt = '12000:';
    $clave = md5($salt . $password);
    //selecionamos el usuario
    $result = mysql_query("SELECT * FROM users where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   
    while($row = mysql_fetch_array($result)){
    $pfil = $row["__t"];
    }


    if($pfil =="odontologo" && $perfilusuario =="Person")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    
    }
    else if($pfil =="Asistente" && $perfilusuario =="Asistente")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual   
    }
    else if($pfil =="Administrador" && $perfilusuario =="Administrador")
    {
         header ("Location: menuadmin.php");
    }
    else
    {
        header ("Location: index.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    }
    //selecionamos el super usuario
    $resultus = mysql_query("SELECT * FROM users where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   
    while($row = mysql_fetch_array($resultus)){
    $pfilsuperusuario = $row["perfil"];
    $selclinica = $row["clinica"];
    $pfilus = $row["__t"];
    }

    $_SESSION['clinicalocal'] = $selclinica;

    // mysql select lista odontologos
    $resultodontologo = mysql_query("SELECT * FROM users where __t='odontologo' and clinica='$selclinica' ORDER BY last_name ASC");
    //capturo session para enviar al backend de calendario
    $_SESSION['odontologo'] = $_GET["usuario"];
    //
    $pfil = $_SESSION['perfilus'];

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Citas</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin-left: -130px;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}

.pull-leftn {
    float: left !important; 
    padding-right: 10px;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
    height: 220px;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
    height: 100px;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
    height: 140px;
}
}

.media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }

    .media-heading {
    margin: 0 0 5px;
}

.media, .media-body {
    overflow: hidden;
    zoom: 1;
}

.media:first-child {
    margin-top: 0;
}


}
.media, .media .media {
    margin-top: 15px;
}
.media, .media-body {
    overflow: hidden;
    zoom: 1;
}


  </style>
  <script type="text/javascript">
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
         window.onload = init();
  </script>
   <script type='text/javascript'> 
          $(document).ready(function(){

          perfiluser = '<?php echo $pfilus ;?>';
          // then
          localStorage.setItem('perfil', perfiluser);
          var envia = localStorage.getItem("perfil");
           console.log(envia);


          $("#search_results").slideUp(); 
              $("#search_button").click(function(e){ 
                  e.preventDefault(); 
                  ajax_search(); 
              }); 
              $("#nidentificacion").keyup(function(e){ 
                  e.preventDefault(); 
                  ajax_search(); 
                  ajax_search1(); 
                  ajax_search2();
                  ajax_search3();
                  ajax_search4();
              }); 
          });
          function ajax_search(){ 
            $("#apellidos").show(); 
            var search_val=$("#nidentificacion").val(); 
            $.post("./find2.php", {search_term : search_val}, function(data){
             if (data.length>0){ 
               $("#apellidos").val(data); 
             } 
            }) 
          } 
          function ajax_search1(){ 
            $("#nombres").show(); 
            var search_val=$("#nidentificacion").val(); 
            $.post("./find3.php", {search_term : search_val}, function(data){
             if (data.length>0){ 
               $("#nombres").val(data); 
             } 
            }) 
          } 
          function ajax_search2(){ 
            $("#fechan").show(); 
            var search_val=$("#nidentificacion").val(); 
            $.post("./find4.php", {search_term : search_val}, function(data){
             if (data.length>0){ 
               $("#fechan").val(data); 
             } 
            }) 
          } 
          function ajax_search3(){ 
            $("#genero").show(); 
            var search_val=$("#nidentificacion").val(); 
            $.post("./find5.php", {search_term : search_val}, function(data){
             if (data.length>0){ 
               $("#genero").val(data); 
             } 
            }) 
          } 

          function ajax_search4(){ 
            $("#email").show(); 
            var search_val=$("#nidentificacion").val(); 
            $.post("./find6.php", {search_term : search_val}, function(data){
             if (data.length>0){ 
               $("#email").val(data); 
             } 
            }) 
          } 
        </script>

</head>
<body>

<header>
  <?php include 'header.php';?>
</header>
<div class="container-fluid">
  <div class="row">
    </br>
    <div class="col-md-2">   
    </div>
        <div class="col-md-8">
            <!-- Button add 
            <a href="add_event.php" class="btn btn-default pull-right" style="margin-bottom: 20px;">Agregar Cita</a> 
            -->
            <div class="header"><h4>Calendario</h4></div>
            </br>
            </br>
            <div class="content"> 
                <div id="calendar"></div>
              </br>
              </br>
              </br>
              </br>
            </div> 

              <!-- Modal View Event -->
    <div id="cal_viewModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
        
        <div class="modal-body" style="height: 350px;"></div>
        <div class="modal-footer">
            <a href="#" class="btn btn-info" data-option="edit">Editar</a>
            <a href="#" class="btn btn-danger" data-option="remove" >Borrar</a>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
        </div>
        </div>
    </div>
    
    <!-- Modal Edit Event -->
    <div id="cal_editModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
            <a href="#" class="btn btn-primary" data-option="save">Guardar Cambios</a>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
        </div>
        </div>
    </div>
    
    <!-- Modal QuickSave Event -->
    <div id="cal_quickSaveModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
            <a href="#" class="btn btn-primary" data-option="quickSave">Guardar</a>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
        </div>
        </div>
    </div>
    
    <!-- Modal Delete Prompt -->
    <div id="cal_prompt" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <a href="#" class="btn btn-danger" data-option="remove-this">Borrar Esto</a>
            <a href="#" class="btn btn-danger" data-option="remove-repetitives">Borrar Todo</a>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
        </div>
        </div>
    </div>
    
    <!-- Modal Edit Prompt -->
    <div id="cal_edit_prompt_save" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body-custom"></div>
        <div class="modal-footer">
          <a href="#" class="btn btn-info" data-option="save-this">Guardar</a>
            <a href="#" class="btn btn-info" data-option="save-repetitives">Guardar Todo</a>
          <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
        </div>
        </div>
        </div>


      </div>
        
  </div>
          <div class="col-md-2">
          </br>
          </br>
          </br>
          </br>
          </br>
          </br>
          <?php
          if($pfilsuperusuario == "Asistente"){
            $vartodos = "Todos";
            echo "<center><a href='menu.php?usuario=$vartodos' id='vertodos' class='btn btn-primary'>Ver&nbsp;&nbsp;Todos&nbsp;</a></center>";
           ?>
          </br> 
          <div style="height: 500px; overflow-y: auto;background-color: #e5e5e5;">
          </br>
          <center><span><b>Seleccione el Especialista</b></span></center>
          </br>
          <?php
                
                while($row = mysql_fetch_array($resultodontologo))
               {
                    $correo = $row["email"];
                    $imgurl = $row["image_url"];  
                    //calculando edad
                    $fechanacimiento = $row["birthdate"];  
                    $fecha = time() - strtotime($fechanacimiento);
                    $edad = floor($fecha / 31556926);

                    //selector de genero
                    $generos = $row["gender"]; 
                    if($generos =='M'){
                       $genero = "Masculino";
                    }else{
                        $genero = "Femenino";
                    }
                    
                    echo "<div class='col-md-12'>";
                    echo "<div class='media' style='background-color:white;margin-bottom: 10px;padding-bottom: 15px;padding-top: 15px;'>";
                    echo "<a class='pull-leftn' href='menu.php?usuario=$correo' style='margin-left: -20px;'>";
                    if(empty($imgurl)){
                     echo "<img class='media-object dp img-circle' src='images/paciente.png' style='width: 50px;height:50px;'>";    
                 }else{
                    echo "<img class='media-object dp img-circle' src='$imgurl' style='width: 50px;height:50px;'>";
                 }
                    
                    echo "</a>";
                        echo "<div class='media-body'>";
                           echo "<h6 class='media-heading'><b>".$row["last_name"]."</b></h6>";
                           echo "<h6 class='media-heading'><b>".$row["first_name"]."</b></h6>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";

              }
              ?>
            
          </div>
          <?php }?>
            </br>
            </br>
            <center><a href="reportescitas.php" id="rgenerales" class="btn btn-warning ">Reportes&nbsp;&nbsp;Generales&nbsp;</a></center>
            </br>
            <center><a href="reportescitasespecialista.php" d="respecialista" class="btn btn-warning ">Reportes Especialista</a></center>
        </div>
</div>

  <?php include 'menucitas.php';?>
<?php
if($pfil !="Asistente"){
     echo  "<script type='text/javascript'>"; 
     echo  "$(document).ready(function(){";
     echo  "$('#especialista_updated').attr('readonly', true);";
     echo  "})";
     echo  "</script>";
    }
?>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <!-- call calendar plugin -->
    <script type="text/javascript">
    $().FullCalendarExt({
      calendarSelector: '#calendar',
      quickSaveCategory: '<label>Category: </label><select name="categorie"><?php foreach($calendar->getCategories() as $categorie) { ?> <option value="<?php echo $categorie?>"><?php echo $categorie; ?></option><?php } ?></select>',
    });
  </script>
</body>
</html>