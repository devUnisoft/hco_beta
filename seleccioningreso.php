<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inicio</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style type="text/css">
    	

.navbar-default {
	background-color:transparent !important;
	border-color:transparent;
	background-image:none;
	box-shadow:none;	
}
body{
	width: auto;
	height: auto;
	overflow-x:hidden; 
  
}

.navbar-default .navbar-nav>li>a:hover{
	font-weight: bolder;
	background-color: #0683c9;
	color: #FFF;
}
.navbar-default .navbar-nav>li{

}
.navbar-default .navbar-nav>#invisible>a:hover{
	font-weight: normal;
	background: none;
	color: #777;
}
@media (min-width: 1000px){
	#contenido {
	    height: 500px;
	}
}
@media (min-width: 1200px){
.container {
    width: 1170px;
    margin-top: 20px;
}}



@media (max-width: 900px){ 	

	#contenido{
	  	height: 1300px;
	}
} 

@media (max-width: 700px){ 	

	#contenido{
	  	height: 1100px;
	}


} 

@media (max-width: 600px){ 	

	#contenido{
	  	height: 900px;
	}
} 

    </style>
  </head>
  <body>
   <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<img  src="img/bn1.png"  width="100%">

			<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			    <div class="container-fluid" id="navfluid">
			        <div class="navbar-header">
			           <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigationbar">
			               <span class="sr-only"></span>
			               <span class="icon-bar"></span>
			               <span class="icon-bar"></span>
			               <span class="icon-bar"></span>
			            </button>
			            <a class="navbar-brand" style="margin-left: 165px;" href="#"></a>
			       </div>
			        <div class="collapse navbar-collapse" id="navigationbar">
			           	<ul class="nav navbar-nav" style="float: right">
			                <li><a href="index.html" >INICIO</a></li>
			                <li id="invisible"><a href="" >|</a></li>
			                <li><a href="quienesomos.html" >HCO</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="porqueelegirhco.html" >VENTAJAS</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="#" >NUESTROS PLANES</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="beneficios.html" >BENEFICIOS</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="caracteristicas.html" >CARACTERISTICAS</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="contactenos.html" >CONTACTENOS</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="soporte.html" >SOPORTE</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="#" >BLOG</a></li>
			                <li id="invisible" ><a href="" >|</a></li>
			                <li><a href="http://190.60.211.17/hco/seleccioningreso.php" >INGRESAR A HCO</a></li>
			            </ul>
			        </div><!--/.nav-collapse -->
			    </div>
             </div>
           </div>
         </div>


		<div class="row" id="contenido">

		<center><button onclick="location.href='http://190.60.211.17/hco/CentrosRadiologicos/#no-back-button'" type="button" style="background-color: #0288d1;border: none;color: white;padding: 15px 16px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;box-shadow: 2px 2px 5px #999;">Centros Radiologicos</button></center>
		</br>
		<center><button onclick="location.href='http://190.60.211.17/hco/index.php'" type="button" style="background-color: #0288d1;border: none;color: white;padding: 15px 16px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;box-shadow: 2px 2px 5px #999;">HCO</button></center>
		</br>
		<center><button onclick="location.href='http://190.60.211.17/hco/loginobservaciones.php'" type="button" style="background-color: #0288d1;border: none;color: white;padding: 15px 16px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;box-shadow: 2px 2px 5px #999;">Test Group</button></center>
			
				
		
		</br>
		</br>
		</br>
		</br>
		</br>
	</div>
		
	
	<div id="footer" >
	    <div class="col-md-12">
	      <hr style="border-top: 2px solid #0683c9;">
	    </div>
	    <div class="col-md-5" align="left">
	      <font color="#3ca3c1" style="font-weight:bold;">Cont&aacute;ctenos: (571)2189575 Bogot&aacute; Colombia</font>
	      <img src="img/tel.jpeg"  height="45px" width="45px" style="margin-top: 0">
	    </div>
	    <div class="col-md-3" align="left">
	      <font color="#3ca3c1" style="font-weight:bold">Email : Contacto@hco.com.co</font>
	    </div>
	    <div class="col-md-4" align="left">
	      <a target="_blank" href="https://www.facebook.com/HCO-Historia-Clínica-Odontológica-1417745198485609/"><img src="img/facebook_C.png" width="45"></a>
	      <a target="_blank" href="https://www.youtube.com/channel/UCYH1KzayJBMjUY0b7Og99rQ"><img src="img/Youtube_C.png" width="45"></a>
	      <a target="_blank" href="https://plus.google.com/111760529658199492264"><img src="img/google_C.png" width="45"></a>  
	    </div>
  	</div>
 </div>
</div>



    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>