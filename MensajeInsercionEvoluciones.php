<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Mensaje</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
	</script>
	<style type="text/css">
		.loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
		}

		header {
    width: 100%;
    height: 150px;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
	</style>
	<script type="text/javascript">
		function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
         window.onload = init();
	</script>	
</head>
<body>

<header>
    <div class="container clearfix">
        </br>
        <center><img src="images/logo_blanco.png"  height="15%" width="15%"></center>
    </div>
</header>

<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
	<div class="row">
		
		<div class="col-md-3">
		</div>
		<div class="col-md-6">
         <?php
         error_reporting(0);
         //inicio de session
         session_start();

         require_once 'db_connect.php';
         // connecting to db
         $db = new DB_CONNECT();

         $clinicaconsultar = $_SESSION['clinicaperfiluno'];
         //selecciono datos en busqueda clinica y factura
            $resultclinica = mysql_query("SELECT *  FROM clinicas where razonsocial='$clinicaconsultar' ");
            //ciclo pago
            while($row = mysql_fetch_array($resultclinica)){
            $numerofactura = $row["numeroconsecutivoevolucion"];
            }

           if($numerofactura > 0){

             //busqueda de pago
            $resultpagop = mysql_query("SELECT *  FROM evolutions where clinica='$clinicaconsultar'  ORDER BY consecutivo DESC LIMIT 1 ") or die(mysql_error());
            //ciclo pago
            while($row = mysql_fetch_array($resultpagop)){
            $numfact = $row["consecutivo"];
            }
            //busqueda de registros
            $num_rows = mysql_num_rows($resultpagop);

            //validacion de registros > 0
            if ( $numfact > 0) {
               $numfact = $numfact + 1;
            }else{
               $numfact = $numerofactura + 1;
            }


       

         $presupuestoa = $_GET["optseleted"];
         //validacion de seleccion presupuesto
         if ($presupuestoa == "seleccione"){ 
         //valores por get de evoluciones.php
         $didentidad =  $_GET["didentidad"];

        
         //selecciono datos 
         $resulttipod = mysql_query("SELECT *  FROM users where document__number='$didentidad' ");
         //ciclo 
         while($row = mysql_fetch_array($resulttipod)){
         $tipodocumentouser = $row["document__document_type__oid"];
         }

         $tipohistorial =  $_GET["Conocido"];
         $evolucion = $_GET["evolucion"];
         $fecha = $_GET["fecha"];
         $tipo = $_GET["tipo"];
         $causas = $_GET["causas"];
         $procedimiento = $_GET["nombrereferenciap"];
         //validacion campo vacio
         if (!empty($_GET["nuevo"]) && $procedimiento="seleccione"){ 
         $nuevodiagnostico = $_GET["nuevo"];
         $cadena_nuevodiagnostico = implode(",", $nuevodiagnostico);
         }
         $diagnosticoprincipal = $_GET["diagnos"];
         $cadena_diagnosticoprincipal = implode(",", $diagnosticoprincipal);
         $especialista = $_GET["especialista"];
         $clinica = $_SESSION['clinicaperfiluno'];
         $nombrereferenciap = $_GET["nombrereferenciap"];
         $acudiente_seleccionado = $_SESSION['acudiente_seleccionado'];
            //valor a enviar
            $valorpenviar = $_GET["valorpresupuesto"];
            // mysql inserting a new row
            $result = mysql_query("INSERT INTO evolutions(_date, _oid, comments, cups, type, cause, diagnosticoprincipal, nuevodiagnostico,procedimiento,especialista,clinica,tipodocumento,abono,consecutivo,acompanante,tratamiento) VALUES('".$fecha."', '".$didentidad."', '".$evolucion."' , '".$tipohistorial."', '".$tipo."', '".$causas."', '".$cadena_diagnosticoprincipal."', '".$cadena_nuevodiagnostico."', '".$nombrereferenciap."', '".$especialista."', '".$clinica."', '".$tipodocumentouser."', '".$valorpenviar."', '".$numfact."', '".$acudiente_seleccionado."','".$_GET["presupuestoa"]."')");

             $emailesp = $_GET["emailesp"];
             $apellidos = $_GET["apellidos"];
             $nombres = $_GET["nombres"];
             $nombrereferenciap = $_GET["nombrereferenciap"];
             $didentidad = $_GET["didentidad"];
             $fecha_actual=date("d/m/Y");

            //selecionamos el usuario
            $resultusers = mysql_query("SELECT * FROM users where document__number='$didentidad'");   
            while($row = mysql_fetch_array($resultusers)){
            $emailpaciente = $row["email"];
            $tipodocumentouser = $row["document__document_type__oid"];
            }

            $jsonpresupuestos = $_GET["json"];
            $valorpresupuesto = $_GET["valorpresupuesto"];
            $valorpresupuesto = number_format($valorpresupuesto, 2);

            $num = 3;
            $valorpresupuesto = substr($valorpresupuesto, 0, -$num);

            $cuotas = $_GET["cuotas"]; 
            $descuentop = $_GET["descuentop"]; 
            $observacionesp = $_GET["observacionesp"]; 

            // mysql inserting a new row
            $tipo = $_GET["presupuestoa"];
            $resultps = mysql_query("INSERT INTO presupuestos(documento,nombres,apellidos,email,especialista,fecha,json,valor,cuotas,descuento,observaciones,estado,nombreprocedimiento,tipodocumento,clinica,tipo) VALUES('".$didentidad."', '".$nombres."' , '".$apellidos."', '".$emailpaciente."', '".$emailesp."', '".$fecha_actual."', '".$jsonpresupuestos."', '".$valorpresupuesto."', '".$cuotas."', '".$descuentop."', '".$observacionesp."', '1', '".$nombrereferenciap."', '".$tipodocumentouser."', '".$clinica."', '".$tipo."')");

            // check if row inserted or not
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Evolucion";
                            echo "</div>";
                            echo "</div>";
                            header( "refresh:3;url=evoluciones.php?usuario=$emailpaciente" );
            } else {
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Evolucion";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                             header( "refresh:3;url=evoluciones.php?usuario=$emailpaciente" );
            }
        }else{



                         //busqueda de pago
                        $resultpagop = mysql_query("SELECT *  FROM evolutions where clinica='$clinicaconsultar'  ORDER BY consecutivo DESC LIMIT 1 ") or die(mysql_error());
                        //ciclo pago
                        while($row = mysql_fetch_array($resultpagop)){
                        $numfact = $row["consecutivo"];
                        }
                        //busqueda de registros
                        $num_rows = mysql_num_rows($resultpagop);

                        //validacion de registros > 0
                        if ( $numfact > 0) {
                           $numfact = $numfact + 1;
                        }else{
                           $numfact = $numerofactura + 1;
                        }

       
                
                         $fechaactual = date("Y-m-d");
                         //insert evoluciones
                         $didentidad =  $_GET["didentidad"];


                         //selecciono datos 
                         $resulttipod = mysql_query("SELECT *  FROM users where document__number='$didentidad' ");
                         //ciclo 
                         while($row = mysql_fetch_array($resulttipod)){
                         $tipodocumentouser = $row["document__document_type__oid"];
                         }

                         $tipohistorial =  $_GET["Conocido"];
                         $evolucion = $_GET["evolucion"];
                         $fecha = $_GET["fecha"];
                         $tipo = $_GET["tipo"];
                         $causas = $_GET["causas"];
                         $procedimiento = $_GET["presupuestoa"];
                         $nuevodiagnostico = $_GET["nuevo"];
                         $cadena_nuevodiagnostico = implode(",", $nuevodiagnostico);
                         $diagnosticoprincipal = $_GET["diagnos"];
                         $cadena_diagnosticoprincipal = implode(",", $diagnosticoprincipal);
                         $especialista = $_GET["especialista"];
                         $clinica = $_SESSION['clinicaperfiluno'];
                          //selecionamos el usuario
                        $resultusers = mysql_query("SELECT * FROM users where document__number='$didentidad'");   
                        while($row = mysql_fetch_array($resultusers)){
                        $emailpaciente = $row["email"];
                        }
                        // mysql inserting a new row
                        $result = mysql_query("INSERT INTO evolutions(_date, _oid, comments, cups, type, cause, diagnosticoprincipal, nuevodiagnostico,procedimiento,especialista,clinica,tipodocumento,consecutivo) VALUES('".$fecha."', '".$didentidad."', '".$evolucion."' , '".$tipohistorial."', '".$tipo."', '".$causas."', '".$cadena_diagnosticoprincipal."', '".$cadena_nuevodiagnostico."', '".$procedimiento."', '".$especialista."', '".$clinica."', '".$tipodocumentouser."', '".$numfact."')");

       // check if row inserted or not
            if ($result) {
                            // successfully inserted into database
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div >";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Exitosa de Evolucion";
                            echo "</div>";
                            echo "</div>";
                            header( "refresh:3;url=evoluciones.php?usuario=$emailpaciente" );
            } else {
                            // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Insersion Fallida de Evolucion";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                             header( "refresh:3;url=evoluciones.php?usuario=$emailpaciente" );
            }






        }

         }else{

             // failed to insert row
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "</br>";
                            echo "<div>";
                            echo "<div class=\"alert alert-success\" role=\"alert\">";
                            echo "Comuniquese con el administrador, el numero de consecutivo inicial no ha sido creado en la clinica para poder registrar la evolucion";
                            echo $result;
                            echo "</div>";
                            echo "</div>";
                             header( "refresh:3;url=evoluciones.php?usuario=$emailpaciente" );

        }
        
        ?>
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
</body>
</html>