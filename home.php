<?php include('includes/loader.php'); ?>
<?php
error_reporting(0);
/* Empezamos la sesión */
    session_start();

    $perfilusuario = $_SESSION['text'];
    //cuando es doble perfil clinica

    /* Si no hay una sesión creada, redireccionar al index. */
    if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
        header('Location: login_radiologia.php');
    } // Recuerda usar corchetes
   
    require_once 'db_connect.php';
    // connecting to db
    $db = new DB_CONNECT();
    //agregando post a vars
    $nombre = $_SESSION['userid']; 
    $password = $_SESSION['pass'];
    $clave = base64_encode($password);
    //selecionamos el usuario

    $result = mysql_query("SELECT * FROM users_radiologia where email='$nombre' AND `salt`='$clave' AND `active`='True' ");   

    while($row = mysql_fetch_array($result)){
      $pfil = $row["__t"];
    }
   
    if($pfil =="Profesional")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual    
      header ("Location: turneroProfesionales.php");
    }
    else if($pfil =="Asistente")
    {
     //no se redirige por ser perfil que ingresa a menu de esta forma se queda en la hoja actual   
    }
    else if($pfil =="Administrador")
    {
      header ("Location: homeAdmin.php");
    }
    else
    {
      header ("Location: index_radiologia.php?mensaje='Usuario o Contraseña o Perfil Erroneos'");
    } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>RECEPCION DE ATENCION PARA PACIENTES</title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/ui-lightness/jquery-ui.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="lib/colorpicker/css/colorpicker.css" rel="stylesheet">
  <link href="lib/validation/css/validation.css" rel="stylesheet">
  <link href="lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
  <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/ValidacionNumerica.js"></script>
  <style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 10px; 
}
  </style>
  
</head>
<body>

<header>
  <?php include 'headerRadiologia.php';?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">RECEPCION DE ATENCION PARA PACIENTES</h3>
  <br>
  <div class="row"> 
    <form id="frmFind" method="post" action="">
      <div class="col-md-3" style="margin:0">   
        <label><font color="#0683c9">No. IDENTIFICACION</font></label><br>
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr style="padding: 0">
            <td><input type="text" name="txtIdentificacion" id="txtIdentificacion" style="text-align: center;" class="form-control"></td>
            <td width="10px">&nbsp;</td>
            <td><button type="submit" class="btn" style="background-color: green; color: #FFF; border: 3px #CCC; border-radius: 100%; width: 45px; height: 45px">GO</button></td>
          </tr>    
        </table>
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">NOMBRE</font></label><br>
        <input type="text" name="txtNombre" id="txtNombre" style="text-align: center;" class="form-control" disabled>      
      </div> 
      <div class="col-md-3">   
        <label><font color="#0683c9">DIRECCION</font></label><br>
        <input type="text" name="txtDireccion" id="txtDireccion" style="text-align: center;" class="form-control" disabled>      
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">TELEFONO</font></label><br>
        <input type="text" name="txtTelefono" id="txtTelefono" style="text-align: center;" class="form-control" disabled>      
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">EMAIL</font></label><br>
        <input type="text" name="txtEmail" id="txtEmail" style="text-align: center;" class="form-control" disabled>      
      </div> 
    </form>
  </div>
  <br>
  
  <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
    Procedimientos Almacenados
  </div>
  <br><br>

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
    <thead>
      <tr style="color: #0683c9; border-color: #0683c9; font-weight: bolder;">
        <td>FECHA SOLICITUD</td>
        <td>DESCRIPCION</td>
        <td>TIPO</td>
        <td>CARACTERISTICA</td>
        <td>OBSERVACIONES</td>
        <td>&nbsp;</td>
      </tr>
    </thead>
    <tbody id="tbodyOrdenMedica">
                                    
    </tbody>
  </table>

  <br>
  <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
    Datos para Facturación
  </div>
  <br><br>

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1">
    <thead>
      <tr style="color: #0683c9; border-color: #0683c9; font-weight: bolder;">
        <td>DESCRIPCION</td>
        <td>CANTIDAD</td>
        <td>VR.UNITARIO</td>
        <td>SUBTOTAL</td>
        <td>OBSERVACIONES DE RECEPCION</td>
      </tr>
    </thead>
    <tbody id="tbodyResumen">
                                         
    </tbody>
  </table>

  <br><br>
  <div class="row">    
    <div class="col-md-8" align="right">
      <label style="margin-top: 6px"><font color="#0683c9">VALOR TOTAL DE PAGO</font></label>    
    </div>
    <div class="col-md-2" align="center">
      <input type="tex" id="txtValorTotal" name="txtValorTotal" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">    
    </div>
    <div class="col-md-2" align="right">
      <button class="btn btn-primary" disabled="disabled" id="btnGenerarFactura">RECIBIR PAGO</button>    
    </div>
  </div>
  <br><br>
  <?php include 'footerRadiologia.php';?>
  
  

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.js"></script>
    
    <script src="js/custom.js"></script>
    
    <!-- call calendar plugin -->
    <script type="text/javascript">
    $().FullCalendarExt({
      calendarSelector: '#calendar',
      quickSaveCategory: '<label>Category: </label><select name="categorie"><?php foreach($calendar->getCategories() as $categorie) { ?> <option value="<?php echo $categorie?>"><?php echo $categorie; ?></option><?php } ?></select>',
    });
  </script>
  <script src="js/jquery.validate.js"></script>
  <script type="text/javascript">
    $.validator.setDefaults({
        //Capturar evento del boton crear
        submitHandler: function() {
            var identificacion = $("#txtIdentificacion").val();
            //Se guardan los datos en un JSON
            var datos = {
                document: identificacion
            }   
            //$("button").addClass("disabled")
            $.post("findPersonaFactura.php", datos)
            .done(function( data ) {//console.log(data)             
               if($.trim(data) != "{}"){
                var json = JSON.parse(data)
                $("#txtNombre").val(json.first_name + " " + json.last_name)
                $("#txtDireccion").val(json.direccion)
                $("#txtTelefono").val(json.telefono)
                $("#txtEmail").val(json.email)

                $("#tbodyOrdenMedica").html("")
                if(json.ordenMedica.length > 0){                 

                  for(i = 0; i < json.ordenMedica.length; i++){
                    
                    var jsonDatos = json.ordenMedica[i].orden
                    
                    for(var j = 0; j < jsonDatos.length; j++){
                      
                      $("#tbodyOrdenMedica").append("<tr id='tr" + $("#tbodyOrdenMedica > tr").length + "'><td>" + json.ordenMedica[i].fecha + "</td><td>" + jsonDatos[j].Descripcion + "</td><td tipo='" + jsonDatos[j].idTipo + "'>" + jsonDatos[j].Tipo + "</td><td>" + jsonDatos[j].Seleccion + "</td><td>" + jsonDatos[j].Observacion + "</td><td align='center'><input type='checkbox' name='checkboxAgregar' id='checkboxPersona" + i + "Procedimiento" + j + "' presentacion='" + JSON.stringify(jsonDatos[j].idPresentacion) + "' diagnosticos='" + JSON.stringify(jsonDatos[j].diagnosticos) + "' valor='" + jsonDatos[j].valor + "' idprocejson='" + jsonDatos[j].id + "' idorden='" + json.ordenMedica[i].id + "'></td></tr>")    

                      $("#checkboxPersona" + i + "Procedimiento" + j).click(function(e){
                          //console.log($(this).parent().parent()[0].children[2].attributes[0].value)
                          //$(this).parent().parent()[0].remove()
                          //console.log($(this).parent().parent()[0].children[1].outerText)
                          var idTrSelect = $(this).parent().parent()[0].id
                          var idcheckbox = $(this).attr("id")
                          var idprocejson = $(this).attr("idprocejson")
                          var idorden = $(this).attr("idorden")
                          $("#" + $(this).parent().parent()[0].id).css({"background-color":"#0683c9", "color":"#FFFFFF"})
                          var valor = $(this).attr("valor")
                          var descripcion = $(this).parent().parent()[0].children[2].outerText
                          $(this).attr("disabled", "disabled")
                          $("#tbodyResumen").append("<tr><td>" + descripcion + "</td><td><input type='text' id='cantidad" + $("#tbodyResumen > tr").length + "' class='form-control' onKeyUp='format(this)' value='1' ind='" + $("#tbodyResumen > tr").length + "'/></td><td><input type='text' id='valor" + $("#tbodyResumen > tr").length + "' class='form-control' onKeyUp='format(this)' value='" + valor + "' ind='" + $("#tbodyResumen > tr").length + "'/></td><td>" + valor + "</td><td><textarea id='textareaObservacion" + $("#tbodyResumen > tr").length + "' class='form-control' ind='" + $("#tbodyResumen > tr").length + "'></textarea></td><td><button id='borrar" + $("#tbodyResumen > tr").length + "' class='btn btn-danger' indTrOr='" + idTrSelect + "' idCheckbox='" + idcheckbox + "' idprocejson='" + idprocejson + "' idorden='" + idorden + "'>X</button></td></tr>")

                          var valorTotal = 0;
                          $("#tbodyResumen > tr").each(function(index) {
                            valorTotal += parseInt($(this)[0].children[3].outerText)
                            $("#txtValorTotal").val(valorTotal)
                          });

                          if($("#tbodyResumen > tr").length > 0){
                            $("#btnGenerarFactura").removeAttr("disabled")
                          }else{
                            $("#btnGenerarFactura").attr("disabled", "disabled")
                          }

                          $("#borrar" + ($("#tbodyResumen > tr").length - 1)).click(function(e) {
                            var confirmar = window.confirm("¿Desea borrar esta fila?")
                            if(confirmar){
                              $(this).parent().parent()[0].remove()
                              $("#" + $(this).attr("indTrOr")).css({"background-color":"#FFFFFF", "color":"#000000"})
                              $("#" + $(this).attr("idCheckbox")).removeAttr("checked")
                              $("#" + $(this).attr("idCheckbox")).removeAttr("disabled")

                              if($("#tbodyResumen > tr").length > 0){
                                $("#btnGenerarFactura").removeAttr("disabled")
                              }else{
                                $("#btnGenerarFactura").attr("disabled", "disabled")
                              }
                            }
                          })

                          $("#cantidad" + ($("#tbodyResumen > tr").length - 1)).keyup(function(e) {
                            var cantidad = $(this).val()
                            var ind = $(this).attr("ind")
                            var precio = $("#valor" + ind).val()
                            //console.log($(this).parent().parent()[0].children[3].innerHTML)

                            if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){
                              var total = (parseInt(cantidad)) * (parseInt(precio))
                              $(this).parent().parent()[0].children[3].innerHTML = total 

                              var valorTotal = 0;
                              $("#tbodyResumen > tr").each(function(index) {
                                valorTotal += parseInt($(this)[0].children[3].outerText)
                                $("#txtValorTotal").val(valorTotal)
                              });
                            }else{
                              $(this).parent().parent()[0].children[3].innerHTML = ""
                            }
                            
                            //$("#" + $(this).parent().parent()[0].id).val();
                          });

                          $("#valor" + ($("#tbodyResumen > tr").length - 1)).keyup(function(e) {
                            var precio = $(this).val()
                            var ind = $(this).attr("ind")
                            var cantidad = $("#cantidad" + ind).val()

                            if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){
                              var total = (parseInt(cantidad)) * (parseInt(precio))
                              $(this).parent().parent()[0].children[3].innerHTML = total 

                              var valorTotal = 0;
                              $("#tbodyResumen > tr").each(function(index) {
                                valorTotal += parseInt($(this)[0].children[3].outerText)
                                $("#txtValorTotal").val(valorTotal)
                              });
                            }else{
                              $(this).parent().parent()[0].children[3].innerHTML = ""
                            }
                          });

                          
                          /*var datos = {
                            idtipo: identificacion
                          }   
                          //$("button").addClass("disabled")
                          $.post("findDatos_Procedimiento_Radiologia.php", datos)
                          .done(function( result ) {//console.log(result) 
                          }); */  
                      });
                    }
                  }
                }                
               }else{
                $("#txtNombre").val("")
                $("#txtDireccion").val("")
                $("#txtTelefono").val("")
                $("#txtEmail").val("")
               }
            });
          
          
        }
    });
  
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmFind").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmFind").validate({
          rules: {
            txtIdentificacion:"required"
          },
          messages: {
            txtIdentificacion: "Por favor ingrese un numero de documento"
          }
        });
        
    })();

    $("#btnGenerarFactura").click(function() {
      var identificacion = $("#txtIdentificacion").val()
      var total = $("#txtValorTotal").val()
      var jsonDetalle = [];

     
      $("#tbodyResumen > tr").each(function(ind) {
        var descripcion = $(this)[0].children[0].outerText
        var cantidad = $("#cantidad" + ind).val()
        var precio = $("#valor" + ind).val()
        var observacionRecepcion = $("#textareaObservacion" + ind).val()
        var subtotal = $(this)[0].children[3].outerText
        var observacionInicial = $("#" + $("#borrar" + ind).attr("indtror"))[0].children[4].outerText
        var idprocejson = $("#borrar" + ind).attr("idprocejson")
        var idorden = $("#borrar" + ind).attr("idorden")

        jsonDetalle[jsonDetalle.length] = {
          descripcion:descripcion,
          cantidad:cantidad,
          precio:precio,
          observacionInicial:observacionInicial,
          observacionRecepcion:observacionRecepcion,
          subtotal:subtotal,
          idprocejson:idprocejson,
          idorden:idorden
        }
      });
      
      console.log("<?php echo $_SESSION['userid'];?>")
      var datos = {
        documento: identificacion,
        user_radiologia: "<?php echo $_SESSION['userid'];?>",
        total: total,
        jsonDetalle: JSON.stringify(jsonDetalle)
      }   
      $.post("addfacturaRadiologia.php", datos)
      .done(function( result ) {console.log(result) 
        $("input").val("")
        $("#tbodyOrdenMedica").html("")
        $("#tbodyResumen").html("")
        $("#btnGenerarFactura").attr("disabled", "disabled")

        var sOptions = 'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes, statusbar=no';
        sOptions = sOptions + ',width=' + (screen.availWidth).toString();
        sOptions = sOptions + ',height=' + (screen.availHeight - 10).toString();
        sOptions = sOptions + ',screenX=-10,screenY=0,left=-10,top=0';

        window.open("pdfFacturaRadiologia.php?identificacion=" + identificacion + "&idFactura=" + $.trim(result), "Factura", sOptions);
        //window.location.href = "home.php";
      }); 
      
    })
</script>
</body>
</html>