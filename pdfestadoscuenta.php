<?php error_reporting(E_ALL);
include('includes/loader.php'); 
  //directorio tipo utf-8
  header('Content-Type: text/html; charset=UTF-8');
  //error_reporting(0);
  ob_start();
  /* Empezamos la sesión */
  session_start();
  /* Si no hay una sesión creada, redireccionar al index. */
  if(empty($_SESSION['userid'])) { // Recuerda usar corchetes.
    header('Location: login.php');
  } // Recuerda usar corchetes

  require_once 'db_connect.php';
  // connecting to db
  $db = new DB_CONNECT();

  $idpaciente= $_GET['usuario'];
  //paso session de clinica
  $clinicaconsultada = $_SESSION['clinicaperfiluno']; 

  $result_paciente = mysql_query("SELECT * FROM users where document__number='$idpaciente' AND `clinica`='$clinicaconsultada'");   
  while($row = mysql_fetch_array($result_paciente)){
    $apellidos = $row["last_name"];
    $nombres = $row["first_name"];
    $nombre_completo = $nombres . " " . $apellidos;
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Detalle Pagos</title>
    <style type="text/css">
      th{
        background-color: #0683c9;
        font-size: 11px;
        font-family: Arial;
        padding: 10px;
      }
      table{
        font-family: Arial; 
        border-collapse: collapse;
        
      }
      td{
        font-size: 11px;
        font-family: Arial;
        text-align: center;
        height: 30px;
      }

      .tdresumen{
        color: #fff;
    height: 0px;
    padding: 0 50px 0px 7px;
    margin: 0;
    text-align: left;
      }

      
    </style>
  </head>
  <body>
    <header>
      <img src="http://190.60.211.17/hco/images/fondo2.png" width="100%" height="50px">
    </header>
    <div class="container-fluid">
      <div class="row">
        </br>

        <div class="col-md-2">  
        </div>

        <div class="col-md-10">
          <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
          
          <div class="container-fluid">
            </br>
            </br>
            </br>
            </br>
            <div class="row"> 
              <h4 align="center"><font color="#0683c9">Estado de Cuenta Pagos</font></h4>

              <div class="col-md-12">
                <div class="col-md-8">
                </div>
                <div class="col-md-4">
                </div>
              </div>
              
              <div class="col-md-12">
                </br>
                <div class="col-md-2">
                </div>
                
                <div class="col-md-10">
                  <table width="100%" cellspacing="2" cellpadding="2" border="0">
                    <tr align="left">
                     
                      <td>
                       
                      </td>
                    </tr>
                    <tr align="left">
                      <td>
                      <!--<label><font color="#0683c9">Identificación: </font><?php echo $idpaciente;?></label><br>-->
                        <label><font color="#0683c9">Nombre del Paciente: </font><?= $nombre_completo;?></label><br>
                        <label><font color="#0683c9">Fecha Reporte: </font><?= date("Y-m-d");?></label>  
                      </td>  
                    </tr>
                  </table>                  

                  <div>
                    <div>
                    <?php 
                    $sqlcabecera = "SELECT DISTINCT 
                                    `presupuestos`.*
                                  FROM
                                    `presupuestos`
                                    INNER JOIN `pagoscuotas` ON (`presupuestos`.`id` = `pagoscuotas`.`presupuesto`)
                                  WHERE
                                    `presupuestos`.`documento` = '{$idpaciente}' AND 
                                    `presupuestos`.`clinica` = '{$clinicaconsultada}' AND 
                                    `presupuestos`.`estado` = '1'";

                                  
                    $rescabeceras = mysql_query($sqlcabecera);
                    while($rowcabe = mysql_fetch_array($rescabeceras)){ 
                         $saldo = 0;
                         $val_pagado = 0; 
                         $adicionales = 0;
                    ?>
                      <table width='700px'>
                        <tr>
                          <th id='colores'><font color='white'>Procedimiento</font></th>
                          <th id='colores'><font color='white'>Descripción</font></th>                         
                          <th id='colores'><font color='white'>Valor Cuota</font></th>
                          <th id='colores'><font color='white'>Fecha </font></th>
                          <th id='colores'><font color='white'>Valor pagado</font></th>
                          <th id='colores'><font color='white'>Saldo de Cuota</font></th>
                         
                        </tr>                 
                        <?php
                        //SELECT presupuestos.*, pagoscuotas.`fecha` AS fecha_estimada, pagoscuotas.`valorcuota`, pagoscuotas.`cancelado`, pagoscuotas.`tipo` FROM presupuestos INNER JOIN pagoscuotas ON presupuestos.id = pagoscuotas.presupuesto where presupuestos.documento='$idpaciente' and presupuestos.clinica='$clinicaconsultada' and presupuestos.estado='1'
                    
                          $sql = mysql_query("SELECT DISTINCT 
                                              pagoscuotas.fecha,
                                              pagos.procedimiento,
                                              presupuestos.documento,
                                              pagoscuotas.estado,
                                              pagoscuotas.valorcuota,
                                              presupuestos.valor,
                                              pagoscuotas.cancelado
                                            FROM
                                              pagoscuotas,
                                              pagos,
                                              presupuestos
                                            WHERE
                                              pagos.idpaciente = '{$idpaciente}' AND 
                                              pagos.procedimiento = '{$rowcabe["nombreprocedimiento"]}' AND 
                                              presupuestos.id = pagoscuotas.presupuesto AND 
                                              presupuestos.documento = pagos.idpaciente AND 
                                              pagos.clinica = '{$clinicaconsultada}' AND 
                                              pagoscuotas.clinica = pagos.clinica AND 
                                              presupuestos.estado = 1 AND 
                                              pagoscuotas.cancelado <> 0 AND 
                                              presupuestos.nombreprocedimiento = pagos.procedimiento");
                         

                          while($row = mysql_fetch_array($sql)){
                            $idpresupuesto = $row["id"];
                            $descripcion = $row["observaciones"];
                            $valprocedi = $row["valor"];
                            $nombreprocedimiento = $row["procedimiento"];
                            $row['valor'] = str_replace(array(',', '.'), '', $row['valor']);
                            $valor += $row["valorpago"];
                            $valor_cuota = $row["valorcuota"];
                            $fecha_estimada = $row["fecha"];  
                            $row['cancelado'] = str_replace(array(',', '.'), '', $row['cancelado']);
                            $cancelado = $row["cancelado"]; 
                            $tipo = $row["tipo"];    
                            if($tipo == "cuotainicial"){
                              $tipo = "Cuota Inicial"; 
                            }else{
                              $tipo = "Cuota"; 
                            }
                            $saldo = $valor_cuota - $cancelado;
                            $val_pagado += $cancelado;
                        ?>

                        <tr>
                          <td align="center"><?php echo $nombreprocedimiento;?></td>
                          <td align="center"><?php echo $descripcion;?></td>
                          <td align="center"><?= number_format($valor_cuota,0);?></td>
                          <td align="center"><?= $fecha_estimada;?></td>                         
                          <td align="center"><?= number_format($cancelado,0);?></td>
                          <td align="center"><?= number_format($saldo,0);?></td>                         
                        </tr>
                        <?php
                          }

                          $adicionales = "SELECT 
                                          `evolutions`.`id`,
                                          `evolutions`.`_date`,
                                          `evolutions`.`_oid`,
                                          `evolutions`.`tipodocumento`,
                                          `evolutions`.`comments`,
                                          `evolutions`.`cups`,
                                          `evolutions`.`deleted`,
                                          `evolutions`.`kind`,
                                          `evolutions`.`__v`,
                                          `evolutions`.`type`,
                                          `evolutions`.`cause`,
                                          `evolutions`.`diagnosticoprincipal`,
                                          `evolutions`.`nuevodiagnostico`,
                                          `evolutions`.`abono`,
                                          `evolutions`.`procedimiento`,
                                          `evolutions`.`especialista`,
                                          `evolutions`.`clinica`,
                                          `evolutions`.`consecutivo`,
                                          `evolutions`.`acompanante`,
                                          `evolutions`.`tratamiento`
                                          FROM
                                          `evolutions`
                                          WHERE
                                          `evolutions`.`tratamiento` = '{$nombreprocedimiento}' AND 
                                          `evolutions`.`clinica` = '{$clinicaconsultada}' AND 
                                          `evolutions`.`_oid` = '{$idpaciente}' ";

                          $adicionales2 = mysql_query($adicionales);

                          while($list_adic = mysql_fetch_array($adicionales2)){
                        ?>
                        <tr>
                          <td align="center"><?php $cadena1 = str_replace("Evolucion", "Consulta dia", $list_adic['procedimiento']);
                              $cadena2 = str_replace($documento."."," ",$cadena1);
                              echo $cadena2; ?></td>
                          <td align="center"><?php echo $list_adic['comments'];?></td>
                          <td align="center"><?php echo number_format($list_adic['abono'],0);?></td>
                          <td align="center"><?php echo $list_adic['_date'];?></td>                         
                          <td align="center"><?= number_format($list_adic['abono'],0);?></td>
                          <td align="center"><?= number_format($list_adic['abono'],0);?></td>                         
                        </tr>
                          <?php $adicionales += $list_adic['abono'];}?>
                        <br>
                        <table width='0'>
                        <tbody><tr>
                          <th class="tdresumen" id='colores' style="color:#fff">Valor procedimiento</th> <td style="width: 100px;">$<?php echo number_format($valprocedi) ?></td> 
                        </tr>
                        <tr>
                          <th class="tdresumen" id='colores' style="color:#fff">Valor adicionales</th> <td style="width: 100px;">$<?php echo number_format($adicionales) ?></td> 
                        </tr>
                        <tr>
                          <th class="tdresumen" id='colores' style="color:#fff">Valor abono</th> <td style="width: 100px;">$<?php echo number_format($val_pagado) ?></td> 
                        </tr>
                        <tr>
                          <th class="tdresumen" id='colores' style="color:#fff">Saldo</th> <td style="width: 100px;"><?php echo number_format($saldo) ?></td> 
                        </tr>
                      </tbody></table>
                      </table><br>

                        <?php }?>

                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>            
    </br>
    </br>
    </br>
    </br>
    </br> 
  </body>
</html>
<?php 
require_once("dompdf/dompdf_config.inc.php");
  
  $dompdf = new DOMPDF();
  $dompdf->load_html(ob_get_clean());
  $dompdf->set_paper ('a5','landscape'); 
  $dompdf->render();
  $pdf = $dompdf->output();

  $dompdf->stream('estadocuentapagos.pdf',array('Attachment'=>0));

?>