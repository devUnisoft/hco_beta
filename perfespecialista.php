<style type="text/css">
    .img-circle {
        border-radius: 100%;
        background-color: #ccc;
        overflow: hidden;
        padding: 3px;
    }

</style>
<div class="patient">
        <div class="img">
        <?php 
            if(empty($image_url)){
        ?>
        <img  class="img-circle" src="images/paciente.png" height="50%" width="50%">
        <?php 
            }else{
        ?>        
         <img  class="img-circle" src="<?php echo $image_url ?>" height="50%" width="50%">
        <?php
            }
        ?> 
        </div>
        <div class="detail-pata"></div>
        <div class="detail">
            <div class="text-center ng-binding">
                <b class="ng-binding"><?php echo $firstname ?> <?php echo $lastname ?></b><br>
                <?php echo $document__document_type__oid ?>: <?php echo $documentnumber ?><br>
                <?php echo $edad ?> años
            </div>
            <br>
            <br>
            <div class="ng-binding">
                <?php echo $genero ?><br>
                <?php echo $birthdate ?><br>
                <?php echo $lugarnacimiento ?>, <?php echo $lugarresidencia ?>
            </div>
            <?php
                       //paso documento a session para uso del path imagenes en scan.php
                      $_SESSION["documentnumber"]=$documentnumber;
                      ?>
            <div class="text-center edit">
                <a href="modificarodontologoadm.php?usuario=<?= $id ?>"><img src="images/editar.png" title="Editar Odontologo"  height="20px" width="20px"></a>    
            </div>
        </div>
        </br>
        <div class="text-center foto">
             <center><a href="modificarodontologoadm.php?usuario=<?= $id ?>"><img src="images/foto1.png" title="Editar Imagen"  height="50px" width="50px"></a></center>        
             <center>Foto</center>
        </div>
</div> 