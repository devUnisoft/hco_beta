<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Menu
{
	
	function __construct()
	{
		
	}

	public function headerPage($nombre){
		$menu = '<div class="row" >
			        <div class="col-md-12">
			            <div class="col-md-4" align="left" style="margin: 0">
			                    <h3 style="color: #FFF; font-weight: normal; line-height: 1; margin-left: 0px">CENTROS RADIOLOGICOS</h3>
			            </div>
			            <div class="col-md-4">
			                    <p></p>
			                    <center><img id="imagenheadersuperior" src="' . base_url() . 'images/LOGO SW CENTRAL.png"  height="70px" width="120px"></center>
			            </div>
			            <div class="col-md-4" style="margin-top: 10px">' .
			            	$this->menusuarioradiologia($nombre) . 	
			            '</div>
        			</div>
				</div>';		
		return $menu;
	}

	public function menusuarioradiologia($nombre){
		$menu = '<div class="col-md-7">
				    </br>
				    <span class="pull-right"><font color="white">' . $nombre . '</font></span>
				  </div>

				  <div class="col-md-4">  
				    <img  src="' . base_url() . 'images/btn_perfil-off.png"  height="40%" width="40%" style="margin-top: 10px">
				  </div>';		
		return $menu;
	}

	public function footerPage($pfil, $page){
		$menu = '';
		switch ($pfil) {
			case 'Asistente':
				switch ($page) {
					case 'CIERRECAJA':
						$menu = '<div id="footer">
					        <div align="center">

					          <a href="' . base_url() . 'index.php/user_radiologia/home"><img src="' . base_url() . 'images/Recepcionista_N.png" title="Ayuda" width="75px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">

					          <a href="' . base_url() . 'index.php/user_radiologia/reporteCierreCaja"><img src="' . base_url() . 'images/reporteCajaAzul.png" title="" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/entregaResultados"><img src="' . base_url() . 'images/Entrega Resultados _N.png" title="" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/logoutUser"><img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" width="50px" style="margin-top: -5px"></a>
					        
					        </div> 
					      </div>';
						break;		

					case 'ENTREGARIMG':
						$menu = '<div id="footer">
					        <div align="center">

					          <a href="' . base_url() . 'index.php/user_radiologia/home"><img src="' . base_url() . 'images/Recepcionista_N.png" title="Ayuda" width="75px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">

					          <a href="' . base_url() . 'index.php/user_radiologia/reporteCierreCaja"><img src="' . base_url() . 'images/reporteCajaNegro.png" title="Ayuda" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/entregaResultados"><img src="' . base_url() . 'images/Entrega Resultados _A.png" title="" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/logoutUser"><img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" width="50px" style="margin-top: -5px"></a>
					        
					        </div> 
					      </div>';
						break;					
					
					case 'RECEPCION':
						$menu = '<div id="footer">
					        <div align="center">
					          <a href="' . base_url() . 'index.php/user_radiologia/home"><img src="' . base_url() . 'images/Recepcionista_A.png" title="Ayuda" width="75px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">

					          <a href="' . base_url() . 'index.php/user_radiologia/reporteCierreCaja"><img src="' . base_url() . 'images/reporteCajaNegro.png" title="Ayuda" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/entregaResultados"><img src="' . base_url() . 'images/Entrega Resultados _N.png" title="" width="85px" style="margin-top: -5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/logoutUser"><img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" width="50px" style="margin-top: -5px"></a>
					        
					        </div> 
					      </div>';
						break;		
				}
				
				break;

			case 'Administrador':
				$menu = '<div class="row" align="center"> 
					        <a href="' . base_url() . 'index.php/user_radiologia/home">
					          <img src="' . base_url() . 'images/btn_inicio.png" title="Salir" height="50px" width="50px" style="margin-top: -20px">
					          <label style="position: absolute; margin-top: 55px; margin-left: -50px;">Inicio</label>
					        </a>
					        <a href="' . base_url() . 'index.php/user_radiologia/adminProfesionales" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia1.png" width="80">
					        </a>
					        <a href="' . base_url() . 'index.php/user_radiologia/adminAuxiliares" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia2.png" width="80">
					        </a>
					        <a href="' . base_url() . 'index.php/ordenes_medicas/anulacionFactura" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia3.png" width="80">
					        </a>
					        <a href="' . base_url() . 'index.php/ordenes_medicas/reporteCierreCaja" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia4.png" width="80">
					        </a>
					        <a href="' . base_url() . 'index.php/ordenes_medicas/adminProcedimientos" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia5.png" width="80">
					        </a>  
					        <a href="' . base_url() . 'index.php/ordenes_medicas/adminConvenios" class="menu">
					          <img src="' . base_url() . 'img/icon_radiologia6.png" width="80">
					        </a>  
					        <a href="' . base_url() . 'index.php/user_radiologia/entregaResultados" class="menu">
					          <img src="' . base_url() . 'images/Entrega Resultados.png" width="80">
					        </a>  
					        <a href="' . base_url() . 'index.php/user_radiologia/logoutUser">
					          <img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" height="65px" width="65px">
					        </a>  
				      	</div>';		
				break;

			case 'Profesional':
				switch ($page) {
					case 'TURNERO':
						$menu = '<div id="footer">
					        <div align="center">
					          <a href="' . base_url() . 'index.php/user_radiologia/home"><img src="' . base_url() . 'images/turnero_A.png" title="Ayuda" width="75px" style="margin-top: 5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">

					          <a href="' . base_url() . 'index.php/ordenes_medicas/updateImagenes"><img src="' . base_url() . 'images/reporteGeneralNegro.png" title="Ayuda" width="75px" style="margin-top: 5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/logoutUser"><img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" width="55px" style="margin-top: -10px"></a>
					        
					        </div> 
					      </div>';
						break;
					case 'ACTUALIZAR':
						$menu = '<div id="footer">
					        <div align="center">
					          <a href="' . base_url() . 'index.php/user_radiologia/home"><img src="' . base_url() . 'images/Turnero_consultorio _N.png" title="Ayuda" width="75px" style="margin-top: 5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">

					          <a href="' . base_url() . 'index.php/ordenes_medicas/updateImagenes"><img src="' . base_url() . 'images/reporteGeneralAzul.png" title="Ayuda" width="75px" style="margin-top: 5px"></a>
					          <img src="' . base_url() . 'images/separador.png" height="50px" width="30px">  

					          <a href="' . base_url() . 'index.php/user_radiologia/logoutUser"><img id="salir" src="' . base_url() . 'images/Salir_g.png" title="Salir" width="55px" style="margin-top: -10px"></a>
					        
					        </div> 
					      </div>';
						break;
				}
				
				break;
		}
		
		return $menu;
	}


}