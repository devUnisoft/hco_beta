<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adhesivos extends CI_Controller {

	public function Adhesivos_pdf() {

		
		$this->load->model('adhesivos_model');

		$id_centro = $this->session->userdata('IDCRInternoCR');
		$centro_radiologico = $this->adhesivos_model->get_centro_radiologico($id_centro);
		$paciente = $this->adhesivos_model->getUSer();
		
		if(!count($paciente) > 0) {
			echo "No hay información del paciente";
			return;
		}
			
		// var_dump($paciente);	
		$paciente = $paciente[0];

		if(count($centro_radiologico) > 0) {
			$centro_radiologico = $centro_radiologico[0];
		}else {
			return;
		}
		
		// Include the main TCPDF library (search for installation path).
		require_once('/var/www/html/hcobeta/CentrosRadiologicos/TCPDF/tcpdf.php');

		// create new PDF document
		$pdf = new TCPDF('p', 'mm', array('210','297'), true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 002');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set margins
		$pdf->SetMargins(0, 0, 0, 0);

		// set auto page breaks
		$pdf->SetAutoPageBreak(FALSE, 0);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}


		// set font
		$pdf->SetFont('times', 'BI', 20);

		// add a page
		$pdf->AddPage();


		$_x30 = [
			'width' => 199,
			'height' => 76,
			'rows' => 33,
			'cols' => 3,
			'header' => '',
			'breaks' => '',
			'hr' => '<br>',
			'css' => '
				.title {font-size: 10px; }
				.sub-title {font-size: 9px; }
			'
		];

		$_x12 = [
			'width' => 297,
			'height' => 139,
			'rows' => 12,
			'cols' => 2,
			'hr' => '<br><br>',
			'header' => '<br>',
			'breaks' => '<br><br>',
			'css' => '
				span {font-size: 12px; }
				small {display: block; padding-top: 10px; font-size: 9px; }
				.title {font-size: 20px; }
				.sub-title {font-size: 15px; }
			'
		];

		$_x08 = [
			'width' => 297,
			'height' => 209,
			'rows' => 8,
			'hr' => '<br><br><br>',
			'cols' => 2,
			'header' => '<br><br><br>',
			'breaks' => '<br><br><br>',
			'css' => '
				span {font-size: 12px; }
				small {display: block; padding-top: 10px; font-size: 9px; }
				.title {font-size: 20px; }
				.sub-title {font-size: 15px; }
			'
		];

		$config = false;

		if($this->input->post('sheetSelect') == 33) {
			$config = $_x30;
		}else if($this->input->post('sheetSelect') == 12) {
			$config = $_x12;
		}else if($this->input->post('sheetSelect') == 8) {
			$config = $_x08;
		}

		$table_cel = '';

		for ($i=0; $i < $config['rows']; $i++) {
			
			if((($i % $config['cols']) == 0) || $i == 0) {
				$table_cel .= "<tr>";
			}

			$visibility = (in_array($i, json_decode($this->input->post('positions')))); 

			if($visibility != false) {
		  	$table_cel .= '
		  		<td height="'.$config["height"].'" width="'.$config["width"].'" align="center">
		  			'.$config["header"].'
						<span class="uppercase title">'.$centro_radiologico->name.'</span><br>
						<span class="capitalize sub-title">Tel. '.$centro_radiologico->telefonos.'</span><br>
						<span class="uppercase sub-title">'.$centro_radiologico->address.'</span>
						'.$config['hr'].'
						<span>'.$paciente->first_name.' '.$paciente->last_name.'</span><br>
						<span>Tel. '.$paciente->telefono.' '.$paciente->acudientetelefono.'</span>
						'.$config['breaks'].'
						<div><small>'.date("d-m-Y").'</small></div>
		  		</td>
		  	';
			}else {
				$table_cel .= '<td height="'.$config["height"].'" width="'.$config["width"].'" align="center"></td>';
			}

			
			if((($i + 1) % $config['cols']) == 0) {
				$table_cel .= "</tr>";
			}
		}

		$html = '
			<!-- EXAMPLE OF CSS STYLE -->
			<style>
			    table.first {
			        color: #003300;
			        font-family: helvetica;
			        font-size: 8pt;
			        background-color: #ffffff;
			    }
			    td {
			       	border: 1px dashed #eeeeee;
			        background-color: #ffffff;
			        max-height:40px; 
			        overflow:hidden;
			    }
			    .lowercase {
			        text-transform: lowercase;
			    }
			    .uppercase {
			        text-transform: uppercase;
			    }
			    .capitalize {
			        text-transform: capitalize;
			    }
			    '.$config['css'].'
			</style>
			<table class="first" cellpadding="4" cellspacing="1">
				'.$table_cel.'
			</table>
		';
		
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		$pdf->Output('example_001.pdf', 'I');
	}

}