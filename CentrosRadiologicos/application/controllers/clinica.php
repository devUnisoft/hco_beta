<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clinica extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('ordenes_medicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('tipo_procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('factura_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('centro_radiologicos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('diagnosticos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('georeferences_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('turnos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('consultorios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('clinicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('convenios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('grupos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('clinica_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->helper('form');//Cargar el helper de formularios
		$this->load->library('email');//Cargar la libreria de email
	}

	/** VER PAGINAS **/
	public function index()	{
	}

	public function add_clinicas()	{
		$razonsocial = $_POST['razonsocial']; 
		$id = $_POST['id']; 
	    $nit = $_POST['nit']; 
	    $direccion = $_POST['direccion']; 
	    $telefonos = $_POST['telefonos']; 
	    $email = $_POST['email'];
	    $codigosecretaria = $_POST['codigosecretaria']; 
	    $codigoentidad = $_POST['codigoentidad'];
	    $codigodelprestador = $_POST['codigodelprestador'];
	    $codigoadministradora = $_POST['codigoadministradora'];
	    $nombreadministradora = $_POST['nombreadministradora'];
	    $numerofactura = $_POST['numerofactura'];
	    $numeroconsecutivo = $_POST['numeroconsecutivo'];
	    $personacontacto = $_POST['personacontacto'];
	    $envia = $_POST['envia'];
	    $numero_resolucion = $_POST['numero_resolucion'];
	    $fecha = $_POST['fecha'];
	    $rango = $_POST['rango'];
	    $file = "";
		$url = "";
		$uidFoto = uniqid();

	    if($_POST['original'] != "si"){
			$img = $_POST['foto'];
			$img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace('data:image/jpg;base64,', '', $img);
			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace('data:image/gif;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$file = getcwd() . "/uploads/" . $uidFoto . '.png';
			$url = "uploads/" . $uidFoto . '.png';
			$success = file_put_contents($file, $data);

			chmod($file, 0755);
		}else{
			$url = $_POST['foto'];
		}

		$array = array(
			'id' => $id,
			'razonsocial' => $razonsocial,
			'logo' => $url,
			'nit' => $nit,
			'direccion' => $direccion,
			'telefonos' => $telefonos,
			'email' => $email,
			'codigosecretaria' => $codigosecretaria,
			'codigoentidad' => $codigoentidad,
			'codigodelprestador' => $codigodelprestador,
			'codigoadministradora' => $codigoadministradora,
			'nombreadministradora' => $nombreadministradora,
			'numerofactura' => $numerofactura,
			'numeroconsecutivo' => $numeroconsecutivo,
			'envia' => $envia,
			'personacontacto' => $personacontacto,
			'numero_resolucion' => $numero_resolucion,
			'fecha' => $fecha,
			'rango' => $rango);
		$this->clinica_model->update($array);
	}	

    //funcion para ver parametros de clinicas
	public function verClinicas()	{
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data['titulo'] = "Parametros Clinica";//Titulo de la pagina, se lo envio al archivo donde esta el header	
		$this->load->library('menu');
		$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
		$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");		
		$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
		$this->load->view('admin/adminClinica');	
	}
    //funcion para ver fechas rips usuarios
	public function verRips()	{
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data['titulo'] = "Rips";//Titulo de la pagina, se lo envio al archivo donde esta el header	
		$this->load->library('menu');
		$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
		$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");		
		$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
		$this->load->view('admin/rusuarioscentro');	
	}
	//funcion para generar rips
	public function generarRips1(){
		//recibo fechas
		$fechainicial =  $_REQUEST["f1"];
		$fechafinal = $_REQUEST["f2"];
		$clinica = $_REQUEST["clinica"];
		$numerofactura = $_REQUEST["numerofactura"];
		//cargo vistas
		$this->load->view('admin/ripsusuarioscentros');
		//result rip af
	}

	//funcion para generar rips
	public function generarRips2(){
		//recibo fechas
		$fechainicial =  $_REQUEST["f1"];
		$fechafinal = $_REQUEST["f2"];
		$clinica = $_REQUEST["clinica"];
		$numerofactura = $_REQUEST["numerofactura"];
		//cargo vistas
		$this->load->view('admin/ripsafcentros');
		//result rip af
	}

	//funcion para generar rips
	public function generarRips3(){
		//recibo fechas
		$fechainicial =  $_REQUEST["f1"];
		$fechafinal = $_REQUEST["f2"];
		$clinica = $_REQUEST["clinica"];
		$numerofactura = $_REQUEST["numerofactura"];
		//cargo vistas
		$this->load->view('admin/ripsadcentros');
		//result rip af
	}

	//funcion para generar rips
	public function generarRips4(){
		//recibo fechas
		$fechainicial =  $_REQUEST["f1"];
		$fechafinal = $_REQUEST["f2"];
		$clinica = $_REQUEST["clinica"];
		$numerofactura = $_REQUEST["numerofactura"];
		//cargo vistas
		$this->load->view('admin/ripsapcentros');
		//result rip af
	}
	//funcion para generar rips
	public function generarRips5(){
		//recibo fechas
		$fechainicial =  $_REQUEST["f1"];
		$fechafinal = $_REQUEST["f2"];
		$clinica = $_REQUEST["clinica"];
		$numerofactura = $_REQUEST["numerofactura"];
		//cargo vistas
		$this->load->view('admin/ripsctcentros');
		//result rip af
	}


	


		



		

	

}