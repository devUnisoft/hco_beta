<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procedimientos extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('ordenes_medicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('presentacion_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('tipo_procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('factura_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('centro_radiologicos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('diagnosticos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('georeferences_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('consultorios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('grupos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->helper('form');//Cargar el helper de formularios
		$this->load->library('email');//Cargar la libreria de email
	}

	/** VER PAGINAS **/
	
	

	/** WEBSERVICES **/	

	public function listarGrupos() {	
		$data = $this->grupos_model->listar();
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function listarTipos() {	
	    $grupo = $_POST['grupo']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->tipo_procedimientos_model->getGrupo($grupo, $IDCRInternoCR);
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function listarPresentacion() {	
	    $idtipo = $_POST['id_tipo']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->presentacion_model->getTipo($idtipo, $IDCRInternoCR);
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function listarDiagnosticos() {	
	    $idpresentacion = $_POST['id_presentacion']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->diagnosticos_model->getPresentacion($idpresentacion, $IDCRInternoCR);
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}
}