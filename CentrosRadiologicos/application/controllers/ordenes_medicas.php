<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('fpdf/fpdf.php');	
require_once('TCPDF/tcpdf.php');

class Ordenes_medicas extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('ordenes_medicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('tipo_procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('diagnosticos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('factura_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('centro_radiologicos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('consultorios_radiologicos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd		
		$this->load->model('turnos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('imagenes_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd		
		$this->load->model('tags_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('grupos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('clinicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('presentacion_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('convenios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('pagos_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->helper('form');//Cargar el helper de formularios
		$this->load->library('email');//Cargar la libreria de email
	}

	/** VER PAGINAS **/
	public function index()	{
		
	}
	public function capturaInformacion(){
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = " . base_url() . "'index.php/user_radiologia/home'</script>";
	    		break;


	    	case 'Administrador':
	    		echo "<script>window.location.href = " . base_url() . "'index.php/user_radiologia/home'</script>";
	    		break;


	    	case 'Profesional':
	    		$data['titulo'] = "CAPTURA DE INFORMACIÓN";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$document = $this->session->userdata('document_Seleccionado'); 
				$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['persona'] = $this->user_radiologia_model->obtenerPacienteDocumento($document);
				$datos = $this->factura_radiologia_model->getFacturasProcesarPaciente($IDCRInternoCR, $document);

				if($datos != null){
					foreach ($datos->result() as $value) {
						$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
		   				if($dataUsuario != null){
							$value->first_name = $dataUsuario->result()[0]->first_name;
							$value->last_name = $dataUsuario->result()[0]->last_name;  				
		   				}else{
		   					$value->first_name = "";
							$value->last_name = "";
		   				}
						$dataProfesional = $this->user_radiologia_model->getProfesional($value->profesional);

						if($dataProfesional != null){
							$dataCon = $this->convenios_model->getConveniosProfes($dataProfesional->result()[0]->clinica, $dataProfesional->result()[0]->id, $IDCRInternoCR);
							$value->profesional = $dataProfesional->result()[0]->id;
							$value->nameProfesional = $dataProfesional->result()[0]->first_name . " " . $dataProfesional->result()[0]->last_name;
							$value->clinica = $dataProfesional->result()[0]->clinica;



							if($dataCon != null){
								$value->convenio = $dataCon->result()[0]->convenio_entrega;
							}else{
								$value->convenio = "";
							}
						}else{
							$value->profesional = "";
							$value->nameProfesional = "";
							$value->clinica = "";
							$value->convenio = "";
						}
					
						
					}
				}else{
					$datos = null;
				}
				$data['facturas'] = $datos;
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "TURNERO");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('profesional/capturaInformacion');
	    		break;

	    }

		
	}

	public function updateImagenes(){
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = " . base_url() . "'index.php/user_radiologia/home'</script>";
	    		break;


	    	case 'Administrador':
	    		echo "<script>window.location.href = " . base_url() . "'index.php/user_radiologia/home'</script>";
	    		break;


	    	case 'Profesional':
	    		$document_Seleccionado = $this->session->userdata('document_Seleccionado');
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$this->factura_radiologia_model->actualizar($document_Seleccionado, $IDCRInternoCR, "false");
				$this->turnos_model->actualizar($document_Seleccionado, $IDCRInternoCR);
			    $this->session->unset_userdata("document_Seleccionado");

	    		$data['titulo'] = "ACTUALIZACION DE IMAGENES";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "ACTUALIZAR");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('profesional/updateImagenes');
	    		break;

	    }

		
	}

	public function adminConvenios(){
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$data['titulo'] = "ADMINISTRACIÓN DE CONVENIOS";//Titulo de la pagina, se lo envio al archivo donde esta el header	
	    		$data['clinicas'] = $this->clinicas_model->listar();			
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/adminConvenios');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	public function turneroEspera()	{
		$data['titulo'] = "TURNERO";//Titulo de la pagina, se lo envio al archivo donde esta el header
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data['turnos'] = $this->turnos_model->listar($IDCRInternoCR);
		$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
		$this->load->view('admin/turneroEspera');
	}

	public function anulacionFactura()	{
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$data['titulo'] = "ANULACION DE PROCESOS REGISTRADOS";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/anulacionFactura');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	

	public function reporteTiempoAtencion()	{
		$profileCR = $this->session->userdata('profileCR');
	    //verifica perfil
	    switch ($profileCR) {
	    	//inicia caso admin
	    	case 'Administrador':
	    	    $IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "REPORTE TIEMPO DE ATENCION";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/reporteTiempoAtencion');
	    		break;
	    }	
	}




	public function ReporteTiemposAtencion()	{
		$profileCR = $this->session->userdata('profileCR');
	    //verifica perfil
	    switch ($profileCR) {
	    	//inicia caso admin
	    	case 'Administrador':
	    	    $IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
	    	    $datosaenviar = $_POST["datos_a_enviar"];
	    	    $this->session->set_userdata('DatosAEnviar', $datosaenviar);
				$data['titulo'] = "REPORTE TIEMPO DE ATENCION";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/ficheroTiempoAtencion');
	    		break;
	    }	
	}


	public function reporteCierreCaja()	{
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "REPORTE CIERRE DE CAJA";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/reporteCierreCaja');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	public function adminProcedimientos() {
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "ADMINISTRACION DE PROCEDIMIENTOS";//Titulo de la pagina, se lo envio al archivo donde esta el header	
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['grupos'] = $this->grupos_model->listar();
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/adminProcedimientos');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	public function reporteProcesosDesarrollados()	{
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "REPORTE DE PROCESOS DESARROLLADOS";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$data['procedimientos'] = $this->procedimientos_model->listar($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/reporteProcesosDesarrollados');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	/** WEBSERVICES **/
	/** WEBSERVICES **/
	public function addfacturaRadiologia()	{
		$turno = 0;
	    $documento = $_POST['documento']; 	    
	    $tipoFactura = $_POST['tipoFactura']; 
	    $estado = "";
	    $user_radiologia = $_POST['user_radiologia']; 
	    $total = $_POST['total']; 
	    $jsonDetalle = json_decode($_POST['jsonDetalle']); 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$turno = $this->factura_radiologia_model->getTurno($IDCRInternoCR);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);

		$consecutivo = $this->factura_radiologia_model->getConsecutivoFactura($IDCRInternoCR);
		if($consecutivo == "0" || $consecutivo == null){
			$consecutivo = $dataCR->result()[0]->numerofactura;
		}else{
			$consecutivo++;
		}
		//Se llama a la funcion de que esta en modelo y el resultado se guarda

		//Se guardan los datos en un array asociativo para pasarlos la funcion del model
		$array = array(
			'documento' => $documento,
			'user_radiologia' => $user_radiologia,
			'consecutivo' => $consecutivo,
			'total' => $total,
			'centro_radiologico' => $IDCRInternoCR,
			'turno' => $turno,
			'tipoFactura' => $tipoFactura,
			'estado' => "ACTIVO",
			'atendido' => 'false');
		$id = $this->factura_radiologia_model->crear($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		$tipoPago = $_POST['tipoPago'];
		$numeroCheque = $_POST['numeroCheque']; 
	    $codigoBanco = $_POST['codigoBanco'];  
	    $numeroCuenta = $_POST['numeroCuenta'];  
	    $fechaCheque = $_POST['fechaCheque'];  
	    $codigoTarjeta = $_POST['codigoTarjeta'];  
	    $tipoTarjeta = $_POST['tipoTarjeta'];  
	    $bancoTarjeta = $_POST['bancoTarjeta'];  
	    $numeroTarjeta = $_POST['numeroTarjeta'];  
	    $documentoOtra = $_POST['documentoOtra']; 
	    $formaPagoOtra = $_POST['formaPagoOtra'];  
	    $codigoOtra = $_POST['codigoOtra'];	    
	    
	    //Se guardan los datos en un array asociativo para pasarlos la funcion del model
		$arrayPago = array(
			'idFactura' => $id,
			'tipopago' => $tipoPago,
			'numerocheque' => $numeroCheque,
			'codigobanco' => $codigoBanco,
			'numerocuenta' => $numeroCuenta,
			'fechacheque' => $fechaCheque,
			'codigoautorizaciont' => $codigoTarjeta,
			'tipotarjeta' => $tipoTarjeta,
			'bancotarjeta' => $bancoTarjeta,
			'numerotarjeta' => $numeroTarjeta,
			'numdocumento' => $documentoOtra,
			'formapago' => $formaPagoOtra,
			'codigoautorizaciono' => $codigoOtra,
			'estado' => "ACTIVO"); 

	    $this->pagos_radiologia_model->crear($arrayPago);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		if(count($jsonDetalle) > 0){
			foreach ($jsonDetalle as $value) {
	            $descripcion = $value->descripcion; 
	            $observacionInicial = $value->observacionInicial; 
	            $observacionRecepcion = $value->observacionRecepcion; 
	            $cantidad = $value->cantidad; 
	            $precio = $value->precio; 
	            $total = $value->subtotal; 
	            $convenio = $value->convenio; 
	            $descuento = $value->descuento; 
	            $idprocejson = $value->idprocejson; 
	            $idorden = $value->idorden; 
	            $procedimiento = $value->procedimiento; 
	            $profesional = $value->profesional; 

	            if(trim($profesional) != ""){
					$dataProfesional = $this->user_radiologia_model->getProfesional($profesional);
					$clinica = $dataProfesional->result()[0]->clinica;

					$dataCovenio = $this->convenios_model->getConveniosProfes($clinica, $dataProfesional->result()[0]->id, $IDCRInternoCR);

					if($dataCovenio != null){
						$convenio = $dataCovenio->result()[0]->convenio_factura;

						switch ($convenio) {
							case 'FACTURAR A LA CLINICA':
								$estado = "FACTURAR CLINICA";
								break;
							case 'FACTURAR AL PROFESIONAL':
								$estado = "FACTURAR PROFESIONAL";
								break;
							case 'FACTURAR AL PACIENTE':
								$estado = "ACTIVO";
								break;
						}
					}else{
						$estado = "ACTIVO";
					}
						
				}else{
					$estado = "ACTIVO";
				}

				$this->factura_radiologia_model->updateEstado($id, $estado);//Se llama a la funcion de que esta en modelo y el resultado se guarda

	            //Se guardan los datos en un array asociativo para pasarlos la funcion del model
				$arrayDetalle = array(
					'id_factura_radiologia' => $id,
					'descripcion' => $descripcion,
					'observacion_inicial' => $observacionInicial,
					'observacion_recepcion' => $observacionRecepcion,
					'idProcedimiento' => $procedimiento,
					'cantidad' => $cantidad,
					'precio' => $precio,
					'total' => $total,
					'convenio' => $convenio,
					'descuento' => $descuento,
					'orden_medica' => $idorden,
					'id_proceso' => $idprocejson,
					'odontologo' => $profesional,
					'estado' => "ACTIVO");
				$this->factura_radiologia_model->crearDetalle($arrayDetalle);//Se llama a la funcion de que esta en modelo y el resultado se guarda
				
				$arrayDetalle = array(
					'id' => $idprocejson,
					'estado' => "FACTURADO");
				$this->ordenes_medicas_model->updateDetalle($arrayDetalle);
	        }
		}
		$array_result = array('id' => $id, "consecutivo" => $consecutivo);
		echo json_encode($array_result);
	}

	public function updateFactura()	{
		$document = $_POST['document']; 
    	$user_radiologia = $_POST['user_radiologia']; 
    	$idFactura = $_POST['idFactura']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$this->session->set_userdata('document_Seleccionado', $document);
		$this->session->set_userdata('idFactura_Seleccionada', $idFactura);

		$this->factura_radiologia_model->actualizar($document, $IDCRInternoCR, "true");
		$data = $this->consultorios_radiologicos_model->get($user_radiologia, $IDCRInternoCR);

		if($data != null){
		 	$idConsultorio = $data->result()[0]->id;
			$array = array(
				'document' => $document,
				'centro_radiologico' => $IDCRInternoCR,
				'consultorio' => $idConsultorio,
				'estado' => 'ACTIVO');
			$this->turnos_model->crear($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		}
		
	}

	public function updateFacturaCancelar()	{
		$document = $_POST['document']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$idFactura_Seleccionada = $this->session->userdata('idFactura_Seleccionada');
		$detalle = $this->session->userdata('detalle_Seleccionado');
		$this->factura_radiologia_model->actualizar($document, $IDCRInternoCR, "false");
		$this->factura_radiologia_model->actualizarCancelarHoraAtencion($detalle, $idFactura_Seleccionada);
		$this->turnos_model->actualizar($document, $IDCRInternoCR);
		echo $document;
	}
	public function quitarTurno()	{
		$document = $_POST['document']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$this->turnos_model->actualizar($document, $IDCRInternoCR);
		echo $document;
	}

	public function iniciarAtencion()	{
		$document = $_POST['document']; 
    	$detalle = $_POST['detalle']; 
    	$idFactura = $_POST['idFactura']; 
		$this->session->set_userdata('detalle_Seleccionado', $detalle);
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$IDUsuarioInterno = $this->session->userdata('UserIDInternoCR');

		$data = $this->factura_radiologia_model->getFacturasPersonas($document);
   		if($data != null){
   			foreach ($data->result() as $value) {
   				$this->factura_radiologia_model->actualizarHoraAtencion($value->id, $IDUsuarioInterno);
   			}
   		}
		
		$this->turnos_model->actualizar($document, $IDCRInternoCR);
		echo $document;		
	}

	public function borrarAtencion()	{
		$detalle = $_POST['detalle']; 
    	$idFactura = $_POST['idFactura']; 
		$this->factura_radiologia_model->actualizarCancelarHoraAtencion($detalle, $idFactura);	
	}

	public function agregarTurno()	{
		$document = $_POST['document']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$this->turnos_model->agregarTurno($document, $IDCRInternoCR);
		echo $document;
	}

	public function addAnulacionRadiologia(){
		$documento = $_POST['documento']; 
	    $user_radiologia = $_POST['user_radiologia'];
	    $observacion = $_POST['observacion']; 
	    $idFactura = $_POST['idFactura']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		
		$array = array(
			'idFactura' => $idFactura,
			'user_radiologia' => $user_radiologia,
			'centro_radiologico' => $IDCRInternoCR,
			'observacion' => $observacion);
		$this->factura_radiologia_model->crearLogAnulacion($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		
		$this->factura_radiologia_model->anular($idFactura);
	}

	public function findreportecierrecaja(){
		$document = $_POST['document']; 
	    $fechaInicial = $_POST['fechaInicial']; 
	    $fechaFinal = $_POST['fechaFinal']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->factura_radiologia_model->reporteCierreCaja($fechaInicial, $fechaFinal, $document, $IDCRInternoCR);
   		if($data != null){
   			foreach ($data->result() as $value) {
	    		$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
   				if($dataUsuario != null){
					$value->first_name = $dataUsuario->result()[0]->first_name;
					$value->last_name = $dataUsuario->result()[0]->last_name;  				
   				}else{
   					$value->first_name = "";
					$value->last_name = "";
   				}
   			}
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function getDatosPagoFactura(){
		$idFactura = $_POST['idFactura']; 
		$data = $this->pagos_radiologia_model->getPago($idFactura);
   		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function getProcedimientosEntregar(){
		$document = $_POST['document']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = null;

		if(trim($document) != ""){
			$data = $this->factura_radiologia_model->procedimientosEntregar($document, $IDCRInternoCR);
		}else{
			$data = $this->factura_radiologia_model->procedimientosEntregarSinDoc($IDCRInternoCR);
		}
		
		
		if($data != null){   			
   			foreach ($data->result() as $value) {

   				$fecha1=strtotime(date("Y") . "-" . date("m") . "-" . date("d"));
				$fecha2=strtotime($value->fechaEntrega);
				if($fecha1 >= $fecha2){
					$value->estadoEntrega = "LISTO";
				}else{
					$value->estadoEntrega = "NOLISTO";
				}

				$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
   				if($dataUsuario != null){
					$value->first_name = $dataUsuario->result()[0]->first_name;
					$value->last_name = $dataUsuario->result()[0]->last_name;  				
   				}else{
   					$value->first_name = "";
					$value->last_name = "";
   				}

				$dataProfesional = $this->user_radiologia_model->getProfesional($value->odontologo);

				if($dataProfesional != null){
					$dataCon = $this->convenios_model->getConveniosProfes($dataProfesional->result()[0]->clinica, $dataProfesional->result()[0]->id, $IDCRInternoCR);
					$value->profesional = $dataProfesional->result()[0]->id;
					$value->nameProfesional = $dataProfesional->result()[0]->first_name . " " . $dataProfesional->result()[0]->last_name;
					$value->clinica = $dataProfesional->result()[0]->clinica;

					if($dataCon != null){
						$value->convenio_entrega = $dataCon->result()[0]->convenio_entrega;
						$pos = strpos($dataCon->result()[0]->convenio_entrega, "ENTREGA FISICO");
						if ($pos === false) {
						    $value->validateConVenio = "NO";
						} else {
						    $value->validateConVenio = "SI";
						}
					}else{
						$value->convenio_entrega = "";
						$value->validateConVenio = "SI";
					}
					
				}else{
					$value->profesional = "";
					$value->nameProfesional = "";
					$value->clinica = "";
					$value->convenio_entrega = "";
					$value->validateConVenio = "NO";
				}
				
   			}
   			echo json_encode($data->result());
		}else{
			echo "[]";
		}	
   			
   		
	}

	public function entregarImagen(){
		$id = $_POST['idDetalle']; 
		$usuario = $_POST['user_radiologia']; 
		$array = array(
			'id' => $id,
			'estado' => "ENTREGADO");
		$this->imagenes_radiologia_model->updateImagen($array);

		$arrayLog = array(
			'idDetalle' => $id,
			'user_radiologia' => $usuario,
			'estado' => "ACTIVO");
		$this->imagenes_radiologia_model->crearLogEntregadas($arrayLog);
	}

	public function reporteDetalleSinImagen(){
	    $document = $_POST['document']; 
	    $fechaInicial = $_POST['fechaInicial']; 
	    $fechaFinal = $_POST['fechaFinal']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->factura_radiologia_model->getFacturasAddImg($document, $fechaInicial, $fechaFinal, $IDCRInternoCR);
		if($data != null){
			foreach ($data->result() as $value) {
				$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
   				if($dataUsuario != null){
					$value->first_name = $dataUsuario->result()[0]->first_name;
					$value->last_name = $dataUsuario->result()[0]->last_name;  				
   				}else{
   					$value->first_name = "";
					$value->last_name = "";
   				}

				$dataProfesional = $this->user_radiologia_model->getProfesional($value->profesional);

				if($dataProfesional != null){
					$dataCon = $this->convenios_model->getConveniosProfes($dataProfesional->result()[0]->clinica, $dataProfesional->result()[0]->id, $IDCRInternoCR);
					$value->profesional = $dataProfesional->result()[0]->id;
					$value->nameProfesional = $dataProfesional->result()[0]->first_name . " " . $dataProfesional->result()[0]->last_name;
					$value->clinica = $dataProfesional->result()[0]->clinica;



					if($dataCon != null){
						$value->convenio = $dataCon->result()[0]->convenio_entrega;
					}else{
						$value->convenio = "";
					}
				}else{
					$value->profesional = "";
					$value->nameProfesional = "";
					$value->clinica = "";
					$value->convenio = "";
				}
			
				
			}
		}else{
			$data = null;
		}
   		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function exportarReporteCierreCaja(){
		$document = $_GET['document']; 
	    $fechaInicial = $_GET['fechaInicial']; 
	    $fechaFinal = $_GET['fechaFinal']; 
	    $IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
	    $i = 4; 
	    $total = 0;

	    $dataUser = $this->user_radiologia_model->getUsuario($document);

		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');

		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');

		/** Include PHPExcel */
		$this->load->file('PHPExcel/Classes/PHPExcel.php');


		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Unisoft System Corporation")
               ->setLastModifiedBy("HCO")
               ->setTitle("REPORTE CIERRE DE CAJA")
               ->setSubject("REPORTE CIERRE DE CAJA")
               ->setDescription("REPORTE CIERRE DE CAJA")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");


        if($dataUser != null){
        	// Add some data						 
			$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'FUNCIONARIO')
		            ->setCellValue('B1', $dataUser->result()[0]->first_name . " " . $dataUser->result()[0]->last_name)
		            ->setCellValue('D1', 'FECHA INICIAL')
		            ->setCellValue('E1', $fechaInicial)
		            ->setCellValue('G1', 'FECHA FINAL')
		            ->setCellValue('H1', $fechaFinal);
        }
		

        $objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A3', 'FECHA')
		            ->setCellValue('B3', 'HORA')
		            ->setCellValue('C3', 'FACTURA')
		            ->setCellValue('D3', 'NOMBRE')
		            ->setCellValue('E3', 'DESCRIPCION')
		            ->setCellValue('F3', 'CANTIDAD')
		            ->setCellValue('G3', 'VALOR')
		            ->setCellValue('H3', 'ESTADO');
		
		$data = $this->factura_radiologia_model->reporteCierreCaja($fechaInicial, $fechaFinal, $document, $IDCRInternoCR);
   		if($data != null){
			foreach ($data->result() as $value) {

	    		$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
   				if($dataUsuario != null){
					$value->first_name = $dataUsuario->result()[0]->first_name;
					$value->last_name = $dataUsuario->result()[0]->last_name;  				
   				}else{
   					$value->first_name = "";
					$value->last_name = "";
   				}
	   			
				$estado = "";
				if($value->estadoFactura == "ACTIVO"){
					$estado = "APROBADA";
					$total += $value->total;
				}else{
					$estado = $value->estadoFactura;
				}

				$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A' . $i, $value->fecha)
		            ->setCellValue('B' . $i, $value->hora)
		            ->setCellValue('C' . $i, $value->id_factura_radiologia)
		            ->setCellValue('D' . $i, $value->first_name . " " . $value->last_name)
		            ->setCellValue('E' . $i, $value->descripcion)
		            ->setCellValue('F' . $i, $value->cantidad)
		            ->setCellValue('G' . $i, $value->total)
		            ->setCellValue('H' . $i, $estado);
		            $i++;   
			}
		}

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('REPORTE CIERRE DE CAJA');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$nombreArchivo = "Reporte_Cierre_Caja" . date("Y") . date("m") . date("d") . date("H") . date("i") . date("s") . date("u") . ".xls";
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $nombreArchivo . '.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}

	public function findTipoProcedimientosRadiologia(){
		$grupo = $_POST['grupo']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->tipo_procedimientos_model->getGrupo($grupo, $IDCRInternoCR);
   		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function findPresentacionRadiologia(){
		$idtipo = $_POST['idtipo']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->presentacion_model->getTipo($idtipo, $IDCRInternoCR);
   		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function add_procedimiento_radiologia()	{
		$codigo = $_POST['codigo']; 
	    $valor = $_POST['valor']; 
	    $grupo = $_POST['grupo']; 
	    $tipo = $_POST['tipo']; 
	    $dias = $_POST['dias']; 
	    $presentacion = $_POST['presentacion']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		$array = array(
			'codigo' => $codigo,
			'valor' => $valor,
			'tipo' => $tipo,
			'grupo' => $grupo,
			'presentacion' => $presentacion,
			'dias' => $dias,
			'centro_radiologico' => $IDCRInternoCR);
		$this->procedimientos_model->crear($array);
	}

	public function update_procedimiento_radiologia()	{
		$codigo = $_POST['codigo']; 
	    $valor = $_POST['valor']; 
	    $grupo = $_POST['grupo']; 
	    $tipo = $_POST['tipo']; 
	    $dias = $_POST['dias']; 
	    $presentacion = $_POST['presentacion']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		
		$array = array(
			'codigo' => $codigo,
			'valor' => $valor,
			'tipo' => $tipo,
			'grupo' => $grupo,
			'presentacion' => $presentacion,
			'dias' => $dias,
			'centro_radiologico' => $IDCRInternoCR);
		$this->procedimientos_model->actualizar($array);
	}

	public function validateCodigoProcedimientoRadio(){
		$codigo = $_POST['codigo']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->procedimientos_model->getCodigo($codigo, $IDCRInternoCR);
   		if($data != null){
   			foreach ($data->result() as $key) {
				echo json_encode($key);
   			}			
		}else{
			echo "{}";
		}
	}

	public function validateNombreProcedimientoRadio(){
		$nombre = $_POST['nombre']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->procedimientos_model->getNombre($nombre, $IDCRInternoCR);
   		if($data != null){
			echo '{"filas": "' . count($data->result()) . '"}'; 
		}else{
			echo '{"filas": "0"}';
		}
	}

	public function getReporteTiempoAtencion(){
		
		$fechaInicial = $_POST["uno"];
        $fechaFinal = $_POST["dos"];
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		//echo $interval->format('%R%a días %H:%I:%S');
		$data = $this->factura_radiologia_model->reporte_tiempos_atencion($fechaInicial, $fechaFinal, $IDCRInternoCR);
   		if($data != null){
   			foreach ($data as $value) {
   				$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($value->documento);
   				if($dataUsuario != null){
					$value->first_name = $dataUsuario->result()[0]->first_name;
					$value->last_name = $dataUsuario->result()[0]->last_name;  				
   				}else{
   					$value->first_name = "";
					$value->last_name = "";
   				}
   				if(!(empty($value->fecha_atencion)) && !(empty($value->hora_atencion))){
   					$datetimeFactura = date_create($value->fechafactura . ' ' . $value->horafactura);
					$datetimeAtencion = date_create($value->fecha_atencion . ' ' . $value->hora_atencion);
					$interval = date_diff($datetimeFactura, $datetimeAtencion);
					$value->tiempo_factura_atencion = $interval->format('%R%a días %H:%I:%S');

					$dataFechaConsulta = $this->imagenes_radiologia_model->getFechaFinConsultar($value->id);
					if($dataFechaConsulta != null){
						$datetimeConsultaFin = date_create($dataFechaConsulta[0]->fecha . ' ' . $dataFechaConsulta[0]->hora);
						$intervalConsulta = date_diff($datetimeAtencion, $datetimeConsultaFin);
						$value->tiempo_consulta = $intervalConsulta->format('%R%a días %H:%I:%S');

	   					$value->fecha_fin_consulta = $dataFechaConsulta[0]->fecha;
	   					$value->hor_fin_consulta = $dataFechaConsulta[0]->hora;
					}else{
						$value->tiempo_consulta = "";
						$value->fecha_fin_consulta = "";
						$value->hor_fin_consulta = "";	
					}

					$dataFecha = $this->imagenes_radiologia_model->getFechaEntrega($value->id);
					if($dataFecha != null){
						$datetimeEntrega = date_create($dataFecha[0]->fecha . ' ' . $dataFecha[0]->hora);
						$intervalAtencion = date_diff($datetimeConsultaFin, $datetimeEntrega);
						$value->tiempo_atencion_entrega = $intervalAtencion->format('%R%a días %H:%I:%S');

	   					$value->fecha_entrega = $dataFecha[0]->fecha;
	   					$value->hora_entrega = $dataFecha[0]->hora;
					}else{
						$value->tiempo_atencion_entrega = "";
						$value->fecha_entrega = "";
						$value->hora_entrega = "";	
					}


   				}else{
   					$value->tiempo_factura_atencion = "";
   					$value->tiempo_atencion_entrega = "";
   					$value->tiempo_consulta = "";
					$value->fecha_entrega = "";
					$value->hora_entrega = "";
					$value->fecha_atencion = "";
					$value->hora_atencion = "";
					$value->fecha_fin_consulta = "";
					$value->hor_fin_consulta = "";
   				}
   				if((empty($value->nombre_profesional)) && (empty($value->apellido_profesional))){   					
					$value->nombre_profesional = "";
					$value->apellido_profesional = "";
   				}
   			}
			echo json_encode($data); 
		}else{
			echo '[]';
		}
	}

	public function obtenerProfesionalesClinica(){
		$nombre = $_POST['nombre']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->clinicas_model->getProfesionales($nombre);


   		if($data != null){
   			foreach ($data->result() as $key) {
   				$dataClinicas = $this->convenios_model->getConveniosProfes($key->clinica, $key->id, $IDCRInternoCR);

   				if($dataClinicas != null){
   					$key->convenios = $dataClinicas->result();
   				}else{
   					$key->convenios = array();
   				}
   			}
   			$dataVerificar = $this->convenios_model->verificarConvenios($nombre, $IDCRInternoCR);
   			$numero = 0;

   			if($dataVerificar != null){
   				$numero = $dataVerificar->result()[0]->Total;
   			}else{
   				$numero = 0;
   			}
   			$array = array('datos' => $data->result(), 'totalConveniosIguales' => $numero);	
			echo json_encode($array);
		}else{
			echo "[]";
		}
	}

	public function obtenerConvenioProfesional(){
		$profesional = $_POST['profesional']; 
		$clinica = $_POST['clinica'];		 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		if($profesional == "Todos"){	

			$dataVerificar = $this->convenios_model->verificarConvenios($clinica, $IDCRInternoCR);
			$numero = 0;
			if($dataVerificar != null){
   				$numero = $dataVerificar->result()[0]->Total;
   			}else{
   				$numero = 0;
   			}

   			$dataConven = $this->convenios_model->getConvenios($clinica, $IDCRInternoCR);
   			if($dataConven != null){

   				if($numero == count($dataConven->result())){
   					foreach ($dataConven->result() as $key) {
   						$key->resultado = "IGUALES";			   				
		   			}
		   			echo json_encode($dataConven->result()[0]);
   				}else{
   					
   					foreach ($dataConven->result() as $key) {
   						$key->resultado = "DIFERENTES";			   				
		   			}
		   			echo json_encode($dataConven->result()[0]);
			   		
   				}
   			}else{
				echo "{}";
			}
		}else{

			$data = $this->convenios_model->getConveniosProfes($clinica, $profesional, $IDCRInternoCR);
	   		if($data != null){
	   			foreach ($data->result() as $key) {
					$key->resultado = "INDIVIDUAL";
	   				echo json_encode($key);
	   			}
				
			}else{
				echo "{}";
			}
		}
	}

	public function addConvenio() {
		$clinica = $_POST['clinica']; 
		$codigo = $_POST['codigo'];
		$verificacion = $_POST['verificacion']; 
	    $profesional = $_POST['profesional']; 
	    $descuento = $_POST['descuento']; 
	    $entrega = $_POST['entrega']; 
	    $facturacion = $_POST['facturacion']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		if($profesional == "Todos"){
			$data = $this->clinicas_model->getProfesionales($clinica);

	   		if($data != null){
	   			foreach ($data->result() as $key) {
	   				$array = array(
						'clinica' => $clinica,
						'codigo' => $codigo,
						'verificacion' => $verificacion,
						'profesional' => $key->id,
						'descuento' => $descuento,
						'entrega' => $entrega,
						'facturacion' => $facturacion,
						'centro_radiologico' => $IDCRInternoCR);
					$this->convenios_model->crear($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
	   			}
	   		}
		}else{
			$array = array(
				'clinica' => $clinica,
				'codigo' => $codigo,
				'verificacion' => $verificacion,
				'profesional' => $profesional,
				'descuento' => $descuento,
				'entrega' => $entrega,
				'facturacion' => $facturacion,
				'centro_radiologico' => $IDCRInternoCR);
			$this->convenios_model->crear($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		}
		
	}

	public function updateConvenio() {
		$clinica = $_POST['clinica'];
		$codigo = $_POST['codigo'];
		$verificacion = $_POST['verificacion']; 
	    $profesional = $_POST['profesional']; 
	    $descuento = $_POST['descuento']; 
	    $entrega = $_POST['entrega']; 
	    $facturacion = $_POST['facturacion']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		if($profesional == "Todos"){
			$data = $this->clinicas_model->getProfesionales($clinica);

	   		if($data != null){
	   			foreach ($data->result() as $key) {
	   				$array = array(
						'clinica' => $clinica,
						'codigo' => $codigo,
						'verificacion' => $verificacion,
						'profesional' => $key->id,
						'descuento' => $descuento,
						'entrega' => $entrega,
						'facturacion' => $facturacion,
						'centro_radiologico' => $IDCRInternoCR);
					$this->convenios_model->update($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
	   			}
	   		}
		}else{
			$array = array(
				'clinica' => $clinica,
				'codigo' => $codigo,
				'verificacion' => $verificacion,
				'profesional' => $profesional,
				'descuento' => $descuento,
				'entrega' => $entrega,
				'facturacion' => $facturacion,
				'centro_radiologico' => $IDCRInternoCR);
			$this->convenios_model->update($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		}
		
	}

	public function updateImagenesRadiologia()	{
	    $user_radiologia = $_POST['user_radiologia']; 
	    $idDetalle = $_POST['idDetalle']; 
	    $profesional = $_POST['profesional'];
	    $name_profesional = $_POST['name_profesional'];  
	    $clinica = $_POST['clinica'];   
	    $id_imagen = $_POST['id_imagen'];  
	    $document = $_POST['document']; 
	    $estado = "";	    
	    $correo = $_POST['correo']; 
	    $observacion = $_POST['observacion']; 
	    $id_procedimiento = $_POST['id_procedimiento']; 
	    $jsonFotos = json_decode($_POST['jsonFotos']); 
	    $jsonTags = json_decode($_POST['jsonTags']); 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		//$this->imagenes_radiologia_model->delete($id_imagen);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		
		if(count($jsonFotos) > 0){

			for ($i=0; $i < count($jsonFotos); $i++) {
				if($jsonFotos[$i] != null){
					$foto = $jsonFotos[$i]->imagen; 
					$tags = $jsonTags[$i]->id; 
		            //Se guardan los datos en un array asociativo para pasarlos la funcion del model
					$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);
					$nombreCentro = "";

					$dataProced = $this->procedimientos_model->getId($id_procedimiento);

		            if($dataProced != null){
						$array = array(
							'id' => $id_imagen,
							'dias' => $dataProced->result()[0]->diasEntrega,
							'imagen' => $foto,
							'document' => $document,
							'tags' => $tags,
							'especialista' => $name_profesional,
							'clinica' => $clinica);
						$this->imagenes_radiologia_model->updateImagenUrl($array);//Se llama a la funcion de que esta en modelo y el resultado se guarda

						//Se guardan los datos en un array asociativo para pasarlos la funcion del model
						$arrayDetalle = array(
							'detalleFactura' => $idDetalle,
							'user_radiologia' => $user_radiologia,
							'observacion' => $observacion);
						$this->imagenes_radiologia_model->crearLog($arrayDetalle);//Se llama a la funcion de que esta en modelo y el resultado se guarda
					}

		            if(trim($profesional) != ""){	            	
		            	

						$clinica = "";
						$idProfesional = "";

						$dataProfesional = $this->user_radiologia_model->getProfesionalId($profesional);
						if($dataProfesional != null){
							$clinica = $dataProfesional->result()[0]->clinica;	
							$idProfesional = $dataProfesional->result()[0]->id;			
						}

						$dataCovenio = $this->convenios_model->getConveniosProfes($clinica, $idProfesional, $IDCRInternoCR);
						//echo $idProfesional . " " . $clinica;
						if($dataCovenio != null){

							$convenio = $dataCovenio->result()[0]->convenio_entrega;
							$findme   = 'ENTREGA FISICO';
							$pos = strpos($convenio, $findme);
							// Nótese el uso de ===. Puesto que == simple no funcionará como se espera
							// porque la posición de 'a' está en el 1° (primer) caracter.
							if ($pos === false) { 
								$arrayUpdate = array(
									'id' => $idDetalle,
									'estado' => "ENTREGADO");
								$this->imagenes_radiologia_model->updateImagen($arrayUpdate);

								$arrayLog = array(
									'idDetalle' => $idDetalle,
									'user_radiologia' => $user_radiologia,
									'estado' => "ACTIVO");
								$this->imagenes_radiologia_model->crearLogEntregadas($arrayLog);
							} else {
							    
							}	
							
							$conv = explode(",", $convenio);
							//echo $conv;
							for ($j=0; $j < count($conv); $j++) { 
								switch ($conv[$j]) {
									case 'PLATAFORMA HCO':
										
										break;

									case 'EMAIL':
										

										

										if($dataCR != null){
											$nombreCentro = $dataCR->result()[0]->name;
										}
							            
							            
							            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;

			
			                            $message = '<!DOCTYPE html>
			                            <html>
			                            <head>
			                              <meta charset="UTF-8">
			                              <title>Seleccion Perfil</title>
			                              <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
			                              <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			                              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			                                <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
			                              <script type="text/javascript">
			                              $(window).load(function() {
			                                $(".loader").fadeOut("slow");
			                              })
			                              </script>
			                              <style type="text/css">
			                                .loader {
			                                  position: fixed;
			                                  left: 0px;
			                                  top: 0px;
			                                  width: 100%;
			                                  height: 100%;
			                                  z-index: 9999;
			                                  background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
			                                }

			                                header {
			                                width: 100%;
			                                top: 0;
			                                left: 0;
			                                z-index: 999;
			                            }
			                            header h1#logo {
			                                display: inline-block;
			                                height: 150px;
			                                line-height: 150px;
			                                float: left;
			                                font-family: "Oswald", sans-serif;
			                                font-size: 60px;
			                                color: white;
			                                font-weight: 400;
			                                -webkit-transition: all 0.3s;
			                                -moz-transition: all 0.3s;
			                                -ms-transition: all 0.3s;
			                                -o-transition: all 0.3s;
			                                transition: all 0.3s;
			                            }
			                            header nav {
			                                display: inline-block;
			                                float: right;
			                            }
			                            header nav a {
			                                line-height: 150px;
			                                margin-left: 20px;
			                                color: #9fdbfc;
			                                font-weight: 700;
			                                font-size: 18px;
			                                -webkit-transition: all 0.3s;
			                                -moz-transition: all 0.3s;
			                                -ms-transition: all 0.3s;
			                                -o-transition: all 0.3s;
			                                transition: all 0.3s;
			                            }
			                            header nav a:hover {
			                                color: white;
			                            }
			                            header.smaller {
			                                height: 75px;
			                            }
			                            header.smaller h1#logo {
			                                width: 150px;
			                                height: 75px;
			                                line-height: 75px;
			                                font-size: 30px;
			                            }
			                            header.smaller nav a {
			                                line-height: 75px;
			                            }

			                            @media all and (max-width: 660px) {
			                                header h1#logo {
			                                    display: block;
			                                    float: none;
			                                    margin: 0 auto;
			                                    height: 100px;
			                                    line-height: 100px;
			                                    text-align: center;
			                                }
			                                header nav {
			                                    display: block;
			                                    float: none;
			                                    height: 50px;
			                                    text-align: center;
			                                    margin: 0 auto;
			                                }
			                                header nav a {
			                                    line-height: 50px;
			                                    margin: 0 10px;
			                                }
			                                header.smaller {
			                                    height: 75px;
			                                }
			                                header.smaller h1#logo {
			                                    height: 40px;
			                                    line-height: 40px;
			                                    font-size: 30px;
			                                }
			                                header.smaller nav {
			                                    height: 35px;
			                                }
			                                header.smaller nav a {
			                                    line-height: 35px;
			                                }
			                            }
			                            #footer{
			                                position:absolute;
			                                width:100%;
			                                height:90px;
			                                background-color:#FFFFFF;
			                                color:#000000;
			                                bottom:0px;
			                                clear:both;
			                              }

			                             .media
			                                {
			                                    /*box-shadow:0px 0px 4px -2px #000;*/
			                                    margin: 20px 0;
			                                    padding:30px;
			                                }
			                                .dp
			                                {
			                                    border:10px solid #eee;
			                                    transition: all 0.2s ease-in-out;
			                                }
			                                .dp:hover
			                                {
			                                    border:2px solid #eee;
			                                    
			                                }



			                                .nav {
			                                left:50%;
			                                margin-left:-150px;
			                                top:50px;
			                                position:absolute;
			                            }
			                            .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
			                                background:#fff;
			                            }
			                            .dropdown {
			                                border-radius:4px;
			                                width:300px;    
			                            }
			                            .dropdown-menu>li>a {
			                                color:#428bca;
			                            }
			                            .dropdown ul.dropdown-menu {
			                                border-radius:4px;
			                                box-shadow:none;
			                                margin-top:20px;
			                                width:300px;
			                            }
			                            .dropdown ul.dropdown-menu:before {
			                                content: "";
			                                border-bottom: 10px solid #fff;
			                                border-right: 10px solid transparent;
			                                border-left: 10px solid transparent;
			                                position: absolute;
			                                top: -10px;
			                                right: 16px;
			                                z-index: 10;
			                            }
			                            .dropdown ul.dropdown-menu:after {
			                                content: "";
			                                border-bottom: 12px solid #ccc;
			                                border-right: 12px solid transparent;
			                                border-left: 12px solid transparent;
			                                position: absolute;
			                                top: -12px;
			                                right: 14px;
			                                z-index: 9;
			                            }




			                            .profile 
			                            {
			                                min-height: 300px;
			                                display: inline-block;
			                                }
			                            figcaption.ratings
			                            {
			                                margin-top:20px;
			                                }
			                            figcaption.ratings a
			                            {
			                                color:#f1c40f;
			                                font-size:11px;
			                                }
			                            figcaption.ratings a:hover
			                            {
			                                color:#f39c12;
			                                text-decoration:none;
			                                }
			                            .divider 
			                            {
			                                border-top:1px solid rgba(0,0,0,0.1);
			                                }
			                            .emphasis 
			                            {
			                                border-top: 4px solid transparent;
			                                }
			                            .emphasis:hover 
			                            {
			                                border-top: 4px solid #1abc9c;
			                                }
			                            .emphasis h2
			                            {
			                                margin-bottom:0;
			                                }

			                            span.tags 
			                            {
			                                background: #1abc9c;
			                                border-radius: 2px;
			                                color: #f5f5f5;
			                                font-weight: bold;
			                                padding: 2px 4px;
			                                }

			                            .row {
			                                margin-left: -130px;
			                            }

			                            .col-fixed {
			                                /* custom width */
			                                width:320px;
			                            }
			                            .col-min {
			                                /* custom min width */
			                                min-width:320px;
			                            }
			                            .col-max {
			                                /* custom max width */
			                                max-width:320px;
			                            }

			                            #menuinicial{
			                                position:fixed;
			                                width:100%;
			                                height:90px;
			                                background-color:#FFFFFF;
			                                color:#000000;
			                                clear:both;
			                                z-index: 100;
			                              }

			                            .notifications, footer .int li .notifications {
			                                background-color: #3ca3c1;
			                                color: #fff;
			                                position: absolute;
			                                left: 42%;
			                                font-weight: bold;
			                                padding: 4px;
			                                border-radius: 12px;
			                            }

			                            #css {

			                            box-shadow: 0 0 15px #ddd;
			                            background: #FFFFFF;
			                            }

			                            .btn-primary:hover {
			                                color: #fff;
			                                background-color: #286090;
			                                border-color: #204d74;
			                            }

			                            .btn.focus, .btn:focus, .btn:hover {
			                                color: #333;
			                                text-decoration: none;
			                            }
			                            a:focus, a:hover {
			                                color: #23527c;
			                                text-decoration: underline;
			                            }
			                            a:active, a:hover {
			                                outline: 0;
			                            }
			                            .btn-group-lg>.btn, .btn-lg {
			                                padding: 10px 16px;
			                                font-size: 18px;
			                                line-height: 1.3333333;
			                                border-radius: 6px;
			                            }
			                            .btn-primary {
			                                color: #fff;
			                                background-color: #337ab7;
			                                border-color: #2e6da4;
			                            }
			                            .btn {
			                                display: inline-block;
			                                padding: 6px 12px;
			                                margin-bottom: 0;
			                                font-size: 14px;
			                                font-weight: 400;
			                                line-height: 1.42857143;
			                                text-align: center;
			                                white-space: nowrap;
			                                vertical-align: middle;
			                                -ms-touch-action: manipulation;
			                                touch-action: manipulation;
			                                cursor: pointer;
			                                -webkit-user-select: none;
			                                -moz-user-select: none;
			                                -ms-user-select: none;
			                                user-select: none;
			                                background-image: none;
			                                border: 1px solid transparent;
			                                border-radius: 4px;
			                            }
			                            [role=button] {
			                                cursor: pointer;
			                            }


			                              </style>
			                              <script type="text/javascript">
			                                function init() {
			                                    window.addEventListener("scroll", function(e){
			                                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
			                                            shrinkOn = 300,
			                                            header = document.querySelector("header");
			                                        if (distanceY > shrinkOn) {
			                                            classie.add(header,"smaller");
			                                        } else {
			                                            if (classie.has(header,"smaller")) {
			                                                classie.remove(header,"smaller");
			                                            }
			                                        }
			                                    });
			                                }
			                                     window.onload = init();
			                              </script>
			                                
			                                
			                                
			                            </head>
			                            <body>
			                            <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
			                            <header>
			                                   <img src="' . base_url() . 'images/headerimg.png" width="100%"  height="100px">
			                            </header>

			                            <div class="loader"></div>
			                            <div class="container-fluid">
			                                </br>
			                                </br>
			                                </br>
			                                </br>
			                                </br>
			                                <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
			                                <div class="col-md-12" height="150px" width="100%">
			                                </br>
			                                </br>
			                                <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
			                                </div>
			                                <div class="col-md-12" style="width:100%;height: 100px;">
			                                <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
			    background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
			                                </div>
			                                </div>
			                                </br>
			                                </br>
			                                </br>
			                                </br>
			                                <div class="col-md-12" style="width: 100%;">
			                                </br>
			                                </br>
			                                <hr style="border-top: 2px solid #0683c9;">
			                                </div>
			                                <div class="col-md-5" style="width: 100%;">
			                                <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

			                                <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
			                                <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
			                                <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
			                                <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
			                                <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
			                                 </p>

			                                </div>
			                                
			                            </div>
			                            </div>
			                            </div>  
			                            </body>
			                            </html>';

							            $configuraciones["mailtype"] = 'html';
										$this->email->initialize($configuraciones);
										$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
										$this->email->to($correo);
										$this->email->subject($titulo);
										$this->email->message($message);
										$this->email->send();
										break;

									case 'ENTREGA FISICO':
										
										break;
								}
							}
						}else{
							

							if($dataCR != null){
								$nombreCentro = $dataCR->result()[0]->name;
							}
				            
				            
				            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;


                            $message = '<!DOCTYPE html>
                            <html>
                            <head>
                              <meta charset="UTF-8">
                              <title>Seleccion Perfil</title>
                              <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
                              <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
                              <script type="text/javascript">
                              $(window).load(function() {
                                $(".loader").fadeOut("slow");
                              })
                              </script>
                              <style type="text/css">
                                .loader {
                                  position: fixed;
                                  left: 0px;
                                  top: 0px;
                                  width: 100%;
                                  height: 100%;
                                  z-index: 9999;
                                  background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
                                }

                                header {
                                width: 100%;
                                top: 0;
                                left: 0;
                                z-index: 999;
                            }
                            header h1#logo {
                                display: inline-block;
                                height: 150px;
                                line-height: 150px;
                                float: left;
                                font-family: "Oswald", sans-serif;
                                font-size: 60px;
                                color: white;
                                font-weight: 400;
                                -webkit-transition: all 0.3s;
                                -moz-transition: all 0.3s;
                                -ms-transition: all 0.3s;
                                -o-transition: all 0.3s;
                                transition: all 0.3s;
                            }
                            header nav {
                                display: inline-block;
                                float: right;
                            }
                            header nav a {
                                line-height: 150px;
                                margin-left: 20px;
                                color: #9fdbfc;
                                font-weight: 700;
                                font-size: 18px;
                                -webkit-transition: all 0.3s;
                                -moz-transition: all 0.3s;
                                -ms-transition: all 0.3s;
                                -o-transition: all 0.3s;
                                transition: all 0.3s;
                            }
                            header nav a:hover {
                                color: white;
                            }
                            header.smaller {
                                height: 75px;
                            }
                            header.smaller h1#logo {
                                width: 150px;
                                height: 75px;
                                line-height: 75px;
                                font-size: 30px;
                            }
                            header.smaller nav a {
                                line-height: 75px;
                            }

                            @media all and (max-width: 660px) {
                                header h1#logo {
                                    display: block;
                                    float: none;
                                    margin: 0 auto;
                                    height: 100px;
                                    line-height: 100px;
                                    text-align: center;
                                }
                                header nav {
                                    display: block;
                                    float: none;
                                    height: 50px;
                                    text-align: center;
                                    margin: 0 auto;
                                }
                                header nav a {
                                    line-height: 50px;
                                    margin: 0 10px;
                                }
                                header.smaller {
                                    height: 75px;
                                }
                                header.smaller h1#logo {
                                    height: 40px;
                                    line-height: 40px;
                                    font-size: 30px;
                                }
                                header.smaller nav {
                                    height: 35px;
                                }
                                header.smaller nav a {
                                    line-height: 35px;
                                }
                            }
                            #footer{
                                position:absolute;
                                width:100%;
                                height:90px;
                                background-color:#FFFFFF;
                                color:#000000;
                                bottom:0px;
                                clear:both;
                              }

                             .media
                                {
                                    /*box-shadow:0px 0px 4px -2px #000;*/
                                    margin: 20px 0;
                                    padding:30px;
                                }
                                .dp
                                {
                                    border:10px solid #eee;
                                    transition: all 0.2s ease-in-out;
                                }
                                .dp:hover
                                {
                                    border:2px solid #eee;
                                    
                                }



                                .nav {
                                left:50%;
                                margin-left:-150px;
                                top:50px;
                                position:absolute;
                            }
                            .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
                                background:#fff;
                            }
                            .dropdown {
                                border-radius:4px;
                                width:300px;    
                            }
                            .dropdown-menu>li>a {
                                color:#428bca;
                            }
                            .dropdown ul.dropdown-menu {
                                border-radius:4px;
                                box-shadow:none;
                                margin-top:20px;
                                width:300px;
                            }
                            .dropdown ul.dropdown-menu:before {
                                content: "";
                                border-bottom: 10px solid #fff;
                                border-right: 10px solid transparent;
                                border-left: 10px solid transparent;
                                position: absolute;
                                top: -10px;
                                right: 16px;
                                z-index: 10;
                            }
                            .dropdown ul.dropdown-menu:after {
                                content: "";
                                border-bottom: 12px solid #ccc;
                                border-right: 12px solid transparent;
                                border-left: 12px solid transparent;
                                position: absolute;
                                top: -12px;
                                right: 14px;
                                z-index: 9;
                            }




                            .profile 
                            {
                                min-height: 300px;
                                display: inline-block;
                                }
                            figcaption.ratings
                            {
                                margin-top:20px;
                                }
                            figcaption.ratings a
                            {
                                color:#f1c40f;
                                font-size:11px;
                                }
                            figcaption.ratings a:hover
                            {
                                color:#f39c12;
                                text-decoration:none;
                                }
                            .divider 
                            {
                                border-top:1px solid rgba(0,0,0,0.1);
                                }
                            .emphasis 
                            {
                                border-top: 4px solid transparent;
                                }
                            .emphasis:hover 
                            {
                                border-top: 4px solid #1abc9c;
                                }
                            .emphasis h2
                            {
                                margin-bottom:0;
                                }

                            span.tags 
                            {
                                background: #1abc9c;
                                border-radius: 2px;
                                color: #f5f5f5;
                                font-weight: bold;
                                padding: 2px 4px;
                                }

                            .row {
                                margin-left: -130px;
                            }

                            .col-fixed {
                                /* custom width */
                                width:320px;
                            }
                            .col-min {
                                /* custom min width */
                                min-width:320px;
                            }
                            .col-max {
                                /* custom max width */
                                max-width:320px;
                            }

                            #menuinicial{
                                position:fixed;
                                width:100%;
                                height:90px;
                                background-color:#FFFFFF;
                                color:#000000;
                                clear:both;
                                z-index: 100;
                              }

                            .notifications, footer .int li .notifications {
                                background-color: #3ca3c1;
                                color: #fff;
                                position: absolute;
                                left: 42%;
                                font-weight: bold;
                                padding: 4px;
                                border-radius: 12px;
                            }

                            #css {

                            box-shadow: 0 0 15px #ddd;
                            background: #FFFFFF;
                            }

                            .btn-primary:hover {
                                color: #fff;
                                background-color: #286090;
                                border-color: #204d74;
                            }

                            .btn.focus, .btn:focus, .btn:hover {
                                color: #333;
                                text-decoration: none;
                            }
                            a:focus, a:hover {
                                color: #23527c;
                                text-decoration: underline;
                            }
                            a:active, a:hover {
                                outline: 0;
                            }
                            .btn-group-lg>.btn, .btn-lg {
                                padding: 10px 16px;
                                font-size: 18px;
                                line-height: 1.3333333;
                                border-radius: 6px;
                            }
                            .btn-primary {
                                color: #fff;
                                background-color: #337ab7;
                                border-color: #2e6da4;
                            }
                            .btn {
                                display: inline-block;
                                padding: 6px 12px;
                                margin-bottom: 0;
                                font-size: 14px;
                                font-weight: 400;
                                line-height: 1.42857143;
                                text-align: center;
                                white-space: nowrap;
                                vertical-align: middle;
                                -ms-touch-action: manipulation;
                                touch-action: manipulation;
                                cursor: pointer;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                                background-image: none;
                                border: 1px solid transparent;
                                border-radius: 4px;
                            }
                            [role=button] {
                                cursor: pointer;
                            }


                              </style>
                              <script type="text/javascript">
                                function init() {
                                    window.addEventListener("scroll", function(e){
                                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                                            shrinkOn = 300,
                                            header = document.querySelector("header");
                                        if (distanceY > shrinkOn) {
                                            classie.add(header,"smaller");
                                        } else {
                                            if (classie.has(header,"smaller")) {
                                                classie.remove(header,"smaller");
                                            }
                                        }
                                    });
                                }
                                     window.onload = init();
                              </script>
                                
                                
                                
                            </head>
                            <body>
                            <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
                            <header>
                                   <img src="' . base_url() . 'images/headerimg.png" width="100%"   height="100px">
                            </header>

                            <div class="loader"></div>
                            <div class="container-fluid">
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                                <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
                                <div class="col-md-12" height="150px" width="100%">
                                </br>
                                </br>
                                <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
                                </div>
                                <div class="col-md-12" style="width:100%;height: 100px;">
                                <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
    background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
                                </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                </br>
                                <div class="col-md-12" style="width: 100%;">
                                </br>
                                </br>
                                <hr style="border-top: 2px solid #0683c9;">
                                </div>
                                <div class="col-md-5" style="width: 100%;">
                                <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

                                <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
                                 </p>

                                </div>
                                
                            </div>
                            </div>
                            </div>  
                            </body>
                            </html>';

				            $configuraciones["mailtype"] = 'html';
							$this->email->initialize($configuraciones);
							$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
							$this->email->to($correo);
							$this->email->subject($titulo);
							$this->email->message($message);
							$this->email->send();
						}
						
					}else{						

						if($dataCR != null){
							$nombreCentro = $dataCR->result()[0]->name;
						}
			            
			            
			            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;


                        $message = '<!DOCTYPE html>
                        <html>
                        <head>
                          <meta charset="UTF-8">
                          <title>Seleccion Perfil</title>
                          <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
                          <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                          <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                            <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
                          <script type="text/javascript">
                          $(window).load(function() {
                            $(".loader").fadeOut("slow");
                          })
                          </script>
                          <style type="text/css">
                            .loader {
                              position: fixed;
                              left: 0px;
                              top: 0px;
                              width: 100%;
                              height: 100%;
                              z-index: 9999;
                              background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
                            }

                            header {
                            width: 100%;
                            top: 0;
                            left: 0;
                            z-index: 999;
                        }
                        header h1#logo {
                            display: inline-block;
                            height: 150px;
                            line-height: 150px;
                            float: left;
                            font-family: "Oswald", sans-serif;
                            font-size: 60px;
                            color: white;
                            font-weight: 400;
                            -webkit-transition: all 0.3s;
                            -moz-transition: all 0.3s;
                            -ms-transition: all 0.3s;
                            -o-transition: all 0.3s;
                            transition: all 0.3s;
                        }
                        header nav {
                            display: inline-block;
                            float: right;
                        }
                        header nav a {
                            line-height: 150px;
                            margin-left: 20px;
                            color: #9fdbfc;
                            font-weight: 700;
                            font-size: 18px;
                            -webkit-transition: all 0.3s;
                            -moz-transition: all 0.3s;
                            -ms-transition: all 0.3s;
                            -o-transition: all 0.3s;
                            transition: all 0.3s;
                        }
                        header nav a:hover {
                            color: white;
                        }
                        header.smaller {
                            height: 75px;
                        }
                        header.smaller h1#logo {
                            width: 150px;
                            height: 75px;
                            line-height: 75px;
                            font-size: 30px;
                        }
                        header.smaller nav a {
                            line-height: 75px;
                        }

                        @media all and (max-width: 660px) {
                            header h1#logo {
                                display: block;
                                float: none;
                                margin: 0 auto;
                                height: 100px;
                                line-height: 100px;
                                text-align: center;
                            }
                            header nav {
                                display: block;
                                float: none;
                                height: 50px;
                                text-align: center;
                                margin: 0 auto;
                            }
                            header nav a {
                                line-height: 50px;
                                margin: 0 10px;
                            }
                            header.smaller {
                                height: 75px;
                            }
                            header.smaller h1#logo {
                                height: 40px;
                                line-height: 40px;
                                font-size: 30px;
                            }
                            header.smaller nav {
                                height: 35px;
                            }
                            header.smaller nav a {
                                line-height: 35px;
                            }
                        }
                        #footer{
                            position:absolute;
                            width:100%;
                            height:90px;
                            background-color:#FFFFFF;
                            color:#000000;
                            bottom:0px;
                            clear:both;
                          }

                         .media
                            {
                                /*box-shadow:0px 0px 4px -2px #000;*/
                                margin: 20px 0;
                                padding:30px;
                            }
                            .dp
                            {
                                border:10px solid #eee;
                                transition: all 0.2s ease-in-out;
                            }
                            .dp:hover
                            {
                                border:2px solid #eee;
                                
                            }



                            .nav {
                            left:50%;
                            margin-left:-150px;
                            top:50px;
                            position:absolute;
                        }
                        .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
                            background:#fff;
                        }
                        .dropdown {
                            border-radius:4px;
                            width:300px;    
                        }
                        .dropdown-menu>li>a {
                            color:#428bca;
                        }
                        .dropdown ul.dropdown-menu {
                            border-radius:4px;
                            box-shadow:none;
                            margin-top:20px;
                            width:300px;
                        }
                        .dropdown ul.dropdown-menu:before {
                            content: "";
                            border-bottom: 10px solid #fff;
                            border-right: 10px solid transparent;
                            border-left: 10px solid transparent;
                            position: absolute;
                            top: -10px;
                            right: 16px;
                            z-index: 10;
                        }
                        .dropdown ul.dropdown-menu:after {
                            content: "";
                            border-bottom: 12px solid #ccc;
                            border-right: 12px solid transparent;
                            border-left: 12px solid transparent;
                            position: absolute;
                            top: -12px;
                            right: 14px;
                            z-index: 9;
                        }




                        .profile 
                        {
                            min-height: 300px;
                            display: inline-block;
                            }
                        figcaption.ratings
                        {
                            margin-top:20px;
                            }
                        figcaption.ratings a
                        {
                            color:#f1c40f;
                            font-size:11px;
                            }
                        figcaption.ratings a:hover
                        {
                            color:#f39c12;
                            text-decoration:none;
                            }
                        .divider 
                        {
                            border-top:1px solid rgba(0,0,0,0.1);
                            }
                        .emphasis 
                        {
                            border-top: 4px solid transparent;
                            }
                        .emphasis:hover 
                        {
                            border-top: 4px solid #1abc9c;
                            }
                        .emphasis h2
                        {
                            margin-bottom:0;
                            }

                        span.tags 
                        {
                            background: #1abc9c;
                            border-radius: 2px;
                            color: #f5f5f5;
                            font-weight: bold;
                            padding: 2px 4px;
                            }

                        .row {
                            margin-left: -130px;
                        }

                        .col-fixed {
                            /* custom width */
                            width:320px;
                        }
                        .col-min {
                            /* custom min width */
                            min-width:320px;
                        }
                        .col-max {
                            /* custom max width */
                            max-width:320px;
                        }

                        #menuinicial{
                            position:fixed;
                            width:100%;
                            height:90px;
                            background-color:#FFFFFF;
                            color:#000000;
                            clear:both;
                            z-index: 100;
                          }

                        .notifications, footer .int li .notifications {
                            background-color: #3ca3c1;
                            color: #fff;
                            position: absolute;
                            left: 42%;
                            font-weight: bold;
                            padding: 4px;
                            border-radius: 12px;
                        }

                        #css {

                        box-shadow: 0 0 15px #ddd;
                        background: #FFFFFF;
                        }

                        .btn-primary:hover {
                            color: #fff;
                            background-color: #286090;
                            border-color: #204d74;
                        }

                        .btn.focus, .btn:focus, .btn:hover {
                            color: #333;
                            text-decoration: none;
                        }
                        a:focus, a:hover {
                            color: #23527c;
                            text-decoration: underline;
                        }
                        a:active, a:hover {
                            outline: 0;
                        }
                        .btn-group-lg>.btn, .btn-lg {
                            padding: 10px 16px;
                            font-size: 18px;
                            line-height: 1.3333333;
                            border-radius: 6px;
                        }
                        .btn-primary {
                            color: #fff;
                            background-color: #337ab7;
                            border-color: #2e6da4;
                        }
                        .btn {
                            display: inline-block;
                            padding: 6px 12px;
                            margin-bottom: 0;
                            font-size: 14px;
                            font-weight: 400;
                            line-height: 1.42857143;
                            text-align: center;
                            white-space: nowrap;
                            vertical-align: middle;
                            -ms-touch-action: manipulation;
                            touch-action: manipulation;
                            cursor: pointer;
                            -webkit-user-select: none;
                            -moz-user-select: none;
                            -ms-user-select: none;
                            user-select: none;
                            background-image: none;
                            border: 1px solid transparent;
                            border-radius: 4px;
                        }
                        [role=button] {
                            cursor: pointer;
                        }


                          </style>
                          <script type="text/javascript">
                            function init() {
                                window.addEventListener("scroll", function(e){
                                    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                                        shrinkOn = 300,
                                        header = document.querySelector("header");
                                    if (distanceY > shrinkOn) {
                                        classie.add(header,"smaller");
                                    } else {
                                        if (classie.has(header,"smaller")) {
                                            classie.remove(header,"smaller");
                                        }
                                    }
                                });
                            }
                                 window.onload = init();
                          </script>
                            
                            
                            
                        </head>
                        <body>
                        <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
                        <header>
                               <img src="' . base_url() . 'images/headerimg.png" width="100%"   height="100px">
                        </header>

                        <div class="loader"></div>
                        <div class="container-fluid">
                            </br>
                            </br>
                            </br>
                            </br>
                            </br>
                            <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
                            <div class="col-md-12" height="150px" width="100%">
                            </br>
                            </br>
                            <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
                            </div>
                            <div class="col-md-12" style="width:100%;height: 100px;">
                            <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
                            </div>
                            </div>
                            </br>
                            </br>
                            </br>
                            </br>
                            <div class="col-md-12" style="width: 100%;">
                            </br>
                            </br>
                            <hr style="border-top: 2px solid #0683c9;">
                            </div>
                            <div class="col-md-5" style="width: 100%;">
                            <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

                            <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
                            <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
                            <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
                            <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
                            <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
                             </p>

                            </div>
                            
                        </div>
                        </div>
                        </div>  
                        </body>
                        </html>';

			            $configuraciones["mailtype"] = 'html';
						$this->email->initialize($configuraciones);
						$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
						$this->email->to($correo);
						$this->email->subject($titulo);
						$this->email->message($message);
						$this->email->send();
					}
					

					
				} 
					
			}
		}
	
	}
	function mensaje(){
		/*$message = "Se han ingresado imagenes de la persona registrada con su correo electronico";
		$configuraciones["mailtype"] = 'html';
		$this->email->initialize($configuraciones);
		$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
		$this->email->to("shelvinbb@gmail.com");
		$this->email->subject("Mensaje del Centro Radiologico");
		$this->email->message($message);
		$this->email->send();*/
		$convenio = "ENMTREGA";
		$conv = explode(",", $convenio);
		echo count($conv);
	}
	public function addImagenesRadiologia()	{
		$idFactura = $_POST['idFactura']; 
	    $user_radiologia = $_POST['user_radiologia']; 
	    $document = $_POST['document']; 
	    $idDetalle = $_POST['idDetalle']; 
	    $profesional = $_POST['profesional'];  
	    $name_profesional = $_POST['name_profesional'];  
	    $clinica = $_POST['clinica'];  
	    $id_procedimiento = $_POST['id_procedimiento']; 
	    $estado = "";
	    $orden_medica = $_POST['orden_medica']; 
	    $observacion = $_POST['observacion']; 
	    $tipo = $_POST['tipo']; 
	    $correo = $_POST['correo']; 
	    $jsonFotos = json_decode($_POST['jsonFotos']); 
	    $jsonTags = json_decode($_POST['jsonTags']); 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		$this->factura_radiologia_model->updateDetalle($idDetalle, "PROCESADO");

		if($tipo == "CONFOTOS"){
			if(count($jsonFotos) > 0){

				for ($i=0; $i < count($jsonFotos); $i++) {
					if($jsonFotos[$i] != null){
						$foto = $jsonFotos[$i]->imagen; 
						$tags = $jsonTags[$i]->id; 
			            //Se guardan los datos en un array asociativo para pasarlos la funcion del model
			            $dataProced = $this->procedimientos_model->getId($id_procedimiento);

			            if($dataProced != null){
			            	$array = array(
								'detalleFactura' => $idDetalle,
								'user_radiologia' => $user_radiologia,
								'imagen' => $foto,
								'dias' => $dataProced->result()[0]->diasEntrega,
								'document' => $document,
								'tags' => $tags,
								'especialista' => $name_profesional,
								'clinica' => $clinica);
							$this->imagenes_radiologia_model->crear($array, $tipo);//Se llama a la funcion de que esta en modelo y el resultado se guarda

							//Se guardan los datos en un array asociativo para pasarlos la funcion del model
							$arrayDetalle = array(
								'detalleFactura' => $idDetalle,
								'user_radiologia' => $user_radiologia,
								'observacion' => $observacion);
							$this->imagenes_radiologia_model->crearLog($arrayDetalle);//Se llama a la funcion de que esta en modelo y el resultado se guarda

						 	
			            }           

						

					//	
					} 
						
				}
			}
		}else{
			$array = array(
				'detalleFactura' => $idDetalle,
				'user_radiologia' => $user_radiologia,
				'imagen' => "",
				'dias' => "",
				'document' => $document,
				'tags' => "");
			$this->imagenes_radiologia_model->crear($array, $tipo);//Se llama a la funcion de que esta en modelo y el resultado se guarda

			//Se guardan los datos en un array asociativo para pasarlos la funcion del model
			$arrayDetalle = array(
				'detalleFactura' => $idDetalle,
				'user_radiologia' => $user_radiologia,
				'observacion' => $observacion);
			$this->imagenes_radiologia_model->crearLog($arrayDetalle);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		}

		
		$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);		
		$nombreCentro = "";

		if(trim($profesional) != ""){
			$dataProfesional = $this->user_radiologia_model->getProfesionalId($profesional);
			$clinica = "";
			$idProfesional = "";

			if($dataProfesional != null){
				$clinica = $dataProfesional->result()[0]->clinica;	
				$idProfesional = $dataProfesional->result()[0]->id;			
			}
			
			

			$dataCovenio = $this->convenios_model->getConveniosProfes($clinica, $idProfesional, $IDCRInternoCR);
			//echo $idProfesional . " " . $clinica;
			if($dataCovenio != null){
				$convenio = $dataCovenio->result()[0]->convenio_entrega;
				$conv = explode(",", $convenio);
				//echo $convenio;
				if($tipo == "CONFOTOS"){
					$convenio = $dataCovenio->result()[0]->convenio_entrega;
					$findme   = 'ENTREGA FISICO';
					$pos = strpos($convenio, $findme);
					// Nótese el uso de ===. Puesto que == simple no funcionará como se espera
					// porque la posición de 'a' está en el 1° (primer) caracter.
					if ($pos === false) { 
						$arrayUpdate = array(
							'id' => $idDetalle,
							'estado' => "ENTREGADO");
						$this->imagenes_radiologia_model->updateImagen($arrayUpdate);

						$arrayLog = array(
							'idDetalle' => $idDetalle,
							'user_radiologia' => $user_radiologia,
							'estado' => "ACTIVO");
						$this->imagenes_radiologia_model->crearLogEntregadas($arrayLog);
					} else {
					    
					}	
				}

				for ($i=0; $i < count($conv); $i++) { 
					switch ($conv[$i]) {
						case 'PLATAFORMA HCO':
							
							break;
						case 'EMAIL':

							
							if($tipo == "CONFOTOS"){
								if(count($jsonFotos) > 0){

									for ($i=0; $i < count($jsonFotos); $i++) {
										if($jsonFotos[$i] != null){
											$foto = $jsonFotos[$i]->imagen; 
											$tags = $jsonTags[$i]->id; 

											

											if($dataCR != null){
												$nombreCentro = $dataCR->result()[0]->name;
											}
								            
								            
								            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;

 			
				                            $message = '<!DOCTYPE html>
				                            <html>
				                            <head>
				                              <meta charset="UTF-8">
				                              <title>Seleccion Perfil</title>
				                              <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
				                              <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
				                              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
				                                <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
				                              <script type="text/javascript">
				                              $(window).load(function() {
				                                $(".loader").fadeOut("slow");
				                              })
				                              </script>
				                              <style type="text/css">
				                                .loader {
				                                  position: fixed;
				                                  left: 0px;
				                                  top: 0px;
				                                  width: 100%;
				                                  height: 100%;
				                                  z-index: 9999;
				                                  background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
				                                }

				                                header {
				                                width: 100%;
				                                top: 0;
				                                left: 0;
				                                z-index: 999;
				                            }
				                            header h1#logo {
				                                display: inline-block;
				                                height: 150px;
				                                line-height: 150px;
				                                float: left;
				                                font-family: "Oswald", sans-serif;
				                                font-size: 60px;
				                                color: white;
				                                font-weight: 400;
				                                -webkit-transition: all 0.3s;
				                                -moz-transition: all 0.3s;
				                                -ms-transition: all 0.3s;
				                                -o-transition: all 0.3s;
				                                transition: all 0.3s;
				                            }
				                            header nav {
				                                display: inline-block;
				                                float: right;
				                            }
				                            header nav a {
				                                line-height: 150px;
				                                margin-left: 20px;
				                                color: #9fdbfc;
				                                font-weight: 700;
				                                font-size: 18px;
				                                -webkit-transition: all 0.3s;
				                                -moz-transition: all 0.3s;
				                                -ms-transition: all 0.3s;
				                                -o-transition: all 0.3s;
				                                transition: all 0.3s;
				                            }
				                            header nav a:hover {
				                                color: white;
				                            }
				                            header.smaller {
				                                height: 75px;
				                            }
				                            header.smaller h1#logo {
				                                width: 150px;
				                                height: 75px;
				                                line-height: 75px;
				                                font-size: 30px;
				                            }
				                            header.smaller nav a {
				                                line-height: 75px;
				                            }

				                            @media all and (max-width: 660px) {
				                                header h1#logo {
				                                    display: block;
				                                    float: none;
				                                    margin: 0 auto;
				                                    height: 100px;
				                                    line-height: 100px;
				                                    text-align: center;
				                                }
				                                header nav {
				                                    display: block;
				                                    float: none;
				                                    height: 50px;
				                                    text-align: center;
				                                    margin: 0 auto;
				                                }
				                                header nav a {
				                                    line-height: 50px;
				                                    margin: 0 10px;
				                                }
				                                header.smaller {
				                                    height: 75px;
				                                }
				                                header.smaller h1#logo {
				                                    height: 40px;
				                                    line-height: 40px;
				                                    font-size: 30px;
				                                }
				                                header.smaller nav {
				                                    height: 35px;
				                                }
				                                header.smaller nav a {
				                                    line-height: 35px;
				                                }
				                            }
				                            #footer{
				                                position:absolute;
				                                width:100%;
				                                height:90px;
				                                background-color:#FFFFFF;
				                                color:#000000;
				                                bottom:0px;
				                                clear:both;
				                              }

				                             .media
				                                {
				                                    /*box-shadow:0px 0px 4px -2px #000;*/
				                                    margin: 20px 0;
				                                    padding:30px;
				                                }
				                                .dp
				                                {
				                                    border:10px solid #eee;
				                                    transition: all 0.2s ease-in-out;
				                                }
				                                .dp:hover
				                                {
				                                    border:2px solid #eee;
				                                    
				                                }



				                                .nav {
				                                left:50%;
				                                margin-left:-150px;
				                                top:50px;
				                                position:absolute;
				                            }
				                            .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
				                                background:#fff;
				                            }
				                            .dropdown {
				                                border-radius:4px;
				                                width:300px;    
				                            }
				                            .dropdown-menu>li>a {
				                                color:#428bca;
				                            }
				                            .dropdown ul.dropdown-menu {
				                                border-radius:4px;
				                                box-shadow:none;
				                                margin-top:20px;
				                                width:300px;
				                            }
				                            .dropdown ul.dropdown-menu:before {
				                                content: "";
				                                border-bottom: 10px solid #fff;
				                                border-right: 10px solid transparent;
				                                border-left: 10px solid transparent;
				                                position: absolute;
				                                top: -10px;
				                                right: 16px;
				                                z-index: 10;
				                            }
				                            .dropdown ul.dropdown-menu:after {
				                                content: "";
				                                border-bottom: 12px solid #ccc;
				                                border-right: 12px solid transparent;
				                                border-left: 12px solid transparent;
				                                position: absolute;
				                                top: -12px;
				                                right: 14px;
				                                z-index: 9;
				                            }




				                            .profile 
				                            {
				                                min-height: 300px;
				                                display: inline-block;
				                                }
				                            figcaption.ratings
				                            {
				                                margin-top:20px;
				                                }
				                            figcaption.ratings a
				                            {
				                                color:#f1c40f;
				                                font-size:11px;
				                                }
				                            figcaption.ratings a:hover
				                            {
				                                color:#f39c12;
				                                text-decoration:none;
				                                }
				                            .divider 
				                            {
				                                border-top:1px solid rgba(0,0,0,0.1);
				                                }
				                            .emphasis 
				                            {
				                                border-top: 4px solid transparent;
				                                }
				                            .emphasis:hover 
				                            {
				                                border-top: 4px solid #1abc9c;
				                                }
				                            .emphasis h2
				                            {
				                                margin-bottom:0;
				                                }

				                            span.tags 
				                            {
				                                background: #1abc9c;
				                                border-radius: 2px;
				                                color: #f5f5f5;
				                                font-weight: bold;
				                                padding: 2px 4px;
				                                }

				                            .row {
				                                margin-left: -130px;
				                            }

				                            .col-fixed {
				                                /* custom width */
				                                width:320px;
				                            }
				                            .col-min {
				                                /* custom min width */
				                                min-width:320px;
				                            }
				                            .col-max {
				                                /* custom max width */
				                                max-width:320px;
				                            }

				                            #menuinicial{
				                                position:fixed;
				                                width:100%;
				                                height:90px;
				                                background-color:#FFFFFF;
				                                color:#000000;
				                                clear:both;
				                                z-index: 100;
				                              }

				                            .notifications, footer .int li .notifications {
				                                background-color: #3ca3c1;
				                                color: #fff;
				                                position: absolute;
				                                left: 42%;
				                                font-weight: bold;
				                                padding: 4px;
				                                border-radius: 12px;
				                            }

				                            #css {

				                            box-shadow: 0 0 15px #ddd;
				                            background: #FFFFFF;
				                            }

				                            .btn-primary:hover {
				                                color: #fff;
				                                background-color: #286090;
				                                border-color: #204d74;
				                            }

				                            .btn.focus, .btn:focus, .btn:hover {
				                                color: #333;
				                                text-decoration: none;
				                            }
				                            a:focus, a:hover {
				                                color: #23527c;
				                                text-decoration: underline;
				                            }
				                            a:active, a:hover {
				                                outline: 0;
				                            }
				                            .btn-group-lg>.btn, .btn-lg {
				                                padding: 10px 16px;
				                                font-size: 18px;
				                                line-height: 1.3333333;
				                                border-radius: 6px;
				                            }
				                            .btn-primary {
				                                color: #fff;
				                                background-color: #337ab7;
				                                border-color: #2e6da4;
				                            }
				                            .btn {
				                                display: inline-block;
				                                padding: 6px 12px;
				                                margin-bottom: 0;
				                                font-size: 14px;
				                                font-weight: 400;
				                                line-height: 1.42857143;
				                                text-align: center;
				                                white-space: nowrap;
				                                vertical-align: middle;
				                                -ms-touch-action: manipulation;
				                                touch-action: manipulation;
				                                cursor: pointer;
				                                -webkit-user-select: none;
				                                -moz-user-select: none;
				                                -ms-user-select: none;
				                                user-select: none;
				                                background-image: none;
				                                border: 1px solid transparent;
				                                border-radius: 4px;
				                            }
				                            [role=button] {
				                                cursor: pointer;
				                            }


				                              </style>
				                              <script type="text/javascript">
				                                function init() {
				                                    window.addEventListener("scroll", function(e){
				                                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
				                                            shrinkOn = 300,
				                                            header = document.querySelector("header");
				                                        if (distanceY > shrinkOn) {
				                                            classie.add(header,"smaller");
				                                        } else {
				                                            if (classie.has(header,"smaller")) {
				                                                classie.remove(header,"smaller");
				                                            }
				                                        }
				                                    });
				                                }
				                                     window.onload = init();
				                              </script>
				                                
				                                
				                                
				                            </head>
				                            <body>
				                            <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
				                            <header>
				                                   <img src="' . base_url() . 'images/headerimg.png" width="100%" height="100px">
				                            </header>

				                            <div class="loader"></div>
				                            <div class="container-fluid">
				                                </br>
				                                </br>
				                                </br>
				                                </br>
				                                </br>
				                                <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
				                                <div class="col-md-12" height="150px" width="100%">
				                                </br>
				                                </br>
				                                <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
				                                </div>
				                                <div class="col-md-12" style="width:100%;height: 100px;">
				                                <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
				    background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
				                                </div>
				                                </div>
				                                </br>
				                                </br>
				                                </br>
				                                </br>
				                                <div class="col-md-12" style="width: 100%;">
				                                </br>
				                                </br>
				                                <hr style="border-top: 2px solid #0683c9;">
				                                </div>
				                                <div class="col-md-5" style="width: 100%;">
				                                <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

				                                <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
				                                <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
				                                <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
				                                <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
				                                <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
				                                 </p>

				                                </div>
				                                
				                            </div>
				                            </div>
				                            </div>  
				                            </body>
				                            </html>';

								            $configuraciones["mailtype"] = 'html';
											$this->email->initialize($configuraciones);
											$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
											$this->email->to($correo);
											$this->email->subject($titulo);
											$this->email->message($message);
											$this->email->send();


											
										} 
											
									}
								}
							}else{
								
							}
							
							
							break;
						case 'ENTREGA FISICO':
							
							break;
					}
				}
			}else{//Sin convenio de entrega
				if($tipo == "CONFOTOS"){
					if(count($jsonFotos) > 0){

						for ($i=0; $i < count($jsonFotos); $i++) {
							if($jsonFotos[$i] != null){
								$foto = $jsonFotos[$i]->imagen; 
								$tags = $jsonTags[$i]->id; 

								

								if($dataCR != null){
									$nombreCentro = $dataCR->result()[0]->name;
								}
					            
					            
					            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;

	
	                            $message = '<!DOCTYPE html>
	                            <html>
	                            <head>
	                              <meta charset="UTF-8">
	                              <title>Seleccion Perfil</title>
	                              <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	                              <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	                              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	                                <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
	                              <script type="text/javascript">
	                              $(window).load(function() {
	                                $(".loader").fadeOut("slow");
	                              })
	                              </script>
	                              <style type="text/css">
	                                .loader {
	                                  position: fixed;
	                                  left: 0px;
	                                  top: 0px;
	                                  width: 100%;
	                                  height: 100%;
	                                  z-index: 9999;
	                                  background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
	                                }

	                                header {
	                                width: 100%;
	                                top: 0;
	                                left: 0;
	                                z-index: 999;
	                            }
	                            header h1#logo {
	                                display: inline-block;
	                                height: 150px;
	                                line-height: 150px;
	                                float: left;
	                                font-family: "Oswald", sans-serif;
	                                font-size: 60px;
	                                color: white;
	                                font-weight: 400;
	                                -webkit-transition: all 0.3s;
	                                -moz-transition: all 0.3s;
	                                -ms-transition: all 0.3s;
	                                -o-transition: all 0.3s;
	                                transition: all 0.3s;
	                            }
	                            header nav {
	                                display: inline-block;
	                                float: right;
	                            }
	                            header nav a {
	                                line-height: 150px;
	                                margin-left: 20px;
	                                color: #9fdbfc;
	                                font-weight: 700;
	                                font-size: 18px;
	                                -webkit-transition: all 0.3s;
	                                -moz-transition: all 0.3s;
	                                -ms-transition: all 0.3s;
	                                -o-transition: all 0.3s;
	                                transition: all 0.3s;
	                            }
	                            header nav a:hover {
	                                color: white;
	                            }
	                            header.smaller {
	                                height: 75px;
	                            }
	                            header.smaller h1#logo {
	                                width: 150px;
	                                height: 75px;
	                                line-height: 75px;
	                                font-size: 30px;
	                            }
	                            header.smaller nav a {
	                                line-height: 75px;
	                            }

	                            @media all and (max-width: 660px) {
	                                header h1#logo {
	                                    display: block;
	                                    float: none;
	                                    margin: 0 auto;
	                                    height: 100px;
	                                    line-height: 100px;
	                                    text-align: center;
	                                }
	                                header nav {
	                                    display: block;
	                                    float: none;
	                                    height: 50px;
	                                    text-align: center;
	                                    margin: 0 auto;
	                                }
	                                header nav a {
	                                    line-height: 50px;
	                                    margin: 0 10px;
	                                }
	                                header.smaller {
	                                    height: 75px;
	                                }
	                                header.smaller h1#logo {
	                                    height: 40px;
	                                    line-height: 40px;
	                                    font-size: 30px;
	                                }
	                                header.smaller nav {
	                                    height: 35px;
	                                }
	                                header.smaller nav a {
	                                    line-height: 35px;
	                                }
	                            }
	                            #footer{
	                                position:absolute;
	                                width:100%;
	                                height:90px;
	                                background-color:#FFFFFF;
	                                color:#000000;
	                                bottom:0px;
	                                clear:both;
	                              }

	                             .media
	                                {
	                                    /*box-shadow:0px 0px 4px -2px #000;*/
	                                    margin: 20px 0;
	                                    padding:30px;
	                                }
	                                .dp
	                                {
	                                    border:10px solid #eee;
	                                    transition: all 0.2s ease-in-out;
	                                }
	                                .dp:hover
	                                {
	                                    border:2px solid #eee;
	                                    
	                                }



	                                .nav {
	                                left:50%;
	                                margin-left:-150px;
	                                top:50px;
	                                position:absolute;
	                            }
	                            .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
	                                background:#fff;
	                            }
	                            .dropdown {
	                                border-radius:4px;
	                                width:300px;    
	                            }
	                            .dropdown-menu>li>a {
	                                color:#428bca;
	                            }
	                            .dropdown ul.dropdown-menu {
	                                border-radius:4px;
	                                box-shadow:none;
	                                margin-top:20px;
	                                width:300px;
	                            }
	                            .dropdown ul.dropdown-menu:before {
	                                content: "";
	                                border-bottom: 10px solid #fff;
	                                border-right: 10px solid transparent;
	                                border-left: 10px solid transparent;
	                                position: absolute;
	                                top: -10px;
	                                right: 16px;
	                                z-index: 10;
	                            }
	                            .dropdown ul.dropdown-menu:after {
	                                content: "";
	                                border-bottom: 12px solid #ccc;
	                                border-right: 12px solid transparent;
	                                border-left: 12px solid transparent;
	                                position: absolute;
	                                top: -12px;
	                                right: 14px;
	                                z-index: 9;
	                            }




	                            .profile 
	                            {
	                                min-height: 300px;
	                                display: inline-block;
	                                }
	                            figcaption.ratings
	                            {
	                                margin-top:20px;
	                                }
	                            figcaption.ratings a
	                            {
	                                color:#f1c40f;
	                                font-size:11px;
	                                }
	                            figcaption.ratings a:hover
	                            {
	                                color:#f39c12;
	                                text-decoration:none;
	                                }
	                            .divider 
	                            {
	                                border-top:1px solid rgba(0,0,0,0.1);
	                                }
	                            .emphasis 
	                            {
	                                border-top: 4px solid transparent;
	                                }
	                            .emphasis:hover 
	                            {
	                                border-top: 4px solid #1abc9c;
	                                }
	                            .emphasis h2
	                            {
	                                margin-bottom:0;
	                                }

	                            span.tags 
	                            {
	                                background: #1abc9c;
	                                border-radius: 2px;
	                                color: #f5f5f5;
	                                font-weight: bold;
	                                padding: 2px 4px;
	                                }

	                            .row {
	                                margin-left: -130px;
	                            }

	                            .col-fixed {
	                                /* custom width */
	                                width:320px;
	                            }
	                            .col-min {
	                                /* custom min width */
	                                min-width:320px;
	                            }
	                            .col-max {
	                                /* custom max width */
	                                max-width:320px;
	                            }

	                            #menuinicial{
	                                position:fixed;
	                                width:100%;
	                                height:90px;
	                                background-color:#FFFFFF;
	                                color:#000000;
	                                clear:both;
	                                z-index: 100;
	                              }

	                            .notifications, footer .int li .notifications {
	                                background-color: #3ca3c1;
	                                color: #fff;
	                                position: absolute;
	                                left: 42%;
	                                font-weight: bold;
	                                padding: 4px;
	                                border-radius: 12px;
	                            }

	                            #css {

	                            box-shadow: 0 0 15px #ddd;
	                            background: #FFFFFF;
	                            }

	                            .btn-primary:hover {
	                                color: #fff;
	                                background-color: #286090;
	                                border-color: #204d74;
	                            }

	                            .btn.focus, .btn:focus, .btn:hover {
	                                color: #333;
	                                text-decoration: none;
	                            }
	                            a:focus, a:hover {
	                                color: #23527c;
	                                text-decoration: underline;
	                            }
	                            a:active, a:hover {
	                                outline: 0;
	                            }
	                            .btn-group-lg>.btn, .btn-lg {
	                                padding: 10px 16px;
	                                font-size: 18px;
	                                line-height: 1.3333333;
	                                border-radius: 6px;
	                            }
	                            .btn-primary {
	                                color: #fff;
	                                background-color: #337ab7;
	                                border-color: #2e6da4;
	                            }
	                            .btn {
	                                display: inline-block;
	                                padding: 6px 12px;
	                                margin-bottom: 0;
	                                font-size: 14px;
	                                font-weight: 400;
	                                line-height: 1.42857143;
	                                text-align: center;
	                                white-space: nowrap;
	                                vertical-align: middle;
	                                -ms-touch-action: manipulation;
	                                touch-action: manipulation;
	                                cursor: pointer;
	                                -webkit-user-select: none;
	                                -moz-user-select: none;
	                                -ms-user-select: none;
	                                user-select: none;
	                                background-image: none;
	                                border: 1px solid transparent;
	                                border-radius: 4px;
	                            }
	                            [role=button] {
	                                cursor: pointer;
	                            }


	                              </style>
	                              <script type="text/javascript">
	                                function init() {
	                                    window.addEventListener("scroll", function(e){
	                                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
	                                            shrinkOn = 300,
	                                            header = document.querySelector("header");
	                                        if (distanceY > shrinkOn) {
	                                            classie.add(header,"smaller");
	                                        } else {
	                                            if (classie.has(header,"smaller")) {
	                                                classie.remove(header,"smaller");
	                                            }
	                                        }
	                                    });
	                                }
	                                     window.onload = init();
	                              </script>
	                                
	                                
	                                
	                            </head>
	                            <body>
	                            <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
	                            <header>
	                                   <img src="' . base_url() . 'images/headerimg.png" width="100%"   height="100px">
	                            </header>

	                            <div class="loader"></div>
	                            <div class="container-fluid">
	                                </br>
	                                </br>
	                                </br>
	                                </br>
	                                </br>
	                                <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
	                                <div class="col-md-12" height="150px" width="100%">
	                                </br>
	                                </br>
	                                <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
	                                </div>
	                                <div class="col-md-12" style="width:100%;height: 100px;">
	                                <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
	    background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
	                                </div>
	                                </div>
	                                </br>
	                                </br>
	                                </br>
	                                </br>
	                                <div class="col-md-12" style="width: 100%;">
	                                </br>
	                                </br>
	                                <hr style="border-top: 2px solid #0683c9;">
	                                </div>
	                                <div class="col-md-5" style="width: 100%;">
	                                <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

	                                <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
	                                <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
	                                <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
	                                <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
	                                <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
	                                 </p>

	                                </div>
	                                
	                            </div>
	                            </div>
	                            </div>  
	                            </body>
	                            </html>';

					            $configuraciones["mailtype"] = 'html';
								$this->email->initialize($configuraciones);
								$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
								$this->email->to($correo);
								$this->email->subject($titulo);
								$this->email->message($message);
								$this->email->send();


								
							} 
								
						}
					}
				}else{
					
				}
			}
			
			
		}else{
			$estado = "ACTIVO";
			if($tipo == "CONFOTOS"){
				if(count($jsonFotos) > 0){

					for ($i=0; $i < count($jsonFotos); $i++) {
						if($jsonFotos[$i] != null){
							$foto = $jsonFotos[$i]->imagen; 
							$tags = $jsonTags[$i]->id; 

							

							if($dataCR != null){
								$nombreCentro = $dataCR->result()[0]->name;
							}
				            
				            
				            $titulo = 'Resultados de Imagenes del Centro Radiologico ' . $nombreCentro;


                            $message = '<!DOCTYPE html>
                            <html>
                            <head>
                              <meta charset="UTF-8">
                              <title>Seleccion Perfil</title>
                              <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
                              <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                                <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
                              <script type="text/javascript">
                              $(window).load(function() {
                                $(".loader").fadeOut("slow");
                              })
                              </script>
                              <style type="text/css">
                                .loader {
                                  position: fixed;
                                  left: 0px;
                                  top: 0px;
                                  width: 100%;
                                  height: 100%;
                                  z-index: 9999;
                                  background: url("' . base_url() . 'images/page-loader.gif") 50% 50% no-repeat rgb(249,249,249);
                                }

                                header {
                                width: 100%;
                                top: 0;
                                left: 0;
                                z-index: 999;
                            }
                            header h1#logo {
                                display: inline-block;
                                height: 150px;
                                line-height: 150px;
                                float: left;
                                font-family: "Oswald", sans-serif;
                                font-size: 60px;
                                color: white;
                                font-weight: 400;
                                -webkit-transition: all 0.3s;
                                -moz-transition: all 0.3s;
                                -ms-transition: all 0.3s;
                                -o-transition: all 0.3s;
                                transition: all 0.3s;
                            }
                            header nav {
                                display: inline-block;
                                float: right;
                            }
                            header nav a {
                                line-height: 150px;
                                margin-left: 20px;
                                color: #9fdbfc;
                                font-weight: 700;
                                font-size: 18px;
                                -webkit-transition: all 0.3s;
                                -moz-transition: all 0.3s;
                                -ms-transition: all 0.3s;
                                -o-transition: all 0.3s;
                                transition: all 0.3s;
                            }
                            header nav a:hover {
                                color: white;
                            }
                            header.smaller {
                                height: 75px;
                            }
                            header.smaller h1#logo {
                                width: 150px;
                                height: 75px;
                                line-height: 75px;
                                font-size: 30px;
                            }
                            header.smaller nav a {
                                line-height: 75px;
                            }

                            @media all and (max-width: 660px) {
                                header h1#logo {
                                    display: block;
                                    float: none;
                                    margin: 0 auto;
                                    height: 100px;
                                    line-height: 100px;
                                    text-align: center;
                                }
                                header nav {
                                    display: block;
                                    float: none;
                                    height: 50px;
                                    text-align: center;
                                    margin: 0 auto;
                                }
                                header nav a {
                                    line-height: 50px;
                                    margin: 0 10px;
                                }
                                header.smaller {
                                    height: 75px;
                                }
                                header.smaller h1#logo {
                                    height: 40px;
                                    line-height: 40px;
                                    font-size: 30px;
                                }
                                header.smaller nav {
                                    height: 35px;
                                }
                                header.smaller nav a {
                                    line-height: 35px;
                                }
                            }
                            #footer{
                                position:absolute;
                                width:100%;
                                height:90px;
                                background-color:#FFFFFF;
                                color:#000000;
                                bottom:0px;
                                clear:both;
                              }

                             .media
                                {
                                    /*box-shadow:0px 0px 4px -2px #000;*/
                                    margin: 20px 0;
                                    padding:30px;
                                }
                                .dp
                                {
                                    border:10px solid #eee;
                                    transition: all 0.2s ease-in-out;
                                }
                                .dp:hover
                                {
                                    border:2px solid #eee;
                                    
                                }



                                .nav {
                                left:50%;
                                margin-left:-150px;
                                top:50px;
                                position:absolute;
                            }
                            .nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
                                background:#fff;
                            }
                            .dropdown {
                                border-radius:4px;
                                width:300px;    
                            }
                            .dropdown-menu>li>a {
                                color:#428bca;
                            }
                            .dropdown ul.dropdown-menu {
                                border-radius:4px;
                                box-shadow:none;
                                margin-top:20px;
                                width:300px;
                            }
                            .dropdown ul.dropdown-menu:before {
                                content: "";
                                border-bottom: 10px solid #fff;
                                border-right: 10px solid transparent;
                                border-left: 10px solid transparent;
                                position: absolute;
                                top: -10px;
                                right: 16px;
                                z-index: 10;
                            }
                            .dropdown ul.dropdown-menu:after {
                                content: "";
                                border-bottom: 12px solid #ccc;
                                border-right: 12px solid transparent;
                                border-left: 12px solid transparent;
                                position: absolute;
                                top: -12px;
                                right: 14px;
                                z-index: 9;
                            }




                            .profile 
                            {
                                min-height: 300px;
                                display: inline-block;
                                }
                            figcaption.ratings
                            {
                                margin-top:20px;
                                }
                            figcaption.ratings a
                            {
                                color:#f1c40f;
                                font-size:11px;
                                }
                            figcaption.ratings a:hover
                            {
                                color:#f39c12;
                                text-decoration:none;
                                }
                            .divider 
                            {
                                border-top:1px solid rgba(0,0,0,0.1);
                                }
                            .emphasis 
                            {
                                border-top: 4px solid transparent;
                                }
                            .emphasis:hover 
                            {
                                border-top: 4px solid #1abc9c;
                                }
                            .emphasis h2
                            {
                                margin-bottom:0;
                                }

                            span.tags 
                            {
                                background: #1abc9c;
                                border-radius: 2px;
                                color: #f5f5f5;
                                font-weight: bold;
                                padding: 2px 4px;
                                }

                            .row {
                                margin-left: -130px;
                            }

                            .col-fixed {
                                /* custom width */
                                width:320px;
                            }
                            .col-min {
                                /* custom min width */
                                min-width:320px;
                            }
                            .col-max {
                                /* custom max width */
                                max-width:320px;
                            }

                            #menuinicial{
                                position:fixed;
                                width:100%;
                                height:90px;
                                background-color:#FFFFFF;
                                color:#000000;
                                clear:both;
                                z-index: 100;
                              }

                            .notifications, footer .int li .notifications {
                                background-color: #3ca3c1;
                                color: #fff;
                                position: absolute;
                                left: 42%;
                                font-weight: bold;
                                padding: 4px;
                                border-radius: 12px;
                            }

                            #css {

                            box-shadow: 0 0 15px #ddd;
                            background: #FFFFFF;
                            }

                            .btn-primary:hover {
                                color: #fff;
                                background-color: #286090;
                                border-color: #204d74;
                            }

                            .btn.focus, .btn:focus, .btn:hover {
                                color: #333;
                                text-decoration: none;
                            }
                            a:focus, a:hover {
                                color: #23527c;
                                text-decoration: underline;
                            }
                            a:active, a:hover {
                                outline: 0;
                            }
                            .btn-group-lg>.btn, .btn-lg {
                                padding: 10px 16px;
                                font-size: 18px;
                                line-height: 1.3333333;
                                border-radius: 6px;
                            }
                            .btn-primary {
                                color: #fff;
                                background-color: #337ab7;
                                border-color: #2e6da4;
                            }
                            .btn {
                                display: inline-block;
                                padding: 6px 12px;
                                margin-bottom: 0;
                                font-size: 14px;
                                font-weight: 400;
                                line-height: 1.42857143;
                                text-align: center;
                                white-space: nowrap;
                                vertical-align: middle;
                                -ms-touch-action: manipulation;
                                touch-action: manipulation;
                                cursor: pointer;
                                -webkit-user-select: none;
                                -moz-user-select: none;
                                -ms-user-select: none;
                                user-select: none;
                                background-image: none;
                                border: 1px solid transparent;
                                border-radius: 4px;
                            }
                            [role=button] {
                                cursor: pointer;
                            }


                              </style>
                              <script type="text/javascript">
                                function init() {
                                    window.addEventListener("scroll", function(e){
                                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                                            shrinkOn = 300,
                                            header = document.querySelector("header");
                                        if (distanceY > shrinkOn) {
                                            classie.add(header,"smaller");
                                        } else {
                                            if (classie.has(header,"smaller")) {
                                                classie.remove(header,"smaller");
                                            }
                                        }
                                    });
                                }
                                     window.onload = init();
                              </script>
                                
                                
                                
                            </head>
                            <body>
                            <div class="container-fluid" style="margin-top:50px;margin-left:50px;margin-right:50px;">
                            <header>
                                   <img src="' . base_url() . 'images/headerimg.png" width="100%"   height="100px">
                            </header>

                            <div class="loader"></div>
                            <div class="container-fluid">
                                </br>
                                </br>
                                </br>
                                </br>
                                </br>
                                <img id="css" src="' . base_url() . 'images/envio.png" width="100%">
                                <div class="col-md-12" height="150px" width="100%">
                                </br>
                                </br>
                                <p align="center"><font color="#3ca3c1" style="font-weight:bold">A trav&eacute;s de nuestro servicio de visualizac&iacute;on de im&aacute;genes, el Centro Radiologico ' . $nombreCentro . ' ha querido compartir con usted la siguente informaci&oacute;n gr&aacute;fica.</font></p>
                                </div>
                                <div class="col-md-12" style="width:100%;height: 100px;">
                                <a href="http://190.60.211.17/hco/imagenescompartidasCentroRadiologico.php?usuario='.$document.'&datos='.$idDetalle.'" class="btn btn-primary btn-lg" role="button" style="float: right!important;color: #fff;
    background-color: #337ab7; border-color: #2e6da4;padding: 10px 16px;font-size: 18px;line-height: 1.3333333;border-radius: 6px;font-weight:bold">VER INFORMACION</a>
                                </div>
                                </div>
                                </br>
                                </br>
                                </br>
                                </br>
                                <div class="col-md-12" style="width: 100%;">
                                </br>
                                </br>
                                <hr style="border-top: 2px solid #0683c9;">
                                </div>
                                <div class="col-md-5" style="width: 100%;">
                                <p align="left"><font color="#3ca3c1" style="font-weight:bold">Cont&aacute;ctenos: 0180001124587 Bogot&aacute; Colombia</font><img src="' . base_url() . 'images/tel.jpeg"  height="45px" width="45px">

                                <img style="float: right!important;" src="' . base_url() . 'images/10.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/11.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/12.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/13.png"  height="45px" width="45px"> 
                                <img style="float: right!important;" src="' . base_url() . 'images/14.png"  height="45px" width="45px">
                                 </p>

                                </div>
                                
                            </div>
                            </div>
                            </div>  
                            </body>
                            </html>';

				            $configuraciones["mailtype"] = 'html';
							$this->email->initialize($configuraciones);
							$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
							$this->email->to($correo);
							$this->email->subject($titulo);
							$this->email->message($message);
							$this->email->send();


							
						} 
							
					}
				}
			}else{
				
			}
		}

		$dataUsuario = $this->user_radiologia_model->obtenerPacienteDocumento($document);
		if($dataUsuario != null){
			$datosOM = $this->ordenes_medicas_model->getId($orden_medica);
			if($datosOM != null){
				$messageProfesional = "Se han ingresado imagenes de la persona " . $dataUsuario->result()[0]->first_name . " " . $dataUsuario->result()[0]->last_name . " en la orden medica con fecha " . $datosOM->result()[0]->fecha . " registrada con su correo electronico";
				$configuraciones["mailtype"] = 'html';
				$this->email->initialize($configuraciones);
				$this->email->from('info@ordenes', 'Informacion del Centro Radiologico');
				$this->email->to($datosOM->result()[0]->usuario);
				$this->email->subject("Mensaje del Centro Radiologico");
				$this->email->message($messageProfesional);
				$this->email->send();		
			}
		}
		

        $this->factura_radiologia_model->actualizar($document, $IDCRInternoCR, "false");
		$this->turnos_model->actualizar($document, $IDCRInternoCR);

		$this->session->unset_userdata("document_Seleccionado");
		$this->session->unset_userdata("idFactura_Seleccionada");
		
		$arrayDetalle = array(
			'id' => $orden_medica,
			'estado' => "PROCESADO");
		$this->ordenes_medicas_model->updateDetalleID($arrayDetalle);	    
	}

	public function listTags()	{
		$data = $this->tags_model->listar();
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function crearTag()	{
		$tag = $_POST['name']; 
		$this->tags_model->crear($tag);
	}

	public function pdfFacturaRadiologia(){
		$document = $_GET['identificacion']; 
   		$idFactura = $_GET['idFactura']; 
   		$consecutivo = $_GET['consecutivo']; 
   		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Unisoft System Corporation');
		$pdf->SetTitle('FACTURA ' . $idFactura);
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(12, 12, 12);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();
		$funciones = new PDF();

		/* NOTE:
		 * *********************************************************
		 * You can load external XHTML using :
		 *
		 * $html = file_get_contents('/path/to/your/file.html');
		 *
		 * External CSS files will be automatically loaded.
		 * Sometimes you need to fix the path of the external CSS.
		 * *********************************************************
		 */
		$html = "";
		$data = $this->user_radiologia_model->obtenerPacienteDocumento($document);
   		if($data != null){
   			foreach ($data->result() as $keyUser) {
   				$total = 0;
   				$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);

   				$htmlTabla = '';
   				$dataDetalle = $this->factura_radiologia_model->getDetalle($idFactura);
				if($dataDetalle != null){
					foreach ($dataDetalle->result() as $value) {
						/*$htmlTabla .= '<tr>
					        <td style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="25%">' . $value->descripcion . '</td>
					        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%">' . number_format($value->cantidad,0, ',', '.') . '</td>
					        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%">' . $value->convenio . '</td>
					        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%">' . $value->descuento . '</td>
					        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%">' . number_format($value->precio,0, ',', '.') . '</td>
					        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%">' . number_format($value->total,0, ',', '.') . '</td>
					      </tr>
					      <tr>
					        <td colspan="6" style="padding: 0px; border: none"></td>
					      </tr>';*/
						$total += $value->total;
					}
				}
				$letras = $funciones->numtoletras($total);
				$total = number_format($total,0, ',', '.');
				// define some HTML content with style

				$dataPago = $this->pagos_radiologia_model->getPago($idFactura);

				/*$html = '
				<meta charset="utf-8">				
				<style>
			   		.labelDato{
				      color: #0683C9; 
				      font-size: 8px; 
				      font-weight: bolder;
				      padding: 0px;
				    }
				    .Dato{
				      color: #9B9FA1;  
				      font-size: 10px;
				    }
				    #tabla > thead > tr > td{
				      color: #0683C9; 
				      font-size: 10px;
				      font-weight: bolder;
				    }
				    #tabla td{
				      font-weight: bolder;
				      color: #0683C9; 
				      font-size: 10px;
				      font-weight: bolder;	
				    }
				    *{
				      font-family: "Arial";
				      font-size: 10px;
				      padding: 0px;
				      margin: 0px;
				    }
				</style>

				<h2 align="right" style="color: #0683C9; margin-right: 50px; font-size: 20px;"><i>Factura No. ' . $idFactura . '</i></h2><br>

				<table width="100%" style="font-size: 9px" border="0">
					<tr>
						<td width="40%" valign="top">
							<img src="' . $dataCR->result()[0]->logo . '" width="280px" style="margin-top: -250px">
							<p>
							Resolución DIAN ' . $dataCR->result()[0]->resolucion . '<br>
							dE ' . $dataCR->result()[0]->fecha_resolucion . ' Rango ' . $dataCR->result()[0]->rango_resolucion . '
							</p>
						</td>
						<td width="60%" valign="top">
							
							
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="38%" align="right"><label class="labelDato">No. IDENTIFICACION</label></td>
									<td width="2%">&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left" style="padding: 5px">
								          	' . $document . '
								        </div>
							        </td>
								</tr>
								<tr>
									<td width="38%" align="right"><label class="labelDato">NOMBRE</label></td>
									<td>&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left" style="border-radius: 14px">
								          ' . $keyUser->first_name . " " . $keyUser->last_name . '
								        </div>
							        </td>
								</tr>
								<tr>
									<td width="38%" align="right"><label class="labelDato">DIRECCION</label></td>
									<td>&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left">
								          ' . $keyUser->lugar_residencia . '
								        </div>
							        </td>
								</tr>
								<tr>
									<td width="38%" align="right"><label class="labelDato">TELEFONO</label></td>
									<td>&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left">
								          ' . $keyUser->telefono . " - " . $keyUser->celular . '
								        </div>
							        </td>
								</tr>
								<tr>
									<td width="38%" align="right"><label class="labelDato">EMAIL</label></td>
									<td>&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left">
								          ' . $keyUser->email . '
								        </div>
							        </td>
								</tr>
								<tr>
									<td width="38%" align="right"><label class="labelDato">FORMA DE PAGO</label></td>
									<td>&nbsp;</td>
									<td width="60%">
										<div class="Dato" align="left">
								           ' . $dataPago->result()[0]->tipopago . '
								        </div>
							        </td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				<table width="100%">
			  		<tr>
			  			<td height="5px">&nbsp;</td>
			  		</tr>
			  	</table>

				<div style="width: 100%; height: 1px; border-top: 1px solid #0683C9;">&nbsp;</div>

				<table width="100%" id="tabla" style="font-size: 9px">
				    <thead>
				      <tr>
				        <td style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="25%">DESCRIPCION</td>
				        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">CANTIDAD</td>
				        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">CONVENIO</td>
				        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">DESCUENTO</td>
				        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">VR.UNITARIO</td>
				        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">SUBTOTAL</td>
				      </tr>
				      <tr>
				        <td colspan="6" style="padding: 0px; border: none;"></td>
				      </tr>
				    </thead>

				    <tbody>
				    	' . $htmlTabla . '  
				    </tbody>    

			  	</table>

			  	<table width="100%">
			  		<tr>
			  			<td height="80px">&nbsp;</td>
			  		</tr>
			  	</table>

			  	<table width="100%">
			  		<tr>
			  			<td width="50%">
			  				<div align="center" style="border: 1px solid #9B9FA1; color: #9B9FA1; padding: 10px; width: 100%">
			  					<div style="width: 100%; height: 40px">&nbsp;</div>
			  					<table width="100%">
			  						<tr>
			  							<td width="8%">&nbsp;</td>
			  							<td width="84%"><label style="margin-left: 10px; margin-top: 10px">Son: ' . $letras . '</label></td>
			  							<td width="8%">&nbsp;</td>
			  						</tr>
			  					</table>
						      	<div style="width: 100%; height: 40px">&nbsp;</div>	
						    </div>
			  			</td>
			  			<td width="50%" valign="middle">
			  				<div style="width: 100%; height: 70px">&nbsp;</div>	
			  				<table width="100%">
								<tr>
									<td width="28%" align="right"><label class="labelDato">VALOR TOTAL</label></td>
									<td width="2%">&nbsp;</td>
									<td width="70%">
										<div class="col-md-8 Dato" align="center">
								          <label class="labelDato">$' . $total . '</label>
								        </div>
							        </td>
								</tr>
							</table>
			  			</td>
			  		</tr>
			  	</table>
			    
			    <table width="100%">
			  		<tr>
			  			<td height="100px">&nbsp;</td>
			  		</tr>
			  	</table>

			  	<table width="100%">
			  		<tr>
			  			<td width="46%">
			  				<div style="color: #9B9FA1; border-top: 1px solid #0683C9; padding-top: 5px; font-weight: bolder; margin-left: 20px">	
						      	Firma de Funcionario
						    </div>
					    </td>
					    <td width="8%">&nbsp;</td>
					    <td width="46%">
					    	<div style="color: #9B9FA1; border-top: 1px solid #0683C9; padding-top: 5px; font-weight: bolder; margin-left: 120px">
						      Firma de Cliente
						    </div>
					    </td>
			  		</tr>
			  	</table>

				
				<br />';*/
				$datos_layout['centro'] = $dataCR;
				$datos_layout['id_factura'] = $consecutivo;
				$datos_layout['total_letras'] = $letras;
				$datos_layout['total'] = $total;
				$datos_layout['detalle_factura'] = $dataDetalle->result();
				$datos_layout['data_user'] = $keyUser;
				$datos_layout['tipo_pago'] = $dataPago->result()[0]->tipopago;
				$html = $this->load->view('asistente/layout_pdf_factura', $datos_layout, true);
			}
		}

		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		$nombreArchivo = "Factura" . date("Y") . date("m") . date("d") . date("H") . date("i") . date("s") . date("u") . ".pdf";
		//Close and output PDF document
		$pdf->Output($nombreArchivo, 'I');
		//echo gettype($html);	
	}

	/*public function pdfFacturaRadiologia()	{
		$document = $_GET['identificacion']; 
   		$idFactura = $_GET['idFactura']; 
   		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

   		$data = $this->user_radiologia_model->obtenerPacienteDocumento($document);
   		if($data != null){
   			foreach ($data->result() as $keyUser) {
   				$total = 0;
   				$pdf = new PDF();
				//Primera página
				$pdf->AddPage();

				$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);
				$pdf->Image($dataCR->result()[0]->logo, 5, 10, 60, 'PNG', '');
				// Texto centrado en una celda con cuadro 20*10 mm y salto de línea
				$pdf->SetFont('Arial','I',18);
				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(131);
				$pdf->Cell(60,0,'Recibo de pago No. ' . $idFactura,0,1,'C');
				$pdf->Ln(10);	

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetFont('Arial','B',8);
				$pdf->SetX(40);
				$pdf->Cell(80,5,'No. IDENTIFICACION',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$document,0,0,'C');
				$pdf->RoundedRect(120, 20, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(40);
				$pdf->Cell(80,5,'NOMBRE',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$keyUser->first_name . " " . $keyUser->last_name,0,0,'C');
				$pdf->RoundedRect(120, 26, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(40);
				$pdf->Cell(80,5,'DIRECCION',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$keyUser->lugar_residencia,0,0,'C');
				$pdf->RoundedRect(120, 32, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(40);
				$pdf->Cell(80,5,'TELEFONO',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$keyUser->telefono . " - " . $keyUser->celular,0,0,'C');
				$pdf->RoundedRect(120, 38, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(40);
				$pdf->Cell(80,5,'EMAIL',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$keyUser->email,0,1,'C');
				$pdf->RoundedRect(120, 44, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(40);
				$pdf->Cell(80,5,'EMAIL',0,0,'R');
				$pdf->SetTextColor(155, 159, 161);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->SetX(120);
				$pdf->Cell(80,5,$keyUser->email,0,1,'C');
				$pdf->RoundedRect(120, 50, 80, 5, 1, '1234', '');
				$pdf->Ln(6);

				
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->Line(10,54,200,54);

				$pdf->Ln(2);
				$header = array('CANTIDAD', 'CONVENIO', 'DESCUENTO', 'VR.UNITARIO', 'SUBTOTAL');
				$pdf->HeaderTabla($header);	
				$pdf->Ln(6);

				$dataDetalle = $this->factura_radiologia_model->getDetalle($idFactura);
				if($dataDetalle != null){
					foreach ($dataDetalle->result() as $value) {
						$datos = array(number_format($value->cantidad,0, ',', '.'), $value->convenio, $value->descuento, number_format($value->precio,0, ',', '.'), number_format($value->total,0, ',', '.'));
						$pdf->Tabla($datos, $value->descripcion);
						$total += $value->total;
					}
				}
				
				$total = number_format($total,0, ',', '.');

				$pdf->Ln(20);
				$pdf->SetTextColor(6, 131, 201); //Letra color blanco
				$pdf->SetX(115);
				$pdf->Cell(25,5,"VALOR TOTAL",0,0,'R');		
				$pdf->Cell(60,5,$total,1,0,'C',0);	

				$pdf->SetTextColor(204, 204, 204); //Letra color blanco
			 	$pdf->SetDrawColor(204, 204, 204);
				$pdf->SetX(10);
				$pdf->SetMargins(0, 0, 0);
				$pdf->Cell(80,10,"Son: " . $pdf->numtoletras($total) . " pesos M-L",1,1,'C');

				$pdf->Ln(30);
				$pdf->SetX(15);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->Cell(70,8,"Firma de Funcionario","T",0,'T');

				$pdf->SetX(120);
			 	$pdf->SetDrawColor(6, 131, 201);
				$pdf->Cell(70,8,"Firma de Cliente","T",0,'T');	
				$pdf->Output();
   			}
   		}
	}*/
}

class PDF/* extends FPDF */{	
    /*function HeaderTabla($header) {
    	$this->SetTextColor(6, 131, 201); //Letra color blanco    	
    	$this->Cell(60,5,"DESCRIPCION","R",0,'L');	
	    foreach($header as $col){
			$this->SetTextColor(6, 131, 201); //Letra color blanco
	    	$this->Cell(25,5,$col,"R",0,'C');		    
	   	}
	    
   	}

   	function Tabla($header, $descripcion) {
    	$this->SetTextColor(0, 0, 0); //Letra color blanco    	
    	$this->Cell(60,5,$descripcion,"R",0,'L');		
    	$con = 0;
	    foreach($header as $col){
			$this->SetTextColor(6, 131, 201); //Letra color blanco
			if($con == 4){
				$this->Cell(25,5,$col,"R",1,'C');		
			}else{
				$this->Cell(25,5,$col,"R",0,'C');		
			}
	    	$con++;
	   	}
   	}

   	function WordWrap(&$text, $maxwidth)
	{
	    $text = trim($text);
	    if ($text==='')
	        return 0;
	    $space = $this->GetStringWidth(' ');
	    $lines = explode("\n", $text);
	    $text = '';
	    $count = 0;

	    foreach ($lines as $line)
	    {
	        $words = preg_split('/ +/', $line);
	        $width = 0;

	        foreach ($words as $word)
	        {
	            $wordwidth = $this->GetStringWidth($word);
	            if ($wordwidth > $maxwidth)
	            {
	                // Word is too long, we cut it
	                for($i=0; $i<strlen($word); $i++)
	                {
	                    $wordwidth = $this->GetStringWidth(substr($word, $i, 1));
	                    if($width + $wordwidth <= $maxwidth)
	                    {
	                        $width += $wordwidth;
	                        $text .= substr($word, $i, 1);
	                    }
	                    else
	                    {
	                        $width = $wordwidth;
	                        $text = rtrim($text)."\n".substr($word, $i, 1);
	                        $count++;
	                    }
	                }
	            }
	            elseif($width + $wordwidth <= $maxwidth)
	            {
	                $width += $wordwidth + $space;
	                $text .= $word.' ';
	            }
	            else
	            {
	                $width = $wordwidth + $space;
	                $text = rtrim($text)."\n".$word.' ';
	                $count++;
	            }
	        }
	        $text = rtrim($text)."\n";
	        $count++;
	    }
	    $text = rtrim($text);
	    return $count;
	}

   	function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }*/

    function numtoletras($xcifra) {
	    $xarray = array(0 => "Cero",
	        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
	        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
	        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
	        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
	    );
		//
	    $xcifra = trim($xcifra);
	    $xlength = strlen($xcifra);
	    $xpos_punto = strpos($xcifra, ".");
	    $xaux_int = $xcifra;
	    $xdecimales = "00";
	    if (!($xpos_punto === false)) {
	        if ($xpos_punto == 0) {
	            $xcifra = "0" . $xcifra;
	            $xpos_punto = strpos($xcifra, ".");
	        }
	        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
	        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
	    }
	 
	    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
	    $xcadena = "";
	    for ($xz = 0; $xz < 3; $xz++) {
	        $xaux = substr($XAUX, $xz * 6, 6);
	        $xi = 0;
	        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
	        $xexit = true; // bandera para controlar el ciclo del While
	        while ($xexit) {
	            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
	                break; // termina el ciclo
	            }
	 
	            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
	            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
	            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
	                switch ($xy) {
	                    case 1: // checa las centenas
	                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
	                             
	                        } else {
	                            $key = (int) substr($xaux, 0, 3);
	                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
	                                $xseek = $xarray[$key];
	                                $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
	                                if (substr($xaux, 0, 3) == 100)
	                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
	                            }
	                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
	                                $key = (int) substr($xaux, 0, 1) * 100;
	                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
	                                $xcadena = " " . $xcadena . " " . $xseek;
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 0, 3) < 100)
	                        break;
	                    case 2: // checa las decenas (con la misma lógica que las centenas)
	                        if (substr($xaux, 1, 2) < 10) {
	                             
	                        } else {
	                            $key = (int) substr($xaux, 1, 2);
	                            if (TRUE === array_key_exists($key, $xarray)) {
	                                $xseek = $xarray[$key];
	                                $xsub = $this->subfijo($xaux);
	                                if (substr($xaux, 1, 2) == 20)
	                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                                $xy = 3;
	                            }
	                            else {
	                                $key = (int) substr($xaux, 1, 1) * 10;
	                                $xseek = $xarray[$key];
	                                if (20 == substr($xaux, 1, 1) * 10)
	                                    $xcadena = " " . $xcadena . " " . $xseek;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 1, 2) < 10)
	                        break;
	                    case 3: // checa las unidades
	                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
	                             
	                        } else {
	                            $key = (int) substr($xaux, 2, 1);
	                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
	                            $xsub = $this->subfijo($xaux);
	                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                        } // ENDIF (substr($xaux, 2, 1) < 1)
	                        break;
	                } // END SWITCH
	            } // END FOR
	            $xi = $xi + 3;
	        } // ENDDO
	 
	        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
	            $xcadena.= " DE";
	 
	        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
	            $xcadena.= " DE";
	 
	        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
	        if (trim($xaux) != "") {
	            switch ($xz) {
	                case 0:
	                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
	                        $xcadena.= "UN BILLON ";
	                    else
	                        $xcadena.= " BILLONES ";
	                    break;
	                case 1:
	                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
	                        $xcadena.= "UN MILLON ";
	                    else
	                        $xcadena.= " MILLONES ";
	                    break;
	                case 2:
	                    if ($xcifra < 1) {
	                        $xcadena = "CERO PESOS ";
	                    }
	                    if ($xcifra >= 1 && $xcifra < 2) {
	                        $xcadena = "UN PESO ";
	                    }
	                    if ($xcifra >= 2) {
	                        $xcadena.= " PESOS "; //
	                    }
	                    break;
	            } // endswitch ($xz)
	        } // ENDIF (trim($xaux) != "")
	        // ------------------      en este caso, para México se usa esta leyenda     ----------------
	        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
	        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
	        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
	        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
	        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
	        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
	        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
	    } // ENDFOR ($xz)
	    return trim($xcadena);
	}
	 
	// END FUNCTION
	 
	function subfijo($xx)
	{ // esta función regresa un subfijo para la cifra
	    $xx = trim($xx);
	    $xstrlen = strlen($xx);
	    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
	        $xsub = "";
	    //
	    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
	        $xsub = "MIL";
	    //
	    return $xsub;
	}

}