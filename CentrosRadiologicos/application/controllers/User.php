<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/*==========================================
		Recupera contrasena
	==========================================*/
	public function recuperar_contrasena() {
		$this->load->model('user_radiologia_model');
		$data = $this->user_radiologia_model->recuperar_contrasena();
			
		if(count($data) > 0) {

			$email = $data[0]->email;
			$nombre = $data[0]->first_name;
			$pass = $data[0]->salt;


			$data['message'] = '
			<table style="width: 100%; max-width: 600px;">
				<tbody>
					<tr>
						<td>Hola '.$nombre.'.</td>
					</tr>
					<tr>
						<td>
							Hemos recibido una solicitud para recordar tus datos de acceso a HCO-Centros Radiologícos.
						</td>
					</tr>
					<tr><td></td></tr>
					<tr><td></td></tr>
					<tr>
						<td>
							Estos son tus datos de acceso:
						</td>
					</tr>
					<tr>
						<td>
							Usuario: <b>'.$email.'</b>
						</td>
					</tr>
					<tr>
						<td>
							Contraseña: <b>'.base64_decode($pass).'</b>
						</td>
					</tr>
					<tr><td></td></tr>
					<tr><td></td></tr>
					<tr>
						<td>
							Si no has solicitado el cambio de contraseña comunicalo <a href="">aquí</a>, es posible que alguien 
							escribiera tu email por error.
						</td>
					</tr>
					<tr><td></td></tr>
					<tr><td></td></tr>
					<tr><td></td></tr>
					<tr>
						<td>
							- Equipo HCO.
						</td>
					</tr>
				</tbody>
			</table>
			';

			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);

			$this->email->from('registro@suscribelo.com', 'HCO Centros Radiologícos');
			$this->email->to($email);
			$this->email->subject('Estos son los datos de acceso a tu cuenta.');
			$this->email->message($data['message']);

			$this->email->send();

			echo "
			<script>
				window.alert('Hemos enviado tus datos de acceso al correo ".$email." Por favor, revisa la bandeja de entrada o de span.');
				window.close();
			</script>
			";
		}else {
			echo "
			<script>
				window.alert('Datos invalidos.');
				window.close();
			</script>
			";
		}
	}

}