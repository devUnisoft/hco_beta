<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_radiologia extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('ordenes_medicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('tipo_procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('procedimientos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('factura_radiologia_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('centro_radiologicos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('diagnosticos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('georeferences_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('turnos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd	
		$this->load->model('consultorios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('clinicas_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('convenios_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->model('grupos_model');//Cargar el modelo de vehiculo donde estan las funciones que hacen las consultas a la bd
		$this->load->helper('form');//Cargar el helper de formularios
		$this->load->library('email');//Cargar la libreria de email
	}

	/** VER PAGINAS **/
	public function index()	{
		$data['titulo'] = "Seleccion Perfil";//Titulo de la pagina, se lo envio al archivo donde esta el header
		$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
		$this->load->view('users/seleccionarPerfil');
	}

	public function home() {
	 	$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		$data['titulo'] = "RECEPCION DE ATENCION PARA PACIENTES";//Titulo de la pagina, se lo envio al archivo donde esta el header
	    		$data['pacientes'] = $this->user_radiologia_model->listar_pacientes();//Se llama a la funcion de que esta en modelo y el resultado se guarda
	    		$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "RECEPCION");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('asistente/home');
	    		break;


	    	case 'Administrador':
	    		$data['titulo'] = "MENU ADMIN";//Titulo de la pagina, se lo envio al archivo donde esta el header
	    		$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/home');
	    		break;


	    	case 'Profesional':
	    		$document_Seleccionado = $this->session->userdata('document_Seleccionado');
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$this->factura_radiologia_model->actualizar($document_Seleccionado, $IDCRInternoCR, "false");
				$this->turnos_model->actualizar($document_Seleccionado, $IDCRInternoCR);
			    $this->session->unset_userdata("document_Seleccionado");
			    
	    		$data['titulo'] = "TURNERO - CONSULTORIO";//Titulo de la pagina, se lo envio al archivo donde esta el header
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['facturas'] = $this->factura_radiologia_model->getFacturasProcesar($IDCRInternoCR);//Se llama a la funcion de que esta en modelo y el resultado se guarda
	    		$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "TURNERO");
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('profesional/home');
	    		break;

	    }
		
	}

	public function adminAuxiliares() {
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$data['titulo'] = "ADMINISTRACION DE AUXILIARES";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['lugares'] = $this->georeferences_model->listar();
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/adminAuxiliares');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }
		
	}

	public function adminProfesionales() {
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Administrador':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "ADMINISTRACION DE PROFESIONALES";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "");
				$data['lugares'] = $this->georeferences_model->listar();
				$data['consultorios'] = $this->consultorios_model->listar($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('admin/adminProfesionales');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;
	    }
		
	}

	public function reporteCierreCaja()	{
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "REPORTE CIERRE DE CAJA";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "CIERRECAJA");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('asistente/reporteCierreCaja');
	    		break;


	    	case 'Administrador':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}

	public function entregaResultados()	{
		$profileCR = $this->session->userdata('profileCR');
	    
	    switch ($profileCR) {
	    	case 'Asistente':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "ENTREGA DE RESULTADOS";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "ENTREGARIMG");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada

				$dataSticker = ['imprimirStikers' => $this->load->view('asistente/imprimirStikers', null, true) ];
				$this->load->view('asistente/entregaResultados', $dataSticker);
	    		break;


	    	case 'Administrador':
	    		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
				$data['titulo'] = "ENTREGA DE RESULTADOS";//Titulo de la pagina, se lo envio al archivo donde esta el header
				$this->load->library('menu');
				$data['headerPage'] = $this->menu->headerPage($this->session->userdata('NameInternoCR'));
				$data['footerPage'] = $this->menu->footerPage($this->session->userdata('profileCR'), "ENTREGARIMG");
				$data['funcionarios'] = $this->user_radiologia_model->listarFuncionarios($IDCRInternoCR);
				$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada
				$this->load->view('asistente/entregaResultados');
	    		break;


	    	case 'Profesional':
	    		echo "<script>window.location.href = '" . base_url() . "index.php/user_radiologia/home';</script>";
	    		break;

	    }

		
	}
	

	/** WEBSERVICES **/

	public function seleccionarPerfil()	{	
		$perf = "";
		switch ($_POST["text"]) {
			case base_url() . 'images/profile_Profesional.png':
				$perf = "Profesional";
				break;
			case base_url() . 'images/profile_Admin2.png':
				$perf = "Administrador";
				break;
			case base_url() . 'images/profile_Assistant1.png':
				$perf = "Asistente";
				break;
		}	
		$this->session->set_userdata('profileCR', $perf);
		$data['titulo'] = "Login Centro Radiologico";//Titulo de la pagina, se lo envio al archivo donde esta el header
		$this->load->view('header', $data);//Se muestra en el navegador primero el header con titulo y demas cosas agregada		
		$this->load->view('users/login');
	}

	public function get_clinicas_by_odontologo() 
	{
		if( isset($_POST['email']) ) {
			$data = $this->user_radiologia_model->get_clinicas_by_odontologo( $_POST['email'] );
			if( $data != null ) {
				echo json_encode( $data->result() );
			}
		}else {
			echo false;
		}
	}

	public function loginUser()	{	
		$email = $_POST['txtUsuario']; 
   	 	$password = base64_encode($_POST['txtClave']);	
   	 	$profileCR = $this->session->userdata('profileCR');

 		if( $profileCR == 'odontologo' ) {
			$data = $this->user_radiologia_model->login($email, $password, $profileCR, $clinica);
 		}else {
			$data = $this->user_radiologia_model->login($email, $password, $profileCR);
 		}


		if($data != null){
			$this->session->set_userdata('UserIDInternoCR', $data->result()[0]->email);
			$this->session->set_userdata('NameInternoCR', $data->result()[0]->first_name . " " . $data->result()[0]->last_name);
			$this->session->set_userdata('IDCRInternoCR', $data->result()[0]->id_centro_radiologico);
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function logoutUser()	{	
		
		$document_Seleccionado = $this->session->userdata('document_Seleccionado');
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$this->factura_radiologia_model->actualizar($document_Seleccionado, $IDCRInternoCR, "false");
		$this->turnos_model->actualizar($document_Seleccionado, $IDCRInternoCR);
	    $this->session->unset_userdata("document_Seleccionado");
	    $this->session->unset_userdata("UserIDInternoCR");
		$this->session->unset_userdata("NameInternoCR");
		$this->session->unset_userdata("IDCRInternoCR");
		echo "<script>window.location.href = '" . base_url() . "'</script>";
	}

	public function findUsuarioRadiologia() {
		$NumeroDoc = $_POST['NumeroDoc']; 
    	$Tipo = $_POST['typeuser']; 
		$data = $this->user_radiologia_model->getUsuarioRadiologia($NumeroDoc, $Tipo);

		if($data != null){
			foreach ($data->result() as $key) {
				$key->salt = base64_decode($key->salt);
			}
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function listar_pacientes() {
		$data = $this->user_radiologia_model->listar_pacientes();

		if($data != null){			
			echo json_encode($data);
		}else{
			echo "[]";
		}
	}

	public function addUsuarioRadiologia()	{
		$TipoUsu = $_POST['TipoUsu']; 
	    $TipoDoc = $_POST['TipoDoc']; 
	    $NumeroDoc = $_POST['NumeroDoc']; 
	    $nombre = $_POST['nombre']; 
	    $apellido = $_POST['apellido']; 
	    $email = $_POST['email']; 
	    $birthdate = $_POST['birthdate']; 
	    $lugarNac = $_POST['lugarNac']; 
	    $lugarResi = $_POST['lugarResi']; 
	    $genero = $_POST['genero']; 
	    $consultorio = $_POST['consultorio'];  
	    $password = $_POST['clave'];
    	$clave = base64_encode($password);	
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		$arrayUsuario = array(
			'TipoUsu' => $TipoUsu,
			'clave' => $clave,
			'nombre' => $nombre,
			'apellido' => $apellido,
			'email' => $email,
			'birthdate' => $birthdate,
			'genero' => $genero,
			'TipoDoc' => $TipoDoc,
			'NumeroDoc' => $NumeroDoc,
			'lugarResi' => $lugarResi,
			'lugarNac' => $lugarNac,
			'consultorio' => $consultorio,
			'centro_radiologico' => $IDCRInternoCR);

		$this->user_radiologia_model->crear($arrayUsuario);//Se llama a la funcion de que esta en modelo y el resultado se guarda
	}

	public function addPaciente()	{
		$TipoUsu = $_POST['TipoUsu']; 
	    $TipoDoc = $_POST['TipoDoc']; 
	    $NumeroDoc = $_POST['NumeroDoc']; 
	    $nombre = $_POST['nombre']; 
	    $apellido = $_POST['apellido']; 
	    $email = $_POST['email']; 
	    $birthdate = $_POST['birthdate']; 
	    $lugarNac = $_POST['lugarNac']; 
	    $lugarResi = $_POST['lugarResi']; 
	    $lugarN = $_POST['lugarNac'];
	    $lugarR = $_POST['lugarResi'];
	    $zona = $_POST['zonar'];  	
	    $tipousuario = $_POST['tipousuarior'];
	    $genero = $_POST['genero'];
	    //$foto = $_POST['foto'];
	    $file = "";
		$url = "";
		$uidFoto = uniqid();
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);

		/*if($_POST['foto'] != ""){
			$img = $_POST['foto'];
			$img = str_replace('data:image/png;base64,', '', $img);
			$img = str_replace('data:image/jpg;base64,', '', $img);
			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace('data:image/gif;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$file = getcwd() . "/uploads/" . $uidFoto . '.png';
			$url = "uploads/" . $uidFoto . '.png';
			$success = file_put_contents($file, $data);
		}*/

		$arrayUsuario = array(
			'TipoUsu' => $TipoUsu,
			'nombre' => $nombre,
			'apellido' => $apellido,
			'email' => $email,
			'birthdate' => $birthdate,
			'genero' => $genero,
			'TipoDoc' => $TipoDoc,
			'NumeroDoc' => $NumeroDoc,
			'lugarResi' => $lugarResi,
			'lugarNac' => $lugarNac,
			'zona' => $zona,
			'tipousuario' => $tipousuario,
			'foto' => $url,
			'clinica' => $dataCR->result()[0]->name);

		$this->user_radiologia_model->crearPaciente($arrayUsuario);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		$data = $this->user_radiologia_model->getPaciente($NumeroDoc, "person");

		if($data != null){
			foreach ($data->result() as $keyUser) {

				if($dataCR != null){
					$keyUser->profesional = "";
					$keyUser->profesionalName = "";
					$keyUser->clinica = $dataCR->result()[0]->name;
				}else{
					$keyUser->profesional = "";
					$keyUser->profesionalName = "";
					$keyUser->clinica = "";
				}
				echo json_encode($keyUser);
			}
		}else{
			echo "{}";
		}
		//echo $TipoUsu . " - " . $TipoDoc . " - " . $NumeroDoc . " - " . $nombre . " - " . $apellido . " - " . $email . " - " . $birthdate . " - " . $lugarNac . " - " . $lugarResi . " - " . $genero . " - " . $IDCRInternoCR;
	}

	public function addProfesional()	{
		$TipoUsu = $_POST['TipoUsu']; 
	    $TipoDoc = $_POST['TipoDoc']; 
	    $NumeroDoc = $_POST['NumeroDoc']; 
	    $nombre = $_POST['nombre']; 
	    $apellido = $_POST['apellido']; 
	    $email = $_POST['email']; 
	    $telefono = $_POST['telefono']; 
	    $lugarResi = $_POST['lugarResi']; 
	    $direccion = $_POST['direccion']; 
	    $clinica = $_POST['clinica'];  	   	

		$arrayUsuario = array(
			'TipoUsu' => $TipoUsu,
			'nombre' => $nombre,
			'apellido' => $apellido,
			'email' => $email,
			'telefono' => $telefono,
			'lugarResi' => $lugarResi,
			'TipoDoc' => $TipoDoc,
			'NumeroDoc' => $NumeroDoc,
			'direccion' => $direccion,
			'clinica' => $clinica);

		$this->user_radiologia_model->crearProfesional($arrayUsuario);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		// $data = $this->user_radiologia_model->getPaciente($NumeroDoc, "odontologo");
		$prm['num_doc'] = $NumeroDoc;
		$prm['clinica_id'] = $clinica;
		$prm['tipo_user'] = $TipoUsu;
		
		$data = $this->user_radiologia_model->obtener_Profesional_by_clinic( $prm );

		if($data != null){
			foreach ($data->result() as $keyUser) {
				echo json_encode($keyUser);
			}
		}else{
			echo "{}";
		}
		//echo $TipoUsu . " - " . $TipoDoc . " - " . $NumeroDoc . " - " . $nombre . " - " . $apellido . " - " . $email . " - " . $birthdate . " - " . $lugarNac . " - " . $lugarResi . " - " . $genero . " - " . $IDCRInternoCR;
	}

	public function updateUsuarioRadiologia()	{
		$TipoUsu = $_POST['TipoUsu']; 
	    $TipoDoc = $_POST['TipoDoc']; 
	    $NumeroDoc = $_POST['NumeroDoc']; 
	    $NumeroDocAntes = $_POST['NumeroDocAntes']; 
	    $nombre = $_POST['nombre']; 
	    $apellido = $_POST['apellido']; 
	    $email = $_POST['email']; 
	    $birthdate = $_POST['birthdate']; 
	    $lugarNac = $_POST['lugarNac']; 
	    $lugarResi = $_POST['lugarResi']; 
	    $genero = $_POST['genero']; 
	    $consultorio = $_POST['consultorio']; 
	    $password = $_POST['clave'];
	    $clave = base64_encode($password);
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');

		$arrayUsuario = array(
			'TipoUsu' => $TipoUsu,
			'clave' => $clave,
			'nombre' => $nombre,
			'apellido' => $apellido,
			'email' => $email,
			'birthdate' => $birthdate,
			'genero' => $genero,
			'TipoDoc' => $TipoDoc,
			'NumeroDoc' => $NumeroDoc,
			'NumeroDocAntes' => $NumeroDocAntes,
			'lugarResi' => $lugarResi,
			'lugarNac' => $lugarNac,
			'consultorio' => $consultorio,
			'centro_radiologico' => $IDCRInternoCR);

		$this->user_radiologia_model->actualizar($arrayUsuario);//Se llama a la funcion de que esta en modelo y el resultado se guarda

		//echo $TipoUsu . " - " . $TipoDoc . " - " . $NumeroDoc . " - " . $NumeroDocAntes . " - " . $nombre . " - " . $apellido . " - " . $email . " - " . $birthdate . " - " . $lugarNac . " - " . $lugarResi . " - " . $genero . " - " . $consultorio . " - " . $password . " - " . $clave . " - " . $IDCRInternoCR;
	}

	public function obtenerValorTotal() {	
		$grupo = $_REQUEST['grupo']; 
	    $idTipo = $_REQUEST['idTipo']; 
	    $profesional = $_REQUEST['profesional']; 
	    $clinica = $_REQUEST['clinica']; 
	    $idPresentacion = json_decode($_REQUEST['idPresentacion']); 
	    $diagnosticos = json_decode($_REQUEST['diagnosticos']); 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$valorTotal = 0;
		$idprocedimiento = 0;
		$convenio = "";
		$descuento = "";

		$dataProce = null;
 		if(count($idPresentacion) == 0){
 			$dataProce = $this->procedimientos_model->get($idTipo, $IDCRInternoCR);
 		}else{
 			$dataProce = $this->procedimientos_model->getConPresentacion($idTipo, $idPresentacion[0]->id, $IDCRInternoCR);
 		}
 		if($dataProce != null){ 			
 			if(count($diagnosticos) > 0){ 				
                foreach ($diagnosticos as $valorDiag) {
                    $idDiag = $valorDiag->id;
                    $dataDiag = $this->diagnosticos_model->get($idDiag, $IDCRInternoCR);
                    if($dataDiag != null){
                    	$valorTotal += $dataDiag->result()[0]->valor;
                    }else{
                    	$valorTotal += 0;
                    }
                    
                }
             	$valorTotal += $dataProce->result()[0]->valor;
 			}else{
 				$valorTotal += $dataProce->result()[0]->valor;
 			}
 			$dataProfesional = $this->user_radiologia_model->getProfesional($profesional);
			$clinica = $dataProfesional->result()[0]->clinica;

			$dataCovenio = $this->convenios_model->getConveniosProfes($clinica, $dataProfesional->result()[0]->id, $IDCRInternoCR);

			$nameProfesional = $dataProfesional->result()[0]->first_name . " " . $dataProfesional->result()[0]->last_name;

			if($dataCovenio != null){
				$convenio = $dataCovenio->result()[0]->convenio_factura;
				$descuento = $dataCovenio->result()[0]->descuento_proce;				
			}else{
				$convenio = "";
				$descuento = "0";
			}
			
 			$idprocedimiento += $dataProce->result()[0]->id;
 			$array = array('valor' => $valorTotal, 'idprocedimiento' => $idprocedimiento, 'convenio' => $convenio, 'descuento' => $descuento, 'nameProfesional' => $nameProfesional);
 			echo json_encode($array);
 			//echo '{"valor":"' . $valorTotal . '", "idprocedimiento":"' . $idprocedimiento . '", "convenio":"' . $convenio . '", "descuento":"' . $descuento . '", "nameProfesional":"' . $nameProfesional . '"}';
 		}else{
 			echo "{}";
 		}
 		
 		//echo $grupo . " - " . $idTipo . " - " . $_REQUEST['idPresentacion'] . " - " . $_REQUEST['diagnosticos'] . " - " . $IDCRInternoCR . " - " . $valorTotal;
	}

	public function findPersonaFactura() {	
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->user_radiologia_model->obtenerPacienteDocumento($_POST['document']);

		if($data != null){
			foreach ($data->result() as $keyUser) {
				$dataOM = $this->ordenes_medicas_model->get($_POST['document']);
				if($dataOM != null){
					foreach ($dataOM->result() as $key) {
						//$profesional = $key->emailProfesional;
						//$jsonOrden = json_decode($key->json);
						$jsonTipos = "[";
						$contadorTipos = 0; 

						$dataCovenio = $this->convenios_model->getConveniosProfes($key->clinica, $key->idProfesional, $IDCRInternoCR);

						if($dataCovenio != null){
							$key->descuento = $dataCovenio->result()[0]->descuento_proce;
			 				$key->tipoConvenio = $dataCovenio->result()[0]->convenio_factura;
						}else{
							$key->descuento = "0";
							$key->tipoConvenio = "";
						}
					}
					$keyUser->ordenMedica = $dataOM->result();
					echo json_encode($keyUser);
				}else{
					$keyUser->ordenMedica = array();
					$keyUser->profesional = "";
					$dataCR = $this->centro_radiologicos_model->get($IDCRInternoCR);
					$keyUser->clinica = $dataCR->result()[0]->name;
					echo json_encode($keyUser);
				}
				
			}
			
		}else{
			echo "{}";
		}
	}

	public function findPersonaAnulacion() {	
		$document = $_POST['document']; 
	    $fecha = $_POST['fecha']; 
	    $factura = $_POST['factura']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$dataFA = null;

		$data = $this->user_radiologia_model->obtenerPacienteDocumento($_POST['document']);

		if($data != null){
			
			if(trim($fecha) != "" && trim($factura) != ""){
				$dataFA = $this->factura_radiologia_model->getFiltroFacturasFechaIdFa($document, $fecha, $factura);
			}else{
				if(trim($fecha) == "" && trim($factura) == ""){
					$dataFA = $this->factura_radiologia_model->getSinFiltroFacturas($document);
				}else{
					if(trim($fecha) != ""){
						$dataFA = $this->factura_radiologia_model->getFiltroFacturasFecha($document, $fecha);
					}else{
						$dataFA = $this->factura_radiologia_model->getFiltroFacturasIdFac($document, $factura);
					}
				}
			}

			if($dataFA != null){
				echo json_encode($dataFA->result());
			}else{
				echo "[]";
			}

		}
	}
	function listarProfesionales(){	
		$clinica = $_POST['clinica'];	
		$data = $this->clinicas_model->getProfesionales($clinica);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}
	function listarClinicas(){		
		$data = $this->clinicas_model->listar();
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}
		
	function obtenerFacturasProcesar(){		
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$data = $this->factura_radiologia_model->getFacturasProcesar($IDCRInternoCR);//Se llama a la funcion de que esta en modelo y el resultado se guarda
		if($data != null){
			echo json_encode($data->result());
		}else{
			echo "[]";
		}
	}

	public function findFacturasDesarrollo() {	
		$document = $_POST['document'];
	    $fechaInicial = $_POST['fechaInicial']; 
	    $fechaFinal = $_POST['fechaFinal']; 
	    $factura = $_POST['factura']; 
	    $proceso = $_POST['proceso'];  
	    $user_radiologia = $_POST['user_radiologia']; 
		$IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
		$dataFA = null;

		$data = $this->user_radiologia_model->obtenerPacienteDocumento($_POST['document']);

		if($data != null){
			
			if(trim($proceso) != "" && trim($factura) != "" && trim($document) != ""){
				$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcFacDoc($proceso, $document, $factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
			}else{
				if(trim($proceso) != "" && trim($factura)){
					$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcFac($proceso, $factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
				}else{
					if(trim($proceso) != "" && trim($document) != ""){
						$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcDoc($proceso, $document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
					}else{
						if(trim($factura) != "" && trim($document) != ""){
							$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroFacDoc($factura, $document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
						}else{
                    		if(trim($proceso) == "" && trim($factura) == "" && trim($document) == ""){
                    			$dataFA = $this->factura_radiologia_model->getSinFiltroProcesosDesarroProcFacDoc($IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                    		}else{
                            	if(trim($proceso) != ""){
                            		$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProc($proceso, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            	}else{
                            		if(trim($factura) != ""){
                            			$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroFac($factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            		}else{
                            			$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroDoc($document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            		}
                            	}
							}
						}
					}
				}
			}

			if($dataFA != null){
				echo json_encode($dataFA->result());
			}else{
				echo "[]";
			}

		}
	}

	public function exportarReporteProcesosDesarrollados(){
		$document = $_GET['document'];
	    $fechaInicial = $_GET['fechaInicial']; 
	    $fechaFinal = $_GET['fechaFinal']; 
	    $factura = $_GET['factura']; 
	    $proceso = $_GET['proceso'];  
	    $user_radiologia = $_GET['user_radiologia']; 
	    $IDCRInternoCR = $this->session->userdata('IDCRInternoCR');
	    $i = 7; 
	    $total = 0;

	    $dataFA = null;

		$data = $this->user_radiologia_model->obtenerPacienteDocumento($_GET['document']);

		if($data != null){
			
			if(trim($proceso) != "" && trim($factura) != "" && trim($document) != ""){
				$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcFacDoc($proceso, $document, $factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
			}else{
				if(trim($proceso) != "" && trim($factura)){
					$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcFac($proceso, $factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
				}else{
					if(trim($proceso) != "" && trim($document) != ""){
						$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProcDoc($proceso, $document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
					}else{
						if(trim($factura) != "" && trim($document) != ""){
							$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroFacDoc($factura, $document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
						}else{
                    		if(trim($proceso) == "" && trim($factura) == "" && trim($document) == ""){
                    			$dataFA = $this->factura_radiologia_model->getSinFiltroProcesosDesarroProcFacDoc($IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                    		}else{
                            	if(trim($proceso) != ""){
                            		$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroProc($proceso, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            	}else{
                            		if(trim($factura) != ""){
                            			$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroFac($factura, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            		}else{
                            			$dataFA = $this->factura_radiologia_model->getFiltroProcesosDesarroDoc($document, $IDCRInternoCR, $fechaInicial, $fechaFinal, $user_radiologia);
                            		}
                            	}
							}
						}
					}
				}
			}

			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);
			date_default_timezone_set('Europe/London');

			if (PHP_SAPI == 'cli')
				die('This example should only be run from a Web Browser');

			/** Include PHPExcel */
			$this->load->file('PHPExcel/Classes/PHPExcel.php');


			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Unisoft System Corporation")
	               ->setLastModifiedBy("HCO")
	               ->setTitle("REPORTE DE PROCESOS DESARROLLADOS")
	               ->setSubject("REPORTE DE PROCESOS DESARROLLADOS")
	               ->setDescription("REPORTE DE PROCESOS DESARROLLADOS")
	               ->setKeywords("office 2007 openxml php")
	               ->setCategory("Test result file");


	       	$dataUser = $this->user_radiologia_model->getUsuario($user_radiologia);
	       	if($dataUser != null){
				$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A1', 'ID.PACIENTE')
			            ->setCellValue('B1', $data->result()[0]->document__number)
			            ->setCellValue('A2', 'PACIENTE')
			            ->setCellValue('B2', $data->result()[0]->first_name . " " . $data->result()[0]->last_name)
			            ->setCellValue('A3', 'PROCESO')
			            ->setCellValue('B3', $proceso)
			            ->setCellValue('A4', 'FACTURA')
			            ->setCellValue('B4', $factura)
			            ->setCellValue('C1', 'FUNCIONARIO')
			            ->setCellValue('D1', $data->result()[0]->first_name . " " . $data->result()[0]->last_name)
			            ->setCellValue('C2', 'FECHA INICIAL')
			            ->setCellValue('D2', $fechaInicial)
			            ->setCellValue('F2', 'FECHA FINAL')
			            ->setCellValue('G2', $fechaFinal);
	        }
	        
		

        	$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A6', 'FECHA')
			            ->setCellValue('B6', 'HORA')
			            ->setCellValue('C6', 'FACTURA')
			            ->setCellValue('D6', 'ID.PACIENTE')
			            ->setCellValue('E6', 'PACIENTE')
			            ->setCellValue('F6', 'PROCESO')
			            ->setCellValue('G6', 'FUNCIONARIO')
			            ->setCellValue('H6', 'VALOR')
			            ->setCellValue('I6', 'ESTADO');


			if($dataFA != null){
				foreach ($dataFA->result() as $value) {
					$estado = "";
					if($value->estadoFactura == "ACTIVO"){
						$estado = "APROBADA";
						$total += $value->total;
					}else{
						$estado = $value->estadoFactura;
					}

					$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValue('A' . $i, $value->fecha)
			            ->setCellValue('B' . $i, $value->hora)
			            ->setCellValue('C' . $i, $value->id_factura_radiologia)
			            ->setCellValue('D' . $i, $value->document__number)
			            ->setCellValue('E' . $i, $value->first_name . " " . $value->last_name)
			            ->setCellValue('F' . $i, $value->descripcion)
			            ->setCellValue('G' . $i, $value->nombreFuncionario . " " . $value->apellidosFuncionario)
			            ->setCellValue('H' . $i, $value->total)			            
			            ->setCellValue('I' . $i, $estado);
			            $i++;   
				}
			}else{
				
			}

			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('REPORTE CIERRE DE CAJA');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$nombreArchivo = "Reporte_Procesos_Desarrollados" . date("Y") . date("m") . date("d") . date("H") . date("i") . date("s") . date("u") . ".xls";
			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $nombreArchivo . '.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}	

		
	}

	public function getPacienteDocument() {	
		if( isset($_POST['is_profesional']) && $_POST['is_profesional'] == true ) {
			$prm = [
				'num_doc' => $_POST['NumeroDoc'],
				'clinica_id' => $_POST['clinica_id']
			];
			$data = $this->user_radiologia_model->obtener_Profesional_by_clinic( $prm );
		}else {
			$data = $this->user_radiologia_model->obtenerPacienteDocumento($_POST['NumeroDoc']);
		}

		if($data != null){
			foreach ($data->result() as $key) {
				echo json_encode($key);
			}			
		}else{
			echo "{}";
		}
	}

	public function getPaciente() {	
		$data = $this->user_radiologia_model->getPaciente($_POST['NumeroDoc'], "person");
		if($data != null){
			foreach ($data->result() as $key) {
				echo json_encode($key);
			}			
		}else{
			echo "{}";
		}
	}

	public function listarDepartamentos() {	
		$data = $this->georeferences_model->listarDepartamentos();
		if($data != null){
			echo json_encode($data->result());		
		}else{
			echo "[]";
		}
	}

}