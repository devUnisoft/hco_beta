<?php
  

  $profileCR = $this->session->userdata('UserIDInternoCR');
  if(empty($profileCR)) { // Recuerda usar corchetes.
    header('Location: ' . base_url());
  }else{
    $document_Seleccionado = $this->session->userdata('document_Seleccionado');
    if(empty($document_Seleccionado)) { // Recuerda usar corchetes.
        header('Location: ' . base_url() . "index.php/user_radiologia/home");
    }
  }

  
?>
<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/chosen.css" />
<script type="text/javascript" src = "http://www.google.com/jsapi" charset="utf-8"></script>
<style type="text/css">
  span.error {
    margin-left: 10px;
    width: auto;
    display: inline;
    color:#F00;
    font-size:12px;
  }
  header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}


 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 10px; 
}
  </style>
  <script type="text/javascript">
    function nobackbutton(){
       window.location.hash="no-back-button";
       window.location.hash="Again-No-back-button" 
       window.onhashchange=function(){window.location.hash="no-back-button";} 
    }
</script>
<body onload="nobackbutton();">

<header>
  <?= $headerPage;?>
</header>

<div class="container-fluid">

  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row"> 
    <form id="frmFind" method="post" action="">
      <div class="col-md-3" style="margin:0">   
        <label><font color="#0683c9">No. IDENTIFICACION</font></label><br>
        <input type="text" name="txtIdentificacion" id="txtIdentificacion" style="text-align: center;" class="form-control" disabled value="<?= $persona->result()[0]->document__number;?>">
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">NOMBRE</font></label><br>
        <input type="text" name="txtNombre" id="txtNombre" style="text-align: center;" class="form-control" disabled value="<?= $persona->result()[0]->first_name . ' ' . $persona->result()[0]->last_name;?>">      
      </div> 
      <div class="col-md-3">   
        <label><font color="#0683c9">DIRECCION</font></label><br>
        <input type="text" name="txtDireccion" id="txtDireccion" style="text-align: center;" class="form-control" disabled value="<?= $persona->result()[0]->direccion;?>">      
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">TELEFONO</font></label><br>
        <input type="text" name="txtTelefono" id="txtTelefono" style="text-align: center;" class="form-control" disabled value="<?= $persona->result()[0]->telefono . ' - ' . $persona->result()[0]->celular;?>">      
      </div> 
      <div class="col-md-2">   
        <label><font color="#0683c9">EMAIL</font></label><br>
        <input type="text" name="txtEmail" id="txtEmail" style="text-align: center;" class="form-control" disabled value="<?= $persona->result()[0]->email;?>">      
      </div> 
    </form>
  </div>
  <br>

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
    <thead>
      <tr style="color: #0683c9; border-color: #0683c9; font-weight: bolder;" align="center">
        <td>HORA</td>
        <td>DESCRIPCION</td>
        <td>CANTIDAD</td>
        <td>CONVENIO</td>
        <td>CLINICA</td>
        <td>PROFESIONAL</td>
        <td>OBSERV. INICIALES DE SOLICITUD</td>
        <td>OBSERV. DE RECEPCION</td>
        <td>CAPTURA CON IMAGEN</td>
        <td>CAPTURA SIN IMAGEN</td>
      </tr>
    </thead>
    <tbody id="tbodyOrdenMedica">
      <?php
        $contador = 0;
        if($facturas != null){
          foreach ($facturas->result() as $value) {
            $id = $value->id;
            $hora = $value->hora;
            $descripcion = $value->descripcion;
            $cantidad = $value->cantidad;
            $profesional = $value->profesional;
            $nameProfesional = $value->nameProfesional;
            $clinica = $value->clinica;
            $convenio = $value->convenio;
            $procedimiento = $value->idProcedimiento;
            $name = $value->first_name . " " . $value->last_name;
            $observacion_inicial = $value->observacion_inicial;
            $observacion_recepcion = $value->observacion_recepcion;
            $id_factura_radiologia = $value->id_factura_radiologia;
            $documento = $value->documento;
            $atendido = $value->atendido;         
            $id_orden_medica = $value->id_orden_medica;             
      ?>  
        <tr>
          <td><?= $hora;?></td>
          <td><?= $descripcion;?></td>
          <td align="center"><?= $cantidad;?></td>
          <td><?= $convenio;?></td>
          <td><?= $clinica;?></td>
          <td><?= $nameProfesional;?></td>
          <td><?= $observacion_inicial;?></td>
          <td><?= $observacion_recepcion;?></td>
          <?php
            $estado = 0;
            if(trim($convenio) != ""){
              $conv = explode(",", $convenio);
              if(count($conv) > 0){
                for ($i=0; $i < count($conv); $i++) {
                  if($conv[$i] == "EMAIL"){
                    $estado++;
                  }
                }
              }else{
                if($convenio == "EMAIL"){
                  $estado++;
                }
              }
              

            }else{
              $estado++;
            }
            ?>
          <td align="center"><img src="../../images/plus_blanco.png" class="fotoAgregar" width="48" style="cursor: pointer;" idDetalleFact="<?= $id;?>" ind="<?= $contador;?>" id="btnLlamar<?= $contador;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" clinica="<?= $clinica;?>" name_profesional="<?= $nameProfesional;?>" profesional="<?= $profesional;?>" documento="<?= $documento;?>" id_orden_medica="<?= $id_orden_medica;?>" id_procedimiento="<?= $procedimiento;?>" indjson="-1" correo="<?= $estado;?>" atendido="no"></td> 
            
          <td align="center"><img src="../../images/plus_blanco.png" class="sinFoto" width="48" style="cursor: pointer;" idDetalleFact="<?= $id;?>" ind="<?= $contador;?>" id="btnSinFotos<?= $contador;?>" clinica="<?= $clinica;?>" name_profesional="<?= $nameProfesional;?>" profesional="<?= $profesional;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" documento="<?= $documento;?>" id_orden_medica="<?= $id_orden_medica;?>" id_procedimiento="<?= $procedimiento;?>" indjson="-1" atendido="no"></td>
        </tr>
      <?php
            $contador++;
          }
        }
      ?>       
    </tbody>
  </table><br><br>   
    <div class="col-md-12" align="right">
      <div id="spin"></div>
      <button type="button" class="btn btn-danger" id="btnCancelarResultados"><b>CANCELAR</b></button>
      <button type="button" class="btn btn-primary" id="btnRegistrarResultados" disabled="disabled"><b>REGISTRAR RESULTADOS</b></button>
    </div> 
    

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 0px">
          <div class="col-md-12" align="right">
            <h4 style="color: #428bca; font-weight: bolder">HCO - Imagenes / Adicionar</h4>
          </div>
          <div class="col-md-4">
            <img width="100%" src="../../images/LOGO SW CENTRAL.png">
          </div>
          <div class="col-md-8" align="right">
            <br>
            <input type="file" name="fileFotos" id="fileFotos" class="form-control" style="background-color: #428bca; color: #FFFFFF" accept="image/*" multiple>
          </div>

        </div>
        <div class="modal-body" style="border: 0px">
          
        </div>
        <div class="modal-footer" style="border: 0px">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAceptar" data-toggle="modal" data-target="#myModalFotos" disabled="disabled">Aceptar</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelar">Cancelar</button>
        </div>
      </div>

    </div>
  </div>

  <div id="myModalSinFotos" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">
          
          <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

        </div>
        <div class="modal-body" style="border: 0px; ">

          <div class="col-md-12">
            <h3 style="color: #428bca; font-weight: bolder" align="center">CAPTURA DE INFORMACIÓN</h3>

            <div class="col-md-12" style="">                           
              <label style="margin-top: 10px"><font color="#0683c9">OBSERVACIONES DE PROCESO CAPTURA</font></label><br>
              <textarea class="form-control" id="textareaObservacionSinFoto" name="textareaObservacionSinFoto"></textarea><br><br>
            </div>

            <div class="col-md-12" style="">                           
              <button type="button" class="btn btn-primary" id="btnSubirInformacion" style="width: 100%">SUBIR INFORMACION</button><br><br>
              <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarInformacion" style="width: 100%">CANCELAR</button>
            </div>        
          </div>

        </div>
        <div class="modal-footer" style="border: 0px">
          
          
        </div>

      </div>

    </div>
  </div>

  <div id="myModalFotos" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 80%">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px; height: 700px">
        <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">
          
          <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

        </div>
        <div class="modal-body" style="border: 0px; height: 600px">

          <div class="col-md-4"><br><br>
            <button type="button" class="btn btn-primary" id="btnSubirArchivos" style="width: 100%">SUBIR ARCHIVOS</button><br><br>
            <input type="file" name="fileFotosModal" id="fileFotosModal" class="form-control" style="background-color: #428bca; color: #FFFFFF" accept="image/*" multiple><br>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarSubida" style="width: 100%">CANCELAR</button><br>
            <label id="lblEmailCompartir">Correo</label>
            <input type="email" name="txtEmailCompartir" id="txtEmailCompartir" class="form-control"><br>
            <span class="error" style="display: none" id="errorEmail"></span>
          </div>

          <div class="col-md-8">
            <h3 style="color: #428bca; font-weight: bolder" align="center">CAPTURA DE INFORMACIÓN</h3>
            <h4 style="color: #CCC; font-weight: bolder" align="center"><u>ARCHIVOS PARA CARGUE</u></h4>  

            <div class="col-md-12" style="height: 500px; overflow-y: scroll;">
              <div style="width: 100%; height: 200px; overflow: visible;" id="divImagenes">

                <div class="col-md-12" style="width: 100%; margin-top: 10px">
                  <div style="padding: 0; width: 50%; float: left">
                    <img src="../../uploads/577bcadb29eb6.png" width="100%">
                  </div>
                  <div style="padding: 0; width: 50%; float: left">
                    <img src="../../img/cancel.png" width="26" style="float: right;"></br>
                    <label>Etiqueta:</label></br>    
                  </div> 
                </div>

              </div>              
              <label style="margin-top: 10px"><font color="#0683c9">OBSERVACIONES DE PROCESO CAPTURA</font></label><br>
              <textarea class="form-control" id="textareaObservacion" name="textareaObservacion"></textarea><br><br>
            </div>    
          </div>

        </div>
        <div class="modal-footer" style="border: 0px">
          
          
        </div>
      </div>

    </div>
  </div>
  <br>
  <?= $footerPage;?>  
  <br><br>
  </div>
    
    
  <!-- call calendar plugin -->
  <script type="text/javascript">
  
  </script>
  <!--<script type="text/javascript" src="<?= base_url();?>js/bootstrap-tokenfield.js" charset="UTF-8"></script>-->
  <script src="<?= base_url();?>js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="<?= base_url();?>js/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="<?= base_url();?>js/bootstrap.js"></script>
  <script src="<?= base_url();?>js/jquery.babypaunch.spinner.min.js"></script>
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript"> 
    
    var jsonFotos = [];
    var contadorFoto = 0; 
    $(function(){
      //$("#spin").spinner();

      $("#spin").spinner({
        color: "black"
        , background: "rgba(255,255,255,0.5)"
        , html: "<i class='fa fa-circle-o-notch fa-5x' style='color: gray;'></i>"
        , spin: true
      });     
      

    });
    function readImage() {
      if ( this.files && this.files[0] ) {
        var long = this.files.length;
        $("#divImagenes").html("")
        $("#textareaObservacion").val("")
        for(var i = 0; i < long; i++){
          var FR = new FileReader();
          contadorFoto = 0;
          FR.onload = function(e) {              
            $("#divImagenes").append('<div class="col-md-12" style="width: 100%; margin-top: 10px" id="div' + contadorFoto + '" index="' + contadorFoto + '">' + 
                '<div style="padding: 0; width: 50%; float: left">' + 
                  '<img src="' + e.target.result + '" width="100%" id="foto' + contadorFoto + '" class="fotoSubida">' + 
                '</div>' + 
                '<div style="padding: 0; width: 50%; float: left">' + 
                  '<img src="../../img/cancel.png" width="26" style="float: right;" id="borrar' + contadorFoto + '" ind="' + contadorFoto + '"></br>' + 
                  '<label>Etiqueta:</label><br>' + 
                  '<select  data-placeholder="" ind="' + contadorFoto + '" id="tag' + contadorFoto + '" name="tags[]" class="form-control chzn-select" multiple="multiple" tabindex="6" >' +
                    
                  '</select>' + 
                  '<button id="btnAgregar' + contadorFoto + '" class="btn btn-success" ind="' + contadorFoto + '"><i class="fa fa-plus" aria-hidden="true"></i></button>' +
                  '<input type="text" id="txtName' + contadorFoto + '" name="txtName' + contadorFoto + '" class="form-control" style="display:none"><br>' +
                  '<div class="col-md-6">' +
                    '<button id="btnEnviar' + contadorFoto + '" class="btn btn-success" style="display:none" ind="' + contadorFoto + '">Agregar</button>' +
                  '</div>' +
                  '<div class="col-md-6">' +
                    '<button id="btnCancelar' + contadorFoto + '" class="btn btn-danger" style="display:none" ind="' + contadorFoto + '">Cancelar</button><br>' +
                  '</div>' +
                  
                  '<span class="error" style="display: none" id="error' + contadorFoto + '"></span>' + 
                '</div> ' + 
              '</div>') 
            
            llenarLista(contadorFoto, [])
            
            $('#btnAgregar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              $("#btnAgregar" + ind).css({"display":"none"})
              $("#txtName" + ind).css({"display":"block"})
              $("#btnEnviar" + ind).css({"display":"block"})
              $("#btnCancelar" + ind).css({"display":"block"})
              $("#txtName" + ind).val("")
            });

            $('#btnEnviar' + contadorFoto).click(function (e) {
              
              var ind = $(this).attr("ind")
              var nombre = $("#txtName" + ind).val()

              if($.trim(nombre).length > 0){
                $("#spin").show();
                var datos = {
                  name: nombre
                }   
                //Enviar datos para modificarse
                $.post("<?= base_url();?>index.php/ordenes_medicas/crearTag", datos)
                .done(function( data ) {console.log(data)      
                  llenarListaClase(ind, nombre)
                  $("#btnAgregar" + ind).css({"display":"block"})
                  $("#txtName" + ind).css({"display":"none"})
                  $("#btnEnviar" + ind).css({"display":"none"})
                  $("#btnCancelar" + ind).css({"display":"none"})
                  $("#txtName" + ind).val("")
                  $("#spin").hide();
                  

                  $("#tag" + ind).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
                  $(".chzn-select-deselect").chosen({
                      allow_single_deselect:true
                  });
                  $("#tag" + ind).on('change', function(evt, params) {
                    $("#error" + $(this).attr("ind")).css({"display":"none"})
                    $("#error" + $(this).attr("ind")).html("")
                  });
                });
              }else{
                alert("¡Debe ingresar un nombre de la etiqueta nueva!")
              }
              
            });

            $('#btnCancelar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              $("#btnAgregar" + ind).css({"display":"block"})
              $("#txtName" + ind).css({"display":"none"})
              $("#btnEnviar" + ind).css({"display":"none"})
              $("#btnCancelar" + ind).css({"display":"none"})
              $("#txtName" + ind).val("")
            });

            $("#tag" + contadorFoto).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
            $(".chzn-select-deselect").chosen({
                allow_single_deselect:true
            });
            $("#tag" + contadorFoto).on('change', function(evt, params) {
              $("#error" + $(this).attr("ind")).css({"display":"none"})
              $("#error" + $(this).attr("ind")).html("")
            });

            $(".chzn-container").css({"width":"100%"})
            $(".chzn-drop").css({"width":"100%"})
            $(".default").css({"width":"100%"})
            $(".chzn-choices").css({"width":"100%"})
            
            console.log($('#tags_' + contadorFoto))
            $("#btnSubirArchivos").removeAttr("disabled")
            $('#borrar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              var confirmar = window.confirm("¿Desea quitar esta foto?")
              if(confirmar){
                $("#div" + ind).remove()
                console.log($("#divImagenes > div").length)
                if($("#btnSubirArchivos").attr("indicajson") == "-1"){
                  if($("#divImagenes > div").length > 0){
                    $("#btnSubirArchivos").removeAttr("disabled")
                  }else{
                    $("#btnSubirArchivos").attr("disabled", "disabled")
                  }
                }
                
              }
            })
            contadorFoto++;
          }           
            
          
          
          
          FR.readAsDataURL( this.files[i] );
          $(".chzn-container").css({"width":"100%"})
          $(".chzn-drop").css({"width":"100%"})
          $(".default").css({"width":"100%"})
          $(".chzn-choices").css({"width":"100%"})
        }      
          
      }
      $("#btnAceptar").removeAttr("disabled")

    }

    function readImagePopUp() {
      if ( this.files && this.files[0] ) {
        var long = this.files.length;
       
        for(var i = 0; i < long; i++){
          var FR = new FileReader();

          if($("#divImagenes > div").length > 0){
            contadorFoto = parseInt($("#divImagenes > div:last").attr("index")) + 1;
          }else{
            contadorFoto = 0;
          }
          
          FR.onload = function(e) {              
            $("#divImagenes").append('<div class="col-md-12" style="width: 100%; margin-top: 10px" id="div' + contadorFoto + '" index="' + contadorFoto + '">' + 
                '<div style="padding: 0; width: 50%; float: left">' + 
                  '<img src="' + e.target.result + '" width="100%" id="foto' + contadorFoto + '" class="fotoSubida">' + 
                '</div>' + 
                '<div style="padding: 0; width: 50%; float: left">' + 
                  '<img src="../../img/cancel.png" width="26" style="float: right;" id="borrar' + contadorFoto + '" ind="' + contadorFoto + '"></br>' + 
                  '<label>Etiqueta:</label><br>' +  
                  '<select  data-placeholder="" ind="' + contadorFoto + '" id="tag' + contadorFoto + '" name="tags[]" class="form-control chzn-select" multiple="multiple" tabindex="6" >' +
                    
                  '</select>' + 
                  '<button id="btnAgregar' + contadorFoto + '" class="btn btn-success" ind="' + contadorFoto + '"><i class="fa fa-plus" aria-hidden="true"></i></button>' +
                  '<input type="text" id="txtName' + contadorFoto + '" name="txtName' + contadorFoto + '" class="form-control" style="display:none"><br>' +
                  '<div class="col-md-6">' +
                    '<button id="btnEnviar' + contadorFoto + '" class="btn btn-success" style="display:none" ind="' + contadorFoto + '">Agregar</button>' +
                  '</div>' +
                  '<div class="col-md-6">' +
                    '<button id="btnCancelar' + contadorFoto + '" class="btn btn-danger" style="display:none" ind="' + contadorFoto + '">Cancelar</button><br>' +
                  '</div>' +
                  '<span class="error" style="display: none" id="error' + contadorFoto + '"></span>' + 
                '</div> ' + 
              '</div>') 

            llenarLista(contadorFoto, [])

            $('#btnAgregar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              $("#btnAgregar" + ind).css({"display":"none"})
              $("#txtName" + ind).css({"display":"block"})
              $("#btnEnviar" + ind).css({"display":"block"})
              $("#btnCancelar" + ind).css({"display":"block"})
              $("#txtName" + ind).val("")
            });

            $('#btnEnviar' + contadorFoto).click(function (e) {
              
              var ind = $(this).attr("ind")
              var nombre = $("#txtName" + ind).val()

              if($.trim(nombre).length > 0){
                $("#spin").show();
                var datos = {
                  name: nombre
                }   
                //Enviar datos para modificarse
                $.post("<?= base_url();?>index.php/ordenes_medicas/crearTag", datos)
                .done(function( data ) {console.log(data)      
                  llenarListaClase(ind, nombre)
                  $("#btnAgregar" + ind).css({"display":"block"})
                  $("#txtName" + ind).css({"display":"none"})
                  $("#btnEnviar" + ind).css({"display":"none"})
                  $("#btnCancelar" + ind).css({"display":"none"})
                  $("#txtName" + ind).val("")
                  $("#spin").hide();
                  

                  $("#tag" + ind).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
                  $(".chzn-select-deselect").chosen({
                      allow_single_deselect:true
                  });
                  $("#tag" + ind).on('change', function(evt, params) {
                    $("#error" + $(this).attr("ind")).css({"display":"none"})
                    $("#error" + $(this).attr("ind")).html("")
                  });
                });
              }else{
                alert("¡Debe ingresar un nombre de la etiqueta nueva!")
              }
              
            });

            $('#btnCancelar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              $("#btnAgregar" + ind).css({"display":"block"})
              $("#txtName" + ind).css({"display":"none"})
              $("#btnEnviar" + ind).css({"display":"none"})
              $("#btnCancelar" + ind).css({"display":"none"})
              $("#txtName" + ind).val("")
            });

            $("#tag" + contadorFoto).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
            $(".chzn-select-deselect").chosen({
                allow_single_deselect:true
            });
            $("#tag" + contadorFoto).on('change', function(evt, params) {
              $("#error" + $(this).attr("ind")).css({"display":"none"})
              $("#error" + $(this).attr("ind")).html("")
            });

            $(".chzn-container").css({"width":"100%"})
            $(".chzn-drop").css({"width":"100%"})
            $(".default").css({"width":"100%"})
            $(".chzn-choices").css({"width":"100%"})
            $("#btnSubirArchivos").removeAttr("disabled")
            $('#borrar' + contadorFoto).click(function (e) {
              var ind = $(this).attr("ind")
              var confirmar = window.confirm("¿Desea quitar esta foto?")
              if(confirmar){
                $("#div" + ind).remove()
                console.log($("#divImagenes > div").length)
                if($("#btnSubirArchivos").attr("indicajson") == "-1"){
                  if($("#divImagenes > div").length > 0){
                    $("#btnSubirArchivos").removeAttr("disabled")
                  }else{
                    $("#btnSubirArchivos").attr("disabled", "disabled")
                  }
                }
              }
            })
            contadorFoto++;
          }           
          
          FR.readAsDataURL( this.files[i] );
          $(".chzn-container").css({"width":"100%"})
          $(".chzn-drop").css({"width":"100%"})
          $(".default").css({"width":"100%"})
          $(".chzn-choices").css({"width":"100%"})
        }      
          
      }
      $("#btnAceptar").removeAttr("disabled")      
      $("#fileFotosModal").val("")
    }
    
    $('#myModal').on('hidden.bs.modal', function () {
      $("#fileFotos").val("")
      $("#fileFotosModal").val("")
      $("#btnAceptar").attr("disabled", "disabled")
      //$("#divImagenes").html("")
    })
    $('#myModalFotos').on('hidden.bs.modal', function () {
      $("#fileFotos").val("")
      $("#fileFotosModal").val("")
      $("#divImagenes").html("")
      $("#textareaObservacion").val("")
      $("#txtEmailCompartir").val("")
      $("#btnSubirArchivos").removeAttr("disabled")
      $("#btnSubirArchivos").removeAttr("correo")
      $("#errorEmail").css({"display":"none"})
      $("#errorEmail").html("")
      $("#btnAceptar").attr("disabled", "disabled")
    })
    $('#myModalFotos').on('shown.bs.modal', function() {
      $("#errorEmail").css({"display":"none"})
      $("#errorEmail").html("")
      var correo = $("#btnSubirArchivos").attr("correo")
      if($.trim(correo) != "0"){
        $("#lblEmailCompartir").css({"display":"block"})
        $("#txtEmailCompartir").css({"display":"block"})
      }else{
        $("#lblEmailCompartir").css({"display":"none"})
        $("#txtEmailCompartir").css({"display":"none"})
      }
      $("#btnAceptar").attr("disabled", "disabled")
    })
    $("#fileFotos").change( readImage );
    $("#fileFotosModal").change( readImagePopUp );

    $(".sinFoto").click(function() {
      var idDetalle = $(this).attr("iddetallefact")
      var indjson = $(this).attr("indjson")
      var ind = $(this).attr("ind")
      $("#btnSubirInformacion").attr("idfacturadetalle", idDetalle)
      $("#btnSubirInformacion").attr("indicajson", $(this).attr("indjson"))
      
      if($("#btnLlamar" + ind).attr("src") != "../../images/plus_azul.png"){
        $("#myModalSinFotos").modal('show')

        if(indjson != "-1"){
          $("#textareaObservacionSinFoto").val(jsonFotos[parseInt(indjson)].observacion)
        }else{
          $("#textareaObservacionSinFoto").val("")
        }
      }

      if(obtenerTotal() == 0){
        var identificacion = $("#txtIdentificacion").val()
        //Se guardan los datos en un JSON
        var datos = {
          document: identificacion
        }   
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/ordenes_medicas/quitarTurno", datos)
        .done(function( data ) {console.log(data)      
          
        });
      }
      
      
    });

    $(".fotoAgregar").click(function() {
      var idDetalle = $(this).attr("iddetallefact")
      var indjson = $(this).attr("indjson")
      var ind = $(this).attr("ind")
      var correo = $(this).attr("correo")
      $("#btnSubirArchivos").attr("idfacturadetalle", idDetalle)
      $("#btnSubirArchivos").attr("correo", correo)
      $("#btnSubirArchivos").attr("indicajson", $(this).attr("indjson"))

      if($("#btnSinFotos" + ind).attr("src") != "../../images/plus_azul.png"){

        var con = 0;

        for(i = 0; i < jsonFotos.length; i++){
          if(jsonFotos[i] != null){
            if(jsonFotos[i].ideDetalle == idDetalle){
              con++;
            }
          }
          
        }
        if(con != 0){        
          $("#btnSubirArchivos").html("SUBIR CAMBIOS")      
          $('#myModalFotos').modal('show');
          console.log(jsonFotos[parseInt(indjson)].fotos.length)
          for(j = 0; j < jsonFotos[parseInt(indjson)].fotos.length; j++){
            if(jsonFotos[parseInt(indjson)] != null){
              $("#divImagenes").append('<div class="col-md-12" style="width: 100%; margin-top: 10px" id="div' + j + '" index="' + j + '">' + 
                    '<div style="padding: 0; width: 50%; float: left">' + 
                      '<img src="' + jsonFotos[parseInt(indjson)].fotos[j].imagen + '" width="100%" id="foto' + j + '" class="fotoSubida">' + 
                    '</div>' + 
                    '<div style="padding: 0; width: 50%; float: left">' + 
                      '<img src="../../img/cancel.png" width="26" style="float: right;" id="borrar' + j + '" ind="' + j + '"></br>' + 
                      '<label>Etiqueta:</label><br>' + 
                      
                      '<select  data-placeholder="" ind="' + j + '" id="tag' + j + '" name="tags[]" class="form-control chzn-select" multiple="multiple" tabindex="6" >' +
                        
                      '</select>' + 
                      '<button id="btnAgregar' + j + '" class="btn btn-success" ind="' + j + '"><i class="fa fa-plus" aria-hidden="true"></i></button>' +
                      '<input type="text" id="txtName' + j + '" name="txtName' + j + '" class="form-control" style="display:none"><br>' +
                      '<div class="col-md-6">' +
                        '<button id="btnEnviar' + j + '" class="btn btn-success" style="display:none" ind="' + j + '">Agregar</button>' +
                      '</div>' +
                      '<div class="col-md-6">' +
                        '<button id="btnCancelar' + j + '" class="btn btn-danger" style="display:none" ind="' + j + '">Cancelar</button><br>' +
                      '</div>' +
                      '<span class="error" style="display: none" id="error' + j + '"></span>' +    
                    '</div> ' + 
                  '</div>')
                
                $('#btnAgregar' + j).click(function (e) {
                  var ind = $(this).attr("ind")
                  $("#btnAgregar" + ind).css({"display":"none"})
                  $("#txtName" + ind).css({"display":"block"})
                  $("#btnEnviar" + ind).css({"display":"block"})
                  $("#btnCancelar" + ind).css({"display":"block"})
                  $("#txtName" + ind).val("")
                });

                $('#btnEnviar' + j).click(function (e) {
                  
                  var ind = $(this).attr("ind")
                  var nombre = $("#txtName" + ind).val()

                  if($.trim(nombre).length > 0){
                    $("#spin").show();
                    var datos = {
                      name: nombre
                    }   
                    //Enviar datos para modificarse
                    $.post("<?= base_url();?>index.php/ordenes_medicas/crearTag", datos)
                    .done(function( data ) {console.log(data)      
                      llenarListaClase(ind, nombre)
                      $("#btnAgregar" + ind).css({"display":"block"})
                      $("#txtName" + ind).css({"display":"none"})
                      $("#btnEnviar" + ind).css({"display":"none"})
                      $("#btnCancelar" + ind).css({"display":"none"})
                      $("#txtName" + ind).val("")
                      $("#spin").hide();
                      

                      $("#tag" + ind).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
                      $(".chzn-select-deselect").chosen({
                          allow_single_deselect:true
                      });
                      $("#tag" + ind).on('change', function(evt, params) {
                        $("#error" + $(this).attr("ind")).css({"display":"none"})
                        $("#error" + $(this).attr("ind")).html("")
                      });
                    });
                  }else{
                    alert("¡Debe ingresar un nombre de la etiqueta nueva!")
                  }
                  
                });

                $('#btnCancelar' + j).click(function (e) {
                  var ind = $(this).attr("ind")
                  $("#btnAgregar" + ind).css({"display":"block"})
                  $("#txtName" + ind).css({"display":"none"})
                  $("#btnEnviar" + ind).css({"display":"none"})
                  $("#btnCancelar" + ind).css({"display":"none"})
                  $("#txtName" + ind).val("")
                });
                
                $("#tag" + j).chosen({no_results_text: "¡Etiqueta no encontrada! Agregala como nueva"}); 
                $(".chzn-select-deselect").chosen({
                    allow_single_deselect:true
                });
                $("#tag" + j).on('change', function(evt, params) {
                  $("#error" + $(this).attr("ind")).css({"display":"none"})
                  $("#error" + $(this).attr("ind")).html("")
                });

                $("#textareaObservacion").val(jsonFotos[parseInt(indjson)].observacion)
                $("#txtEmailCompartir").val(jsonFotos[parseInt(indjson)].correo)
                $(".chzn-container").css({"width":"100%"})
                $(".chzn-drop").css({"width":"100%"})
                $(".default").css({"width":"100%"})
                $(".chzn-choices").css({"width":"100%"})

                if($.trim(jsonFotos[parseInt(indjson)].tags[j].id) != ""){
                  var res = jsonFotos[parseInt(indjson)].tags[j].id.split(",");
                  $("#tag" + j).chosen().val(res)
                  $("#tag" + j).trigger("liszt:updated");
                  llenarLista(j, res)
                }else{
                  llenarLista(j, [])
                }
                

              $('#borrar' + j).click(function (e) {
                var ind = $(this).attr("ind")
                var confirmar = window.confirm("¿Desea quitar esta foto?")
                if(confirmar){
                  $("#div" + ind).remove()
                  if($("#btnSubirArchivos").attr("indicajson") == "-1"){
                    if($("#divImagenes > div").length > 0){
                      $("#btnSubirArchivos").removeAttr("disabled")
                    }else{
                      $("#btnSubirArchivos").attr("disabled", "disabled")
                    }
                  }
                }
              })
            }
          }

          $(".chzn-container").css({"width":"100%"})
          $(".chzn-drop").css({"width":"100%"})
          $(".default").css({"width":"100%"})
          $(".chzn-choices").css({"width":"100%"})

          

        }else{
          $("#myModal").modal('show') 
          $("#btnSubirArchivos").html("SUBIR ARCHIVOS")   
          if(obtenerTotal() == 0){
            var identificacion = $("#txtIdentificacion").val()
            //Se guardan los datos en un JSON
            var datos = {
              document: identificacion
            }   
            //Enviar datos para modificarse
            $.post("<?= base_url();?>index.php/ordenes_medicas/quitarTurno", datos)
            .done(function( data ) {console.log(data)      
              
            });
          }
        }

      }
      
    });

    function borrarAtencion(){
      $('.sinFoto').each(function(ind) { 
        if($("#btnSinFotos" + ind).attr("atendido") == "si"){
          console.log($("#btnSinFotos" + ind).attr("iddetallefact")) 
        }else{
          
        }
        
      })
      
    }
    $("#btnSubirInformacion").click(function (e) {
      var jsonSRC = []
      var jsonEtiquetas = []
      var observacion = $("#textareaObservacionSinFoto").val()
      var idDetalle = $("#btnSubirInformacion").attr("idfacturadetalle")
      var indicajson = $("#btnSubirInformacion").attr("indicajson")

      if($.trim(observacion).length > 0){

        if(indicajson != "-1"){
          jsonFotos[parseInt(indicajson)] = {
            ideDetalle: idDetalle,
            observacion: observacion,
            fotos: jsonSRC,
            tags: jsonEtiquetas
          }
          var contadorAgregados = 0;
          $('.sinFoto').each(function() {
            if($(this).attr("iddetallefact") == idDetalle){
              $(this).attr("indjson", indicajson)
              var ind = $(this).attr("ind")
              $("#btnLlamar" + ind).attr("atendido", "si")
              $("#btnSinFotos" + ind).attr("atendido", "si")
              $(this).attr("src", "../../images/plus_azul.png")
            }
            if($(this).attr("src") == "../../images/plus_azul.png"){
              contadorAgregados++;
            }
            
          });

          if(contadorAgregados > 0){
            $("#btnRegistrarResultados").removeAttr("disabled")
          }
          $("#myModalSinFotos").modal("hide");
          $(".modal-backdrop").css({"display":"none"})
          $("#btnSubirInformacion").removeAttr("idfacturadetalle")
          $("#btnSubirInformacion").removeAttr("indicajson")
          $("#textareaObservacionSinFoto").val("")
        }else{
          jsonFotos[jsonFotos.length] = {
            ideDetalle: idDetalle,
            observacion: observacion,
            fotos: jsonSRC,
            tags: jsonEtiquetas
          }
          var contadorAgregados = 0;
          $('.sinFoto').each(function() {
            if($(this).attr("iddetallefact") == idDetalle){
              $(this).attr("indjson", (jsonFotos.length - 1))              
              var ind = $(this).attr("ind")
              $("#btnLlamar" + ind).attr("atendido", "si")
              $("#btnSinFotos" + ind).attr("atendido", "si")
              $(this).attr("src", "../../images/plus_azul.png")
            }
            if($(this).attr("src") == "../../images/plus_azul.png"){
              contadorAgregados++;
            }
            
          });

          if(contadorAgregados > 0){
            $("#btnRegistrarResultados").removeAttr("disabled")
          }
          $("#myModalSinFotos").modal("hide");
          $(".modal-backdrop").css({"display":"none"})
          $("#btnSubirInformacion").removeAttr("idfacturadetalle")
          $("#btnSubirInformacion").removeAttr("indicajson")
          $("#textareaObservacionSinFoto").val("")
          console.log(jsonFotos)
        }
      }else{
        alert("Debe ingresar una observacion")
      }
      

      
    });

    $("#btnSubirArchivos").click(function (e) {
      
      var idDetalle = $("#btnSubirArchivos").attr("idfacturadetalle")
      var indicajson = $("#btnSubirArchivos").attr("indicajson")
      var jsonSRC = []
      var jsonEtiquetas = []
      var observacion = $("#textareaObservacion").val()
      var contadorVacio = 0;      
      var estadoCorreo = $(this).attr("correo")
      var correo = $("#txtEmailCompartir").val()
      
      if($("#divImagenes > div").length > 0){

        if(indicajson != "-1"){

          $("#divImagenes > div").each(function() {
            if($(this).length > 0){
              var etiquetas = "";
              var index = parseInt($(this).attr("index"))
              

              if($.trim(estadoCorreo) != "0"){
                if($("#tag" + index).chosen().val() != null && $.trim(correo) != ""){
                  for(i = 0; i < $("#tag" + index).chosen().val().length; i++){
                    if(i == 0){
                      etiquetas += $("#tag" + index).chosen().val()[i];
                    }else{
                      etiquetas += "," + $("#tag" + index).chosen().val()[i];
                    }
                  }

                  jsonSRC[jsonSRC.length] = {
                    imagen: $("#foto" + index).attr("src")
                  }
                  jsonEtiquetas[jsonEtiquetas.length] = {
                    id: etiquetas
                  }
                }else{
                  if($("#tag" + index).chosen().val() == null){
                    $("#error" + index).css({"display":"block"})
                    $("#error" + index).html("Seleccione al menos una etiqueta")
                  }
                  
                  contadorVacio++;
                }
              }else{
                if($("#tag" + index).chosen().val() != null){
                  for(i = 0; i < $("#tag" + index).chosen().val().length; i++){
                    if(i == 0){
                      etiquetas += $("#tag" + index).chosen().val()[i];
                    }else{
                      etiquetas += "," + $("#tag" + index).chosen().val()[i];
                    }
                  }

                  jsonSRC[jsonSRC.length] = {
                    imagen: $("#foto" + index).attr("src")
                  }
                  jsonEtiquetas[jsonEtiquetas.length] = {
                    id: etiquetas
                  }
                }else{
                  if($("#tag" + index).chosen().val() == null){
                    $("#error" + index).css({"display":"block"})
                    $("#error" + index).html("Seleccione al menos una etiqueta")
                  }
                  
                  contadorVacio++;
                }
              }
                   
              
            } 
          });
          
          if(contadorVacio == 0){
            jsonFotos[parseInt(indicajson)] = {
              ideDetalle: idDetalle,
              observacion: observacion,
              fotos: jsonSRC,
              tags: jsonEtiquetas,
              correo: correo
            }

            $('.fotoAgregar').each(function() {
              if($(this).attr("iddetallefact") == idDetalle){
                $(this).attr("indjson", indicajson)
              }
            });
            $("#myModalFotos").modal("hide");
            $(".modal-backdrop").css({"display":"none"})

            $("#errorEmail").css({"display":"none"})
            $("#errorEmail").html("")
          }else{
            if($.trim(estadoCorreo) != "0"){
              var correo = $("#txtEmailCompartir").val()
              if($.trim(correo).length == 0){
                $("#errorEmail").css({"display":"block"})
                $("#errorEmail").html("Debe ingresar un correo valido")
              }
            }
            alert("Hay datos sin llenar")
          }
        }else{

          $("#divImagenes > div").each(function() {
            if($(this).length > 0){
              var etiquetas = "";
              var index = parseInt($(this).attr("index"))              

              if($.trim(estadoCorreo) != "0"){
                if($("#tag" + index).chosen().val() != null && $.trim(correo) != ""){
                  for(i = 0; i < $("#tag" + index).chosen().val().length; i++){
                    if(i == 0){
                      etiquetas += $("#tag" + index).chosen().val()[i];
                    }else{
                      etiquetas += "," + $("#tag" + index).chosen().val()[i];
                    }
                  }
                  jsonSRC[jsonSRC.length] = {
                    imagen: $("#foto" + index).attr("src")
                  }
                  jsonEtiquetas[jsonEtiquetas.length] = {
                    id: etiquetas
                  }
                }else{
                  if($("#tag" + index).chosen().val() == null){
                    $("#error" + index).css({"display":"block"})
                    $("#error" + index).html("Seleccione al menos una etiqueta")
                  }
                  contadorVacio++;
                }
              }else{
                if($("#tag" + index).chosen().val() != null){
                  for(i = 0; i < $("#tag" + index).chosen().val().length; i++){
                    if(i == 0){
                      etiquetas += $("#tag" + index).chosen().val()[i];
                    }else{
                      etiquetas += "," + $("#tag" + index).chosen().val()[i];
                    }
                  }
                  jsonSRC[jsonSRC.length] = {
                    imagen: $("#foto" + index).attr("src")
                  }
                  jsonEtiquetas[jsonEtiquetas.length] = {
                    id: etiquetas
                  }
                }else{
                  if($("#tag" + index).chosen().val() == null){
                    $("#error" + index).css({"display":"block"})
                    $("#error" + index).html("Seleccione al menos una etiqueta")
                  }
                  contadorVacio++;
                }
              }

              
            }      
            //console.log(JSON.stringify($("#tag" + index).chosen().val()))
            //console.log($("#tag" + index).chosen().val())
          });
          console.log("Numero de Vacios: " + contadorVacio)
          if(contadorVacio == 0){
            jsonFotos[jsonFotos.length] = {
              ideDetalle: idDetalle,
              observacion: observacion,
              fotos: jsonSRC,
              tags: jsonEtiquetas,
              correo: correo
            }

            var contadorAgregados = 0;
            $('.fotoAgregar').each(function() {
              if($(this).attr("iddetallefact") == idDetalle){
                $(this).attr("indjson", (jsonFotos.length - 1))
                var ind = $(this).attr("ind")
                $("#btnLlamar" + ind).attr("atendido", "si")
                $("#btnSinFotos" + ind).attr("atendido", "si")
                $(this).attr("src", "../../images/plus_azul.png")
              }
              if($(this).attr("src") == "../../images/plus_azul.png"){
                contadorAgregados++;
              }
              
            });
            if(contadorAgregados > 0){
              $("#btnRegistrarResultados").removeAttr("disabled")
              $("#btnCancelarResultados").removeAttr("disabled")
              $("#btnAceptar").attr("disabled", "disabled")
            }
            
            $("#btnSubirArchivos").removeAttr("idfacturadetalle")
            $("#btnSubirArchivos").removeAttr("indicajson")
            $("#myModalFotos").modal("hide");
            $(".modal-backdrop").css({"display":"none"})
            console.log(jsonFotos)
            $("#errorEmail").css({"display":"none"})
            $("#errorEmail").html("")
          }else{
            if($.trim(estadoCorreo) != "0"){
              var correo = $("#txtEmailCompartir").val()
              if($.trim(correo).length == 0){
                $("#errorEmail").css({"display":"block"})
                $("#errorEmail").html("Debe ingresar un correo valido")
              }
            }
            alert("Hay etiquetas sin llenar")
          }
          
          
        }

      }else{
        if(indicajson != "-1"){
          $('.fotoAgregar').each(function() {
            if($(this).attr("iddetallefact") == idDetalle){
              $(this).attr("indjson", "-1")
              $(this).attr("src", "../../images/plus_blanco.png")
              var ind = $(this).attr("ind")
              $("#btnLlamar" + ind).attr("atendido", "no")
              $("#btnSinFotos" + ind).attr("atendido", "no")
              delete jsonFotos[ parseInt(indicajson) ];
            }
          });

          $("#myModalFotos").modal("hide");
          $(".modal-backdrop").css({"display":"none"})
        }/*else{
          $('.fotoAgregar').each(function() {
            if($(this).attr("iddetallefact") == idDetalle){
              $(this).attr("indjson", "-1")
              $(this).attr("src", "../../images/plus_blanco.png")
            }
          });
          $("#myModalFotos").modal("hide");
          $(".modal-backdrop").css({"display":"none"})
        }*/
      }
      
      
      
    })
    function obtenerTotal(){
      var total = 0;
      $('.fotoAgregar[src="../../images/plus_azul.png"]').each(function() {         
        total++;
      });
      $('.sinFoto[src="../../images/plus_azul.png"]').each(function() {      
        total++;
      });
      return total;
    }

    function obtenerTotalSinAtender(){
      var total = 0;
      $('.fotoAgregar').each(function() {         
        var ind = $(this).attr("ind")
        if($("#btnSinFotos" + ind).attr("atendido") == "no"){
          total++;
        }
      });
      return total;
    }
    $("#btnRegistrarResultados").click(function() {
      var identificacion = $("#txtIdentificacion").val()
      var total = $("#txtValorTotal").val()  
      var contador = 1; 
      $("#btnRegistrarResultados").attr("disabled", "disabled")     
      $("#spin").show();

      $('.fotoAgregar').each(function() {     
        var ind = $(this).attr("ind")

        if($(this).attr("src") == "../../images/plus_azul.png"){
          var idDetalle = $(this).attr("iddetallefact")
          var profesional = $(this).attr("profesional")
          var name_profesional = $(this).attr("name_profesional")
          var clinica = $(this).attr("clinica")
          var indjson = $(this).attr("indjson")
          var id_orden_medica = $(this).attr("id_orden_medica")
          var id_procedimiento = $(this).attr("id_procedimiento")

          var datos = {
            user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
            document: "<?= $this->session->userdata('document_Seleccionado');?>",
            idFactura: "<?= $this->session->userdata('idFactura_Seleccionada');?>",
            idDetalle: idDetalle,
            orden_medica: id_orden_medica,
            id_procedimiento: id_procedimiento,
            observacion: jsonFotos[parseInt(indjson)].observacion,
            jsonFotos: JSON.stringify(jsonFotos[parseInt(indjson)].fotos),
            jsonTags: JSON.stringify(jsonFotos[parseInt(indjson)].tags),
            tipo: "CONFOTOS",
            profesional: profesional,
            name_profesional: name_profesional,
            clinica: clinica,
            correo: jsonFotos[parseInt(indjson)].correo
          }   
          $.post("<?= base_url();?>index.php/ordenes_medicas/addImagenesRadiologia", datos)
          .done(function( result ) {console.log(result)                     
            if(contador == obtenerTotal()){
              //$("#spin").hide();
              borrarAtencion()
              //window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
            }else{
              contador++;  
            } 
          }); 
        }else{
          if($("#btnSinFotos" + ind).attr("src") == "../../images/plus_azul.png"){
            var idDetalle = $("#btnSinFotos" + ind).attr("iddetallefact")
            var indjson = $("#btnSinFotos" + ind).attr("indjson")
            var profesional = $("#btnSinFotos" + ind).attr("profesional")
            var name_profesional = $(this).attr("name_profesional")
            var clinica = $(this).attr("clinica")
            var id_orden_medica = $(this).attr("id_orden_medica")
            var id_procedimiento = $(this).attr("id_procedimiento")

            var datos = {
              user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
              document: "<?= $this->session->userdata('document_Seleccionado');?>",
              idFactura: "<?= $this->session->userdata('idFactura_Seleccionada');?>",
              idDetalle: idDetalle,
              orden_medica: id_orden_medica,
              id_procedimiento: id_procedimiento,
              observacion: jsonFotos[parseInt(indjson)].observacion,
              jsonFotos: JSON.stringify(jsonFotos[parseInt(indjson)].fotos),
              jsonTags: JSON.stringify(jsonFotos[parseInt(indjson)].tags),
              tipo: "SINFOTOS",
              profesional: profesional,
              clinica: clinica,
              name_profesional: name_profesional,
              correo: ""
            }   
            $.post("<?= base_url();?>index.php/ordenes_medicas/addImagenesRadiologia", datos)
            .done(function( result ) {console.log(result)                     
              if(contador == obtenerTotal()){
                //$("#spin").hide();
                borrarAtencion()
                //window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
              }else{
                contador++;  
              } 
            }); 
          }
        }   
        
          
        
      });      
      console.log($('.fotoAgregar[src="../../images/plus_azul.png"]').length)
      
    })

    function borrarAtencion(){
      var contadorFoto = 1; 
      $('.sinFoto').each(function() { 
        var ind = $(this).attr("ind")
        
        if($("#btnSinFotos" + ind).attr("atendido") == "no"){                    
          var idDetalle = $("#btnSinFotos" + ind).attr("iddetallefact")          
          var id_factura = $("#btnSinFotos" + ind).attr("id_factura_radiologia")
          var datos = {
            detalle: idDetalle,
            idFactura: id_factura
          } 
          $.post("<?= base_url();?>index.php/ordenes_medicas/borrarAtencion", datos)
          .done(function( data ) {console.log(data)   
            console.log(contadorFoto);
            console.log(obtenerTotalSinAtender());   
            if(contadorFoto == obtenerTotalSinAtender()){
              $("#spin").hide();
              window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
            }else{
              contadorFoto++;  
            } 

          });
          console.log($("#btnSinFotos" + ind).attr("iddetallefact"))
          
        }else{
          //console.log($("#btnLlamar" + ind).attr("iddetallefact")) 
        }
        
      })
      
    }

    $("#btnCancelarResultados").click(function() {
      var identificacion = $("#txtIdentificacion").val()
      //Se guardan los datos en un JSON
      var datos = {
        document: identificacion
      }   
      //Enviar datos para modificarse
      $.post("<?= base_url();?>index.php/ordenes_medicas/updateFacturaCancelar", datos)
      .done(function( data ) {console.log(data)      
        window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
      });
      
      
    })

    /*$("#btnCancelar").click(function() {
      if(obtenerTotal() == 0){
        var identificacion = $("#txtIdentificacion").val()
        //Se guardan los datos en un JSON
        var datos = {
          document: identificacion
        }   
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/ordenes_medicas/agregarTurno", datos)
        .done(function( data ) {console.log(data)      
          
        });
      }
      
      
    })

    $("#btnCancelarInformacion").click(function() {
      if(obtenerTotal() == 0){
        var identificacion = $("#txtIdentificacion").val()
        //Se guardan los datos en un JSON
        var datos = {
          document: identificacion
        }   
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/ordenes_medicas/agregarTurno", datos)
        .done(function( data ) {console.log(data)      
          
        });
      }
      
      
    })*/

    function llenarLista(id, datos){
      $.post("<?= base_url();?>index.php/ordenes_medicas/listTags", {})
      .done(function( data ) {console.log(data)
        if($.trim(data) != "[]"){
          var json = JSON.parse(data);
          $("#tag" + id).html("")

          for(i = 0; i < json.length; i++){
            $("#tag" + id).append('<option value="' + json[i].value + '">' + json[i].label + '</option>');
            $("#tag" + id).chosen().val(datos)
            $("#tag" + id).trigger("liszt:updated");

            $(".chzn-container").css({"width":"100%"})
            $(".chzn-drop").css({"width":"100%"})
            $(".default").css({"width":"100%"})
            $(".chzn-choices").css({"width":"100%"})
          }
          

          $(".chzn-container").css({"width":"100%"})
          $(".chzn-drop").css({"width":"100%"})
          $(".default").css({"width":"100%"})
          $(".chzn-choices").css({"width":"100%"})
        } 
      });
    }

    function llenarListaClase(ind, nombre){
      $.post("<?= base_url();?>index.php/ordenes_medicas/listTags", {})
      .done(function( data ) {console.log(data)
        if($.trim(data) != "[]"){
          var json = JSON.parse(data);
            
          $("#divImagenes > div").each(function() {            
            var index = $(this).attr("index")
            var arrayEtiquetas = $("#tag" + index).chosen().val();
            $("#tag" + index).html("")

            for(i = 0; i < json.length; i++){
              $("#tag" + index).append('<option value="' + json[i].value + '">' + json[i].label + '</option>');
            }
            if(ind == index){
              if(arrayEtiquetas != null){
                arrayEtiquetas[arrayEtiquetas.length] = nombre;
                $("#tag" + index).chosen().val(arrayEtiquetas)  
                $("#tag" + index).trigger("liszt:updated");    
              }else{
                arrayEtiquetas = [];
                arrayEtiquetas[0] = nombre;
                $("#tag" + index).chosen().val(arrayEtiquetas)  
                $("#tag" + index).trigger("liszt:updated");   
              }
              
            }else{
              if(arrayEtiquetas != null){
                $("#tag" + index).chosen().val(arrayEtiquetas)  
                $("#tag" + index).trigger("liszt:updated");   
              }else{
                $("#tag" + index).chosen().val([])  
                $("#tag" + index).trigger("liszt:updated");   
              } 
            }
                      
            console.log($("#tag" + index).chosen().val())
          });
          
          $(".chzn-container").css({"width":"100%"})
          $(".chzn-drop").css({"width":"100%"})
          $(".default").css({"width":"100%"})
          $(".chzn-choices").css({"width":"100%"})


        } 
      });
    }

    /*setTimeout(function(){ 
      var identificacion = $("#txtIdentificacion").val()
      //Se guardan los datos en un JSON
      var datos = {
        document: identificacion
      }   
      //Enviar datos para modificarse
      $.post("<?= base_url();?>index.php/ordenes_medicas/quitarTurno", datos)
      .done(function( data ) {console.log(data)      
        
      });  
    }, 50000);*/
</script>
</body>
</html>