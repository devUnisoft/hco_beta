<?php
  $profileCR = $this->session->userdata('UserIDInternoCR');
  if(empty($profileCR)) { // Recuerda usar corchetes.
      header('Location: ' . base_url());
  }
?>
<script type="text/javascript">
    function nobackbutton(){
       window.location.hash="no-back-button";
       window.location.hash="Again-No-back-button" 
       window.onhashchange=function(){window.location.hash="no-back-button";} 
    }
</script>
<style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}


 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
  margin-bottom: 0;
  margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 2px; 
     border-right: 1px solid;
     font-weight: bold;
}
  </style>
<body onload="nobackbutton();">

<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">TURNERO - CONSULTORIO</h3>

  
  <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
    ORDENES REGISTRADAS POR RECEPCION
  </div>
  <br><br>

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="0">
    <thead>
      <tr style="color: #0683c9; font-weight: bolder;">
        <td>HORA</td>
        <td>DESCRIPCION</td>
        <td>CANTIDAD</td>
        <td>NOMBRE</td>
        <td>OBSERV. INICIALES DE SOLICITUD</td>
        <td>OBSERV. DE RECEPCION</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="7" height="1px" style="padding: 0; border: 0">&nbsp;</td>
      </tr>
    </thead>
    <tbody id="tbodyOrdenMedica">
      <?php
        $contador = 0;
        if($facturas != null){
          foreach ($facturas->result() as $value) {
            $fecha = $value->hora;
            $descripcion = $value->descripcion;
            $cantidad = $value->cantidad;
            $name = $value->first_name . " " . $value->last_name;
            $observacion_inicial = $value->observacion_inicial;
            $observacion_recepcion = $value->observacion_recepcion;
            $id_factura_radiologia = $value->id_factura_radiologia;
            $id_detalle = $value->id;
            $documento = $value->documento;
            $atendido = $value->atendido;
          

        
          
      ?>
        <tr>
          <td><?= $fecha;?></td>
          <td><?= $descripcion;?></td>
          <td><?= $cantidad;?></td>
          <td><?= $name;?></td>
          <td><?= $observacion_inicial;?></td>
          <td><?= $observacion_recepcion;?></td>
          <?php
            if(trim($atendido) == "true"){
          ?>
          <td align="center"><button class="btn btn-warning boton" disabled="disabled" ind="<?= $contador;?>" id="btnLlamar<?= $contador;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" id_detalle_radiologia="<?= $id_detalle;?>" documento="<?= $documento;?>"><b>LLAMADO</b></button></td>
          <?php
            }else{
              ?>
          <td align="center"><button class="btn btn-success boton" ind="<?= $contador;?>" id="btnLlamar<?= $contador;?>" id_factura_radiologia="<?= $id_factura_radiologia;?>" id_detalle_radiologia="<?= $id_detalle;?>" documento="<?= $documento;?>"><b>LLAMADO</b></button></td>
          <?php
            }
          ?>
        </tr>
        <tr>
          <td colspan="7" height="1px" style="padding: 0; border: 0">&nbsp;</td>
        </tr>
      <?php  
        $contador++;
        }
      }
      ?>                     
    </tbody>
  </table>
  <br>
  <?= $footerPage;?>  
  <br><br>
  </div>

<div id="myModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">
          
          <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

        </div>
        <div class="modal-body" style="border: 0px; ">

          <div class="col-md-12">
            <h3 style="color: #428bca; font-weight: bolder" align="center">INICIO DE ATENCIÓN AL PACIENTE</h3>

            <div class="col-md-12" style="">                           
              <button type="button" class="btn btn-primary" id="btnIniciarConsulta" style="width: 100%">INICIAR CONSULTA</button><br><br>
              <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarConsulta" style="width: 100%">CANCELAR</button>
            </div>        
          </div>

        </div>
        <div class="modal-footer" style="border: 0px">
          
          
        </div>

      </div>

    </div>
  </div>
    
    <script src="<?= base_url();?>js/jquery.validate.js"></script>
    <script type="text/javascript">
    var interval = null;
      $(".boton").click(function(e){
        var idFactura = $(this).attr("id_factura_radiologia")
        var documento = $(this).attr("documento")
        var detalle_radiologia = $(this).attr("id_detalle_radiologia")

        
        
        //Se guardan los datos en un JSON
        var datos = {
          user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
          document: documento,
          idFactura: idFactura
        }   
        console.log(datos) 
        $.post("<?= base_url();?>index.php/ordenes_medicas/updateFactura", datos)
        .done(function( data ) {console.log(data)      
          $("#myModal").modal('show')
          clearInterval(interval);
          $("#btnIniciarConsulta").attr("document", documento)
          $("#btnIniciarConsulta").attr("detalle", detalle_radiologia)
          $("#btnIniciarConsulta").attr("idfactura", idFactura)
          $('.boton').each(function() {  
            if($.trim($(this).attr("documento")) == documento){
              $(this).addClass("btn-danger")
            }       
            
          });
        });
        
      })
      interval = setInterval(function(){ 
        //window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
      },10000);

      /*function listar(){
        $("#tbodyOrdenMedica").html("")
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/user_radiologia/obtenerFacturasProcesar", {})
        .done(function( data ) {console.log(data)     
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)

            for (var i = 0; i < json.length; i++) {
              var boton = "";

              if($.trim(json[i].atendido) == "true"){
                boton = '<button class="btn btn-warning boton" disabled="disabled" ind="' + i + '" id="btnLlamar' + i + '" id_factura_radiologia="' + json[i].id_factura_radiologia + '" documento="' + json[i].documento + '"><b>LLAMADO</b></button>';
              }else{
                boton = '<button class="btn btn-success boton" ind="' + i + '" id="btnLlamar' + i + '" id_factura_radiologia="' + json[i].id_factura_radiologia + '" documento="' + json[i].documento + '"><b>LLAMADO</b></button>';
              }
              $("#tbodyOrdenMedica").append('<tr><td>' + json[i].hora + '</td><td>' + json[i].descripcion + '</td><td align="center">' + json[i].cantidad + '</td><td>' + json[i].first_name + ' ' + json[i].last_name + '</td><td>' + json[i].observacion_inicial + '</td><td>' + json[i].observacion_recepcion + '</td><td align="center">' + boton + '</td></tr><tr><td colspan="7" height="1px" style="padding: 0; border: 0">&nbsp;</td></tr>')
            }

            $(".boton").click(function(e){
              var idFactura = $(this).attr("id_factura_radiologia")
              var documento = $(this).attr("documento")
              
              //Se guardan los datos en un JSON
              var datos = {
                user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
                document: documento,
                idFactura: idFactura
              }   
              //Enviar datos para modificarse
              $.post("<?= base_url();?>index.php/ordenes_medicas/updateFactura", datos)
              .done(function( data ) {console.log(data)      
                window.location.href = "<?= base_url();?>index.php/ordenes_medicas/capturaInformacion";
              });
            })
          } 
          
        });
      }*/

      $('#myModalFotos').on('shown.bs.modal', function() {
          
      })

      $("#btnIniciarConsulta").click(function(e){
        var identificacion = $("#btnIniciarConsulta").attr("document")
        var detalle = $("#btnIniciarConsulta").attr("detalle")
        var idfactura = $("#btnIniciarConsulta").attr("idfactura")
        //Se guardan los datos en un JSON
        var datos = {
          document: identificacion,
          detalle: detalle,
          idFactura: idfactura
        }   
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/ordenes_medicas/iniciarAtencion", datos)
        .done(function( data ) {console.log(data)      
          $("#btnIniciarConsulta").removeAttr("document")
          window.location.href = "<?= base_url();?>index.php/ordenes_medicas/capturaInformacion";
        });
        
      })

      $("#btnCancelarConsulta").click(function(e){
        var identificacion = $("#btnIniciarConsulta").attr("document")
        //Se guardan los datos en un JSON
        var datos = {
          document: identificacion
        }   
        //Enviar datos para modificarse
        $.post("<?= base_url();?>index.php/ordenes_medicas/quitarTurno", datos)
        .done(function( data ) {console.log(data)      
          $("#btnIniciarConsulta").removeAttr("document")

          $('.boton').each(function() {  
            if($.trim($(this).attr("documento")) == identificacion){
              $(this).removeClass("btn-danger")
            }       
            
          });

          interval = setInterval(function(){ 
            window.location.href = "<?= base_url();?>index.php/user_radiologia/home";
          },10000);
        });
      })
    </script>
</body>
</html>