<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= $titulo;?></title>
    <link href="<?= base_url();?>css/bootstrap.css" rel="stylesheet">
    <link href="<?= base_url();?>css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?= base_url();?>css/style.css" rel="stylesheet">
    <link href="<?= base_url();?>css/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="<?= base_url();?>lib/validation/css/validation.css" rel="stylesheet">
    <link href="<?= base_url();?>lib/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?= base_url();?>css/base.css" rel="stylesheet">
    <link href="<?= base_url();?>tools/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="<?= base_url();?>js/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?= base_url();?>css/bootstrap-select.css">
    <script src="<?= base_url();?>js/bootstrap-select.js"></script>


    <link rel="stylesheet" type="text/css" href="<?= base_url();?>css/chosen.css" />
    <script type="text/javascript" src = "http://www.google.com/jsapi" charset="utf-8"></script>
    <script type="text/javascript" src="<?= base_url();?>js/ValidacionNumerica.js"></script>

    <style type="text/css">
    </script>
    <style type="text/css">
    
        .loader {
            position: fixed;
            bottom:0px;
            left: 0px;
            right: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
        }

        header {
    width: 100%;
    height: 90px;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
        width:100%;
        height:90px;
        background-color:#FFFFFF;
        color:#000000;
        bottom:0px;
        left: 0px;
        right: 0px;
        clear:both;
    }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



    .nav {
    left:50%;
    margin-left:-150px;
    top:50px;
    position:absolute;
}
.nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
    background:#fff;
}
.dropdown {
    border-radius:4px;
    width:300px;    
}
.dropdown-menu>li>a {
    color:#428bca;
}
.dropdown ul.dropdown-menu {
    border-radius:4px;
    box-shadow:none;
    margin-top:20px;
    width:300px;
}
.dropdown ul.dropdown-menu:before {
    content: "";
    border-bottom: 10px solid #fff;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    top: -10px;
    right: 16px;
    z-index: 10;
}
.dropdown ul.dropdown-menu:after {
    content: "";
    border-bottom: 12px solid #ccc;
    border-right: 12px solid transparent;
    border-left: 12px solid transparent;
    position: absolute;
    top: -12px;
    right: 14px;
    z-index: 9;
}




.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }

span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin-left: -130px;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.notifications, footer .int li .notifications {
    background-color: #3ca3c1;
    color: #fff;
    position: absolute;
    left: 42%;
    font-weight: bold;
    padding: 4px;
    border-radius: 12px;
}




@media only screen and (max-width: 1200px) {    

/* Styles */
.Redo {
     width: 60%;
     height: 60% 
}
.perfil {
     width: 100%;
     height: 100% 
}
.Undo {
     width: 60%;
     height: 60% 
}

}

@media only screen and (max-width: 800px) {    

/* Styles */
.Redo {
     width: 100%;
     height: 100% 
}
.perfil {
     width: 110%;
     height: 110% 
}
.Undo {
     width: 100%;
     height: 100% 
}

body{
    height:500px;
}
html{
    height:auto; 
}
#logosuperior{
    height: 30%;
    width: 30%;
}

}

@media only screen and (min-width: 710px){

/* Styles */
.Redo {
     width: 30%;
     height: 30% 
}
.perfil {
     width: 60%;
     height: 60% 
}
.Undo {
     width: 30%;
     height: 30% 
}

#logosuperior{
    height: 15%;
    width: 15%;
}

}

    </style>
    
</head>