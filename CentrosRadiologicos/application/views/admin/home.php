<?php
  $profileCR = $this->session->userdata('UserIDInternoCR');
  if(empty($profileCR)) { // Recuerda usar corchetes.
      header('Location: ' . base_url());
  }
?>
<style type="text/css">
  .menu{
    text-decoration: none;
  }
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
  .col-md-5 {
      width: 41.66666667%;
  }
}
.tabla td, .tabla th {
     padding: 10px; 
}

  </style>
 <script type="text/javascript">
    function nobackbutton(){
       window.location.hash="no-back-button";
       window.location.hash="Again-No-back-button" 
       window.onhashchange=function(){window.location.hash="no-back-button";} 
    }
</script>
<body onload="nobackbutton();">
<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">Bienvenido Admin <?= $this->session->userdata('UserIDInternoCR');?></h3>
  <br>
  <div class="row" align="center"> 
    <a href="<?= base_url();?>index.php/user_radiologia/adminProfesionales" class="menu">
      <img src="../../img/icon_radiologia1.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/user_radiologia/adminAuxiliares" class="menu">
      <img src="../../img/icon_radiologia2.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/ordenes_medicas/anulacionFactura" class="menu">
      <img src="../../img/icon_radiologia3.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/ordenes_medicas/reporteCierreCaja" class="menu">
      <img src="../../img/icon_radiologia4.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/ordenes_medicas/adminProcedimientos" class="menu">
      <img src="../../img/icon_radiologia5.png" width="140">
    </a>  
    <a href="<?= base_url();?>index.php/ordenes_medicas/adminConvenios" class="menu">
      <img src="<?= base_url();?>img/icon_radiologia6.png" width="140">
    </a>  
    <a href="<?= base_url();?>index.php/user_radiologia/entregaResultados" class="menu">
      <img src="<?= base_url();?>images/Entrega Resultados.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/clinica/verClinicas" class="menu">
      <img src="<?= base_url();?>images/parametrosgenerales.png" width="140">
    </a>
    <a href="<?= base_url();?>index.php/clinica/verRips" class="menu">
      <img src="<?= base_url();?>images/rip.png" width="140">
    </a>   
    <a href="<?= base_url();?>index.php/user_radiologia/logoutUser" class="menu">
      <img id="salir" src="<?= base_url();?>images/Salir_g.png" title="Salir" width="140">
    </a>    
  </div> 
  </div>
  
  <!-- call calendar plugin -->
  <script type="text/javascript">
    
  </script>
  <script type="text/javascript">
    
</script>
</body>
</html>