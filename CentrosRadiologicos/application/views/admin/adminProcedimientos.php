<style type="text/css">
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
    }
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
td, th {
     padding: 10px; 
}
  </style>
<body>
<header>
  <?= $headerPage;?>
</header>

<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row"> 
        
      <div class="col-md-10">
        <h4 style="color: #0683c9;">CREAR PROCEDIMIENTOS</h4> 
        <hr>
        <form id="frmCrear" method="post" action="">    
          
          <label><font color="#0683c9">Código</font></label><br>
          <input type="text" name="txtCodigo" id="txtCodigo" class="form-control">
          <span style="color: red; font-size: 12px; display: none; font-weight: bolder;" id="spanErrorCodigo" class="errorDatoCrear"></span><br>

          <label for="selectGrupo"><font color="#0683c9">Grupo</font></label><br>
          <select id="selectGrupo" class="form-control">
            <option value="Seleccione">SELECCIONE...</option>            
            <?php
              if($grupos != null){
                foreach ($grupos->result() as $key) {
                  $grupo = $key->nombre;
                  $idgrupo = $key->id;
                
            ?>
              <option value="<?= $idgrupo;?>"><?= $grupo;?></option>
            <?php  
                }
              }
            ?>
          </select><br>

          <label for="selectTipo"><font color="#0683c9">Tipo</font></label><br>
          <select id="selectTipo" class="form-control"></select><br>

          <label for="selectPresentacion"><font color="#0683c9">Presentación</font></label><br>
          <select id="selectPresentacion" class="form-control"></select><br>

          <label><font color="#0683c9">Dias de Entrega</font></label><br>
          <input type="text" name="txtDias" id="txtDias" onKeyUp="format(this)" class="form-control"><br>

          <label><font color="#0683c9">Valor</font></label><br>
          <input type="text" name="txtValor" id="txtValor" onKeyUp="format(this)" class="form-control">
          <br>
          <button class="btn btn-primary pull-right" type="submit" id="btnEnviar">CREAR</button>    
        </form>    
      </div> 

      <!--<div class="col-md-6"> 
        <h4 style="color: #0683c9;">MODIFICAR PROCEDIMIENTOS</h4> 
        <hr>
        <form id="frmEditar" method="post" action="">    
          <label for="selectGrupoEditar"><font color="#0683c9">Grupo</font></label><br>
          <select id="selectGrupoEditar" class="form-control">
            <option value="Seleccione">SELECCIONE...</option>
            <?php
              if($grupos != null){
                foreach ($grupos->result() as $key) {
                  $grupo = $key->nombre;
                  $idgrupo = $key->id;
                
            ?>
              <option value="<?= $idgrupo;?>"><?= $grupo;?></option>
            <?php  
                }
              }
            ?>
          </select><br>

          <label><font color="#0683c9">Procedimiento</font></label><br>
          <select id="selectProcedimientoEditar" name="selectProcedimientoEditar" class="form-control">
            <option value="Seleccione">SELECCIONE</option>
          </select><br> 
            
          <label><font color="#0683c9">Código</font></label><br>
          <input type="text" name="txtCodigoEditar" id="txtCodigoEditar" class="form-control" disabled="disabled"><br> 

          <label><font color="#0683c9">Valor</font></label><br>
          <input type="text" name="txtValorEditar" id="txtValorEditar" onKeyUp="format(this)" class="form-control"> 
          <br>
          <button class="btn btn-primary btn-warning pull-right" type="submit" id="btnEditar" disabled="disabled">MODIFICAR</button>    
        </form>    
      </div> -->
    
  </div>
  <br>
  <?= $footerPage;?>  
  <br><br>
</div>
    <!-- javascript
    ================================================== -->
   
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript">
    
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrear").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmCrear").validate({
          rules: {
            txtCodigo:"required",
            txtValor:"required",
            txtDias:"required"
          },
          messages: {
            txtCodigo: "Por favor ingrese un código del procedimiento",
            txtValor: "Por favor ingrese un valor del procedimiento",
            txtDias: "Por favor ingrese el numero de dias de entrega"
          },
          submitHandler: function(form) {
            var codigo = $("#txtCodigo").val();
            var valor = $("#txtValor").val();
            var grupo = $("#selectGrupo").val();
            var tipo = $("#selectTipo").val();
            var presentacion = $("#selectPresentacion").val();
            var dias = $("#txtDias").val();

            var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
            // Los eliminamos todos
            for (var i = 0; i < specialChars.length; i++) {
              valor= valor.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
            }
            if($.trim($("#btnEnviar").html()) == "CREAR"){
              //Se guardan los datos en un JSON
              var datos = {
                codigo: codigo,
                valor: valor,
                grupo: grupo,
                tipo: tipo,
                presentacion: presentacion,
                dias: dias
              }   
              
              $.post("<?= base_url();?>index.php/ordenes_medicas/add_procedimiento_radiologia", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se registro con exito el procedimiento!")
                 $("#txtCodigo").val("")
                 $("#txtValor").val("")                 
                 window.location.href = '<?= base_url();?>index.php/ordenes_medicas/adminProcedimientos'
              });
            }else{
              //Se guardan los datos en un JSON
              var datos = {
                codigo: codigo,
                valor: valor,
                grupo: grupo,
                tipo: tipo,
                presentacion: presentacion,
                dias: dias
              }   
              
              $.post("<?= base_url();?>index.php/ordenes_medicas/update_procedimiento_radiologia", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se actualizo con exito el procedimiento!")
                 $("#txtCodigo").val("")
                 $("#txtValor").val("")
                 window.location.href = '<?= base_url();?>index.php/ordenes_medicas/adminProcedimientos'
              });
            }
          }
        });

        $("#frmEditar").tooltip({
          show: false,
          hide: false
        });
      
    })();

    //Validacion del campo codigo para que no se repita
    $("#txtCodigo").keyup(function(e){
      var codigo = $("#txtCodigo").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        codigo: codigo
      }   
      
      $.post("<?= base_url();?>index.php/ordenes_medicas/validateCodigoProcedimientoRadio", datos)
      .done(function( data ) {console.log(data)  
      if($.trim(data) != "{}"){
        var json = JSON.parse(data)
        $("#selectGrupo").val(json.id_grupo)
        listarTipos(json.id_tipo, json.id_presentacion)         
        $("#txtValor").val(json.valor)      
        $("#txtDias").val(json.diasEntrega)
        format(document.getElementById("txtValor"))
        $("#btnEnviar").html("MODIFICAR")
        $("#btnEnviar").addClass("btn-warning")
      }else{
        $("#btnEnviar").html("CREAR")
        $("#btnEnviar").removeClass("btn-warning")
      }   
         
         /*if($.trim(json.filas) != "0"){
          $("#spanErrorCodigo").css({"display":"block"})
          $("#spanErrorCodigo").html("¡El codigo de este procedimiento en el centro radiologico ya existe!")
         }else{
          $("#spanErrorCodigo").css({"display":"none"})
          $("#spanErrorCodigo").html("")
         }
         mostrarBotonCrear()*/
      });
      
    })


    //Validacion del campo nombre para que no se repita en la modificacion
    $("#txtNombreEditar").keyup(function(e){
      var nombre = $("#txtNombreEditar").val();
      var nombreHidden = $("#txtNombreEditarHidden").val();
      
      if(nombre != nombreHidden){
        //Se guardan los datos en un JSON
        var datos = {
          nombre: nombre
        }         
        $.post("<?= base_url();?>index.php/ordenes_medicas/validateNombreProcedimientoRadio", datos)
        .done(function( data ) {
          var json = JSON.parse(data)
          if($.trim(json.filas) != "0"){
            $("#spanErrorNombreEditar").css({"display":"block"})
            $("#spanErrorNombreEditar").html("¡El nombre de este procedimiento en el centro radiologico ya existe!")          
          }else{
            $("#spanErrorNombreEditar").css({"display":"none"})
            $("#spanErrorNombreEditar").html("")
          }
          mostrarBotonEditar()
        });
      }else{
        $("#spanErrorNombreEditar").css({"display":"none"})
        $("#spanErrorNombreEditar").html("")
      }
      mostrarBotonEditar()
      
    })
    
    //Mostrar Datos de un procedimiento a modificar    
    $("#selectGrupo").change(function(e){
      if($("#selectGrupo").val() != "Seleccione"){
        var codigo = $("#selectGrupo").val()
        var datos = {
          grupo: codigo
        }   
        
        $.post("<?= base_url();?>index.php/ordenes_medicas/findTipoProcedimientosRadiologia", datos)
        .done(function( data ) {console.log(data)
          $("#selectTipo").html("")
          $("#selectPresentacion").html("")
          $("#selectTipo").append("<option value='Seleccione'>SELECCIONE...</option>")
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            for (var i = 0; i < json.length; i++) {
              $("#selectTipo").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
            }
          }else{
            $("#selectTipo").html("")
          }
        });
      }else{
        $("#selectTipo").html("")
      }
    })

    //Mostrar Datos de un procedimiento a modificar    
    $("#selectTipo").change(function(e){
      if($("#selectTipo").val() != "Seleccione"){
        var codigo = $("#selectTipo").val()
        var datos = {
          idtipo: codigo
        }   
        
        $.post("<?= base_url();?>index.php/ordenes_medicas/findPresentacionRadiologia", datos)
        .done(function( data ) {console.log(data)
          $("#selectPresentacion").html("")
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            for (var i = 0; i < json.length; i++) {
              $("#selectPresentacion").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
            }
          }else{
            $("#selectPresentacion").append("<option value=''>Sin Presentación</option>")
          }
        });
      }else{
        $("#selectPresentacion").html("")
      }
    })


    //funcion que valida si hay errores para habilitar el boton crear
    function mostrarBotonCrear(){
      var contador = 0;
      $( ".errorDato" ).each(function( index ) {
        if($( this ).text() != ""){
          contador++;
        }
      });
      if(contador > 0 || $("#selectPresentacion > option").length == 0){
        $("#btnCrear").attr("disabled", "disabled")
      }else{
        $("#btnCrear").removeAttr("disabled")
      }
    }

    //funcion que valida si hay errores para habilitar el boton crear
    function mostrarBotonEditar(){
      var contador = 0;
      $( ".errorDatoEditar" ).each(function( index ) {
        if($( this ).text() != ""){
          contador++;
        }
      });
      if(contador > 0){
        $("#btnEditar").attr("disabled", "disabled")
      }else{
        $("#btnEditar").removeAttr("disabled")
      }
    }

   

    function listarTipos(tipo, presentacion){
      var codigo = $("#selectGrupo").val()
      var datos = {
        grupo: codigo
      }   
      
      $.post("<?= base_url();?>index.php/ordenes_medicas/findTipoProcedimientosRadiologia", datos)
      .done(function( data ) {console.log(data)
        $("#selectTipo").html("")
        $("#selectPresentacion").html("")
        $("#selectTipo").append("<option value='Seleccione'>SELECCIONE...</option>")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for (var i = 0; i < json.length; i++) {
            $("#selectTipo").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
          }
          $("#selectTipo").val(tipo)
          listarPresentacion(presentacion)
        }else{
          $("#selectTipo").html("")
        }
      });
    }

    function listarPresentacion(presentacion){
      var codigo = $("#selectTipo").val()
      var datos = {
        idtipo: codigo
      }   
      
      $.post("<?= base_url();?>index.php/ordenes_medicas/findPresentacionRadiologia", datos)
      .done(function( data ) {console.log(data)
        $("#selectPresentacion").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for (var i = 0; i < json.length; i++) {
            $("#selectPresentacion").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>")
          }
          $("#selectPresentacion").val(presentacion)
        }else{
          $("#selectPresentacion").append("<option value=''>Sin Presentación</option>")
        }
      });
    }
    
</script>
</body>
</html>