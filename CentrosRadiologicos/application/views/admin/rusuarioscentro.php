<style type="text/css">
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
    }
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
td, th {
     padding: 10px; 
}
  </style>
<body>
<header>
  <?= $headerPage;?>
</header>

<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;">Rips</h3>
  <br>
  <div class="row">

      <div class="col-md-1">
      </div>  
      <div class="col-md-10">

        <div class="col-md-2">
        </div> 
        <div class="col-md-4">
        <form  name="fvalida" method="post" action="">   
        <label><font color="#0683c9">Nombre Clinica</font></label>
        <input type="text" name="clinica" id="clinica" class="form-control" required>
        </div>
        <div class="col-md-4">
        <label><font color="#0683c9">Numero Factura</font></label>
        <input type="number" name="numerofactura" id="numerofactura" class="form-control" required>
        </div>
      </div>
      <div class="col-md-1">
      </div>
      <div class="col-md-12">
      </div>
      <div class="col-md-1">
      </div>  
      <div class="col-md-10">

        <div class="col-md-2">
        </div> 
        <div class="col-md-4">  
        <label><font color="#0683c9">Fecha Inicial</font></label>
        <input type="date" name="f1" id="f1" class="form-control" required>
        </div>
        <div class="col-md-4">
        <label><font color="#0683c9">Fecha Final</font></label>
        <input type="date" name="f2" id="f2" class="form-control" required>
        </div>
        <div class="col-md-2">
        </br>
        <button type="button" id="busquedabutton" class="btn btn-primary">Buscar</button>
        </div> 
      </div>
      </form>
      <div class="col-md-12">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
      </div>

  </div>
  <br>
  <?= $footerPage;?>  
  <br><br>
</div>
    <!-- javascript
    ================================================== -->
   
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript">

  $("#busquedabutton").click( function(){
            //

            if (document.fvalida.clinica.value.length==0){ 
                alert("Por favor Ingrese la clinica") 
                document.fvalida.clinica.focus() 
                return false; 
            }
            if (document.fvalida.numerofactura.value.length==0){ 
                alert("Por favor Ingrese el numero de factura") 
                document.fvalida.numerofactura.focus() 
                return false; 
            }
            if (document.fvalida.f1.value.length==0){ 
                alert("Por favor Ingrese la fecha inicial") 
                document.fvalida.f1.focus() 
                return false; 
            }
            if (document.fvalida.f2.value.length==0){ 
                alert("Por favor Ingrese la fecha final") 
                document.fvalida.f2.focus() 
                return false; 
            }

            ripusuarios();
            ripaf();
            ripad();
            ripap();
            ripct();
  });

  function ripusuarios(){
     //paso los ids del form a variables
            var clinica = $("#clinica").val();
            var numerofactura = $("#numerofactura").val();
            var fechainicial = $("#f1").val();
            var fechafinal = $("#f2").val();
             //fecha
             var d = new Date(); 

            
              //Se guardan los datos en un JSON
              var datos = {
                clinica: clinica,
                numerofactura: numerofactura,
                f1: fechainicial,
                f2: fechafinal
              }   
              
              $.post("<?= base_url();?>index.php/clinica/generarRips1", datos)
              .done(function(data) {
                  console.log(data);
                  var element = document.createElement('a');
                  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                  element.setAttribute('download', "US"+d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear(), ', '+d.getHours(),':'+d.getMinutes(),':'+d.getSeconds());
                  element.style.display = 'none';
                  document.body.appendChild(element);
                  element.click();
                  document.body.removeChild(element);
              });
  }


  function ripaf(){
              //paso los ids del form a variables
              var clinica = $("#clinica").val();
              var numerofactura = $("#numerofactura").val();
              var fechainicial = $("#f1").val();
              var fechafinal = $("#f2").val();
              //fecha
               var d = new Date();
                //Se guardan los datos en un JSON
                var datos = {
                  clinica: clinica,
                  numerofactura: numerofactura,
                  f1: fechainicial,
                  f2: fechafinal
                }

              $.post("<?= base_url();?>index.php/clinica/generarRips2", datos)
              .done(function(data) {
                  console.log(data);
                  var element = document.createElement('a');
                  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                  element.setAttribute('download', "AF"+d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear(), ', '+d.getHours(),':'+d.getMinutes(),':'+d.getSeconds());
                  element.style.display = 'none';
                  document.body.appendChild(element);
                  element.click();
                  document.body.removeChild(element);
              });
  }

  function ripad(){
              //paso los ids del form a variables
              var clinica = $("#clinica").val();
              var numerofactura = $("#numerofactura").val();
              var fechainicial = $("#f1").val();
              var fechafinal = $("#f2").val();
              //fecha
               var d = new Date();
                //Se guardan los datos en un JSON
                var datos = {
                  clinica: clinica,
                  numerofactura: numerofactura,
                  f1: fechainicial,
                  f2: fechafinal
                }

              $.post("<?= base_url();?>index.php/clinica/generarRips3", datos)
              .done(function(data) {
                  console.log(data);
                  var element = document.createElement('a');
                  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                  element.setAttribute('download', "AD"+d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear(), ', '+d.getHours(),':'+d.getMinutes(),':'+d.getSeconds());
                  element.style.display = 'none';
                  document.body.appendChild(element);
                  element.click();
                  document.body.removeChild(element);
              });
  }

  function ripap(){
              //paso los ids del form a variables
              var clinica = $("#clinica").val();
              var numerofactura = $("#numerofactura").val();
              var fechainicial = $("#f1").val();
              var fechafinal = $("#f2").val();
              //fecha
               var d = new Date();
                //Se guardan los datos en un JSON
                var datos = {
                  clinica: clinica,
                  numerofactura: numerofactura,
                  f1: fechainicial,
                  f2: fechafinal
                }

              $.post("<?= base_url();?>index.php/clinica/generarRips4", datos)
              .done(function(data) {
                  console.log(data);
                  var element = document.createElement('a');
                  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                  element.setAttribute('download', "AP"+d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear(), ', '+d.getHours(),':'+d.getMinutes(),':'+d.getSeconds());
                  element.style.display = 'none';
                  document.body.appendChild(element);
                  element.click();
                  document.body.removeChild(element);
              });
  }

  function ripct(){
              //paso los ids del form a variables
              var clinica = $("#clinica").val();
              var numerofactura = $("#numerofactura").val();
              var fechainicial = $("#f1").val();
              var fechafinal = $("#f2").val();
              //fecha
               var d = new Date();
                //Se guardan los datos en un JSON
                var datos = {
                  clinica: clinica,
                  numerofactura: numerofactura,
                  f1: fechainicial,
                  f2: fechafinal
                }

              $.post("<?= base_url();?>index.php/clinica/generarRips5", datos)
              .done(function(data) {
                  console.log(data);
                  var element = document.createElement('a');
                  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data));
                  element.setAttribute('download', "CT"+d.getDate() + "/" + (d.getMonth() +1) + "/" + d.getFullYear(), ', '+d.getHours(),':'+d.getMinutes(),':'+d.getSeconds());
                  element.style.display = 'none';
                  document.body.appendChild(element);
                  element.click();
                  document.body.removeChild(element);
              });
  }

          
  </script>
</body>
</html>