<style type="text/css">
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 5px; 
     border-right: 1px solid;
     font-weight: bold;
}
  </style>
<body>

<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row"> 
    <form id="frmFind" method="post" action="">
      <div class="col-md-6">   
        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">ID.PACIENTE</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-9" align="right" style=" padding: 0">   
            <input type="text" name="txtIdentificacion" id="txtIdentificacion" class="form-control">  
          </div>
        </div>

        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">PACIENTE</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-9" align="right" style=" padding: 0">   
            <input type="text" name="txtNombre" id="txtNombre" class="form-control" disabled="disabled">  
          </div>
        </div>

        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">PROCESO</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-9" align="right" style=" padding: 0">   
            <select id="selectProcesos" name="selectProcesos" class="form-control">
              <option value="">SELECCIONE...</option>
           

            <?php
              if($procedimientos != null){
                foreach ($procedimientos->result() as $key) {
                  $name = $key->nombre;
                  $grupo = $key->nombreGrupo;
                  $id_tipo = $key->id_tipo;
                  $id = $key->idProcedimiento;

                  if($id_tipo == "1" || $id_tipo == "2" || $id_tipo == "18" || $id_tipo == "19"){
            ?>
              <option value="<?= $id;?>"><?= $name;?></option>
              <?php 
                }else{
                  ?>
                  <option value="<?= $id;?>"><?= $grupo . " " . $name;?></option>
            <?php  
                }
              }
            }
            ?>
            </select>  
          </div>
        </div>

        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">FACTURA</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-9" align="right" style=" padding: 0">   
            <input type="text" name="txtFactura" id="txtFactura" class="form-control">  
          </div>
        </div>
      </div>

      <div class="col-md-6">   
        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">FUNCIONARIO</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-9" style=" padding: 0">   
            <select id="selectFuncionarios" name="selectFuncionarios" class="form-control">
            <?php
              if($funcionarios != null){
                foreach ($funcionarios->result() as $key) {
                  $name = $key->first_name . " " . $key->last_name;
                  $id = $key->email;
                
            ?>
              <option value="<?= $id;?>"><?= $name;?></option>
            <?php  
                }
              }
            ?>
            </select>
          </div>
        </div>
        <br>
        <div class="row"> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">FECHA INICIAL</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-3" style="padding: 0">   
            <input type="date" name="txtFechaInicial" id="txtFechaInicial" class="form-control">  
          </div> 
          <div class="col-md-3" align="right" style=" padding: 0">   
            <label style="margin-top: 7px"><font color="#0683c9">FECHA FINAL</font></label>&nbsp;&nbsp;
          </div>
          <div class="col-md-3" style="padding: 0">   
            <input type="date" name="txtFechaFinal" id="txtFechaFinal" class="form-control">  
          </div>
        </div> 

        <br>
        <div class="col-md-6" align="right">   
          <button class="btn btn-primary" id="btnBuscar" type="submit" style="width: 90%"><b>BUSCAR</b></button>   
        </div>
      </div>
    </form>
  </div>
  <hr size="2px" style="border-color: #0683c9" />

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
    <thead>
      <tr style="background-color: #0683c9; color: white">
        <td>FECHA</td>
        <td>HORA</td>
        <td>FACTURA</td>
        <td>ID.PACIENTE</td>
        <td>PACIENTE</td>
        <td>PROCESO</td>
        <td>FUNCIONARIO</td>
        <td>VALOR</td>
        <td>ESTADO</td>
      </tr>
    </thead>
    <tbody id="tbodyDatosFactura" style="font-weight: normal;">
                                 
    </tbody>
  </table>

  

 
  <br><br>
  <div class="row">    
    <div class="col-md-8" align="right">
      <label style="margin-top: 6px"><font color="#0683c9">VALOR TOTAL</font></label>    
    </div>
    <div class="col-md-2" align="center">
      <input type="tex" id="txtValorTotal" name="txtValorTotal" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">    
    </div>
    <div class="col-md-2" align="right">
      <button class="btn btn-success" disabled="disabled" id="btnExportar">EXPORTAR INFORME</button>    
    </div>
  </div>

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 0px">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="col-md-4">
            <img width="100%" src="../../images/LOGO SW CENTRAL.png">
          </div>
          <div class="col-md-6">
            <h4 align="center" id="h4Mensaje"></h4>
          </div>

        </div>
        <div class="modal-body" style="border: 0px">
          
        </div>
        <div class="modal-footer" style="border: 0px">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAceptarAnulacion">ACEPTAR</button>
        </div>
      </div>

    </div>
  </div>
  <br>
  <div id="footer">
    <div align="center">
      <a href="<?= base_url();?>index.php/user_radiologia/home">
        <img src="<?= base_url();?>images/Inicio_g.png" title="Salir" width="60px" style="">
      </a>
      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteCierreCaja"><img id="REPORTE CIERRE CAJA" src="<?= base_url();?>images/reporteCajaNegro.png" title="Ayuda" width="100px"></a>
      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteProcesosDesarrollados"><img id="REPORTE PROCESO DESARROLLADO" src="<?= base_url();?>images/reporteGeneralAzulRec.png" title="Ayuda"  width="100px"></a>

      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteTiempoAtencion"><img id="REPORTE TIEMPO ATENCION" src="<?= base_url();?>images/tiemposo.png" title="Tiempo Atencion"  width="100px"></a>

      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

    
    </div> 
  </div>
  </div>
     <br><br>
  

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript">
    $.validator.setDefaults({
        //Capturar evento del boton crear
        submitHandler: function() {
          var identificacion = $("#txtIdentificacion").val();
          var factura = $("#txtFactura").val();
          var proceso = $("#selectProcesos  ").val();
          var fechaInicial = $("#txtFechaInicial").val();
          var fechaFinal = $("#txtFechaFinal").val();
          var user_radiologia = $("#selectFuncionarios").val();
          $("#tbodyDatosFactura").html("")         
          
          //Se guardan los datos en un JSON
          var datos = {
            document: identificacion,
            fechaInicial: fechaInicial,
            fechaFinal: fechaFinal,
            proceso: proceso,
            factura: factura,
            user_radiologia: user_radiologia
          }   
          $.post("<?= base_url();?>index.php/user_radiologia/findFacturasDesarrollo", datos)
          .done(function( data ) {console.log(data)             
            if($.trim(data) != "[]"){
              var json = JSON.parse(data)
              var total = 0;
              
              for (var i = 0; i < json.length; i++) {            
                var estado = "";      
                if($.trim(json[i].estadoFactura) == "ACTIVO"){
                  estado = "APROBADA";      
                }else{
                  estado = json[i].estadoFactura; 
                }             
                $("#tbodyDatosFactura").append("<tr><td>" + json[i].fecha + "</td><td>" + json[i].hora + "</td><td align='center'>" + json[i].id_factura_radiologia + "</td><td>" + json[i].document__number + "</td><td>" + json[i].first_name + " " + json[i].last_name + "</td><td>" + json[i].descripcion + "</td><td>" + json[i].nombreFuncionario + " " + json[i].apellidosFuncionario + "</td><td align='right'>" + json[i].total + "</td><td>" + estado + "</td></tr>")  
                if($.trim(json[i].estadoFactura) == "ACTIVO"){
                  total += parseInt(json[i].total)
                }
              }     
              if($("#tbodyDatosFactura > tr").length > 0){
                $("#btnExportar").removeAttr("disabled")
              }else{
                $("#btnExportar").attr("disabled", "disabled")
              }
              $("#txtValorTotal").val(total)     
            }else{
              //$("#txtNombre").val("")   
              $("#btnAnular").attr("disabled", "disabled")  
              $("#btnAceptarAnulacion").css({"display":"none"})
              $("#h4Mensaje").html("¡No hay facturas disponibles!");
              $('#myModal').modal();
            }
          });         
          
        }
    });
  
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmFind").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmFind").validate({
          rules: {
            txtFechaInicial:"required",
            txtFechaFinal:"required"
          },
          messages: {
            txtFechaInicial: "Por favor ingrese una fecha incial",
            txtFechaFinal: "Por favor ingrese una fecha final"
          }
        });
        
    })();

    $("#btnExportar").click(function(e) {
      var identificacion = $("#txtIdentificacion").val();
      var factura = $("#txtFactura").val();
      var proceso = $("#selectProcesos  ").val();
      var fechaInicial = $("#txtFechaInicial").val();
      var fechaFinal = $("#txtFechaFinal").val();
      var user_radiologia = $("#selectFuncionarios").val();
      var win = window.open("<?= base_url();?>index.php/user_radiologia/exportarReporteProcesosDesarrollados?document=" + identificacion + "&fechaInicial=" + fechaInicial + "&fechaFinal=" + fechaFinal + "&factura=" + factura + "&proceso=" + proceso + "&user_radiologia=" + user_radiologia, '_blank');
      win.focus();  
        
    });

    $("#txtIdentificacion").keyup(function(e){      
      var identificacion = $("#txtIdentificacion").val(); 
      //Se guardan los datos en un JSON 
      var datos = {
        NumeroDoc: identificacion
      }   
      $.post("<?= base_url();?>index.php/user_radiologia/getPacienteDocument", datos)
      .done(function( data ) {console.log(data)  
        if($.trim(data) != "{}"){
          var json = JSON.parse(data)
          $("#txtNombre").val(json.first_name + " " + json.last_name)
        }else{
          $("#txtNombre").val("")
        }
      });   
    })
</script>
</body>
</html>