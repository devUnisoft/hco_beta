<?php
  $profileCR = $this->session->userdata('UserIDInternoCR');
  if(empty($profileCR)) { // Recuerda usar corchetes.
      header('Location: ' . base_url());
  }
?>
<style type="text/css">
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
      font-weight: bolder;
    }
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}


a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
td, th {
     padding: 10px; 
}
  </style>
<body>

<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row"> 
        
    
      <form id="frmAdmin" method="post" action="">    
        <div class="col-md-12">
          <div class="col-md-2">
            <label for="selectClinica" style="margin-top: 7px"><font>NOMBRE DE CLINICA</font></label>
          </div>
          <div class="col-md-6">
            <select class="form-control" id="selectClinica" tabindex="1">
              <option value="Seleccione">Seleccione...</option>
              <?php
              if($clinicas != null){
                foreach ($clinicas->result() as $key) {
                  $name = $key->clinica;
                
              ?>
                <option value="<?= $name;?>"><?= $name;?></option>
              <?php  
                  }
                }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-12">
        <br>
          <div class="col-md-2">
            <label for="selectCodigo" style="margin-top: 7px"><font>CODIGO ENTIDAD RIPS</font></label>
          </div>
          <div class="col-md-6">
            <input class="form-control" id="selectCodigo" tabindex="1" />
          </div>
        </div>
        <div class="col-md-12">
        <br>
          <div class="col-md-2">
            <label for="verificacion" style="margin-top: 7px"><font>CODIGO VERIFICACION RIPS</font></label>
          </div>
          <div class="col-md-6">
            <input class="form-control" id="verificacion" tabindex="1" />
          </div>
        </div><br><br><br><br><br><br>
        
        <div class="col-md-12">
          <div class="col-md-3">
            <label for="selectNombrePro"><font>NOMBRE DE PROFESIONAL</font></label><br>
            <select class="form-control" id="selectNombrePro" tabindex="1">
              
            </select>
          </div>
          <div class="col-md-9">
            <div class="col-md-12">
              <div class="col-md-5">
                <label for="txtDescuento" style="margin-top: 7px"><font>Convenio Descuento en procedimientos (%)</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtDescuento" id="txtDescuento" class="form-control" onKeyUp="format(this)" tabindex="3">
              </div>
            </div><br><br>

            <div class="col-md-12">
              <div class="col-md-5">
                <label for="" style="margin-top: 15px"><font>Convenio Entrega de Resultados</font></label>
              </div>
              <div class="col-md-6">
                <div class="checkbox">
                  <label><input type="checkbox" name="checkboxConveEntre" value="PLATAFORMA HCO">PLATAFORMA HCO</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="checkboxConveEntre" value="EMAIL">EMAIL</label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" name="checkboxConveEntre" value="ENTREGA FISICO">ENTREGA FISICO</label>
                </div>
              </div>
            </div><br><br>
            
            <div class="col-md-12">
              <div class="col-md-5">
                <label for="txtDescuento" style="margin-top: 15px"><font>Convenio para Facturación</font></label>
              </div>
              <div class="col-md-6">
                <div class="radio">
                  <label><input type="radio" name="radioConvFac" value="FACTURAR A LA CLINICA">FACTURAR A LA CLINICA</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="radioConvFac" value="FACTURAR AL PROFESIONAL">FACTURAR AL PROFESIONAL</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="radioConvFac" value="FACTURAR AL PACIENTE">FACTURAR AL PACIENTE</label>
                </div>
              </div>
            </div>

          </div>
        </div>
        
       

        <div class="col-md-11">
          <br>
          <button class="btn btn-primary pull-right" type="submit" id="btnEnviar" tabindex="13" disabled="disabled">CREAR</button>    
        </div>
      </form>    
   

      
    
  </div>
  <br>
  <?= $footerPage;?>  
  <br><br>
  </div>
    <!-- javascript
    ================================================== -->
   
    
    <!-- call calendar plugin -->
  
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript">
    
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmAdmin").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmAdmin").validate({
          rules: {
            txtDescuento:"required",
            checkboxConveEntre:"required",
            radioConvFac:"required",
            selectCodigo:"required",
            verificacion:"required"
          },
          messages: {
            txtDescuento: "Por favor ingrese un porcentaje de descuento",
            checkboxConveEntre: "Por favor seleccione un convenio de entrega de resultados",
            radioConvFac: "Por favor seleccione un convenio de facturación",
            selectCodigo: "Por favor ingrese el codigo entidad rips",
            verificacion: "Por favor ingrese el codigo de verificacion"
          },
          submitHandler: function(form) {
            var descuento = $("#txtDescuento").val();
            var clinica = $("#selectClinica").val();
            var codigo = $("#selectCodigo").val();
            var verificacion = $("#verificacion").val();
            var profesional = $("#selectNombrePro").val();
            var entrega = "";  
            var facturacion = $('input[name=radioConvFac]:checked').val();    
            var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,";
            // Los eliminamos todos
            for (var i = 0; i < specialChars.length; i++) {
              descuento= descuento.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
            }    

            $('input[name=checkboxConveEntre]:checked').each(function(index) {
              if(index == 0){
                entrega += $(this).val();
              }else{
                entrega += "," + $(this).val();
              }

              
            });  
            //Se guardan los datos en un JSON
            var datos = {
              clinica: clinica,
              codigo: codigo,
              verificacion: verificacion,
              profesional: profesional,
              descuento: descuento,
              entrega: entrega,
              facturacion: facturacion
            }         
            
            if($.trim($("#btnEnviar").html()) == "CREAR"){               
              
              //Enviar datos para modificarse
              $.post("<?= base_url();?>index.php/ordenes_medicas/addConvenio", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se registro con exito el convenio!")
                 $("input").val("")
                 
                 window.location.href = "<?= base_url();?>index.php/ordenes_medicas/adminConvenios";
              });
            }else{
              
              //Enviar datos para crearse 
              $.post("<?= base_url();?>index.php/ordenes_medicas/updateConvenio", datos)
              .done(function( data ) {console.log(data)             
                 alert("¡Se modifico con exito el convenio!")
                 $("input").val("")
                 
                 window.location.href = "<?= base_url();?>index.php/ordenes_medicas/adminConvenios";
              });
            }
            
          }
        });     
        
    })();

    $("#selectClinica").change(function(e){
      //Se guardan los datos en un JSON
      if($("#selectClinica").val() != "Seleccione"){
        var datos = {
          nombre: $("#selectClinica").val()
        } 
        $.post("<?= base_url();?>index.php/ordenes_medicas/obtenerProfesionalesClinica", datos)
        .done(function( data ) {console.log(data)             
          $("#selectNombrePro").html("")
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            $("#selectNombrePro").append('<option value="Todos">Todos</option>')

            for(i = 0; i < json.datos.length; i++){
              $("#selectNombrePro").append('<option value="' + json.datos[i].id + '">' + json.datos[i].first_name + ' ' + json.datos[i].last_name + '</option>')
            }
            
            if(json.datos[0].convenios.length > 0){
              if(json.datos.length == parseInt(json.totalConveniosIguales)){
                $("#btnEnviar").html("MODIFICAR")
                $("#btnEnviar").addClass("btn-warning")

                $("#txtDescuento").val(json.datos[0].convenios[0].descuento_proce)
                var arrayEntrega = json.datos[0].convenios[0].convenio_entrega.split(",");

                $('input[name=checkboxConveEntre]').prop('checked', false);
                $('input[name=radioConvFac]').prop('checked', false);

                for(i = 0; i < arrayEntrega.length; i++){
                  $("input[name=checkboxConveEntre][value='" + arrayEntrega[i] + "']").prop('checked', true);
                }

                $("input[name=radioConvFac][value='" + json.datos[0].convenios[0].convenio_factura + "']").prop('checked', true);
              }else{
                $("#btnEnviar").html("MODIFICAR")
                $("#btnEnviar").addClass("btn-warning")
                alert("Los Profesionales tienen distintos valores de convenio. Si modifica el convenio para todos se actualizaran los valores especificos de cada profesional")
                $("#txtDescuento").val("")
                $('input[name=checkboxConveEntre]').prop('checked', false);
                $('input[name=radioConvFac]').prop('checked', false);
              }
              
              

            }else{
              $("#btnEnviar").html("CREAR")
              $("#btnEnviar").removeClass("btn-warning")
              $("#txtDescuento").val("")
              $('input[name=checkboxConveEntre]').prop('checked', false);
              $('input[name=radioConvFac]').prop('checked', false);
            }
            $("#btnEnviar").removeAttr("disabled")
            //console.log(json[0].convenios)       
          }
        })
      }else{
        $("#selectNombrePro").html("")
        $('input[name=checkboxConveEntre]').prop('checked', false);
              $('input[name=radioConvFac]').prop('checked', false);
        $("#txtDescuento").val("")
        $("#btnEnviar").html("CREAR")
        $("#btnEnviar").removeClass("btn-warning")
        $("#btnEnviar").attr("disabled","disabled")
      }
      
    });  

    $("#selectNombrePro").change(function(e){
      if($.trim($("#btnEnviar").html()) == "MODIFICAR"){

        var datos = {
          profesional: $("#selectNombrePro").val(),
          clinica: $("#selectClinica").val()
        } 
        $.post("<?= base_url();?>index.php/ordenes_medicas/obtenerConvenioProfesional", datos)
        .done(function( data ) {console.log(data)
          $('input[name=checkboxConveEntre]').prop('checked', false);
          $('input[name=radioConvFac]').prop('checked', false);
          $("#txtDescuento").val("")

          if($.trim(data) != "{}"){
            var json = JSON.parse(data)

            if(json.resultado == "DIFERENTES"){
              alert("Los Profesionales tienen distintos valores de convenio. Si modifica el convenio para todos se actualizaran los valores especificos de cada profesional")
              $("#txtDescuento").val("")
              $('input[name=checkboxConveEntre]').prop('checked', false);
              $('input[name=radioConvFac]').prop('checked', false);
            }else{
              $("#txtDescuento").val(json.descuento_proce)
              var arrayEntrega = json.convenio_entrega.split(",");

              for(i = 0; i < arrayEntrega.length; i++){
                $("input[name=checkboxConveEntre][value='" + arrayEntrega[i] + "']").prop('checked', true);
              }
              $("input[name=radioConvFac][value='" + json.convenio_factura + "']").prop('checked', true);        
            }

               
          }
        });
        
      }else{
        if($("#selectNombrePro").val() != "Todos"){
          alert("Debe registrar primero el convenio con todos los profesionales de una clinica")
          $('#selectNombrePro option:first-child').attr('selected', 'selected');
        }
      }
    })
    
</script>
</body>
</html>