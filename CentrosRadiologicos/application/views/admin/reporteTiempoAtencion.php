
<style type="text/css">
#commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
  color:red;
  font-family: "Oswald";
  right: 0;
  font-weight: bolder;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 5px; 
     border-right: 1px solid;
     font-weight: bold;
}

#colores{
    background-color: #0683c9;
    width: 5%;
    text-align: center;
    color: #FFFFFF;
}
  </style>
   <script type="text/javascript">
    $(document).ready(function(){                   
        var consulta;

        //comprobamos si se pulsa una tecla
        $("#btnBuscar").click(function(e){

              $('#buttonsend').removeAttr('disabled');
                                     
              //obtenemos el texto introducido en el campo de búsqueda
              consulta = $("#txtFechaInicial").val();
              consulta2 = $("#txtFechaFinal").val();
              consulta3 = $("#clinica").val();

              if($.trim(consulta) != "" && $.trim(consulta2) != ""){
                $.ajax({
                  type: "POST",
                  url: "<?= base_url();?>index.php/ordenes_medicas/getReporteTiempoAtencion",
                  data: { "uno" : consulta,"dos" : consulta2,"tres" : consulta3},
                  dataType: "html",
                  beforeSend: function(){
                        //imagen de carga
                        $("#resultado").html("<p align='center'><img src='<?= base_url();?>images/page-loader.gif'  /></p>");
                  },
                  error: function(){
                    //alert("Error en la busqueda " + e.responseText);
                    alert("Error en la busqueda no hay datos ");
                  },
                  success: function(data){
                    console.log(data)                                                     
                    $("#resultado").empty();
                    if($.trim(data) != "[]"){
                      var json = JSON.parse(data)
                      for (var i = 0; i < json.length; i++) {
                        $("#resultado").append('<tr>' + 
                          '<td>' + json[i].documento + '</td>' + 
                          '<td>' + json[i].first_name + ' ' + json[i].last_name + '</td>' + 
                          '<td>' + json[i].descripcion + '</td>' + 
                          '<td>' + json[i].nombre_profesional + ' ' + json[i].apellido_profesional + '</td>' + 
                          '<td>' + json[i].fechafactura + ' ' + json[i].horafactura + '</td>' +
                          '<td>' + json[i].tiempo_factura_atencion + '</td>' +  
                          '<td>' + json[i].fecha_atencion + ' ' + json[i].hora_atencion + '</td>' + 
                          '<td>' + json[i].tiempo_consulta + '</td>' + 
                          '<td>' + json[i].fecha_fin_consulta + ' ' + json[i].hor_fin_consulta + '</td>' + 
                          '<td>' + json[i].tiempo_atencion_entrega + '</td>' + 
                          '<td>' + json[i].fecha_entrega + ' ' + json[i].hora_entrega + '</td>' + 
                        '</tr>');
                      }
                    }
                          
                  }
                });
              }else{
                if($.trim(consulta) == "" && $.trim(consulta2) == ""){
                  alert("¡Debe ingresar fecha de inicio y fecha final para realizar la busqueda!")
                }else{
                  if($.trim(consulta) == ""){
                    alert("¡Debe ingresar fecha de inicio para realizar la busqueda!")
                  }else{
                    alert("¡Debe ingresar fecha final para realizar la busqueda!")
                  }
                }
              }

                                                                                     
                                                                           
        });
});
</script>
<script language="javascript">
              $(document).ready(function() {
                  $("#buttonsend").click(function(event) {
                      $("#datos_a_enviar").val( $("<div>").append( $("#tabla_resultado").eq(0).clone()).html());
                      $("#FormularioExportacion").submit();
              });
              });
</script> 
<body>
<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row">

  
      <div class="col-md-12">   
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA INICIAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaInicial" id="txtFechaInicial" style="text-align: center;" class="form-control">  
        </div>  
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA FINAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaFinal" id="txtFechaFinal" style="text-align: center;" class="form-control">
          <input type="hidden" name="clinica" id="clinica" style="text-align: center;" class="form-control" value="<?php echo  $this->session->userdata('IDCRInternoCR') ?>">  
        </div>
        <div class="col-md-2" style="padding: 0" align="right">   
          <button class="btn btn-primary" id="btnBuscar" type="submit" style="width: 90%"><b>BUSCAR</b></button>   
        </div>
      </div>
    
  <br><br><br>
  <div class="col-md-12" id="tabla_resultado">
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th id='colores'><font>Id Paciente</font></th>
          <th id='colores'><font>Paciente</font></th>
          <th id='colores'><font>Procedimiento</font></th>
          <th id='colores'><font>Profesional</font></th>
          <th id='colores'><font>Fecha Facturación</font></th>
          <th id='colores'><font>Tiempo Espera</font></th>
          <th id='colores'><font>Fecha Consulta</font></th>
          <th id='colores'><font>Tiempo Consulta</font></th>
          <th id='colores'><font>Finalización Consulta</font></th>
          <th id='colores'><font>Tiempo Espera Resultados</font></th>
          <th id='colores'><font>Fecha Entrega</font></th>
        </tr>
      </thead>
      <tbody id="resultado">
      </tbody>
    </table>
  </div> 
    
  </div>
  <div class="col-md-10">
  </div>
  <div class="col-md-2">
  <form action="ReporteTiemposAtencion" method="post"  id="FormularioExportacion">
  <button type="submit" id="buttonsend"  class="btn btn-success" disabled>Exportar a Excel</button>
  <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
  </form>           
  </div>

  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <div id="footer">
    <div align="center">
      <a href="<?= base_url();?>index.php/user_radiologia/home">
        <img src="<?= base_url();?>images/Inicio_g.png" title="Salir" width="60px" style="">
      </a>
      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteCierreCaja"><img id="REPORTE CIERRE CAJA" src="<?= base_url();?>images/reporteCajaNegro.png" title="Ayuda" width="100px"></a>
      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteProcesosDesarrollados"><img id="REPORTE PROCESO DESARROLLADO" src="<?= base_url();?>images/reporteGeneralNegroRec.png" title="Ayuda"  width="100px"></a>

      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

      <a href="<?= base_url();?>index.php/ordenes_medicas/reporteTiempoAtencion"><img id="REPORTE TIEMPO ATENCION" src="<?= base_url();?>images/tiemposa.png" title="Tiempo Atencion"  width="100px"></a>

      <img src="<?= base_url();?>images/separador.png"  height="50px" width="30px">

    
    </div> 
  </div>
     <br><br>
  </div>

  <div id="myModalFormaPago" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">
          
          <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

        </div>
        <div class="modal-body" style="border: 0px; ">

          <div class="col-md-12">
            <h3 style="color: #428bca; font-weight: bolder" align="center">DATOS DE FORMA DE PAGO</h3>

            <div class="col-md-12" id="divResultado">                           
              
            </div>

            <div class="col-md-12" style="">
              <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarInformacion" style="width: 100%">CANCELAR</button>
            </div>        
          </div>

        </div>
        <div class="modal-footer" style="border: 0px">
          
          
        </div>

      </div>

    </div>
  </div>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
  <script src="<?= base_url();?>js/jquery.validate.js"></script>

</body>
</html>