<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
	<style>
		.adhesivo_4x12 {
			width: 760px;
			margin: auto;
			display: flex;
			flex-wrap: wrap;
			justify-content: space-around;
		}
		.tr-container {
			display: inline-flex;
			width: 225px;
			padding: 10px;
			border: 1px dashed #eee;
			margin: 1px;
			height: 85px;
			flex-direction: column;
		}
		.data-clinica {
			text-align: center;
			font-size: 10px;
		}
		.separador {
			width: 100%;
			margin-bottom: 3px;
			margin-top: 1px;
		}
		.data-paciente {
			font-size: 12px;
			width: 100%;
			display: block;
			border-bottom: 1px solid #eee;
		}
	</style>


	<div class="adhesivo_4x12">
		
		<?php for ($i = 1; $i <= 30; $i++) { ?>
		
			<div class="tr-container">
				<div class="data-clinica">
					<?php echo $centro_radiologico[0]->name  ?>
				</div>
				<div class="data-clinica">
					<?php echo $centro_radiologico[0]->address  ?>
				</div>
				<div class="data-clinica">
					Tel(s). <?php echo $centro_radiologico[0]->telefonos  ?>					
				</div>
				<hr class="separador">
				<div>
					<span class="data-paciente"><i>Paciente:</i></span>
				</div>
				<div>
					<span class="data-paciente"><i>Teléfono:</i></span>
				</div>
				<div>
					<span class="data-paciente"><i>Num. Documento:</i></span>
				</div>
			</div>

		<?php } ?>


	</div>
		

	<script>
		(function() {
			
			'use strict';

			print();

			var beforePrint = function() {
				// console.log('Functionality to run before printing.');
			};
			var afterPrint = function() {
				window.history.back();
			};

			if (window.matchMedia) {
				var mediaQueryList = window.matchMedia('print');
				mediaQueryList.addListener(function(mql) {
					if (mql.matches) {
						beforePrint();
					} else {
						afterPrint();
					}
				});
			}

			window.onbeforeprint = beforePrint;
			window.onafterprint = afterPrint;


		})();
	</script>

</body>
</html>
