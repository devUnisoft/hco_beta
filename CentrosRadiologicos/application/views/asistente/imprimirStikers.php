<div ng-app="printStiker">
	
	<sticker-printer></sticker-printer>

  <script type="text/ng-template" id="stickerprinter.html">
    <style>
      .sticker-description {
        font-size: 10px;
      }
      .sticker-select {
        background: #0683c9;
        color: white;
      }
    </style>

    <!-- Modal open slect stiker-->
    <button 
      ng-click="initModal()" 
      type="button" 
      class="btn btn-default" 
      data-toggle="modal" 
      data-target="#selectStockerModal"
    > Imprimir Stiker </button>
  
    <!-- Modal slect stiker-->
    <div class="modal fade" id="selectStockerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">
              <span ng-if="nextStep == true">
                Seleccionar formato
              </span>
              <span ng-if="nextStep == false">
                Seleccionar área de impresión
              </span>
            </h4>
          </div>
          <div class="modal-body text-center">
            
            <!-- step 1 -->
            <div ng-show="nextStep == true" class="row">
              <div class="col-xs-4">
                <img ng-click="setSheetSelect(33)" src="<?php echo base_url() ?>img/stiker-pequeno.jpg" alt="img 1">
                <div class="text-center sticker-description">
                  Impresión Pequeña 33 Stickers 
                </div>
              </div>
              <div class="col-xs-4">
                <img ng-click="setSheetSelect(12)" src="<?php echo base_url() ?>img/stiker-mediano.jpg" alt="img 2">
                <div class="text-center sticker-description">
                  Impresión Mediana 12 Stickers
                </div>
              </div>
              <div class="col-xs-4">
                <img ng-click="setSheetSelect(8)" src="<?php echo base_url() ?>img/stiker-gande.jpg" alt="img 3">
                <div class="text-center sticker-description">
                  Impresión Grande 8 Stickers
                </div>
              </div>
            </div>

            <!-- step 2 -->
            <div ng-show="nextStep == false">
              <div class="row">
                <div 
                  ng-repeat="(index, item) in repeat"
                  class="col-xs-{{ (sheetSelect == 33) ? '4' : '6' }}" 
                  ng-click="setPositions(item)"
                  ng-class="{'sticker-select' : showData2Print(item)}"
                  style="height: 5vh; border: 1px solid #eee; text-align: center; padding-top: 1vh;">
                    <span ng-if="!showData2Print(item)" class="glyphicon glyphicon-remove">
                    </span>
                    <span ng-if="showData2Print(item)" class="glyphicon glyphicon-print">
                    </span>
                  </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            
            <form id="fmrDownloadStickers" 
              style="display: none;"
              name="num_documento"
              method="POST"
              target="_blank" 
              action="<?php echo base_url().'index.php/Adhesivos/Adhesivos_pdf'?>">
              <input type="text" name="num_documento" value="{{num_documento}}">
              <input type="text" name="sheetSelect" value="{{sheetSelect}}">
              <input type="text" name="positions" value="{{positions}}">
            </form>

            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button ng-click="setNextStep()" type="button" class="btn btn-primary">
              {{ (nextStep) ? 'Siguiente' : 'Anterior'}}
            </button>
            <button ng-click="sendData()" ng-if="nextStep === false" ng-click="" type="button" class="btn btn-success">
              Imprimir
            </button>
          </div>
        </div>
      </div>
    </div>
    
  </script>

</div>

<!-- ================== -->
<!--     STICKERS       -->
<!-- ================== -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.js"></script>

<script>
  (function() {

    'use strict';

    angular
    .module('printStiker', [])
    .directive('stickerPrinter', stickerPrinter);

    stickerPrinter.$inject = [];

    function stickerPrinter() {
      var directive = {};

      directive.restrict = 'EA';
      directive.templateUrl = 'stickerprinter.html';
      directive.scope = {
        width: '@',
        heigth: '@'
      };

      directive.controller = function ($scope, $http, $httpParamSerializerJQLike, $timeout) {


        $scope.initModal = function () {
          $scope.nextStep = true;
          $scope.sheetSelect = 0;
          $scope.width = 0;
          $scope.heigth = 0;
          $scope.data = {};
          $scope.repeat = [];
          $scope.positions = [];
          $scope.num_documento = undefined;
        }
        $scope.initModal();


        $scope.showData2Print = function (item) {
          return ( $scope.positions.indexOf(item) != -1 );
        }


        $scope.setPositions = function (item) {
          var _index = $scope.positions.indexOf(item);

          if (_index != -1) {
            $scope.positions.splice(_index, 1);
          }else {
            $scope.positions.push(item);
          }
        }


        $scope.setNextStep = function() {
          $scope.nextStep = !$scope.nextStep;
        }


        $scope.setSheetSelect = function(cant) {
          $scope.repeat = [];
          $scope.positions = [];
          $scope.nextStep = false;
          $scope.sheetSelect = cant;

          for (var i = 0; i < cant; i++) {
            $scope.repeat.push(i);
          }

          if($scope.sheetSelect === 33) {
            $scope.width = 71.9;
            $scope.heigth = 25.4;
          }else if($scope.sheetSelect === 12) {
            $scope.width = 107.9;
            $scope.heigth = 46.5;
          }else if($scope.sheetSelect === 8) {
            $scope.width = 107.9;
            $scope.heigth = 69.8;
          }

        }

        $scope.sendData = function() {
          $scope.num_documento = angular.element( document.getElementById('txtPaciente') ).val();

          if($scope.num_documento) {
            $timeout(function() {
              document.getElementById('fmrDownloadStickers').submit();
            }, 500);
          }else {
            window.alert('El número de documento no es válido o esta vacio. \n\n Por favor intente de nuevo.')
          }
        
        };

      };

      return directive;
    }

  })();  
</script>