<meta charset="utf-8">				
<style>
	.labelDato{
		color: #0683C9; 
		font-size: 8px; 
		font-weight: bolder;
		padding: 0px;
		margin-top: 10px;
    }
    .animation-duration: {
		color: #9B9FA1;  
		font-size: 8px;
		padding: 0px;
    }
 	.DatoTotal{
		color: #9B9FA1;  
		font-size: 10px;
		border: 1px solid #0683C9;
    }
    .Dato{
		color: #9B9FA1;  
		font-size: 10px;
    }
    #tabla > thead > tr > td{
		color: #0683C9; 
		font-size: 10px;
		font-weight: bolder;
    }
    #tabla td{
		font-weight: bolder;
		color: #0683C9; 
		font-size: 10px;
		font-weight: bolder;	
    }
    *{
		font-family: "Arial";
		font-size: 10px;
		margin: 0px;
		padding: 0px;
    }
</style>

<table width="100%" style="font-size: 9px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="40%" valign="top">			
			<img src="<?= $centro->result()[0]->logo;?>" width="180px" style="margin: 0px; padding: 0px">
			<label class="labelDato">Resolución DIAN <?= $centro->result()[0]->resolucion;?><br>
				Fecha de Resolución <?= $centro->result()[0]->fecha_resolucion;?><br>Rango Fact.Autorz. <?= $centro->result()[0]->rango_resolucion;?><br>Fecha <?= date("Y-m-d");?> 
			</label>
			
		</td>
		<td width="60%" valign="top">			
			<label style="color: #0683C9; font-size: 20px; text-align: right;font-weight: bolder;"><i>Factura No. <?= $id_factura;?></i></label><br><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="38%" align="right"><label class="labelDato">No. IDENTIFICACION:</label></td>
					<td width="2%">&nbsp;</td>
					<td width="60%" class="Dato">						
			          	<?= $data_user->document__number;?>	
			        </td>
				</tr>
				<tr>
					<td width="38%" align="right"><label class="labelDato">NOMBRE:</label></td>
					<td>&nbsp;</td>
					<td width="60%" class="Dato">
						<?= $data_user->first_name . " " . $data_user->last_name;?>
			        </td>
				</tr>
				<tr>
					<td width="38%" align="right"><label class="labelDato">DIRECCION:</label></td>
					<td>&nbsp;</td>
					<td width="60%" class="Dato">
						<?= $data_user->lugar_residencia;?>
			        </td>
				</tr>
				<tr>
					<td width="38%" align="right"><label class="labelDato">TELEFONO:</label></td>
					<td>&nbsp;</td>
					<td width="60%" class="Dato">
						<?= $data_user->telefono . " - " . $data_user->celular;?>
			        </td>
				</tr>
				<tr>
					<td width="38%" align="right"><label class="labelDato">EMAIL:</label></td>
					<td>&nbsp;</td>
					<td width="60%" class="Dato">					
			           <?= $data_user->email;?>			       
			        </td>
				</tr>
				<tr>
					<td width="38%" align="right"><label class="labelDato">FORMA DE PAGO:</label></td>
					<td>&nbsp;</td>
					<td width="60%" class="Dato">
			           <?= $tipo_pago;?>		
			        </td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table width="100%">
	<tr>
		<td height="5px">&nbsp;</td>
	</tr>
</table>

<div style="width: 100%; height: 1px; border-top: 1px solid #0683C9;">&nbsp;</div>

<table width="100%" id="tabla" style="font-size: 9px">
    <thead>
      <tr>
        <td style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="25%">DESCRIPCION</td>
        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">CANTIDAD</td>
        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">CONVENIO</td>
        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">DESCUENTO (%)</td>
        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">VR.UNITARIO</td>
        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9; font-weight: bolder" width="15%">SUBTOTAL</td>
      </tr>
      <tr>
        <td colspan="6" style="padding: 0px; border: none;"></td>
      </tr>
    </thead>

    <tbody>
    	<?php
    		if($detalle_factura != null){
				foreach ($detalle_factura as $value) {
    	?>
    	<tr>
	        <td style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="25%"><?= $value->descripcion;?></td>
	        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%"><?= number_format($value->cantidad,0, ',', '.');?></td>
	        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%"><?= $value->convenio;?></td>
	        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%"><?= $value->descuento;?></td>
	        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%"><?= number_format($value->precio,0, ',', '.');?></td>
	        <td align="center" style="padding: 0px; color: #0683C9; border-right: 1px solid #0683C9;" width="15%"><?= number_format($value->total,0, ',', '.');?></td>
      	</tr>
      	<tr>
	        <td colspan="6" style="padding: 0px; border: none"></td>
		</tr>
		<?php
				}
			}
		?>
    </tbody>    

</table>

<table width="100%">
	<tr>
		<td height="80px">&nbsp;</td>
	</tr>
</table>

<table width="100%">
	<tr>
		<td width="50%">
			<div align="center" style="border: 1px solid #9B9FA1; color: #9B9FA1; padding: 10px; width: 100%">
				<div style="width: 100%; height: 40px">&nbsp;</div>
				<table width="100%">
					<tr>
						<td width="8%">&nbsp;</td>
						<td width="84%"><label style="margin-left: 10px; margin-top: 10px">Son: <?= $total_letras;?></label></td>
						<td width="8%">&nbsp;</td>
					</tr>
				</table>
	      	<div style="width: 100%; height: 40px">&nbsp;</div>	
	    </div>
		</td>
		<td width="50%" valign="middle">
			<div style="width: 100%; height: 70px">&nbsp;</div>	
			<table width="100%">
			<tr>
				<td width="28%" align="right"><label class="labelDato">VALOR TOTAL</label></td>
				<td width="2%">&nbsp;</td>
				<td width="70%">
					<div class="col-md-8 DatoTotal" align="center">
			          <label class="labelDato">$<?= $total;?>	</label>
			        </div>
		        </td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<table width="100%">
	<tr>
		<td height="100px">&nbsp;</td>
	</tr>
</table>
<table width="100%">
	<tr>
		<td width="46%" style="color: #9B9FA1; border-top: 1px solid #0683C9; padding-top: 5px; font-weight: bolder">			
      	Firma de Funcionario	   
    </td>
    <td width="8%">&nbsp;</td>
    <td width="46%" style="color: #9B9FA1; border-top: 1px solid #0683C9; padding-top: 5px; font-weight: bolder;">    	
      	Firma de Cliente	    
    </td>
	</tr>
</table>


<br />