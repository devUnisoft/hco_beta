
<style type="text/css">
#commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
  color:red;
  font-family: ArialRounded;
  right: 0;
  font-weight: bolder;
}

header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
   
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    bottom:0px;
    clear:both;
    z-index: 999;
  }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



  
.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin:0;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.pull-left {
    /* float: left !important; */
    width: 100%;
}


 .form-control {
    border-style:solid;
    border-color: #3ca3c1;
    border-width: 1px;
}

@media (max-width: 767px)
{
.container-fluid {
    padding: 0;
    padding-left: 115px;
}



#footer{
    left: 0px;
}

body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

}

@media (max-width: 1000px)
{
.container-fluid {
    padding: 0;
    padding-left: 116px;
}
#footer{
    left: 0px;
}



a{
  width: 100%;
  float: none;
  margin-bottom: 20px;
}


body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 10px; 
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
}

#imagenheadersuperior{
  height:50px; 
  width:100px;
}

#icitas{
height:30px;
width:30px;
}

#iroles{
height:30px;
width:30px;
}

#irips{
height:30px;
width:30px;
}

#isalir{
height:30px;
width:30px;
}

#ieditarperfil{
height:30px;
width:30px;  
}

.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 0px;
}

#logodescripcion{
  height:50%; 
  width:50%;
}

#logoeditar{
  height:50%; 
  width:50%;
}


}

.btn btn-warning pull-right{

  margin-bottom: 20px;
  width: 80%;

}

@media (min-width: 992px){
.col-md-5 {
    width: 41.66666667%;
}
}

@media (min-width: 992px)
{
.col-md-12 {
    width: 100%;
}
}

@media (min-width: 992px)
{
.col-md-5 {
    width: 41.66666667%;
}
}
.tabla td, .tabla th {
     padding: 5px; 
     border-right: 1px solid;
     font-weight: bold;
}

  </style>
<body>
<header>
  <?= $headerPage;?>
</header>
<div class="container-fluid">
  <br>
  <h3 align="center" style="color: #0683c9;"><?= $titulo;?></h3>
  <br>
  <div class="row"> 
    <form id="frmFind" method="post" action="">
      <div class="col-md-12">   
        <div class="col-md-1" style="padding: 0">   
          <label style="margin-top: 7px"><font color="#0683c9">FUNCIONARIO</font></label>
        </div>
        <div class="col-md-2" style="padding: 0">   
          <select id="selectFuncionarios" name="selectFuncionarios" class="form-control" disabled> 
            <?php
              if($funcionarios != null){
                foreach ($funcionarios->result() as $key) {
                  $name = $key->first_name . " " . $key->last_name;
                  $id = $key->email;
                
            ?>
              <option value="<?= $id;?>" disabled><?= $name;?></option>
            <?php  
                }
              }
            ?>
          </select>
        </div> 
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA INICIAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaInicial" id="txtFechaInicial" style="text-align: center;" class="form-control">  
        </div>  
        <div class="col-md-2" style="padding: 0" align="right">   
          <label style="margin-top: 7px"><font color="#0683c9">FECHA FINAL</font></label>&nbsp;&nbsp;
        </div>
        <div class="col-md-2" style="padding: 0">   
          <input type="date" name="txtFechaFinal" id="txtFechaFinal" style="text-align: center;" class="form-control">  
        </div>
        <div class="col-md-1" style="padding: 0" align="right">   
          <button class="btn btn-primary" id="btnBuscar" type="submit" style="width: 90%"><b>BUSCAR</b></button>   
        </div>
      </div>
    </form>
  </div>
  <hr size="2px" style="border-color: #0683c9" />

  <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
    <thead>
      <tr style="background-color: #0683c9; color: white">
        <td>FECHA</td>
        <td>HORA</td>
        <td align='center'>FACTURA</td>
        <td>NOMBRE</td>
        <td>DESCRIPCION</td>
        <td align='center'>CANTIDAD</td>
        <td align='right'>VALOR</td>
        <td>FORMA PAGO</td>
        <td>ESTADO</td>
      </tr>
    </thead>
    <tbody id="tbodyDatosFactura" style="font-weight: normal;">
                                 
    </tbody>
  </table>

  

 
  <br><br>
  <div class="row">    
    <div class="col-md-2"">
      <label><font color="#0683c9">VALOR TOTAL EFECTIVO</font></label><br>
      <input type="tex" id="txtValorTotalEfectivo" name="txtValorTotalEfectivo" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">    
    </div>
   
    <div class="col-md-2">
      <label><font color="#0683c9">VALOR TOTAL CHEQUE</font></label><br>
      <input type="tex" id="txtValorTotalCheque" name="txtValorTotalCheque" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">     
    </div>

    <div class="col-md-3">
      <label><font color="#0683c9">VALOR TOTAL TARJETA DE CREDITO</font></label><br>
      <input type="tex" id="txtValorTotalTarjetaCredito" name="txtValorTotalTarjetaCredito" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">        
    </div>

    <div class="col-md-2" style="padding: 0px">
      <label><font color="#0683c9">VALOR TOTAL OTRA FORMA</font></label><br>      
      <input type="tex" id="txtValorTotalOtro" name="txtValorTotalOtro" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">          
    </div>

    <div class="col-md-2">
      <label><font color="#0683c9">VALOR TOTAL</font></label><br>
      <input type="tex" id="txtValorTotal" name="txtValorTotal" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">        
    </div>

  </div><br>
  <div class="row"> 
    <div class="col-md-11" align="right">
        <button class="btn btn-success" disabled="disabled" id="btnExportar">EXPORTAR INFORME</button>  
      </div>   
  </div>

  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 0px">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="col-md-4">
            <img width="100%" src="<?= base_url();?>images/LOGO SW CENTRAL.png">
          </div>
          <div class="col-md-6">
            <h4 align="center" id="h4Mensaje"></h4>
          </div>

        </div>
        <div class="modal-body" style="border: 0px">
          
        </div>
        <div class="modal-footer" style="border: 0px">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnAceptarAnulacion">ACEPTAR</button>
        </div>
      </div>

    </div>
  </div>

  <div id="myModalFormaPago" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
        <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">
          
          <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

        </div>
        <div class="modal-body" style="border: 0px; ">

          <div class="col-md-12">
            <h3 style="color: #428bca; font-weight: bolder" align="center">DATOS DE FORMA DE PAGO</h3>

            <div class="col-md-12" id="divResultado">                           
              
            </div>

            <div class="col-md-12" style="">
              <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelarInformacion" style="width: 100%">CANCELAR</button>
            </div>        
          </div>

        </div>
        <div class="modal-footer" style="border: 0px">
          
          
        </div>

      </div>

    </div>
  </div>

  <br>
  <?= $footerPage;?>  
 <br><br>
  </div>

    <!-- javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
  <script src="<?= base_url();?>js/jquery.validate.js"></script>
  <script type="text/javascript">

    $("#selectFuncionarios").val("<?= $this->session->userdata('UserIDInternoCR');?>");
    $("#selectFuncionarios").attr("disabled", "disabled");
    $.validator.setDefaults({
        //Capturar evento del boton crear
        submitHandler: function() {
          var identificacion = "<?= $this->session->userdata('UserIDInternoCR');?>";
          var fechaInicial = $("#txtFechaInicial").val();
          var fechaFinal = $("#txtFechaFinal").val();
          $("#tbodyDatosFactura").html("")
          console.log(identificacion)   
          $("#btnAnular").attr("disabled", "disabled")  
          //Se guardan los datos en un JSON
          var datos = {
            document: identificacion,
            fechaInicial: fechaInicial,
            fechaFinal: fechaFinal
          }   
          $.post("<?= base_url();?>index.php/ordenes_medicas/findreportecierrecaja", datos)
          .done(function( data ) {console.log(data)             
            if($.trim(data) != "[]"){
              var json = JSON.parse(data)
              $("#txtNombre").val(json[0].first_name + " " + json[0].last_name)
              var totalEfectivo = 0;
              var totalCheque = 0;
              var totalTarjeta = 0;
              var totalOtra = 0;
              var total = 0;

              for (var i = 0; i < json.length; i++) {
                total += parseInt(json[i].total)    
                var estado = "";      
                if($.trim(json[i].estadoFactura) == "ACTIVO"){
                  estado = "APROBADA";      
                }else{
                  estado = json[i].estadoFactura; 
                }       
                $("#tbodyDatosFactura").append("<tr><td>" + json[i].fecha + "</td><td>" + json[i].hora + "</td><td align='center'>" + json[i].id_factura_radiologia + "</td><td>" + json[i].first_name + " " + json[i].last_name + "</td><td>" + json[i].descripcion + "</td><td align='center'>" + json[i].cantidad + "</td><td align='right'>" + json[i].total + "</td><td id='tdFactura" + i + "'  idFactura='" + json[i].id_factura_radiologia + "' data-toggle='modal' data-target='#myModalFormaPago' style='cursor: pointer; color: #428bca; font-weight: bolder; text-decoration: underline'>" + json[i].tipopago + "</td><td>" + estado + "</td></tr>") 

                $("#tdFactura" + i).click(function(e){
                  var idFactura = $(this).attr("idFactura")
                  var datos = {
                    idFactura: idFactura
                  }   
                  $.post("<?= base_url();?>index.php/ordenes_medicas/getDatosPagoFactura", datos)
                  .done(function( resultado ) {console.log(resultado) 
                    $("#divResultado").html("")
                    if($.trim(resultado) != "[]"){
                      var jsonDatos = JSON.parse(resultado)

                      for (var j = 0; j < jsonDatos.length; j++) {
                        
                        switch(jsonDatos[j].tipopago){
                          case "Efectivo":
                            $("#divResultado").append('<label><b>Forma de Pago: </b>' + jsonDatos[j].tipopago + '</label><br>')
                            break;

                          case "Cheque":
                            $("#divResultado").append('<label><b>Forma de Pago: </b>' + jsonDatos[j].tipopago + '</label><br><label><b>Numero de Cheque: </b>' + jsonDatos[j].numerocheque + '</label><br><label><b>Codigo de Banco: </b>' + jsonDatos[j].codigobanco + '</label><br><label><b>Numero de Cuenta: </b>' + jsonDatos[j].numerocuenta + '</label><br><label><b>Fecha: </b>' + jsonDatos[j].fechacheque + '</label><br>')
                            break;
                            
                          case "Tarjeta de Credito":
                            $("#divResultado").append('<label><b>Forma de Pago: </b>' + jsonDatos[j].tipopago + '</label><br><label><b>Código: </b>' + jsonDatos[j].codigoautorizaciont + '</label><br><label><b>Tipo: </b>' + jsonDatos[j].tipotarjeta + '</label><br><label><b>Banco: </b>' + jsonDatos[j].bancotarjeta + '</label><br><label><b>Numero: </b>' + jsonDatos[j].numerotarjeta + '</label><br>')
                            break;
                            
                          case "Otra forma de pago":
                            $("#divResultado").append('<label><b>Forma de Pago: </b>' + jsonDatos[j].tipopago + '</label><br><label><b>Documento: </b>' + jsonDatos[j].numdocumento + '</label><br><label><b>Forma de Pago: </b>' + jsonDatos[j].formapago + '</label><br><label><b>Código: </b>' + jsonDatos[j].codigoautorizaciono + '</label><br>')
                            break;
                        }
                        
                      }
                    }
                  })
                }) 

                switch(json[i].tipopago){
                  case "Efectivo":
                    totalEfectivo += parseInt(json[i].total)
                    break;

                  case "Cheque":
                    totalCheque += parseInt(json[i].total)
                    break;
                    
                  case "Tarjeta de Credito":
                    totalTarjeta += parseInt(json[i].total)
                    break;
                    
                  case "Otra forma de pago":
                    totalOtra += parseInt(json[i].total)
                    break;
                }
                
              }     
              if($("#tbodyDatosFactura > tr").length > 0){
                $("#btnExportar").removeAttr("disabled")
              }else{
                $("#btnExportar").attr("disabled", "disabled")
              }
              $("#txtValorTotalEfectivo").val(formatNumber.new(totalEfectivo)) 
              $("#txtValorTotalCheque").val(formatNumber.new(totalCheque)) 
              $("#txtValorTotalTarjetaCredito").val(formatNumber.new(totalTarjeta)) 
              $("#txtValorTotalOtro").val(formatNumber.new(totalOtra))    
              $("#txtValorTotal").val(formatNumber.new(total))  
            }else{
              $("#txtValorTotalEfectivo").val("0") 
              $("#txtValorTotalCheque").val("0") 
              $("#txtValorTotalTarjetaCredito").val("0") 
              $("#txtValorTotalOtro").val("0") 
              $("#txtValorTotal").val("0") 
              $("#txtNombre").val("")   
              $("#btnAnular").attr("disabled", "disabled")  
              $("#btnAceptarAnulacion").css({"display":"none"})
              $("#h4Mensaje").html("¡No hay facturas disponibles!");
              $('#myModal').modal();
            }
          });         
          
        }
    });
    $("#btnExportar").click(function(e) {
      var identificacion = "<?= $this->session->userdata('UserIDInternoCR');?>";
      var fechaInicial = $("#txtFechaInicial").val();
      var fechaFinal = $("#txtFechaFinal").val();
      var win = window.open("<?= base_url();?>index.php/ordenes_medicas/exportarReporteCierreCaja?document=" + identificacion + "&fechaInicial=" + fechaInicial + "&fechaFinal=" + fechaFinal, '_blank');
      win.focus();  
        
    });
    (function() {
        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmFind").tooltip({
          show: false,
          hide: false
        });
      
        // validate signup form on keyup and submit
        $("#frmFind").validate({
          rules: {
            txtIdentificacion:"required",
            txtFechaInicial:"required",
            txtFechaFinal:"required"
          },
          messages: {
            txtIdentificacion: "Por favor ingrese un numero de documento",
            txtFechaInicial: "Por favor ingrese una fecha incial",
            txtFechaFinal: "Por favor ingrese una fecha final"
          }
        });
        
    })();

</script>
</body>
</html>