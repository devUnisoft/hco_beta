<?php
$profileCR = $this->session->userdata('UserIDInternoCR');
  if(empty($profileCR)) { // Recuerda usar corchetes.
    header('Location: ' . base_url());
  }
  ?>
  <script type="text/javascript" src="<?= base_url();?>js/ValidacionNumerica.js"></script>
  <style type="text/css">
    #commentForm label.error,  label.error,  span.errorDatoCrear,  span.errorDatoEditar{
      color:red;
      font-family: ArialRounded;
      right: 0;
      font-weight: bolder;
    }
    header h1#logo {
      display: inline-block;
      height: 150px;
      line-height: 150px;
      float: left;
      font-family: "Oswald", sans-serif;
      font-size: 60px;
      color: white;
      font-weight: 400;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    header nav {
      display: inline-block;
      float: right;
    }
    header nav a {
      line-height: 150px;
      margin-left: 20px;
      color: #9fdbfc;
      font-weight: 700;
      font-size: 18px;
      -webkit-transition: all 0.3s;
      -moz-transition: all 0.3s;
      -ms-transition: all 0.3s;
      -o-transition: all 0.3s;
      transition: all 0.3s;
    }
    header nav a:hover {
      color: white;
    }
    header.smaller {
      height: 75px;
    }
    header.smaller h1#logo {
      width: 150px;
      height: 75px;
      line-height: 75px;
      font-size: 30px;
    }
    header.smaller nav a {
      line-height: 75px;
    }

    @media all and (max-width: 660px) {
      header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
      }
      header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
      }
      header nav a {
        line-height: 50px;
        margin: 0 10px;
      }
      header.smaller {
        height: 75px;
      }
      header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
      }
      header.smaller nav {
        height: 35px;
      }
      header.smaller nav a {
        line-height: 35px;
      }
    }


    .media
    {
      /*box-shadow:0px 0px 4px -2px #000;*/
      margin: 20px 0;
      padding:30px;
    }
    .dp
    {
      border:10px solid #eee;
      transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
      border:2px solid #eee;

    }




    .profile 
    {
      min-height: 300px;
      display: inline-block;
    }
    figcaption.ratings
    {
      margin-top:20px;
    }
    figcaption.ratings a
    {
      color:#f1c40f;
      font-size:11px;
    }
    figcaption.ratings a:hover
    {
      color:#f39c12;
      text-decoration:none;
    }
    .divider 
    {
      border-top:1px solid rgba(0,0,0,0.1);
    }
    .emphasis 
    {
      border-top: 4px solid transparent;
    }
    .emphasis:hover 
    {
      border-top: 4px solid #1abc9c;
    }
    .emphasis h2
    {
      margin-bottom:0;
    }
    span.tags 
    {
      background: #1abc9c;
      border-radius: 2px;
      color: #f5f5f5;
      font-weight: bold;
      padding: 2px 4px;
    }

    .row {
      margin:0;
    }

    .col-fixed {
      /* custom width */
      width:320px;
    }
    .col-min {
      /* custom min width */
      min-width:320px;
    }
    .col-max {
      /* custom max width */
      max-width:320px;
    }

    #menuinicial{
      position:fixed;
      width:100%;
      height:90px;
      background-color:#FFFFFF;
      color:#000000;
      clear:both;
      z-index: 100;
    }

    .pull-left {
      /* float: left !important; */
      width: 100%;
    }


    .form-control {
      border-style:solid;
      border-color: #3ca3c1;
      border-width: 1px;
    }

    @media (max-width: 767px)
    {
      .container-fluid {
        padding: 0;
        padding-left: 115px;
      }



      #footer{
      }

      body {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 10px; 
        line-height: 1.42857143;
        color: #333;
        background-color: #fff;
      }

      #imagenheadersuperior{
        height:50px; 
        width:100px;
      }

    }

    @media (max-width: 1000px)
    {
      .container-fluid {
        padding: 0;
        padding-left: 116px;
      }
      #footer{
      }



      a{
        width: 100%;
        float: none;
        margin-bottom: 20px;
      }


      body {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 10px; 
        line-height: 1.42857143;
        color: #333;
        background-color: #fff;
      }

      #imagenheadersuperior{
        height:50px; 
        width:100px;
      }

      #icitas{
        height:30px;
        width:30px;
      }

      #iroles{
        height:30px;
        width:30px;
      }

      #irips{
        height:30px;
        width:30px;
      }

      #isalir{
        height:30px;
        width:30px;
      }

      #ieditarperfil{
        height:30px;
        width:30px;  
      }

      .modal-footer .btn + .btn {
        margin-bottom: 0;
        margin-left: 0px;
      }

      #logodescripcion{
        height:50%; 
        width:50%;
      }

      #logoeditar{
        height:50%; 
        width:50%;
      }


    }

    .btn btn-warning pull-right{

      margin-bottom: 20px;
      width: 80%;

    }

    @media (min-width: 992px){
      .col-md-5 {
        width: 41.66666667%;
      }
    }

    @media (min-width: 992px)
    {
      .col-md-12 {
        width: 100%;
      }
    }

    @media (min-width: 992px)
    {
      .col-md-5 {
        width: 41.66666667%;
      }
    }
    .tabla td, .tabla th {
     padding: 10px; 
   }
   .nav{
    position: relative;
    left: 30%;
    margin-right: 24%;
    top: 5px; 
    font-size: 12px;
  }
  .nav > li > a {
    position: relative;
    display: block;
    padding: 4px 10px;
    background-color: #68AEEC;
    color: #FFFFFF;
  }

  .nav > li > a:hover {
    position: relative;
    display: block;
    padding: 4px 10px;
    background-color: #68AEEC;
    color: #FFFFFF;
    font-weight: bolder;
  }

  .tab-pane{
    margin-top: 2%;
    margin-left: -2%;
    margin-right: -2%;
    padding: 5px 7px 5px 7px;
    margin-bottom: 10px;
  }
</style>
<script type="text/javascript">
  function nobackbutton(){
   window.location.hash="no-back-button";
   window.location.hash="Again-No-back-button" 
   window.onhashchange=function(){window.location.hash="no-back-button";} 
 }
</script>
<body onload="nobackbutton();">

  <header>
    <?= $headerPage;?>
  </header>

  <div class="container-fluid">
    <br>
    <h3 align="center" style="color: #0683c9;">RECEPCIÓN DE ATENCION PARA PACIENTES</h3>
    <br>
    <div class="row" style="font-size: 12px; margin:0px"> 
      <form id="frmFind" method="post" action="">
        <div class="col-md-4" style="margin:0px; padding: 0px">   

          <table width="100%" cellpadding="0" cellspacing="0">
            <tr style="padding: 0px">
              <td valign="bottom" width="58px">                
                <a href="#" data-toggle="modal" data-target="#myModalAddPerson">
                  <img src="<?= base_url();?>images/paciente.png" title="Agregar Paciente" height="40px" width="40px">
                  <img src="<?= base_url();?>images/globo_plus.png" title="Agregar Paciente" height="15px" width="15px" style="margin-left: -3px">
                </a>
              </td>
              <td>
                <label><font color="#0683c9">No. IDENTIFICACIÓN</font></label><br>                     
                <select multiple class="chosen-select form-control" tabindex="-1" name="txtIdentificacion" id="txtIdentificacion">
                  <?php
                    if($pacientes != null){
                      foreach ($pacientes as $value) {
                      ?>
                        <option value="<?= $value->document__number;?>"><?= $value->first_name . " " . $value->last_name;?> - <?= $value->document__number;?></option>
                      <?php
                      }
                    }
                  ?>
                </select>
              </td>
            </tr>    
          </table>
        </div> 
        <div class="col-md-2">   
          <label><font color="#0683c9">NOMBRE</font></label><br>
          <input type="text" name="txtNombre" id="txtNombre" style="text-align: center;" class="form-control" disabled>      
        </div> 
        <div class="col-md-2">   
          <label><font color="#0683c9">DIRECCION</font></label><br>
          <input type="text" name="txtDireccion" id="txtDireccion" style="text-align: center;" class="form-control" disabled>      
        </div> 
        <div class="col-md-2">   
          <label><font color="#0683c9">TÉLEFONO</font></label><br>
          <input type="text" name="txtTelefono" id="txtTelefono" style="text-align: center;" class="form-control" disabled>      
        </div> 
        <div class="col-md-2">   
          <label><font color="#0683c9">EMAIL</font></label><br>
          <input type="text" name="txtEmail" id="txtEmail" style="text-align: center;" class="form-control" disabled>      
        </div>       
      </form>
    </div>
    <br>

    <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
      Procedimientos Almacenados
    </div>
    <br><br>

    <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1" style="padding-left: 5px">
      <thead>
        <tr style="color: #0683c9; border-color: #0683c9; font-weight: bolder;">
          <td>FECHA SOLICITUD</td>
          <td>CLÍNICA</td>
          <td>PROFESIONAL</td>
          <td>DESCRIPCIÓN</td>
          <td>TIPO</td>
          <td>CARACTERISTICA</td>
          <td>OBSERVACIONES</td>
          <td>&nbsp;</td>
        </tr>
      </thead>
      <tbody id="tbodyOrdenMedica">

      </tbody>
    </table>

    <br>

    <button class="btn btn-primary btn-warning" data-toggle="modal" data-target="#myModalAddProcedimientos" disabled="disabled" id="btnAddProcedimientos">Incluir Procedimientos</button><br><br>

    <div class="col-md-12" style="margin:0; color: #FFF; background-color: #0683c9; font-size: 16px; padding: 5px" align="center">
      Datos para Facturación
    </div>
    <br><br>

    <table width="100%" class="tabla" cellpadding="5" cellspacing="5" border="1">
      <thead>
        <tr style="color: #0683c9; border-color: #0683c9; font-weight: bolder;" align="center">
          <td>DESCRIPCION</td>
          <td>CANTIDAD</td>
          <td>VR.UNITARIO</td>
          <td>CONVENIO</td>
          <td>DESCUENTO (%)</td>
          <td>SUBTOTAL</td>
          <td>OBSERVACIONES DE RECEPCION</td>
        </tr>
      </thead>
      <tbody id="tbodyResumen">

      </tbody>
    </table>
    <input type="hidden" name="txtClinicaHidden" id="txtClinicaHidden">
    <input type="hidden" name="txtProfesionalHidden" id="txtProfesionalHidden">

    <br><br>
    <div class="row">
      <div id="spin"></div>    
      <div class="col-md-8" align="right">
        <label style="margin-top: 6px"><font color="#0683c9">VALOR TOTAL DE PAGO</font></label>    
      </div>
      <div class="col-md-2" align="center">
        <input type="tex" id="txtValorTotal" name="txtValorTotal" style="background-color: #0683c9; color: #FFF; text-align: center" class="form-control" disabled value="0">    
      </div>
      <div class="col-md-2" align="right">
        <button class="btn btn-primary" disabled="disabled" id="btnGenerarFactura" data-toggle="modal" data-target="#myModalFormaPago">RECIBIR PAGO</button>    
      </div>
    </div>
    <br>
    <?= $footerPage;?>  
    <br><br>
  </div>

  <!-- Modal -->
  <div id="myModalAddPerson" class="modal fade" role="dialog">
    <div class="modal-dialog" style="height: 600px; width: 60%;">

      <!-- Modal content-->
      <div class="modal-content" style="height: 600px">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">NUEVO PACIENTE</h4>
        </div>
        <div class="modal-body" style="padding: 5px;height: 540px;" align="left">
          <form id="frmCrear" method="post" action="">    
            <div class="col-md-12">   
              <label><font color="#0683c9">Tipo de Documento</font></label><br>
              <select class="form-control" id="selectTipoDocumento" tabindex="1">
                <option>Cédula de ciudadanía</option>
                <option>Registro civil</option>
                <option>Tarjeta de identidad</option>
                <option>Cédula de extranjería</option>
                <option>NIT</option>
                <option>Adulto sin identificación</option>
                <option>Menor sin identificación</option>
                <option>Pasaporte</option>
              </select><br>    

              <label><font color="#0683c9">Número de Documento: </font></label><br>
              <input type="text" name="txtDocumentoPaciente" id="txtDocumentoPaciente" class="form-control" tabindex="2"><br>  

              <label><font color="#0683c9">Nombres: </font></label><br>
              <input type="text" name="txtNombresPaciente" id="txtNombresPaciente" class="form-control" disabled="disabled" tabindex="3"><br>  

              <label><font color="#0683c9">Apellidos: </font></label><br>
              <input type="text" name="txtApellidosPaciente" id="txtApellidosPaciente" class="form-control" disabled="disabled" tabindex="4"><br>

              <label><font color="#0683c9">E-mail: </font></label><br>
              <input type="email" name="txtEmailPaciente" id="txtEmailPaciente" class="form-control" disabled="disabled" tabindex="5"><br>

              <label><font color="#0683c9">Fecha de Nacimiento: </font></label><br>
              <input type="date" name="txtFechaNacimientoPaciente" id="txtFechaNacimientoPaciente" class="form-control" disabled="disabled" tabindex="6"><br>


              <label><font color="#0683c9">Departamento Nacimiento</font></label><br>
              <select  class="form-control" id="selectDepartamentoNacimiento" name="selectDepartamentoNacimiento" onchange="fetch(this.value);" disabled="disabled" tabindex="7" >
                <?php
                            //consulta de tipo

                $result1 = mysql_query("SELECT DISTINCT  fullname FROM georeferences ORDER BY fullname asc ");
                while($row = mysql_fetch_array($result1)){
                 echo "<option>".$row['fullname']."</option>";
               } 
               ?>
             </select><br>

             <label><font color="#0683c9">Departamento de Residencia</font></label><br>
             <select  class="form-control" id="selectDepartamentoResidencia" name="selectDepartamentoResidencia" onchange="fetch2(this.value);" disabled="disabled" tabindex="8" >
              <?php
                            //consulta de tipo

              $result1 = mysql_query("SELECT DISTINCT  fullname FROM georeferences ORDER BY fullname asc ");
              while($row = mysql_fetch_array($result1)){
               echo "<option>".$row['fullname']."</option>";
             } 
             ?>
           </select><br>




           <label><font color="#0683c9">Ciudad de Nacimiento</font></label></br>
           <select  class="form-control" id="lugarn" name="lugarn" disabled="disabled" tabindex="8">             
           </select><br>

           <label><font color="#0683c9">Ciudad de Residencia</font></label></br>
           <select  class="form-control" id="lugarr" name="lugarr" disabled="disabled" tabindex="8">              
           </select></br>

           <label><font color="#0683c9">Zona Residencial</font></label>
         </br> 
         <input type="radio" name="Zonar" id="Zonar" value="U" tabindex="10"   /> Urbana</br>           

         <input type="radio" name="Zonar" id="Zonar" value="R" tabindex="10"   /> Rural</br>


         <label><font color="#0683c9">Tipo de Usuario</font></label></br>
         <select id="tipodeusuario" name="tipodeusuario" class="form-control" disabled="disabled" tabindex="8">
          <option value="1">Contributivo</option>
          <option value="2">Subsidiado</option>
          <option value="3">Vinculado</option>
          <option value="4">Particular</option>
          <option value="5">Otro</option>
          <option value="6">Desplazado Contributivo</option>
          <option value="7">Desplazado Subsidiado</option>
          <option value="8">Desplazado no Asegurado</option>
        </select><br>


        <label><font color="#0683c9">Género: </font></label>
        <div class="radio">
          <label><input type="radio" name="radioGenero" id="radioMasculino" value="M" tabindex="10" disabled="disabled">Masculino</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="radioGenero" id="radioFemenino" value="F" tabindex="11" disabled="disabled">Femenino</label>
        </div><br> 

        <label><font color="#0683c9">Clínica</font></label><br>
        <select class="form-control" id="selectClinicaPaciente" tabindex="12" disabled="disabled">

        </select><br> 

        <label><font color="#0683c9">Profesional</font></label><br>
        <div class="col-md-12" style="padding: 0px">   
          <div class="col-md-11" style="padding: 0px"> 
            <select class="form-control" id="selectProfesionalPaciente" tabindex="13" disabled="disabled">
            </select>  
          </div>
          <div class="col-md-1" style="padding: 0px" align="left"> 
            <button id="btnAddProfesionalPaciente" class="btn btn-success pull-right" type="button" disabled="disabled" data-toggle="modal" data-target="#myModalAddProfesional"><i class="fa fa-plus fa-1x" aria-hidden="true"></i></button>
          </div>
        </div>

        <br><br><button class="btn btn-primary" type="submit" id="btnAddPaciente" tabindex="15" disabled="disabled">Crear Paciente</button><br><br>



            <!--<div class="col-md-8">              
              <label><font color="#0683c9">Seleccione la imagen: </font></label><br>
              <input type="file" name="fileFoto" id="fileFoto" class="form-control" accept="  image/*" disabled="disabled" tabindex="9"><br>

              
            </div>
            <div class="col-md-4">
              <img src="" width="100%" id="imageFoto">              
            </div><br>-->

            

            

            
            
          </div> 
        </form>

      </div>
    </div>

  </div>
</div>

<div id="myModalAddProcedimientos" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 90%">

    <!-- Modal content-->
    <div class="modal-content" style="height: 600px; ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Procedimientos</h4>
      </div>
      <div class="modal-body" style="padding: 5px;height: 540px;" align="left">

        <div class="col-md-6">  
          <ul class="nav nav-pills">
            <li class="active"><a data-toggle="tab" href="#menu1">Fotografías</a></li>
            <li><a data-toggle="tab" href="#menu2">Radiografías</a></li>
            <li><a data-toggle="tab" href="#menu3">Módelos</a></li>
            <li><a data-toggle="tab" href="#menu4">Tomografías</a></li>
            
          </ul>

          <div class="tab-content">
            <div id="menu1" class="tab-pane fade in active">

            </div>
            <div id="menu2" class="tab-pane fade">

            </div>
            <div id="menu3" class="tab-pane fade">

            </div>
            <div id="menu4" class="tab-pane fade">

            </div>
            <div id="menu5" class="tab-pane fade">

            </div>
          </div>
        </div>


        <div class="col-md-6">
          <h4 align="center"><font color="#0683c9">Resumen de la Orden</font></h4><br>
          <table id="target" class="table table-bordered table-hover">
            <thead>
              <tr style="background-color: #01A9DB; color: white">
                <td id='colores'>Descripcion</td>
                <td id='colores'>Tipo</td>
                <td id='colores'>Seleccion</td>
                <td id='colores'>Observacion</td>
                <td id='colores'></td>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>

          <br>
          <label><font color="#0683c9">Clínica</font></label><br>
          <select class="form-control" id="selectClinicaProcedimiento"></select><br> 

          <label><font color="#0683c9">Profesional</font></label><br>
          <div class="col-md-12" style="padding: 0px">   
            <div class="col-md-11" style="padding: 0px"> 
              <select class="form-control" id="selectProfesionalProcedimiento" tabindex="13" disabled="disabled">
              </select>  
            </div>
            <div class="col-md-1" style="padding: 0px" align="left"> 
              <button id="btnAddProfesionalProcedimiento" class="btn btn-success pull-right" type="button" disabled="disabled" data-toggle="modal" data-target="#myModalAddProfesionalProcedimiento"><i class="fa fa-plus fa-1x" aria-hidden="true"></i></button>
            </div>
          </div>
        </div>

        <button class="btn btn-primary pull-right" disabled="disabled" id="btnAddProFactura" style="margin-top: 20px">Agregar Procedimientos</button><br><br>




      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>

<div id="myModalAddProfesional" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
      <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
      border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">

      <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

    </div>
    <form id="frmCrearProfesional" method="post" action=""> 
      <div class="modal-body" style="border: 0px; ">
        <h3 align="center" style="color: #0683c9;">NUEVO PROFESIONAL</h3><br>
        <div class="col-md-12">
          <div class="col-md-6"> 
            <label><font color="#0683c9">Tipo de Documento</font></label><br>
            <select class="form-control" id="selectTipoDocumentoProfesional" tabindex="16">
              <option>Cédula de ciudadanía</option>
              <option>Registro civil</option>
              <option>Tarjeta de identidad</option>
              <option>Cédula de extranjería</option>
              <option>NIT</option>
              <option>Adulto sin identificación</option>
              <option>Menor sin identificación</option>
              <option>Pasaporte</option>
            </select><br>    

            <label><font color="#0683c9">Nombres: </font></label><br>
            <input type="text" name="txtNombresProfesional" id="txtNombresProfesional" class="form-control" disabled="disabled" tabindex="18"><br>                


            <label><font color="#0683c9">E-mail: </font></label><br>
            <input type="email" name="txtEmailProfesional" id="txtEmailProfesional" class="form-control" disabled="disabled" tabindex="20"><br>

            <label><font color="#0683c9">Lugar de Residencia</font></label><br>
            <select class="form-control" id="selectResidenciaProfesional" disabled="disabled" tabindex="22">      
            </select><br>  

            <label><font color="#0683c9">Clinica: </font></label><br>
            <input type="text" name="txtClinicaProfesional" id="txtClinicaProfesional" class="form-control" disabled="disabled" tabindex="24"><br>   

          </div>
          <div class="col-md-6">  
            <label><font color="#0683c9">Número de Documento: </font></label><br>
            <input type="text" name="txtDocumentoProfesional" id="txtDocumentoProfesional" class="form-control" tabindex="17"><br>  

            <label><font color="#0683c9">Apellidos: </font></label><br>
            <input type="text" name="txtApellidosProfesional" id="txtApellidosProfesional" class="form-control" disabled="disabled" tabindex="19"><br>

            <label><font color="#0683c9">Teléfono: </font></label><br>
            <input type="text" name="txtTelefonoProfesional" id="txtTelefonoProfesional" class="form-control" disabled="disabled" tabindex="21"><br>

            <label><font color="#0683c9">Dirección: </font></label><br>
            <input type="text" name="txtDireccionProfesional" id="txtDireccionProfesional" class="form-control" disabled="disabled" tabindex="23"><br>

          </div>
        </div>    

      </div>
      <div class="modal-footer" style="border: 0px">
        <button type="submit" class="btn btn-primary" id="btnRegistrarProfesional" tabindex="25">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" tabindex="26">CANCELAR</button>

      </div>
    </form>

  </div>

</div>
</div>

<div id="myModalAddProfesionalProcedimiento" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
      <div class="modal-header" style="border: 1px; border-bottom-right-radius: 50%;
      border-bottom-left-radius: 50%; background-color: #0683c9;height: 90px;" align="center">

      <img id="imagenheadersuperior" src="../../images/LOGO SW CENTRAL.png" height="70px" width="120px">

    </div>
    <form id="frmCrearProfesionalProcedimiento" method="post" action=""> 
      <div class="modal-body" style="border: 0px; ">
        <h3 align="center" style="color: #0683c9;">NUEVO PROFESIONAL</h3><br>
        <div class="col-md-12">
          <div class="col-md-6"> 
            <label><font color="#0683c9">Tipo de Documento</font></label><br>
            <select class="form-control" id="selectTipoDocumentoProfesionalProcedimiento" tabindex="16">
              <option>Cédula de ciudadanía</option>
              <option>Registro civil</option>
              <option>Tarjeta de identidad</option>
              <option>Cédula de extranjería</option>
              <option>NIT</option>
              <option>Adulto sin identificación</option>
              <option>Menor sin identificación</option>
              <option>Pasaporte</option>
            </select><br>    

            <label><font color="#0683c9">Nombres: </font></label><br>
            <input type="text" name="txtNombresProfesionalProcedimiento" id="txtNombresProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="18"><br>                


            <label><font color="#0683c9">E-mail: </font></label><br>
            <input type="email" name="txtEmailProfesionalProcedimiento" id="txtEmailProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="20"><br>

            <label><font color="#0683c9">Lugar de Residencia</font></label><br>
            <select class="form-control" id="selectResidenciaProfesionalProcedimiento" disabled="disabled" tabindex="22">      
            </select><br>  

            <label><font color="#0683c9">Clínica: </font></label><br>
            <input type="text" name="txtClinicaProfesionalProcedimiento" id="txtClinicaProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="24"><br>

          </div>
          <div class="col-md-6">  
            <label><font color="#0683c9">Número de Documento: </font></label><br>
            <input type="text" name="txtDocumentoProfesionalProcedimiento" id="txtDocumentoProfesionalProcedimiento" class="form-control" tabindex="17"><br>  

            <label><font color="#0683c9">Apellidos: </font></label><br>
            <input type="text" name="txtApellidosProfesionalProcedimiento" id="txtApellidosProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="19"><br>

            <label><font color="#0683c9">Teléfono: </font></label><br>
            <input type="text" name="txtTelefonoProfesionalProcedimiento" id="txtTelefonoProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="21"><br>

            <label><font color="#0683c9">Dirección: </font></label><br>
            <input type="text" name="txtDireccionProfesionalProcedimiento" id="txtDireccionProfesionalProcedimiento" class="form-control" disabled="disabled" tabindex="23"><br>

          </div>
        </div>    

      </div>
      <div class="modal-footer" style="border: 0px">
        <button type="submit" class="btn btn-primary" id="btnRegistrarProfesionalProcedimiento" tabindex="25">REGISTRAR</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" tabindex="26">CANCELAR</button>

      </div>
    </form>

  </div>

</div>
</div>

<!-- Modal -->
<div id="myModalFormaPago" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog" style="width: 80%">

    <!-- Modal content-->
    <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
      <div class="modal-header" style="border: 0px">
        <h4 align="right" style="color: #428bca; font-weight: bolder">HCO - Formas de Pagos</h4>
        <hr>
      </div>
      <div class="modal-body" style="border: 0px; padding: 0px">

        <div class="col-md-4">
          <img width="100%" src="../../images/logohco.png">
        </div>
        <div class="col-md-8">
          <form id="frmCrearPago" method="post" action="">    

            <div class="col-md-12">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Forma de Pago</font></label>
              </div>
              <div class="col-md-6">
                <select id="selecTipoPago" class="form-control">
                  <option value="Seleccione">Séleccione</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Tarjeta de Credito">Tarjeta de Credito</option>
                  <option value="Otra forma de pago">Otra forma de pago</option>
                </select>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divNumeroChque">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Número Cheque</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtNumeroCheque" id="txtNumeroCheque" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanNumeroCheque"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divCodigoBanco">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Codigo Banco</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtCodigoBanco" id="txtCodigoBanco" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanCodigoBanco"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divNumeroCuenta">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Número Cuenta</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtNumeroCuenta" id="txtNumeroCuenta" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanNumeroCuenta"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divFechaCheque">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Fecha</font></label>
              </div>
              <div class="col-md-6">
                <input type="date" name="txtFechaCheque" id="txtFechaCheque" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanFechaCheque"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divCodigoTarjeta">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Código</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtCodigoTarjeta" id="txtCodigoTarjeta" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanCodigoTarjeta"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divTipoTarjeta">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Tipo</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtTipoTarjeta" id="txtTipoTarjeta" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanTipoTarjeta"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divBancoTarjeta">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Banco</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtBancoTarjeta" id="txtBancoTarjeta" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanBancoTarjeta"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divNumeroTarjeta">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Número</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtNumeroTarjeta" id="txtNumeroTarjeta" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanNumeroTarjeta"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divDocuemtoOtro">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Documento</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtDocuemtoOtro" id="txtDocuemtoOtro" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanDocuemtoOtro"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divFormaPagoOtro">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Forma de Pago</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtFormaPagoOtro" id="txtFormaPagoOtro" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanFormaPagoOtro"></span>
              </div>
            </div>

            <div class="col-md-12" style="margin-top: 7px; display: none;" id="divCodigoOtro">
              <div class="col-md-6" align="right">
                <label style="margin-top: 7px"><font color="#428bca">Código</font></label>
              </div>
              <div class="col-md-6">
                <input type="text" name="txtCodigoOtro" id="txtCodigoOtro" class="form-control input-forma">
                <span class="errorDatoCrear" style="display: none;" id="spanCodigoOtro"></span>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer" style="border: 0px">
        <button type="button" class="btn btn-primary" disabled="disabled" id="btnRealizarPago">Aceptar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancelar">Cancelar</button>
      </div>
    </div>

  </div>
</div>

<div id="myModalMensaje" data-backdrop="static" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-color: #0683c9; border-width: 4px;">
      <div class="modal-header" style="border: 0px">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="col-md-4">
          <img width="100%" src="<?= base_url();?>images/LOGO SW CENTRAL.png">
        </div>
        <div class="col-md-6">
          <h4 align="center" id="h4Mensaje"></h4>
        </div>

      </div>
      <div class="modal-body" style="border: 0px">

      </div>
      <div class="modal-footer" style="border: 0px">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="">ACEPTAR</button>
      </div>
    </div>

  </div>
</div>





<!-- call calendar plugin -->
<script src="<?= base_url();?>js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?= base_url();?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url();?>js/bootstrap.js"></script>
<script src="<?= base_url();?>js/jquery.babypaunch.spinner.min.js"></script>
<script src="<?= base_url();?>js/jquery.validate.js"></script>


<script type="text/javascript">
  var arrayDatos = []
  var arrayDatosTomo = []
  $.validator.setDefaults({
        //Capturar evento del boton crear
        
      });

  $(function(){
      //$("#spin").spinner();

      $("#spin").spinner({
        color: "black"
        , background: "rgba(255,255,255,0.5)"
        , html: "<i class='fa fa-circle-o-notch fa-5x' style='color: gray;'></i>"
        , spin: true
      });     
      

    });
  
  (function() {     

        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrear").tooltip({
          show: false,
          hide: false
        });

        // validate signup form on keyup and submit
        $("#frmCrear").validate({
          rules: {
            txtNombresPaciente:"required",
            txtDocumentoPaciente:"required",
            txtApellidosPaciente:"required",
            txtEmailPaciente:"required",
            txtFechaNacimientoPaciente:"required"
          },
          messages: {
            txtNombresPaciente: "Por favor ingrese un nombre",
            txtDocumentoPaciente: "Por favor ingrese un numero de documento",
            txtApellidosPaciente: "Por favor ingrese el apellido",
            txtEmailPaciente: "Por favor ingrese un e-mail válido",
            txtFechaNacimientoPaciente: "Por favor ingrese una fecha de nacimiento"
          },
          submitHandler: function(form) {           
            var typeuser = "person";
            var typedoc = $("#selectTipoDocumento").val();
            var numerodoc = $("#txtDocumentoPaciente").val();
            var clinica = $("#selectClinicaPaciente").val();
            var profesionalSelec = $("#selectProfesionalPaciente").val();
            var nombre = $("#txtNombresPaciente").val();
            var apellido = $("#txtApellidosPaciente").val();
            var email = $("#txtEmailPaciente").val();
            var birthdate = $("#txtFechaNacimientoPaciente").val();
            var lugarNac = $("#selectDepartamentoNacimiento").val();
            var lugarResi = $("#selectDepartamentoResidencia").val();
            var lugarN = $("#lugarn").val();
            var lugarR = $("#lugarr").val();
            var zonar = $('input[name=Zonar]:checked').val();
            var tipousuarior = $("#tipodeusuario").val();
            var genero = $('input[name=radioGenero]:checked').val();
            //var foto = $('#imageFoto').attr("src");

            //Se guardan los datos en un JSON
            var datos = {
              TipoUsu: typeuser,
              TipoDoc: typedoc,
              NumeroDoc: numerodoc,
              nombre: nombre,
              apellido: apellido,
              email: email,
              birthdate: birthdate,
              lugarNac: lugarN,
              lugarResi: lugarR,
              zonar: zonar,
              tipousuarior: tipousuarior,
              genero: genero/*,
              foto: foto*/
            }   
            console.log(genero)
            //Enviar datos para modificarse
            $.post("<?= base_url();?>index.php/user_radiologia/addPaciente", datos)
            .done(function( data ) {console.log(data)             
              alert("¡Se registro con exito el paciente!")
              $("#txtNombresPaciente").val("")
              $("#txtDocumentoPaciente").val("")
              $("#txtApellidosPaciente").val("")
              $("#txtEmailPaciente").val("")
              $("#txtFechaNacimientoPaciente").val("")   
              $("#fileFoto").val("")

              $("#myModalAddPerson").modal("hide");
              $(".modal-backdrop").css({"display":"none"})
              $("#imageFoto").attr("src", "")
              $("#selectTipoDocumento").val("Cedula de Ciudadania")

              $("#txtNombresPaciente").attr("disabled", "disabled");
              $("#txtApellidosPaciente").attr("disabled", "disabled");
              $("#txtEmailPaciente").attr("disabled", "disabled");
              $("#txtFechaNacimientoPaciente").attr("disabled", "disabled");
              $("#selectDepartamentoNacimiento").attr("disabled", "disabled");
              $("#selectDepartamentoResidencia").attr("disabled", "disabled");
              $("#lugarn").attr("disabled", "disabled");
              $("#lugarr").attr("disabled", "disabled");
              $("#tipodeusuario").attr("disabled", "disabled");
              $("#fileFoto").attr("disabled", "disabled");
              $('input[name=radioGenero]').attr("disabled", "disabled");
              $("#btnAddPaciente").attr("disabled", "disabled");
              $("#btnAddProfesionalPaciente").attr("disabled", "disabled");
              /*listarDepartamentos()*/  

              if($.trim(data) != "{}"){
                var json = JSON.parse(data)
                llenarListaPacientes(json.document__number)
                //$("#txtIdentificacion").val(json.document__number)
                $("#txtNombre").val(json.first_name + " " + json.last_name)
                $("#txtDireccion").val(json.direccion)
                $("#txtTelefono").val(json.telefono + " " + json.celular)
                $("#txtEmail").val(json.email)
                $("#txtClinicaHidden").val(clinica)
                $("#txtProfesionalHidden").val(profesionalSelec)
                $("#selectClinicaProcedimiento").attr("disabled", "disabled");
                $("#selectProfesionalProcedimiento").attr("disabled", "disabled");
                $("#btnAddProcedimientos").removeAttr("disabled")
                $('input[name=radioGenero]').prop('checked', false);
                $("#tbodyOrdenMedica").html("")
                $("#tbodyResumen").html("")
                $("#btnGenerarFactura").attr("disabled", "disabled")
              }else{
                $("#btnGenerarFactura").attr("disabled", "disabled")
                $("#selectClinicaProcedimiento").attr("disabled", "disabled");
                $("#selectProfesionalProcedimiento").attr("disabled", "disabled");
                $("#tbodyOrdenMedica").html("")
                $("#tbodyResumen").html("")
                $("#txtIdentificacion").chosen().val([])
                $("#txtNombre").val("")
                $("#txtDireccion").val("")
                $("#txtTelefono").val("")
                $("#txtEmail").val("")
                $("#txtClinicaHidden").val("")
                $("#txtProfesionalHidden").val("")
                $("#btnAddProcedimientos").attr("disabled", "disabled")
                $('input[name=radioGenero]').prop('checked', false);

              }

            });  
          } 
        });


        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrearProfesional").tooltip({
          show: false,
          hide: false
        });

        // validate signup form on keyup and submit
        $("#frmCrearProfesional").validate({
          rules: {
            txtNombresProfesional:"required",
            txtDocumentoProfesional:"required",
            txtApellidosProfesional:"required",
            txtEmailProfesional:"required",
            txtTelefonoProfesional:"required",
            txtDireccionProfesional:"required"
          },
          messages: {
            txtNombresProfesional: "Por favor ingrese un nombre del profesional",
            txtDocumentoProfesional: "Por favor ingrese un numero de documento del profesional",
            txtApellidosProfesional: "Por favor ingrese el apellido del profesional",
            txtEmailProfesional: "Por favor ingrese un e-mail válido del profesional",
            txtTelefonoProfesional: "Por favor ingrese un telefono del profesional",
            txtDireccionProfesional: "Por favor ingrese una dirección del profesional"
          },
          submitHandler: function(form) {           
            var typeuser = "odontologo";
            var typedoc = $("#selectTipoDocumentoProfesional").val();
            var numerodoc = $("#txtDocumentoProfesional").val();
            var nombre = $("#txtNombresProfesional").val();
            var apellido = $("#txtApellidosProfesional").val();
            var email = $("#txtEmailProfesional").val();
            var telefono = $("#txtTelefonoProfesional").val();
            var lugarResi = $("#selectResidenciaProfesional").val();
            var direccion = $("#txtDireccionProfesional").val();
            var clinica = $("#selectClinicaPaciente").val();

            //Se guardan los datos en un JSON
            var datos = {
              TipoUsu: typeuser,
              TipoDoc: typedoc,
              NumeroDoc: numerodoc,
              nombre: nombre,
              apellido: apellido,
              email: email,
              telefono: telefono,
              lugarResi: lugarResi,
              direccion: direccion,
              clinica: clinica
            }   
            //Enviar datos para modificarse
            $.post("<?= base_url();?>index.php/user_radiologia/addProfesional", datos)
            .done(function( data ) {console.log(data)             
              if($.trim(data) != "{}"){
                alert("¡Profesional creado con exito!")
                var json = JSON.parse(data)
                listarProfesionales($("#selectClinicaPaciente").val(), json.email)
                $("#myModalAddProfesional").modal("hide");
                $(".modal-backdrop").css({"display":"none"})
              }else{

              }

            }); 
          } 
        });

        // use custom tooltip; disable animations for now to work around lack of refresh method on tooltip
        $("#frmCrearProfesionalProcedimiento").tooltip({
          show: false,
          hide: false
        });

        // validate signup form on keyup and submit
        $("#frmCrearProfesionalProcedimiento").validate({
          rules: {
            txtNombresProfesionalProcedimiento:"required",
            txtDocumentoProfesionalProcedimiento:"required",
            txtApellidosProfesionalProcedimiento:"required",
            txtEmailProfesionalProcedimiento:"required",
            txtTelefonoProfesionalProcedimiento:"required",
            txtDireccionProfesionalProcedimiento:"required"
          },
          messages: {
            txtNombresProfesionalProcedimiento: "Por favor ingrese un nombre del profesional",
            txtDocumentoProfesionalProcedimiento: "Por favor ingrese un numero de documento del profesional",
            txtApellidosProfesionalProcedimiento: "Por favor ingrese el apellido del profesional",
            txtEmailProfesionalProcedimiento: "Por favor ingrese un e-mail válido del profesional",
            txtTelefonoProfesionalProcedimiento: "Por favor ingrese un telefono del profesional",
            txtDireccionProfesionalProcedimiento: "Por favor ingrese una dirección del profesional"
          },
          submitHandler: function(form) {           
            var typeuser = "odontologo";
            var typedoc = $("#selectTipoDocumentoProfesionalProcedimiento").val();
            var numerodoc = $("#txtDocumentoProfesionalProcedimiento").val();
            var nombre = $("#txtNombresProfesionalProcedimiento").val();
            var apellido = $("#txtApellidosProfesionalProcedimiento").val();
            var email = $("#txtEmailProfesionalProcedimiento").val();
            var telefono = $("#txtTelefonoProfesionalProcedimiento").val();
            var lugarResi = $("#selectResidenciaProfesionalProcedimiento").val();
            var direccion = $("#txtDireccionProfesionalProcedimiento").val();
            var clinica = $("#selectClinicaProcedimiento").val();

            //Se guardan los datos en un JSON
            var datos = {
              TipoUsu: typeuser,
              TipoDoc: typedoc,
              NumeroDoc: numerodoc,
              nombre: nombre,
              apellido: apellido,
              email: email,
              telefono: telefono,
              lugarResi: lugarResi,
              direccion: direccion,
              clinica: clinica
            }   
            //Enviar datos para modificarse
            $.post("<?= base_url();?>index.php/user_radiologia/addProfesional", datos)
            .done(function( data ) {console.log(data)             
              if($.trim(data) != "{}"){
                alert("¡Profesional creado con exito!")
                var json = JSON.parse(data)
                listarProfesionalesProcedimiento($("#selectClinicaProcedimiento").val(), json.email)
                $("#myModalAddProfesionalProcedimiento").modal("hide");
                $(".modal-backdrop").css({"display":"none"})
              }else{

              }

            }); 
          } 
        });
        
      })();

    //Validacion del campo codigo para que no se repita
    $("#txtDocumentoPaciente").keyup(function(e){
      var documento = $("#txtDocumentoPaciente").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        NumeroDoc: documento
      }   
      
      $.post("<?= base_url();?>index.php/user_radiologia/getPaciente", datos)
      .done(function( data ) {console.log(data)             
        if($.trim(data) != "{}"){
          $("#txtNombresPaciente").attr("disabled", "disabled");
          $("#txtApellidosPaciente").attr("disabled", "disabled");
          $("#txtEmailPaciente").attr("disabled", "disabled");
          $("#txtFechaNacimientoPaciente").attr("disabled", "disabled");
          $("#selectDepartamentoNacimiento").attr("disabled", "disabled");
          $("#selectClinicaPaciente").attr("disabled", "disabled");
          $("#selectProfesionalPaciente").attr("disabled", "disabled");
          $("#selectDepartamentoResidencia").attr("disabled", "disabled");
          $("#fileFoto").attr("disabled", "disabled");
          $('input[name=radioGenero]').attr("disabled", "disabled");
          $("#btnAddPaciente").attr("disabled", "disabled");
          $("#btnAddProfesionalPaciente").attr("disabled", "disabled");
          alert("¡El paciente con el numero de documento " + documento + " ya se encuentra registrado! Realice la busqueda de sus datos para facturar")  
        }else{
          $("#txtNombresPaciente").removeAttr("disabled");
          $("#txtApellidosPaciente").removeAttr("disabled");
          $("#txtEmailPaciente").removeAttr("disabled");
          $("#txtFechaNacimientoPaciente").removeAttr("disabled");
          $("#selectClinicaPaciente").removeAttr("disabled");
          $("#selectProfesionalPaciente").attr("disabled", "disabled");
          $("#selectDepartamentoNacimiento").removeAttr("disabled");
          $("#selectDepartamentoResidencia").removeAttr("disabled");
          $("#lugarn").removeAttr("disabled");
          $("#lugarr").removeAttr("disabled");
          $("#tipodeusuario").removeAttr("disabled");
          $("#fileFoto").removeAttr("disabled");
          $('input[name=radioGenero]').removeAttr("disabled");
          $("#fileFoto").removeAttr("disabled");
          $("#btnAddPaciente").attr("disabled", "disabled");
          $("#btnAddProfesionalPaciente").attr("disabled", "disabled");
          listarClinicas()
        }
        

      });
      
    })  

    //Validacion del campo codigo para que no se repita
    $("#txtDocumentoProfesional").keyup(function(e){
      var documento = $("#txtDocumentoProfesional").val();
      var clinica = $("#txtClinicaProfesional").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        NumeroDoc: documento,
        clinica_id: clinica,
        is_profesional: true
      }   
      
      $.post("<?= base_url();?>index.php/user_radiologia/getPacienteDocument", datos)
      .done(function( data ) {
        console.log(data)             
        if($.trim(data) != "{}"){          
          $("#txtNombresProfesional").attr("disabled", "disabled");
          $("#txtApellidosProfesional").attr("disabled", "disabled");
          $("#txtEmailProfesional").attr("disabled", "disabled");
          $("#txtTelefonoProfesional").attr("disabled", "disabled");
          $("#selectResidenciaProfesional").attr("disabled", "disabled");
          $("#txtDireccionProfesional").attr("disabled", "disabled");
          $("#txtClinicaProfesional").attr("disabled", "disabled");
          alert("¡El profesional con el numero de documento " + documento + " ya se encuentra registrado!")  
        }else{
          if(documento != $("#txtDocumentoPaciente").val()){
            $("#txtNombresProfesional").removeAttr("disabled");
            $("#txtApellidosProfesional").removeAttr("disabled");
            $("#txtEmailProfesional").removeAttr("disabled");
            $("#txtTelefonoProfesional").removeAttr("disabled");
            $("#selectResidenciaProfesional").removeAttr("disabled");
            $("#txtDireccionProfesional").removeAttr("disabled");
          }else{
            $("#txtNombresProfesional").attr("disabled", "disabled");
            $("#txtApellidosProfesional").attr("disabled", "disabled");
            $("#txtEmailProfesional").attr("disabled", "disabled");
            $("#txtTelefonoProfesional").attr("disabled", "disabled");
            $("#selectResidenciaProfesional").attr("disabled", "disabled");
            $("#txtDireccionProfesional").attr("disabled", "disabled");
          }
          
        }
        

      });
      
    })  

    //Validacion del campo codigo para que no se repita
    $("#txtDocumentoProfesionalProcedimiento").keyup(function(e){
      var documento = $("#txtDocumentoProfesionalProcedimiento").val();
      
      //Se guardan los datos en un JSON
      var datos = {
        NumeroDoc: documento
      }   
      
      $.post("<?= base_url();?>index.php/user_radiologia/getPacienteDocument", datos)
      .done(function( data ) {console.log(data)             
        if($.trim(data) != "{}"){          
          $("#txtNombresProfesionalProcedimiento").attr("disabled", "disabled");
          $("#txtApellidosProfesionalProcedimiento").attr("disabled", "disabled");
          $("#txtEmailProfesionalProcedimiento").attr("disabled", "disabled");
          $("#txtTelefonoProfesionalProcedimiento").attr("disabled", "disabled");
          $("#selectResidenciaProfesionalProcedimiento").attr("disabled", "disabled");
          $("#txtDireccionProfesionalProcedimiento").attr("disabled", "disabled");
          $("#txtClinicaProfesionalProcedimiento").attr("disabled", "disabled");
          alert("¡El profesional con el numero de documento " + documento + " ya se encuentra registrado!")  
        }else{

          $("#txtNombresProfesionalProcedimiento").removeAttr("disabled");
          $("#txtApellidosProfesionalProcedimiento").removeAttr("disabled");
          $("#txtEmailProfesionalProcedimiento").removeAttr("disabled");
          $("#txtTelefonoProfesionalProcedimiento").removeAttr("disabled");
          $("#selectResidenciaProfesionalProcedimiento").removeAttr("disabled");
          $("#txtDireccionProfesionalProcedimiento").removeAttr("disabled");
          
        }
        

      });
      
    })  

    function readImage() {
      if ( this.files && this.files[0] ) {
        var long = this.files.length;

        for(var i = 0; i < long; i++){
          var FR = new FileReader();
          
          FR.onload = function(e) {            
            $("#imageFoto").attr("src", e.target.result)
            console.log(e.target.result)
          }           
          
          FR.readAsDataURL( this.files[i] );
        }      

      }
    }  

    
    $("#fileFoto").change( readImage );

    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
      //console.log(e.target.innerText)
      listarTipos(e.target.attributes.valor.value)
    })

    $('#myModalAddProcedimientos').on('shown.bs.modal', function() {
      $("#target > tbody").html("")
      listarGrupos()
      if($.trim($("#txtProfesionalHidden").val()) != ""){
        listarClinicasProcedimientos($("#txtClinicaHidden").val(), $("#txtProfesionalHidden").val())
        $("#selectClinicaProcedimiento").attr("disabled", "disabled")        
        $("#btnAddProFactura").attr("disabled", "disabled")
      }else{
        listarClinicasProcedimientos("", "")
        $("#selectClinicaProcedimiento").removeAttr("disabled")
        $("#selectProfesionalProcedimiento").html("")
        $("#btnAddProFactura").attr("disabled", "disabled")          
        
      }
      
    })

    $('#myModalAddProfesional').on('shown.bs.modal', function() {
      $("#txtClinicaProfesional").val($("#selectClinicaPaciente").val())

      $("#txtNombresProfesional").val("")
      $("#txtDocumentoProfesional").val("")
      $("#txtApellidosProfesional").val("")
      $("#txtEmailProfesional").val("")
      $("#txtTelefonoProfesional").val("")
      $("#txtDireccionProfesional").val("")
      listarDepartamentosProfesional()
      $("#selectTipoDocumentoProfesional").val("Cedula de Ciudadania")
    })

    $('#myModalAddProfesionalProcedimiento').on('shown.bs.modal', function() {
      $("#txtClinicaProfesionalProcedimiento").val($("#selectClinicaProcedimiento").val())

      $("#txtNombresProfesionalProcedimiento").val("")
      $("#txtDocumentoProfesionalProcedimiento").val("")
      $("#txtApellidosProfesionalProcedimiento").val("")
      $("#txtEmailProfesionalProcedimiento").val("")
      $("#txtTelefonoProfesionalProcedimiento").val("")
      $("#txtDireccionProfesionalProcedimiento").val("")
      listarDepartamentosProfesionalProce()
      $("#selectTipoDocumentoProfesionalProcedimiento").val("Cedula de Ciudadania")
    })

    $('#myModalAddPerson').on('shown.bs.modal', function() {
      /*listarDepartamentos()*/
      $("#txtNombresPaciente").val("")
      $("#txtDocumentoPaciente").val("")
      $("#txtApellidosPaciente").val("")
      $("#txtEmailPaciente").val("")
      $("#txtFechaNacimientoPaciente").val("")   
      $("#fileFoto").val("")
      $("#selectTipoDocumento").val("Cedula de Ciudadania")
      $('input[name=radioGenero]').prop('checked', false);
      $("#imageFoto").removeAttr("src", "")

      $("#txtNombresPaciente").attr("disabled", "disabled");
      $("#txtApellidosPaciente").attr("disabled", "disabled");
      $("#txtEmailPaciente").attr("disabled", "disabled");
      $("#txtFechaNacimientoPaciente").attr("disabled", "disabled");
      $("#selectDepartamentoNacimiento").attr("disabled", "disabled");
      $("#selectClinicaPaciente").attr("disabled", "disabled");
      $("#selectProfesionalPaciente").attr("disabled", "disabled");
      $("#selectDepartamentoResidencia").attr("disabled", "disabled");
      $("#fileFoto").attr("disabled", "disabled");
      $('input[name=radioGenero]').attr("disabled", "disabled");
      $("#btnAddPaciente").attr("disabled", "disabled");
      $("#btnAddProfesionalPaciente").attr("disabled", "disabled");
    })

    $("#selectClinicaPaciente").change(function() {
      if($("#selectClinicaPaciente").val() != "Seleccione"){
        listarProfesionales($("#selectClinicaPaciente").val(), "")
      }else{
        $("#selectProfesionalPaciente").html("")
        $("#selectProfesionalPaciente").attr("disabled", "disabled");
      }
    })  

    $("#selectClinicaProcedimiento").change(function() {
      if($("#selectClinicaProcedimiento").val() != "Seleccione"){
        listarProfesionalesProcedimiento($("#selectClinicaProcedimiento").val(), "")

      }else{
        $("#btnAddProFactura").attr("disabled", "disabled");
        $("#selectProfesionalProcedimiento").html("")
        $("#selectProfesionalProcedimiento").attr("disabled", "disabled");
      }

    })  

    $("#btnAddProcedimientos").click(function() {
    })  

    $("#btnGenerarFactura").click(function() {


    })

    function listarClinicas(){
      $.post("<?= base_url();?>index.php/user_radiologia/listarClinicas", {})
      .done(function( data ) {
        $("#selectClinicaPaciente").html("")
        $("#selectClinicaPaciente").append("<option value='Seleccione'>Seleccione...</option>") 
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectClinicaPaciente").append("<option value='" + json[i].clinica + "'>" + json[i].clinica + "</option>") 
          }
        }else{
          $("#selectClinicaPaciente").attr("disabled", "disabled");
        }
      })
    }

    function listarClinicasProcedimientos(clinica, profesional){
      $.post("<?= base_url();?>index.php/user_radiologia/listarClinicas", {})
      .done(function( data ) {
        $("#selectClinicaProcedimiento").html("")
        $("#selectClinicaProcedimiento").append("<option value='Seleccione'>Seleccione...</option>") 
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectClinicaProcedimiento").append("<option value='" + json[i].clinica + "'>" + json[i].clinica + "</option>") 
          }
          if($.trim(clinica) != ""){
            $("#selectClinicaProcedimiento").val(clinica)
            listarProfesionalesProcedimiento(clinica, profesional)
            $("#selectProfesionalPaciente").attr("disabled", "disabled");
          }else{
            $("#selectProfesionalProcedimiento").attr("disabled", "disabled")
          }
        }else{
          $("#selectProfesionalProcedimiento").attr("disabled", "disabled")
        }
      })
    }

    function listarProfesionales(clinica, profesional){
      var datos = {
        clinica: clinica
      }
      
      $.post("<?= base_url();?>index.php/user_radiologia/listarProfesionales", datos)
      .done(function( data ) {
        $("#selectProfesionalPaciente").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectProfesionalPaciente").append("<option value='" + json[i].email + "'>" + json[i].first_name + " " + json[i].last_name + "</option>") 
          }
          $("#selectProfesionalPaciente").removeAttr("disabled");
          $("#btnAddProfesionalPaciente").removeAttr("disabled");          
          $("#btnAddPaciente").removeAttr("disabled");

          if(profesional != ""){
            $("#selectProfesionalPaciente").val(profesional)
          }
        }else{
          $("#selectProfesionalPaciente").attr("disabled", "disabled");
          $("#btnAddProfesionalPaciente").attr("disabled", "disabled");
          $("#btnAddPaciente").attr("disabled", "disabled");
        }
      })
    }

    function listarProfesionalesProcedimiento(clinica, profesional){
      var datos = {
        clinica: clinica
      }
      
      $.post("<?= base_url();?>index.php/user_radiologia/listarProfesionales", datos)
      .done(function( data ) {
        $("#selectProfesionalProcedimiento").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectProfesionalProcedimiento").append("<option value='" + json[i].email + "'>" + json[i].first_name + " " + json[i].last_name + "</option>") 
          }

          $("#btnAddProfesionalProcedimiento").removeAttr("disabled"); 

          if(profesional != ""){
            $("#selectProfesionalProcedimiento").val(profesional)
            $("#selectProfesionalProcedimiento").attr("disabled", "disabled")
          }else{ 
            $("#selectProfesionalProcedimiento").removeAttr("disabled");
          }

          if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
            $("#btnAddProFactura").removeAttr("disabled");
          }else{
            $("#btnAddProFactura").attr("disabled", "disabled");
          }
        }else{
          $("#selectProfesionalProcedimiento").attr("disabled", "disabled");
          $("#btnAddProfesionalProcedimiento").attr("disabled", "disabled");
          $("#btnAddProFactura").attr("disabled", "disabled");
        }
      })
    }
/*
    function listarDepartamentos(){
      $.post("<?= base_url();?>index.php/user_radiologia/listarDepartamentos", {})
      .done(function( data ) {//console.log(data)
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectDepartamentoResidencia").append("<option value='" + json[i].fullname + "'>" + json[i].fullname + "</option>") 
            $("#selectDepartamentoNacimiento").append("<option value='" + json[i].fullname + "'>" + json[i].fullname + "</option>") 
          }
        }
      });
    }
    */
    function listarDepartamentosProfesional(){
      $.post("<?= base_url();?>index.php/user_radiologia/listarDepartamentos", {})
      .done(function( data ) {//console.log(data)
        $("#selectResidenciaProfesional").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectResidenciaProfesional").append("<option value='" + json[i].fullname + "'>" + json[i].fullname + "</option>") 
          }
        }
      });
    }

    function listarDepartamentosProfesionalProce(){
      $.post("<?= base_url();?>index.php/user_radiologia/listarDepartamentos", {})
      .done(function( data ) {//console.log(data)
        $("#selectResidenciaProfesionalProcedimiento").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            $("#selectResidenciaProfesionalProcedimiento").append("<option value='" + json[i].fullname + "'>" + json[i].fullname + "</option>") 
          }
        }
      });
    }
    function listarGrupos(){
      $.post("<?= base_url();?>index.php/procedimientos/listarGrupos", {})
      .done(function( data ) {//console.log(data)
        $(".nav-pills").html("")
        $(".tab-content").html("")
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for(i = 0; i < json.length; i++){
            if(i == 0){
              $(".nav-pills").append('<li class="active"><a data-toggle="tab" href="#menu' + (i + 1) + '" valor="' + json[i].id + '">' + json[i].nombre + '</a></li>')

              $(".tab-content").append('<div id="menu' + (i + 1) + '" class="tab-pane fade in active" style="border: 2px solid #efefef;font-size: 12px;height: 490px;overflow-y: scroll;"></div>')
              listarTipos(json[i].id)
            }else{
              $(".nav-pills").append('<li><a data-toggle="tab" href="#menu' + (i + 1) + '" valor="' + json[i].id + '">' + json[i].nombre + '</a></li>')  

              $(".tab-content").append('<div id="menu' + (i + 1) + '" class="tab-pane fade"></div>')
            }
            
          }
          $(".nav-pills > li > a").click(function(e){
            var id = $(this).attr("valor")
            listarTipos(id)
          })
        }  
      });
    }

    function listarTipos(grupo){   
      var datos = {
        grupo: grupo
      }  
      $.post("<?= base_url();?>index.php/procedimientos/listarTipos", datos)
      .done(function( data ) {console.log(data)

        if($.trim(data) != "[]"){
          var json = JSON.parse(data)

          switch(grupo){
            case "1":
            $("#menu" + grupo).html("")
            $("#menu" + grupo).append('<div class="col-md-6"><br>' + 
              'Fotografias Intraorales<br>' +
              '<select id="selintraoral" class="form-control">' +
              '</select>' + 
              '<div style="width: 100%; display: none;" id="obserfotointraoral"><br>' +
              '<br>Observaciones<br>' +
              '<textarea class="form-control" id="observacionesfotointraoral"></textarea><br>' +
              '</div>' +
              '</div>')      

            $("#menu" + grupo).append('<div class="col-md-6"><br>' + 
              'Fotografias Extraorales<br>' +
              '<select id="selextraoral" class="form-control">' +
              '</select>' +
              '<br>' +
              '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 6px; padding-left: 20px; display: none" id="divPresentacionFotografiaExtra">' +
              '<br><br>Presentacion: <br>' +

              '</div>' +


              '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 6px; padding-left: 20px; margin-top: 15px; display: none" id="divDiagnosticoFotografiaExtra">' +
              '<br><br>Fondo: <br>' +

              '</div> ' + 
              '<div style="width: 100%; display: none; " id="obserfotoextraoral"><br>' +
              '<br>Observaciones<br>' +
              '<textarea class="form-control" id="observacionesfotoextraoral"></textarea><br>' +
              '</div>' +
              '</div>')   

            $("#menu" + grupo).append('<div class="col-md-12" style=""><br>' +
              '<button type="button" id="btnAddFoto" class="btn btn-primary pull-right" style="display: none; margin-bottom:10px">Agregar</button>' +
              '</div>')          

            for(i = 0; i < json.length; i++){
              if(parseInt(json[i].id) < 10){
                $("#selintraoral").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>") 
              }else{
                $("#selextraoral").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>") 
              }
            }

            $("#selintraoral").click(function(e){
              $("#obserfotointraoral").css({"display":"block"})
              $("#obserfotoextraoral").css({"display":"none"})
              $("#btnAddFoto").css({"display":"block"})
              $("#divPresentacionFotografiaExtra").css({"display":"none"})
              $("#divPresentacionFotografiaExtra").html("")
              $("#divDiagnosticoFotografiaExtra").css({"display":"none"})
              $("#divDiagnosticoFotografiaExtra").html("")      
            });

            $("#selextraoral").click(function(e){
              $("#divPresentacionFotografiaExtra").css({"display":"block"})
              $("#divPresentacionFotografiaExtra").html("<br><b>Presentacion: </b><br>")
              $("#obserfotointraoral").css({"display":"none"})
              $("#obserfotoextraoral").css({"display":"none"})
              $("#btnAddFoto").css({"display":"none"})
              $("#divDiagnosticoFotografiaExtra").css({"display":"none"})
              $("#divDiagnosticoFotografiaExtra").html("") 

              var idtipo = $("#selextraoral").val();

                //Se guardan los datos en un JSON
                var datos = {
                  id_tipo: idtipo
                }    
                $.post("<?= base_url();?>index.php/procedimientos/listarPresentacion", datos)
                .done(function( result ) {//console.log(result) 
                  if($.trim(result) != "[]"){
                    var json = JSON.parse(result)
                    for (var i = 0; i < json.length; i++) {
                      $("#divPresentacionFotografiaExtra").append('<div class="radio" style="border: none; background: transparent;">' +
                        '<label><input type="radio" name="radioPresentacionFoto" id="PreseFoto' + i + '" value="' + json[i].id + '" style="margin-top: 0.7px;"><font>' + json[i].nombre + '</font></label>' +
                        '</div>') 

                      $("#PreseFoto" + i).click(function () {    
                        var idSelec = $('input:radio[name=radioPresentacionFoto]:checked').val()
                        $("#divDiagnosticoFotografiaExtra").css({"display":"block"})
                        $("#divDiagnosticoFotografiaExtra").html("<br><b>Fondo: </b><br>")

                        var datos = {
                          id_presentacion: idSelec
                        }   
                        $.post("<?= base_url();?>index.php/procedimientos/listarDiagnosticos", datos)
                        .done(function( resultado ) {console.log(resultado) 

                          if($.trim(result) != "[]"){
                            var jsonDiag = JSON.parse(result)
                            for (var j = 0; j < jsonDiag.length; j++) {
                              $("#divDiagnosticoFotografiaExtra").append('<div class="radio" style="border: none; background: transparent;">' +
                                '<label><input type="radio" name="radioPDiagnosticoFoto" id="DiagFoto' + j + '" value="' + jsonDiag[j].id + '" style="margin-top: 0.7px;"><font>' + jsonDiag[j].nombre + '</font></label>' +
                                '</div>') 

                              $("#DiagFoto" + j).click(function(e){
                                $("#btnAddFoto").css({"display":"block"})
                                $("#obserfotoextraoral").css({"display":"block"})
                                $("#obserfotointraoral").css({"display":"none"})
                              });
                            }
                            

                            
                          }else{

                          }
                        });

                      });
                    }
                  }else{

                  }
                });
              });  

            $("#btnAddFoto").click(function(e){
              if($.trim($("#obserfotointraoral").css("display")) == "block"){
                var tipo = $('#selintraoral option:selected').text()
                var idtipo = $('#selintraoral').val()
                var observacion = $("#observacionesfotointraoral").val()

                var contadorFoto;
                if($("#target > tbody > tr").length > 0){
                  contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
                }else{
                  contadorFoto = 0;
                }

                $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Fotografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/>' + tipo + '</td><td>&nbsp;</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

                $("#observacionesfotointraoral").val("")
                $("#observacionesfotoextraoral").val("")
                $("#obserfotointraoral").css({"display":"none"})
                $("#obserfotoextraoral").css({"display":"none"})
                $("#btnAddFoto").css({"display":"none"})
                $("#selintraoral").val($("#selintraoral option:first").val())
                console.log($("#selintraoral option:first").val())
              }else{
                var tipo = $('#selextraoral option:selected').text()
                var idtipo = $('#selextraoral').val()

                var presentacion = $('input:radio[name=radioPresentacionFoto]:checked').val()
                var textpresentacion = $('input:radio[name=radioPresentacionFoto]:checked').parent()[0].children[1].innerHTML

                var idfondo = $('input:radio[name=radioPDiagnosticoFoto]:checked').val()
                var fondo = $('input:radio[name=radioPDiagnosticoFoto]:checked').parent()[0].children[1].innerHTML

                var observacion = $("#observacionesfotoextraoral").val()
                var contadorFoto;
                if($("#target > tbody > tr").length > 0){
                  contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
                }else{
                  contadorFoto = 0;
                }

                $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Fotografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + presentacion + '"/><input type="hidden" id="targetfondo' + contadorFoto + '" value="' + idfondo + '"/>' + tipo + '</td><td>Presentacion: ' + textpresentacion + ' Fondo: ' + fondo + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

                $("#divPresentacionFotografiaExtra").css({"display":"none"})
                $("#observacionesfotointraoral").val("")
                $("#observacionesfotoextraoral").val("")
                $("#divPresentacionFotografiaExtra").html("")
                $("#obserfotointraoral").css({"display":"none"})
                $("#obserfotoextraoral").css({"display":"none"})
                $("#btnAddFoto").css({"display":"none"})
                $("#divDiagnosticoFotografiaExtra").css({"display":"none"})
                $("#divDiagnosticoFotografiaExtra").html("") 
                $("#selextraoral").val($("#selextraoral option:first").val())
              }

              if($.trim($("#txtProfesionalHidden").val()) != ""){
                $("#btnAddProFactura").removeAttr("disabled");
              }else{
                if($("#selectProfesionalProcedimiento > option").length > 0 && $("#target > tbody > tr").length > 0){
                  $("#btnAddProFactura").removeAttr("disabled");
                }else{
                  $("#btnAddProFactura").attr("disabled", "disabled");
                }                  
              }

              $("#borrarItem" + contadorFoto).click(function(e) {
                var confirmar = window.confirm("¿Desea borrar esta fila?")
                if(confirmar){
                  $(this).parent().parent().remove()
                  if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
                    $("#btnAddProFactura").removeAttr("disabled");
                  }else{
                    $("#btnAddProFactura").attr("disabled", "disabled");
                  }
                }
              })
            })
break;

case "2":
var dientesSuperIz = ''
var dientesInferiorIz = ''
dientesSuperIz += '<div style="width: 45px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;">' +
'<font style="color: gray;">Sup.DER</font>' +
'</div>';

dientesInferiorIz += '<div style="width: 45px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;">' +
'<font style="color: gray;">Inf.DER</font>' +
'</div>';

for(i = 8; i >= 1; i--){
  dientesSuperIz += '<div style="width: 7px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;cursor: pointer; background-color: #FFFFFF" class="dientes" valor="Superior Der ' + i + '">' +
  i +
  '</div>';

  dientesInferiorIz += '<div style="width: 7px;height: 15px;margin-right: 0px;float: inherit;border-top: 1px solid black;font-size: 10px;cursor: pointer; background-color: #FFFFFF" class="dientes" valor="Inferior Der ' + i + '">' +
  i +
  '</div>';
}
dientesSuperIz += '<div style="width: 3px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;" align="center">' +
'|' +
'</div>';

dientesInferiorIz += '<div style="width: 3px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;" align="center">' +
'|' +
'</div>';
for(i = 1; i <= 8; i++){
  dientesSuperIz += '<div style="width: 7px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;cursor: pointer; background-color: #FFFFFF" class="dientes" valor="Superior Izq ' + i + '">' +
  i +
  '</div>';

  dientesInferiorIz += '<div style="width: 7px;height: 15px;margin-right: 0px;float: inherit;border-top: 1px solid black;font-size: 10px;cursor: pointer; background-color: #FFFFFF" class="dientes" valor="Inferior Izq ' + i + '">' +
  i +
  '</div>';
}
dientesSuperIz += '<div style="width: 45px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;">' +
'<font style="color: gray;">Sup.IZQ</font>' +
'</div>';

dientesInferiorIz += '<div style="width: 45px;height: 15px;margin-right: 0px;float: inherit;font-size: 10px;">' +
'<font style="color: gray;">Inf.IZQ</font>' +
'</div>';

$("#menu" + grupo).html("")
$("#menu" + grupo).append('<div class="col-md-6">' +
  '<select id="selradiografiaintraoral" class="form-control">' +

  '</select>' +
  '<br>' +
  '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 15px; padding-left: 20px; display: none" id="divPresentacionRadiografiaIntra">' +

  '</div>' +
  '<span id="spanPresentacionRadioIntra" style="display: none">Tipo</span>' +
  '<select class="form-control" id="selectPresentacionRadioIntra" style="display: none;">' +

  '</select><br>' +
  '<input type="hidden" id="dientesRadio" name="dientesRadio">' +
  '<div class="col-md-12" style="padding-left: 0px;padding-right: 0px; margin-bottom: 10px; display: none; font-size: 12px" id="divDientesRadio">' +

  dientesSuperIz + '<br>' + dientesInferiorIz +                     

  '</div>' + 

  '<div style="width: 100%; display: none;" id="obserradintraoral">' +
  '<br>Observaciones<br>' +
  '<textarea class="form-control" id="observacionesradintraoral"></textarea><br>' +
  '</div>' +
  '</div>')

$("#menu" + grupo).append('<div class="col-md-6">' +
  '<select id="selecradiografiaextraoral" class="form-control">' +

  '</select>' +
  '<br>' +
  '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 6px; padding-left: 20px; display: none" id="divPresentacionRadiografiaExtra">' +

  '</div>' +


  '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 6px; padding-left: 20px; margin-top: 15px; display: none" id="divDiagnositcoRadiografiaExtra">' +

  '</div>' +

  '<div style="width: 100%; display: none;" id="obserradextraoral">' +
  '<br>Observaciones<br>' +
  '<textarea class="form-control" id="observacionesradextraoral"></textarea><br>' +
  '</div>' +
  '</div>')

$("#menu" + grupo).append('<br><br><br>' +
  '<div class="col-md-12" style="">' +
  '<button type="button" id="btnAddRadiografias" class="btn btn-primary pull-right" style="display: none">Agregar</button>' +
  ' </div>');

for(i = 0; i < json.length; i++){
  if(i == 0){
    $("#selradiografiaintraoral").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>") 
  }else{
    console.log(json[i])
    $("#selecradiografiaextraoral").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>") 
  }
}

$(".dientes").click(function(e){ 
  var valor = $(this).attr("valor")

  switch($(this)[0].style.backgroundColor){
    case "rgb(255, 255, 255)":
    $(this).css({"background-color":"#0683c9", "color":"#FFFFFF"})
    var complemento = valor;     
    arrayDatos[arrayDatos.length] = complemento;
    break;

    case "rgb(6, 131, 201)":
    if(existeDiente(valor) != -1){
      arrayDatos.splice(existeDiente(valor),1);
      $(this).css({"background-color":"rgb(255, 255, 255)", "color":"#000000"})
    }
    break;
  }

  $("#dientesRadio").val(arrayDatos.toString()); 
});

$("#selradiografiaintraoral").click(function(e){
  $("#obserradintraoral").css({"display":"block"})
  $("#obserradextraoral").css({"display":"none"})
  $("#btnAddRadiografias").css({"display":"block"})
  $("#divPresentacionRadiografiaExtra").css({"display":"none"})
  $("#divPresentacionRadiografiaExtra").html("")
  $("#divDiagnositcoRadiografiaExtra").css({"display":"none"})
  $("#divDiagnositcoRadiografiaExtra").html("")
  $("#spanPresentacionRadioIntra").css({"display":"block"})
  $("#selectPresentacionRadioIntra").css({"display":"block"})
  $("#selectPresentacionRadioIntra").html("")
  $("#divDientesRadio").css({"display":"block"})

  var idtipo = $("#selradiografiaintraoral").val();

                  //Se guardan los datos en un JSON
                  var datos = {
                    id_tipo: idtipo
                  }   
                  
                  $.post("<?= base_url();?>index.php/procedimientos/listarPresentacion", datos)
                  .done(function( result ) {//console.log(result) 
                    if($.trim(result) != "[]"){
                      var json = JSON.parse(result)
                      for (var i = 0; i < json.length; i++) {
                        $("#selectPresentacionRadioIntra").append('<option value="' + json[i].id + '">' + json[i].nombre + '</option>')    
                      }
                    }
                  });
                  
                });

$("#selecradiografiaextraoral").click(function(e){
  $("#obserradintraoral").css({"display":"none"})
  $("#obserradextraoral").css({"display":"block"})
  $("#btnAddRadiografias").css({"display":"none"})
  $("#divPresentacionRadiografiaExtra").css({"display":"block"})
  $("#divPresentacionRadiografiaExtra").html("<br><b>Presentacion: </b><br>")
  $("#spanPresentacionRadioIntra").css({"display":"none"})
  $("#selectPresentacionRadioIntra").css({"display":"none"})
  $("#selectPresentacionRadioIntra").html("")
  $("#divDientesRadio").css({"display":"none"})
  var idtipo = $("#selecradiografiaextraoral").val();

                  //Se guardan los datos en un JSON
                  
                  var datos = {
                    id_tipo: idtipo
                  }    
                  $.post("<?= base_url();?>index.php/procedimientos/listarPresentacion", datos)
                  .done(function( result ) {//console.log(result) 
                    if($.trim(result) != "[]"){
                      var json = JSON.parse(result)
                      for (var i = 0; i < json.length; i++) {
                        $("#divPresentacionRadiografiaExtra").append('<div class="radio" style="border: none; background: transparent;">' +
                          '<label><input type="radio" name="radioPresentacionDiag" id="RadiagFoto' + i + '" value="' + json[i].id + '" style="margin-top: 4px;"><font>' + json[i].nombre + '</font></label>' +
                          '</div>') 

                        $("#RadiagFoto" + i).click(function () {    
                          var idSelec = $('input:radio[name=radioPresentacionDiag]:checked').val()
                          $("#divDiagnositcoRadiografiaExtra").css({"display":"block"})
                          $("#divDiagnositcoRadiografiaExtra").html("<br><b>Diagnostico: </b><br>")

                          var datos = {
                            id_presentacion: idSelec
                          }   
                          $.post("<?= base_url();?>index.php/procedimientos/listarDiagnosticos", datos)
                          .done(function( resultado ) {console.log(resultado) 

                            if($.trim(resultado) != "[]"){
                              var jsonDiag = JSON.parse(resultado)
                              for (var j = 0; j < jsonDiag.length; j++) {
                                $("#divDiagnositcoRadiografiaExtra").append('<div class="checkbox" style="border: none; background: transparent;">' +
                                  '<label><input type="checkbox" name="checkboxDiagnosticoRadio[]" id="DiagRadio' + j + '" value="' + jsonDiag[j].id + '" style="margin-top: 4px;"><font>' + jsonDiag[j].nombre + '</font></label>' +
                                  '</div>') 

                                
                              }
                              
                              
                              
                            }else{
                              $("#divDiagnositcoRadiografiaExtra").html("<br><b>Diagnostico: </b><br>")

                            }
                            $("#obserradintraoral").css({"display":"none"})
                            $("#obserradextraoral").css({"display":"block"})
                            $("#btnAddRadiografias").css({"display":"block"})
                          });

                        });
                      }
                    }else{

                    }
                  });  
                })

$("#btnAddRadiografias").click(function(e){
  var contadorFoto;
  if($("#target > tbody > tr").length > 0){
    contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
  }else{
    contadorFoto = 0;
  }

  if($.trim($("#obserradintraoral").css("display")) == "block"){
    var tipo = $('#selradiografiaintraoral option:selected').text()
    var idtipo = $('#selradiografiaintraoral').val()
    var observacion = $("#observacionesradintraoral").val()
    var presentacion = $('#selectPresentacionRadioIntra').val()
    var textpresentacion = $('#selectPresentacionRadioIntra option:selected').text()
    var dientes = $("#dientesRadio").val()



    $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Radiografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + presentacion + '"/>' + tipo + '</td><td>' + dientes + ", " + textpresentacion + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

    arrayDatos.splice(0,arrayDatos.length);
    $("#observacionesradintraoral").val("")
    $("#observacionesradextraoral").val("")
    $("#dientesRadio").val("")
    $(".dientes").css({"background-color":"#FFFFFF", "color":"#000000"})
    $("#obserradintraoral").css({"display":"none"})
    $("#obserradextraoral").css({"display":"none"})
    $("#spanPresentacionRadioIntra").css({"display":"none"})
    $("#selectPresentacionRadioIntra").css({"display":"none"})
    $("#divDientesRadio").css({"display":"none"})
    $("#btnAddRadiografias").css({"display":"none"})
    $("#selradiografiaintraoral").val($("#selradiografiaintraoral option:first").val())
  }else{
    var tipo = $('#selecradiografiaextraoral option:selected').text()
    var idtipo = $('#selecradiografiaextraoral').val()

    var presentacion = $('input:radio[name=radioPresentacionDiag]:checked').val()
    var textpresentacion = $('input:radio[name=radioPresentacionDiag]:checked').parent()[0].children[1].innerHTML

    var iddiagnostico = "";
    var diagnostico = "";
    var con = 0;
    $('input[name="checkboxDiagnosticoRadio[]"]:checked').each(function() {
                      //checkboxValues.push($(this).val());
                      if(con == 0){
                        iddiagnostico += "" + $(this).val();
                        diagnostico += $(this).parent()[0].children[1].innerHTML
                      }else{
                        iddiagnostico += "," + $(this).val();
                        diagnostico += "," + $(this).parent()[0].children[1].innerHTML
                      }
                      con++;
                    });

    var observacion = $("#observacionesradextraoral").val()
    console.log(iddiagnostico)

    $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Radiografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + presentacion + '"/><input type="hidden" id="targetfondo' + contadorFoto + '" value="' + iddiagnostico + '"/>' + tipo + '</td><td>Presentacion: ' + textpresentacion + ' Diagnostico: ' + diagnostico + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

    $("#divPresentacionRadiografiaExtra").css({"display":"none"})
    $("#observacionesradintraoral").val("")
    $("#observacionesradextraoral").val("")
    $("#divPresentacionRadiografiaExtra").html("")
    $("#obserradintraoral").css({"display":"none"})
    $("#obserradextraoral").css({"display":"none"})
    $("#btnAddRadiografias").css({"display":"none"})
    $("#divDiagnositcoRadiografiaExtra").css({"display":"none"})
    $("#divDiagnositcoRadiografiaExtra").html("") 
    $("#selecradiografiaextraoral").val($("#selecradiografiaextraoral option:first").val())
    $('input[name="checkboxDiagnosticoRadio[]"]').removeAttr("checked")
  }

  if($.trim($("#txtProfesionalHidden").val()) != ""){
    $("#btnAddProFactura").removeAttr("disabled");
  }else{
    if($("#selectProfesionalProcedimiento > option").length > 0 && $("#target > tbody > tr").length > 0){
      $("#btnAddProFactura").removeAttr("disabled");
    }else{
      $("#btnAddProFactura").attr("disabled", "disabled");
    }                  
  }

  console.log(contadorFoto)
  $("#borrarItem" + contadorFoto).click(function(e) {
    console.log(contadorFoto + " DELETE")
    var confirmar = window.confirm("¿Desea borrar esta fila?")
    if(confirmar){
      $(this).parent().parent().remove()

      if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
        $("#btnAddProFactura").removeAttr("disabled");
      }else{
        $("#btnAddProFactura").attr("disabled", "disabled");
      }
    }
  })
});

break;

case "3":
$("#menu" + grupo).html("");
var html_modelos = "";

for(var i = 0; i < json.length; i++){
  html_modelos += '<label class="radio-inline" style="width: 48%"><input type="radio" name="radioModelos" value="' + json[i].id + '"><font>' + json[i].nombre + '</font></label>'
}
                      

$("#menu" + grupo).append('<div style="background-color: #efefef;border-radius: 4px;padding-left: 20px; padding-top: 4px; padding-bottom: 4px">' + html_modelos + '</div>') 
$("#menu" + grupo).append('<br>' +
  '<div class="col-md-12" id="divPresentacionModelo" style="background-color: rgb(239, 239, 239); border-radius: 4px; display: block;">   ' +
  '</div><br><br>' +
  '<div class="col-md-12" id="divTipo" style="background-color: rgb(239, 239, 239); border-radius: 4px; display: block;margin-top: 10px;">   ' +
  '</div>' +
  '<br>' +
  '<div class="col-md-12" id="divDiagModelos" style="background-color: rgb(239, 239, 239); border-radius: 4px; display: block;margin-top: 10px;">' +
  '</div>' +

  '<br><br>' +
  '<div style="width: 100%; display: none; margin-top: 10px" id="obsermodelosdiv">' +
  '<br>Observaciones<br>' +
  '<textarea class="form-control" id="observacionmodelos"></textarea><br>' +
  '</div>' +

  '<div class="col-md-12" style="">' +
  '<button type="button" id="btnAddModelos" class="btn btn-primary pull-right" style="display: none">Agregar</button>' +
  '</div>')

$('input:radio[name=radioModelos]').change(function(e){

  $("#btnAddModelos").css({"display":"none"})
  $("#divPresentacionModelo").html("")
  $("#divTipo").html("")
  $("#divDiagModelos").html("")
  
  
  $("#divPresentacionModelo").css({"display":"block"})
  
  var idModelo = $('input:radio[name=radioModelos]:checked').val()
  var datos = {
    id_tipo: idModelo
  }    
  $.post("<?= base_url();?>index.php/procedimientos/listarPresentacion", datos)
  .done(function( data ) {console.log(data)             
    if($.trim(data) != "[]"){
      var json = JSON.parse(data)
      for (var i = 0; i < json.length; i++) {
        if(json[i].id != "44"){
          $("#divPresentacionModelo").append('<div class="radio" style="border: none; background: transparent;">' +
          '<label><input type="radio" name="radioPresentacionModelo" id="radioPresentacionModelo' + i + '" value="' + json[i].id + '"><font>' + json[i].nombre + '</font></label>' +
        '</div>') 
        }

        $("#radioPresentacionModelo" + i).click(function () {    
          var idSelec = $('input:radio[name=radioPresentacionModelo]:checked').val()
          $("#divTipo").css({"display":"block"})
          $("#divTipo").html("Tipo de Yeso:")

          var datos = {
            id_presentacion: idSelec
          }   
          $.post("<?= base_url();?>index.php/procedimientos/listarDiagnosticos", datos)
          .done(function( result ) {console.log(result) 
            $("#obsermodelosdiv").css({"display":"block"})
            if($.trim(result) != "[]"){
              var jsonDiag = JSON.parse(result)
              for (var j = 0; j < jsonDiag.length; j++) {
                $("#divTipo").append('<div class="radio" style="border: none; background: transparent;">' +
                '<label><input type="radio" name="checkboxTipoYeso" id="checkboxTipoYeso' + j + '" value="' + jsonDiag[j].id + '"><font>' + jsonDiag[j].nombre + '</font></label>' +
              '</div>') 

                $("#checkboxTipoYeso" + j).click(function () {    
                  $("#btnAddModelos").css({"display":"block"})
                });
              }
              
            }else{
              
            }
          });
                 
        });   
      }
      
    }else{
    }
    
     
  });

   
  if(idModelo == "18"){
        $("#divDiagModelos").css({"display":"block"})
        $("#divDiagModelos").html("Diagnóstico:")
        //Se guardan los datos en un JSON
        var datos = {
          id_presentacion: 44
        }   
        
        $.post("<?= base_url();?>index.php/procedimientos/listarDiagnosticos", datos)
        .done(function( data ) {console.log(data)             
          if($.trim(data) != "[]"){
            var json = JSON.parse(data)
            for (var i = 0; i < json.length; i++) {
              $("#divDiagModelos").append('<div class="checkbox" style="border: none; background: transparent;">' +
                '<label><input type="checkbox" name="checkboxDiagModelos[]" id="checkboxDiagModelos' + i + '" value="' + json[i].id + '"><font>' + json[i].nombre + '</font></label>' +
              '</div>')             
            }
            
          }else{
          }
          
           
        });
      }else{

      }
  })

$("#btnAddModelos").click(function(e){
  var tipo = $('input:radio[name=radioModelos]:checked').parent()[0].children[1].innerHTML
  var idtipo = $('input:radio[name=radioModelos]:checked').val()
  var idPresentacion = $('input:radio[name=radioPresentacionModelo]:checked').val()
  var presentacion = $('input:radio[name=radioPresentacionModelo]:checked').parent()[0].children[1].innerHTML
  var idYeso = $('input:radio[name=checkboxTipoYeso]:checked').val()
  var Yeso = $('input:radio[name=checkboxTipoYeso]:checked').parent()[0].children[1].innerHTML
  var observacion = $("#observacionmodelos").val()
 
  var iddiagnostico = "";
  var diagnostico = "";
  var con = 0;
  var contadorFoto;
  if($("#target > tbody > tr").length > 0){
    contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
  }else{
    contadorFoto = 0;
  }

  $('input[name="checkboxDiagModelos[]"]:checked').each(function(index) {
    if(con == 0){
      iddiagnostico += "" + $(this).val();
      diagnostico += $(this).parent()[0].children[1].innerHTML
    }else{
      iddiagnostico += "," + $(this).val();
      diagnostico += "," + $(this).parent()[0].children[1].innerHTML
    }
    con++;
  });
  if($.trim(iddiagnostico) != ""){
    iddiagnostico += "," + idYeso;
  }else{
    iddiagnostico += idYeso;
  }
  
  if($('input[name="checkboxDiagModelos[]"]').length != 0){
    $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Modelos</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + idPresentacion + '"/><input type="hidden" id="targetfondo' + contadorFoto + '" value="' + iddiagnostico + '"/>' + tipo + '</td><td>Presentación: ' + presentacion + ' Tipo de Yeso: ' + Yeso + ' Diagnostico: ' + diagnostico + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrar' + contadorFoto + '">X</button></td></tr>');
  }else{
    $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Modelos</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + idPresentacion + '"/><input type="hidden" id="targetfondo' + contadorFoto + '" value="' + iddiagnostico + '"/>' + tipo + '</td><td>Presentación: ' + presentacion + ' Tipo de Yeso: ' + Yeso + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrar' + contadorFoto + '">X</button></td></tr>');
  }
  

  $("#observacionmodelos").val("")
  $("#obsermodelosdiv").css({"display":"none"})
  $("#divPresentacionModelo").css({"display":"none"})
  $("#divTipo").css({"display":"none"})
  $("#divDiagModelos").css({"display":"none"})
  $("#divTipo").css({"display":"none"})
  $("#btnAddModelos").css({"display":"none"})
  $("#btnGenerarOrden").css({"display":"block"})
  $('input[name="checkboxDiagModelos"]').removeAttr("checked")
  $("input[name=radioModelos]").prop('checked', false);

  $("#borrar" + contadorFoto).click(function(e) {
    var confirmar = window.confirm("¿Desea borrar esta fila?")
    if(confirmar){
      $(this).parent().parent().remove()
    }
  })
});
break;

case "4":
var dientesSuper = ''
var dientesInferior = ''
dientesSuper += '<div style="width: 60px;height: 20px;margin-right: 5px;float: inherit;" align="left">' +
'<b><font style="color: gray;">Sup.DER</font></b>' +
'</div>';

dientesInferior += '<div style="width: 60px;height: 20px;margin-right: 5px;float: inherit;" align="left">' +
'<b><font style="color: gray;">Inf.DER</font></b>' +
'</div> ';

for(i = 18; i >= 11; i--){
  dientesSuper += '<div style="width: 18px;height: 20px;margin-right: 0px;float: inherit;cursor: pointer; background-color: #FFFFFF" class="dientetomografia superior" valor="Superior Der ' + i + '" align="center">' +
  i +
  '</div>';
}

for(i = 48; i >= 41; i--){
  dientesInferior += '<div style="width: 18px;height: 20px;margin-right: 0px;float: inherit;border-top: 1px solid black;cursor: pointer; background-color: #FFFFFF" class="dientetomografia inferior" valor="Inferior Der ' + i + '" align="center">' +
  i +
  '</div>';
}

dientesSuper += '<div style="width: 5px;height: 20px;margin-right: 0px;float: inherit;font-size: 12px;" align="center">' +
'|' +
'</div>';

dientesInferior += '<div style="width: 5px;height: 20px;margin-right: 0px;float: inherit;font-size: 12px;" align="center">' +
'|' +
'</div>';

for(i = 21; i <= 28; i++){
  dientesSuper += '<div style="width: 18px;height: 20px;margin-right: 0px;float: inherit;cursor: pointer; background-color: #FFFFFF" class="dientetomografia superior" valor="Superior Izq ' + i + '" align="center">' +
  i +
  '</div>';
}

for(i = 31; i <= 38; i++){
  dientesInferior += '<div style="width: 18px;height: 20px;margin-right: 0px;float: inherit;border-top: 1px solid black;cursor: pointer; background-color: #FFFFFF" class="dientetomografia inferior" valor="Inferior Izq ' + i + '" align="center">' +
  i +
  '</div>';
}
dientesSuper += '<div style="width: 60px;height: 20px;margin-right: 5px;float: inherit;" align="right">' +
'<b><font style="color: gray;">Sup.IZQ</font></b>' +
'</div>';

dientesInferior += '<div style="width: 60px;height: 20px;margin-right: 5px;float: inherit;" align="right">' +
'<b><font style="color: gray;">Inf.IZQ</font></b>' +
'</div>';

$("#menu" + grupo).html("")
$("#menu" + grupo).append('<div class="col-md-12">' +
  'Tipo de Tomografia' +
  '<select id="tipotomografia" class="form-control">' +
  '</select></br>' +

  '<input type="hidden" id="dientestomografia" name="dientestomografia">' +
  '<div class="col-md-12" style="padding: 0px; margin: 0px">' +
  dientesSuper + '<br>' + dientesInferior +  
  '</div></br></br>' +



  '<div class="col-md-12" id="divPresentacionTomo">' + 

  '</div>' +

  '<br><br>' +
  '<div style="width: 100%;" id="obsertomografiasdiv">' +
  '<br>Observaciones<br>' +
  '<textarea class="form-control" id="observaciontomografias"></textarea><br>' +
  '</div>' +
  '</div>' +
  '<br><br><br>' +
  '<div class="col-md-12" style="">' +
  '<button type="button" id="btnAddTomografia" class="btn btn-primary pull-right">Agregar</button>' +
  '</div>');

for(i = 0; i < json.length; i++){
  $("#tipotomografia").append("<option value='" + json[i].id + "'>" + json[i].nombre + "</option>") 
}
listarPresentacionTomo(json[0].id)

$("#tipotomografia").click(function(e){
  var idtipo = $("#tipotomografia").val();
  listarPresentacionTomo(idtipo)
});

$(".dientetomografia").click(function(e){ 
  var valor = $(this).attr("valor")
  var disabled = $(this).attr("disabled")

  var tomografias = $('#tipotomografia option:selected').text();


  if(disabled != "disabled" && disabled == null){
    console.log(existeDienteTomo(valor))

    switch(tomografias){
      case "Zona":
      switch($(this)[0].style.backgroundColor){
        case "rgb(255, 255, 255)":
        $(this).css({"background-color":"#0683c9", "color":"#FFFFFF"})
        var complemento = valor;     
        arrayDatosTomo[arrayDatosTomo.length] = complemento;
        break;

        case "rgb(6, 131, 201)":
        if(existeDienteTomo(valor) != -1){
          arrayDatosTomo.splice(existeDienteTomo(valor),1);
          $(this).css({"background-color":"rgb(255, 255, 255)", "color":"#000000"})
        }
        break;
      }
      break;
      case "Maxilar Inferior":

      break;
      case "Zona + Maxilar Inferior":
      switch($(this)[0].style.backgroundColor){
        case "rgb(255, 255, 255)":
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $("#dientestomografia").val("")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        $(this).css({"background-color":"#0683c9", "color":"#FFFFFF"})
        var complemento = valor;     
        arrayDatosTomo[arrayDatosTomo.length] = complemento;
        break;

        case "rgb(6, 131, 201)":
        if(existeDienteTomo(valor) != -1){
          arrayDatosTomo.splice(0,arrayDatosTomo.length);
          $("#dientestomografia").val("")
          $(this).css({"background-color":"rgb(255, 255, 255)", "color":"#000000"})
        }
        break;
      }
      break;
      case "Maxilar Superior":            

      break;
      case "Zona + Maxilar Superior":
      switch($(this)[0].style.backgroundColor){
        case "rgb(255, 255, 255)":
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $("#dientestomografia").val("")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        $(this).css({"background-color":"#0683c9", "color":"#FFFFFF"})
        var complemento = valor;     
        arrayDatosTomo[arrayDatosTomo.length] = complemento;
        break;

        case "rgb(6, 131, 201)":
        if(existeDienteTomo(valor) != -1){
          arrayDatosTomo.splice(0,arrayDatosTomo.length);
          $("#dientestomografia").val("")
          $(this).css({"background-color":"rgb(255, 255, 255)", "color":"#000000"})
        }
        break;
      }
      break;
      case "Bimaxilar":

      break;
      case "Zona + Bimaxilar":
      switch($(this)[0].style.backgroundColor){
        case "rgb(255, 255, 255)":
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $("#dientestomografia").val("")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        $(this).css({"background-color":"#0683c9", "color":"#FFFFFF"})
        var complemento = valor;     
        arrayDatosTomo[arrayDatosTomo.length] = complemento;
        break;

        case "rgb(6, 131, 201)":
        if(existeDienteTomo(valor) != -1){
          arrayDatosTomo.splice(0,arrayDatosTomo.length);
          $("#dientestomografia").val("")
          $(this).css({"background-color":"rgb(255, 255, 255)", "color":"#000000"})
        }
        break;
      }
      break;
      case "Senos Paranasales":

      break;
      case "ATM Condilo 3D":

      break;
      case "Cara Completa":

      break;
      case "Guia Quirurgica":

      break;
      case "Tomografia Terceros Molares":

      break;
    }
    $("#dientestomografia").val(arrayDatosTomo.toString()); 
  }


});

$("#btnAddTomografia").click(function(e){
  var tipo = $('#tipotomografia option:selected').text()
  var idtipo = $('#tipotomografia').val()
  var observacion = $("#observaciontomografias").val()
  var dientes = $("#dientestomografia").val(); 

  var idpresentacion = "";
  var presentacion = "";
  var con = 0;

  if(tipo == "Zona" || tipo == "Zona + Maxilar Inferior" || tipo == "Zona + Maxilar Superior" || tipo == "Zona + Bimaxilar"){

    if(dientes != ""){
      $('input[name="checkboxPresentacionTomo[]"]:checked').each(function() {
                          //checkboxValues.push($(this).val());
                          if(con == 0){
                            idpresentacion += "" + $(this).val();
                            presentacion += $(this).parent()[0].children[1].innerHTML
                          }else{
                            idpresentacion += "," + $(this).val();
                            presentacion += "," + $(this).parent()[0].children[1].innerHTML
                          }
                          con++;
                        });

      var contadorFoto;
      if($("#target > tbody > tr").length > 0){
        contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
      }else{
        contadorFoto = 0;
      }
      $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Tomografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + idpresentacion + '"/>' + tipo + ' Caracteristicas: ' + presentacion + '</td><td>' + dientes + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

      $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
      $(".dientetomografia").removeAttr("disabled")
      $("#dientestomografia").val("")
      $("#observaciontomografias").val("")
      arrayDatosTomo.splice(0,arrayDatosTomo.length);
                        //$("#btnAddTomografia").css({"display":"none"})
                        $("#tipotomografia").val($("#tipotomografia option:first").val())
                        $('input[name="checkboxPresentacionTomo[]"]').removeAttr("checked")

                        if($.trim($("#txtProfesionalHidden").val()) != ""){
                          $("#btnAddProFactura").removeAttr("disabled");
                        }else{
                          if($("#selectProfesionalProcedimiento > option").length > 0 && $("#target > tbody > tr").length > 0){
                            $("#btnAddProFactura").removeAttr("disabled");
                          }else{
                            $("#btnAddProFactura").attr("disabled", "disabled");
                          }                  
                        }
                        console.log(contadorFoto + " : ID")
                        $("#borrarItem" + contadorFoto).click(function(e) {
                          var confirmar = window.confirm("¿Desea borrar esta fila?")
                          if(confirmar){
                            $(this).parent().parent().remove()
                            if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
                              $("#btnAddProFactura").removeAttr("disabled");
                            }else{
                              $("#btnAddProFactura").attr("disabled", "disabled");
                            }
                          }
                        })
                      }else{
                        alert("Sebe seleccionar al menos un diente en la Tomografia " + tipo)
                      }

                    }else{

                      $('input[name="checkboxPresentacionTomo[]"]:checked').each(function() {
                        //checkboxValues.push($(this).val());
                        if(con == 0){
                          idpresentacion += "" + $(this).val();
                          presentacion += $(this).parent()[0].children[1].innerHTML
                        }else{
                          idpresentacion += "," + $(this).val();
                          presentacion += "," + $(this).parent()[0].children[1].innerHTML
                        }
                        con++;
                      });

                      var contadorFoto;
                      if($("#target > tbody > tr").length > 0){
                        contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
                      }else{
                        contadorFoto = 0;
                      }

                      $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Tomografias</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/><input type="hidden" id="targetpresentacion' + contadorFoto + '" value="' + idpresentacion + '"/>' + tipo + ' Caracteristicas: ' + presentacion + '</td><td>' + dientes + '</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');
                      
                      $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
                      $(".dientetomografia").removeAttr("disabled")
                      $("#observaciontomografias").val("")
                      $("#dientestomografia").val("")
                      arrayDatosTomo.splice(0,arrayDatosTomo.length);
                      //$("#btnAddTomografia").css({"display":"none"})
                      $("#tipotomografia").val($("#tipotomografia option:first").val())
                      $('input[name="checkboxPresentacionTomo[]"]').removeAttr("checked")

                      if($.trim($("#txtProfesionalHidden").val()) != ""){
                        $("#btnAddProFactura").removeAttr("disabled");
                      }else{
                        if($("#selectProfesionalProcedimiento > option").length > 0 && $("#target > tbody > tr").length > 0){
                          $("#btnAddProFactura").removeAttr("disabled");
                        }else{
                          $("#btnAddProFactura").attr("disabled", "disabled");
                        }                  
                      }

                      $("#borrarItem" + contadorFoto).click(function(e) {
                        var confirmar = window.confirm("¿Desea borrar esta fila?")
                        if(confirmar){
                          $(this).parent().parent().remove()
                          if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
                            $("#btnAddProFactura").removeAttr("disabled");
                          }else{
                            $("#btnAddProFactura").attr("disabled", "disabled");
                          }
                        }
                      })

                    }
                    
                  });
break;

case "5":
$("#menu" + grupo).html("")
$("#menu" + grupo).append('<div class="col-md-12">' +
  'Paquete Diagnostico' +
  '<select id="paquetesdiagnosticos" class="form-control">' +
  '</select>' +
  '</div>' +

  '<div class="col-md-12" style="background-color: rgb(239, 239, 239); border-radius: 15px; padding-left: 20px; margin-top: 15px; font-size: 12px;" id="divPaqueDiagnostico">' +

  '</div>' +

  '<div style="width: 100%; margin-top: 45px" id="obserPaqueteDiag">' +
  '<br>Observaciones<br>' +
  '<textarea class="form-control" id="observacionesPaqueteDiag"></textarea><br>' +
  '</div>' +

  '<div class="col-md-12" style="margin-top: 15px">' +
  '<button type="button" id="btnAddPaqueteDiag" class="btn btn-primary pull-right">Agregar</button>' +
  '</div>');

for(i = 0; i < json.length; i++){
  $("#paquetesdiagnosticos").append("<option value='" + json[i].id + "' desc='" + json[i].descripcion + "'>" + json[i].nombre + "</option>") 
}
$("#divPaqueDiagnostico").html(json[0].descripcion)
$("#paquetesdiagnosticos").change(function(e){
  $("#divPresentacionFotografiaExtra").html("")
  var texto = $("#paquetesdiagnosticos option:selected").attr("desc")
  $("#divPaqueDiagnostico").html(texto)
  console.log(texto)
  $("#obserPaqueteDiag").css({"display":"block"})
  $("#btnAddPaqueteDiag").css({"display":"block"})
});

$("#btnAddPaqueteDiag").click(function(e){      
  var tipo = $('#paquetesdiagnosticos option:selected').text()
  var idtipo = $('#paquetesdiagnosticos').val()
  var observacion = $("#observacionesPaqueteDiag").val()

  var contadorFoto;
  if($("#target > tbody > tr").length > 0){
    contadorFoto = parseInt($("#target > tbody > tr:last").attr("index")) + 1;
  }else{
    contadorFoto = 0;
  }
  $("#target > tbody").append('<tr index="' + contadorFoto + '"><td>Paquete Diagnostico</td><td><input type="hidden" id="targettipo' + contadorFoto + '" value="' + idtipo + '"/><input type="hidden" id="txtGrupo' + contadorFoto + '" value="' + grupo + '"/>' + tipo + '</td><td>&nbsp;</td><td>' + observacion + '</td><td><button class="btn btn-danger" id="borrarItem' + contadorFoto + '">X</button></td></tr>');

  $("#observacionesPaqueteDiag").val("")
  $("#obserPaqueteDiag").css({"display":"block"})
  $("#paquetesdiagnosticos").val($("#paquetesdiagnosticos option:first").val())  

  if($.trim($("#txtProfesionalHidden").val()) != ""){
    $("#btnAddProFactura").removeAttr("disabled");
  }else{
    if($("#selectProfesionalProcedimiento > option").length > 0 && $("#target > tbody > tr").length > 0){
      $("#btnAddProFactura").removeAttr("disabled");
    }else{
      $("#btnAddProFactura").attr("disabled", "disabled");
    }                  
  }

  $("#borrarItem" + contadorFoto).click(function(e) {
    var confirmar = window.confirm("¿Desea borrar esta fila?")
    if(confirmar){
      $(this).parent().parent().remove()
      if($("#target > tbody > tr").length > 0 && $("#selectProfesionalProcedimiento > option").length > 0){
        $("#btnAddProFactura").removeAttr("disabled");
      }else{
        $("#btnAddProFactura").attr("disabled", "disabled");
      }
    }
  })  
})
break;
}

}  
});
}

$("#btnAddProFactura").click(function(e){   
  $("#btnGenerarFactura").attr("tipoFactura", "SINORDENMEDICA")
  var email_profesional = $("#selectProfesionalProcedimiento").val();
  var clinica = $("#selectClinicaProcedimiento").val();

  var json = {
    myrows:[]
  }
  $('#target > tbody > tr').each(function(i) {
    var objH = $(this)[0]
        //Agregan datos al json de la tabla de la orden

        var presentacion = "";
        var grupo = $("#txtGrupo" + i).val();
        var idTipo = $("#targettipo" + i).val();
        var diagnosticos = "";
        var jsonPresentacion = [];
        var jsonDiagnosticos = [];

        if($("#targetpresentacion" + i).length > 0){
          presentacion = $("#targetpresentacion" + i).val()
          var arrayId = presentacion.split(',');
          if($.trim($("#targetpresentacion" + i).val()).length > 0){            
            for (var j = 0; j < arrayId.length; j++) {
              jsonPresentacion[jsonPresentacion.length] = {
                id:arrayId[j]
              }
            }

          }
          
        }

        if($("#targetfondo" + i).length > 0){

          diagnosticos = $("#targetfondo" + i).val()
          var arrayId = diagnosticos.split(',');
          
          if($.trim($("#targettipo" + i).val()).length > 0){
            for (var j = 0; j < arrayId.length; j++) {
              jsonDiagnosticos[jsonDiagnosticos.length] = {
                id:arrayId[j]
              }
            }
          }
          
        }
        var contadorFoto;
        if($("#tbodyOrdenMedica > tr").length > 0){
          contadorFoto = parseInt($("#tbodyOrdenMedica > tr:last").attr("index")) + 1;
        }else{
          contadorFoto = 0;
        }
        var date = new Date();
        var dia = date.getDate();
        var mes = (date.getMonth() + 1);
        var year = date.getFullYear();
        
        if(dia < 10) {
          dia = '0' + dia;
        } 
        
        if(mes < 10) {
          mes = '0' + mes;
        } 
        
        var fechaActual = dia + "/" + mes + "/" + year;
        $("#tbodyOrdenMedica").append("<tr id='tr" + contadorFoto + "' style='background-color: #0683c9; color: #FFFFFF' index = '" + contadorFoto + "'><td>" + fechaActual + "</td><td>" + clinica + "</td><td>" + email_profesional + "</td><td>" + objH.children[0].innerText + "</td><td tipo='" + $("#targettipo" + contadorFoto).val() + "'>" + objH.children[1].innerText + "</td><td>" + objH.children[2].innerText + "</td><td>" + objH.children[3].innerText + "</td><td align='center'><input type='checkbox' name='checkboxAgregar' id='checkboxProcedimiento" + contadorFoto + "' presentacion='" + JSON.stringify(jsonPresentacion) + "' diagnosticos='" + JSON.stringify(jsonDiagnosticos) + "' valor='' idprocejson='" + contadorFoto + "' email_profesional='" + email_profesional + "' idorden='0' checked='checked' disabled></td></tr>") 

        jsonPresentacion = JSON.parse($("#checkboxProcedimiento" + contadorFoto).attr("presentacion") )
        jsonDiagnosticos = JSON.parse($("#checkboxProcedimiento" + contadorFoto).attr("diagnosticos") )
        //Se guardan los datos en un JSON
        var datos = {
          grupo: grupo,
          idTipo: idTipo,
          idPresentacion: JSON.stringify(jsonPresentacion),
          diagnosticos: JSON.stringify(jsonDiagnosticos),
          profesional: email_profesional,
          clinica: clinica
        }   
        console.log(datos)
        

        $.post("<?= base_url();?>index.php/user_radiologia/obtenerValorTotal", datos)
        .done(function( data ) {console.log(data)   
          if($.trim(data) != "{}"){
            var json = JSON.parse(data)
            var contadorFotoRes;
            if($("#tbodyResumen > tr").length > 0){
              contadorFotoRes = parseInt($("#tbodyResumen > tr:last").attr("index")) + 1;
            }else{
              contadorFotoRes = 0;
            }

            $("#checkboxProcedimiento" + contadorFoto).attr("valor", json.valor)  
            $("#checkboxProcedimiento" + contadorFoto).attr("idprocedimiento", json.idprocedimiento) 
            $("#checkboxProcedimiento" + contadorFoto).attr("descuento", json.descuento)  
            $("#checkboxProcedimiento" + contadorFoto).attr("tipoconvenio", json.convenio) 
            $("#tbodyOrdenMedica > tr")[contadorFoto].children[2].innerHTML = json.nameProfesional

            console.log($("#tbodyOrdenMedica > tr")[contadorFoto].children[2].innerText)
            var desc = "";
            if($.trim(objH.children[0].innerText) == "Radiografias" || $.trim(objH.children[0].innerText) == "Modelos"){
              desc = objH.children[1].innerText;
            }else{
              desc = objH.children[0].innerText + " " + objH.children[1].innerText
            }
            $("#tbodyResumen").append("<tr index = '" + contadorFotoRes + "' align='center'><td>" + desc + "</td><td><input type='text' id='cantidad" + contadorFotoRes + "' class='form-control' onKeyUp='format(this)' value='1' ind='" + contadorFotoRes + "' style='text-align: center;'/></td><td><input type='text' id='valor" + contadorFotoRes + "' class='form-control' onKeyUp='format(this)' value='" + json.valor + "' ind='" + contadorFotoRes + "' style='text-align: center;'/><input type='hidden' id='descuento" + contadorFotoRes + "' class='form-control' value='" + json.descuento + "'/><input type='hidden' id='procedimiento" + contadorFotoRes + "' class='form-control' value='" + json.idprocedimiento + "'/><input type='hidden' id='emailpro" + contadorFotoRes + "' class='form-control' value='" + email_profesional + "'></td><td>" + json.convenio + "</td><td>" + json.descuento + "</td><td>" + (parseInt(json.valor) - (parseInt(json.valor) * (parseInt(json.descuento) / 100))) + "</td><td><textarea id='textareaObservacion" + contadorFotoRes + "' class='form-control' ind='" + contadorFotoRes + "'></textarea></td><td><button id='borrar" + contadorFotoRes + "' class='btn btn-danger' indtror='tr" + contadorFoto + "' idCheckbox='checkboxProcedimiento" + contadorFotoRes + "' idprocejson='" + contadorFotoRes + "' idorden='0'>X</button></td></tr>")
            if($("#tbodyResumen > tr").length > 0){
              $("#btnGenerarFactura").removeAttr("disabled")
            }else{
              $("#btnGenerarFactura").attr("disabled", "disabled")
            } 
            console.log($("#tbodyResumen > tr").length)
            validarDatos(contadorFotoRes)

            $("#checkboxProcedimiento" + contadorFotoRes).click(function(e){
              $("#btnGenerarFactura").attr("tipoFactura", "CONORDENMEDICA")
              var idTrSelect = $(this).parent().parent()[0].id
              var idcheckbox = $(this).attr("id")
              var idprocejson = $(this).attr("idprocejson")
              var idorden = $(this).attr("idorden")
              $("#" + $(this).parent().parent()[0].id).css({"background-color":"#0683c9", "color":"#FFFFFF"})
              var tipoConvenio = $(this).attr("tipoConvenio")
              var descuento = $(this).attr("descuento")
              var idprocedimiento = $(this).attr("idprocedimiento")
              var emailprofesional = $(this).attr("email_profesional")
              var valor = $(this).attr("valor")
              var subtotal = 0;
              var descripcion = $(this).parent().parent()[0].children[3].outerText

              var desc = "";
              if($.trim($(this).parent().parent()[0].children[3].outerText) == "Radiografias" || $.trim($(this).parent().parent()[0].children[3].outerText) == "Modelos"){
                desc = $(this).parent().parent()[0].children[4].outerText;
              }else{
                desc = $(this).parent().parent()[0].children[3].outerText + " " + $(this).parent().parent()[0].children[4].outerText
              }
              $(this).attr("disabled", "disabled")
              agregarResumen(idprocedimiento, idTrSelect, valor, desc, emailprofesional, descuento, tipoConvenio)
            })


            $("#myModalAddProcedimientos").modal("hide");
            $(".modal-backdrop").css({"display":"none"}) 

          }
        });

calcularTotal() 

if($("#tbodyResumen > tr").length > 0){
  $("#btnGenerarFactura").removeAttr("disabled")
}else{
  $("#btnGenerarFactura").attr("disabled", "disabled")
}






});



})

function existeDiente(diente){
  var estado = -1;
  for (var i = 0; i < arrayDatos.length; i++) {
    if(arrayDatos[i] != null){
      if(arrayDatos[i] == diente){
        estado = i;
      }
    }        
  }
  return estado; 
}  

function calcularTotal() {
  var valorTotal = 0;
  $("#tbodyResumen > tr").each(function(index) {
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
        // Los eliminamos todos
        var precio = $(this)[0].children[5].outerText
        for (var n = 0; n < specialChars.length; n++) {
          precio= precio.replace(new RegExp("\\" + specialChars[n], 'gi'), '');
        }
        valorTotal += parseInt(precio)
        $("#txtValorTotal").val(formatNumber.new(valorTotal))
      });
}

function agregarResumen(idprocedimiento, contadorFoto, valor, desc, emailProfesional, descuento, tipoConvenio) {
  var contadorFotoRes;
  if($("#tbodyResumen > tr").length > 0){
    contadorFotoRes = parseInt($("#tbodyResumen > tr:last").attr("index")) + 1;
  }else{
    contadorFotoRes = 0;
  }

  $("#tbodyResumen").append("<tr index = '" + contadorFotoRes + "' align='center'><td>" + desc + "</td><td><input type='text' id='cantidad" + contadorFotoRes + "' class='form-control' onKeyUp='format(this)' value='1' ind='" + contadorFotoRes + "' style='text-align: center;'/></td><td><input type='text' id='valor" + contadorFotoRes + "' class='form-control' onKeyUp='format(this)' value='" + valor + "' ind='" + contadorFotoRes + "' style='text-align: center;'/><input type='hidden' id='descuento" + contadorFotoRes + "' class='form-control' value='" + descuento + "'/><input type='hidden' id='procedimiento" + contadorFotoRes + "' class='form-control' value='" + idprocedimiento + "'/><input type='hidden' id='emailpro" + contadorFotoRes + "' class='form-control' value='" + emailProfesional + "'></td><td>" + tipoConvenio + "</td><td>" + descuento + "</td><td>" + (parseInt(valor) - (parseInt(valor) * (parseInt(descuento) / 100))) + "</td><td><textarea id='textareaObservacion" + contadorFotoRes + "' class='form-control' ind='" + contadorFotoRes + "'></textarea></td><td><button id='borrar" + contadorFotoRes + "' class='btn btn-danger' indtror='" + contadorFoto + "' idCheckbox='checkboxProcedimiento" + contadorFotoRes + "' idprocejson='" + contadorFotoRes + "' idorden='0'>X</button></td></tr>")
  validarDatos(contadorFotoRes)
  if($("#tbodyResumen > tr").length > 0){
    $("#btnGenerarFactura").removeAttr("disabled")
  }else{
    $("#btnGenerarFactura").attr("disabled", "disabled")
  } 


}
function validarDatos(contadorFoto){

  calcularTotal()

  $("#borrar" + contadorFoto).click(function(e) {
    var confirmar = window.confirm("¿Desea borrar esta fila?")
    if(confirmar){
      $(this).parent().parent()[0].remove()
      $("#" + $(this).attr("indTrOr")).css({"background-color":"#FFFFFF", "color":"#000000"})
      $("#" + $(this).attr("idCheckbox")).removeAttr("checked")
      $("#" + $(this).attr("idCheckbox")).removeAttr("disabled")

      calcularTotal()          

      if($("#tbodyResumen > tr").length > 0){
        $("#btnGenerarFactura").removeAttr("disabled")
      }else{
        $("#btnGenerarFactura").attr("disabled", "disabled")
      } 
      console.log($("#tbodyResumen > tr").length)
    }
  })

  $("#cantidad" + contadorFoto).keyup(function(e) {
    var cantidad = $(this).val()
    var ind = $(this).attr("ind")
    var precio = $("#valor" + ind).val()
    var descuento = $("#descuento" + ind).val()
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
    var subtotal = 0;
        // Los eliminamos todos
        for (var n = 0; n < specialChars.length; n++) {
          precio= precio.replace(new RegExp("\\" + specialChars[n], 'gi'), '');
          cantidad= cantidad.replace(new RegExp("\\" + specialChars[n], 'gi'), '');
        }

        

        if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){

          var total = (parseInt(cantidad)) * (parseInt(precio))

          if($.trim(descuento) != "0"){
            subtotal = parseInt(total) - (parseInt(total) * (parseInt(descuento) / 100))
          }else{
            subtotal = parseInt(total)
          }
          $(this).parent().parent()[0].children[5].innerHTML = formatNumber.new(subtotal) 

          calcularTotal() 
        }else{
          $(this).parent().parent()[0].children[5].innerHTML = ""
        }
        
        //$("#" + $(this).parent().parent()[0].id).val();
      });

  $("#valor" + contadorFoto).keyup(function(e) {
    var precio = $(this).val()
    var ind = $(this).attr("ind")
    var cantidad = $("#cantidad" + ind).val()
    var descuento = $("#descuento" + ind).val()
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
    var subtotal = 0;
        // Los eliminamos todos
        for (var n = 0; n < specialChars.length; n++) {
          precio= precio.replace(new RegExp("\\" + specialChars[n], 'gi'), '');
          cantidad= cantidad.replace(new RegExp("\\" + specialChars[n], 'gi'), '');
        }

        if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){
          var total = (parseInt(cantidad)) * (parseInt(precio))

          if($.trim(descuento) != "0"){
            subtotal = parseInt(total) - (parseInt(total) * (parseInt(descuento) / 100))
          }else{
            subtotal = parseInt(total)
          }
          $(this).parent().parent()[0].children[5].innerHTML = formatNumber.new(subtotal) 

          console.log(cantidad + " " + precio + " " + total)

          calcularTotal() 
        }else{
          $(this).parent().parent()[0].children[5].innerHTML = ""
        }
      });     
}  

function existeDienteTomo(diente){
  var estado = -1;
  for (var i = 0; i < arrayDatosTomo.length; i++) {
    if(arrayDatosTomo[i] != null){
      if(arrayDatosTomo[i] == diente){
        estado = i;
      }
    }        
  }
  return estado; 
}    

function listarPresentacionTomo(idtipo){
  $("#divPresentacionTomo").css({"display":"block"})
  $("#divPresentacionTomo").html("")
  $("#btnAddTomografia").css({"display":"block"})
  $("#obsertomografiasdiv").css({"display":"block"})


      //Se guardan los datos en un JSON
      var datos = {
        id_tipo: idtipo
      }   
      
      $.post("<?= base_url();?>index.php/procedimientos/listarPresentacion", datos)
      .done(function( data ) {//console.log(data)             
        if($.trim(data) != "[]"){
          var json = JSON.parse(data)
          for (var i = 0; i < json.length; i++) {
            $("#divPresentacionTomo").append('<div class="checkbox" style="border: none; background: transparent;">' +
              '<label><input type="checkbox" name="checkboxPresentacionTomo[]" id="" value="' + json[i].id + '"><font>' + json[i].nombre + '</font></label>' +
              '</div>')             
          }
          
        }else{
        }
        

      });

      var sel =  $('#tipotomografia option:selected').text();

      switch(sel){
        case "Zona":
        $("#dientestomografia").val(""); 
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").removeAttr("disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Maxilar Inferior":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Zona + Maxilar Inferior":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").removeAttr("disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        $(".superior").attr("disabled", "disabled")
        $(".inferior").removeAttr("disabled")
        break;
        case "Maxilar Superior":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Zona + Maxilar Superior":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").removeAttr("disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        $(".inferior").attr("disabled", "disabled")
        $(".superior").removeAttr("disabled")
        break;
        case "Bimaxilar":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Zona + Bimaxilar":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").removeAttr("disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Senos Paranasales":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "ATM Condilo 3D":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Cara Completa":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Guia Quirurgica":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
        case "Tomografia Terceros Molares":
        $("#dientestomografia").val("");
        arrayDatosTomo.splice(0,arrayDatosTomo.length);
        $(".dientetomografia").attr("disabled", "disabled")
        $(".dientetomografia").css({"background-color":"#FFFFFF", "color":"#000000"})
        break;
      }
    }

    $("#btnRealizarPago").click(function(e){
      
      if(validarCampos() == 100){
        window.alert('Por favor seleccione una forma de pago');
      }else {
        if(validarCampos() == 0){
          var identificacion = $("#txtIdentificacion").chosen().val()[0]
          var total = $("#txtValorTotal").val()
          var tipoFactura = $("#btnGenerarFactura").attr("tipofactura")
          var jsonDetalle = [];

          var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
          // Los eliminamos todos
          for (var i = 0; i < specialChars.length; i++) {
            total= total.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
          }

          var datos = {}   
          $("#spin").show();
          $("#tbodyResumen > tr").each(function() {
            var ind = $(this).attr("index")
            var descripcion = $(this)[0].children[0].outerText
            var cantidad = $("#cantidad" + ind).val()
            var convenio = $(this)[0].children[3].outerText
            var descuento = $("#descuento" + ind).val()
            var precio = $("#valor" + ind).val()
            var idProcedimiento = $("#procedimiento" + ind).val()
            var emailProfesional = $("#emailpro" + ind).val()
            var observacionRecepcion = $("#textareaObservacion" + ind).val()
            var subtotal = $(this)[0].children[5].outerText
            var observacionInicial = "";
            if($("#" + $("#borrar" + ind).attr("indtror")).length > 0 && $.trim($("#" + $("#borrar" + ind).attr("indtror"))[0].children[6].outerText).length > 0){
              observacionInicial = $("#" + $("#borrar" + ind).attr("indtror"))[0].children[6].outerText  
            }else{
              observacionInicial = "";
            }
            
            var idprocejson = $("#borrar" + ind).attr("idprocejson")
            var idorden = $("#borrar" + ind).attr("idorden")

            var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
            // Los eliminamos todos
            for (var i = 0; i < specialChars.length; i++) {
              precio = precio.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
              cantidad = cantidad.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
              subtotal = subtotal.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
            }

            jsonDetalle[jsonDetalle.length] = {
              descripcion:descripcion,
              convenio:convenio,
              descuento:descuento,
              cantidad:cantidad,
              precio:precio,
              observacionInicial:observacionInicial,
              observacionRecepcion:observacionRecepcion,
              subtotal:subtotal,
              idprocejson:idprocejson,
              idorden:idorden,
              procedimiento:idProcedimiento,
              profesional:emailProfesional
            }
          });
          
          switch($("#selecTipoPago").val()){
            case "Efectivo":
            datos = {
              documento: identificacion,
              user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
              total: total,
              jsonDetalle: JSON.stringify(jsonDetalle),
              tipoPago: $("#selecTipoPago").val(),
              numeroCheque: "",
              codigoBanco: "",
              numeroCuenta: "",
              fechaCheque: "",
              codigoTarjeta: "",
              tipoTarjeta: "",
              bancoTarjeta: "",
              numeroTarjeta: "",
              documentoOtra: "",
              formaPagoOtra: "",
              codigoOtra: "",
              tipoFactura: tipoFactura
            }   
            break;

            case "Cheque":
            var numeroCheque = $("#txtNumeroCheque").val()
            var codigoBanco = $("#txtCodigoBanco").val()
            var numeroCuenta = $("#txtNumeroCuenta").val()
            var fecha = $("#txtFechaCheque").val()

            datos = {
              documento: identificacion,
              user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
              total: total,
              jsonDetalle: JSON.stringify(jsonDetalle),
              tipoPago: $("#selecTipoPago").val(),
              numeroCheque: numeroCheque,
              codigoBanco: codigoBanco,
              numeroCuenta: numeroCuenta,
              fechaCheque: fecha,
              codigoTarjeta: "",
              tipoTarjeta: "",
              bancoTarjeta: "",
              numeroTarjeta: "",
              documentoOtra: "",
              formaPagoOtra: "",
              codigoOtra: "",
              tipoFactura: tipoFactura
            }   
            break;

            case "Tarjeta de Credito":
            var codigo = $("#txtCodigoTarjeta").val()
            var tipo = $("#txtTipoTarjeta").val()
            var banco = $("#txtBancoTarjeta").val()
            var numero = $("#txtNumeroTarjeta").val()

            datos = {
              documento: identificacion,
              user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
              total: total,
              jsonDetalle: JSON.stringify(jsonDetalle),
              tipoPago: $("#selecTipoPago").val(),
              numeroCheque: "",
              codigoBanco: "",
              numeroCuenta: "",
              fechaCheque: "",
              codigoTarjeta: codigo,
              tipoTarjeta: tipo,
              bancoTarjeta: banco,
              numeroTarjeta: numero,
              documentoOtra: "",
              formaPagoOtra: "",
              codigoOtra: "",
              tipoFactura: tipoFactura
            }   
            break;

            case "Otra forma de pago":
            var documento = $("#txtDocuemtoOtro").val()
            var formaPago = $("#txtFormaPagoOtro").val()
            var codigo = $("#txtCodigoOtro").val()

            datos = {
              documento: identificacion,
              user_radiologia: "<?= $this->session->userdata('UserIDInternoCR');?>",
              total: total,
              jsonDetalle: JSON.stringify(jsonDetalle),
              tipoPago: $("#selecTipoPago").val(),
              numeroCheque: "",
              codigoBanco: "",
              numeroCuenta: "",
              fechaCheque: "",
              codigoTarjeta: "",
              tipoTarjeta: "",
              bancoTarjeta: "",
              numeroTarjeta: "",
              documentoOtra: documento,
              formaPagoOtra: formaPago,
              codigoOtra: codigo,
              tipoFactura: tipoFactura
            }   
            break;
          }

          
          $.post("<?= base_url();?>index.php/ordenes_medicas/addfacturaRadiologia", datos)
          .done(function( result ) {console.log(result) 
            var json = JSON.parse(result)
            $("input").val("")
            $("#tbodyOrdenMedica").html("")
            $("#tbodyResumen").html("")
            $("#txtIdentificacion").chosen().val([])
            $("#btnGenerarFactura").attr("disabled", "disabled")

            var sOptions = 'status=yes,menubar=no,scrollbars=yes,resizable=yes,toolbar=yes, statusbar=no';
            sOptions = sOptions + ',width=' + (screen.availWidth).toString();
            sOptions = sOptions + ',height=' + (screen.availHeight - 10).toString();
            sOptions = sOptions + ',screenX=-10,screenY=0,left=-10,top=0';

            window.open("<?= base_url();?>index.php/ordenes_medicas/pdfFacturaRadiologia?identificacion=" + identificacion + "&idFactura=" + json.id + "&consecutivo=" + json.consecutivo, "Factura", sOptions);
            $("#btnAddProcedimientos").attr("disabled", "disabled")
            $("#myModalFormaPago").modal("hide");
            $(".modal-backdrop").css({"display":"none"})
            $("#spin").hide();
            //window.location.href = "home.php";
          }); 
        }
      }

    });

$("#selecTipoPago").change(function(e){

  if($("#selecTipoPago").val() != "Seleccione"){
    switch($("#selecTipoPago").val()){
      case "Efectivo":
      $("#divNumeroChque").css({"display": "none"})
      $("#divCodigoBanco").css({"display": "none"})
      $("#divNumeroCuenta").css({"display": "none"})
      $("#divFechaCheque").css({"display": "none"})

      $("#divCodigoTarjeta").css({"display": "none"})
      $("#divTipoTarjeta").css({"display": "none"})
      $("#divBancoTarjeta").css({"display": "none"})
      $("#divNumeroTarjeta").css({"display": "none"})

      $("#divDocuemtoOtro").css({"display": "none"})
      $("#divFormaPagoOtro").css({"display": "none"})
      $("#divCodigoOtro").css({"display": "none"})


      break;

      case "Cheque":
      $("#divNumeroChque").css({"display": "block"})
      $("#divCodigoBanco").css({"display": "block"})
      $("#divNumeroCuenta").css({"display": "block"})
      $("#divFechaCheque").css({"display": "block"})

      $("#divCodigoTarjeta").css({"display": "none"})
      $("#divTipoTarjeta").css({"display": "none"})
      $("#divBancoTarjeta").css({"display": "none"})
      $("#divNumeroTarjeta").css({"display": "none"})

      $("#divDocuemtoOtro").css({"display": "none"})
      $("#divFormaPagoOtro").css({"display": "none"})
      $("#divCodigoOtro").css({"display": "none"})            
      break;

      case "Tarjeta de Credito":
      $("#divNumeroChque").css({"display": "none"})
      $("#divCodigoBanco").css({"display": "none"})
      $("#divNumeroCuenta").css({"display": "none"})
      $("#divFechaCheque").css({"display": "none"})

      $("#divCodigoTarjeta").css({"display": "block"})
      $("#divTipoTarjeta").css({"display": "block"})
      $("#divBancoTarjeta").css({"display": "block"})
      $("#divNumeroTarjeta").css({"display": "block"})

      $("#divDocuemtoOtro").css({"display": "none"})
      $("#divFormaPagoOtro").css({"display": "none"})
      $("#divCodigoOtro").css({"display": "none"})
      break;

      case "Otra forma de pago":
      $("#divNumeroChque").css({"display": "none"})
      $("#divCodigoBanco").css({"display": "none"})
      $("#divNumeroCuenta").css({"display": "none"})
      $("#divFechaCheque").css({"display": "none"})

      $("#divCodigoTarjeta").css({"display": "none"})
      $("#divTipoTarjeta").css({"display": "none"})
      $("#divBancoTarjeta").css({"display": "none"})
      $("#divNumeroTarjeta").css({"display": "none"})

      $("#divDocuemtoOtro").css({"display": "block"})
      $("#divFormaPagoOtro").css({"display": "block"})
      $("#divCodigoOtro").css({"display": "block"})
      break;
    }
    $("#btnRealizarPago").removeAttr("disabled")
  }else{
    $("#btnRealizarPago").attr("disabled", "disabled")  

    $("#divNumeroChque").css({"display": "none"})
    $("#divCodigoBanco").css({"display": "none"})
    $("#divNumeroCuenta").css({"display": "none"})
    $("#divFechaCheque").css({"display": "none"}) 

    $("#divCodigoTarjeta").css({"display": "none"})
    $("#divTipoTarjeta").css({"display": "none"})
    $("#divBancoTarjeta").css({"display": "none"})
    $("#divNumeroTarjeta").css({"display": "none"})   

    $("#divDocuemtoOtro").css({"display": "none"})
    $("#divFormaPagoOtro").css({"display": "none"})
    $("#divCodigoOtro").css({"display": "none"}) 
  }
})
$('#myModalFormaPago').on('hidden.bs.modal', function () {
  $("#selecTipoPago").val("Seleccione")
  $("#btnRealizarPago").attr("disabled", "disabled")  

  $("#divNumeroChque").css({"display": "none"})
  $("#divCodigoBanco").css({"display": "none"})
  $("#divNumeroCuenta").css({"display": "none"})
  $("#divFechaCheque").css({"display": "none"}) 

  $("#divCodigoTarjeta").css({"display": "none"})
  $("#divTipoTarjeta").css({"display": "none"})
  $("#divBancoTarjeta").css({"display": "none"})
  $("#divNumeroTarjeta").css({"display": "none"})   

  $("#divDocuemtoOtro").css({"display": "none"})
  $("#divFormaPagoOtro").css({"display": "none"})
  $("#divCodigoOtro").css({"display": "none"}) 
})
$('#myModalFormaPago').on('shown.bs.modal', function() {

  $("#selecTipoPago").val("Seleccione")
  $("#btnRealizarPago").attr("disabled", "disabled")  

  $("#divNumeroChque").css({"display": "none"})
  $("#divCodigoBanco").css({"display": "none"})
  $("#divNumeroCuenta").css({"display": "none"})
  $("#divFechaCheque").css({"display": "none"}) 

  $("#divCodigoTarjeta").css({"display": "none"})
  $("#divTipoTarjeta").css({"display": "none"})
  $("#divBancoTarjeta").css({"display": "none"})
  $("#divNumeroTarjeta").css({"display": "none"})   

  $("#divDocuemtoOtro").css({"display": "none"})
  $("#divFormaPagoOtro").css({"display": "none"})
  $("#divCodigoOtro").css({"display": "none"}) 
})

$("#txtNumeroCheque").keyup(function(e){
  var numeroCheque = $("#txtNumeroCheque").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanNumeroCheque").html("")
    $("#spanNumeroCheque").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanNumeroCheque").html("Debe Ingresar el numero de cheque")
      $("#spanNumeroCheque").css({"display": "block"})
    }
  }
})

$("#txtCodigoBanco").keyup(function(e){
  var numeroCheque = $("#txtCodigoBanco").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanCodigoBanco").html("")
    $("#spanCodigoBanco").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanCodigoBanco").html("Debe Ingresar el codigo del banco")
      $("#spanCodigoBanco").css({"display": "block"})
    }
  }
})

$("#txtNumeroCuenta").keyup(function(e){
  var numeroCheque = $("#txtNumeroCuenta").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanNumeroCuenta").html("")
    $("#spanNumeroCuenta").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanNumeroCuenta").html("Debe Ingresar el numero de cuenta")
      $("#spanNumeroCuenta").css({"display": "block"})
    }
  }
})

$("#txtFechaCheque").keyup(function(e){
  var numeroCheque = $("#txtFechaCheque").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanFechaCheque").html("")
    $("#spanFechaCheque").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanFechaCheque").html("Debe Ingresar la fecha")
      $("#spanFechaCheque").css({"display": "block"})
    }
  }
})

$("#txtCodigoTarjeta").keyup(function(e){
  var numeroCheque = $("#txtCodigoTarjeta").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanCodigoTarjeta").html("")
    $("#spanCodigoTarjeta").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanCodigoTarjeta").html("Debe Ingresar el codigo")
      $("#spanCodigoTarjeta").css({"display": "block"})
    }
  }
})

$("#txtTipoTarjeta").keyup(function(e){
  var numeroCheque = $("#txtTipoTarjeta").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanTipoTarjeta").html("")
    $("#spanTipoTarjeta").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanTipoTarjeta").html("Debe Ingresar el tipo de tarjeta")
      $("#spanTipoTarjeta").css({"display": "block"})
    }
  }
})

$("#txtBancoTarjeta").keyup(function(e){
  var numeroCheque = $("#txtBancoTarjeta").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanBancoTarjeta").html("")
    $("#spanBancoTarjeta").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanNumeroTarjeta").html("Debe Ingresar el numero")
      $("#spanNumeroTarjeta").css({"display": "block"})
    }
  }
})

$("#txtNumeroTarjeta").keyup(function(e){
  var numeroCheque = $("#txtNumeroTarjeta").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanNumeroTarjeta").html("Debe Ingresar el numero")
    $("#spanNumeroTarjeta").css({"display": "block"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanNumeroTarjeta").html("Debe Ingresar el numero")
      $("#spanNumeroTarjeta").css({"display": "block"})
    }
  }
})

$("#txtDocuemtoOtro").keyup(function(e){
  var numeroCheque = $("#txtDocuemtoOtro").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanDocuemtoOtro").html("")
    $("#spanDocuemtoOtro").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanDocuemtoOtro").html("Debe Ingresar el numero de documento")
      $("#spanDocuemtoOtro").css({"display": "block"})
    }
  }
})

$("#txtFormaPagoOtro").keyup(function(e){
  var numeroCheque = $("#txtFormaPagoOtro").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanFormaPagoOtro").html("")
    $("#spanFormaPagoOtro").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanFormaPagoOtro").html("Debe Ingresar la forma de pago")
      $("#spanFormaPagoOtro").css({"display": "block"})
    }
  }
})

$("#txtCodigoOtro").keyup(function(e){
  var numeroCheque = $("#txtCodigoOtro").val()
  if($.trim(numeroCheque).length > 0){
    $("#spanCodigoOtro").html("")
    $("#spanCodigoOtro").css({"display": "none"})
  }else{
    if($.trim(numeroCheque).length == 0){
      $("#spanCodigoOtro").html("Debe Ingresar el codigo")
      $("#spanCodigoOtro").css({"display": "block"})
    }
  }
})

function validarCampos(){

  if( $('#selecTipoPago').val() == 'Seleccione' ) {
    return 100;
  }else {
    var estado = 0;
    switch($("#selecTipoPago").val()){
      case "Efectivo":

      break;

      case "Cheque":
      var numeroCheque = $("#txtNumeroCheque").val()
      var codigoBanco = $("#txtCodigoBanco").val()
      var numeroCuenta = $("#txtNumeroCuenta").val()
      var fecha = $("#txtFechaCheque").val()
      if($.trim(numeroCheque).length > 0 && $.trim(codigoBanco).length > 0 && $.trim(numeroCuenta).length > 0 && $.trim(fecha).length > 0){
        $("#spanNumeroCheque").html("")
        $("#spanNumeroCheque").css({"display": "none"})

        $("#spanCodigoBanco").html("")
        $("#spanCodigoBanco").css({"display": "none"})

        $("#spanNumeroCuenta").html("")
        $("#spanNumeroCuenta").css({"display": "none"})

        $("#spanFechaCheque").html("")
        $("#spanFechaCheque").css({"display": "none"})
      }else{
        if($.trim(numeroCheque).length == 0){
          $("#spanNumeroCheque").html("Debe Ingresar el numero de cheque")
          $("#spanNumeroCheque").css({"display": "block"})
          estado++;
        }

        if($.trim(codigoBanco).length == 0){
          $("#spanCodigoBanco").html("Debe Ingresar el codigo del banco")
          $("#spanCodigoBanco").css({"display": "block"})
          estado++;
        }

        if($.trim(numeroCuenta).length == 0){
          $("#spanNumeroCuenta").html("Debe Ingresar el numero de cuenta")
          $("#spanNumeroCuenta").css({"display": "block"})
          estado++;
        }

        if( $.trim(fecha).length == 0){
          $("#spanFechaCheque").html("Debe Ingresar la fecha")
          $("#spanFechaCheque").css({"display": "block"})
          estado++;
        }
      }
      break;

      case "Tarjeta de Credito":
      var codigo = $("#txtCodigoTarjeta").val()
      var tipo = $("#txtTipoTarjeta").val()
      var banco = $("#txtBancoTarjeta").val()
      var numero = $("#txtNumeroTarjeta").val()
      if($.trim(codigo).length > 0 && $.trim(tipo).length > 0 && $.trim(banco).length > 0 && $.trim(numero).length > 0){
        $("#spanCodigoTarjeta").html("")
        $("#spanCodigoTarjeta").css({"display": "none"})

        $("#spanTipoTarjeta").html("")
        $("#spanTipoTarjeta").css({"display": "none"})

        $("#spanBancoTarjeta").html("")
        $("#spanBancoTarjeta").css({"display": "none"})

        $("#spanNumeroTarjeta").html("")
        $("#spanNumeroTarjeta").css({"display": "none"})
      }else{
        if($.trim(codigo).length == 0){
          $("#spanCodigoTarjeta").html("Debe Ingresar el codigo")
          $("#spanCodigoTarjeta").css({"display": "block"})
          estado++;
        }

        if($.trim(tipo).length == 0){
          $("#spanTipoTarjeta").html("Debe Ingresar el tipo de tarjeta")
          $("#spanTipoTarjeta").css({"display": "block"})
          estado++;
        }

        if($.trim(banco).length == 0){
          $("#spanBancoTarjeta").html("Debe Ingresar el banco")
          $("#spanBancoTarjeta").css({"display": "block"})
          estado++;
        }

        if( $.trim(numero).length == 0){
          $("#spanNumeroTarjeta").html("Debe Ingresar el numero")
          $("#spanNumeroTarjeta").css({"display": "block"})
          estado++;
        }
      }
      break;

      case "Otra forma de pago":
      var documento = $("#txtDocuemtoOtro").val()
      var formaPago = $("#txtFormaPagoOtro").val()
      var codigo = $("#txtCodigoOtro").val()
      if($.trim(documento).length > 0 && $.trim(formaPago).length > 0 && $.trim(codigo).length > 0){
        $("#spanDocuemtoOtro").html("")
        $("#spanDocuemtoOtro").css({"display": "none"})

        $("#spanFormaPagoOtro").html("")
        $("#spanFormaPagoOtro").css({"display": "none"})

        $("#spanCodigoOtro").html("")
        $("#spanCodigoOtro").css({"display": "none"})
      }else{
        if($.trim(documento).length == 0){
          $("#spanDocuemtoOtro").html("Debe Ingresar el numero de documento")
          $("#spanDocuemtoOtro").css({"display": "block"})
          estado++;
        }

        if($.trim(formaPago).length == 0){
          $("#spanFormaPagoOtro").html("Debe Ingresar la forma de pago")
          $("#spanFormaPagoOtro").css({"display": "block"})
          estado++;
        }

        if($.trim(codigo).length == 0){
          $("#spanCodigoOtro").html("Debe Ingresar el codigo")
          $("#spanCodigoOtro").css({"display": "block"})
          estado++;
        }
      }
      break;
    }
    return estado;
  }

}
</script>

<script type="text/javascript">
  function fetch(val)
  {
   $.ajax({
     type: 'post',
     url: '<?= base_url();?>ajax/fetch.php',
     data: {
       get_option:val
     },
     success: function (response) {
       document.getElementById("lugarn").innerHTML=response; 
       console.log(response);
     }
   });
 }
</script>
<script type="text/javascript">
  function fetch2(val)
  {
   $.ajax({
     type: 'post',
     url: '<?= base_url();?>ajax/fetch2.php',
     data: {
       get_option:val
     },
     success: function (response) {
       document.getElementById("lugarr").innerHTML=response; 
       console.log(response);
     }
   });
 }
</script>



<!-- desabilitar paciente sin numero de documento -->
<script>

  (function() {

    /*var handler = document.getElementById('selectTipoDocumento');
    var target = document.getElementById('txtDocumentoPaciente');

    handler.addEventListener('change', function() {
      disabled_input_doc(this.value);
    }, false);

    function attrManager( input, attr ) {
      var element = document.getElementById( input );
      if( element ) {
        if( attr === 'remove' ) {
          element.removeAttribute('disabled');
        }else {
          element.setAttribute('disabled', 'true');
        }       
      }
    }

    function disabled_input_doc( select ) {

      var inputsProfesional = [
      'txtNombresProfesional',
      'txtApellidosProfesional',
      'txtEmailProfesional',
      'txtTelefonoProfesional',
      'selectResidenciaProfesional',
      'txtDireccionProfesional',
      ];

      var inputsPaciente = [
      'txtNombresPaciente',
      'txtApellidosPaciente',
      'txtEmailPaciente',
      'txtFechaNacimientoPaciente',
      'selectDepartamentoNacimiento',
      'selectClinicaPaciente',
      'selectProfesionalPaciente',
      'selectDepartamentoResidencia',
      'fileFoto',
      'input[name=radioGenero]',
      'btnAddPaciente',
      'btnAddProfesionalPaciente',
      ];

      if( select === 'Adulto sin identificación' || select === 'Menor sin identificación' ) {
        target.value = 0;                 
        target.setAttribute('disabled', 'true');
        for (var i = inputsProfesional.length - 1; i >= 0; i--) {
          attrManager( inputsProfesional[i], 'remove' );
        }
        for (var i = inputsPaciente.length - 1; i >= 0; i--) {
          attrManager( inputsPaciente[i], 'remove' );
        }
      }else {              
        target.removeAttribute("disabled"); 
        for (var i = inputsProfesional.length - 1; i >= 0; i--) {
          attrManager( inputsProfesional[i], 'set' );
        }
        for (var i = inputsPaciente.length - 1; i >= 0; i--) {
          attrManager( inputsPaciente[i], 'set' );
        }
      }
    }*/

  })();

</script>


<!-- ========================== -->
<!-- MODAL CALCULADOR DEVUIELTA -->
<!-- ========================== -->
<style>
  #fomulario_calculadora_devuelta {
    display: none;
  }
</style>
<div id="calculadora_modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Valor recibido</h4>
      </div>
      <div class="modal-body">
        <form id="fomulario_calculadora">
          <div class="form-group">
            <label>Valor Recibido</label>
            <input type="text" id="_valor_factura" class="form-control text-right" autofocus>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success btn-block">Aceptar</button>
          </div>
        </form>
        <form id="fomulario_calculadora_devuelta">
          <div class="form-group">
            <label>Devuelta</label>
            <input type="text" id="_devuelta_factura" class="form-control text-right" disabled="true">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-danger btn-block" autofocus="true">
              Finalizar
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<!-- ============================================== -->
<!-- =========== CALCULADORA DEVOLUCION =========== -->
<!-- ============================================== -->
<script>
  (function() {

    'use strict';

    var _valor_total = $('#txtValorTotal');
    var _valor_recibido = $('#_valor_factura');
    var _valor_devuelta = $('#_devuelta_factura');

    var config_mask = {
      radixPoint: ",",
      groupSeparator: ".",
      digits: 0,
      autoGroup: true,
      prefix: '$ ',
      rightAlign: false,
    };

    $(_valor_recibido).inputmask("numeric", config_mask);
    $(_valor_devuelta).inputmask("numeric", config_mask);

    $('#calculadora_modal').on('hidden.bs.modal', function () {
      $('#fomulario_calculadora').css('display', 'block');
      $('#fomulario_calculadora_devuelta').css('display', 'none');
      $(_valor_recibido).val('');
      $('#selecTipoPago').val('Seleccione');
      $('#selecTipoPago').focus();
    });

    document.getElementById('fomulario_calculadora_devuelta').addEventListener('submit', function(event) {
      event.preventDefault();

      var event = new MouseEvent('click', {
        'view': window,
        'bubbles': true,
        'cancelable': true
      });

      $('#calculadora_modal').modal('hide');
      document.getElementById('btnRealizarPago').dispatchEvent(event);
    }, false);

    document.getElementById('fomulario_calculadora').addEventListener('submit', function(event) {
      event.preventDefault();

      if(! _valor_recibido.val ) {
        _valor_recibido =  $('#_valor_factura');
      }

      _valor_recibido = _valor_recibido.val().replace('$', '');
      _valor_recibido = _valor_recibido.replace('.', '');
      var _total = _valor_total.val().replace('.', '');

      var recibido = _valor_recibido; 
      var devuleta = (recibido - _total);

      if( parseInt(devuleta) >= 0 ) {
        _valor_devuelta.val( devuleta );
        $('#fomulario_calculadora').css('display', 'none');
        $('#fomulario_calculadora_devuelta').css('display', 'block');
      }else {
        alert( 'Error. \n \n El cobro es mayor que el valor recibido' );
        _valor_recibido = $('#_valor_factura');
      }

    }, false);

    document.getElementById('selecTipoPago').addEventListener('change', function() {
      if(this.value === 'Efectivo') {
        $('#calculadora_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#_valor_factura').val('0');
      }
    }, false);


  })();
</script>


<!-- desabilitar paciente sin numero de documento -->
<script>

  (function() {

    /*var handler = document.getElementById('selectTipoDocumento');
    var target = document.getElementById('txtDocumentoPaciente');

    var handler_1 = document.getElementById('selectTipoDocumentoProfesional');
    var target_1 = document.getElementById('txtDocumentoProfesional');

    var handler_2 = document.getElementById('selectTipoDocumentoProfesionalProcedimiento');
    var target_2 = document.getElementById('txtDocumentoProfesionalProcedimiento');

    handler.addEventListener('change', function() {
      disabled_input_doc(this.value);
    }, false);
  
    handler_1.addEventListener('change', function() {
      disabled_input_doc(this.value);
    }, false);

    handler_2.addEventListener('change', function() {
      disabled_input_doc(this.value);
    }, false);

    function attrManager( input, attr ) {
      var element = document.getElementById( input );
      if( element ) {
        if( attr === 'remove' ) {
          element.removeAttribute('disabled');
        }else {
          element.setAttribute('disabled', 'true');
        }       
      }
    }

    function disabled_input_doc( select ) {

      var inputsProfesional = [
      'txtNombresProfesional',
      'txtApellidosProfesional',
      'txtEmailProfesional',
      'txtTelefonoProfesional',
      'selectResidenciaProfesional',
      'txtDireccionProfesional',
      'txtNombresProfesional',
      'txtEmailProfesional',
      'selectResidenciaProfesional',
      'txtClinicaProfesional',
      'txtApellidosProfesional',
      'txtTelefonoProfesional',
      'txtDireccionProfesional',
      'txtNombresProfesionalProcedimiento',
      'txtEmailProfesionalProcedimiento',
      'selectResidenciaProfesionalProcedimiento',
      'txtClinicaProfesionalProcedimiento',
      'txtApellidosProfesionalProcedimiento',
      'txtTelefonoProfesionalProcedimiento',
      'txtDireccionProfesionalProcedimiento',

      ];

      var inputsPaciente = [
      'txtNombresPaciente',
      'txtApellidosPaciente',
      'txtEmailPaciente',
      'txtFechaNacimientoPaciente',
      'selectDepartamentoNacimiento',
      'selectClinicaPaciente',
      'selectProfesionalPaciente',
      'selectDepartamentoResidencia',
      'fileFoto',
      'input[name=radioGenero]',
      'btnAddPaciente',
      'btnAddProfesionalPaciente',
      ];

      if( select === 'Adulto sin identificación' || select === 'Menor sin identificación' ) {
        
        target.value = 0;                 
        target.setAttribute('disabled', 'true');
        
        target_1.value = 0;                 
        target_1.setAttribute('disabled', 'true');

        target_2.value = 0;                 
        target_2.setAttribute('disabled', 'true');
        
        for (var i = inputsProfesional.length - 1; i >= 0; i--) {
          attrManager( inputsProfesional[i], 'remove' );
        }
        for (var i = inputsPaciente.length - 1; i >= 0; i--) {
          attrManager( inputsPaciente[i], 'remove' );
        }
      }else {              
        
        target.removeAttribute("disabled"); 

        target_1.removeAttribute("disabled"); 
        
        target_2.removeAttribute("disabled"); 
        
        for (var i = inputsProfesional.length - 1; i >= 0; i--) {
          attrManager( inputsProfesional[i], 'set' );
        }
        for (var i = inputsPaciente.length - 1; i >= 0; i--) {
          attrManager( inputsPaciente[i], 'set' );
        }
      }
    }*/

  })();

  function llenarListaPacientes(nombre){
    $("#txtIdentificacion").html("")
    $.post("<?= base_url();?>index.php/user_radiologia/listar_pacientes", {})
    .done(function( data ) {console.log(data)
      if($.trim(data) != "[]"){
        var json = JSON.parse(data);

        for(i = 0; i < json.length; i++){
          $("#txtIdentificacion").append('<option value="' + json[i].document__number + '">' + json[i].first_name + ' ' + json[i].last_name + ' - ' + json[i].document__number + '</option>');
        }

        $("#txtIdentificacion").chosen().val(nombre)  
        $("#txtIdentificacion").trigger("liszt:updated");          
        
        $(".chzn-drop").css({"width":"100%"})
        $(".default").css({"width":"100%"})
        $(".chzn-choices").css({"width":"100%"})


      } 
    });
  }
  $("#txtIdentificacion").chosen({
    allow_single_deselect:true,
    max_selected_options: 1,
    no_results_text: "¡El paciente no existe en el sistema, por favor registrelo!"
  }); 

  //$(".chzn-container").css({"width":"100%"})
  $(".chzn-drop").css({"width":"100%"})
  $(".default").css({"width":"100%"})
  $(".chzn-choices").css({"width":"100%"})

  $("#txtIdentificacion").on('change', function(evt, params) {
    if($(this).chosen().val() != null){
      var identificacion = $(this).chosen().val()[0]

      $("#txtClinicaHidden").val("")
      $("#txtProfesionalHidden").val("")
      //Se guardan los datos en un JSON
      var datos = {
        document: identificacion
      }   

      $.post("<?= base_url();?>index.php/user_radiologia/findPersonaFactura", datos)
      .done(function( data ) {console.log(data) 
        $("#tbodyResumen").html("")            
        if($.trim(data) != "{}"){
          var json = JSON.parse(data)
          $("#txtNombre").val(json.first_name + " " + json.last_name)
          $("#txtDireccion").val(json.direccion)
          $("#txtTelefono").val(json.telefono)
          $("#txtEmail").val(json.email)

          $("#tbodyOrdenMedica").html("")
          if(json.ordenMedica.length > 0){                 

            for(i = 0; i < json.ordenMedica.length; i++){                    
              var jsonDatos = json.ordenMedica  
              var desc = "";
              if($.trim(jsonDatos[i].id_tipo) == "1" || $.trim(jsonDatos[i].id_tipo) == "2" || $.trim(jsonDatos[i].id_tipo) == "18" || $.trim(jsonDatos[i].id_tipo) == "19"){
                desc = jsonDatos[i].nombreTipo;
              }else{
                desc = jsonDatos[i].nombreGrupo + " " + jsonDatos[i].nombreTipo
              }

              $("#tbodyOrdenMedica").append("<tr id='tr" + $("#tbodyOrdenMedica > tr").length + "'><td>" + jsonDatos[i].fecha + "</td><td>" + jsonDatos[i].clinica + "</td><td>" + jsonDatos[i].nombreProfesional + " " + jsonDatos[i].apellidoProfesional + "</td><td>" + desc + "</td><td tipo='" + jsonDatos[i].id_tipo + "'>" + jsonDatos[i].nombreTipo + "</td><td>" + jsonDatos[i].seleccion + "</td><td>" + jsonDatos[i].observacion + "</td><td align='center'><input type='checkbox' name='checkboxAgregar' id='checkboxProcedimiento" + i + "' presentacion='" + JSON.stringify(jsonDatos[i].presentacion) + "' diagnosticos='" + JSON.stringify(jsonDatos[i].diagnostico) + "' descuento='" + jsonDatos[i].descuento + "' tipoConvenio='" + jsonDatos[i].tipoConvenio + "' valor='" + jsonDatos[i].valor + "' idprocedimiento='" + jsonDatos[i].id_procedimiento + "' idprocejson='" + jsonDatos[i].id + "' idorden='" + json.ordenMedica[i].id + "' email_profesional='" + jsonDatos[i].emailProfesional + "'></td></tr>")  ; 

              $("#checkboxProcedimiento" + i).click(function(e){
                $("#btnGenerarFactura").attr("tipoFactura", "CONORDENMEDICA")
                var idTrSelect = $(this).parent().parent()[0].id
                var idcheckbox = $(this).attr("id")
                var idprocejson = $(this).attr("idprocejson")
                var idorden = $(this).attr("idorden")
                $("#" + $(this).parent().parent()[0].id).css({"background-color":"#0683c9", "color":"#FFFFFF"})
                var tipoConvenio = $(this).attr("tipoConvenio")
                var descuento = $(this).attr("descuento")
                var idprocedimiento = $(this).attr("idprocedimiento")
                var emailprofesional = $(this).attr("email_profesional")
                var valor = $(this).attr("valor")
                var subtotal = 0;
                var descripcion = $(this).parent().parent()[0].children[3].outerText
                
                $(this).attr("disabled", "disabled")

                var contadorID;
                if($("#tbodyResumen > tr").length > 0){
                  contadorID = parseInt($("#tbodyResumen > tr:last").attr("index")) + 1;
                }else{
                  contadorID = 0;
                }

                if($.trim(descuento) != "0"){
                  subtotal = parseInt(valor) - (parseInt(valor) * (parseInt(descuento) / 100))
                }else{
                  subtotal = parseInt(valor)
                }



                $("#tbodyResumen").append("<tr index = '" + contadorID + "' align='center' align='center'><td>" + descripcion + "</td><td><input type='text' id='cantidad" + contadorID + "' class='form-control' onKeyUp='format(this)' value='1' ind='" + contadorID + "' style='text-align: center;'/></td><td><input type='text' id='valor" + contadorID + "' class='form-control' onKeyUp='format(this)' value='" + valor + "' ind='" + contadorID + "' style='text-align: center;'/><input type='hidden' id='descuento" + contadorID + "' class='form-control' value='" + descuento + "'/><input type='hidden' id='procedimiento" + contadorID + "' class='form-control' value='" + idprocedimiento + "'/><input type='hidden' id='emailpro" + contadorID + "' class='form-control' value='" + emailprofesional + "'/></td><td>" + tipoConvenio + "</td><td>" + descuento + "</td><td>" + subtotal + "</td><td><textarea id='textareaObservacion" + contadorID + "' class='form-control' ind='" + contadorID + "'></textarea></td><td><button id='borrar" + contadorID + "' class='btn btn-danger' indTrOr='" + idTrSelect + "' idCheckbox='" + idcheckbox + "' idprocejson='" + idprocejson + "' idorden='" + idorden + "'>X</button></td></tr>")

                calcularTotal()                       

                if($("#tbodyResumen > tr").length > 0){
                  $("#btnGenerarFactura").removeAttr("disabled")
                }else{
                  $("#btnGenerarFactura").attr("disabled", "disabled")
                }

                $("#borrar" + contadorID).click(function(e) {
                  var confirmar = window.confirm("¿Desea borrar esta fila?")
                  if(confirmar){
                    $(this).parent().parent()[0].remove()
                    $("#" + $(this).attr("indTrOr")).css({"background-color":"#FFFFFF", "color":"#000000"})
                    $("#" + $(this).attr("idCheckbox")).removeAttr("checked")
                    $("#" + $(this).attr("idCheckbox")).removeAttr("disabled")

                    if($("#tbodyResumen > tr").length > 0){
                      $("#btnGenerarFactura").removeAttr("disabled")
                    }else{
                      $("#btnGenerarFactura").attr("disabled", "disabled")
                    }

                    calcularTotal() 
                  }
                })

                $("#cantidad" + contadorID).keyup(function(e) {
                  var cantidad = $(this).val()
                  var ind = $(this).attr("ind")
                  var precio = $("#valor" + ind).val()
                  var descuento = $("#descuento" + ind).val()
                  var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
                  var subtotal = 0;
                  // Los eliminamos todos
                  for (var i = 0; i < specialChars.length; i++) {
                    precio= precio.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
                    cantidad= cantidad.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
                  }

                  if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){

                    var total = (parseInt(cantidad)) * (parseInt(precio))
                    if($.trim(descuento) != "0"){
                      subtotal = parseInt(total) - (parseInt(total) * (parseInt(descuento) / 100))
                    }else{
                      subtotal = parseInt(total)
                    }
                    $(this).parent().parent()[0].children[5].innerHTML = formatNumber.new(subtotal) 

                    calcularTotal() 
                  }else{
                    $(this).parent().parent()[0].children[5].innerHTML = ""
                  }                        
                });

                $("#valor" + contadorID).keyup(function(e) {
                  var precio = $(this).val()
                  var ind = $(this).attr("ind")
                  var cantidad = $("#cantidad" + ind).val()
                  var descuento = $("#descuento" + ind).val()
                  var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
                  var subtotal = 0;
                  // Los eliminamos todos
                  for (var i = 0; i < specialChars.length; i++) {
                    precio= precio.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
                    cantidad= cantidad.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
                  }

                  if($.trim(cantidad).length > 0 && $.trim(precio).length > 0){
                    var total = (parseInt(cantidad)) * (parseInt(precio))

                    if($.trim(descuento) != "0"){
                      subtotal = parseInt(total) - (parseInt(total) * (parseInt(descuento) / 100))
                    }else{
                      subtotal = parseInt(total)
                    }
                    $(this).parent().parent()[0].children[5].innerHTML = formatNumber.new(subtotal) 

                    calcularTotal() 
                  }else{
                    $(this).parent().parent()[0].children[5].innerHTML = ""
                  }
                });


              }); 
            }

            $("#btnAddProcedimientos").attr("disabled", "disabled")
          }else{
            $("#btnAddProcedimientos").removeAttr("disabled")
            $("#btnGenerarFactura").attr("disabled", "disabled")
          } 
          if($("#tbodyOrdenMedica > tr").length > 0){
            $("#btnAddProcedimientos").attr("disabled", "disabled")

          }else{
            $("#btnAddProcedimientos").removeAttr("disabled")
            $("#btnGenerarFactura").attr("disabled", "disabled")
          }

        }else{
          $("#txtNombre").val("")
          $("#txtDireccion").val("")
          $("#txtTelefono").val("")
          $("#txtEmail").val("")  
          $("#txtEmail").val("")
          $("#txtClinica").val("")              
          $("#btnAddProcedimientos").attr("disabled", "disabled")
          $("#btnGenerarFactura").attr("disabled", "disabled")
          $("#tbodyOrdenMedica").html("")
          $("#h4Mensaje").html("¡El documento del paciente no existe en el sistema!");
          $('#myModalMensaje').modal();
        }
      });
    }else{
      $("#txtNombre").val("")
      $("#txtDireccion").val("")
      $("#txtTelefono").val("")
      $("#txtEmail").val("")  
      $("#txtEmail").val("")
      $("#txtClinica").val("")              
      $("#btnAddProcedimientos").attr("disabled", "disabled")
      $("#btnGenerarFactura").attr("disabled", "disabled")
      $("#tbodyOrdenMedica").html("")
      $("#txtIdentificacion").chosen().val([])  
      $("#txtIdentificacion").trigger("liszt:updated");       
    }
    
  });
</script>



</body>
</html>