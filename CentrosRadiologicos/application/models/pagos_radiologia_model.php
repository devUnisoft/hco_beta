<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Pagos_radiologia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'idFactura' => $datos["idFactura"],
			'tipopago' => $datos["tipopago"],
			'numerocheque' => $datos["numerocheque"],
			'codigobanco' => $datos["codigobanco"],
			'numerocuenta' => $datos["numerocuenta"],
			'fechacheque' => $datos["fechacheque"],
			'codigoautorizaciont' => $datos["codigoautorizaciont"],
			'tipotarjeta' => $datos["tipotarjeta"],
			'bancotarjeta' => $datos["bancotarjeta"],
			'numerotarjeta' => $datos["numerotarjeta"],
			'numdocumento' => $datos["numdocumento"],
			'formapago' => $datos["formapago"],
			'codigoautorizaciono' => $datos["codigoautorizaciono"],
			'estado' => $datos["estado"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('forma_pago_factura_radiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function getPago($id){
		$this->db->where("idFactura", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('forma_pago_factura_radiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	
}