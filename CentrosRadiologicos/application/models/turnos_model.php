<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Turnos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'document' => $datos["document"],
			'id_centro_radiologico' => $datos["centro_radiologico"],
			'id_consultorio' => $datos["consultorio"],
			'estado' => $datos["estado"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('turnos', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function actualizar($document, $centro_radiologico){
		$this->db->query("UPDATE `turnos` SET `estado`='CERRADO' WHERE `document`='$document' AND `id_centro_radiologico`='$centro_radiologico'");	
	}

	function agregarTurno($document, $centro_radiologico){
		$this->db->query("UPDATE `turnos` SET `estado`='ACTIVO' WHERE `document`='$document' AND `id_centro_radiologico`='$centro_radiologico'");	
	}

	function listar($centro_radiologico){
		$data = $this->db->query("SELECT turnos.id AS idturno, turnos.fecha, turnos.hora, users.*, consultorio_radiologia.name FROM `turnos` INNER JOIN users ON users.document__number = turnos.document INNER JOIN consultorio_radiologia ON consultorio_radiologia.id = turnos.id_consultorio WHERE turnos.estado = 'ACTIVO' AND turnos.id_centro_radiologico = '$centro_radiologico' ORDER BY turnos.fecha ASC, turnos.hora ASC LIMIT 5");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}
}