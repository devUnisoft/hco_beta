<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Convenios_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'clinica' => $datos["clinica"],
			'codigo' => $datos["codigo"],
			'verificacion' => $datos["verificacion"],
			'profesional' => $datos["profesional"],
			'descuento_proce' => $datos["descuento"],
			'convenio_entrega' => $datos["entrega"],
			'convenio_factura' => $datos["facturacion"],
			'id_centro_radiologico' => $datos["centro_radiologico"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('convenios', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	//Funcion para crear un pedido
	function update($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'descuento_proce' => $datos["descuento"],
			'convenio_entrega' => $datos["entrega"],
			'convenio_factura' => $datos["facturacion"]);
		$this->db->where('clinica', $datos["clinica"]);
		$this->db->where('profesional', $datos["profesional"]);
		$this->db->where('id_centro_radiologico', $datos["centro_radiologico"]);
		$this->db->update('convenios', $array);
	}

	function verificarConvenios($clinica, $centro_radiologico){
		$data = $this->db->query("SELECT COUNT(*) Total FROM convenios WHERE convenios.clinica= '$clinica' AND convenios.id_centro_radiologico = '$centro_radiologico' GROUP BY convenios.clinica, convenios.descuento_proce, convenios.convenio_entrega, convenios.convenio_factura HAVING COUNT(*) > 0");
		if($data->num_rows() > 0) return $data;
		else return false;				
	}

	function getConveniosProfes($clinica, $profesional, $centro_radiologico){
		$this->db->where("clinica", $clinica);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("profesional", $profesional);
		$this->db->where("id_centro_radiologico", $centro_radiologico);
		$result = $this->db->get('convenios');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}


	function getConvenios($clinica, $centro_radiologico){
		$this->db->where("clinica", $clinica);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("id_centro_radiologico", $centro_radiologico);
		$result = $this->db->get('convenios');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}
}