<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Adhesivos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get_centro_radiologico($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('centro_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		return $result->result();		
	}

	function getUSer($document = '')
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('document__number', trim($this->input->post('num_documento', false)));	
		$query = $this->db->get();
		return $query->result();
	}


}