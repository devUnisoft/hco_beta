<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Centro_radiologicos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('centro_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}


}