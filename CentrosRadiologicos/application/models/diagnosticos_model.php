<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Diagnosticos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get($id, $centro_radiologico){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("id_centro_radiologico", $centro_radiologico);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('diagnostico_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getPresentacion($idpresentacion, $centro_radiologico){
		$data = $this->db->query("SELECT * FROM `diagnostico_radiologico` WHERE `id_presentacion`='$idpresentacion' AND `id_centro_radiologico`='$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;			
	}


}