<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Procedimientos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'codigo' => $datos["codigo"],
			'valor' => $datos["valor"],
			'id_tipo' => $datos["tipo"],
			'id_grupo' => $datos["grupo"],
			'id_presentacion' => $datos["presentacion"],
			'diasEntrega' => $datos["dias"],
			'id_centro_radiologico' => $datos["centro_radiologico"]);
		$this->db->insert('procedimientos_radiologico', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}
	//Funcion para crear un pedido
	function actualizar($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'valor' => $datos["valor"],
			'id_tipo' => $datos["tipo"],
			'id_grupo' => $datos["grupo"],
			'id_presentacion' => $datos["presentacion"],
			'diasEntrega' => $datos["dias"],
			'id_centro_radiologico' => $datos["centro_radiologico"]);
		$this->db->where('codigo', $datos["codigo"]);
		$this->db->update('procedimientos_radiologico', $array);
	}
	function get($id_tipo, $centro_radiologico){
		$this->db->where("id_tipo", $id_tipo);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("id_centro_radiologico", $centro_radiologico);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('procedimientos_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getConPresentacion($id_tipo, $idPresentacion, $centro_radiologico){
		$this->db->where("id_tipo", $id_tipo);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("id_presentacion", $idPresentacion);
		$this->db->where("id_centro_radiologico", $centro_radiologico);
		$result = $this->db->get('procedimientos_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getId($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('procedimientos_radiologico');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getCodigo($codigo, $centro_radiologico){
		$data = $this->db->query("SELECT * FROM procedimientos_radiologico WHERE codigo='$codigo' AND id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;			
	}

	function getNombre($nombre, $centro_radiologico){
		$data = $this->db->query("SELECT * FROM procedimientos_radiologico WHERE name='$nombre' AND id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;			
	}

	function listar($centro_radiologico){
		$data = $this->db->query("SELECT tipo_procedimientos.*, grupo.nombre AS nombreGrupo, procedimientos_radiologico.id_tipo, procedimientos_radiologico.id AS idProcedimiento FROM `procedimientos_radiologico` INNER JOIN tipo_procedimientos ON tipo_procedimientos.id = procedimientos_radiologico.id_tipo INNER JOIN grupo ON grupo.id = tipo_procedimientos.id_grupo WHERE procedimientos_radiologico.id_centro_radiologico='$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}


}