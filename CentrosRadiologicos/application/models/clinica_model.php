<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Clinica_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'name' => $datos["razonsocial"],
			'nit' => $datos["nit"],
			'address' => $datos["direccion"],
			'telefonos' => $datos["telefonos"],
			'email' => $datos["email"],
			'codigosecretaria' => $datos["codigosecretaria"],
			'codigoentidad' => $datos["codigoentidad"],
			'codigodelprestador' => $datos["codigodelprestador"],
			'codigoadministradora' => $datos["codigoadministradora"],
			'nombreadministradora' => $datos["nombreadministradora"],
			'numerofactura' => $datos["numerofactura"],
			'numeroconsecutivo' => $datos["numeroconsecutivo"],
			'active' => "true",
			'documento' => $datos["envia"],
			'personacontacto' => $datos["personacontacto"]);
		$this->db->insert('centro_radiologico', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	//Funcion para crear un pedido
	function update($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'name' => $datos["razonsocial"],
			'nit' => $datos["nit"],
			'logo' => $datos["logo"],
			'address' => $datos["direccion"],
			'telefonos' => $datos["telefonos"],
			'email' => $datos["email"],
			'codigosecretaria' => $datos["codigosecretaria"],
			'codigoentidad' => $datos["codigoentidad"],
			'codigodelprestador' => $datos["codigodelprestador"],
			'codigoadministradora' => $datos["codigoadministradora"],
			'nombreadministradora' => $datos["nombreadministradora"],
			'numerofactura' => $datos["numerofactura"],
			'numeroconsecutivo' => $datos["numeroconsecutivo"],
			'active' => "true",
			'documento' => $datos["envia"],
			'personacontacto' => $datos["personacontacto"],
			'resolucion' => $datos["numero_resolucion"],
			'fecha_resolucion' => $datos["fecha"],
			'rango_resolucion' => $datos["rango"]);
		$this->db->where('id', $datos["id"]);
		$this->db->update('centro_radiologico', $array);
	}


	function generarRipUsuario($fechainicial, $fechafinal)	{
		//proceso para crear rip usuarios
	    error_reporting(0);
		//conexion necesaria para las consultas de rips
		$pdo = new PDO('mysql:host=localhost;dbname=hco', 'root', 'usc');
		//query
		$queryus = "SELECT DISTINCT users.birthdate, users.document__document_type__oid, users.last_name, users.first_name, users.document__number, users.tipousuario, users.gender, users.zona, centro_radiologico.codigoentidad, georeferences.codigodepartamento, georeferences.codigomunicipio , convenios.codigo, convenios.verificacion  FROM users  INNER JOIN centro_radiologico  ON users.clinica=centro_radiologico.name INNER JOIN georeferences ON users.lugar_residencia=georeferences.name INNER JOIN convenios ON users.clinica=convenios.clinica INNER JOIN factura_radiologia ON users.document__number=factura_radiologia.documento    where  factura_radiologia.fecha <= '$fechafinal' and factura_radiologia.fecha  >= '$fechainicial' ";
		//result
		$result = $pdo->query($queryus);
		$data = '';
		while ($row = $result->fetch(PDO::FETCH_OBJ)) {
		        //convierto en edad
		        $fecha = time() - strtotime($row->birthdate);
		        $edad = floor($fecha / 31556926);

		        //abrevio tipo documento
		        if($row->document__document_type__oid == "Cedula de Ciudadania"){
		            $tipo = "CC";
		        }
		        if($row->document__document_type__oid == "Cedula de Extranjeria"){
		            $tipo = "CE";
		        }
		        if($row->document__document_type__oid == "Registro Civil"){
		            $tipo = "RC";
		        }
		        if($row->document__document_type__oid == "Tarjeta de Identidad"){
		            $tipo = "TI";
		        }
		        if($row->document__document_type__oid == "NIT"){
		            $tipo = "NT";
		        }
		        if($row->document__document_type__oid == "Adulto sin identificacion"){
		            $tipo = "AS";
		        }
		        if($row->document__document_type__oid == "Menor sin identificacion"){
		            $tipo = "MS";
		        }if($row->document__document_type__oid == "Pasaporte"){
		            $tipo = "PA";
		        }

		        //division de apellido
		        $apellido = explode(" ", $row->last_name);
		        $count = count($apellido);
		        if($count > 0){
		        $apellido1 = $apellido[0];
		        $apellido2 = $apellido[1];
		        }else{
		        $apellido1 = $row->last_name;
		        $apellido2 = "";
		        }
		        //division de apellido
		        $nombre = explode(" ", $row->first_name);
		        $count2 = count($nombre);
		        if($count2 > 0){
		        $nombre1 = $nombre[0];
		        $nombre2 = $nombre[1];
		        }else{
		        $nombre1 = $row->first_name;
		        $nombre2 = "";    
		        }

		    
		        $data .= "{$tipo},{$row->document__number},{$row->codigo},{$row->verificacion},{$apellido1},{$apellido2},{$nombre1},{$nombre2},{$edad},1,{$row->gender},{$row->codigomunicipio},{$row->codigodepartamento},{$row->zona}\r\n";
		}

		//fecha actual
		$fecha_actual=date("Y");
		$fecha_hora=date("Y-m-d h:i:s");
		//saco el semestre para enviarlo en el nombre del txt
		$mes = date("m",strtotime($fecha_hora));
		$mes = is_null($mes) ? date('m') : $mes;
		$trim=floor(($mes-1) / 6)+1;
		//tipos de header a enviar
 
		header("Content-type: text/plain");
		header('Content-disposition: attachment; filename="US'.$fecha_actual.'-'.$trim.'.txt"');
		echo $data;
		header_remove('Content-Type'); 
		}



		function generarRipAF($fechainicial, $fechafinal,$numerofactura)	{
		//proceso para crear rip af
	    error_reporting(0);
		//conexion necesaria para las consultas de rips
		$pdoaf = new PDO('mysql:host=localhost;dbname=hco', 'root', 'usc');
        //query
		$queryaf = "SELECT  centro_radiologico.codigosecretaria, centro_radiologico.name, centro_radiologico.nit, convenios.codigo , convenios.clinica  FROM centro_radiologico  INNER JOIN convenios  ON centro_radiologico.id=convenios.id_centro_radiologico";
		//result
		$resultaf = $pdoaf->query($queryaf);
		$dataaf = '';
		while ($row = $resultaf->fetch(PDO::FETCH_OBJ)) {
		        //convierto en edad
		        $fecha = time() - strtotime($row->birthdate);
		        $edad = floor($fecha / 31556926);
		        //fecha generacion
		        $fechageneracion=date("Y-m-d");

		        $dataaf .= "{$row->codigosecretaria},{$row->name},NI,{$row->nit},{$numerofactura},{$fechainicial},{$fechafinal},{$fechageneracion},{$row->codigo},{$row->clinica}\r\n";
		}

		//fecha actual
		$fecha_actual=date("Y");
		$fecha_hora=date("Y-m-d h:i:s");
		//saco el semestre para enviarlo en el nombre del txt
		$mes = date("m",strtotime($fecha_hora));
		$mes = is_null($mes) ? date('m') : $mes;
		$trim=floor(($mes-1) / 6)+1;
		//tipos de header a enviar
		
		header("Content-type: text/plain");
		header('Content-disposition: attachment; filename="AF'.$fecha_actual.'-'.$trim.'.txt"');
		echo $dataaf;
		 
		}




	

}