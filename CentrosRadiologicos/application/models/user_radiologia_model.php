<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class User_radiologia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$arrayUsuario = array(
			'__t' => $datos["TipoUsu"],
			'salt' => $datos["clave"],
			'first_name' => $datos["nombre"],
			'last_name' => $datos["apellido"],
			'email' => $datos["email"],
			'birthdate' => $datos["birthdate"],
			'gender' => $datos["genero"],
			'image_url' => "",
			'active' => "True",
			'document__document_type__oid' => $datos["TipoDoc"],
			'document__number' => $datos["NumeroDoc"],
			'lugar_residencia' => $datos["lugarResi"],
			'lugar_nacimiento' => $datos["lugarNac"],
			'consultorio' => $datos["consultorio"],
			'id_centro_radiologico' => $datos["centro_radiologico"]);
		$this->db->insert('users_radiologia', $arrayUsuario);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function crearPaciente($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$arrayUsuario = array(
			'__t' => $datos["TipoUsu"],
			'salt' => "",
			'first_name' => $datos["nombre"],
			'last_name' => $datos["apellido"],
			'email' => $datos["email"],
			'birthdate' => $datos["birthdate"],
			'gender' => $datos["genero"],
			'image_url' => $datos["foto"],
			'active' => "True",
			'document__document_type__oid' => $datos["TipoDoc"],
			'document__number' => $datos["NumeroDoc"],
			'lugar_residencia' => $datos["lugarResi"],
			'lugar_nacimiento' => $datos["lugarNac"],
			'zona' => $datos["zona"],
			'tipousuario' => $datos["tipousuario"],
			'clinica' => $datos["clinica"],
			'especialista' => "",
			'update_fake_data' => "",
			'celular' => "",
			'telefono' => "",
			'_id__oid' => "",
			'email_validate' => "",
			'kind' => "user.person",
			'__v' => "0",
			'turno' => "",
			'direccion' => "",
			'porcentaje' => "0");
		$this->db->set('fechainscripcion', 'CURDATE()', FALSE);
		$this->db->insert('users', $arrayUsuario);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function crearProfesional($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$arrayUsuario = array(
			'__t' => $datos["TipoUsu"],
			'salt' => "",
			'first_name' => $datos["nombre"],
			'last_name' => $datos["apellido"],
			'email' => $datos["email"],
			'birthdate' => "",
			'gender' => "",
			'image_url' => "",
			'active' => "True",
			'document__document_type__oid' => $datos["TipoDoc"],
			'document__number' => $datos["NumeroDoc"],
			'lugar_residencia' => $datos["lugarResi"],
			'lugar_nacimiento' => "",
			'clinica' => $datos["clinica"],
			'especialista' => "",
			'update_fake_data' => "",
			'celular' => "",
			'telefono' => $datos["telefono"],
			'_id__oid' => "",
			'email_validate' => "",
			'kind' => "user.person",
			'__v' => "0",
			'turno' => "",
			'direccion' => $datos["direccion"],
			'porcentaje' => "0");
		$this->db->set('fechainscripcion', 'CURDATE()', FALSE);
		$this->db->insert('users', $arrayUsuario);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function actualizar($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$arrayUsuario = array(
			'__t' => $datos["TipoUsu"],
			'salt' => $datos["clave"],
			'first_name' => $datos["nombre"],
			'last_name' => $datos["apellido"],
			'email' => $datos["email"],
			'birthdate' => $datos["birthdate"],
			'gender' => $datos["genero"],
			'document__document_type__oid' => $datos["TipoDoc"],
			'document__number' => $datos["NumeroDoc"],
			'lugar_residencia' => $datos["lugarResi"],
			'lugar_nacimiento' => $datos["lugarNac"],
			'consultorio' => $datos["consultorio"]);

		$this->db->where('email', $datos["NumeroDocAntes"]);
		$this->db->where('id_centro_radiologico', $datos["centro_radiologico"]);
		$this->db->update('users_radiologia', $arrayUsuario);
	}

	function login($idUsuario, $clave, $perfil, $clinica =  null){

		if( $clinica != null ) {
			$data = $this->db->query("
				SELECT users_radiologia.*, centro_radiologico.name AS NombreCentro, centro_radiologico.logo 
				FROM users_radiologia 
				INNER JOIN centro_radiologico ON centro_radiologico.id = users_radiologia.id_centro_radiologico 
				where users_radiologia.email='$idUsuario' 
				AND users_radiologia.`salt`='$clave' 
				AND users_radiologia.`__t`='$perfil' 
				AND users_radiologia.`active`='True' 
				AND centro_radiologico.name='$clinica'
			");
		}else {
			$data = $this->db->query("SELECT users_radiologia.*, centro_radiologico.name AS NombreCentro, centro_radiologico.logo FROM users_radiologia INNER JOIN centro_radiologico ON centro_radiologico.id = users_radiologia.id_centro_radiologico where users_radiologia.email='$idUsuario' AND users_radiologia.`salt`='$clave' AND users_radiologia.`__t`='$perfil' AND users_radiologia.`active`='True'");
		}

		
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function obtenerPacienteDocumento($documento){
		$this->db->where("document__number", $documento);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function get_clinicas_by_odontologo( $email = null )
	{
		$this->db->select( 'clinica' );
		$this->db->from( 'users' );
		$this->db->where( "email", $email );
		$this->db->group_by('clinica'); 
		
		$result = $this->db->get();

		if( $result->num_rows() > 0 ) {
			return $result;
		}else {
			return [];
		}
	}

	function listar_pacientes(){
		$this->db->select( '*' );
		$this->db->from( 'users' );
		$this->db->group_by('document__number'); 
		$this->db->order_by("first_name", "ASC");
		$this->db->order_by("last_name", "ASC"); 
		
		$result = $this->db->get();

		if($result->num_rows() > 0) return $result->result();
		else return false;	
	}

	function obtener_Profesional_by_clinic($prm = null)
	{
		$this->db->select( '*' );
		$this->db->from( 'users' );
		$this->db->where("document__number", $prm['num_doc']);
		$this->db->where("clinica", $prm['clinica_id']);

		if( isset($prm['tipo_user']) ) {
			$this->db->where("__t", $prm['tipo_user']);
		}else {
			$this->db->where("__t", 'odontologo');
		}

		$result = $this->db->get();

		if( $result->num_rows() > 0 ) {
			return $result;
		}else {
			return false;
		}
	}

	function getPaciente($documento, $Tipo){
		$this->db->where("document__number", $documento);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("__t", $Tipo);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function listarFuncionarios($centro_radiologico){
		$this->db->where("id_centro_radiologico", $centro_radiologico);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("__t", "Asistente");
		$this->db->order_by("first_name", "ASC");
		$this->db->order_by("last_name", "ASC"); 
		$result = $this->db->get('users_radiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}


	function getUsuarioRadiologia($documento, $Tipo){
		$this->db->where("document__number", $documento);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("__t", $Tipo);
		$result = $this->db->get('users_radiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getUsuario($email){
		$this->db->where("email", $email);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('users_radiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getProfesional($email){
		$this->db->where("email", $email);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function listarProfesional($clinica){
		$this->db->where("__t", "odontologo");
		$this->db->where("clinica", $clinica);
		$this->db->order_by("first_name", "ASC");
		$this->db->order_by("last_name", "ASC");
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getProfesionalId($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}
}