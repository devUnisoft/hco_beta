<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Clinicas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function listar(){
		$data = $this->db->query("SELECT DISTINCT clinica FROM users where __t='odontologo' ORDER BY clinica ASC");
		if($data->num_rows() > 0) return $data;
		else return false;				
	}

	function getProfesionales($clinica){
		$this->db->where("clinica", $clinica);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("__t", 'odontologo');//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->order_by('first_name asc, last_name asc'); 
		$result = $this->db->get('users');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}


	



}