<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Grupos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function listar(){
		$data = $this->db->query("SELECT * FROM `grupo`");
		if($data->num_rows() > 0) return $data;
		else return false;				
	}


}