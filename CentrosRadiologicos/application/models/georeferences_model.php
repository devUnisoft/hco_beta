<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Georeferences_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function listar(){
		$data = $this->db->query("SELECT * FROM `georeferences` ORDER BY name ASC");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function listarDepartamentos(){
		$data = $this->db->query("SELECT DISTINCT  fullname FROM georeferences ORDER BY fullname asc ");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}


}