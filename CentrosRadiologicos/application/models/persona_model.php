<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Persona_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function crearPersona($data){
		$this->db->insert('persona', array(
			'Nombres' => $data['nombre'],
			'Apellidos' => $data['apellidos'],
			'Direccion' => $data['direccion'],
			'Telefono' => $data['telefono']));

	}
	function obtenerPersonas(){
		$datos = $this->db->get('persona');
		if($datos->num_rows() > 0) return $datos;
		else return false;
	}
	function consultarPersona($id){
		$this->db->where('id', $id);
		$datos = $this->db->get('persona');
		if($datos->num_rows() > 0) return $datos;
		else return false;
	}
	function actualizar($id, $data){
		$datos = array(
			'Nombres' => $data['nombre'],
			'Apellidos' => $data['apellidos'],
			'Direccion' => $data['direccion'],
			'Telefono' => $data['telefono']);
		$this->db->where('id', $id);
		$this->db->update('persona', $datos);
	}
	function eliminar($id){
		//$sql = "DELETE FROM persona WHERE id = $id";
		//$this->db->query($sql);
		$this->db->delete('persona', array('id' => $id));
	}
}