<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Factura_radiologia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function getTurno($centro_radiologico){
		$turno = 0;
		$data = $this->db->query("SELECT * FROM `factura_radiologia` WHERE `turno`!= '0' AND id_centro_radiologico = '$centro_radiologico' ORDER BY `id` DESC LIMIT 1");
		if($data->num_rows() > 0){
			$turno = $data->result()[0]->turno + 1;
		}else{
			$turno = 1;
		}
		return $turno;
	}

	//Funcion para crear un pedido
	function crear($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'documento' => $datos["documento"],
			'user_radiologia' => $datos["user_radiologia"],
			'total' => $datos["total"],
			'id_centro_radiologico' => $datos["centro_radiologico"],
			'turno' => $datos["turno"],
			'estado' => $datos["estado"],
			'tipo_factura' => $datos["tipoFactura"],
			'consecutivo_id' => $datos["consecutivo"],
			'atendido' => $datos["atendido"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('factura_radiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
		$this->db->select('MAX(id) AS id');
		$this->db->from('factura_radiologia');
		$data = $this->db->get();
		if($data->num_rows() > 0) return $data->result()[0]->id;
		else return false;	
	}

	//Funcion para crear un pedido
	function crearDetalle($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'id_factura_radiologia' => $datos["id_factura_radiologia"],
			'descripcion' => $datos["descripcion"],
			'idProcedimiento' => $datos["idProcedimiento"],
			'observacion_inicial' => $datos["observacion_inicial"],
			'observacion_recepcion' => $datos["observacion_recepcion"],
			'cantidad' => $datos["cantidad"],
			'precio' => $datos["precio"],
			'total' => $datos["total"],
			'convenio' => $datos["convenio"],
			'descuento' => $datos["descuento"],
			'id_orden_medica' => $datos["orden_medica"],
			'id_proceso' => $datos["id_proceso"],
			'odontologo' => $datos["odontologo"],
			'estado' => $datos["estado"]);
		$this->db->insert('detallefacturaradiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function crearLogAnulacion($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'idFactura' => $datos["idFactura"],
			'user_radiologia' => $datos["user_radiologia"],
			'id_centro_radiologico' => $datos["centro_radiologico"],
			'observacion' => $datos["observacion"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('log_factura_anuladas_radiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function get($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('factura_radiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}
	function getConsecutivoFactura($centro_radiologico){
		$this->db->where("id_centro_radiologico", $centro_radiologico);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->select('MAX(consecutivo_id) AS id');
		$this->db->from('factura_radiologia');
		$data = $this->db->get();
		if($data->num_rows() > 0) return $data->result()[0]->id;
		else return false;	
	}

	function getId($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('detallefacturaradiologia');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getFacturasPersonas($documento){
		$data = $this->db->query("SELECT id FROM factura_radiologia WHERE documento = '" . $documento . "'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getDetalle($idFactura){
		$data = $this->db->query("SELECT * FROM `detallefacturaradiologia` WHERE `id_factura_radiologia`='$idFactura'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function reporteCierreCaja($fechaInicial, $fechaFinal, $document, $centro_radiologico){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.estado AS estadoFactura, forma_pago_factura_radiologia.*,factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN forma_pago_factura_radiologia ON factura_radiologia.id = forma_pago_factura_radiologia.idFactura INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.user_radiologia = '$document' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function procedimientosEntregar($document, $centro_radiologico){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.estado AS estadoFactura, forma_pago_factura_radiologia.`idFactura`, forma_pago_factura_radiologia.`fecha`, forma_pago_factura_radiologia.`hora`, forma_pago_factura_radiologia.`tipopago`, forma_pago_factura_radiologia.`numerocheque`, forma_pago_factura_radiologia.`codigobanco`, forma_pago_factura_radiologia.`numerocuenta`, forma_pago_factura_radiologia.`fechacheque`, forma_pago_factura_radiologia.`codigoautorizaciont`, forma_pago_factura_radiologia.`tipotarjeta`, forma_pago_factura_radiologia.`bancotarjeta`, forma_pago_factura_radiologia.`numerotarjeta`, forma_pago_factura_radiologia.`numdocumento`, forma_pago_factura_radiologia.`formapago`, forma_pago_factura_radiologia.`codigoautorizaciono`, forma_pago_factura_radiologia.`estado`, imagenes_radiologia.id AS idImagen, imagenes_radiologia.fecha AS fechaImagen, imagenes_radiologia.hora AS horaImagen, imagenes_radiologia.fechaEntrega, imagenes_radiologia.estadoEntrega, userpro.first_name AS nombreProfesional, userpro.last_name AS apellidoProfesional, userpro.email AS emailProfesional,factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia AND factura_radiologia.estado != 'ANULADO' INNER JOIN users ON users.document__number = factura_radiologia.documento  INNER JOIN forma_pago_factura_radiologia ON factura_radiologia.id = forma_pago_factura_radiologia.idFactura INNER JOIN users userpro ON userpro.email = detallefacturaradiologia.odontologo INNER JOIN imagenes_radiologia ON detallefacturaradiologia.id = imagenes_radiologia.iddetalleFacturaRadiologia AND imagenes_radiologia.estadoEntrega = 'ACTIVO' WHERE factura_radiologia.documento = '$document' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'  GROUP BY detallefacturaradiologia.id");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function procedimientosEntregarSinDoc($centro_radiologico){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.estado AS estadoFactura, forma_pago_factura_radiologia.`idFactura`, forma_pago_factura_radiologia.`fecha`, forma_pago_factura_radiologia.`hora`, forma_pago_factura_radiologia.`tipopago`, forma_pago_factura_radiologia.`numerocheque`, forma_pago_factura_radiologia.`codigobanco`, forma_pago_factura_radiologia.`numerocuenta`, forma_pago_factura_radiologia.`fechacheque`, forma_pago_factura_radiologia.`codigoautorizaciont`, forma_pago_factura_radiologia.`tipotarjeta`, forma_pago_factura_radiologia.`bancotarjeta`, forma_pago_factura_radiologia.`numerotarjeta`, forma_pago_factura_radiologia.`numdocumento`, forma_pago_factura_radiologia.`formapago`, forma_pago_factura_radiologia.`codigoautorizaciono`, forma_pago_factura_radiologia.`estado`, imagenes_radiologia.id AS idImagen, imagenes_radiologia.fecha AS fechaImagen, imagenes_radiologia.hora AS horaImagen, imagenes_radiologia.fechaEntrega, imagenes_radiologia.estadoEntrega, userpro.first_name AS nombreProfesional, userpro.last_name AS apellidoProfesional, userpro.email AS emailProfesional,factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia AND factura_radiologia.estado != 'ANULADO' INNER JOIN users ON users.document__number = factura_radiologia.documento INNER JOIN forma_pago_factura_radiologia ON factura_radiologia.id = forma_pago_factura_radiologia.idFactura INNER JOIN users userpro ON userpro.email = detallefacturaradiologia.odontologo INNER JOIN imagenes_radiologia ON detallefacturaradiologia.id = imagenes_radiologia.iddetalleFacturaRadiologia AND imagenes_radiologia.estadoEntrega = 'ACTIVO' WHERE factura_radiologia.id_centro_radiologico = '$centro_radiologico'  GROUP BY detallefacturaradiologia.id");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFacturasProcesar($centro_radiologico){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, factura_radiologia.atendido,factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE `id_centro_radiologico`='$centro_radiologico' AND detallefacturaradiologia.estado = 'ACTIVO'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFacturasProcesarPaciente($centro_radiologico, $document){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, factura_radiologia.atendido, detallefacturaradiologia.odontologo AS profesional,factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE `id_centro_radiologico`='$centro_radiologico' AND factura_radiologia.documento = '$document' AND detallefacturaradiologia.estado = 'ACTIVO'");
		if($data->num_rows() > 0) return $data;
		else return false;	
	}

	function getFacturasAddImg($document, $fechaInicial, $fechaFinal, $centro_radiologico){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, factura_radiologia.atendido, detallefacturaradiologia.odontologo AS profesional, imagenes_radiologia.imagen, imagenes_radiologia.tags, imagenes_radiologia.id AS idImagen, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `imagenes_radiologia` INNER JOIN detallefacturaradiologia ON imagenes_radiologia.iddetalleFacturaRadiologia = detallefacturaradiologia.id INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.`id_centro_radiologico`='$centro_radiologico' AND factura_radiologia.documento = '$document' AND imagenes_radiologia.imagen = '' AND imagenes_radiologia.tags = '' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function actualizar($document, $centro_radiologico, $estado){
		$this->db->query("UPDATE `factura_radiologia` SET `atendido`='$estado' WHERE `documento`='$document' AND `id_centro_radiologico`='$centro_radiologico'");	
	}

	function actualizarHoraAtencion($idFactura, $profesional){
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'profesional_atencion' => $profesional);
		$this->db->set('fecha_atencion', 'CURDATE()', FALSE);
		$this->db->set('hora_atencion', 'curTime()', FALSE);

		$this->db->where('id_factura_radiologia', $idFactura);
		$this->db->update('detallefacturaradiologia', $array);
	}

	function actualizarCancelarHoraAtencion($id_detalle, $idFactura){
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'fecha_atencion' => null,
			'hora_atencion' => null,
			'profesional_atencion' => null);

		$this->db->where('id', $id_detalle);
		$this->db->where('id_factura_radiologia', $idFactura);
		$this->db->update('detallefacturaradiologia', $array);
	}

	
	function reporte_tiempos_atencion($fechaInicial, $fechaFinal, $centro_radiologico){				
		$this->db->select('detallefacturaradiologia.*, users_radiologia.first_name AS nombre_profesional, users_radiologia.last_name AS apellido_profesional, factura_radiologia.fecha as fechafactura, factura_radiologia.hora as horafactura, factura_radiologia.total, factura_radiologia.tipo_factura, factura_radiologia.documento, users.first_name, users.last_name, users.email');
        $this->db->from('detallefacturaradiologia');
		$this->db->where("factura_radiologia.fecha BETWEEN '". $fechaInicial . "' AND '". $fechaFinal . "'");
		$this->db->where('factura_radiologia.id_centro_radiologico', $centro_radiologico);
        $this->db->join('factura_radiologia', 'factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia', 'left');
        $this->db->join('procedimientos_radiologico', 'procedimientos_radiologico.id = detallefacturaradiologia.idProcedimiento', 'left');        
        $this->db->join('users', 'users.document__number = factura_radiologia.documento', 'left');
        $this->db->join('users_radiologia', 'users_radiologia.email = detallefacturaradiologia.profesional_atencion', 'left');          
        $query = $this->db->get();
        return $query->result();	
	}

	function anular($idFactura){
		$this->db->query("UPDATE `factura_radiologia` SET `estado`='ANULADO' WHERE `id`='$idFactura'");	
	}

	function updateEstado($idFactura, $estado){
		$this->db->query("UPDATE `factura_radiologia` SET `estado`='$estado' WHERE `id`='$idFactura'");	
	}

	function updateDetalle($idDetalle, $estado){
		$this->db->query("UPDATE `detallefacturaradiologia` SET `estado`='$estado' WHERE `id`='$idDetalle'");	
	}

	function getFiltroFacturasFechaIdFa($document, $fecha, $factura){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.id = '$factura' AND factura_radiologia.estado != 'ANULADO' AND factura_radiologia.fecha = '$fecha' AND factura_radiologia.documento = '$document'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getSinFiltroFacturas($document){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.estado != 'ANULADO' AND factura_radiologia.documento = '$document'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroFacturasFecha($document, $fecha){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.estado != 'ANULADO' AND factura_radiologia.fecha = '$fecha' AND factura_radiologia.documento = '$document'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroFacturasIdFac($document, $factura){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.estado != 'ANULADO' AND factura_radiologia.documento = '$document' WHERE factura_radiologia.id = '$factura'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	/*************************************************************************************/

	function getFiltroProcesosDesarroProcFacDoc($proceso, $document, $factura, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia AND factura_radiologia.documento = '$document' INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.id = '$factura' AND factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico' AND detallefacturaradiologia.idProcedimiento = '$proceso'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroProcFac($proceso, $factura, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.id = '$factura' AND factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico' AND detallefacturaradiologia.idProcedimiento = '$proceso'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroProcDoc($proceso, $document, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia AND factura_radiologia.documento = '$document' INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico' AND detallefacturaradiologia.idProcedimiento = '$proceso'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroFacDoc($factura, $document, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.id = '$factura' AND factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND factura_radiologia.documento = '$document' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getSinFiltroProcesosDesarroProcFacDoc($centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia  INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroProc($proceso, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico' AND detallefacturaradiologia.idProcedimiento = '$proceso'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroFac($factura, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.id = '$factura' AND factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}

	function getFiltroProcesosDesarroDoc($document, $centro_radiologico, $fechaInicial, $fechaFinal, $user_radiologia){
		$data = $this->db->query("SELECT detallefacturaradiologia.*, factura_radiologia.fecha, factura_radiologia.hora, users_radiologia.first_name AS nombreFuncionario, users_radiologia.last_name AS apellidosFuncionario, factura_radiologia.estado AS estadoFactura, factura_radiologia.documento, users.first_name, users.last_name, users.email FROM `detallefacturaradiologia` INNER JOIN factura_radiologia ON factura_radiologia.id = detallefacturaradiologia.id_factura_radiologia INNER JOIN users_radiologia ON users_radiologia.email = factura_radiologia.user_radiologia INNER JOIN users ON users.document__number = factura_radiologia.documento WHERE factura_radiologia.user_radiologia = '$user_radiologia' AND factura_radiologia.documento = '$document' AND factura_radiologia.fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND factura_radiologia.id_centro_radiologico = '$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}
}