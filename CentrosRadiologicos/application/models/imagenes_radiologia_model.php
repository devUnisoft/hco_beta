<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Imagenes_radiologia_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	//Funcion para crear un pedido
	function crear($datos, $tipo){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$nuevafecha;

		if(trim($datos["dias"]) != ""){
			$fecha = date('Y-m-j');
		  	$nuevafecha = strtotime ( '+' . $datos["dias"] . ' day' , strtotime ( $fecha ) ) ;
		  	$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
		}else{
			$nuevafecha = null;
		}
		

		$array = array(
			'iddetalleFacturaRadiologia' => $datos["detalleFactura"],
			'user_radiologia' => $datos["user_radiologia"],
			'imagen' => $datos["imagen"],
			'tags' => $datos["tags"],
			'fechaEntrega' => $nuevafecha,
			'estadoEntrega' => "ACTIVO");
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('imagenes_radiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo		

		if($tipo == "CONFOTOS"){
			$arrayHco = array(
				'iduser' => $datos["document"],
				'data' => $datos["imagen"],
				'menu' => $datos["tags"],
				'especialista' => $datos["especialista"],
				'clinica' => $datos["clinica"]);
			$this->db->set('fecha', 'CURDATE()', FALSE);
			$this->db->insert('imagenes', $arrayHco);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
		}	

		
	}

	//Funcion para crear un pedido
	function crearLog($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'iddetalleFacturaRadiologia' => $datos["detalleFactura"],
			'user_radiologia' => $datos["user_radiologia"],
			'observacion' => $datos["observacion"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('log_procedimientos_procesados', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	//Funcion para crear un pedido
	function crearLogEntregadas($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'id_detalle_factura' => $datos["idDetalle"],
			'user_radiologia' => $datos["user_radiologia"],
			'estado' => $datos["estado"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->set('hora', 'curTime()', FALSE);
		$this->db->insert('log_imagenes_entregadas', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	//Funcion para borrar los permisos de los usuarios
	function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('imagenes_radiologia');  
	}

	//Funcion para crear un pedido
	function updateImagenUrl($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$nuevafecha;

		if(trim($datos["dias"]) != ""){
			$fecha = date('Y-m-j');
		  	$nuevafecha = strtotime ( '+' . $datos["dias"] . ' day' , strtotime ( $fecha ) ) ;
		  	$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
		}else{
			$nuevafecha = null;
		}
		$array = array(
			'imagen' => $datos["imagen"],
			'tags' => $datos["tags"],
			'fechaEntrega' => $nuevafecha,
			'estadoEntrega' => "ACTIVO");
		$this->db->where('id', $datos["id"]);
		$this->db->update('imagenes_radiologia', $array);

		$arrayHco = array(
			'iduser' => $datos["document"],
			'data' => $datos["imagen"],
			'menu' => $datos["tags"],
			'especialista' => $datos["especialista"],
			'clinica' => $datos["clinica"]);
		$this->db->set('fecha', 'CURDATE()', FALSE);
		$this->db->insert('imagenes', $arrayHco);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}

	function getFechaFinConsultar($idDetalle){
		$data = $this->db->query("SELECT * FROM `imagenes_radiologia` WHERE `iddetalleFacturaRadiologia`='$idDetalle' GROUP BY iddetalleFacturaRadiologia");
		if($data->num_rows() > 0) return $data->result();
		else return false;		
	}

	function getFechaEntrega($idDetalle){
		$data = $this->db->query("SELECT * FROM `log_imagenes_entregadas` WHERE `id_detalle_factura`='$idDetalle' GROUP BY id_detalle_factura");
		if($data->num_rows() > 0) return $data->result();
		else return false;		
	}

	//Funcion para crear un pedido
	function updateImagen($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'estadoEntrega' => $datos["estado"]);
		$this->db->where('iddetalleFacturaRadiologia', $datos["id"]);
		$this->db->update('imagenes_radiologia', $array);
	}
}