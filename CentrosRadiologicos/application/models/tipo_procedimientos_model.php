<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Tipo_procedimientos_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get($id, $centro_radiologico){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$this->db->where("id_centro_radiologico", $centro_radiologico);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('tipo_procedimientos');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getGrupo($grupo, $centro_radiologico){
		$result = $this->db->query("SELECT tipo_procedimientos.*, grupo.nombre AS nombreGrupo FROM `tipo_procedimientos` INNER JOIN grupo ON grupo.id = tipo_procedimientos.id_grupo WHERE id_centro_radiologico='$centro_radiologico' AND  id_grupo='$grupo'");
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function listar($centro_radiologico){
		$data = $this->db->query("SELECT tipo_procedimientos.*, grupo.nombre AS nombreGrupo FROM `tipo_procedimientos` INNER JOIN grupo ON grupo.id = tipo_procedimientos.id_grupo WHERE id_centro_radiologico='$centro_radiologico'");
		if($data->num_rows() > 0) return $data;
		else return false;		
	}


}