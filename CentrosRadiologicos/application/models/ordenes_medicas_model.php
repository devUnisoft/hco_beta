<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Ordenes_medicas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function get($document){
		$result = $this->db->query("SELECT detalle_orden_medica.*, ordenesmedicas.fecha, ordenesmedicas.hora, userpro.id AS idProfesional, userpro.first_name AS nombreProfesional, userpro.last_name AS apellidoProfesional, userpro.email AS emailProfesional, userpro.clinica, procedimientos_radiologico.`codigo`, procedimientos_radiologico.`id_tipo`, procedimientos_radiologico.`id_grupo`, procedimientos_radiologico.`id_presentacion`, procedimientos_radiologico.`id_centro_radiologico`, procedimientos_radiologico.`valor`, procedimientos_radiologico.`diasEntrega`, `tipo_procedimientos`.`nombre` AS nombreTipo, tipo_procedimientos.`descripcion` AS descripcionTipo, grupo.`nombre` AS nombreGrupo FROM `detalle_orden_medica` INNER JOIN ordenesmedicas ON ordenesmedicas.id = detalle_orden_medica.id_orden_medica INNER JOIN users userpro ON userpro.email = ordenesmedicas.usuario INNER JOIN procedimientos_radiologico ON detalle_orden_medica.id_procedimiento = procedimientos_radiologico.id INNER JOIN tipo_procedimientos ON tipo_procedimientos.id = procedimientos_radiologico.id_tipo INNER JOIN grupo ON grupo.id = tipo_procedimientos.id_grupo WHERE ordenesmedicas.documento = '$document' AND detalle_orden_medica.estado = 'ACTIVO'");	
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getId($id){
		$this->db->where("id", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('ordenesmedicas');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function getDetalle($id){
		$this->db->where("id_orden_medica", $id);//Con esta linea se agrega un WHERE a un SELECT. Esto debe ir primero
		$result = $this->db->get('detalle_orden_medica');//El metodo get sirve para realizar un SELECT en la bd.
		if($result->num_rows() > 0) return $result;
		else return false;		
	}

	function actualizar($jsonOrden, $idorden){
		$this->db->query("UPDATE `ordenesmedicas` SET `json`='$jsonOrden' WHERE `id`='$idorden'");	
	}

	//Funcion para crear un pedido
	function updateDetalle($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'estado' => $datos["estado"]);
		$this->db->where('id', $datos["id"]);
		$this->db->update('detalle_orden_medica', $array);
	}

	//Funcion para crear un pedido
	function updateDetalleID($datos){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'estado' => $datos["estado"]);
		$this->db->where('id_orden_medica', $datos["id"]);
		$this->db->update('detalle_orden_medica', $array);
	}


}