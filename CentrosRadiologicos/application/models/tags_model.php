<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Tags_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function listar(){
		$data = $this->db->query("SELECT * FROM `tags_radiologia` ORDER BY label ASC");
		if($data->num_rows() > 0) return $data;
		else return false;				
	}
	//Funcion para crear un pedido
	function crear($valor){		
		//Se guardan sus datos en un array asociativo, el cual cada id o key tiene que tener el mismo nombre de la columna de la tabla en la bd. Sino se hace asi se genera error
		$array = array(
			'label' => $valor,
			'value' => $valor);
		$this->db->insert('tags_radiologia', $array);//Se llama a la funcion de insertar datos y se le pasa el array asociativo
	}


}