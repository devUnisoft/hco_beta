<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Help</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" media="screen" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.min.css">
    <script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
    </script>
    <style type="text/css">
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
        }

        header {
    width: 100%;
    height: 90px;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    -moz-border-radius:0px 55px;
    background-color: #0683c9;
    -webkit-transition: height 0.3s;
    -moz-transition: height 0.3s;
    -ms-transition: height 0.3s;
    -o-transition: height 0.3s;
    transition: height 0.3s;
}
header h1#logo {
    display: inline-block;
    height: 150px;
    line-height: 150px;
    float: left;
    font-family: "Oswald", sans-serif;
    font-size: 60px;
    color: white;
    font-weight: 400;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav {
    display: inline-block;
    float: right;
}
header nav a {
    line-height: 150px;
    margin-left: 20px;
    color: #9fdbfc;
    font-weight: 700;
    font-size: 18px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
header nav a:hover {
    color: white;
}
header.smaller {
    height: 75px;
}
header.smaller h1#logo {
    width: 150px;
    height: 75px;
    line-height: 75px;
    font-size: 30px;
}
header.smaller nav a {
    line-height: 75px;
}

@media all and (max-width: 660px) {
    header h1#logo {
        display: block;
        float: none;
        margin: 0 auto;
        height: 100px;
        line-height: 100px;
        text-align: center;
    }
    header nav {
        display: block;
        float: none;
        height: 50px;
        text-align: center;
        margin: 0 auto;
    }
    header nav a {
        line-height: 50px;
        margin: 0 10px;
    }
    header.smaller {
        height: 75px;
    }
    header.smaller h1#logo {
        height: 40px;
        line-height: 40px;
        font-size: 30px;
    }
    header.smaller nav {
        height: 35px;
    }
    header.smaller nav a {
        line-height: 35px;
    }
}
#footer{
        position:absolute;
        width:100%;
        height:90px;
        background-color:#FFFFFF;
        color:#000000;
        bottom:0px;
        clear:both;
    }

 .media
    {
        /*box-shadow:0px 0px 4px -2px #000;*/
        margin: 20px 0;
        padding:30px;
    }
    .dp
    {
        border:10px solid #eee;
        transition: all 0.2s ease-in-out;
    }
    .dp:hover
    {
        border:2px solid #eee;
        
    }



    .nav {
    left:50%;
    margin-left:-150px;
    top:50px;
    position:absolute;
}
.nav>li>a:hover, .nav>li>a:focus, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
    background:#fff;
}
.dropdown {
    border-radius:4px;
    width:300px;    
}
.dropdown-menu>li>a {
    color:#428bca;
}
.dropdown ul.dropdown-menu {
    border-radius:4px;
    box-shadow:none;
    margin-top:20px;
    width:300px;
}
.dropdown ul.dropdown-menu:before {
    content: "";
    border-bottom: 10px solid #fff;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    top: -10px;
    right: 16px;
    z-index: 10;
}
.dropdown ul.dropdown-menu:after {
    content: "";
    border-bottom: 12px solid #ccc;
    border-right: 12px solid transparent;
    border-left: 12px solid transparent;
    position: absolute;
    top: -12px;
    right: 14px;
    z-index: 9;
}




.profile 
{
    min-height: 300px;
    display: inline-block;
    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider 
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis 
{
    border-top: 4px solid transparent;
    }
.emphasis:hover 
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }

span.tags 
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }

.row {
    margin-left: -130px;
}

.col-fixed {
    /* custom width */
    width:320px;
}
.col-min {
    /* custom min width */
    min-width:320px;
}
.col-max {
    /* custom max width */
    max-width:320px;
}

#menuinicial{
    position:fixed;
    width:100%;
    height:90px;
    background-color:#FFFFFF;
    color:#000000;
    clear:both;
    z-index: 100;
  }

.notifications, footer .int li .notifications {
    background-color: #3ca3c1;
    color: #fff;
    position: absolute;
    left: 42%;
    font-weight: bold;
    padding: 4px;
    border-radius: 12px;
}
    </style>
    <script type="text/javascript">
        function init() {
            window.addEventListener('scroll', function(e){
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                    shrinkOn = 300,
                    header = document.querySelector("header");
                if (distanceY > shrinkOn) {
                    classie.add(header,"smaller");
                } else {
                    if (classie.has(header,"smaller")) {
                        classie.remove(header,"smaller");
                    }
                }
            });
        }
         window.onload = init();
    </script>
</head>
<body>

<header>
    <div class="col-md-12">
        <P></P>
        <center><img src="images/logo_blanco.png"  height="9%" width="9%"></center>
    </div>
</header>

<div class="loader"></div>
</br>
</br>
</br>
</br>
<div class="container-fluid">
    </br>
    </br>
    </br>
    </br>
    
        <div class="col-md-1">
        </div>
        <div class="col-md-12">
        <div class="video-responsive1">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/w8AdA3maVNo" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive2">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/G1P9UNG2vys" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive3">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/ruYvqDm17Us" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive4">
        <center>
       <iframe width="560" height="315" src="https://www.youtube.com/embed/NBgkF9dFHcg" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive5">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/aI06-tW6xLg" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive6">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/uP06WPywzb0" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive7">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/xYFvrJh4f9I" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive8">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/1blT3Bedsy4" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive9">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/AX6ALyW3gQQ" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive10">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/GVe-UaXx5fw" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive11">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/B8RNTisS-_w" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive12">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/CEndbzey3Q8" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        <div class="video-responsive13">
        <center>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/FtZj0Az4tio" frameborder="0" allowfullscreen></iframe>
        </center>
        </div>
        </br>
        </br>
        </div>
    </br>
    </br>
    </br>
    </br>
    </br>
    </br>
    </div>
    </br>
    </br>
    </br>
    </br>
</div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/fullcalendar.js"></script>
    <script src="js/gcal.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.calendar.js"></script>
    <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="lib/validation/jquery.validationEngine.js"></script>
    <script src="lib/validation/jquery.validationEngine-en.js"></script>
    <script src="lib/timepicker/jquery-ui-sliderAccess.js"></script>
    <script src="lib/timepicker/jquery-ui-timepicker-addon.min.js"></script>
    <script src="js/custom.js"></script>
</div>  
</body>
</html>